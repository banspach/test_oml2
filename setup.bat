set HW_DEBUG=1
set HW_DEVEL_BLD=1
set HW_EDITION=business
set HW_NO_FORTRAN=1
set HW_PYTHON_VER=34
set HW_ROOTDIR=%CD:\=/%/hw
set HW_UNITY_ROOTDIR=%CD:\=/%/hw/unity
set HW_TARGET_ARCH=64

@echo MODIFY MY_SANDBOX to a Perforce Workspace on your computer
set MY_SANDBOX=C:/sandboxes/comp22.3_slim_mu

@echo MODIFY HW_THIRDPARTY to a Perforce Workspace on your computer
set HW_THIRDPARTY=%MY_SANDBOX%/hw/third_party

@echo MODIFY HW_FRAMEWORK to a Perforce Workspace on your computer
set HW_FRAMEWORK=%MY_SANDBOX%/common/framework/win64/hwx

@echo Add local binary directory to path
set PATH=%PATH%;%CD%\hw\hw\bin\win64d

@echo ADD A PATH TO AN MKL DIRECTORY from a Perforce Workspace on your computer 
set PATH=%PATH%;%MY_SANDBOX:/=\%\hw\third_party\intel\intel2019u3\win64

@echo ADD A PATH HDF5 DLLs
set PATH=%PATH%;%MY_SANDBOX:/=\%\hw\unity\bin\win64d

@echo ADD A PATH libmatio.dll
set PATH=%PATH%;%MY_SANDBOX:/=\%\hw\hw\bin\win64d