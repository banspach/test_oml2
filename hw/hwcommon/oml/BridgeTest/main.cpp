#include <process.h> // _spawnl
#include <io.h> // _pipe
#include <stdio.h>
#include <fcntl.h> // O_BINARY
#include <string>
#include <iostream>
#include <windows.h>

enum { MODE_CALL_FUNCTION = 1, MODE_EVALUATE_STRING, MODE_SET_VALUE, MODE_GET_VALUE, MODE_EXIT, MODE_CALL_MR_FUNCTION };
enum { TYPE_SCALAR, TYPE_MATRIX, TYPE_STRING, TYPE_STRING_LIST, TYPE_ERROR, TYPE_CELL_LIST };

class OMLBridgeError
{
public:
	OMLBridgeError() : g_err("Error: bridgeserver internal error.") {}
	OMLBridgeError(const std::string& err) { g_err = err; }
	~OMLBridgeError() {}

	std::string g_err;
};

void WriteInt(FILE* file, int val);
void WriteDouble(FILE* file, double val);
void WriteString(FILE* file, const char* str);

void WriteFunctionCall(FILE* file, const char* func_name, double value)
{
	WriteInt(file, MODE_CALL_FUNCTION);
	WriteString(file, func_name);
	WriteInt(file, 1);
	WriteDouble(file, value);
	fflush(file);
}

void WriteMRFunctionCall(FILE* file, const char* func_name, double value, int num_outputs)
{
	WriteInt(file, MODE_CALL_MR_FUNCTION);
	WriteString(file, func_name);
	WriteInt(file, 1);
	WriteInt(file, num_outputs);
	WriteDouble(file, value);
	fflush(file);
}

void WriteGetValue(FILE* file, const char* var_name)
{
	WriteInt(file, MODE_GET_VALUE);
	WriteString(file, var_name);
	fflush(file);
}

void WriteEvaluateString(FILE* file, const char* string)
{
	WriteInt(file, MODE_EVALUATE_STRING);
	WriteString(file, string);
	fflush(file);
}

std::string ReadString(FILE* file)
{
	size_t res;

	int num_chars;
	res = fread(&num_chars, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	char my_string[16384]; //  [1024];
	res = fread(my_string, sizeof(char), num_chars, file);

	if (res == 0)
		throw OMLBridgeError();

	my_string[num_chars] = '\0';

	return my_string;
}

int ReadInt(FILE* file)
{
	if (!file)
	{
		throw OMLBridgeError("Error: bridgeserver input file pointer is invalid");
	}
	int result = 0;
	size_t res = fread(&result, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError("Error: cannot read integer from bridgeserver");

	return result;
}

double ReadDouble(FILE* file)
{
	double my_dbl;
	size_t res = fread(&my_dbl, sizeof(double), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	return my_dbl;
}

void WriteInt(FILE* file, int val)
{
	size_t res = fwrite(&val, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();
}

void WriteDouble(FILE* file, double val)
{
	size_t res;

	int type = TYPE_SCALAR;
	res = fwrite(&type, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(&val, sizeof(double), 1, file);

	if (res == 0)
		throw OMLBridgeError();
}

void WriteString(FILE* file, const char* string)
{
	size_t res;
	int type = TYPE_STRING;
	size_t num_chars = strlen(string);

	res = fwrite(&type, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(&num_chars, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(string, sizeof(char), num_chars, file);

	if (res == 0)
		throw OMLBridgeError();
}

void WriteMatrix(FILE* file, const double* data, int nrows, int ncols)
{
	size_t res;

	int type = TYPE_MATRIX;
	int num_dims = 2; // number of dimensions is always 2
	res = fwrite(&type, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(&num_dims, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(&nrows, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(&ncols, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	// do it this way to switch from row major to column major
	for (int j = 0; j<ncols; j++)
	{
		for (int k = 0; k<nrows; k++)
		{
			res = fwrite(&(data[ncols*k + j]), sizeof(double), 1, file);

			if (res == 0)
				throw OMLBridgeError();
		}
	}
}

void WriteMatrix2(FILE* file, const double* data, int nrows, int ncols)
{
	size_t res;
	int type = TYPE_MATRIX;
	int num_dims = 2; // number of dimensions is always 2
	res = fwrite(&type, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(&num_dims, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(&nrows, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(&ncols, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(data, sizeof(double), nrows*ncols, file);

	if (res == 0)
		throw OMLBridgeError();
}

void WriteStringList(FILE* file, char** data, int nstrings)
{
	size_t res;
	int type = TYPE_STRING_LIST;
	res = fwrite(&type, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	res = fwrite(&nstrings, sizeof(int), 1, file);

	if (res == 0)
		throw OMLBridgeError();

	for (int j = 0; j<nstrings; j++)
		WriteString(file, data[j]);
}

int main(int argc, char* argv[])
{
	FILE* file_in = NULL;
	FILE* file_out = NULL;

	int filedes_out[] = { 0, 0 };
	int filedes_in[] = { 0, 0 };

	if (_pipe(filedes_out, 4096, O_BINARY) < 0)
	{
		std::cout << ("pipe() failed") << std::endl;
		return 1;
	}

	if (_pipe(filedes_in, 4096, O_BINARY) < 0)
	{
		std::cout << ("pipe() failed") << std::endl;
		return 1;
	}

	char buff1[16384]; //  256];
	char buff2[16384]; //  256];

	sprintf_s(buff1, "%d", filedes_out[0]);
	sprintf_s(buff2, "%d", filedes_in[1]);

	// make sure the BridgeServer code allows HW_UNITY_ROOTDIR to not be set
	// otherwise it will immediately exit

#if 0 // Comment line for attaching a debugger on gui startup
	int num = 0;
	std::cout << "Attach a debugger and enter any number: ";
	std::cin >> num;
#endif // Comment line for attaching a debugger on gui startup

	// Clear out the HW_EDITION environment variable as this is how HyperWorks behaves
#ifdef OS_WIN
	_putenv_s("HW_EDITION", "");
#else
	setenv("HW_EDITION", "", 1);
#endif

	// Set up the exe path/names.
	std::string name("OMLBridgeServer");
#ifdef OS_WIN
	name += ".exe";
#endif

	std::string workspace("c:\\software\\hs2022"); // Update this for your workspace
	std::string dir("\\hw\\hw\\bin\\");
#ifdef OS_WIN
#    ifdef _DEBUG
		dir += "win64d\\";
#    else
		dir += "win64\\";
#    endif
#endif

	intptr_t childid = _spawnl(P_DETACH, (workspace + dir + name).c_str(), name.c_str(),
		buff1, buff2, 0);
#if 0
	childid = _spawnl(P_DETACH, "c:\\sandboxes\\comp21.2_mu\\hw\\hw\\bin\\win64d\\OMLBridgeServer.exe", "OMLBridgeServer.exe", buff1, buff2, 0);
#endif

	if (childid < 0)
	{
		std::cout << ("_spawnl failed") << std::endl;
		return 1;
	}

	_close(filedes_out[0]);
	_close(filedes_in[1]);

	file_in = _fdopen(filedes_in[0], "rb");
	file_out = _fdopen(filedes_out[1], "wb");

	if (!file_in)
	{
		std::cout << ("file_in is NULL") << std::endl;
		return 1;
	}


	if (!file_out)
	{
		std::cout << ("file_out is NULL") << std::endl;
		return 1;
	}

	// version info for later
	double version = 1.0;
	fwrite(&version, sizeof(double), 1, file_out);

	int    type = -1;
	double result = 0.0;
	std::string strResult;

	try
	{
		WriteEvaluateString(file_out, "version");
		type = ReadInt(file_in);
		if (type == TYPE_STRING)
		{
			std::cout << "Answer is: " << ReadString(file_in) << std::endl;
		}


		WriteEvaluateString(file_out, "rand()");
		type = ReadInt(file_in);

		if (type == TYPE_SCALAR)
		{
			result = ReadDouble(file_in);
			std::cout << "Answer is: " << result << std::endl;
		}
	}
	catch (const OMLBridgeError& e)
	{
		std::cout << e.g_err << std::endl;
	}


	while (1)
	{
		std::string strCommand, partialCommand;
		std::getline(std::cin, strCommand);

		try
		{
			WriteEvaluateString(file_out, strCommand.c_str());
			type = ReadInt(file_in);

			switch (type)
			{
			case TYPE_SCALAR:
				std::cout << "Return TYPE_SCALAR" << std::endl;
				result = ReadDouble(file_in);
				std::cout << "Answer is: " << result << std::endl;
				break;
			case TYPE_MATRIX:
				std::cout << "Return TYPE_MATRIX" << std::endl;
				break;
			case TYPE_STRING:
				std::cout << "Return TYPE_STRING" << std::endl;
				strResult = ReadString(file_in);
				std::cout << "Answer is: " << strResult << std::endl;
				break;
			case TYPE_STRING_LIST:
				std::cout << "Return TYPE_STRING_LIST" << std::endl;
				break;
			case TYPE_ERROR:
				std::cout << "Return TYPE_ERROR" << std::endl;
				strResult = ReadString(file_in);
				std::cout << "Answer is: " << strResult << std::endl;
				break;
			case TYPE_CELL_LIST:
				std::cout << "Return TYPE_CELL_LIST" << std::endl;
				int num_out = ReadInt(file_in);

				for (int j = 0; j < num_out; ++j)
				{
					type = ReadInt(file_in);

					if (type == TYPE_SCALAR) // not complete for multi-ret functions
					{
						result = ReadDouble(file_in);
						std::cout << "Answer is: " << result << std::endl;
					}
				}
				break;
			}
			WriteInt(file_out, MODE_EXIT);
		}
		catch (const OMLBridgeError& e)
		{
			std::cout << e.g_err << std::endl;
		}
	}

	fclose(file_in);
	fclose(file_out);

	return 0;
}
