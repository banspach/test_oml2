/**
* @file dummyomlmenuapistbox.cxx
* @date March 2021
* Copyright (C) 2021 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include "dummyomlmenuapistbox.h"

#include "Currency.h"

#define TBOXVERSION 2021.1
#define OMLMENU "Gui/IDE Customization"

//------------------------------------------------------------------------------
// Returns toolbox version
//------------------------------------------------------------------------------
double GetToolboxVersion(EvaluatorInterface eval)
{
    return TBOXVERSION;
}
//------------------------------------------------------------------------------
// Dummy method, returning 1 if needed - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyIntFunc(EvaluatorInterface           eval,
                  const std::vector<Currency>& inputs,
                  std::vector<Currency>&       outputs)
{
    if (eval.GetNargoutValue() > 0)
    {
        outputs.push_back(0);
    }
    return true;
}
//------------------------------------------------------------------------------
// Entry point which registers toolbox with oml
//------------------------------------------------------------------------------
int InitDll(EvaluatorInterface eval)
{
	// Returns dummy values for functions that cannot be run in console/batch
		
	eval.RegisterBuiltInFunction("addribbonpage", &DummyIntFunc, FunctionMetaData(2, 1, OMLMENU));
    eval.RegisterBuiltInFunction("removeribbonpage", &DummyIntFunc, FunctionMetaData(1, 1, OMLMENU));
    eval.RegisterBuiltInFunction("addribbonmenu", &DummyIntFunc, FunctionMetaData(2, 1, OMLMENU));
    eval.RegisterBuiltInFunction("removeribbonmenu", &DummyIntFunc, FunctionMetaData(2, 1, OMLMENU));
    eval.RegisterBuiltInFunction("addpopupmenu", &DummyIntFunc, FunctionMetaData(2, 1, OMLMENU));
    eval.RegisterBuiltInFunction("removepopupmenu", &DummyIntFunc, FunctionMetaData(1, 1, OMLMENU));
    eval.RegisterBuiltInFunction("addmenuitem", &DummyIntFunc, FunctionMetaData(6, 1, OMLMENU));
    eval.RegisterBuiltInFunction("removemenuitem", &DummyIntFunc, FunctionMetaData(3, 1, OMLMENU));
    eval.RegisterBuiltInFunction("setmenuaccel", &DummyIntFunc, FunctionMetaData(2, 1, OMLMENU));
    eval.RegisterBuiltInFunction("addmenuseparator", &DummyIntFunc, FunctionMetaData(1, 1, OMLMENU));

    //Sprite actions
    eval.RegisterBuiltInFunction("addribbonpagegroup", &DummyIntFunc, FunctionMetaData(2, 1, OMLMENU));
    eval.RegisterBuiltInFunction("removeribbonpagegroup", &DummyIntFunc, FunctionMetaData(2, 1, OMLMENU));
    eval.RegisterBuiltInFunction("addspriteactiongroup", &DummyIntFunc, FunctionMetaData(6, 1, OMLMENU));
    eval.RegisterBuiltInFunction("removespriteactiongroup", &DummyIntFunc, FunctionMetaData(1, 1, OMLMENU));
    eval.RegisterBuiltInFunction("addspriteaction", &DummyIntFunc, FunctionMetaData(8, 1, OMLMENU));
    eval.RegisterBuiltInFunction("removespriteaction", &DummyIntFunc, FunctionMetaData(1, 1, OMLMENU));
    eval.RegisterBuiltInFunction("movesprite", &DummyIntFunc, FunctionMetaData(3, 1, OMLMENU));
    eval.RegisterBuiltInFunction("enablespriteaction", &DummyIntFunc, FunctionMetaData(2, 1, OMLMENU));
    eval.RegisterBuiltInFunction("showspriteaction", &DummyIntFunc, FunctionMetaData(2, 1, OMLMENU));
	
    return 1;
}

