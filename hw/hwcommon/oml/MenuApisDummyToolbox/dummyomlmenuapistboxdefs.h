/**
* @file dummyomlmenuapistboxdefs.h
* @date March 2021
* Copyright (C) 2021 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#ifndef __DUMMY_OMLMENUAPIS_TBOX_DEFS_H__
#define __DUMMY_OMLMENUAPIS_TBOX_DEFS_H__

//------------------------------------------------------------------------------
//!
//! \brief Contains macro definitions for exporting functions in the library
//!
//------------------------------------------------------------------------------
// Windows export macro
#ifdef OS_WIN
#ifdef DUMMY_OMLMENUAPISTBOX_EXPORT
#undef DUMMY_OMLMENUAPISTBOX_DECLS
#define DUMMY_OMLMENUAPISTBOX_DECLS __declspec(dllexport)
#else
#undef  DUMMY_OMLMENUAPISTBOX_DECLS
#define DUMMY_OMLMENUAPISTBOX_DECLS __declspec(dllimport)
#endif  // DUMMY_OMLMENUAPISTBOX_EXPORT
#else
#undef  DUMMY_OMLMENUAPISTBOX_DECLS
#define DUMMY_OMLMENUAPISTBOX_DECLS
#endif // OS_WIN

#endif // __DUMMY_OMLMENUAPIS_TBOX_DEFS_H__

