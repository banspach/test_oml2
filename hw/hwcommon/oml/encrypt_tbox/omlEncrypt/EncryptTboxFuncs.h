/**
* @file EncryptTboxFuncs.h
* @date January 2015
* Copyright (C) 2015-2018 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#pragma warning(disable: 4251)

#include "EvaluatorInt.h"
#include "EncryptTboxDefs.h"

extern "C" 
{
    ENCRYPTTBOX_DECLS int InitDll(EvaluatorInterface eval);
    //!
    //! Returns toolbox version
    //! \param eval Evaluator interface
    //!
    ENCRYPTTBOX_DECLS double GetToolboxVersion(EvaluatorInterface eval);
}

bool hml_load(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool hml_save(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);

