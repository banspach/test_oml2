/**
* @file omlPythonBridgeSignal.cxx
* @date April, 2018
* Copyright (c) 2013-2018 Altair Engineering Inc. All Rights Reserved. Contains trade 
* secrets of Altair Engineering Inc. Copyright notice does not imply publication. 
* Decompilation or disassembly of this software is strictly prohibited.
*/

// Begin defines/includes
#include "omlPythonBridgeSignal.h"
// End defines/includes

omlPythonBridgeSignal* omlPythonBridgeSignal::_instance = NULL;

omlPythonBridgeSignal* omlPythonBridgeSignal::GetInstance()
{
    if (!_instance)
    {
        _instance = new omlPythonBridgeSignal();
    }
    return _instance;
}

void omlPythonBridgeSignal::ReleaseInstance()
{
    delete _instance;
    _instance = NULL;
}

//! Constructor
omlPythonBridgeSignal::omlPythonBridgeSignal()
{

}

//! Destructor
omlPythonBridgeSignal::~omlPythonBridgeSignal()
{

}
