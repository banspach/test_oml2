/**
* @file omlPythonBridgeSignalDefs.h
* @date April, 2018
* Copyright (c) 2013-2018 Altair Engineering Inc. All Rights Reserved. Contains trade 
* secrets of Altair Engineering Inc. Copyright notice does not imply publication. 
* Decompilation or disassembly of this software is strictly prohibited.
*/
#ifndef OMLPYTHONBRIDGESIGNAL_H
#define OMLPYTHONBRIDGESIGNAL_H

#ifdef OS_WIN
#   ifdef OMLPYTHONBRIDGESIGNAL_EXPORTS
#      undef  OMLPYTHONBRIDGESIGNAL_DECLS
#      define OMLPYTHONBRIDGESIGNAL_DECLS __declspec(dllexport)
#   else
#      undef  OMLPYTHONBRIDGESIGNAL_DECLS
#      define OMLPYTHONBRIDGESIGNAL_DECLS __declspec(dllimport)
#   endif  // OMLPYTHONBRIDGESIGNAL_EXPORTS
#else
#   undef  OMLPYTHONBRIDGESIGNAL_DECLS
#   define OMLPYTHONBRIDGESIGNAL_DECLS
#endif // OS_WIN

#ifndef NULL
#define NULL 0
#endif

#endif
