/**
* @file omlPythonBridgeSignal.h
* @date April, 2018
* Copyright (c) 2013-2022 Altair Engineering Inc. All Rights Reserved. Contains trade 
* secrets of Altair Engineering Inc. Copyright notice does not imply publication. 
* Decompilation or disassembly of this software is strictly prohibited.
*/

#ifndef OML_PYTHON_BRIDGE_SIGNAL_H__
#define OML_PYTHON_BRIDGE_SIGNAL_H__

#include <sigslot/hwSigslot.h>
#include "Currency.h"
#include "omlPythonBridgeSignalDefs.h"

class OMLPYTHONBRIDGESIGNAL_DECLS omlPythonBridgeSignal
{
public:

    static omlPythonBridgeSignal* GetInstance();
    static void ReleaseInstance();
    typedef hwiSignal0<void> hwiSignal0_OnEndEval;
    typedef hwiSignal1<bool, bool> hwiSignal1_OnLoad;
    typedef hwiSignal3<void,hwString, bool, bool &> hwiSignal3_OnEval;
    typedef hwiSignal4<void, hwString, std::vector<Currency>&, bool &, std::string &> hwiSignal4_OnGetPythonVar;
    typedef hwiSignal4<void, hwString, const Currency &, bool &, std::string &> hwiSignal4_OnSetPythonVar;

    hwiSignal0_OnEndEval &  OnEndEval    () {return m_EndEval;};
    hwiSignal1_OnLoad&  OnLoad() { return m_OnLoad; };
    hwiSignal3_OnEval &  OnEvalFile() { return m_OnEvalFile; };
    hwiSignal3_OnEval &  OnEvalScript() { return m_OnEvalScript; };
    hwiSignal4_OnGetPythonVar &  OnGetPythonVar() { return m_OnGetPythonVar; };
    hwiSignal4_OnSetPythonVar &  OnSetPythonVar() { return m_OnSetPythonVar; };
private:
    
    //! Constructor
    omlPythonBridgeSignal();
    //! Copy Constructor
    omlPythonBridgeSignal( const omlPythonBridgeSignal& ) {};
    //! Assignment Operator
    omlPythonBridgeSignal& operator=( const omlPythonBridgeSignal& ) { return *_instance;};
    //! Destructor
    ~omlPythonBridgeSignal();
    
    static omlPythonBridgeSignal* _instance;
    hwiSignal0_OnEndEval   m_EndEval;
    hwiSignal1_OnLoad   m_OnLoad;
    hwiSignal3_OnEval   m_OnEvalFile, m_OnEvalScript;
    hwiSignal4_OnGetPythonVar m_OnGetPythonVar;
    hwiSignal4_OnSetPythonVar m_OnSetPythonVar;
};

#endif //OML_PYTHON_BRIDGE_SIGNAL_H__
