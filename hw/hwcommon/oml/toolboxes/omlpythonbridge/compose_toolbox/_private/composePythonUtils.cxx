/**
* @file composePythonUtils.cxx
* @date February 2015
* Copyright (C) 2015-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  Licensee may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#define PBOC "PythonBridgeOmlCommands"
#define TBOXVERSION 2020

#include "Python.h"

#include "composePythonUtils.h"
#include "EvaluatorInt.h"
#include "hwInterp.h"
#include "OML_Error.h"
#include "vos/vosLibrary.h"
#include "StructData.h"
#include "SignalHandlerBase.h"
#include "hwPythonInterpCreator.h"
#include <utl/hwWString.h>
#include "hwversion.h"
#include "omlPythonBridgeSignal.h"
#include "OmlPythonBridgeCore.h"
// Windows specific
#if defined(OS_WIN)
#include <shlobj.h>  // for SHGetKnownFolderPath()
#else
#include <dlfcn.h>
#endif

namespace {
static int functionCallDepth = 0;
static bool isAnalysisInProgress = false;
static bool suppressCommandWindow = false;
static bool isCommandWindowLoaded = false;
static bool isPyInterpLocalInit = false;

void refreshPythonVariableBrowser()
{
    omlPythonBridgeSignal::GetInstance()->OnEndEval().Emit();
}

bool errorNotificationPython(const hwString &error)
{
    OmlPythonBridgeCore::GetInstance()->SetErrorMessage(error);
    return true;
}

bool isUIMode(EvaluatorInterface eval)
{
    static bool isUIMode = false;
    static bool isModeChecked = false;
    if (!isModeChecked)
    {
        isModeChecked = true;
        SignalHandlerBase* handler = eval.GetSignalHandler();
        if (handler)
        {
            if (handler->CanExecuteInGuiThread())
            {
                isUIMode = true;
            }
        }
    }

    return isUIMode;
}

// GetHomeDirectory duplicated from unity\common\application\_private\Application.cxx
hwString GetHomeDirectory()
{
    static hwString s_homeDirectory = "";
    if (!s_homeDirectory.isEmpty())
        return s_homeDirectory;

    bool isUsingTemp = false;
#ifdef OS_WIN

    wchar_t *homePathW = NULL;

    // The below method is applicable to all window platforms such as windows xp, vista, 7 etc.
    HRESULT result = SHGetKnownFolderPath(FOLDERID_Profile, 0, NULL, &homePathW);
    if (result == S_OK)
    {
        s_homeDirectory = hwWString(homePathW).narrow_string();
        CoTaskMemFree(homePathW);
    }
    else
    {
        // This could be S_FALSE, E_FAIL and INVALIDARG
        // As a fallback only, trying using %userprofile%
        wchar_t* userProfW = _wgetenv(L"userprofile");
        if (userProfW)
        {
            s_homeDirectory = hwWString(userProfW).narrow_string();
        }
    }

    if (s_homeDirectory.empty())
    {
        s_homeDirectory = "C:/temp";
        isUsingTemp = true;
    }

#else

    vosLibrary lib;
    lib.GetEnvironmentVariable(s_homeDirectory, "HOME");
    if (s_homeDirectory.empty())
    {
        s_homeDirectory = "/tmp";
        isUsingTemp = true;
    }

#endif

    if (isUsingTemp && !s_homeDirectory.isEmpty())
    {
        // Make sure everyones consistent including 
        // Tcl_CreateInterp & QDir::home() or QDir::homePath()
        // %APPDATA%? and/or SHGetKnownFolderPath(FOLDERID_,...) might be considered also

        // Actually, I don't think this approach works. Putenv don't make the set values available outside of dynamic objects.
        hwString newHomeEnv = "HOME=" + s_homeDirectory;

#ifdef OS_WIN

        _wputenv(hwWString(newHomeEnv).c_str());

        // Replace backslashes before returning the value
        std::replace(s_homeDirectory.begin(), s_homeDirectory.end(), '\\', '/');

#else
        putenv((char*)newHomeEnv.c_str());
#endif

    }

    return s_homeDirectory;
}

// GetHomeDirectory duplicated from unity\common\application\_private\Application.cxx
hwString GetSettingsDirectory(EvaluatorInterface eval)
{
    static hwString s_settingsDirectory = "";
    if (!s_settingsDirectory.isEmpty())
        return s_settingsDirectory;

    s_settingsDirectory = GetHomeDirectory();
    std::vector<Currency> params;
    Currency arg_count = eval.CallFunction("getargc", params);

    int count = static_cast<int>(arg_count.Scalar());
    bool found = false;
    int index = -1;
    for (int i = 1; i <= count; i++)
    {
        params.clear();
        params.push_back(i);
        Currency arg_v = eval.CallFunction("getargv", params);
        if ("-v" == arg_v.StringVal())
        {
            index = i + 1;
            found = true;
            break;
        }
    }

    if (found && index < count)
    {
        params.clear();
        params.push_back(index);
        Currency arg_v = eval.CallFunction("getargv", params);
        s_settingsDirectory += "/.altair/" + arg_v.StringVal() + "/hwx";
    }
    else
        s_settingsDirectory += "/.altair/" HW_VERSION_STR "/hwx";    // use default value

    return s_settingsDirectory;
}

class GetPythonObject
{
public:
    //! Constructor
    GetPythonObject(const hwString& name, std::vector<Currency>& outputs) 
    : m_name(name), m_value(outputs) {}
    //! Destructor
    virtual ~GetPythonObject() {}
    //! Operator ()
    virtual bool operator ()()
    {   
        bool success = false;
        hwInterp *interp = hwInterp::Get("python");

        if (interp) 
        {
            PyGILState_STATE state = PyGILState_Ensure();
            PyObject *m = PyImport_AddModule("__main__");
            PyObject *nameobj = NULL;
            OmlPythonBridgeCore::GetInstance()->ConvertCurrencyToPyObject(nameobj, Currency(m_name));
            
            if (PyObject_HasAttr(m,nameobj))
            {
                PyObject* valueobj = PyObject_GetAttr(m,nameobj);			
                bool isTypeSupported = false;		

                if (valueobj)
                {
                    //check for limitations:  1. list to cell 2. dict to struct
                    if (PyList_Check(valueobj) || PyDict_Check(valueobj))
                    {
                        isTypeSupported = OmlPythonBridgeCore::GetInstance()->IsTypeSupported(valueobj);
                    } 
                    else
                    {
                        isTypeSupported = true;
                    }
                
                    if (isTypeSupported)
                        success = OmlPythonBridgeCore::GetInstance()->ConvertPyObjectToCurrency(m_value, valueobj);
                    
                    Py_DECREF(valueobj);
                }
            
                if (!isTypeSupported)
                {
                    m_value.push_back("");
                }

                OmlPythonBridgeCore::GetInstance()->HandleException();
            } 
            else
            {
                m_value.push_back("");
                m_value.push_back("0");
                m_value.push_back("NameError: name '"+m_name+"' is not defined");
            }

            Py_XDECREF(nameobj);

            PyGILState_Release(state);
        }
        return success;
    }

private:
    const hwString &m_name;
    std::vector<Currency> &m_value;
};

class SetPythonObject
{
public:
    //! Constructor
    SetPythonObject(const hwString& name, const Currency &value) 
    : m_name(name), m_value(value) {}
    //! Destructor
    virtual ~SetPythonObject() {}
    //! Operator ()
    virtual bool operator ()()
    {   
        bool success = false;
        hwInterp *interp = hwInterp::Get("python");

        if (interp) 
        {
            OmlPythonBridgeCore::GetInstance()->SetErrorMessage("");
            PyGILState_STATE state = PyGILState_Ensure();
            PyObject *m = PyImport_AddModule("__main__");
            PyObject *valueobj = NULL;
            PyObject *nameobj = NULL;
            int status = -1;

            OmlPythonBridgeCore::GetInstance()->ConvertCurrencyToPyObject(valueobj, m_value);
            if (valueobj != NULL)
            {
                OmlPythonBridgeCore::GetInstance()->ConvertCurrencyToPyObject(nameobj, Currency(m_name));
                
                if (nameobj != NULL)
                {
                    if (PyUnicode_IsIdentifier(nameobj))
                        status = PyObject_SetAttr(m, nameobj, valueobj);
                    else
                        OmlPythonBridgeCore::GetInstance()->SetErrorMessage("Error: invalid variable name");

                    Py_DECREF(nameobj);
                }
                Py_DECREF(valueobj);
            }
            
            if (status == 0)
            {
                success = true;
            }
    
            OmlPythonBridgeCore::GetInstance()->HandleException();
            PyGILState_Release(state);
        }
        return success;
    }

private:
    const hwString& m_name;
    const Currency &m_value;
};


hwInterp * getPythonInterp(EvaluatorInterface eval)
{
    hwInterp* interp = hwInterp::Get("python");
    if (isUIMode(eval))
    {
        if (!isCommandWindowLoaded)
        {
            if (!suppressCommandWindow || !interp)
            {
                hwSigSlotAggregate<hwBoolList> combiner = omlPythonBridgeSignal::GetInstance()->OnLoad().
                                                          Emit<hwSigSlotAggregate<hwBoolList> >(suppressCommandWindow);
                
                hwBoolList ret_value_list = combiner.GetValue();
                for (auto ret_value : ret_value_list)
                {
                    if (ret_value)
                    {
                        isCommandWindowLoaded = ret_value;
                        break;
                    }   
                }
            }

            if (!interp)
                interp = hwInterp::Get("python");
        }
    }
    else if (!interp)
    {
#ifndef OS_WIN
        hwString name("libpython");
        name += std::to_string(PY_MAJOR_VERSION) + "." + std::to_string(PY_MINOR_VERSION);
#   ifdef _DEBUG
        name += "d";
#   endif
        name += ".so";
        dlopen(name.c_str(), RTLD_GLOBAL | RTLD_NOW);
#endif
        char* hw_root = getenv("HW_ROOTDIR");
        if (nullptr == hw_root)
        {
            hw_root = getenv("ALTAIR_HOME");
            if (nullptr == hw_root)
            {
                throw OML_Error("ALTAIR_HOME or HW_ROOTDIR must be set");
            }
        }

        hwString settingsDir = GetSettingsDirectory(eval);

        hwStringList argList;
        std::vector<std::string> argv(
            OmlPythonBridgeCore::GetInstance()->GetArgv(eval));
        int argc = static_cast<int>(argv.size());
        argList.reserve(argc);
        for (int i = 0; i < argc; ++i)
        {
            argList.push_back(argv[i].c_str());
        }
        isPyInterpLocalInit = true;
        hwInterp::RegisterCreator(new hwPythonInterpCreator(
            argList,
            hw_root,
            NULL,
            settingsDir));
        /*hwInterp *interp = hwInterp::Get("python");

        if (interp)
        {
            hwString unity_dir = getenv("HW_UNITY_ROOTDIR");
            hwString u_bin_dir = vosLibrary::GetAbsoluteFileName(unity_dir + "/bin/$PLATFORM");
            interp->EvalScript("import sys; sys.path.append(r'" + u_bin_dir + "');");
            hwString file = vosLibrary::GetAbsoluteFileName(unity_dir + "/plugins/interpreters/core/python/python.py");
            interp->EvalFile(file);
        }*/

        interp = hwInterp::Get("python");
        if (interp)
        {
            char* framework_root = getenv("HW_FRAMEWORK");
            hwString framework_bin_dir = framework_root ? framework_root : "";
            framework_bin_dir = vosLibrary::GetAbsoluteFileName(framework_bin_dir + "/bin/$PLATFORM");                        
            interp->EvalScript("import sys; import os;\nif os.path.normpath('" + framework_bin_dir + "') not in sys.path: sys.path.append(os.path.normpath('" + framework_bin_dir + "'));");            
            
            char* unity_root = getenv("HW_UNITY_ROOTDIR");
            hwString file = unity_root ? unity_root : "";
            file += "/scripts/python/hwx/compose/omlpythonbridge/omlpythonbridge.py";
            interp->EvalFile(file);
        }
    }

    return interp;
}

void checkFunctionCallDepth()
{
    // count 44 is derived from function MemoryScopeManager::OpenScope. 
    if (functionCallDepth == 44)
    {
        functionCallDepth = 0;
        hwInterp *interp = hwInterp::Get("oml");
        if(interp)
            interp->Interrupt();

        throw OML_Error("Maximum function depth reached");
    }
}
}

//Start oml functions
bool oml_evalpythonfile(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    bool results = false;
    size_t nargin = inputs.size();
    
    checkFunctionCallDepth();

    if (nargin != 1)
    {
        throw OML_Error("Valid syntax is \"evalpythonfile filename.\"");
    }
    
    if (!(inputs[0].IsString()))
    {
        throw OML_Error("Valid syntax is \"evalpythonfile filename.File name must be string.\"");
    }
    
    functionCallDepth = functionCallDepth+1;
    try
    {

        hwInterp* interp = getPythonInterp(eval);

        if (interp)
        {
            hwString file((inputs[0].StringVal()));
            OmlPythonBridgeCore::GetInstance()->SetErrorMessage("");
            interp->SetAlert(&errorNotificationPython);
            if (isUIMode(eval))
            {
                omlPythonBridgeSignal::GetInstance()->OnEvalFile().Emit(file, (isAnalysisInProgress || suppressCommandWindow), results);
            }
            else
            {
                results = interp->EvalFile(file);
            }
            interp->SetAlert(NULL);
        }

        if(results)
        {
            outputs.push_back(1);
        }
        else
        {
            outputs.push_back(0);
        }
    
        outputs.push_back(OmlPythonBridgeCore::GetInstance()->GetErrorMessage());
        if(functionCallDepth > 0)
            functionCallDepth = functionCallDepth-1;
    }
    catch (OML_Error)
    {
        if(functionCallDepth > 0)
            functionCallDepth = functionCallDepth-1;
    }
    return results;
}

bool oml_evalpythonscript(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    bool results = false;	
    size_t nargin = inputs.size();

    checkFunctionCallDepth();

    if (nargin != 1)
    {
        throw OML_Error("Valid syntax is \"evalpythonfile script.\"");
    }
    
    if (!(inputs[0].IsString()))
    {
        throw OML_Error("Valid syntax is \"evalpythonfile script.Script must be string.\"");
    }

    functionCallDepth = functionCallDepth+1;
    try
    {

        hwInterp* interp = getPythonInterp(eval);

        if (interp)
        {
            hwString script((inputs[0].StringVal()));
            OmlPythonBridgeCore::GetInstance()->SetErrorMessage("");
            interp->SetAlert(&errorNotificationPython);
            if (isUIMode(eval))
            {
                omlPythonBridgeSignal::GetInstance()->OnEvalScript().Emit(script, (isAnalysisInProgress || suppressCommandWindow), results);
            }
            else
            {
                results = interp->EvalScript(script);
            }
            interp->SetAlert(NULL);
        }
        if (results)
        {
            outputs.push_back(1);
        }
        else
        {
            outputs.push_back(0);
        }

        outputs.push_back(OmlPythonBridgeCore::GetInstance()->GetErrorMessage());
        if(functionCallDepth > 0)
            functionCallDepth = functionCallDepth-1;
    }
    catch (OML_Error)
    {
        if(functionCallDepth > 0)
            functionCallDepth = functionCallDepth-1;
    }
    return results;
}

bool oml_getpythonvar(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    bool results = false;
    hwString msg = "";
    size_t nargin = inputs.size();

    if (nargin != 1)
    {
        throw OML_Error("Valid syntax is \"getpythonvar python_var.\"");
    }
    
    if (!(inputs[0].IsString()))
    {
        throw OML_Error("Valid syntax is \"getpythonvar python_var.python_var must be string.\"");
    }

    if (inputs[0].IsEmpty())
    {
        throw OML_Error("python_var should not be empty.");
    }

    hwInterp *interp = getPythonInterp(eval);

    if (interp)
    {
        hwString var((inputs[0].StringVal()));
        
        if (isUIMode(eval))
        {
            std::string error = "";
            omlPythonBridgeSignal::GetInstance()->OnGetPythonVar().Emit(var, outputs, results, error);
            OmlPythonBridgeCore::GetInstance()->SetErrorMessage(error);
        }
        else
        {
            GetPythonObject obj(var,outputs);
            results = obj();
        }

        if (!results)
        {
            outputs.push_back(0);
            outputs.push_back("NameError: name '"+var+"' is not defined");
        }
        else
        {
            outputs.push_back(1);
            outputs.push_back("");
        }
    } 
    else
    {
        outputs.push_back(0);
        outputs.push_back("Python Interpreter is not avialable.");
    }

    return results;
}

bool oml_exporttopython (EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    bool results = false;
    size_t nargin = inputs.size();

    if (nargin != 2)
    {
        throw OML_Error("Valid syntax is \"exporttopython oml_var python_var.\"");
    }


    if (!(inputs[0].IsString() || inputs[0].IsScalar() || inputs[0].IsComplex() || inputs[0].IsMatrix() 
        || inputs[0].IsNDMatrix() || inputs[0].IsStruct() || inputs[0].IsCellArray() || inputs[0].IsSparse()))
    {
        throw OML_Error("Supported OML types for export are numbers, strings, complex, struct, Cell, matrix, ndmatrix and sparse matrix.");
    }

    if (inputs[1].IsEmpty())
    {
        throw OML_Error("python_var should not be empty.");
    }
    
    if (!(inputs[1].IsString()))
    {
        throw OML_Error("\"python_var must be string.\"");
    }

    hwInterp *interp = getPythonInterp(eval);

    if (interp)
    {	
        hwString var((inputs[1].StringVal()));
        
        if (isUIMode(eval))
        {
            std::string error = "";
            omlPythonBridgeSignal::GetInstance()->OnSetPythonVar().Emit(var, inputs[0], results, error);
            OmlPythonBridgeCore::GetInstance()->SetErrorMessage(error);
        }
        else
        {
            SetPythonObject obj(var,inputs[0]);
            results = obj();
        }

        if (results)
        {
            outputs.push_back(1);
            if (isUIMode(eval) && !(isAnalysisInProgress || suppressCommandWindow))
            {
                refreshPythonVariableBrowser();
            }
        }
        else
        {
            outputs.push_back(0);
        }

        outputs.push_back(OmlPythonBridgeCore::GetInstance()->GetErrorMessage());
    }
    else
    {
        outputs.push_back(0);
        outputs.push_back("Python Interpreter is not avialable.");
    }

    return results;
}

bool oml_analysisusingpythonbegin (EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    isAnalysisInProgress = true;
    return true;
}

bool oml_analysisusingpythonend (EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    isAnalysisInProgress = false;
    if (isUIMode(eval) && !suppressCommandWindow)
    {
        refreshPythonVariableBrowser();
    }
    return true;
}

bool oml_suppresspythoncmdwindow(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    if (inputs.size() != 1)
    {
        throw OML_Error("Valid syntax is \"suppresspythoncmdwindow flag.\"");
    }

    if (!inputs[0].IsLogical())
        throw OML_Error("Valid syntax is \"suppresspythoncmdwindow flag.flag must be logical.\"");

    suppressCommandWindow = inputs[0].Scalar();
    return true;
}

// register python interface methods.
extern "C" COMPOSEPYTHONUTIL_DECLS int InitDll(EvaluatorInterface evl)
{
    evl.RegisterBuiltInFunction("evalpythonfile", oml_evalpythonfile, FunctionMetaData(1, 2, PBOC));
    evl.RegisterBuiltInFunction("evalpythonscript", oml_evalpythonscript, FunctionMetaData(1, 2, PBOC));
    evl.LockBuiltInFunction("evalpythonscript", false);
    evl.RegisterBuiltInFunction("getpythonvar", oml_getpythonvar, FunctionMetaData(1, 3, PBOC));
    evl.RegisterBuiltInFunction("exporttopython", oml_exporttopython, FunctionMetaData(2, 2, PBOC));
    evl.RegisterBuiltInFunction("analysisusingpythonbegin", oml_analysisusingpythonbegin, FunctionMetaData(0, 0, PBOC));
    evl.RegisterBuiltInFunction("analysisusingpythonend", oml_analysisusingpythonend, FunctionMetaData(0, 0, PBOC));
    evl.RegisterBuiltInFunction("suppresspythoncmdwindow", oml_suppresspythoncmdwindow, FunctionMetaData(1, 0, PBOC));

    return 0;
}
//------------------------------------------------------------------------------
// Returns toolbox version
//------------------------------------------------------------------------------
extern "C" COMPOSEPYTHONUTIL_DECLS double GetToolboxVersion(EvaluatorInterface eval)
{
    return TBOXVERSION;
}

extern "C" void Finalize()
{
    if (isPyInterpLocalInit)
    {
        hwInterp *pyInterp = hwInterp::Get ("py");
        hwInterp::RegisterEvalNotificationHandler(nullptr);
        delete pyInterp;
        hwIInterpCreator *pyInterpCreator =  hwInterp::UnregisterCreator ("py");
        delete pyInterpCreator;
    }
}

