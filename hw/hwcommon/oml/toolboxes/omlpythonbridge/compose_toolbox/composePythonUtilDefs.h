#ifndef COMPOSEPYTHONUTIL_H
#define COMPOSEPYTHONUTIL_H

#ifdef OS_WIN
#   ifdef COMPOSEPYTHONUTIL_EXPORTS
#      undef  COMPOSEPYTHONUTIL_DECLS
#      define COMPOSEPYTHONUTIL_DECLS __declspec(dllexport)
#   else
#      undef  COMPOSEPYTHONUTIL_DECLS
#      define COMPOSEPYTHONUTIL_DECLS __declspec(dllimport)
#   endif  // COMPOSEPYTHONUTIL_EXPORTS
#else
#   undef  COMPOSEPYTHONUTIL_DECLS
#   define COMPOSEPYTHONUTIL_DECLS
#endif // OS_WIN

#ifndef NULL
#define NULL 0
#endif

#endif
