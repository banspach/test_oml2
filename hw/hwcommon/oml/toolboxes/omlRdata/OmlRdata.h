/**
* @file OmlRdata.h
* @date October 2021
* Copyright (C) 2021-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/
#ifndef __OMLRDATA_H__
#define __OMLRDATA_H__

#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <unordered_map>
#include <rData/Tools/Bridge.h>
#include "OmlRdataDefs.h"
#include "OMLInterfacePublic.h"
namespace omlrdata
{
    //ToDo: remove GetErrorMessage function once ThrowError start accepting argument index
    std::string GetErrorMessage(const std::string message, long long arg_index);

    void StringCheck(OMLInterface* eval, const OMLCurrency* input,
        long long arg_index);

    void CellArryCheck(OMLInterface* eval, const OMLCurrency* input,
        long long arg_index);
    
	void ScalarCheck(OMLInterface* eval, const OMLCurrency* input,
        long long arg_index);
    //static inline boost::shared_ptr<Bridge::Model> __getModel(OMLInterface* i, std::string id);
    //static void AttrToStr(std::ostringstream& os, IAttributeListPtr attr);

    void Initialize();

    void Clear();

    bool StopRdataApplication(OMLInterface* eval, const OMLCurrencyList* inputs,
        OMLCurrencyList* outputs);

    std::string CreateApplicationCore(OMLInterface* eval, const std::string& appname,
        const std::string& console);

    void DeleteApplicationCore(const std::string& app_id);

    std::string AddResourceCore(OMLInterface* eval, const std::string& app_id,
        const std::string& res_label, const std::string& path);

    void RemoveResourceCore(OMLInterface* eval, const std::string& app_id,
        const std::string& res_label);

    std::string CreateModelCore(OMLInterface* eval, const std::string& app_id,
        const std::string& model_label,
        const std::string& res_label,
        const std::string& channel, const OMLCellArray* args);

    void DeleteModelCore(OMLInterface* eval, const std::string& app_id,
        const std::string& model_label);

    void StartRdataApplicationCore(OMLInterface* eval, std::string& app_id,
        const std::string& res, const std::string& model,
        std::string& filename, std::string& res_label,
        std::string& model_label,
        std::string app = "aero",
        std::string model_type = "model",
        std::string console = "false");

    OMLCellArray* GetLoadcaseListCore(OMLInterface* eval, const std::string& app_id,
        const std::string& res_label,
        OMLCurrencyList* outputs);

    OMLCellArray* GetDatasetListCore(OMLInterface* eval, const std::string& app_id,
        const std::string& res_label,
        uint32_t subcase_id, OMLCurrencyList* outputs);

    OMLCellArray* GetStepListCore(const std::string& app_id,
        const std::string& res_label, uint32_t subcase_id,
        OMLCurrencyList* outputs);

    std::string CreateDerivedValueCore(OMLInterface* eval,
        const std::string& app_id,
        const std::string& dv_label,
        const OMLCellArray* args);

    OMLCellArray* GetNodesCore(OMLInterface* eval, const std::string& app_id,
        const std::string& model_label,
        OMLCurrencyList* outputs, bool allinfo);

    OMLCellArray* GetElementsCore(OMLInterface* eval, const std::string& app_id,
        const std::string& model_label,
        OMLCurrencyList* outputs, bool allinfo);

    rData::Enums::EntityType GetEntityType(OMLInterface* eval,
        const std::string& app_id,
        const std::string& model_label,
        const std::string& value_str,
        const std::string& subcase_str,
        const std::string& step_str);

    bool GetValuesCore(OMLInterface* eval, const std::string& app_id,
        const std::string& model_label,
        const std::string& value_str, const std::string& subcase_str,
        const std::string& step_str,
        const std::vector<unsigned int>& reqids, double* outdata,
        int start_index);
    /*
    bool GetVersion(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool StartRdataApplication(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool CreateApplication(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool DeleteApplication(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool AddResource(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool RemoveResource(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool CreateModel(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool DeleteModel(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetResourceList(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetLoadcaseList(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetDatasetList(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetStepList(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetHierarchy(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool CreateDerivedValue(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool RemoveDerivedValue(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetNumParts(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetNumElements(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetNumNodes(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetParts(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetElements(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetNodes(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetValues(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);

    */
    std::string Normpath(const std::string& path);

    //!
    //! Throws error for functions if not running in business edition mode
    //! \param Evaluator
    //! \param Function name
    //! 
    void OmlRDataCheckFunc(OMLInterface*, const std::string&);

}
#endif