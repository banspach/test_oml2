/**
* @file OmlRdataWrapper.cxx
* @date October 2021
* Copyright (C) 2021-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include "OmlRdataWrapper.h"
#include "OmlRdataError.h"
#include <random>
#include <unordered_set>

class OMLError;

namespace omlrdata
{
    std::vector<std::string> format_vector = { "X","Y","Z","MAG" };
    std::vector<std::string> format_tensor = { "vonMises","P1 (major)","P2 (mid)","P3(minor)",\
                                            "MaxShear","Intensity","XX","YY","ZZ","XY","YZ","ZX",\
                                            "Pressure","SignedVonMises","Tresca","Triaxiality",\
                                            "Load Param xi","Load Param theta",\
                                            "Absolute Max Principal",\
                                            "In-plane P1 (major)",\
                                            "In-plane P3 (minor)" };
    std::vector<std::string> format_scalar = { "Value" };

    void GetSubcaseInfo(OMLInterface* eval, std::string& filename, OMLCellArray*& subidlist, OMLCellArray*& sublist, OMLCurrencyList* outputs)
    {
        std::string app_id, res_label("res_1"), model_label("model_1");
        std::string fileext = ValidateFileAndGetExt(eval, filename);
        try
        {
            StartRdataApplicationCore(eval, app_id, res_label, model_label, filename, res_label, model_label);
        }
        catch (...)
        {
            if (".h3d" == fileext)
                eval->ThrowError(ERR_MSG_UNSUPPORTED_MULTIBODY);
            else
                throw;
        }
        TreatSubcase(eval, app_id, res_label, subidlist, sublist, outputs);
    }

    bool GetSubcaseList3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getsubcaselist3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin != 1)
            eval->ThrowError("Usage: getsubcaselist3d [file]");

        StringCheck(eval, inputs->Get(0), 1);
        std::string filename = Normpath(inputs->Get(0)->GetString());        
        OMLCellArray* subidlist = nullptr;
        OMLCellArray* sublist = nullptr;
        GetSubcaseInfo(eval, filename, subidlist, sublist, outputs);

        outputs->AddCellArray(SwapAndGetNonTempCellArray(sublist, outputs));

        return true;
    }

    bool GetSubcaseName3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getsubcasename3d"); // License check
        size_t nargin = inputs->Size();
        if (nargin != 2)
            eval->ThrowError("Usage: getsubcaseindex3d [file] [subcaseindex]");

        StringCheck(eval, inputs->Get(0), 1);
        ScalarCheck(eval, inputs->Get(1), 2);
        
        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* subidlist = nullptr;
        OMLCellArray* sublist = nullptr;
        GetSubcaseInfo(eval, filename, subidlist, sublist, outputs);

        int inputsubcaseindex = static_cast<int>(inputs->Get(1)->GetScalar());
        int rows = subidlist->GetRows();

        if (inputsubcaseindex < 1 || inputsubcaseindex > rows)
        {
            eval->ThrowError(ERR_MSG_SUBCASEINDEX);
        }

        OMLCurrency* subcaseval = sublist->GetValue(inputsubcaseindex - 1);
        std::string subcasename = subcaseval->GetString();
        outputs->AddString(subcasename.c_str());
        return true;
    }

    bool GetSubcaseIndex3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getsubcaseindex3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin != 2)
            eval->ThrowError("Usage: getsubcaseindex3d [file] [subcasename]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);

        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* subidlist = nullptr;
        OMLCellArray* sublist = nullptr;
        GetSubcaseInfo(eval, filename, subidlist, sublist, outputs);
        std::string inputsubcasename = inputs->Get(1)->GetString();
        int rows = subidlist->GetRows();

        bool found = false;
        for (int subcase_index = 0; subcase_index < rows; subcase_index++)
        {
            OMLCurrency* subcaseval = sublist->GetValue(subcase_index);
            std::string subcasename = subcaseval->GetString();
            if (subcasename == inputsubcasename)
            {
                outputs->AddScalar(subcase_index + 1);
                found = true;
                break;
            }
        }

        if (!found)
        {
            eval->ThrowError(ERR_MSG_INPUT_SUBCASE);
        }

        return true;
    }

    bool GetNumSubcases3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getnumsubcases3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin != 1)
            eval->ThrowError("Usage: getnumsubcases3d [file]");

        StringCheck(eval, inputs->Get(0), 1);
        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* subidlist = nullptr;
        OMLCellArray* sublist = nullptr;
        GetSubcaseInfo(eval, filename, subidlist, sublist, outputs);

        outputs->AddScalar(sublist ? sublist->GetRows() : 0);

        return true;
    }

    OMLCurrency* GetSubcase(OMLInterface* eval, std::string& filename,
                            const OMLCurrency* inputsubcase, OMLCurrencyList* outputs,
                            std::string& app_id, std::string& res_label, std::string& model_label)
    {
        std::string fileext = ValidateFileAndGetExt(eval, filename);
        try
        {
            StartRdataApplicationCore(eval, app_id, res_label, model_label, filename, res_label, model_label);
        }
        catch (...)
        {
            if (".h3d" == fileext)
                eval->ThrowError(ERR_MSG_UNSUPPORTED_MULTIBODY);
            else
                throw;
        }
        OMLCellArray* subidlist = nullptr;
        OMLCellArray* sublist = nullptr;

        TreatSubcase(eval, app_id, res_label, subidlist, sublist, outputs);

        int rows = subidlist->GetRows();

        if ((!inputsubcase) && (rows > 1))
        {
            eval->ThrowError(ERR_MSG_SUBCASE_MUST);
        }

        OMLCurrency* subcase = nullptr;
        if (!inputsubcase)
        {
            subcase = subidlist->GetValue(0);
        }
        else
        {
            if (inputsubcase->IsScalar())
            {
                if (inputsubcase->GetScalar() < 1 || inputsubcase->GetScalar() > rows)
                {
                    eval->ThrowError(ERR_MSG_INVALIDSUBCASERANGE);
                }
                subcase = subidlist->GetValue(static_cast<int>(inputsubcase->GetScalar() - 1));
            }
            else
            {
                std::string inputsubcasename = inputsubcase->GetString();
                bool found = false;
                for (int subcase_index = 0; subcase_index < rows; subcase_index++)
                {
                    OMLCurrency* subcaseval = sublist->GetValue(subcase_index);
                    std::string subcasename = subcaseval->GetString();
                    if (subcasename == inputsubcasename)
                    {
                        subcase = subidlist->GetValue(subcase_index);
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    eval->ThrowError(ERR_MSG_INPUT_SUBCASE);
                }
            }
        }
        return subcase;
    }

    void TreatSubcase(OMLInterface* eval, const std::string& appid, const std::string& reslabel,
                      OMLCellArray*& subidlist, OMLCellArray*& sublist,
                      OMLCurrencyList* outputs)
    {
        OMLCellArray* sublist_original = GetLoadcaseListCore(eval, appid, reslabel, outputs);

        bool is_string = (sublist_original->GetValue(0, 0))->IsString();
        std::string value = is_string ? (sublist_original->GetValue(0, 0))->GetString():"";

        int rows = sublist_original->GetRows();
        int cols = sublist_original->GetCols();
        int start_row = 0;

        if (is_string && ("0" == value))
        {
            start_row = 1;
            rows = rows - 1;
        }

        OMLCurrencyList3* outputs3 = reinterpret_cast<OMLCurrencyList3*>(outputs);
        subidlist = outputs3->CreateTemporaryCellArray(rows, 1);
        sublist = outputs3->CreateTemporaryCellArray(rows, cols - 1);

        std::string delimiter("label");

        for (int row_index = 0; row_index < rows; row_index++)
        {
            subidlist->SetValue(row_index, 0, sublist_original->GetValue(row_index + start_row, 0));
            for (int col_index = 1; col_index < cols; col_index++)
            {
                OMLCurrency* val = sublist_original->GetValue(row_index + start_row, col_index);
                if (val->IsString())
                {
                    std::string tempval(val->GetString());
                    size_t index = tempval.find(delimiter);
                    if (std::string::npos != index)
                    {
                        index += 5;
                        tempval = tempval.substr(index + 1);

                        std::string tempdelimiter(",");
                        size_t tempindex = tempval.find(tempdelimiter);
                        if (std::string::npos != tempindex)
                            tempval = tempval.substr(0, tempindex);

                        OMLCurrency* newval = outputs->CreateCurrencyFromString(tempval.c_str());
                        sublist->SetValue(row_index, col_index - 1, newval);
                    }
                }
            }
        }
        if (0 == rows)
        {
            std::string tempval("0");
            subidlist = outputs3->CreateTemporaryCellArray(1, 1);
            sublist = outputs3->CreateTemporaryCellArray(1, 1);
            OMLCurrency* newval = outputs->CreateCurrencyFromString(tempval.c_str());
            sublist->SetValue(0, 0, newval);
            subidlist->SetValue(0, 0, newval);
        }
    }

    bool GetNumTypes3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getnumtypes3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin < 1 || nargin > 2)
            eval->ThrowError("Usage: getnumtypes3d [file] [subcase]");

        StringCheck(eval, inputs->Get(0), 1);
        const OMLCurrency* inputsubcase = nullptr;
        if (2 == nargin)
        {
            ValidateInputScalarOrString(eval, inputs->Get(1), 2);
            inputsubcase = inputs->Get(1);
        }
        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* typesandformat = GetTypelist3dCore(eval, filename, inputsubcase, outputs);
        OMLCurrency* types = typesandformat->GetValue(0, 0);
        const OMLCellArray* cell = types->GetCellArray();
        outputs->AddScalar(cell? cell->GetCols() : 0);
        return true;
    }
    //--------------------------------------------------------------------------
    // Implements [gettypelist3d]
    //--------------------------------------------------------------------------
    bool GetTypelist3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "gettypelist3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin < 1 || nargin > 2)
            eval->ThrowError("Usage: gettypelist3d [file] [subcase]");

        StringCheck(eval, inputs->Get(0), 1);
        const OMLCurrency* inputsubcase = nullptr;
        if (2 == nargin)
        {
            ValidateInputScalarOrString(eval, inputs->Get(1), 2);
            inputsubcase = inputs->Get(1);
        }
        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* typesandformat = GetTypelist3dCore(eval, filename, inputsubcase, outputs);
        OMLCurrency* types = typesandformat->GetValue(0, 0);
        const OMLCellArray* cell = types->GetCellArray();        
        outputs->AddCellArray(GetNonTempCellArray(cell, outputs));
        return true;
    }

    //--------------------------------------------------------------------------
    // Implements [gettypename3d]
    //--------------------------------------------------------------------------
    bool GetTypename3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "gettypename3d"); // License check

        GetTypeInfo(eval, inputs, outputs, false);
        return true;
    }
    //--------------------------------------------------------------------------
    // Implements [gettypeindex3d]
    //--------------------------------------------------------------------------
    bool GetTypeIndex3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "gettypeindex3d"); // License check

        GetTypeInfo(eval, inputs, outputs, true);
        return true;
    }

    std::string GetTypeName(OMLInterface* eval, std::string& filename, const OMLCurrency* inputsubcase,
                            const OMLCurrency* inputtype, OMLCurrencyList* outputs)
    {
        std::string type_name;
        OMLCellArray* typesandformat = GetTypelist3dCore(eval, filename, inputsubcase, outputs);
        OMLCurrency* types = typesandformat->GetValue(0, 0);
        const OMLCellArray* typelist = types->GetCellArray();
        int typescount = typelist->GetCols();
        if (inputtype->IsScalar())
        {
            int typeindex = static_cast<int>(inputtype->GetScalar());
            if (typeindex < 1 || typeindex > typescount)
            {
                eval->ThrowError(ERR_MSG_INVALIDTYPERANGE);
            }

            type_name = (typelist->GetValue(0, typeindex - 1))->GetString();
        }
        else
        {
            bool found = false;
            type_name = inputtype->GetString();
            for (int index = 0; index < typescount; index++)
            {
                if ((typelist->GetValue(0, index))->GetString() == type_name)
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                eval->ThrowError(ERR_MSG_INPUT_TYPE);
            }
        }
        return type_name;
    }

    OMLCellArray* GetTypelist3dCore(OMLInterface* eval, std::string& filename, const OMLCurrency* inputsubcase,
                                    OMLCurrencyList* outputs)
    {
        std::string app_id;
        std::string res_label("res_1");
        std::string model_label("model_1");
        OMLCurrency* subcase = GetSubcase(eval, filename, inputsubcase, outputs, app_id, res_label, model_label);
        uint32_t subcase_id = static_cast<uint32_t>(std::stoi(subcase->GetString()));
        return GetDatasetListCore(eval, app_id, res_label, subcase_id, outputs);
    }

    OMLCurrency* GetTypeFormat(OMLInterface* eval, std::string& filename,
                               const OMLCurrency* inputsubcase, const OMLCurrency* inputtype,
                               OMLCurrencyList* outputs)
    {
        OMLCellArray* typesandformat = GetTypelist3dCore(eval, filename, inputsubcase, outputs);
        OMLCurrency* types = typesandformat->GetValue(0, 0);
        const OMLCellArray* typelist = types->GetCellArray();
        types = typesandformat->GetValue(1, 0);
        const OMLCellArray* formatlist = types->GetCellArray();

        int typescount = typelist->GetCols();

        int typeindex = 0;
        if (inputtype->IsScalar())
        {
            typeindex = static_cast<int>(inputtype->GetScalar());
            if (typeindex < 1 || typeindex > typescount)
            {
                eval->ThrowError(ERR_MSG_INVALIDTYPERANGE);
            }
            typeindex -= 1;
        }
        else
        {
            bool found = false;
            std::string type_name = inputtype->GetString();
            for (int index = 0; index < typescount; index++)
            {
                if ((typelist->GetValue(0, index))->GetString() == type_name)
                {
                    typeindex = index;
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                eval->ThrowError(ERR_MSG_INPUT_TYPE);
            }
        }

        return formatlist->GetValue(0, typeindex);
    }

    void GetTypeInfo(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs, bool isindex)
    {
        size_t nargin = inputs->Size();
        if (nargin < 2 || nargin > 3)
        {
            std::string error = "Usage: ";

            if (isindex)
                error = error + "gettypeindex3d";
            else
                error = error + "gettypename3d";

            error = error + " [file] [subcase] [type]";

            eval->ThrowError(error.c_str());
        }
        

        StringCheck(eval, inputs->Get(0), 1);
        const OMLCurrency* inputsubcase = nullptr;
        const OMLCurrency* inputtype = nullptr;
        if (3 == nargin)
        {
            ValidateInputScalarOrString(eval, inputs->Get(1), 2);
            ValidateInputScalarOrString(eval, inputs->Get(2), 3);
            inputsubcase = inputs->Get(1);
            inputtype = inputs->Get(2);
        }
        else
        {
            ValidateInputScalarOrString(eval, inputs->Get(1), 2);
            inputtype = inputs->Get(1);
        }

        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* typesandformat = GetTypelist3dCore(eval, filename, inputsubcase, outputs);
        OMLCurrency* types = typesandformat->GetValue(0, 0);
        const OMLCellArray* typelist = types->GetCellArray();

        int typescount = typelist->GetCols();
        std::string type_name;
        if (inputtype->IsScalar())
        {
            int typeindex = static_cast<int>(inputtype->GetScalar());
            if (typeindex < 1 || typeindex > typescount)
            {
                eval->ThrowError(ERR_MSG_INVALIDTYPERANGE);
            }
        
            if (isindex)
            {
                outputs->AddScalar(typeindex);
            }
            else
            {
                type_name = (typelist->GetValue(0, typeindex - 1))->GetString();
                outputs->AddString(type_name.c_str());
            }        
        }
        else
        {
            bool found = false;
            type_name = inputtype->GetString();
            for (int index = 0; index < typescount; index++)
            {
                if ((typelist->GetValue(0, index))->GetString() == type_name)
                {
                    if (isindex)
                    {
                        outputs->AddScalar(index+1);
                    }
                    else
                    {
                        outputs->AddString(type_name.c_str());
                    }                
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                eval->ThrowError(ERR_MSG_INPUT_TYPE);
            }
        }
    }

    //--------------------------------------------------------------------------
    // Implements [getelemlist3d]
    //--------------------------------------------------------------------------
    bool GetElemList3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getelemlist3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin != 1)
            eval->ThrowError("Usage: getelemlist3d [file]");

        StringCheck(eval, inputs->Get(0), 1);
        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* cell = GetElementOrNodeList(eval, filename, outputs, true);
        outputs->AddCellArray(GetNonTempCellArray(cell, outputs));
        return true;
    }
    //--------------------------------------------------------------------------
    // Implements [getnumelems3d]
    //--------------------------------------------------------------------------
    bool GetNumElems3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getnumelems3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin != 1)
            eval->ThrowError("Usage: getnumelems3d [file]");

        StringCheck(eval, inputs->Get(0), 1);
        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* cell = GetElementOrNodeList(eval, filename, outputs, true);
        outputs->AddScalar(cell ? cell->GetCols() : 0);
        return true;
    }
    //--------------------------------------------------------------------------
    // Implements [getelemindex3d]
    //--------------------------------------------------------------------------
    bool GetElemIndex3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getelemindex3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin != 2)
            eval->ThrowError("Usage: getelemindex3d [file] [elementname]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);

        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* cell = GetElementOrNodeList(eval, filename, outputs, true);
        

        std::string inputelename = inputs->Get(1)->GetString();
        int num_elements = cell->GetCols();

        bool found = false;
        for (int ele_index = 0; ele_index < num_elements; ele_index++)
        {
            OMLCurrency* eleval = cell->GetValue(ele_index);
            std::string elename = eleval->GetString();
            if (elename == inputelename)
            {
                outputs->AddScalar(ele_index + 1);
                found = true;
                break;
            }
        }

        if (!found)
        {
            eval->ThrowError(ERR_MSG_INVALID_ELEMENT);
        }
        return true;
    }
    
    void GetElemOrNodeName(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs, bool is_element)
    {
        size_t nargin = inputs->Size();
        if (nargin != 2)
        {
            if (is_element)
                eval->ThrowError("Usage: getelemname3d [file] [elementindex]");
            else
                eval->ThrowError("Usage: getnodename3d [file] [nodeindex]");
        }
            

        StringCheck(eval, inputs->Get(0), 1);
        ScalarCheck(eval, inputs->Get(1), 2);

        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* cell = GetElementOrNodeList(eval, filename, outputs, is_element);

        int inputindex = static_cast<int>(inputs->Get(1)->GetScalar());
        int totalitems = cell->GetCols();

        if (inputindex < 1 || inputindex > totalitems)
        {
            if (is_element)
               eval->ThrowError(ERR_MSG_ELEMENTINDEX);
            else
               eval->ThrowError(ERR_MSG_NODEINDEX);
        }

        OMLCurrency* val = cell->GetValue(0, inputindex - 1);
        std::string name = val->GetString();
        outputs->AddString(name.c_str());
    }
    //--------------------------------------------------------------------------
    // Implements [getelemname3d]
    //--------------------------------------------------------------------------
    bool GetElemName3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getelemname3d"); // License check

        GetElemOrNodeName(eval, inputs, outputs, true);
        return true;
    }
    //--------------------------------------------------------------------------
    // Implements [getnodelist3d]
    //--------------------------------------------------------------------------
    bool GetNodeList3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getnodelist3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin != 1)
            eval->ThrowError("Usage: getnodelist3d [file]");

        StringCheck(eval, inputs->Get(0), 1);
        std::string filename = Normpath(inputs->Get(0)->GetString());

        OMLCellArray* cell = GetElementOrNodeList(eval, filename, outputs, false);
        outputs->AddCellArray(GetNonTempCellArray(cell, outputs));
        return true;
    }


    //--------------------------------------------------------------------------
    // Implements [getnumnodes3d]
    //--------------------------------------------------------------------------
    bool GetNumNodes3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getnumnodes3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin != 1)
            eval->ThrowError("Usage: getnumnodes3d [file]");

        StringCheck(eval, inputs->Get(0), 1);
        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* cell = GetElementOrNodeList(eval, filename, outputs, false);
        outputs->AddScalar(cell ? cell->GetCols() : 0);
        return true;
    }
    //--------------------------------------------------------------------------
    // Implements [getnodeindex3d]
    //--------------------------------------------------------------------------
    bool GetNodeIndex3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getnodeindex3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin != 2)
            eval->ThrowError("Usage: getnodeindex3d [file] [nodename]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);

        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCellArray* cell = GetElementOrNodeList(eval, filename, outputs, false);


        std::string inputnodename = inputs->Get(1)->GetString();
        int num_nodes = cell->GetCols();

        bool found = false;
        for (int node_index = 0; node_index < num_nodes; node_index++)
        {
            OMLCurrency* eleval = cell->GetValue(node_index);
            std::string nodename = eleval->GetString();
            if (nodename == inputnodename)
            {
                outputs->AddScalar(node_index + 1);
                found = true;
                break;
            }
        }

        if (!found)
        {
            eval->ThrowError(ERR_MSG_INVALID_NODE);
        }
        return true;
    }
    //--------------------------------------------------------------------------
    // Implements [getnodename3d]
    //--------------------------------------------------------------------------
    bool GetNodeName3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getnodename3d"); // License check

        GetElemOrNodeName(eval, inputs, outputs, false);
        return true;
    }
    std::vector<unsigned int> GetRequestIds(OMLInterface* eval, std::string& filename,
                                            const OMLCurrency* inputreq, OMLCurrencyList* outputs,
                                            bool is_element)
    {
        std::vector<unsigned int> reqidlist;
        OMLCellArray* allreqs = GetElementOrNodeList(eval, filename, outputs, is_element);
        int totalreqs = allreqs->GetCols();
        bool found = false;
        if (inputreq->IsScalar())
        {
            int req_index = static_cast<int>(inputreq->GetScalar());
            if (totalreqs < req_index || req_index < 1)
            {
                eval->ThrowError(ERR_MSG_INVALIDREQRANGE);
            }
            else
            {
                reqidlist.push_back(GetIdFromStr(eval, allreqs->GetValue(0, req_index - 1)->GetString(), is_element));
            }
        }
        else if (inputreq->IsString())
        {
            std::string reqstr = inputreq->GetString();
            if (reqstr.size() < 2)
            {
                eval->ThrowError(ERR_MSG_INPUT_REQUEST);
            }
            else
            {
                std::string indicator(1, reqstr[0]);
                if ((ELIMENT_REPR != indicator) && (NODE_REPR != indicator))
                {
                    eval->ThrowError(ERR_MSG_INPUT_REQUEST);
                }

                for (int inindex = 0; inindex < totalreqs; inindex++)
                {
                    if (reqstr == allreqs->GetValue(0, inindex)->GetString())
                    {
                        found = true;
                        reqidlist.push_back(GetIdFromStr(eval, reqstr, is_element));
                        break;
                    }
                }

                if (!found)
                {
                    eval->ThrowError(ERR_MSG_INPUT_REQUEST);
                }
            }
        }
        else if (inputreq->IsCellArray())
        {
            const OMLCellArray* inputreqcell = inputreq->GetCellArray();
            int inputreqcount = inputreqcell->GetCols();

            if (inputreqcount)
            {
                std::unordered_set<std::string> allreqs_set;
                reqidlist.reserve(inputreqcount);
                for (int inindex = 0; inindex < inputreqcount; inindex++)
                {
                    OMLCurrency* tempincomp = inputreqcell->GetValue(0, inindex);

                    if (tempincomp->IsScalar())
                    {
                        int req_index = static_cast<int>(tempincomp->GetScalar());
                        if (totalreqs < req_index || req_index < 1)
                        {
                            eval->ThrowError(ERR_MSG_INVALIDREQRANGE);
                        }
                        else
                        {
                            reqidlist.push_back(GetIdFromStr(eval, allreqs->GetValue(0, req_index - 1)->GetString(), is_element));
                        }
                    }
                    else if (tempincomp->IsString())
                    {
                        found = false;
                        std::string reqstr = tempincomp->GetString();

                        std::string indicator(1, reqstr[0]);
                        if ((ELIMENT_REPR != indicator) && (NODE_REPR != indicator))
                        {
                            eval->ThrowError(ERR_MSG_INPUT_REQUEST);
                        }

                        if (allreqs_set.empty())
                        {
                            allreqs_set.reserve(totalreqs);
                            for (int inindex = 0; inindex < totalreqs; inindex++)
                            {
                                allreqs_set.insert(allreqs->GetValue(0, inindex)->GetString());
                            }
                        }

                        if (allreqs_set.end() !=  allreqs_set.find(reqstr))
                        {
                                found = true;
                                reqidlist.push_back(GetIdFromStr(eval, reqstr, is_element));
                        }

                        if (!found)
                        {
                            eval->ThrowError(ERR_MSG_INPUT_REQUEST);
                        }
                    }
                    else
                    {
                        eval->ThrowError(ERR_MSG_INPUT_REQUEST);
                    }
                }
            }
            else
            {
                reqidlist.reserve(totalreqs);
                for (int inindex = 0; inindex < totalreqs; inindex++)
                {
                    reqidlist.push_back(GetIdFromStr(eval, allreqs->GetValue(0, inindex)->GetString(), is_element));
                }
            }
        }
        else if (inputreq->IsMatrix())
        {
            const OMLMatrix* inputreqmat = inputreq->GetMatrix();
            if (!inputreqmat->IsReal())
            {
                eval->ThrowError(ERR_MSG_INPUT_REQUEST);
            }

            int rows = inputreqmat->GetRows();
            int cols = inputreqmat->GetCols();
            if ((0 == rows) && (0 == cols))
            {
                reqidlist.reserve(totalreqs);
                for (int inindex = 0; inindex < totalreqs; inindex++)
                {
                    reqidlist.push_back(GetIdFromStr(eval, allreqs->GetValue(0, inindex)->GetString(), is_element));
                }
            }
            else if ((1 == rows) && (cols > 0))
            {
                reqidlist.reserve(cols);
                const double* data = inputreqmat->GetRealData();
                for (int index = 0; index < cols; index++)
                {
                    int req_index = static_cast<int>(data[index]);
                    if (totalreqs < req_index || req_index < 1)
                    {
                        eval->ThrowError(ERR_MSG_INVALIDREQRANGE);
                    }
                    else
                    {
                        reqidlist.push_back(GetIdFromStr(eval, allreqs->GetValue(0, req_index - 1)->GetString(), is_element));
                    }
                }
            }
            else
            {
                eval->ThrowError(ERR_MSG_INPUT_REQUEST);
            }
        }
        else
        {
            eval->ThrowError(ERR_MSG_INPUT_REQUEST);
        }

        return reqidlist;
    }

    OMLCellArray* GetElementOrNodeList(OMLInterface* eval, std::string& filename, OMLCurrencyList* outputs, bool elements)
    {
        std::string app_id;
        std::string res_label("res_1");
        std::string model_label("model_1");
        std::string fileext = ValidateFileAndGetExt(eval, filename);
        try
        {
            StartRdataApplicationCore(eval, app_id, res_label, model_label, filename, res_label, model_label);
        }
        catch (...)
        {
            if (".h3d" == fileext)
                eval->ThrowError(ERR_MSG_UNSUPPORTED_MULTIBODY);
            else
                throw;
        }

        if (elements)
            return GetElementsCore(eval, app_id, model_label, outputs, false);
        else
            return GetNodesCore(eval, app_id, model_label, outputs, false);
    }

    bool GetNumComps3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getnumcomps3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin < 2 || nargin > 3)
            eval->ThrowError("Usage: getnumcomps3d [file] [subcase] [type]");

        StringCheck(eval, inputs->Get(0), 1);
        const OMLCurrency* inputsubcase = nullptr;
        const OMLCurrency* inputtype = nullptr;
        if (3 == nargin)
        {
            ValidateInputScalarOrString(eval, inputs->Get(1), 2);
            ValidateInputScalarOrString(eval, inputs->Get(2), 3);
            inputsubcase = inputs->Get(1);
            inputtype = inputs->Get(2);
        }
        else
        {
            ValidateInputScalarOrString(eval, inputs->Get(1), 2);
            inputtype = inputs->Get(1);
        }

        std::string filename = Normpath(inputs->Get(0)->GetString());

        std::string compformat("");
        OMLCellArray* comps = nullptr;

        GetComplist3dCore(eval, filename, inputsubcase,
            inputtype, outputs, comps, compformat);

        outputs->AddScalar(comps ? comps->GetCols() : 0);
        return true;
    }

    //--------------------------------------------------------------------------
    // Implements [getcomplist3d]
    //--------------------------------------------------------------------------
    bool GetComplist3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getcomplist3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin < 2 || nargin > 3)
            eval->ThrowError("Usage: getcomplist3d [file] [subcase] [type]");

        StringCheck(eval, inputs->Get(0), 1);
        const OMLCurrency* inputsubcase = nullptr;
        const OMLCurrency* inputtype = nullptr;
        if (3 == nargin)
        {
            ValidateInputScalarOrString(eval, inputs->Get(1), 2);
            ValidateInputScalarOrString(eval, inputs->Get(2), 3);
            inputsubcase = inputs->Get(1);
            inputtype = inputs->Get(2);
        }
        else
        {
            ValidateInputScalarOrString(eval, inputs->Get(1), 2);
            inputtype = inputs->Get(2);
        }

        std::string filename = Normpath(inputs->Get(0)->GetString());

        std::string compformat("");
        OMLCellArray* comps = nullptr;

        GetComplist3dCore(eval, filename, inputsubcase,
            inputtype, outputs, comps, compformat);

        if (comps)
        {
            outputs->AddCellArray(GetNonTempCellArray(comps, outputs));
            outputs->AddString(compformat.c_str());
        }

        return true;
    }
    //--------------------------------------------------------------------------
    // Implements [getcompname3d]
    //--------------------------------------------------------------------------
    bool GetCompname3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getcompname3d"); // License check

        GetCompInfo(eval, inputs, outputs, false);
        return true;
    }
    //--------------------------------------------------------------------------
    // Implements [getcompindex3d]
    //--------------------------------------------------------------------------
    bool GetCompindex3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "getcompindex3d"); // License check

        GetCompInfo(eval, inputs, outputs, true);
        return true;
    }

    void GetComplist3dCore(OMLInterface* eval, std::string& filename, const OMLCurrency* inputsubcase, 
                            const OMLCurrency* inputtype, OMLCurrencyList* outputs, OMLCellArray*& comps,
                            std::string& compformat)
    {
        OMLCurrency* typeformat = GetTypeFormat(eval, filename, inputsubcase, inputtype, outputs);
        OMLCurrencyList3* outputs3 = reinterpret_cast<OMLCurrencyList3*>(outputs);
        bool validcomp = false;
        if (typeformat->IsString())
        {
            std::string typeformatstr = typeformat->GetString();
            int index = 0;
            std::vector<std::string>* compptr = nullptr;
            if (std::string::npos != typeformatstr.find("FORMAT_VECTOR"))
            {
                compptr = &omlrdata::format_vector;
                validcomp = true;
                compformat = "v";
            }
            else if (std::string::npos != typeformatstr.find("FORMAT_TENSOR"))
            {
                compptr = &omlrdata::format_tensor;
                validcomp = true;
                compformat = "t";
            }
            else if (std::string::npos != typeformatstr.find("FORMAT_SCALAR"))
            {
                compptr = &omlrdata::format_scalar;
                validcomp = true;
                compformat = "s";
            }
            else if (std::string::npos != typeformatstr.find("FORMAT_ENTITY"))
            {
                //DoNothing
            }

            if (compptr)
            {
                comps = outputs3->CreateTemporaryCellArray(1, static_cast<int>(compptr->size()));
                for (std::string comp : (*compptr))
                {
                    comps->SetValue(0, index, outputs->CreateCurrencyFromString(comp.c_str()));
                    index++;
                }
            }
        }

        if (!validcomp)
        {
            comps = outputs3->CreateTemporaryCellArray(1, 1);
            comps->SetValue(0, outputs->CreateCurrencyFromString(""));
        }
    }

    void GetCompInfo(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs, bool isindex)
    {
        size_t nargin = inputs->Size();
        if (nargin < 2 || nargin > 4)
        {
            std::string error = "Usage: ";

            if (isindex)
                error = error + "getcompindex3d";
            else
                error = error + "getcompname3d";

            error = error + " [file] [subcase] [type] [comp]";

            eval->ThrowError(error.c_str());
        }


        StringCheck(eval, inputs->Get(0), 1);
        const OMLCurrency* inputsubcase = nullptr;
        const OMLCurrency* inputtype = nullptr;
        const OMLCurrency* inputcomp = nullptr;
        if (4 == nargin)
        {
            ValidateInputScalarOrString(eval, inputs->Get(1), 2);
            ValidateInputScalarOrString(eval, inputs->Get(2), 3);
            ValidateInputScalarOrString(eval, inputs->Get(3), 4);
            inputsubcase = inputs->Get(1);
            inputtype = inputs->Get(2);
            inputcomp = inputs->Get(3);
        }
        else
        {
            ValidateInputScalarOrString(eval, inputs->Get(1), 2);
            ValidateInputScalarOrString(eval, inputs->Get(2), 3);
            inputtype = inputs->Get(1);
            inputcomp = inputs->Get(2);
        }

        std::string filename = Normpath(inputs->Get(0)->GetString());
        OMLCurrency* typeformat = GetTypeFormat(eval, filename, inputsubcase, inputtype, outputs);

        bool validcomp = false;
        if (typeformat->IsString())
        {
            std::string typeformatstr = typeformat->GetString();
            int index = 0;
            std::vector<std::string>* compptr = nullptr;
            if (std::string::npos != typeformatstr.find("FORMAT_VECTOR"))
            {
                compptr = &omlrdata::format_vector;
            }
            else if (std::string::npos != typeformatstr.find("FORMAT_TENSOR"))
            {
                compptr = &omlrdata::format_tensor;
            }
            else if (std::string::npos != typeformatstr.find("FORMAT_SCALAR"))
            {
                compptr = &omlrdata::format_scalar;
            }
            else if (std::string::npos != typeformatstr.find("FORMAT_ENTITY"))
            {
                //DoNothing
            }

            if (compptr)
            {
                if (inputcomp->IsScalar())
                {
                    int compindex = static_cast<int>(inputcomp->GetScalar());
                    if (compindex < 1 || compindex > compptr->size())
                    {
                        eval->ThrowError(ERR_MSG_INVALIDCOMPONENTRANGE);
                    }

                    if (isindex)
                    {
                        outputs->AddScalar(compindex);
                    }
                    else
                    {
                        outputs->AddString(((*compptr)[compindex - 1]).c_str());
                    }

                    validcomp = true;
                }
                else
                {
                    std::string compname = inputcomp->GetString();
                    int compindex = 1;
                    for (std::string comp : (*compptr))
                    {
                        if (compname == comp)
                        {
                            if (isindex)
                            {
                                outputs->AddScalar(compindex);
                            }
                            else
                            {
                                outputs->AddString(compname.c_str());
                            }

                            validcomp = true;
                            break;
                        }
                        ++compindex;
                    }
                }
            }
        }

        if (!validcomp)
        {
            eval->ThrowError(ERR_MSG_INPUT_COMPONENT);
        }
    }

    std::vector<std::string> GetCompList(OMLInterface* eval, std::string& filename,
        const OMLCurrency* inputsubcase, const OMLCurrency* inputtype,
        const OMLCurrency* inputcomp, OMLCurrencyList* outputs,
        std::string& compformat)
    {
        OMLCellArray* comps = nullptr;
        GetComplist3dCore(eval, filename, inputsubcase, inputtype, outputs, comps, compformat);
        int totalcomps = comps->GetCols();

        std::vector<std::string> complist;
        if (inputcomp->IsScalar())
        {
            if (inputcomp->GetScalar() < 1 || inputcomp->GetScalar() > totalcomps)
            {
                eval->ThrowError(ERR_MSG_INPUT_COMPONENT);
            }
            complist.push_back(comps->GetValue(0, static_cast<int>(inputcomp->GetScalar() - 1))->GetString());
        }
        else if (inputcomp->IsString())
        {
            bool found = false;
            for (int index = 0; index < totalcomps; index++)
            {
                OMLCurrency* tempcomp = comps->GetValue(0, index);
                if (!strcmp(tempcomp->GetString(), inputcomp->GetString()))
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                eval->ThrowError(ERR_MSG_INPUT_COMPONENT);
            }

            complist.push_back(inputcomp->GetString());
        }
        else if (inputcomp->IsCellArray())
        {
            const OMLCellArray* inputcompcell = inputcomp->GetCellArray();
            int inputcompcount = inputcompcell->GetCols();
            for (int inindex = 0; inindex < inputcompcount; inindex++)
            {
                OMLCurrency* tempincomp = inputcompcell->GetValue(0, inindex);
                if (tempincomp->IsScalar())
                {
                    if (tempincomp->GetScalar() < 1 || tempincomp->GetScalar() > totalcomps)
                    {
                        eval->ThrowError(ERR_MSG_INVALIDCOMPONENTRANGE);
                    }
                    complist.push_back(comps->GetValue(0, static_cast<int>(tempincomp->GetScalar() - 1))->GetString());
                }
                else if (tempincomp->IsString())
                {
                    bool found = false;
                    for (int index = 0; index < totalcomps; index++)
                    {
                        OMLCurrency* tempcomp = comps->GetValue(0, index);
                        if (!strcmp(tempincomp->GetString(), tempcomp->GetString()))
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        eval->ThrowError(ERR_MSG_INPUT_COMPONENT);
                    }
                    complist.push_back(tempincomp->GetString());
                }
                else
                {
                    eval->ThrowError(ERR_MSG_INPUT_COMPONENT);
                }
            }
        }
        else if (inputcomp->IsMatrix())
        {
            const OMLMatrix* inputcompmat = inputcomp->GetMatrix();
            if ((0 == inputcompmat->GetRows()) && (0 == inputcompmat->GetCols()))
            {
                for (int inindex = 0; inindex < totalcomps; inindex++)
                {
                    complist.push_back(comps->GetValue(0, inindex)->GetString());
                }
            }
            else
            {
                if (!inputcompmat->IsReal())
                {
                    eval->ThrowError(ERR_MSG_INVALIDCOMPONENTRANGE);
                }
                const double* data = inputcompmat->GetRealData();

                int inputcompcount = inputcompmat->GetCols();
                for (int inindex = 0; inindex < inputcompcount; inindex++)
                {
                    int tempcompindex = static_cast<int>(data[inindex]);
                    if (tempcompindex < 1 || tempcompindex > totalcomps)
                    {
                        eval->ThrowError(ERR_MSG_INVALIDCOMPONENTRANGE);
                    }
                    complist.push_back(comps->GetValue(0, tempcompindex - 1)->GetString());
                }
            }
        }
        else
        {
            eval->ThrowError(ERR_MSG_INPUT_COMPONENT);
        }

        return complist;
    }
    //--------------------------------------------------------------------------
    // Implements [gettimesteplist3d]
    //--------------------------------------------------------------------------
    bool GetTimeSteplist3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "gettimesteplist3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin < 1 || nargin > 2)
            eval->ThrowError("Usage: gettimesteplist3d [file] [subcase]");

        int inputargindex = 0;
        StringCheck(eval, inputs->Get(inputargindex), inputargindex + 1);
        std::string filename = Normpath(inputs->Get(inputargindex++)->GetString());
        const OMLCurrency* inputsubcase = nullptr;
        if (2 == nargin)
        {
            ValidateInputScalarOrString(eval, inputs->Get(inputargindex), inputargindex + 1);
            inputsubcase = inputs->Get(inputargindex++);
        }

        std::string app_id;
        std::string res_label("res_1");
        std::string model_label("model_1");
        OMLCurrency* subcase = GetSubcase(eval, filename, inputsubcase, outputs, app_id, res_label, model_label);;

        //GetTimeStep
        uint32_t subcase_id = static_cast<uint32_t>(std::stoi(subcase->GetString()));
        OMLCellArray* originalsteplistout = GetStepListCore(app_id, res_label, subcase_id, outputs);
        const OMLCellArray* originalsteplist = originalsteplistout->GetValue(0, 0)->GetCellArray();
        OMLCellArray* timelist = TreatTimeSteps(originalsteplist, outputs);
        outputs->AddCellArray(GetNonTempCellArray(timelist, outputs));
        return true;
    }

    OMLCellArray* TreatTimeSteps(const OMLCellArray* originalsteplist, OMLCurrencyList* outputs)
    {
        int totaltimesteps = originalsteplist->GetRows();
        OMLCurrencyList3* outputs3 = reinterpret_cast<OMLCurrencyList3*>(outputs);
        OMLCellArray* timesteplist = outputs3->CreateTemporaryCellArray(1, totaltimesteps);
        std::string delimiter1("sync=");
        std::string delimiter2(",");

        for (int row_index = 0; row_index < totaltimesteps; row_index++)
        {
            OMLCurrency* val = originalsteplist->GetValue(row_index, 0);
            if (val->IsString())
            {
                std::string tempval(val->GetString());
                size_t index1 = tempval.find(delimiter1);
                if (std::string::npos != index1)
                {
                    index1 += delimiter1.length();
                    tempval = tempval.substr(index1);
                    size_t index2 = tempval.find_first_of(delimiter2);
                    if (std::string::npos != index2)
                    {
                        tempval = tempval.substr(0, index2);                        
                        OMLCurrency* newval = outputs->CreateCurrencyFromString(tempval.c_str());
                        timesteplist->SetValue(0, row_index, newval);
                    }
                }
            }
        }
        return timesteplist;
    }

    std::vector<std::string> GetTimeList(OMLInterface* eval, const OMLCurrency* subcase,
                                        const OMLCurrency* inputtime, const std::string& app_id,
                                        const std::string& res_label, OMLCurrencyList* outputs)
    {
        //GetTimeStep
        uint32_t subcase_id = static_cast<uint32_t>(std::stoi(subcase->GetString()));
        OMLCellArray* originalsteplistout = GetStepListCore(app_id, res_label, subcase_id, outputs);
        const OMLCellArray* originalsteplist = originalsteplistout->GetValue(0, 0)->GetCellArray();
        const OMLCellArray* originalsteplistids = originalsteplistout->GetValue(1, 0)->GetCellArray();
        int totaltimesteps = originalsteplist->GetRows();

        OMLCellArray* timesteplist = TreatTimeSteps(originalsteplist, outputs);
        //Validate Input time step
       // Check validity of given component
        std::vector<std::string> timelist;
        if (inputtime->IsScalar())
        {
            if (inputtime->GetScalar() < 1 || inputtime->GetScalar() > totaltimesteps)
            {
                eval->ThrowError(ERR_MSG_INVALIDTIMERANGE);
            }
            timelist.push_back(originalsteplistids->GetValue(static_cast<int>(inputtime->GetScalar() - 1))->GetString());
        }
        else if (inputtime->IsString())
        {
            int idindex = -1;
            for (int index = 0; index < totaltimesteps; index++)
            {
                OMLCurrency* temptime = timesteplist->GetValue(0, index);

                OMLInterface4* eval4 = dynamic_cast<OMLInterface4*>(eval);
                std::string func("str2double");
                OMLCurrencyList* time1_list = eval4->CreateCurrencyList();
                time1_list->AddString(inputtime->GetString());
                const OMLCurrency* out1 = eval4->CallFunction(func.c_str(), time1_list);
                OMLCurrencyList* time2_list = eval4->CreateCurrencyList();
                time2_list->AddString(temptime->GetString());
                const OMLCurrency* out2 = eval4->CallFunction(func.c_str(), time2_list);
                double timeone = out1->GetScalar();
                double timetwo = out2->GetScalar();

                if (TimeCmp(timeone, timetwo))
                {
                    idindex = index;
                    break;
                }
            }

            if (-1 == idindex)
            {
                eval->ThrowError(ERR_MSG_INPUT_TIME);
            }

            timelist.push_back(originalsteplistids->GetValue(idindex)->GetString());
        }
        else if (inputtime->IsCellArray())
        {
            const OMLCellArray* inputtimecell = inputtime->GetCellArray();
            int inputtimecount = inputtimecell->GetCols();
            OMLInterface4* eval4 = dynamic_cast<OMLInterface4*>(eval);

            for (int inindex = 0; inindex < inputtimecount; inindex++)
            {
                OMLCurrency* tempintime = inputtimecell->GetValue(0, inindex);
                if (tempintime->IsScalar())
                {
                    if (tempintime->GetScalar() < 1 || tempintime->GetScalar() > totaltimesteps)
                    {
                        eval->ThrowError(ERR_MSG_INVALIDTIMERANGE);
                    }
                    timelist.push_back(originalsteplistids->GetValue(static_cast<int>(tempintime->GetScalar() - 1))->GetString());
                }
                else if (tempintime->IsString())
                {
                    std::string func("str2double");
                    OMLCurrencyList* time1_list = eval4->CreateCurrencyList();
                    time1_list->AddString(tempintime->GetString());
                    const OMLCurrency* out1 = eval4->CallFunction(func.c_str(), time1_list);
                    double timeone = out1->GetScalar();

                    int idindex = -1;
                    for (int index = 0; index < totaltimesteps; index++)
                    {
                        OMLCurrency* temptime = timesteplist->GetValue(0, index);
                        OMLCurrencyList* time2_list = eval4->CreateCurrencyList();
                        time2_list->AddString(temptime->GetString());
                        const OMLCurrency* out2 = eval4->CallFunction(func.c_str(), time2_list);
                        double timetwo = out2->GetScalar();

                        if (TimeCmp(timeone, timetwo))
                        {
                            idindex = index;
                            break;
                        }
                    }

                    if (-1 == idindex)
                    {
                        eval->ThrowError(ERR_MSG_INPUT_TIME);
                    }
                    timelist.push_back(originalsteplistids->GetValue(idindex)->GetString());
                }
                else
                {
                    eval->ThrowError(ERR_MSG_INPUT_TIME);
                }
            }
        }
        else if (inputtime->IsMatrix())
        {
            const OMLMatrix* inputtimemat = inputtime->GetMatrix();

            if ((0 == inputtimemat->GetRows()) && (0 == inputtimemat->GetCols()))
            {
                for (int temptimeindex = 0; temptimeindex < totaltimesteps; temptimeindex++)
                {
                    timelist.push_back(originalsteplistids->GetValue(temptimeindex)->GetString());
                }
            }
            else
            {
                if (!inputtimemat->IsReal())
                {
                    eval->ThrowError(ERR_MSG_INVALIDTIMERANGE);
                }
                const double* data = inputtimemat->GetRealData();

                int inputtimecount = inputtimemat->GetCols();
                for (int inindex = 0; inindex < inputtimecount; inindex++)
                {
                    int temptimeindex = static_cast<int>(data[inindex]);
                    if (temptimeindex < 1 || temptimeindex > totaltimesteps)
                    {
                        eval->ThrowError(ERR_MSG_INVALIDTIMERANGE);
                    }
                    timelist.push_back(originalsteplistids->GetValue(temptimeindex)->GetString());
                }
            }
        }
        else
        {
            eval->ThrowError(ERR_MSG_INPUT_TIME);
        }

        return timelist;
    }

    //--------------------------------------------------------------------------
    // Implements [readcae3d]
    //--------------------------------------------------------------------------
    bool ReadCae3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "readcae3d"); // License check

        size_t nargin = inputs->Size();
        if (nargin < 9 || nargin > 10)
            eval->ThrowError("Usage: readcae3d [file] [subcase] [type] [request] [component] [timestep] \
                                [cornerdata] [coordinatesystem] [averaging] [layer]");

        int inputargindex = 0;
        StringCheck(eval, inputs->Get(inputargindex), inputargindex + 1);
        std::string filename = Normpath(inputs->Get(inputargindex++)->GetString());
        const OMLCurrency* inputsubcase = nullptr;
        if (10 == nargin)
        {
            ValidateInputScalarOrString(eval, inputs->Get(inputargindex), inputargindex + 1);
            inputsubcase = inputs->Get(inputargindex++);
        }

        ValidateInputScalarOrString(eval, inputs->Get(inputargindex), inputargindex + 1);
        const OMLCurrency* inputtype = inputs->Get(inputargindex++);
        ValidateInputTypeStringScalarMatCell(eval, inputs->Get(inputargindex), inputargindex + 1);
        const OMLCurrency* inputreq = inputs->Get(inputargindex++);
        ValidateInputTypeStringScalarMatCell(eval, inputs->Get(inputargindex), inputargindex + 1);
        const OMLCurrency* inputcomp = inputs->Get(inputargindex++);
        ValidateInputTypeStringScalarMatCell(eval, inputs->Get(inputargindex), inputargindex + 1);
        const OMLCurrency* inputtime = inputs->Get(inputargindex++);
    
        std::string app_id;
        std::string res_label("res_1");
        std::string model_label("model_1");
        OMLCurrency* subcase = GetSubcase(eval, filename, inputsubcase, outputs, app_id, res_label, model_label);;

        std::string type_name = GetTypeName(eval, filename, inputsubcase, inputtype, outputs);

        std::string compformat("");
        std::vector<std::string> complist = GetCompList(eval, filename, inputsubcase, inputtype, inputcomp, outputs, compformat);
        int totalcomps = static_cast<int>(complist.size());

        std::vector<std::string> timelist = GetTimeList(eval, subcase, inputtime, app_id, res_label, outputs);

        // Beginning of the construction of rData cell
        //create max possible rows, later assign to correct size cell array
        int rdatacellindex = 0;
        int dv_opt_originalcount = 7;
        OMLCurrencyList3* outputs3 = reinterpret_cast<OMLCurrencyList3*>(outputs);
        OMLCellArray* dv_opt = outputs3->CreateTemporaryCellArray(dv_opt_originalcount, 2);

        //dv_opt(end + 1, :) = { 'dataset', typ };
        dv_opt->SetValue(rdatacellindex, 0, outputs->CreateCurrencyFromString("dataset"));
        dv_opt->SetValue(rdatacellindex++, 1, outputs->CreateCurrencyFromString(type_name.c_str()));

        bool cornerdata = false;
        std::string outsys("global");
        std::string averaging("none");
        std::string layer("max");

    //Start corner data
        const OMLCurrency* inputcorner = inputs->Get(inputargindex);
        const OMLCurrency4* inputcorner4 = dynamic_cast<const OMLCurrency4*>(inputcorner);
    
        if (nullptr == inputcorner4)
            eval->ThrowError(ERR_MSG_INVALID_CORNERDATA);

        bool default_corner = false;
        if (inputcorner4->IsLogical())
        {
            cornerdata = inputcorner4->GetLogical();
        }
        else
        {
            bool is_valid = false;
            if (inputs->Get(inputargindex)->IsMatrix())
            {
                const OMLMatrix* inputcompmat = inputs->Get(inputargindex)->GetMatrix();
                if ((0 == inputcompmat->GetRows()) && (0 == inputcompmat->GetCols()))
                {
                    is_valid = true;
                    default_corner = true;
                }
            }

            if (!is_valid)
                eval->ThrowError(ERR_MSG_INVALID_CORNERDATA);
        }

        //Corner data is not applicable for vectorsand scalars
        if ("s" == compformat)
        {
            if (cornerdata && !default_corner)
                RdataWarningMsgOnly(eval, WAR_MSG_INVALID_CORNERDATA);
        }
        else
        {
            dv_opt->SetValue(rdatacellindex, 0, outputs->CreateCurrencyFromString("corners"));
            if (cornerdata)
            {
                dv_opt->SetValue(rdatacellindex++, 1, outputs->CreateCurrencyFromString("true"));
            }
            else
            {
                dv_opt->SetValue(rdatacellindex++, 1, outputs->CreateCurrencyFromString("false"));
            }
        }
    //End corner data

    //Start output/coordinate system
        inputargindex++;
        bool default_coordinate = false;
        if (inputs->Get(inputargindex)->IsString())
        {
            outsys = inputs->Get(inputargindex)->GetString();
        }
        else
        {
            bool is_valid = false;
            if (inputs->Get(inputargindex)->IsMatrix())
            {
                const OMLMatrix* inputcompmat = inputs->Get(inputargindex)->GetMatrix();
                if ((0 == inputcompmat->GetRows()) && (0 == inputcompmat->GetCols()))
                {
                    is_valid = true;
                    default_coordinate = true;
                }
            }

            if (!is_valid)
                eval->ThrowError(ERR_MSG_INVALID_COORDINATESYS);
        }

        std::string outsystofill("");

        // Output system is not applicable for scalars
        if ("s" == compformat)
        {
            if ("global" == outsys && !default_coordinate)
                RdataWarningMsgOnly(eval, WAR_MSG_INVALID_COORDINATESYS);

            outsystofill = "global";
        }
        else
        {
            if ("global" == outsys)
                outsystofill = "global";
            else if ("elemental" == outsys)
            {
                if ("v" == compformat)
                {
                    if (!default_coordinate)
                        RdataWarningMsgOnly(eval, WAR_MSG_INVALID_ELEMENTAL);
                    outsystofill = "global";
                }
                else
                {
                    outsystofill = "elemental";
                }
            }
            else if ("analysis" == outsys)
                outsystofill = "analysis";
            else
                eval->ThrowError(ERR_MSG_INVALID_COORDINATESYS);
        }

        if ("" != outsystofill)
        {
            dv_opt->SetValue(rdatacellindex, 0, outputs->CreateCurrencyFromString("output_system"));
            dv_opt->SetValue(rdatacellindex++, 1, outputs->CreateCurrencyFromString(outsystofill.c_str()));
        }

    //End output/coordinate systme

    //Start averaging method
        inputargindex++;
        bool default_averaging = false;
        if (inputs->Get(inputargindex)->IsString())
        {
            averaging = inputs->Get(inputargindex)->GetString();
        }
        else
        {
            bool is_valid = false;
            if (inputs->Get(inputargindex)->IsMatrix())
            {
                const OMLMatrix* inputcompmat = inputs->Get(inputargindex)->GetMatrix();
                if ((0 == inputcompmat->GetRows()) && (0 == inputcompmat->GetCols()))
                {
                    is_valid = true;
                    default_averaging = true;
                }
            }

            if (!is_valid)
                eval->ThrowError(ERR_MSG_INVALID_AVERAGEMODE);
        }

        //Averaging is not applicable for vectors
        if ("v" == compformat)
        {
            if (!default_averaging)
                RdataWarningMsgOnly(eval, WAR_MSG_INVALID_AVERAGING);
        }
        else
        {
            std::string averagingmodetofill("");
            if ("none" == averaging)
            {
                if (cornerdata)
                    eval->ThrowError(ERR_MSG_INVALID_AVERAGING_CORNER);
                else
                    ; //do nothing
            }
            else if ("simple" == averaging)
                averagingmodetofill = "average";
            else if ("maximum" == averaging)
                averagingmodetofill = "max";
            else if ("minimum" == averaging)
                averagingmodetofill = "min";
            else if ("summation" == averaging)
                averagingmodetofill = "sum";
            else if ("extreme" == averaging)
                averagingmodetofill = "ext";
            else
                eval->ThrowError(ERR_MSG_INVALID_AVERAGEMODE);

            if ("" != averagingmodetofill)
            {
                dv_opt->SetValue(rdatacellindex, 0, outputs->CreateCurrencyFromString("average"));
                dv_opt->SetValue(rdatacellindex++, 1, outputs->CreateCurrencyFromString("nodes_advanced"));

                dv_opt->SetValue(rdatacellindex, 0, outputs->CreateCurrencyFromString("average_mode"));
                dv_opt->SetValue(rdatacellindex++, 1, outputs->CreateCurrencyFromString(averagingmodetofill.c_str()));
            }
        }

    //End averaging method

    //Start Layer
        bool default_layer = false;
        inputargindex++;
        if (inputs->Get(inputargindex)->IsString())
        {
            layer = inputs->Get(inputargindex)->GetString();
        }
        else
        {
            bool is_valid = false;
            if (inputs->Get(inputargindex)->IsMatrix())
            {
                const OMLMatrix* inputcompmat = inputs->Get(inputargindex)->GetMatrix();
                if ((0 == inputcompmat->GetRows()) && (0 == inputcompmat->GetCols()))
                {
                    is_valid = true;
                    default_layer = true;
                }
            }

            if (!is_valid)
                eval->ThrowError(ERR_MSG_INVALID_LAYER);
        }
    
        std::string layertofill("");
        if ("max" == layer || "min" == layer || "sum" == layer || "average" == layer ||
            "range" == layer || "count" == layer || "maxlayer" == layer ||
            "minlayer" == layer || "bottom" == layer || "top" == layer)
            layertofill = layer;
        else if ("extreme" == layer)
            layertofill = "ext";
        else
            eval->ThrowError(ERR_MSG_INVALID_LAYER);

        //Layer is not applicable for vectors
        if ("v" == compformat)
        {
            if (!default_layer)
                RdataWarningMsgOnly(eval, WAR_MSG_INVALID_LAYER);
        }
        else
        {
            dv_opt->SetValue(rdatacellindex, 0, outputs->CreateCurrencyFromString("layer"));
            dv_opt->SetValue(rdatacellindex++, 1, outputs->CreateCurrencyFromString(layertofill.c_str()));
        }

     //End Layer

        //Create newdv_opt with correct size, since resize option not yet available for BCI interface
        OMLCellArray* newdv_opt = nullptr;    
        if (dv_opt_originalcount > rdatacellindex)
        {
            newdv_opt = outputs3->CreateTemporaryCellArray(rdatacellindex+1, 2);
            for (int index = 0; index < rdatacellindex; index++)
            {
                newdv_opt->SetValue(index, 0, dv_opt->GetValue(index, 0));
                newdv_opt->SetValue(index, 1, dv_opt->GetValue(index, 1));
            }
        }
        else
        {
            newdv_opt = dv_opt;
        }
    

        if (timelist.size() > 0 && complist.size() > 0)
        {
            std::string indvlabel;
            std::string outdvlabel;
            int compindex = 0;
            rData::Enums::EntityType etype = rData::Enums::EntityType::ETYPE_NULL;
            for (auto compnent : complist)
            {
                if ("Value" != complist[compindex]) //Scalar component named 'Value' should not be written
                {
                    newdv_opt->SetValue(rdatacellindex, 0, outputs->CreateCurrencyFromString("component"));
                    newdv_opt->SetValue(rdatacellindex, 1, outputs->CreateCurrencyFromString(complist[compindex].c_str()));
                }

                indvlabel = GetRandomString();
                outdvlabel = CreateDerivedValueCore(eval, app_id, indvlabel, newdv_opt);
                bool is_failed = false;
                try
                {
                    etype = GetEntityType(eval, app_id, model_label, outdvlabel, subcase->GetString(), timelist[0]);
                }
                catch (...)
                {
                    is_failed = true;
                }

                if (!is_failed)
                    break;

            }
        
            if (rData::Enums::EntityType::ETYPE_NULL == etype)
                eval->ThrowError(ERR_MSG_VALIDATE_REQUEST_FAIL);

            bool is_element = true;
            if (rData::Enums::EntityType::ETYPE_ELEMENT == etype)
            {
                //Do nothing
            }
            else if (rData::Enums::EntityType::ETYPE_NODE == etype)
            {
                is_element = false;
            }
            else
            {
                eval->ThrowError(ERR_MSG_INPUT_REQUEST);
            }

            const std::vector<unsigned int>& reqeids = GetRequestIds(eval, filename, inputreq, outputs, is_element);
        
            double* outdata = outputs->AllocateData(static_cast<int>(reqeids.size() * timelist.size() * complist.size()));
            int dims[3] = { static_cast<int>(reqeids.size()), static_cast<int>(complist.size()), static_cast<int>(timelist.size()) };
            outputs->AddNDMatrix(outputs->CreateNDMatrix(3, dims, outdata));

            int num_failures = 0;
            int timeindex = 0;
            for (auto time : timelist)
            {
                compindex = 0;
                for (auto compnent : complist)
                {
                    if ("Value" != compnent) //Scalar component named 'Value' should not be written
                    {
                        newdv_opt->SetValue(rdatacellindex, 0, outputs->CreateCurrencyFromString("component"));
                        newdv_opt->SetValue(rdatacellindex, 1, outputs->CreateCurrencyFromString(compnent.c_str()));
                    }
                    indvlabel = GetRandomString();
                    outdvlabel = CreateDerivedValueCore(eval, app_id, indvlabel, newdv_opt);
                
                    int start_index = static_cast<int>( (compindex * reqeids.size()) + (timeindex * reqeids.size() * complist.size()));
                    bool status = GetValuesCore(eval, app_id, model_label, outdvlabel, subcase->GetString(), time, reqeids, 
                                                outdata, start_index);
                    if (!status)
                    {
                        std::string msg("Warning: not able to read ");
                        msg += compnent;
                        msg += ". Component will be skipped";
                        RdataWarningMsgOnly(eval, msg);
                        num_failures++;
                    }
                    compindex++;
                }
                timeindex++;
            }

            if (timelist.size() * complist.size() == num_failures)
                eval->ThrowError(ERR_MSG_UNABLE_TO_RETRIEVE);

        }
        else
        {
            eval->ThrowError(ERR_MSG_UNABLE_TO_RETRIEVE);
        }

        return true;
    }

    bool IsFileExist(OMLInterface* eval, std::string& filename)
    {
        OMLInterface4* eval4 = dynamic_cast<OMLInterface4*>(eval);    
        std::string func("isfile");
        OMLCurrencyList* in_list = eval4->CreateCurrencyList();
        in_list->AddString(filename.c_str());
        const OMLCurrency* out = eval4->CallFunction(func.c_str(), in_list);
        if (out->IsScalar())
            if (out->GetScalar())
                return true;

        return false;
    }

    std::string ValidateFileAndGetExt(OMLInterface* eval, std::string& filename)
    {
        if (!IsFileExist(eval, filename))
            eval->ThrowError(GetErrorMessage(ERR_MSG_FILE, 1).c_str());

        std::string fileext;
        size_t lastdot = filename.find_last_of('.');

        if (lastdot != std::string::npos)
            fileext = filename.substr(lastdot);

        std::transform(fileext.begin(), fileext.end(), fileext.begin(), ::tolower);

        if (".plt" == fileext)
            eval->ThrowError(ERR_MSG_UNSUPPORTED_MULTIBODY);

        if (".rad" == fileext)
            eval->ThrowError(ERR_MSG_UNSUPPORTED_RADIOSSCRASH);

        if (".abstat" == fileext)
            eval->ThrowError(ERR_MSG_UNSUPPORTED_ABSTAT);

        return fileext;
    }

    void ValidateInputScalarOrString(OMLInterface* eval, const OMLCurrency* input, int index)
    {
        if (!input->IsScalar() && !input->IsString())
        {
            eval->ThrowError(GetErrorMessage(ERR_MSG_SCALARSTRING, index).c_str());
        }
    }

    void ValidateInputTypeStringScalarMatCell(OMLInterface* eval, const OMLCurrency* input, int index)
    {
        if (!input->IsScalar() && !input->IsString() && !input->IsCellArray() && !input->IsMatrix())
        {
            eval->ThrowError(GetErrorMessage(ERR_MSG_SCALARSTRINGMATCELL, index).c_str());
        }
    }

    bool TimeCmp(double timeone, double timetwo)
    {
        double max_f_s = std::max({ 1.0, std::fabs(timeone), std::fabs(timetwo) });
        if (std::fabs(timeone - timetwo) <= std::numeric_limits<double>::epsilon() * max_f_s)
        {
            return true;
        }
        return false;
    }

    unsigned int GetIdFromStr(OMLInterface* eval, const std::string& req, bool iselement)
    {
        std::string req_token;

        try
        {
            if (iselement)
                req_token = strtok(const_cast<char*>(req.c_str()), ELIMENT_REPR);
            else
                req_token = strtok(const_cast<char*>(req.c_str()), NODE_REPR);
        }
        catch (...)
        {
            eval->ThrowError(ERR_MSG_INPUT_REQUEST);
        }


        return std::stoul(req_token);
    }

    std::string GetRandomString()
    {
        std::string tempstr("_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
        std::random_device randevice;
        std::mt19937 generator(randevice());
        std::shuffle(tempstr.begin(), tempstr.end(), generator);
        int size = 20; // to limit the size of the label
        if (tempstr.size() > size)
            tempstr = tempstr.substr(0, size);

        return "dv_" + tempstr + "_label";
    }

    bool RdataWarningMsgOnly(OMLInterface* eval, std::string msg)
    {
        std::string func("warningmsgonly");

        OMLInterface4* eval4 = dynamic_cast<OMLInterface4*>(eval);
        OMLCurrencyList* in_list = eval4->CreateCurrencyList();
        in_list->AddString(msg.c_str());

        eval4->CallFunction(func.c_str(), in_list);

        return true;
    }

    OMLCellArray* GetNonTempCellArray(const OMLCellArray* cell, OMLCurrencyList* outputs)
    {
        OMLCellArray* outcell = outputs->CreateCellArray(cell->GetRows(), cell->GetCols());
        for (int rowind = 0; rowind < cell->GetRows(); rowind++)
        {
            for (int colind = 0; colind < cell->GetCols(); colind++)
            {
                outcell->SetValue(rowind, colind, cell->GetValue(rowind, colind));
            }
        }
        return outcell;
    }

    OMLCellArray* SwapAndGetNonTempCellArray(const OMLCellArray* cell, OMLCurrencyList* outputs)
    {
        OMLCellArray* outcell = outputs->CreateCellArray(cell->GetCols(), cell->GetRows());
        for (int rowind = 0; rowind < cell->GetRows(); rowind++)
        {
            for (int colind = 0; colind < cell->GetCols(); colind++)
            {
                outcell->SetValue(colind, rowind, cell->GetValue(rowind, colind));
            }
        }
        return outcell;
    }
}
