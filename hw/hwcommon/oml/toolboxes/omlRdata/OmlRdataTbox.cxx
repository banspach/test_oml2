/**
* @file OmlRdataTbox.cxx
* @date October 2021
* Copyright (C) 2021-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include "OmlRdataTbox.h"
#include "OmlRdata.h"
#include "OmlRdataWrapper.h"
#include "OmlRdataError.h"


#define TBOXVERSION 2022.2
#define RDATA "CAEReaders3D"


int InitToolbox(OMLInterface5 * eval)
{
    //ToGet Rdata debug information set following environment variable
    //RDATA_DEBUG = true

    //To avoid Compose crash on fail to locate rdata binaries
    //VSM-9068
    /*try
    {
        omlrdata::Initialize();
    }
    catch (...)
    {
        eval->ThrowError(ERR_MSG_RDATA_INITIALIZE_FAIL);
        return 0;
    }*/

    //Since we provided wrapper functions we don't need following hidden functions
    //As discussed with roberta, commenting hidden functions, keeping the commented code future reference
    /*eval->RegisterHiddenFunction("getversion", GetVersion);
    eval->RegisterHiddenFunction("startrdataapplication", StartRdataApplication);
    eval->RegisterHiddenFunction("createapplication", CreateApplication);
    eval->RegisterHiddenFunction("deleteapplication", DeleteApplication);
    eval->RegisterHiddenFunction("addresource", AddResource);
    eval->RegisterHiddenFunction("removeresource", RemoveResource);
    eval->RegisterHiddenFunction("createmodel", CreateModel);
    eval->RegisterHiddenFunction("deletemodel", DeleteModel);
    eval->RegisterHiddenFunction("getresourcelist", GetResourceList);
    eval->RegisterHiddenFunction("getloadcaselist", GetLoadcaseList);
    eval->RegisterHiddenFunction("getdatasetlist", GetDatasetList);
    eval->RegisterHiddenFunction("getsteplist", GetStepList);
    eval->RegisterHiddenFunction("gethierarchy", GetHierarchy);
    eval->RegisterHiddenFunction("createderivedvalue", CreateDerivedValue);
    eval->RegisterHiddenFunction("removederivedvalue", RemoveDerivedValue);
    eval->RegisterHiddenFunction("getnumparts", GetNumParts);
    eval->RegisterHiddenFunction("getnumelements", GetNumElements);
    eval->RegisterHiddenFunction("getnumnodes", GetNumNodes);
    eval->RegisterHiddenFunction("getparts", GetParts);
    eval->RegisterHiddenFunction("getelements", GetElements);
    eval->RegisterHiddenFunction("getnodes", GetNodes);
    eval->RegisterHiddenFunction("getvalues", GetValues);*/
    
    // Note: When adding new functions, the first line should call
    // OmlRDataCheckFunc as this is a professional library
    eval->RegisterFunctionWithMetadata("getsubcaselist3d", omlrdata::GetSubcaseList3d, RDATA, 1, 1, false);
    eval->RegisterFunctionWithMetadata("getsubcasename3d", omlrdata::GetSubcaseName3d, RDATA, 2, 1, false);
    eval->RegisterFunctionWithMetadata("getsubcaseindex3d", omlrdata::GetSubcaseIndex3d, RDATA, 2, 1, false);
    eval->RegisterFunctionWithMetadata("getnumsubcases3d", omlrdata::GetNumSubcases3d, RDATA, 1, 1, false);
    eval->RegisterFunctionWithMetadata("getnumtypes3d", omlrdata::GetNumTypes3d, RDATA, -2, 1, false);
    eval->RegisterFunctionWithMetadata("gettypelist3d", omlrdata::GetTypelist3d, RDATA, -2, 1, false);
    eval->RegisterFunctionWithMetadata("gettypename3d", omlrdata::GetTypename3d, RDATA, -3, 1, false);
    eval->RegisterFunctionWithMetadata("gettypeindex3d", omlrdata::GetTypeIndex3d, RDATA, -3, 1, false);
    eval->RegisterFunctionWithMetadata("getelemlist3d", omlrdata::GetElemList3d, RDATA, 1, 1, false);
    eval->RegisterFunctionWithMetadata("getnumelems3d", omlrdata::GetNumElems3d, RDATA, 2, 1, false);
    eval->RegisterFunctionWithMetadata("getelemindex3d", omlrdata::GetElemIndex3d, RDATA, 2, 1, false);
    eval->RegisterFunctionWithMetadata("getelemname3d", omlrdata::GetElemName3d, RDATA, 2, 1, false);
    eval->RegisterFunctionWithMetadata("getnodelist3d", omlrdata::GetNodeList3d, RDATA, 1, 1, false);
    eval->RegisterFunctionWithMetadata("getnumnodes3d", omlrdata::GetNumNodes3d, RDATA, 2, 1, false);
    eval->RegisterFunctionWithMetadata("getnodeindex3d", omlrdata::GetNodeIndex3d, RDATA, 2, 1, false);
    eval->RegisterFunctionWithMetadata("getnodename3d", omlrdata::GetNodeName3d, RDATA, 2, 1, false);
    eval->RegisterFunctionWithMetadata("getnumcomps3d", omlrdata::GetNumComps3d, RDATA, -3, 1, false);
    eval->RegisterFunctionWithMetadata("getcomplist3d", omlrdata::GetComplist3d, RDATA, -3, 1, false);
    eval->RegisterFunctionWithMetadata("getcompindex3d", omlrdata::GetCompindex3d, RDATA, -4, 1, false);
    eval->RegisterFunctionWithMetadata("getcompname3d", omlrdata::GetCompname3d, RDATA, -4, 1, false);
    eval->RegisterFunctionWithMetadata("gettimesteplist3d", omlrdata::GetTimeSteplist3d, RDATA, -2, 1, false);
    eval->RegisterFunctionWithMetadata("readcae3d", omlrdata::ReadCae3d, RDATA, -10, 1, false);
    eval->RegisterFunctionWithMetadata("releasefileinfo3d", omlrdata::StopRdataApplication, RDATA, -1, 0, false);

    return 0;
}

void Finalize(void)
{
    omlrdata::Clear();
}

double GetToolboxVersion(OMLInterface4* eval)
{
    return TBOXVERSION;
}
