/**
* @file OmlRdata.cxx
* @date June 2021
* Copyright (C) 2021-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include "OmlRdata.h"
#include "OmlRdataError.h"

#include "BuiltInFuncsCore.h"
#include "OML_Error.h"

// Licensing
typedef bool(*LICFUNCPTR)();
bool g_licInitialized = false;              // True if licensing is done
bool g_businessLibsAuthorized = false;      // True if business library is authorized

#define DOUBLE_NAN std::numeric_limits<double>::quiet_NaN()

using namespace rData;
using namespace rData::Tools;
namespace omlrdata
{

    struct AppInfo {
        std::string app_id;
        std::string res_label;
        std::string model_label;
    };

    boost::shared_ptr<Bridge::Factory> _factory;

    std::unordered_map<std::string, boost::shared_ptr<Bridge::Application> > _apps;
    std::unordered_map<std::string, boost::shared_ptr<Bridge::Model> > _models;
    std::unordered_map<std::string, AppInfo> _appids;

    //ToDo: remove GetErrorMessage function once ThrowError accepts argument index
    std::string GetErrorMessage(const std::string message, long long arg_index)
    {
        std::string msgStr;

        size_t pos = message.find(';');
        size_t len = message.length();

        std::string argChar = std::to_string(static_cast<long long>(arg_index));

        msgStr = (pos == std::string::npos) ? message :
            message.substr(0, pos);

        // get message up to semicolon, insert argument number
        msgStr += " in argument " + argChar;

        if ((pos + 2) < len)
            msgStr += "; " + message.substr(pos + 2, len - pos);
        else
            msgStr += "; ";

        return msgStr;
    }

    void StringCheck(OMLInterface* eval, const OMLCurrency* input,
                     long long arg_index)
    {
        if (nullptr != input && !input->IsString())
        {
            std::string err = GetErrorMessage(ERR_MSG_STRING, arg_index);
            eval->ThrowError(err.c_str());
        }
    }

    void CellArryCheck(OMLInterface* eval, const OMLCurrency* input,
                       long long arg_index)
    {
        if (nullptr != input && !input->IsCellArray())
        {
            std::string err = GetErrorMessage(ERR_MSG_CELLARRAY, arg_index);
            eval->ThrowError(err.c_str());
        }
    }

    void ScalarCheck(OMLInterface* eval, const OMLCurrency* input,
                     long long arg_index)
    {
        if (nullptr != input && !input->IsScalar())
        {
            std::string err = GetErrorMessage(ERR_MSG_SCALAR, arg_index);
            eval->ThrowError(err.c_str());
        }
    }
	
    inline boost::shared_ptr<Bridge::Model> __getModel(OMLInterface* i, std::string id)
    {
        auto m = _models.find(id);
        if (m == _models.end())
        {
            i->ThrowError("rData: model doesn't exists");
        }

        try
        {
            if (m->second->Valid())
                return m->second;
        }
        catch (rData::Exception e)
        {
            return nullptr;
        }

        return nullptr;
    }

    void AttrToStr(std::ostringstream& os, IAttributeListPtr attr)
    {
        for (attr->First(); attr->Valid(); attr->Next())
        {
            os << attr->Item()->GetLabel() << '=' << 
                attr->Item()->GetValue()->GetString()->c_str() << ", ";
        }
    }

    void Initialize()
    {
        _factory.reset(new Bridge::Factory());
    }

    void Clear()
    {
        _appids.clear();
        _models.clear();
        _apps.clear();
        _factory.reset();
    }
    //--------------------------------------------------------------------------
    // Implements [releasefileinfo3d]
    //--------------------------------------------------------------------------
    bool StopRdataApplication(OMLInterface* eval, const OMLCurrencyList* inputs,
                              OMLCurrencyList* outputs)
    {
        OmlRDataCheckFunc(eval, "releasefileinfo3d"); // License check

        if (inputs->Size() > 1)
            eval->ThrowError("Usage: releasefileinfo3d [file]");

        if (inputs->Get(0))
        {
            StringCheck(eval, inputs->Get(0), 1);
            std::string filename = Normpath(inputs->Get(0)->GetString());
            auto app_id_itr = _appids.find(filename);
            if (app_id_itr != _appids.end())
            {
                DeleteModelCore(eval, app_id_itr->second.app_id,
                             app_id_itr->second.model_label);

                RemoveResourceCore(eval, app_id_itr->second.app_id,
                                app_id_itr->second.res_label);

                DeleteApplicationCore(app_id_itr->second.app_id);
                _appids.erase(filename);
            }
        }
        else
        {
            for (auto const& itr : _appids)
            {
                DeleteModelCore(eval, itr.second.app_id, itr.second.model_label);
                RemoveResourceCore(eval, itr.second.app_id, itr.second.res_label);
                DeleteApplicationCore(itr.second.app_id);
            }
            _appids.clear();
        }

        return true;
    }

    std::string CreateApplicationCore(OMLInterface* eval, const std::string& appname,
                                      const std::string& console)
    {
        AttributeListBuilder args;
        if (console == "true")
            args.Add("~argv", "--console");

        boost::shared_ptr<Bridge::Application> app;

        try
        {
		    app.reset(new Bridge::Application(_factory->CreateApplication(
                appname.c_str(), args.GetAttributes())));
        }
        catch (rData::Exception e)
        {
            eval->ThrowError(StringOps::Format("rD  ata: %s", 
                             e.what().c_str())->c_str());
        }

        uint64_t id = (uint64_t)app->CoreObject()->GetApplication()->GetID();
        std::string app_id = std::to_string(id);
        _apps[app_id] = app;

        return app_id;
    }

    void DeleteApplicationCore(const std::string& app_id)
    {
        _apps.erase(app_id);
    }

    std::string AddResourceCore(OMLInterface* eval, const std::string& app_id,
                             const std::string& res_label, const std::string& path)
    {
        try
        {
            Bridge::ResourceListPtr rlist = _apps[app_id]->GetResourceList();
            if (rlist->Find(res_label.c_str()))
            {
                throw rData::Exception(*StringOps::Format("Resource %s alredy exists.", 
                                                          res_label.c_str()));
            }

            auto attr = AttributeListBuilder().Add("path", path.c_str()).GetAttributes();
            _apps[app_id]->AddResource(res_label.c_str(), attr);

            rlist = _apps[app_id]->GetResourceList();
            if (!rlist->Find(res_label.c_str()))
            {
                throw rData::Exception(*StringOps::Format("Failed to add resource %s",
                    path.c_str()));
            }
        }
        catch (rData::Exception e)
        {
            eval->ThrowError(StringOps::Format("rData: %s", e.what().c_str())->c_str());
        }

        return res_label;
    }

    void RemoveResourceCore(OMLInterface* eval, const std::string& app_id,
                         const std::string& res_label)
    {
        try
        {
            _apps[app_id]->RemoveResource(res_label.c_str());
        }
        catch (rData::Exception e)
        {
            eval->ThrowError(StringOps::Format("rData: %s", 
                                                e.what().c_str())->c_str());
        }
    }

    std::string CreateModelCore(OMLInterface* eval, const std::string& app_id,
                            const std::string& model_label,
                            const std::string& res_label,
                            const std::string& channel, const OMLCellArray* args)
    {
        std::string model_id = model_label + app_id;

        AttributeListBuilder a;
        a.Add("resource", res_label.c_str());
        a.Add("channel", channel.c_str());
        a.Add("name", channel.c_str());
        a.Add("label", model_label.c_str());
        a.Add("step", 0);
        a.Add("subcase", 0);
        a.Add("database", "main");
        a.Add("lod", 0);

        if (args)
        {
            const int rows = args->GetRows();
            for (int r = 0; r < rows; ++r)
            {
                const std::string key = args->GetValue(r, 0)->GetString();
                const std::string val = args->GetValue(r, 1)->GetString();
                a.Add(key.c_str(), val.c_str());
            }
        }

        try
        {
            auto attr = _apps[app_id]->CreateModel(channel.c_str(), a.GetAttributes());
            _models[model_id].reset(new Bridge::Model(attr));
        }
        catch (rData::Exception e)
        {
            eval->ThrowError(StringOps::Format("rData: %s", e.what().c_str())->c_str());
        }

        return model_label;
    }

    void DeleteModelCore(OMLInterface* eval, const std::string& app_id,
                      const std::string& model_label)
    {
        std::string model_id = model_label + app_id;

        auto m = __getModel(eval, model_id);
        if (m)
        {
            m->Dispose();
        }
        _models.erase(model_id);
    }

    void StartRdataApplicationCore(OMLInterface* eval, std::string& app_id,
                                   const std::string& res, const std::string& model,
                                   std::string& filename, std::string& res_label,
                                   std::string& model_label, std::string app,
                                   std::string model_type, std::string console)
    {
        auto app_id_itr = _appids.find(filename);
        //std::string app_id;

        if (app_id_itr == _appids.end())
        {
            app_id = CreateApplicationCore(eval, app, console);

            res_label = AddResourceCore(eval, app_id, res_label, filename);
            model_label = CreateModelCore(eval, app_id, model_label, res_label,
                                       model_type, nullptr);

            AppInfo app_data = { app_id,  res_label , model_label };
            _appids[filename] = app_data;
        }
        else
        {
            app_id = app_id_itr->second.app_id;
            res_label = app_id_itr->second.res_label;
            model_label = app_id_itr->second.model_label;
        }
    }

    OMLCellArray* GetLoadcaseListCore(OMLInterface* eval, const std::string& app_id,
                                      const std::string& res_label,
                                      OMLCurrencyList* outputs)
    {
        HierarchyBrowser browser(_apps[app_id]->GetHierarchy());

        HierarchyBrowser::ResourcePtr res = browser.FindResource(res_label.c_str());
        auto subcases = res->GetSubcases();

        int count = 0;
        for (subcases->First(); subcases->Valid(); subcases->Next())
        {
            ++count;
        }
        
        OMLCurrencyList3* outputs3 = reinterpret_cast<OMLCurrencyList3*>(outputs);
        OMLCellArray* result = outputs3->CreateTemporaryCellArray(count ? count : 1, 2);
        if (count)
        {
            count = 0;
            char buff[16];
            for (subcases->First(); subcases->Valid(); subcases->Next())
            {
                sprintf(buff, "%u", subcases->Item()->GetID());
                result->SetValue(count, 0, outputs->CreateCurrencyFromString(buff));

                std::ostringstream os;
                AttrToStr(os, subcases->Item()->GetAttributes());
                result->SetValue(count, 1, 
                                 outputs->CreateCurrencyFromString(os.str().c_str()));

                ++count;
            }
        }
        return result;
    }

    OMLCellArray* GetDatasetListCore(OMLInterface* eval, const std::string& app_id,
                                     const std::string& res_label,
                                     uint32_t subcase_id, OMLCurrencyList* outputs)
    {
        HierarchyBrowser browser(_apps[app_id]->GetHierarchy());

        HierarchyBrowser::ResourcePtr res = browser.FindResource(res_label.c_str());
        auto subcase = res->FindSubcase(subcase_id);

        if (nullptr == subcase)
            eval->ThrowError(ERR_MSG_SUBCASEID);

        auto datasetlist = subcase->GetDatasets();
        int count = 0;
        for (datasetlist->First(); datasetlist->Valid(); datasetlist->Next())
        {
            ++count;
        }

        int size = static_cast<int>(count ? count : 1);
        OMLCurrencyList3* outputs3 = reinterpret_cast<OMLCurrencyList3*>(outputs);
        OMLCellArray* type = outputs3->CreateCellArray(1, size);
        OMLCellArray* format = outputs3->CreateCellArray(1, size);
        if (count)
        {
            count = 0;
            for (datasetlist->First(); datasetlist->Valid(); datasetlist->Next())
            {
                auto label = datasetlist->Item()->GetLabel();
                OMLCurrency* label_cur = outputs->CreateCurrencyFromString(label);
                type->SetValue(0, count, label_cur);

                std::ostringstream os;
                AttrToStr(os, datasetlist->Item()->GetAttributes());
                format->SetValue(0, count,
                                outputs->CreateCurrencyFromString(os.str().c_str()));

                ++count;
            }
        }
        OMLCellArray* result = outputs3->CreateTemporaryCellArray(2, 1);
        result->SetValue(0, 0, type->GetCurrency());
        result->SetValue(1, 0, format->GetCurrency());
        return result;
    }

    OMLCellArray* GetStepListCore(const std::string& app_id,
                                  const std::string& res_label, uint32_t subcase_id,
                                  OMLCurrencyList* outputs)
    {
        HierarchyBrowser browser(_apps[app_id]->GetHierarchy());

        HierarchyBrowser::ResourcePtr res = browser.FindResource(res_label.c_str());
        auto subcase = res->FindSubcase(subcase_id);
        auto steplist = subcase->GetSteps();
        const size_t count = steplist->size();
        OMLCurrencyList3* outputs3 = reinterpret_cast<OMLCurrencyList3*>(outputs);
        OMLCellArray* result = outputs3->CreateTemporaryCellArray(2, 1);
        int size = static_cast<int>(count ? count : 1);
        OMLCellArray* stepidlist = outputs3->CreateCellArray(size, 1);
        OMLCellArray* outsteplist = outputs3->CreateCellArray(size, 1);

        char buff[64];
        for (size_t step_idx = 0; step_idx < count; ++step_idx)
        {
            auto step = (*steplist)[step_idx];
            sprintf(buff, "%u", step->GetIndex());
            stepidlist->SetValue(static_cast<int>(step_idx), 0,
                                 outputs->CreateCurrencyFromString(buff));

            std::ostringstream os;
            AttrToStr(os, step->GetAttributes());
            outsteplist->SetValue(static_cast<int>(step_idx), 0,
                                  outputs->CreateCurrencyFromString(os.str().c_str()));
        }

        result->SetValue(0, 0, outsteplist->GetCurrency());
        result->SetValue(1, 0, stepidlist->GetCurrency());
    
        return result;
    }

    std::string CreateDerivedValueCore(OMLInterface* eval,
                                       const std::string& app_id,
                                       const std::string& dv_label,
                                       const OMLCellArray* args)
    {
        AttributeListBuilder a;
        if (args)
        {
            const int rows = args->GetRows();
            for (int r = 0; r < rows; ++r)
            {
                const std::string key = args->GetValue(r, 0)->GetString();
                const std::string val = args->GetValue(r, 1)->GetString();
                a.Add(key.c_str(), val.c_str());
            }
        }

        try
        {
            _apps[app_id]->CreateDerivedValue(dv_label.c_str(), a.GetAttributes());
        }
        catch (rData::Exception e)
        {
            eval->ThrowError(StringOps::Format("rData: %s", 
                                               e.what().c_str())->c_str());
        }
        std::string new_dv_label = dv_label;
        return new_dv_label;
    }

    OMLCellArray* GetElementsCore(OMLInterface* eval, const std::string& app_id,
                                  const std::string& model_label,
                                  OMLCurrencyList* outputs, bool allinfo)
    {
        std::string model_id = model_label + app_id;

        auto m = __getModel(eval, model_id);

        OMLCellArray* elems = nullptr;
        int num_col = 4;
        if (!allinfo)
            num_col = 1;

        OMLCurrencyList3* outputs3 = reinterpret_cast<OMLCurrencyList3*>(outputs);
        if (m)
        {
            Bridge::Mesh mesh = m->GetMesh();
            size_t elem_num = mesh.GetNumElems();
            elems = outputs3->CreateTemporaryCellArray(num_col, static_cast<int>(elem_num));
            char buff[16];
            ID eId;

            for (size_t idx = 0; idx < elem_num; ++idx)
            {
                // element ID
                Bridge::Mesh::Elem e = mesh.GetElem(idx);
                eId = e.GetID();
                if (allinfo)
                {
                    OMLCellArray* id = outputs3->CreateCellArray(1, 2);
                    sprintf(buff, "%u", eId.EID());
                    id->SetValue(0, 0, outputs->CreateCurrencyFromString(buff));

                    sprintf(buff, "%u", eId.PID());
                    id->SetValue(0, 1, outputs->CreateCurrencyFromString(buff));

                    elems->SetValue(0, static_cast<int>(idx), id->GetCurrency());

                    // element Configuration
                    sprintf(buff, "%u", e.GetConfig());
                    elems->SetValue(1, static_cast<int>(idx),
                                    outputs->CreateCurrencyFromString(buff));

                    // element Connectivity
                    IArray<uint32_t>& nodes = e.GetNodes();
                    int nodes_num = static_cast<int>(nodes.size());
                    OMLCellArray* connect = outputs3->CreateCellArray(nodes_num, 1);
                    for (size_t n = 0; n < nodes_num; ++n)
                    {
                        sprintf(buff, "%u", nodes[n]);
                        connect->SetValue(static_cast<int>(n),
                                          outputs->CreateCurrencyFromString(buff));
                    }
                    elems->SetValue(2, static_cast<int>(idx), connect->GetCurrency());

                    // element's Part Index
                    sprintf(buff, "%u", e.GetPart());
                    elems->SetValue(3, static_cast<int>(idx),
                                    outputs->CreateCurrencyFromString(buff));
                }
                else
                {
                    std::string outstr_format = ELIMENT_REPR;
                    outstr_format += "%u";
                    sprintf(buff, outstr_format.c_str(), eId.EID());
                    elems->SetValue(0, static_cast<int>(idx),
                                    outputs->CreateCurrencyFromString(buff));
                }
            }
        }
        else
        {
            elems = outputs3->CreateTemporaryCellArray(0, num_col);
        }

        return elems;
    }

    OMLCellArray* GetNodesCore(OMLInterface* eval, const std::string& app_id,
                               const std::string& model_label,
                               OMLCurrencyList* outputs, bool allinfo)
    {

        std::string model_id = model_label + app_id;

        auto m = __getModel(eval, model_id);

        OMLCellArray* nodes = nullptr;
        OMLCurrencyList3* outputs3 = reinterpret_cast<OMLCurrencyList3*>(outputs);
        if (m)
        {
            Bridge::Mesh mesh = m->GetMesh();
            size_t nodes_num = mesh.GetNumNodes();
            nodes = outputs3->CreateTemporaryCellArray(1, static_cast<int>(nodes_num));
            char idString[16];
            ID nId;
            for (size_t idx = 0; idx < nodes_num; ++idx)
            {
                
                Bridge::Mesh::Node n = mesh.GetNode(idx);
                nId = n.GetID();

                if (allinfo)
                {
                    OMLCellArray* id = outputs3->CreateCellArray(1, 2);
                    sprintf(idString, "%u", nId.EID());
                    id->SetValue(0, 0, outputs->CreateCurrencyFromString(idString));

                    sprintf(idString, "%u", nId.PID());
                    id->SetValue(0, 1, outputs->CreateCurrencyFromString(idString));

                    nodes->SetValue(0, static_cast<int>(idx), id->GetCurrency());
                }
                else
                {
                    std::string outstr_format = NODE_REPR;
                    outstr_format += "%u";
                    sprintf(idString, outstr_format.c_str(), nId.EID());
                    nodes->SetValue(0, static_cast<int>(idx),
                                    outputs->CreateCurrencyFromString(idString));
                }
            }
        }
        else
        {
            nodes = outputs3->CreateTemporaryCellArray(0, 1);
        }

        return nodes;
    }

    rData::Enums::EntityType GetEntityType(OMLInterface* eval,
                                           const std::string& app_id,
                                           const std::string& model_label,
                                           const std::string& value_str,
                                           const std::string& subcase_str,
                                           const std::string& step_str)
    {
        std::string model_id = model_label + app_id;
        auto m = __getModel(eval, model_id);

        if (m)
        {
            boost::scoped_ptr<Bridge::Values> vals;
            try
            {
                vals.reset(new Bridge::Values(m->GetValues(value_str.c_str(), 
                                               AttributeListBuilder().
                                               Add("subcase", subcase_str.c_str()).
                                               Add("step", step_str.c_str()).
                                               GetAttributes())));
            }
            catch (rData::Exception e)
            {
                eval->ThrowError(StringOps::Format("rData: %s",
                                                   e.what().c_str())->c_str());
            }

            return vals->GetBinding();
        }
        else
        {
            return rData::Enums::EntityType::ETYPE_NULL;
        }
    }

    bool GetValuesCore(OMLInterface* eval, const std::string& app_id,
                       const std::string& model_label,
                       const std::string& value_str, const std::string& subcase_str,
                       const std::string& step_str,
                       const std::vector<unsigned int>& reqids, double* outdata,
                       int start_index)
    {
        std::string model_id = model_label + app_id;
        auto m = __getModel(eval, model_id);
        if (m)
        {
            boost::scoped_ptr<Bridge::Values> vals;
            try
            {
                vals.reset(new Bridge::Values(m->GetValues(value_str.c_str(),
                                              AttributeListBuilder().
                                              Add("subcase", subcase_str.c_str()).
                                              Add("step", step_str.c_str()).
                                              GetAttributes())));
            }
            catch (rData::Exception e)
            {
                for (size_t index = 0; index < reqids.size(); index++)
                {
                    outdata[start_index + index] = DOUBLE_NAN;
                }
                return false;
            }

            size_t num_rec = vals->GetNumRecords();
            const Bridge::Mesh& mesh = m->GetMesh();
            rData::Enums::EntityType bind = vals->GetBinding();
            std::unordered_map <unsigned int, size_t> records;

            for (size_t r = 0; r < num_rec; ++r)
            {
                const Bridge::Mesh::Entity& e = mesh.GetEntity(bind, r);
                
                if (records.find(e.GetID().EID()) != records.end())
                {
                    vals->Dispose();
                    //As requested by Roberta, ignoring pool id and error out if eid exist in multiple pools
                    eval->ThrowError(ERR_MSG_DUPLICATE_ID);
                }

                records[e.GetID().EID()] = r;
            }

            size_t outindex = 0;
            for (auto it : reqids)
            {
                if (records.find(it) != records.end())
                {
                    auto rec = vals->GetRecord(records[it]);
                    if (rec)
                    {
                        const float* data = rec.GetData();
                        //Assumption by Roberta: only one item exist and stried value also 1
                        outdata[start_index + outindex] = data[0];
                    }
                    else
                    {
                        outdata[start_index + outindex] = DOUBLE_NAN;
                    }
                
                }
                else
                {
                    //Never expected to reach here
                    vals->Dispose();
                    eval->ThrowError(ERR_MSG_UNABLE_TO_RETRIEVE);
                }

                outindex++;
            }

            vals->Dispose();
        }
        else
        {
            eval->ThrowError(ERR_MSG_UNABLE_TO_RETRIEVE);
        }

        return true;
    }



    //Since we provided wrapper functions we don't need following hidden functions
    //As discussed with roberta, commenting hidden functions, keeping the commented code future reference
    /*
    bool GetVersion(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 0)
            eval->ThrowError("Usage: getversion");

        Core::Configuration& cfg = Core::GetInstance().GetConfiguration();
        IAttribute::IValuePtr vmajor = cfg.GetProperty("version", "major");
        IAttribute::IValuePtr vminor = cfg.GetProperty("version", "minor");
        IAttribute::IValuePtr build = cfg.GetProperty("version", "build");
        IAttribute::IValuePtr date = cfg.GetProperty("version", "date");

        std::string ver = std::string(vmajor->GetString()->c_str()) + "." +
                            std::string(vminor->GetString()->c_str()) + "." +
                            std::string(build->GetString()->c_str())  + "." +
                            std::string(date->GetString()->c_str());

        outputs->AddString(ver.c_str());

        return true;
    }

    bool StartRdataApplication(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() < 1 || inputs->Size() > 6)
            eval->ThrowError("Usage: startrdataapplication [file] [app] [model_type] [res_label] [model_label] [console]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);
        StringCheck(eval, inputs->Get(2), 3);
        StringCheck(eval, inputs->Get(3), 4);
        StringCheck(eval, inputs->Get(4), 5);
        StringCheck(eval, inputs->Get(5), 6);

        std::string filename = inputs->Get(0)->GetString();

    #  ifdef OS_WIN
        std::replace(filename.begin(), filename.end(), '\\', '/');
    #  endif

        std::string app_id;
        std::string app = inputs->Get(1) ? inputs->Get(1)->GetString() : "aero";
        std::string model_type = inputs->Get(2) ? inputs->Get(2)->GetString() : "model";
        std::string res_label = inputs->Get(3) ? inputs->Get(3)->GetString() : "res_1";
        std::string model_label = inputs->Get(4) ? inputs->Get(4)->GetString() : "model_1";
        std::string console = inputs->Get(5) ? inputs->Get(5)->GetString() : "false";

        StartRdataApplicationCore(eval, app_id, res_label, model_label, filename,
                                  res_label, model_label, app, model_type, console);

        outputs->AddString(app_id.c_str());
        outputs->AddString(res_label.c_str());
        outputs->AddString(model_label.c_str());

        return true;
    }

    bool CreateApplication(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() < 1 || inputs->Size() > 2)
            eval->ThrowError("Usage: createapplication [name] <enable console>");


        StringCheck(eval, inputs->Get(0), 1);

        std::string appname = inputs->Get(0)->GetString();
        std::string console = "false";
        if (inputs->Get(1))
        {
            StringCheck(eval, inputs->Get(1), 2);

            console = inputs->Get(1)->GetString();
        }


        std::string app_id = CreateApplicationCore(eval, appname, console);

        outputs->AddString(app_id.c_str());

        return true;
    }

    bool DeleteApplication(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 1)
            eval->ThrowError("Usage: deleteapplication [name]");

        StringCheck(eval, inputs->Get(0), 1);

        std::string app_id = inputs->Get(0)->GetString();

        DeleteApplicationCore(app_id);

        return true;
    }

    bool AddResource(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 3)
            eval->ThrowError("Usage: addresource [application ID] [resource label] [path to resource]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);
        StringCheck(eval, inputs->Get(2), 3);

        std::string app_id = inputs->Get(0)->GetString();
        std::string res_label = inputs->Get(1)->GetString();
        std::string path = inputs->Get(2)->GetString();

        res_label = AddResourceCore(eval, app_id, res_label, path);

        outputs->AddString(res_label.c_str());

        return true;
    }

    bool RemoveResource(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 2)
            eval->ThrowError("Usage: removeresource [application ID] [resource label]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);

        std::string app_id = inputs->Get(0)->GetString();
        std::string res_label = inputs->Get(1)->GetString();

        RemoveResourceCore(eval, app_id, res_label);

        outputs->AddString("ok");

        return true;
    }

    bool CreateModel(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() < 4 || inputs->Size() > 5)
            eval->ThrowError("Usage: createmodel [application ID] [model label] [resource label] [channel] <args>");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);
        StringCheck(eval, inputs->Get(2), 3);
        StringCheck(eval, inputs->Get(3), 4);
        CellArryCheck(eval, inputs->Get(4), 5);

        std::string app_id = inputs->Get(0)->GetString();
        std::string model_label = inputs->Get(1)->GetString();
        std::string res_label = inputs->Get(2)->GetString();
        std::string channel = inputs->Get(3)->GetString();
        const OMLCellArray* args = inputs->Get(4) ? inputs->Get(4)->GetCellArray() : nullptr;

        model_label = CreateModelCore(eval, app_id, model_label, res_label, channel, args);

        outputs->AddString(model_label.c_str());

        return true;
    }

    bool DeleteModel(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 2)
            eval->ThrowError("Usage: deletemodel [application ID] [model label]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);

        std::string app_id = inputs->Get(0)->GetString();
        std::string model_label = inputs->Get(1)->GetString();

        DeleteModelCore(eval, app_id, model_label);

        outputs->AddString("ok");

        return true;
    }

    bool GetResourceList(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 1)
            eval->ThrowError("Usage: getresourcelist [application ID]");

        StringCheck(eval, inputs->Get(0), 1);


        std::string app_id = inputs->Get(0)->GetString();

        HierarchyBrowser br(_apps[app_id]->GetHierarchy());
        int count = 0;
        HierarchyBrowser::ResourceListPtr rlist = br.GetResources();
        for (rlist->First(); rlist->Valid(); rlist->Next())
        {
            ++count;
        }

        OMLCellArray * resources = outputs->CreateCellArray(count ? count : 1, 2);
        if (count)
        {
            count = 0;
            for (rlist->First(); rlist->Valid(); rlist->Next())
            {
                HierarchyBrowser::ResourcePtr rinfo = rlist->Item();
                resources->SetValue(count, 0, outputs->CreateCurrencyFromString(rinfo->GetLabel()));

                std::ostringstream os;
                AttrToStr(os, rinfo->GetAttributes());
                resources->SetValue(count, 1, outputs->CreateCurrencyFromString(os.str().c_str()));

                ++count;
            }
        }

        outputs->AddCellArray(resources);

        return true;
    }

    bool GetLoadcaseList(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 2)
            eval->ThrowError("Usage: getloadcaselist [application ID] [resource label]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);

        std::string app_id = inputs->Get(0)->GetString();
        std::string res_label = inputs->Get(1)->GetString();

        outputs->AddCellArray(GetLoadcaseListCore(eval, app_id, res_label, outputs));

        return true;
    }

    bool GetDatasetList(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 3)
            eval->ThrowError("Usage: getdatasetlist [application ID] [resource label] [subcase ID]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);
        StringCheck(eval, inputs->Get(2), 3);

        std::string app_id = inputs->Get(0)->GetString();
        std::string res_label = inputs->Get(1)->GetString();
        uint32_t subcase_id = 0;
        try
        {
            subcase_id = std::stoi(inputs->Get(2)->GetString());
        }
        catch (...)
        {
            eval->ThrowError(ERR_MSG_SUBCASEID);
        }

        OMLCellArray* typelist = GetDatasetListCore(eval, app_id, res_label, subcase_id, outputs);
        OMLCurrency* types = typelist->GetValue(0, 0);
        outputs->AddCellArray(const_cast<OMLCellArray*>(types->GetCellArray()));
        return true;
    }

    bool GetStepList(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 3)
            eval->ThrowError("Usage: getsteplist [application ID] [resource label] [subcase ID]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);
        StringCheck(eval, inputs->Get(2), 3);

        std::string app_id = inputs->Get(0)->GetString();
        std::string res_label = inputs->Get(1)->GetString();
        uint32_t subcase_id = std::stoul(inputs->Get(2)->GetString());
        outputs->AddCellArray(GetStepListCore(app_id, res_label, subcase_id, outputs));

        return true;
    }

    bool GetHierarchy(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 1)
            eval->ThrowError("Usage: gethierarchy [application ID]");

        StringCheck(eval, inputs->Get(0), 1);

        std::string app_id = inputs->Get(0)->GetString();

        std::ostringstream result;
        HierarchyBrowser br(_apps[app_id]->GetHierarchy());
        for (HierarchyBrowser::ResourceListPtr rlist = br.GetResources(); rlist->Valid(); rlist->Next())
        {
            HierarchyBrowser::ResourcePtr rinfo = rlist->Item();
            result << rinfo->GetLabel() << ": ";
            AttrToStr(result, rinfo->GetAttributes());
            result << "\n";

            for (HierarchyBrowser::SubcaseListPtr sclist = rinfo->GetSubcases(); sclist->Valid(); sclist->Next())
            {
                HierarchyBrowser::SubcasePtr sc = sclist->Item();
                result << "\t+ " << sc->GetID() << ": ";
                AttrToStr(result, sc->GetAttributes());
                result << "\n\t  DataSets:\n";
                for (HierarchyBrowser::DatasetListPtr dlist = sc->GetDatasets(); dlist->Valid(); dlist->Next())
                {
                    result << "\t\t+ " << dlist->Item()->GetLabel() << ": ";
                    AttrToStr(result, dlist->Item()->GetAttributes());
                    result << "\n";
                }

                result << "\t  Steps:\n";
                HierarchyBrowser::StepListPtr slist = sc->GetSteps();
                for (size_t i = 0, n = slist->size(); i != n; ++i)
                {
                    result << "\t\t+ " << (*slist)[i]->GetLabel() << ": ";
                    AttrToStr(result, (*slist)[i]->GetAttributes());
                    result << "\n";
                }
            }
        }

        outputs->AddString(result.str().c_str());

        return true;
    }

    bool CreateDerivedValue(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() < 2 || inputs->Size() > 3)
            eval->ThrowError("Usage: createderivedvalue [application ID] [derived value label] <args>");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);
        CellArryCheck(eval,  inputs->Get(2), 3);


        std::string app_id = inputs->Get(0)->GetString();
        std::string dv_label = inputs->Get(1)->GetString();
        const OMLCellArray * args = inputs->Get(2) ? inputs->Get(2)->GetCellArray() : nullptr;

        dv_label = CreateDerivedValueCore(eval, app_id, dv_label, args);
        outputs->AddString(dv_label.c_str());

        return true;
    }

    bool RemoveDerivedValue(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 2)
            eval->ThrowError("Usage: removederivedvalue [application ID] [derived value label]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);

        std::string app_id = inputs->Get(0)->GetString();
        std::string dv_label = inputs->Get(1)->GetString();

        try
        {
            _apps[app_id]->RemoveDerivedValue(dv_label.c_str());
        }
        catch (rData::Exception e)
        {
            eval->ThrowError(StringOps::Format("rData: %s", e.what().c_str())->c_str());
        }

        return true;
    }

    bool GetNumParts(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 2)
            eval->ThrowError("Usage: getnumparts [application ID] [model label]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);

        std::string app_id = inputs->Get(0)->GetString();
        std::string model_label = inputs->Get(1)->GetString();

        std::string model_id = model_label + app_id;

        size_t num = 0;
        auto m = __getModel(eval, model_id);
        if (m)
        {
            num = m->GetMesh().GetNumParts();
        }

        outputs->AddString(std::to_string(num).c_str());

        return true;
    }

    bool GetNumElements(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 2)
            eval->ThrowError("Usage: getnumelements [application ID] [model label]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);

        std::string app_id = inputs->Get(0)->GetString();
        std::string model_label = inputs->Get(1)->GetString();

        std::string model_id = model_label + app_id;

        size_t num = 0;
        auto m = __getModel(eval, model_id);
        if (m)
        {
            num = m->GetMesh().GetNumElems();
        }

        outputs->AddString(std::to_string(num).c_str());

        return true;
    }

    bool GetNumNodes(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 2)
            eval->ThrowError("Usage: getnumnodes [application ID] [model label]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);


        std::string app_id = inputs->Get(0)->GetString();
        std::string model_label = inputs->Get(1)->GetString();

        std::string model_id = model_label + app_id;

        size_t num = 0;
        auto m = __getModel(eval, model_id);
        if (m)
        {
            num = m->GetMesh().GetNumNodes();
        }

        outputs->AddString(std::to_string(num).c_str());

        return true;
    }

    bool GetParts(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 2)
            eval->ThrowError("Usage: getparts [application ID] [model label]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);


        std::string app_id = inputs->Get(0)->GetString();
        std::string model_label = inputs->Get(1)->GetString();

        std::string model_id = model_label + app_id;

        auto m = __getModel(eval, model_id);

        OMLCellArray * parts = nullptr;
        if (m)
        {
            Bridge::Mesh mesh = m->GetMesh();
            size_t num = mesh.GetNumParts();
            parts = outputs->CreateCellArray(static_cast<int>(num), 2);
            char idString[16];
            ID pId;
            for (size_t idx = 0; idx < num; ++idx)
            {
                OMLCellArray * id = outputs->CreateCellArray(1, 2);

                Bridge::Mesh::Part p = mesh.GetPart(idx);
                pId = p.GetID();

                sprintf(idString, "%u", pId.EID());
                id->SetValue(0, 0, outputs->CreateCurrencyFromString(idString));

                sprintf(idString, "%u", pId.PID());
                id->SetValue(0, 1, outputs->CreateCurrencyFromString(idString));

                parts->SetValue(static_cast<int>(idx), 0, id->GetCurrency());

                parts->SetValue(static_cast<int>(idx), 1, outputs->CreateCurrencyFromString(p.GetLabel()));
            }
        }
        else
        {
            parts = outputs->CreateCellArray(0, 2);
        }

        outputs->AddCellArray(parts);

        return true;
    }

    bool GetElements(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 2)
            eval->ThrowError("Usage: getelements [application ID] [model label]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);


        std::string app_id = inputs->Get(0)->GetString();
        std::string model_label = inputs->Get(1)->GetString();


        outputs->AddCellArray(GetElementsCore(eval, app_id, model_label, outputs, true));

        return true;
    }

    bool GetNodes(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 2)
            eval->ThrowError("Usage: getnodes [application ID] [model label]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);


        std::string app_id = inputs->Get(0)->GetString();
        std::string model_label = inputs->Get(1)->GetString();

        outputs->AddCellArray(GetNodesCore(eval, app_id, model_label, outputs, true));

        return true;
    }

    bool GetValues(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
    {
        if (inputs->Size() != 5)
            eval->ThrowError("Usage: getvalues [application ID] [model label] [value name] [subcase number] [step number]");

        StringCheck(eval, inputs->Get(0), 1);
        StringCheck(eval, inputs->Get(1), 2);
        StringCheck(eval, inputs->Get(2), 3);
        StringCheck(eval, inputs->Get(3), 4);
        StringCheck(eval, inputs->Get(4), 5);

        std::string app_id = inputs->Get(0)->GetString();
        std::string model_label = inputs->Get(1)->GetString();
        std::string value_str = inputs->Get(2)->GetString();
        std::string subcase_str = inputs->Get(3)->GetString();
        std::string step_str = inputs->Get(4)->GetString();

        std::string model_id = model_label + app_id;

        auto m = __getModel(eval, model_id);

        OMLCellArray * values = nullptr;
        if (m)
        {
            boost::scoped_ptr<Bridge::Values> vals;
            try
            {
                vals.reset(new Bridge::Values(m->GetValues(value_str.c_str(), AttributeListBuilder().
                    Add("subcase", subcase_str.c_str()).
                    Add("step", step_str.c_str()).
                    GetAttributes())));
            }
            catch (rData::Exception e)
            {
                eval->ThrowError(StringOps::Format("rData: %s", e.what().c_str())->c_str());
            }

            values = outputs->CreateCellArray(1, 2);

            Bridge::Mesh mesh = m->GetMesh();

            rData::Enums::EntityType bind = vals->GetBinding();
            rData::Enums::Format format = vals->GetFormat();

            OMLCellArray * info = outputs->CreateCellArray(1, 2);
            info->SetValue(0, 0, outputs->CreateCurrencyFromDouble(bind));
            info->SetValue(0, 1, outputs->CreateCurrencyFromDouble(format));
            values->SetValue(0, 0, info->GetCurrency());

            const size_t rec_num = vals->GetNumRecords();
            OMLCellArray * table = outputs->CreateCellArray(static_cast<int>(rec_num), 2);
            char idString[16];
            ID eId;
            for (size_t r = 0; r < rec_num; ++r)
            {
                OMLCellArray * id = outputs->CreateCellArray(1, 2);

                Bridge::Mesh::Entity e = mesh.GetEntity(bind, r);
                eId = e.GetID();

                sprintf(idString, "%u", eId.EID());
                id->SetValue(0, 0, outputs->CreateCurrencyFromString(idString));

                sprintf(idString, "%u", eId.PID());
                id->SetValue(0, 1, outputs->CreateCurrencyFromString(idString));

                table->SetValue(static_cast<int>(r), 0, id->GetCurrency());

                auto rec = vals->GetRecord(r);
                const float dummy_data[] = { 0.0f, 0.0f, 0.0f };
                const float * data = rec ? rec.GetData() : dummy_data;
                const uint32_t item_num = rec ? rec.GetNumItems() : 1;
                const uint32_t stride_num = rec ? rec.GetStride() : 3;
                OMLCellArray * row = outputs->CreateCellArray(static_cast<int>(item_num), 2);
                for (uint32_t it = 0; it < item_num; ++it)
                {
                    Descriptor desc = rec ? rec.GetDescriptor(it) : Descriptor();
                    OMLCellArray * descriptor = outputs->CreateCellArray(1, 2);
                    descriptor->SetValue(0, 0, outputs->CreateCurrencyFromDouble((double)(desc.GetNode())));
                    descriptor->SetValue(0, 1, outputs->CreateCurrencyFromDouble((double)(desc.GetSect())));
                    row->SetValue(static_cast<int>(it), 0, descriptor->GetCurrency());

                    OMLCellArray * stride = outputs->CreateCellArray(static_cast<int>(stride_num), 1);
                    for (uint32_t s = 0; s < stride_num; ++s)
                    {
                        stride->SetValue(static_cast<int>(s), outputs->CreateCurrencyFromDouble((double)(*data)));
                        ++data;
                    }
                    row->SetValue(static_cast<int>(it), 1, stride->GetCurrency());
                }
                table->SetValue(static_cast<int>(r), 1, row->GetCurrency());
            }
            vals->Dispose();

            values->SetValue(0, 1, table->GetCurrency());
        }
        else
        {
            values = outputs->CreateCellArray(0, 2);
        }

        outputs->AddCellArray(values);

        return true;
    }
    */

    //duplicate of BuiltInFuncsUtils::Normpath (as suggested by Hema)
    std::string Normpath(const std::string& path)
    {
        if (path.empty()) return path;

        std::string normpath(path);

    #ifdef OS_WIN
        std::replace(normpath.begin(), normpath.end(), '/', '\\');
    #else
        std::replace(normpath.begin(), normpath.end(), '\\', '/');
    #endif

        return normpath;
    }

    //--------------------------------------------------------------------------
    // Throws error for functions if not running in business edition mode
    //--------------------------------------------------------------------------
    void OmlRDataCheckFunc(OMLInterface* eval, const std::string& func)
    {
        try
        {
            if (g_businessLibsAuthorized)
            {
                return;
            }

            if (!g_licInitialized)
            {
                // Attempt to check out a Compose license without the user credentials
                std::string dllname("hwcomposelic");
#ifndef OS_WIN
                dllname = "lib" + dllname + ".so";
#endif
                void* handle = BuiltInFuncsCore::DyLoadLibrary(dllname);
                if (!handle)
                {
                    throw OML_Error("Cannot load [" + dllname + "]");
                }
                void* symbol = BuiltInFuncsCore::DyGetFunction(handle,
                    "AuthorizeBusinessEditionLibrary");
                if (!symbol)
                {
                    throw OML_Error("Unable to authorize [" + func + "]");
                }
                g_licInitialized = true;
                LICFUNCPTR funcptr = (LICFUNCPTR)(symbol);
                if (!funcptr)
                {
                    throw OML_Error("Invalid authorization function");
                }
                g_businessLibsAuthorized = funcptr();
            }

            if (!g_businessLibsAuthorized)
            {
                throw OML_Error(
                    "Invalid command [" + func + "]; available only in Compose Business Edition");
            }

            //VSM-9068
            try
            {
                omlrdata::Initialize();
            }
            catch (...)
            {
                throw OML_Error(ERR_MSG_RDATA_INITIALIZE_FAIL);
            }
        }
        catch (const OML_Error& e)
        {
            eval->ThrowError(e.GetErrorMessage().c_str());
        }
        return;
    }
}
