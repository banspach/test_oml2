/**
* @file OmlRdataError.h
* @date October 2021
* Copyright (C) 2021-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/
#ifndef __OMLRDATATBOXERROR_H__
#define __OMLRDATATBOXERROR_H__

#define ERR_MSG_RDATA_INITIALIZE_FAIL     "Error: failed to initialize 3D CAE Reader library"

#define ERR_MSG_STRING                    "Error: invalid input; must be string"
#define ERR_MSG_CELLARRAY                 "Error: invalid input; must be cell array"
#define ERR_MSG_SCALARSTRING              "Error: input must be scalar or string"
#define ERR_MSG_SCALAR                    "Error: input must be scalar"
#define ERR_MSG_FILE                      "Error: file not found;"
#define ERR_MSG_SCALARSTRINGMATCELL       "Error: invalid input; must be a scalar, string, matrix or cell array"

#define ERR_MSG_SUBCASEID                 "Error: invalid subcase ID"
#define ERR_MSG_SUBCASEINDEX              "Error: invalid subcase index"
#define ERR_MSG_INPUT_SUBCASE             "Error: invalid subcase"
#define ERR_MSG_INVALIDSUBCASERANGE       "Error: invalid input;  subcase index out of range"
#define ERR_MSG_SUBCASE_MUST              "Error: must specify a subcase for files with subcases"

#define ERR_MSG_INPUT_TYPE                "Error: invalid type"
#define ERR_MSG_INVALIDTYPERANGE          "Error: invalid input;  type index out of range"

#define ERR_MSG_INVALID_ELEMENT           "Error: invalid element"
#define ERR_MSG_INVALID_NODE              "Error: invalid node"
#define ERR_MSG_INPUT_REQUEST             "Error: invalid request"
#define ERR_MSG_DUPLICATE_ID              "Error: multiple requests exist with same ID"
#define ERR_MSG_INVALIDREQRANGE           "Error: invalid input;  request index out of range"
#define ERR_MSG_VALIDATE_REQUEST_FAIL     "Error: failed to validate the requests"
#define ERR_MSG_ELEMENTINDEX              "Error: invalid element index"
#define ERR_MSG_NODEINDEX                 "Error: invalid node index"

#define ERR_MSG_INPUT_COMPONENT           "Error: invalid component"
#define ERR_MSG_INVALIDCOMPONENTRANGE     "Error: invalid input;  component index out of range"

#define ERR_MSG_INPUT_TIME                "Error: invalid time step"
#define ERR_MSG_INVALIDTIMERANGE          "Error: invalid input;  time index out of range"

#define ERR_MSG_UNSUPPORTED_MULTIBODY     "Error: Multibody Simulation files are not yet supported"
#define ERR_MSG_UNSUPPORTED_RADIOSSCRASH  "Error: RADIOSS crash files are not yet supported"
#define ERR_MSG_UNSUPPORTED_ABSTAT        "Error: abstat files are not yet supported"

#define ERR_MSG_INVALID_CORNERDATA        "Error: invalid corner data"
#define ERR_MSG_INVALID_COORDINATESYS      "Error: invalid coordinate system"
#define ERR_MSG_INVALID_AVERAGEMODE       "Error: invalid averaging mode"
#define ERR_MSG_INVALID_LAYER             "Error: invalid layer"

#define ERR_MSG_UNABLE_TO_RETRIEVE         "Error: unable to retrieve results"
#define ERR_MSG_INVALID_AVERAGING_CORNER   "Error: an averaging method should be given if corner data is set to true"

#define WAR_MSG_INVALID_CORNERDATA         "Warning: corner data is not applicable for scalars and will be ignored"
#define WAR_MSG_INVALID_COORDINATESYS      "Warning: coordinate system is not applicable for scalars and will be ignored"
#define WAR_MSG_INVALID_ELEMENTAL          "Warning: elemental system is not applicable for vectors and will be ignored"
#define WAR_MSG_INVALID_AVERAGING          "Warning: averaging method is not applicable for vectors and will be ignored"
#define WAR_MSG_INVALID_LAYER              "Warning: layer is not applicable for vectors and will be ignored"


#endif // __OMLRDATATBOXERROR_H__