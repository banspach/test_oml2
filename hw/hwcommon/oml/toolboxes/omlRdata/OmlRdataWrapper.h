/**
* @file OmlRdataWrapper.h
* @date October 2021
* Copyright (C) 2021-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#ifndef __OMLRDATATWRAPPER_H__
#define __OMLRDATATWRAPPER_H__

#include "OmlRdataDefs.h"
#include "OmlRdata.h"

namespace omlrdata
{
    bool GetSubcaseList3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetSubcaseName3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetSubcaseIndex3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetNumSubcases3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    void GetSubcaseInfo(OMLInterface* eval, std::string& filename, OMLCellArray*& subidlist, 
        OMLCellArray*& sublist, OMLCurrencyList* outputs);

    OMLCurrency* GetSubcase(OMLInterface* eval, std::string& filename,
        const OMLCurrency* inputsubcase, OMLCurrencyList* outputs,
        std::string& app_id, std::string& res_label, std::string& model_label);

    void TreatSubcase(OMLInterface* eval, const std::string& appid, const std::string& reslabel,
        OMLCellArray*& subidlist, OMLCellArray*& sublist, OMLCurrencyList* outputs);

    bool GetNumTypes3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetTypelist3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetTypename3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetTypeIndex3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    std::string GetTypeName(OMLInterface* eval, std::string& filename, const OMLCurrency* inputsubcase,
        const OMLCurrency* inputtype, OMLCurrencyList* outputs);
    OMLCellArray* GetTypelist3dCore(OMLInterface* eval, std::string& filename,
        const OMLCurrency* inputsubcase, OMLCurrencyList* outputs);
    OMLCurrency* GetTypeFormat(OMLInterface* eval, std::string& filename,
        const OMLCurrency* inputsubcase,
        const OMLCurrency* inputtype, OMLCurrencyList* outputs);
    void GetTypeInfo(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs, bool isindex);


    bool GetElemList3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetNumElems3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetElemIndex3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetElemName3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetNodeList3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);    
    bool GetNumNodes3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetNodeIndex3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetNodeName3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetNumComps3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    std::vector<unsigned int> GetRequestIds(OMLInterface* eval, std::string& filename,
        const OMLCurrency* inputreq, OMLCurrencyList* outputs,
        bool is_element);
    OMLCellArray* GetElementOrNodeList(OMLInterface* eval, std::string& filename,
        OMLCurrencyList* outputs, bool elements);

    bool GetNumComps3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetComplist3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetCompname3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    bool GetCompindex3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    void GetComplist3dCore(OMLInterface* eval, std::string& filename, const OMLCurrency* inputsubcase,
        const OMLCurrency* inputtype, OMLCurrencyList* outputs, OMLCellArray*& comps,
        std::string& compformat);
    void GetCompInfo(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs, bool isindex);
    std::vector<std::string> GetCompList(OMLInterface* eval, std::string& filename,
        const OMLCurrency* inputsubcase, const OMLCurrency* inputtype,
        const OMLCurrency* inputcomp, OMLCurrencyList* outputs,
        std::string& compformat);

    bool GetTimeSteplist3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
    OMLCellArray* TreatTimeSteps(const OMLCellArray* originalsteplist, OMLCurrencyList* outputs);
    std::vector<std::string> GetTimeList(OMLInterface* eval, const OMLCurrency* subcase,
        const OMLCurrency* inputtime, const std::string& app_id,
        const std::string& res_label, OMLCurrencyList* outputs);

    bool ReadCae3d(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);

    bool IsFileExist(OMLInterface* eval, std::string& filename);
    std::string ValidateFileAndGetExt(OMLInterface* eval, std::string& filename);
    void ValidateInputScalarOrString(OMLInterface* eval, const OMLCurrency* input, int index);
    void ValidateInputTypeStringScalarMatCell(OMLInterface* eval, const OMLCurrency* input, int index);
    bool TimeCmp(double timeone, double timetwo);
    unsigned int GetIdFromStr(OMLInterface* eval, const std::string& req, bool iselement);
    std::string GetRandomString();
    bool RdataWarningMsgOnly(OMLInterface* eval, std::string msg);
    OMLCellArray* GetNonTempCellArray(const OMLCellArray* cell, OMLCurrencyList* outputs);
    OMLCellArray* SwapAndGetNonTempCellArray(const OMLCellArray* cell, OMLCurrencyList* outputs);    
}
#endif