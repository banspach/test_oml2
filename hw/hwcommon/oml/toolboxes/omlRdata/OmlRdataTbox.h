/**
* @file OmlRdataTbox.h
* @date October 2021
* Copyright (C) 2021 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/
#ifndef __OMLRDATATBOX_H__
#define __OMLRDATATBOX_H__

#include "OMLInterfacePublic.h"
#include "OmlRdataDefs.h"

extern "C"
{
    OMLRDATA_DECLS int InitToolbox(OMLInterface5* eval);
    OMLRDATA_DECLS double GetToolboxVersion(OMLInterface4* eval);
    OMLRDATA_DECLS void Finalize(void);
}

#endif // __OMLRDATATBOX_H__