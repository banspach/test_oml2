/**
* @file SpecialFuncsTbox.h
* @date December 2021
* Copyright (C) 2021-2022 Altair Engineering, Inc.
* 
* Commercial License Information: 
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/

#ifndef __SPECIALFUNCSTBOXFUNCS_OML_H__
#define __SPECIALFUNCSTBOXFUNCS_OML_H__

#pragma warning(disable: 4251)

#include "EvaluatorInt.h"
#include "SpecialFuncsTboxDefs.h"

template <typename T1, typename T2> class hwTMatrix;
typedef hwTMatrix<double, hwTComplex<double> > hwMatrix;

//------------------------------------------------------------------------------
//!
//! \brief oml Special Functions functions
//!
//------------------------------------------------------------------------------

extern "C" 
{
    //!
    //! Entry point which registers oml Signals functions with oml
    //! \param eval Evaluator interface
    //!
    SPECIALFUNCSOMLTBOX_DECLS int InitDll(EvaluatorInterface eval);
    //!
    //! Returns toolbox version
    //! \param eval Evaluator interface
    //!
    SPECIALFUNCSOMLTBOX_DECLS double GetToolboxVersion(EvaluatorInterface eval);
}

//!
//! Bessel function of the first kind
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlBesselj(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs);
//!
//! Bessel function of the second kind
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlBessely(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs);
//!
//! Modified Bessel function of the first kind
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlBesseli(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs);
//!
//! Modified Bessel function of the second kind
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlBesselk(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs);
//!
//! Hankel functions of the first and second kinds
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlBesselh(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs);

#endif // __SPECIALFUNCSTBOXFUNCS_OML_H__
