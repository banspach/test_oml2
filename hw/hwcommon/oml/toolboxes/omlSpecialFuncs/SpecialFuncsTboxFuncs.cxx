/**
* @file SignalsTboxFuncs.cxx
* @date January 2015
* Copyright (C) 2021-2022 Altair Engineering, Inc.
* 
* Commercial License Information: 
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/

#include "SpecialFuncsTboxFuncs.h"

#include <memory>  // For std::unique_ptr
#include <limits>

#include "hwMatrix.h"

#include "BuiltInFuncs.h"
#include "BuiltInFuncsMKL.h"
#include "OML_Error.h"

#define SPECFUNCS "SpecialFunctions"
#define TBOXVERSION 2022.2

#ifndef OS_WIN
    // Amos library name mangling
    #define CBESJ cbesj_
    #define CBESY cbesy_
    #define CBESI cbesi_
    #define CBESK cbesk_
    #define CBESH cbesh_
#endif

//------------------------------------------------------------------------------
// Entry point which registers oml Signals functions with oml
//------------------------------------------------------------------------------
int InitDll(EvaluatorInterface eval)
{
    eval.RegisterBuiltInFunction("besselj", OmlBesselj, FunctionMetaData(-3, 1, SPECFUNCS));
    eval.RegisterBuiltInFunction("bessely", OmlBessely, FunctionMetaData(-3, 1, SPECFUNCS));
    eval.RegisterBuiltInFunction("besseli", OmlBesseli, FunctionMetaData(-3, 1, SPECFUNCS));
    eval.RegisterBuiltInFunction("besselk", OmlBesselk, FunctionMetaData(-3, 1, SPECFUNCS));
    eval.RegisterBuiltInFunction("besselh", OmlBesselh, FunctionMetaData(-4, 1, SPECFUNCS));

    return 1;
}

// AMOS library external function prototypes
extern "C" void CBESJ(const hwComplex* Z, double* FNU, int* KODE, int* N, const hwComplex* CY, int* NZ, int* IERR);
extern "C" void CBESY(const hwComplex* Z, double* FNU, int* KODE, int* N, const hwComplex* CY, int* NZ, hwComplex* CWRK, int* IERR);
extern "C" void CBESI(const hwComplex* Z, double* FNU, int* KODE, int* N, const hwComplex* CY, int* NZ, int* IERR);
extern "C" void CBESK(const hwComplex* Z, double* FNU, int* KODE, int* N, const hwComplex* CY, int* NZ, int* IERR);
extern "C" void CBESH(const hwComplex* Z, double* FNU, int* KODE, int* M, int* N, const hwComplex* CY, int* NZ, int* IERR);

// Bessel/Hankel function calls to AMOS library with complex arguments
hwComplex BesselJ(double alpha, const hwComplex& z, int& ierr)
{
    double* fnu = &alpha;
    int kode = 1;
    int n = 1;
    hwComplex cy;
    int nz = -1;
    ierr = 0;

    if (alpha >= 0.0)
    {
        CBESJ(&z, fnu, &kode, &n, &cy, &nz, &ierr);

        if (z.Real() >= 0.0 && z.Imag() == 0.0)
            cy.Imag(0.0);
    }
    else if (IsInteger(alpha).IsOk())
    {
        alpha = -alpha;
        CBESJ(&z, fnu, &kode, &n, &cy, &nz, &ierr);

        if (static_cast<long> (alpha)%2 == 1)
        {
            cy = -cy;
        }
    }
    else if (isnan(alpha))
    {
        ierr = 1;
    }
    else
    {
        alpha = -alpha;
        hwComplex CWRK;
        CBESY(&z, fnu, &kode, &n, &cy, &nz, &CWRK, &ierr);

        if (ierr == 0 || ierr == 3)
        {
            hwComplex cj;
            CBESJ(&z, fnu, &kode, &n, &cj, &nz, &ierr);

            cy = cj * cos(PI * alpha) - cy * sin(PI * alpha);
        }
    }

    if (ierr != 0 && ierr != 3)
    {
        cy = std::numeric_limits<double>::quiet_NaN();
    }

    return cy;
}

hwComplex BesselY(double alpha, const hwComplex& z, int& ierr)
{
    double* fnu = &alpha;
    int kode = 1;
    int n = 1;
    hwComplex cy;
    int nz = -1;
    hwComplex CWRK;
    ierr = 0;

    if (alpha >= 0.0)
    {
        if (z == 0.0)
        {
            cy = std::numeric_limits<double>::infinity();
        }
        else
        {
            CBESY(&z, fnu, &kode, &n, &cy, &nz, &CWRK, &ierr);

            if (z.Real() >= 0.0 && z.Imag() == 0.0)
                cy.Imag(0.0);
        }
    }
    else if (IsInteger(alpha - 0.5).IsOk()) // half-integer order case
    {
        alpha = -alpha;
        CBESJ(&z, fnu, &kode, &n, &cy, &nz, &ierr);

        if (static_cast<long> (alpha - 0.5) % 2 == 1)
        {
            cy = -cy;
        }
    }
    else if (isnan(alpha))
    {
        ierr = 1;
    }
    else
    {
        alpha = -alpha;
        CBESY(&z, fnu, &kode, &n, &cy, &nz, &CWRK, &ierr);

        if (ierr == 0 || ierr == 3)
        {
            hwComplex cj;
            CBESJ(&z, fnu, &kode, &n, &cj, &nz, &ierr);

            cy = cy * cos(PI * alpha) + cj * sin(PI * alpha);
        }
    }

    if (ierr != 0 && ierr != 3)
    {
        cy = std::numeric_limits<double>::quiet_NaN();
    }

    return cy;
}

hwComplex BesselI(double alpha, const hwComplex& z, int& ierr)
{
    double* fnu = &alpha;
    int kode = 1;
    int n = 1;
    hwComplex cy;
    int nz = -1;
    ierr = -1;

    if (alpha >= 0.0)
    {
        CBESI(&z, fnu, &kode, &n, &cy, &nz, &ierr);

        if (z.Real() >= 0.0 && z.Imag() == 0.0)
            cy.Imag(0.0);
    }
    else if (isnan(alpha))
    {
        ierr = 1;
    }
    else
    {
        alpha = -alpha;
        CBESI(&z, fnu, &kode, &n, &cy, &nz, &ierr);

        if (ierr == 0 || ierr == 3)
        {
            hwComplex ck;
            CBESK(&z, fnu, &kode, &n, &ck, &nz, &ierr);

            cy += ck * ((2.0 / PI) * sin(PI * alpha));

        }
    }

    if (ierr != 0 && ierr != 3)
    {
        cy = std::numeric_limits<double>::quiet_NaN();
    }

    return cy;
}

hwComplex BesselK(double alpha, const hwComplex& z, int& ierr)
{
    double* fnu = &alpha;
    int kode = 1;
    int n = 1;
    hwComplex cy;
    int nz = -1;
    ierr = 0;

    if (alpha >= 0.0)
    {
        if (z == 0.0)
        {
            cy = std::numeric_limits<double>::infinity();
        }
        else
        {
            CBESK(&z, fnu, &kode, &n, &cy, &nz, &ierr);

            if (z.Real() >= 0.0 && z.Imag() == 0.0)
                cy.Imag(0.0);
        }
    }
    else if (isnan(alpha))
    {
        ierr = 1;
    }
    else
    {
        alpha = -alpha;
        CBESK(&z, fnu, &kode, &n, &cy, &nz, &ierr);
    }

    if (ierr != 0 && ierr != 3)
    {
        cy = std::numeric_limits<double>::quiet_NaN();
    }

    return cy;
}

hwComplex BesselH1(double alpha, const hwComplex& z, int& ierr)
{
    double* fnu = &alpha;
    int kode = 1;
    int m = 1;
    int n = 1;
    hwComplex cy;
    int nz = -1;
    ierr = -1;

    if (alpha >= 0.0)
    {
        CBESH(&z, fnu, &kode, &m, &n, &cy, &nz, &ierr);
    }
    else if (isnan(alpha))
    {
        ierr = 1;
    }
    else
    {
        alpha = -alpha;
        CBESH(&z, fnu, &kode, &m, &n, &cy, &nz, &ierr);
        cy *= hwComplex::exp(hwComplex(0.0, 1.0) * (PI * alpha));
    }

    if (ierr != 0 && ierr != 3)
    {
        cy = std::numeric_limits<double>::quiet_NaN();
    }

    return cy;
}

hwComplex BesselH2(double alpha, const hwComplex& z, int& ierr)
{
    double* fnu = &alpha;
    int kode = 1;
    int m = 2;
    int n = 1;
    hwComplex cy;
    int nz = -1;
    ierr = -1;

    if (alpha >= 0.0)
    {
        CBESH(&z, fnu, &kode, &m, &n, &cy, &nz, &ierr);
    }
    else if (isnan(alpha))
    {
        ierr = 1;
    }
    else
    {
        alpha = -alpha;
        CBESH(&z, fnu, &kode, &m, &n, &cy, &nz, &ierr);
        cy *= hwComplex::exp(hwComplex(0.0, -1.0) * (PI * alpha));
    }

    if (ierr != 0 && ierr != 3)
    {
        cy = std::numeric_limits<double>::quiet_NaN();
    }

    return cy;
}

// Bessel/Hankel function calls to AMOS library with real arguments
double BesselJ(double alpha, double x, int& ierr)
{
    hwComplex z(x, 0.0);
    hwComplex result = BesselJ(alpha, z, ierr);
    return result.Real();
}

double BesselY(double alpha, double x, int& ierr)
{
    hwComplex z(x, 0.0);
    hwComplex result = BesselY(alpha, z, ierr);
    return result.Real();
}

double BesselI(double alpha, double x, int& ierr)
{
    hwComplex z(x, 0.0);
    hwComplex result = BesselI(alpha, z, ierr);
    return result.Real();
}

double BesselK(double alpha, double x, int& ierr)
{
    hwComplex z(x, 0.0);
    hwComplex result = BesselK(alpha, z, ierr);
    return result.Real();
}

hwComplex BesselH1(double alpha, double x, int& ierr)
{
    hwComplex z(x, 0.0);
    return BesselH1(alpha, z, ierr);
}

hwComplex BesselH2(double alpha, double x, int& ierr)
{
    hwComplex z(x, 0.0);
    return BesselH2(alpha, z, ierr);
}

//------------------------------------------------------------------------------
// Bessel function template
//------------------------------------------------------------------------------
template<double (*BFR)(double, double, int&),
         hwComplex (*BFC)(double, const hwComplex&, int&)>
void BesselTemplate(EvaluatorInterface           eval,
                    const std::vector<Currency>& inputs,
                    std::vector<Currency>& outputs)
{
    size_t nargin = inputs.size();

    if (nargin != 2 && nargin != 3)
    {
        throw OML_Error(OML_ERR_NUMARGIN);
    }

    const Currency& cur1 = inputs[0];
    const Currency& cur2 = inputs[1];
    int Ierr;

    if (cur1.IsScalar())
    {
        double v = cur1.Scalar();

        if (cur2.IsScalar())
        {
            double x = cur2.Scalar();
            double result = BFR(v, x, Ierr);
            outputs.push_back(result);
            outputs.push_back(Ierr);
        }
        else if (cur2.IsComplex())
        {
            hwComplex x = cur2.Complex();
            hwComplex result = BFC(v, x, Ierr);
            outputs.push_back(result);
            outputs.push_back(Ierr);
        }
        else if (cur2.IsMatrix())
        {
            const hwMatrix* x = cur2.Matrix();
            int m = x->M();
            int n = x->N();
            int size = m * n;

            std::unique_ptr<hwMatrix> result(EvaluatorInterface::allocateMatrix(m, n, x->IsReal()));
            std::unique_ptr<hwMatrix> ierr(EvaluatorInterface::allocateMatrix(m, n, true));
            ierr->SetElements(0.0);

            if (x->IsReal())
            {
                for (int i = 0; i < size; ++i)
                {
                    (*result)(i) = BFR(v, (*x)(i), Ierr);
                    (*ierr)(i) = static_cast<double> (Ierr);
                }
            }
            else
            {
                for (int i = 0; i < size; ++i)
                {
                    result->z(i) = BFC(v, x->z(i), Ierr);
                    (*ierr)(i) = static_cast<double> (Ierr);
                }
            }

            outputs.push_back(result.release());
            outputs.push_back(ierr.release());
        }
        else
        {
            throw OML_Error(OML_ERR_SCALARCOMPLEXMTX, 2, OML_VAR_TYPE);
        }
    }
    else if (cur1.IsMatrix())
    {
        const hwMatrix* v = cur1.Matrix();

        if (!v->IsReal())
        {
            throw OML_Error(OML_ERR_REAL, 1, OML_VAR_TYPE);
        }

        if (cur2.IsScalar() || cur2.IsComplex() || cur2.IsMatrix())
        {
            const hwMatrix* x = cur2.ConvertToMatrix();
            int m = v->M();
            int n = v->N();

            if (sameSize(v, x))
            {
                int size = m * n;

                std::unique_ptr<hwMatrix> result(EvaluatorInterface::allocateMatrix(m, n, x->IsReal()));
                std::unique_ptr<hwMatrix> ierr(EvaluatorInterface::allocateMatrix(m, n, true));
                ierr->SetElements(0.0);

                if (x->IsReal())
                {
                    for (int i = 0; i < size; ++i)
                    {
                        (*result)(i) = BFR((*v)(i), (*x)(i), Ierr);
                        (*ierr)(i) = static_cast<double> (Ierr);
                    }
                }
                else
                {
                    for (int i = 0; i < size; ++i)
                    {
                        result->z(i) = BFC((*v)(i), x->z(i), Ierr);
                        (*ierr)(i) = static_cast<double> (Ierr);
                    }
                }

                outputs.push_back(result.release());
                outputs.push_back(ierr.release());
            }
            else if ((m == 1 && n == 1) || (x->M() == 1 && x->N() == 1) || (m == 1 && x->N() == 1))
            {
                hwMatrix* ev = nullptr;
                hwMatrix* ex = nullptr;
                Currency::ExpandMatrixPair(*v, *x, ev, ex);

                if (!sameSize(ev, ex))
                {
                    delete ev;
                    delete ex;
                    throw OML_Error(OML_ERR_ARRAYSIZE, 1, 2);
                }

                int m = ev->M();
                int n = ev->N();
                int size = m * n;

                std::unique_ptr<hwMatrix> result(EvaluatorInterface::allocateMatrix(m, n, x->IsReal()));
                std::unique_ptr<hwMatrix> ierr(EvaluatorInterface::allocateMatrix(m, n, true));
                ierr->SetElements(0.0);

                if (x->IsReal())
                {
                    for (int i = 0; i < size; ++i)
                    {
                        (*result)(i) = BFR((*ev)(i), (*ex)(i), Ierr);
                        (*ierr)(i) = static_cast<double> (Ierr);
                    }
                }
                else
                {
                    for (int i = 0; i < size; ++i)
                    {
                        result->z(i) = BFC((*ev)(i), ex->z(i), Ierr);
                        (*ierr)(i) = static_cast<double> (Ierr);
                    }
                }

                delete ev;
                delete ex;
                outputs.push_back(result.release());
                outputs.push_back(ierr.release());
            }
            else
            {
                throw OML_Error(OML_ERR_ARRAYSIZE, 1, 2);
            }
        }
        else
        {
            throw OML_Error(OML_ERR_SCALARCOMPLEXMTX, 2, OML_VAR_TYPE);
        }
    }
    else
    {
        throw OML_Error(OML_ERR_SCALARMATRIX, 1, OML_VAR_TYPE);
    }
}

//------------------------------------------------------------------------------
// Bessel function of the first kind
//------------------------------------------------------------------------------
bool OmlBesselj(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs)
{
    // BesselTemplate<boost::math::cyl_bessel_j, BesselJ>(eval, inputs, outputs);
    BesselTemplate<BesselJ, BesselJ>(eval, inputs, outputs);

    size_t nargin = inputs.size();

    if (nargin == 3)
    {
        if (inputs[2].IsScalar())
        {
            if (inputs[2].Scalar() != 1.0)
            {
                throw OML_Error(OML_ERR_OPTION, 3);
            }
        }
        else
        {
            throw OML_Error(OML_ERR_OPTION, 3);
        }

        // multiply result by exp(-abs(imag(inputs[1])))
        Currency result = outputs[0];

        std::vector<Currency> inputs2;
        inputs2.push_back(inputs[1]);
        outputs.clear();
        oml_imag(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(outputs[0]);
        outputs.clear();
        BuiltInFuncsMKL::Abs(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(outputs[0]);
        outputs.clear();
        oml_uminus(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(outputs[0]);
        outputs.clear();
        BuiltInFuncsMKL::Exp(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(result);
        inputs2.push_back(outputs[0]);
        outputs.clear();
        oml_times(eval, inputs2, outputs);
    }

    return true;
}
//------------------------------------------------------------------------------
// Bessel function of the second kind
//------------------------------------------------------------------------------
bool OmlBessely(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs)
{
    // BesselTemplate<boost::math::cyl_neumann, BesselY>(eval, inputs, outputs);
    BesselTemplate<BesselY, BesselY>(eval, inputs, outputs);

    size_t nargin = inputs.size();

    if (nargin == 3)
    {
        if (inputs[2].IsScalar())
        {
            if (inputs[2].Scalar() != 1.0)
            {
                throw OML_Error(OML_ERR_OPTION, 3);
            }
        }
        else
        {
            throw OML_Error(OML_ERR_OPTION, 3);
        }

        // multiply result by exp(-abs(imag(inputs[1])))
        Currency result = outputs[0];

        std::vector<Currency> inputs2;
        inputs2.push_back(inputs[1]);
        outputs.clear();
        oml_imag(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(outputs[0]);
        outputs.clear();
        BuiltInFuncsMKL::Abs(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(outputs[0]);
        outputs.clear();
        oml_uminus(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(outputs[0]);
        outputs.clear();
        BuiltInFuncsMKL::Exp(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(result);
        inputs2.push_back(outputs[0]);
        outputs.clear();
        oml_times(eval, inputs2, outputs);
    }

    return true;
}
//------------------------------------------------------------------------------
// Modified Bessel function of the first kind
//------------------------------------------------------------------------------
bool OmlBesseli(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs)
{
    // BesselTemplate<boost::math::cyl_bessel_i, BesselI>(eval, inputs, outputs);
    BesselTemplate<BesselI, BesselI>(eval, inputs, outputs);

    size_t nargin = inputs.size();

    if (nargin == 3)
    {
        if (inputs[2].IsScalar())
        {
            if (inputs[2].Scalar() != 1.0)
            {
                throw OML_Error(OML_ERR_OPTION, 3);
            }
        }
        else
        {
            throw OML_Error(OML_ERR_OPTION, 3);
        }

        // multiply result by exp(-abs(real(inputs[1])))
        Currency result = outputs[0];

        std::vector<Currency> inputs2;
        inputs2.push_back(inputs[1]);
        outputs.clear();
        oml_real(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(outputs[0]);
        outputs.clear();
        BuiltInFuncsMKL::Abs(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(outputs[0]);
        outputs.clear();
        oml_uminus(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(outputs[0]);
        outputs.clear();
        BuiltInFuncsMKL::Exp(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(result);
        inputs2.push_back(outputs[0]);
        outputs.clear();
        oml_times(eval, inputs2, outputs);
    }

    return true;
}
//------------------------------------------------------------------------------
// Modified Bessel function of the second kind
//------------------------------------------------------------------------------
bool OmlBesselk(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs)
{
    // BesselTemplate<boost::math::cyl_bessel_k, BesselK>(eval, inputs, outputs);
    BesselTemplate<BesselK, BesselK>(eval, inputs, outputs);

    size_t nargin = inputs.size();

    if (nargin == 3)
    {
        if (inputs[2].IsScalar())
        {
            if (inputs[2].Scalar() != 1.0)
            {
                throw OML_Error(OML_ERR_OPTION, 3);
            }
        }
        else
        {
            throw OML_Error(OML_ERR_OPTION, 3);
        }

        // multiply result by exp(inputs[1])
        Currency result = outputs[0];

        std::vector<Currency> inputs2;
        inputs2.push_back(inputs[1]);
        outputs.clear();
        BuiltInFuncsMKL::Exp(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(result);
        inputs2.push_back(outputs[0]);
        outputs.clear();
        oml_times(eval, inputs2, outputs);
    }

    return true;
}
//------------------------------------------------------------------------------
// Hankel function template
//------------------------------------------------------------------------------
template<hwComplex (*BFR)(double, double, int&),
         hwComplex (*BFC)(double, const hwComplex&, int&)>
void HankelTemplate(EvaluatorInterface           eval,
                    const std::vector<Currency>& inputs,
                    std::vector<Currency>&       outputs)
{
    const Currency& cur1 = inputs[0];
    const Currency& cur3 = inputs[2];
    int Ierr;

    if (cur1.IsScalar())
    {
        double v = cur1.Scalar();

        if (cur3.IsScalar())
        {
            double x = cur3.Scalar();
            hwComplex result = BFR(v, x, Ierr);
            outputs.push_back(result);
            outputs.push_back(Ierr);
        }
        else if (cur3.IsComplex())
        {
            hwComplex x = cur3.Complex();
            hwComplex result = BFC(v, x, Ierr);
            outputs.push_back(result);
            outputs.push_back(Ierr);
        }
        else if (cur3.IsMatrix())
        {
            const hwMatrix* x = cur3.Matrix();
            int m = x->M();
            int n = x->N();
            int size = m * n;

            std::unique_ptr<hwMatrix> result(EvaluatorInterface::allocateMatrix(m, n, false));
            std::unique_ptr<hwMatrix> ierr(EvaluatorInterface::allocateMatrix(m, n, true));
            ierr->SetElements(0.0);

            if (x->IsReal())
            {
                for (int i = 0; i < size; ++i)
                {
                    result->z(i) = BFR(v, (*x)(i), Ierr);
                    (*ierr)(i) = static_cast<double> (Ierr);
                }
            }
            else
            {
                for (int i = 0; i < size; ++i)
                {
                    result->z(i) = BFC(v, x->z(i), Ierr);
                    (*ierr)(i) = static_cast<double> (Ierr);
                }
            }

            outputs.push_back(result.release());
            outputs.push_back(ierr.release());
        }
        else
        {
            throw OML_Error(OML_ERR_SCALARCOMPLEXMTX, 3, OML_VAR_TYPE);
        }
    }
    else if (cur1.IsMatrix())
    {
        const hwMatrix* v = cur1.Matrix();

        if (!v->IsReal())
        {
            throw OML_Error(OML_ERR_REAL, 1, OML_VAR_TYPE);
        }

        if (cur3.IsScalar() || cur3.IsComplex() || cur3.IsMatrix())
        {
            const hwMatrix* x = cur3.ConvertToMatrix();
            int m = v->M();
            int n = v->N();

            if (sameSize(v, x))
            {
                int size = m * n;

                std::unique_ptr<hwMatrix> result(EvaluatorInterface::allocateMatrix(m, n, false));
                std::unique_ptr<hwMatrix> ierr(EvaluatorInterface::allocateMatrix(m, n, true));
                ierr->SetElements(0.0);

                if (x->IsReal())
                {
                    for (int i = 0; i < size; ++i)
                    {
                        result->z(i) = BFR((*v)(i), (*x)(i), Ierr);
                        (*ierr)(i) = static_cast<double> (Ierr);
                    }
                }
                else
                {
                    for (int i = 0; i < size; ++i)
                    {
                        result->z(i) = BFC((*v)(i), x->z(i), Ierr);
                        (*ierr)(i) = static_cast<double> (Ierr);
                    }
                }

                outputs.push_back(result.release());
                outputs.push_back(ierr.release());
            }
            else if ((m == 1 && n == 1) || (x->M() == 1 && x->N() == 1) || (m == 1 && x->N() == 1))
            {
                hwMatrix* ev = nullptr;
                hwMatrix* ex = nullptr;
                Currency::ExpandMatrixPair(*v, *x, ev, ex);

                if (!sameSize(ev, ex))
                {
                    delete ev;
                    delete ex;
                    throw OML_Error(OML_ERR_ARRAYSIZE, 1, 2);
                }

                int m = ev->M();
                int n = ev->N();
                int size = m * n;

                std::unique_ptr<hwMatrix> result(EvaluatorInterface::allocateMatrix(m, n, false));
                std::unique_ptr<hwMatrix> ierr(EvaluatorInterface::allocateMatrix(m, n, true));
                ierr->SetElements(0.0);

                if (x->IsReal())
                {
                    for (int i = 0; i < size; ++i)
                    {
                        result->z(i) = BFR((*ev)(i), (*ex)(i), Ierr);
                        (*ierr)(i) = static_cast<double> (Ierr);
                    }
                }
                else
                {
                    for (int i = 0; i < size; ++i)
                    {
                        result->z(i) = BFC((*ev)(i), ex->z(i), Ierr);
                        (*ierr)(i) = static_cast<double> (Ierr);
                    }
                }

                delete ev;
                delete ex;
                outputs.push_back(result.release());
                outputs.push_back(ierr.release());
            }
            else
            {
                throw OML_Error(OML_ERR_ARRAYSIZE, 1, 3);
            }
        }
        else
        {
            throw OML_Error(OML_ERR_SCALARCOMPLEXMTX, 3, OML_VAR_TYPE);
        }
    }
    else
    {
        throw OML_Error(OML_ERR_SCALARMATRIX, 1, OML_VAR_TYPE);
    }
}
//------------------------------------------------------------------------------
// Hankel functions of the first and second kinds
//------------------------------------------------------------------------------
bool OmlBesselh(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs)
{
    size_t nargin = inputs.size();

    if (nargin < 2 || nargin > 4)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[1].IsInteger())
        throw OML_Error(OML_ERR_INTEGER, 2, OML_VAR_PARAMETER);

    int k = static_cast<int> (inputs[1].Scalar());

    if (k == 1)
    {
        // HankelTemplate<boost::math::cyl_hankel_1, BesselH1>(eval, inputs, outputs);
        HankelTemplate<BesselH1, BesselH1>(eval, inputs, outputs);
    }
    else if (k == 2)
    {
        // HankelTemplate<boost::math::cyl_hankel_2, BesselH2>(eval, inputs, outputs);
        HankelTemplate<BesselH2, BesselH2>(eval, inputs, outputs);
    }
    else
    {
        throw OML_Error(OML_ERR_OPTION, 2);
    }

    if (nargin == 4)
    {
        if (inputs[3].IsScalar())
        {
            if (inputs[3].Scalar() != 1.0)
            {
                throw OML_Error(OML_ERR_OPTION, 4);
            }
        }
        else
        {
            throw OML_Error(OML_ERR_OPTION, 4);
        }

        // when k = 1 multiply result by exp(-i * inputs[2])
        // when k = 2 multiply result by exp( i * inputs[2])
        Currency result = outputs[0];
        outputs.clear();

        std::vector<Currency> inputs2;

        if (k == 1)
        {
            inputs2.push_back(hwComplex(0.0, -1.0));
        }
        else
        {
            inputs2.push_back(hwComplex(0.0, 1.0));
        }

        inputs2.push_back(inputs[2]);
        oml_times(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(outputs[0]);
        outputs.clear();
        BuiltInFuncsMKL::Exp(eval, inputs2, outputs);
        inputs2.clear();

        inputs2.push_back(result);
        inputs2.push_back(outputs[0]);
        outputs.clear();
        oml_times(eval, inputs2, outputs);
    }

    return true;
}
//------------------------------------------------------------------------------
// Returns toolbox version
//------------------------------------------------------------------------------
double GetToolboxVersion(EvaluatorInterface eval)
{
    return TBOXVERSION;
}
