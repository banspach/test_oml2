/**
* @file SpecialFuncsTboxDefs.h
* @date December 2021
* Copyright (C) 2021-2022 Altair Engineering, Inc.  
* 
* Commercial License Information: 
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/

#ifndef __SPECIALFUNCSTBOXDEFS_OML_H__
#define __SPECIALFUNCSTBOXDEFS_OML_H__

//------------------------------------------------------------------------------
//!
//! \brief Contains macro definitions for exporting functions in the library
//!
//------------------------------------------------------------------------------
#ifdef OS_WIN
  #ifdef SPECIALFUNCSOMLTBOX_EXPORT
    #undef  SPECIALFUNCSOMLTBOX_DECLS
    #define SPECIALFUNCSOMLTBOX_DECLS __declspec(dllexport)
  #else
    #undef  SPECIALFUNCSOMLTBOX_DECLS
    #define SPECIALFUNCSOMLTBOX_DECLS __declspec(dllimport)
  #endif  // SPECIALFUNCSOMLTBOX_EXPORT
#else
  #undef  SPECIALFUNCSOMLTBOX_DECLS
  #define SPECIALFUNCSOMLTBOX_DECLS
#endif // OS_WIN

#endif // __SPECIALFUNCSTBOXDEFS_OML_H__
