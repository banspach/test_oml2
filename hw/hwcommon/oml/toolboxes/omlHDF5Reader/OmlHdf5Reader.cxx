/**
* @file OmlHdf5Reader.cxx
* @date June 2020
* Copyright (C) 2020 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include "OmlHdf5Reader.h"
#include "OML_Error.h"
#include "Currency.h"
#include "BuiltInFuncsUtils.h"
#include "StructData.h"
#include "hwMatrix_NMKL.h"
#include "hwMatrixN_NMKL.h"
//ToDo: Remove following once HDF5 C++ api available to retrieve H5O_info_t
//start
#include "hdf5.h"
//end

#define MAX_SUPPORTED_DIMS 7
#define OML_COMPLEX_ATTRIBUTE "omlcomplex"
OmlHdf5Reader* OmlHdf5Reader::_instance = NULL;

OmlHdf5Reader* OmlHdf5Reader::GetInstance()
{
    if (!_instance)
    {
        _instance = new OmlHdf5Reader();
        H5::Exception::dontPrint();
    }
    return _instance;
}

void OmlHdf5Reader::ReleaseInstance()
{
    delete _instance;
    _instance = NULL;
}

//! Constructor
OmlHdf5Reader::OmlHdf5Reader():_file_id(-1), _is_complex(false)
{
}

//! Destructor
OmlHdf5Reader::~OmlHdf5Reader()
{
}

Currency OmlHdf5Reader::ReadToc(const std::string& file_name,
                                const std::string& group_path,
                                const bool&        read_all)
{
    
    FileManager file_manager(this, file_name);
    Currency out;
    try
    {
        GROUPTRAVELINFO group_addr_local;
        out = ReadToc(group_path, read_all, group_addr_local);
    }
    catch (H5::GroupIException&)
    {
        throw OML_Error(OML_ERR_HDF5_GROUP_READ_FAILED);
    }
    catch (H5::FileIException&)
    {
        throw OML_Error(OML_ERR_HDF5_FILE_READ_FAILED);
    }
    catch (OML_Error& err)
    {
        throw err;
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_FILE_READ_FAILED);
    }

    return out;
}

//private function
Currency OmlHdf5Reader::ReadToc(const std::string& group_path,
                                const bool&        read_all,
                                GROUPTRAVELINFO&   group_addr)
{
    try
    {
        if (H5O_TYPE_GROUP != _file.childObjType(group_path))
        {
            throw OML_Error(OML_ERR_HDF5_INVALID_GROUP);
        }
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_INVALID_GROUP);
    }

    H5::Group group     = _file.openGroup(group_path);
    hsize_t   num_objs  = group.getNumObjs();
    int       num_colum = 2;
    int       num_rows  = static_cast<int>(num_objs);

    if (read_all)
        num_colum = 3;

    HML_CELLARRAY* cell = EvaluatorInterface::allocateCellArray(num_rows,
                                                                num_colum);
    Currency out(cell);

    std::string rout_group = "";

    if ("/" != group_path)
        rout_group = group_path;

    for (hsize_t index = 0; index < num_objs; index++)
    {
        int row_index = static_cast<int>(index);
        std::string child_name = group.getObjnameByIdx(index);
        std::string child_path = rout_group + "/" + child_name;
        (*cell)(row_index, 1)  = child_path;
        switch (group.childObjType(index))
        {
        case H5O_TYPE_GROUP:
        {
            (*cell)(row_index, 0) = "Group";
            if (read_all)
            {
                //ToDo: Remove following once HDF5 C++ api available to retrieve H5O_info_t
                //start
                hid_t group_id = H5Gopen(_file_id, child_path.c_str(), H5P_DEFAULT);
                H5O_info_t obj_info;
                H5Oget_info(group_id, &obj_info);
                H5Gclose(group_id);
                //end
                GROUPTRAVELINFO::iterator gadd_it = group_addr.find(obj_info.addr);
                bool is_not_traversed = (group_addr.end() == gadd_it);

                if (is_not_traversed)
                {
                    group_addr.emplace(obj_info.addr, std::unordered_set<std::string>({ child_name }));
                }
                else
                {
                    if (gadd_it->second.end() == gadd_it->second.find(child_name))
                    {
                        is_not_traversed = true;
                        gadd_it->second.insert(child_name);
                    }
                }

                if (is_not_traversed)
                {
                    GROUPTRAVELINFO group_addr_local;
                    (*cell)(row_index, 2) = ReadToc(child_path,
                                                    read_all,
                                                    group_addr_local);
                }
            }
            break;
        }
        case H5O_TYPE_DATASET:
        {
            (*cell)(row_index, 0) = "Dataset";

            if (3 == num_colum)
                (*cell)(row_index, 2) = "";

            break;
        }
        case H5O_TYPE_NAMED_DATATYPE:
        {
            (*cell)(row_index, 0) = "Datatype";
            if (3 == num_colum)
                (*cell)(row_index, 2) = "";
            break;
        }
        default:
        {
            (*cell)(row_index, 0) = "Unknown";
            (*cell)(row_index, 1) = "";
            if (3 == num_colum)
                (*cell)(row_index, 2) = "";
            break;
        }
        }
    }
    return out;
}

/*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
//start
Currency OmlHdf5Reader::RowMajorToColumnMajor(NDMATRIXPTR ndmat_ptr)
{
    hwMatrixN* ndmat = ndmat_ptr.get();    
    const std::vector<int>& dims = ndmat->Dimensions();
    int num_dims = static_cast<int>(dims.size());
    std::vector<int> permute_vect;
    for (int index = num_dims; index > 0; index--)
    {
        permute_vect.push_back(index - 1);
    }

    NDMATRIXPTR result(EvaluatorInterface::allocateMatrixN());
    result->Permute(*ndmat, permute_vect);
    return result.release();

}
//end

Currency OmlHdf5Reader::ReadString(unsigned char*          data,
                                   const hssize_t&         num_of_elements,
                                   const std::vector<int>& dims,
                                   const size_t&           sz)
{
    BuiltInFuncsUtils utils;
    if (1 == num_of_elements)
    {
        std::string s;
        char* data_ptr = reinterpret_cast<char*>(data);
        if (nullptr != data_ptr)
        {
            size_t len = strlen(data_ptr);
            s.append(data_ptr, (len > sz ? sz : len));
        }

        return s;
    }

    std::unique_ptr<HML_ND_CELLARRAY> ndcell(new HML_ND_CELLARRAY);
    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
    //start        
    std::vector<int> rev_dims = dims;
    std::reverse(rev_dims.begin(), rev_dims.end());
    ndcell->Dimension(rev_dims, HML_ND_CELLARRAY::REAL);
    //end
    /*ToDo: After VSM-7079 implementatoin, uncomment below line*/
    //ndcell->Dimension(dims, HML_ND_CELLARRAY::REAL);    
    unsigned char* start_data = data;
    for (int j = 0; j < num_of_elements; j++)
    {
        std::string s;
        char* data_ptr = reinterpret_cast<char*>(start_data);
        if (nullptr != data_ptr)
        {
            size_t len = strlen(data_ptr);
            s.append(data_ptr, (len > sz ? sz : len));
            
        }
        start_data += sz;
        (*(ndcell.get()))(j) = s;

    }
    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
    //start
    if (dims.size() > 1)
    {
         std::vector<int> permute_vect;
         for (int index = static_cast<int>(dims.size()); index > 0; index--)
         {
             permute_vect.push_back(index - 1);
         }

         std::unique_ptr<HML_ND_CELLARRAY> result(new HML_ND_CELLARRAY);
         result->Permute(*(ndcell.get()), permute_vect);
         //result->Reshape(dims);
         return result.release();
    }
    //end
    return ndcell.release();
}

Currency OmlHdf5Reader::ReadVarLenString(unsigned char*      data[],
                                       const hssize_t&         num_of_elements,
                                       const std::vector<int>& dims)
{
    BuiltInFuncsUtils utils;
    if (1 == num_of_elements)
    {
        std::string s;
        char* data_ptr = reinterpret_cast<char*>(data[0]);
        if (nullptr != data_ptr)
        {
            s.append(data_ptr);
        }

        return s;
    }

    std::unique_ptr<HML_ND_CELLARRAY> ndcell(new HML_ND_CELLARRAY);
    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
    //start        
    std::vector<int> rev_dims = dims;
    std::reverse(rev_dims.begin(), rev_dims.end());
    ndcell->Dimension(rev_dims, HML_ND_CELLARRAY::REAL);
    //end
    /*ToDo: After VSM-7079 implementatoin, uncomment below line*/
    //ndcell->Dimension(dims, HML_ND_CELLARRAY::REAL);    
    for (int j = 0; j < num_of_elements; j++)
    {
        std::string s;
        char* data_ptr = reinterpret_cast<char*>(data[j]);
        if (nullptr != data_ptr)
        {
            s.append(data_ptr);
        }
        (*(ndcell.get()))(j) = s;

    }
    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
    //start
    if (dims.size() > 1)
    {
        std::vector<int> permute_vect;
        for (int index = static_cast<int>(dims.size()); index > 0; index--)
        {
            permute_vect.push_back(index - 1);
        }

        std::unique_ptr<HML_ND_CELLARRAY> result(new HML_ND_CELLARRAY);
        result->Permute(*(ndcell.get()), permute_vect);
        //result->Reshape(dims);
        return result.release();
    }
    //end
    return ndcell.release();
}

Currency OmlHdf5Reader::ReadArrays(DOUBLEDATAPTR data,
                                   const std::vector<int>& dspace_dims,
                                   int   dspace_element_count, 
                                   const std::vector<int>& arr_dims,
                                   int   arr_element_count)
{
    std::unique_ptr<HML_ND_CELLARRAY> ndcell(new HML_ND_CELLARRAY);
    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
    //start        
    std::vector<int> rev_dims = dspace_dims;
    std::reverse(rev_dims.begin(), rev_dims.end());
    ndcell->Dimension(rev_dims, HML_ND_CELLARRAY::REAL);
    //end
    /*ToDo: After VSM-7079 implementatoin, uncomment below line*/
    //ndcell->Dimension(dims, HML_ND_CELLARRAY::REAL);    
    std::vector<int> rev_arr_dims = arr_dims;
    if (arr_dims.size() > 1)
        /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
        //start
        std::reverse(rev_arr_dims.begin(), rev_arr_dims.end());
        //end
    else
        rev_arr_dims.push_back(1); //ND matrix need atleast two dimensions

    int data_buf_index = 0;
    const double* raw_data = data.get();
    
    for (int j = 0; j < dspace_element_count; j++)
    {
        double* mat_data = new double[arr_element_count];
        NDMATRIXPTR ndmat = NDMATRIXPTR(new hwMatrixN(rev_arr_dims, mat_data, hwMatrixN::REAL));
        ndmat->OwnData(true);
        for (int k = 0; k < arr_element_count; k++)
            mat_data[k] = raw_data[data_buf_index++];

        if (arr_dims.size() > 1)
            (*(ndcell.get()))(j) = RowMajorToColumnMajor(std::move(ndmat));
        else
            (*(ndcell.get()))(j) = ndmat.release();
    }

    data.reset();
    raw_data = nullptr;
    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
    //start
    if (dspace_dims.size() > 1)
    {
        std::vector<int> permute_vect;
        for (int index = static_cast<int>(dspace_dims.size()); index > 0; index--)
        {
            permute_vect.push_back(index - 1);
        }

        std::unique_ptr<HML_ND_CELLARRAY> result(new HML_ND_CELLARRAY);
        result->Permute(*(ndcell.get()), permute_vect);
        //result->Reshape(dims);
        return result.release();
    }
    //end
    return ndcell.release();
}

Currency OmlHdf5Reader::ReadDataType(EvaluatorInterface&     eval,
                                     const H5::DataType*     dtype,
                                     const hssize_t&         num_of_elements,
                                     const std::vector<int>& dims,
                                     const ATTRIBUTEPTR&     attr,
                                     std::vector<std::string>& compound_names,
                                     const DATASETPTR&       dset,
                                     const H5::DataSpace&    memdspace,
                                     const H5::DataSpace&    dspace,
                                     const bool              is_parent_compound,
                                     const std::string       child_name,
                                     const H5::CompType*     parent_cmp_type)
{

    if ((nullptr == dset) && (nullptr == attr))
    {
        OML_Error("Error: Implementation error");
    }

    switch (dtype->getClass())
    {
        case H5T_INTEGER:
        case H5T_FLOAT:
        {
            H5::DataType mem_dtype(H5::PredType::NATIVE_DOUBLE);
            size_t  sz = mem_dtype.getSize();

            if (dtype->getSize() > sz)
                throw OML_Error(OML_ERR_HDF5_UNSUPPORT_DATA);


            std::vector<int> rev_dims = dims;
            if (dims.size() > 1)
                /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
                //start
                std::reverse(rev_dims.begin(), rev_dims.end()); 
                //end
            else
                rev_dims.push_back(1); //ND matrix need atleast two dimensions

            double* data_d = new double[num_of_elements];
            NDMATRIXPTR ndmat = NDMATRIXPTR(new hwMatrixN(rev_dims, data_d, hwMatrixN::REAL));
            ndmat->OwnData(true);
            //Currency lout(ndmat);
            /*ToDo: Need not worry about byte order for now, since only native types are supported.
            Create memory data type to pass to read functions, when support for other data types are included*/
            if (is_parent_compound)
            {
                size_t depth = compound_names.size();

                H5::CompType mem_cmp_dtype(sz);
                mem_cmp_dtype.insertMember(compound_names[depth - 1], 0, mem_dtype);
                
                if (depth > 1)
                {
                    for (size_t index = depth-1; index > 0; index--)
                    {
                        H5::CompType temp_mem_cmp_dtype(sz);
                        temp_mem_cmp_dtype.insertMember(compound_names[index - 1], 0, mem_cmp_dtype);
                        mem_cmp_dtype = temp_mem_cmp_dtype;
                    }
                }
                
                mem_dtype = mem_cmp_dtype;
            }

            if (nullptr != dset)
            {
                dset->read(data_d, mem_dtype, memdspace, dspace);
            }
            else
            {
                attr->read(mem_dtype, data_d);
            }

            if (dims.size() > 1)
                return RowMajorToColumnMajor(std::move(ndmat));
            else
                return ndmat.release();

        }
        case H5T_REFERENCE:
        {
            H5::DataType mem_dtype(H5::PredType::STD_REF_OBJ);
            if (mem_dtype == *dtype)
            {
                size_t  sz = mem_dtype.getSize();

                if (dtype->getSize() > sz)
                    throw OML_Error(OML_ERR_HDF5_UNSUPPORT_DATA);


                std::vector<int> rev_dims = dims;
                if (dims.size() > 1)
                    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
                    //start
                    std::reverse(rev_dims.begin(), rev_dims.end());
                //end
                else
                    rev_dims.push_back(1); //ND matrix need atleast two dimensions

                if (is_parent_compound)
                {
                    size_t depth = compound_names.size();

                    H5::CompType mem_cmp_dtype(sz);
                    mem_cmp_dtype.insertMember(compound_names[depth - 1], 0, mem_dtype);

                    if (depth > 1)
                    {
                        for (size_t index = depth - 1; index > 0; index--)
                        {
                            H5::CompType temp_mem_cmp_dtype(sz);
                            temp_mem_cmp_dtype.insertMember(compound_names[index - 1], 0, mem_cmp_dtype);
                            mem_cmp_dtype = temp_mem_cmp_dtype;
                        }
                    }

                    mem_dtype = mem_cmp_dtype;
                }

                REFDATAPTR ref_buf = REFDATAPTR(new hobj_ref_t[num_of_elements]);
                hobj_ref_t* raw_ref_buf = ref_buf.get();
                if (nullptr != dset)
                {
                    dset->read(raw_ref_buf, mem_dtype, memdspace, dspace);
                }
                else
                {
                    attr->read(mem_dtype, raw_ref_buf);
                }

                double* data_d = new double[num_of_elements];
                NDMATRIXPTR ndmat = NDMATRIXPTR(new hwMatrixN(rev_dims, data_d, hwMatrixN::REAL));
                ndmat->OwnData(true);

                for (hssize_t i = 0; i < num_of_elements; i++)
                {
                    data_d[i] = static_cast<double>(raw_ref_buf[i]);
                }

                if (dims.size() > 1)
                    return RowMajorToColumnMajor(std::move(ndmat));
                else
                    return ndmat.release();
            }

            //ToDo: support reading H5::PredType::STD_REF_DSETREG after test hdf5 file available
            //type of data is hdset_reg_ref_t
            break;
        }
        case H5T_STRING:
        {
            if (H5T_STRING == dtype->getClass())
            {
                H5::StrType str_dtype;
                if (is_parent_compound)
                {
                    str_dtype = parent_cmp_type->getMemberStrType(parent_cmp_type->getMemberIndex(child_name));
                }
                else
                {
                    if (nullptr == attr)
                        str_dtype = (dset.get())->getStrType();
                    else
                        str_dtype = (attr.get())->getStrType();
                }

                if (H5T_STR_NULLTERM != str_dtype.getStrpad())
                    return Currency("");
            }
            
            H5::DataType mem_dtype = *dtype;
            size_t  sz = dtype->getSize();

            if (is_parent_compound)
            {
                H5::CompType mem_cmp_dtype(sz);
                mem_cmp_dtype.insertMember(child_name, 0, mem_dtype);
                mem_dtype = mem_cmp_dtype;
            }

            if (dtype->isVariableStr())
            {
                VARSTRDATAPTR data = VARSTRDATAPTR(new unsigned char*[num_of_elements * sizeof(char*)]);
                /*ToDo: Need not worry about byte order for now, since only native types are supported.
                Create memory data type to pass to read functions, when support for other data types are included*/
                if (nullptr != dset)
                {
                    dset->read(data.get(), mem_dtype, memdspace, dspace);
                }
                else
                {
                    attr->read(mem_dtype, data.get());
                }
                return ReadVarLenString(data.get(), num_of_elements, dims);
            }
            else
            {
                DATAPTR data = DATAPTR(new unsigned char[num_of_elements * sz]);
                /*ToDo: Need not worry about byte order for now, since only native types are supported.
                Create memory data type to pass to read functions, when support for other data types are included*/
                if (nullptr != dset)
                {
                    dset->read(data.get(), mem_dtype, memdspace, dspace);
                }
                else
                {
                    attr->read(mem_dtype, data.get());
                }

                return ReadString(data.get(), num_of_elements, dims, sz);
            }
        }
        case H5T_COMPOUND:
        {
            StructData*  sd = new StructData;
            H5::CompType comp_dtype;

            if (is_parent_compound)
            {
                comp_dtype = parent_cmp_type->getMemberCompType(parent_cmp_type->getMemberIndex(child_name));
            }
            else
            {
                if (nullptr != dset)
                {

                    comp_dtype = dset->getCompType();
                }
                else
                {
                    comp_dtype = attr->getCompType();
                }
            }
            
            int number_of_children = comp_dtype.getNmembers();

            Currency out(sd);
            
            for (int i = 0; i < number_of_children; i++)
            {
                std::string  temp_child_name = comp_dtype.getMemberName(i);
                compound_names.push_back(temp_child_name);
                H5::DataType temp_child_type = comp_dtype.getMemberDataType(i);
                sd->SetValue(0, 0, temp_child_name, ReadDataType(eval, &temp_child_type, num_of_elements,
                                                            dims, attr, compound_names, dset,
                                                            memdspace, dspace, true, temp_child_name, &comp_dtype));
                compound_names.pop_back();
            }
            
            if (_is_complex)
            {
                const std::map<std::string, int>& fields = sd->GetFieldNames();
                if (sd->Contains("Real") && sd->Contains("Imag") && (2 == fields.size()))
                {
                    const Currency& real = sd->GetValue(0, 0, "Real");
                    const Currency& imag = sd->GetValue(0, 0, "Imag");

                    if (real.IsScalar() && imag.IsScalar())
                    {
                        out = hwComplex(real.Scalar(), imag.Scalar());
                    }
                    else if (real.IsMatrix() && imag.IsMatrix())
                    {
                        hwMatrix* mat = EvaluatorInterface::allocateMatrix();
                        mat->Dimension(real.Matrix()->M(), real.Matrix()->N(), hwMatrix::COMPLEX);
                        mat->PackComplex(*(real.Matrix()), (imag.Matrix()));
                        out = mat;
                    }
                    else if (real.IsNDMatrix() && imag.IsNDMatrix())
                    {
                        hwMatrixN* matN = EvaluatorInterface::allocateMatrixN();
                        matN->Dimension(real.MatrixN()->Dimensions(), hwMatrixN::COMPLEX);
                        matN->PackComplex(*(real.MatrixN()), (imag.MatrixN()));
                        out = matN;
                    }
                }
                else
                {
                    BuiltInFuncsUtils::SetWarning(eval, "Warning: Dataset is marked as complex but does not conform to the complex data requirement");
                }
            }

            return out;
        }
        case H5T_ARRAY:
        {
            if ((dtype->detectClass(H5T_INTEGER) || dtype->detectClass(H5T_FLOAT)))
            {
                int num_array_dims = 0;
                std::vector<hsize_t> arr_dims; 
                if (is_parent_compound)
                {
                    H5::ArrayType arr_type = parent_cmp_type->getMemberArrayType(parent_cmp_type->getMemberIndex(child_name));
                    num_array_dims = arr_type.getArrayNDims();
                    arr_dims.resize(num_array_dims);
                    arr_type.getArrayDims(arr_dims.data());
                }
                else
                {
                    if (nullptr != dset)
                    {
                        H5::ArrayType arr_type = dset->getArrayType();
                        num_array_dims = arr_type.getArrayNDims();
                        arr_dims.resize(num_array_dims);
                        arr_type.getArrayDims(arr_dims.data());
                    }
                    else
                    {
                        //ToDo: on Linux, getArrayType() failed, recheck after HDF5 update to latest versions
                        //VSM-7572
                        H5::ArrayType arr_type(dtype->getId());
                        num_array_dims = arr_type.getArrayNDims();
                        arr_dims.resize(num_array_dims);
                        arr_type.getArrayDims(arr_dims.data());
                    }
                }

                std::vector<hsize_t>::iterator it = arr_dims.begin();
                std::vector<int> arr_mat_dims;

                int arr_ele_count = 1;

                for (; it != arr_dims.end(); it++)
                {
                    int dim_size = static_cast<int>(*it);
                    arr_mat_dims.push_back(dim_size);
                    arr_ele_count *= dim_size;
                }

                H5::ArrayType mem_arr_type(H5::PredType::NATIVE_DOUBLE, num_array_dims, arr_dims.data());

                H5::DataType mem_dt;
                if (is_parent_compound)
                {
                    H5::CompType mem_cmp_dtype(mem_arr_type.getSize());
                    mem_cmp_dtype.insertMember(child_name, 0, mem_arr_type);
                    mem_dt = mem_cmp_dtype;
                }
                else
                {
                    mem_dt = mem_arr_type;
                }

                DOUBLEDATAPTR data_d = DOUBLEDATAPTR(new double[num_of_elements * arr_ele_count]);

                if (nullptr != dset)
                {
                    dset->read(data_d.get(), mem_dt, memdspace, dspace);
                }
                else
                {
                    attr->read(mem_dt, data_d.get());
                }

                return ReadArrays(std::move(data_d), dims, static_cast<int>(num_of_elements),
                                 arr_mat_dims, arr_ele_count);
            }

            break;
        }
        case H5T_TIME:        
        case H5T_ENUM:
        case H5T_VLEN:
        case H5T_BITFIELD:
        case H5T_OPAQUE:
        default:
        {
            break;
        }
    }

    return Currency();
}

Currency OmlHdf5Reader::ReadDataset(EvaluatorInterface& eval, 
                                    const std::string& file_name,
                                    const std::string& dataset_path,
                                    const hwMatrix*    points)
{
    FileManager file_manager(this, file_name);
    try
    {
        if (H5O_TYPE_DATASET != _file.childObjType(dataset_path))
        {
            throw OML_Error(OML_ERR_HDF5_INVALID_DATASET);
        }
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_INVALID_DATASET);
    }

    try
    {
        DATASETPTR    dset      = DATASETPTR(new H5::DataSet(_file.openDataSet(dataset_path)));
        H5::DataType  dtype     = dset->getDataType();
        H5::DataSpace dspace    = dset->getSpace();
        hssize_t      ele_count = 1;
        int           ndims     = dspace.getSimpleExtentNdims();
        H5::DataSpace mdspace(dspace); /*memory data space*/

        if (nullptr == points)
        {
            if (ndims > MAX_SUPPORTED_DIMS)
                throw OML_Error(OML_ERR_HDF5_UNSUPPORTDIM);
        }
        else
        {
            dspace.selectNone();
            int column_count = points->N(); /*should match number of dimentions*/
            if (ndims != column_count)
                throw OML_Error(OML_ERR_HDF5_POINTS_MATRIXDIM_INVALID);

            int      rows_count  = points->M();
            int      coord_index = 0;
            std::unique_ptr<hsize_t[]> coords(new hsize_t[rows_count * column_count]);

            for (int row_index = 0; row_index < rows_count; row_index++)
            {
                for (int col_index = 0; col_index < column_count; col_index++)
                {
                    int point_index = static_cast<int>((*points)(row_index, col_index));

                    (coords.get())[coord_index] = point_index-1;
                    coord_index++;
                }
            }

            dspace.selectElements(H5S_SELECT_SET, rows_count,coords.get());

            if (!dspace.selectValid())
                throw OML_Error(OML_ERR_HDF5_POINTS_SELECTION_INVALID);

        }
        
        HML_CELLARRAY* cell = EvaluatorInterface::allocateCellArray(1, 1);
        Currency out(cell);

        std::vector<int> dims;
        if (nullptr == points)
        {
            //to read scalar,where ndims are returning as zero by dspace
            if (0 == ndims)
            {
                ndims     = 1;
                ele_count = 1;
                dims.push_back(1);
            }                
            else
            {
                std::vector<hsize_t> idims(ndims);
                dspace.getSimpleExtentDims(idims.data());
                std::vector<hsize_t>::iterator it = idims.begin();
                for (; it != idims.end(); it++)
                {
                    int dim_size = static_cast<int>(*it);
                    dims.push_back(dim_size);
                    ele_count *= dim_size;
                }
            }
        }
        else
        {
            ele_count = dspace.getSelectElemNpoints();
            hsize_t dim1 = ele_count;
            hsize_t dim2 = 1;
            hsize_t dimsm[2] = {dim1, dim2};            
            dims.push_back(static_cast<int>(ele_count));
            dims.push_back(1);
            H5::DataSpace temp_dapace(2, dimsm);
            mdspace = temp_dapace;
        }
        std::vector<std::string> comp_field_names;

        try
        {
            dset->openAttribute(OML_COMPLEX_ATTRIBUTE);
            _is_complex = true;
        }
        catch (...)
        {
            //do Nothing
        }

        (*cell)(0, 0) = ReadDataType(eval, &dtype, ele_count, dims, nullptr, comp_field_names, dset, mdspace, dspace);
        return out;
    }
    catch (H5::FileIException&)
    {
        throw OML_Error(OML_ERR_HDF5_FILE_READ_FAILED);
    }
    catch (H5::GroupIException&)
    {
        throw OML_Error(OML_ERR_HDF5_GROUP_READ_FAILED);
    }
    catch (H5::DataSetIException&)
    {
        throw OML_Error(OML_ERR_HDF5_DATASET_READ_FAILED);
    }
    catch (H5::DataTypeIException&)
    {
        throw OML_Error(OML_ERR_HDF5_DATATYPE_READ_FAILED);
    }
    catch (H5::DataSpaceIException&)
    {
        throw OML_Error(OML_ERR_HDF5_DATASPACE_READ_FAILED);
    }
    catch (OML_Error& err)
    {
        throw err;
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_DATASET_READ_FAILED);
    }
    return Currency();
}


Currency OmlHdf5Reader::ReadAttributes(EvaluatorInterface& eval, 
                                       const std::string& file_name,
                                       const std::string& location_path)
{
    FileManager file_manager(this, file_name);
    bool is_dataset = false;
    bool is_group  = false;
    bool is_file  = location_path.empty();
    try
    {
        if (!is_file)
        {
            is_dataset = (H5O_TYPE_DATASET == _file.childObjType(location_path));
            is_group = (H5O_TYPE_GROUP == _file.childObjType(location_path));
            
            if (!(is_dataset || is_group))
            {
                throw OML_Error(OML_ERR_HDF5_NEITHER_DATASET_NOR_GROUP);
            }
        }

        DATASETPTR   dset     = nullptr;
        GROUPPTR     group    = nullptr;        
        int          num_attr = 0;
        StructData*  sd       = new StructData;
        Currency     out(sd);

        if (is_file)
        {
            num_attr = _file.getNumAttrs();
        }
        else if (is_dataset)
        {
            dset     = DATASETPTR(new H5::DataSet(_file.openDataSet(location_path)));
            num_attr = dset->getNumAttrs();
        }
        else if (is_group)
        {
            group     = GROUPPTR(new H5::Group(_file.openGroup(location_path)));
            num_attr  = group->getNumAttrs();
        }

        ATTRIBUTEPTR attr = nullptr;
        for (int idx = 0; idx < num_attr; idx++)
        {
            if (is_file)
            {
                attr = ATTRIBUTEPTR(new H5::Attribute(_file.openAttribute(idx)));
            }
            else if (is_dataset)
            {
                attr = ATTRIBUTEPTR(new H5::Attribute(dset->openAttribute(idx)));
            }
            else if (is_group)
            {
                attr = ATTRIBUTEPTR(new H5::Attribute(group->openAttribute(idx)));
            }

            H5::DataSpace dspace = attr->getSpace();
            int           ndims = dspace.getSimpleExtentNdims();
            hssize_t      ele_count = 1;

            if (ndims > MAX_SUPPORTED_DIMS)
                throw OML_Error(OML_ERR_HDF5_UNSUPPORTDIM);
                                   
            std::vector<int> dims;            
            if (0 == ndims)
            {
                ndims = 1;
                ele_count = 1;
                dims.push_back(1);
            }
            else
            {
                std::vector<hsize_t> idims(ndims);
                dspace.getSimpleExtentDims(idims.data());
                std::vector<hsize_t>::iterator it = idims.begin();
                for (; it != idims.end(); it++)
                {
                    int dim_size = static_cast<int>(*it);
                    dims.push_back(dim_size);
                    ele_count *= dim_size;
                }
            }
            H5::DataType temp_dt = attr->getDataType();
            std::vector<std::string> comp_field_names;
            sd->SetValue(0, 0, attr->getName(), ReadDataType(eval, &temp_dt, ele_count, dims, attr, comp_field_names));
        }
        return out;
    }
    catch (H5::FileIException&)
    {
        throw OML_Error(OML_ERR_HDF5_FILE_READ_FAILED);
    }
    catch (H5::GroupIException&)
    {
        throw OML_Error(OML_ERR_HDF5_GROUP_READ_FAILED);
    }
    catch (H5::DataSetIException&)
    {
        throw OML_Error(OML_ERR_HDF5_DATASET_READ_FAILED);
    }
    catch (H5::DataTypeIException&)
    {
        throw OML_Error(OML_ERR_HDF5_DATATYPE_READ_FAILED);
    }
    catch (H5::DataSpaceIException&)
    {
        throw OML_Error(OML_ERR_HDF5_DATASPACE_READ_FAILED);
    }
    catch (H5::AttributeIException&)
    {
        throw OML_Error(OML_ERR_HDF5_ATTRIBUTE_READ_FAILED);
    }
    catch (OML_Error& err)
    {
        throw err;
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_ATTRIBUTE_READ_FAILED);
    }

    return Currency();
}

void OmlHdf5Reader::OpenFile(const std::string& file_name)
{
    BuiltInFuncsUtils utils;
    if (!utils.DoesPathExist(file_name))
    {
        throw OML_Error(OML_ERR_FILE_NOTFOUND, 1);
    }

#ifdef OS_WIN
    if (utils.HasWideChars(file_name))
    {
        throw OML_Error(OML_ERR_UNICODE_FILENAME);
    }
#endif

    try
    {
        if (!H5::H5File::isHdf5(file_name))
            throw OML_Error(OML_ERR_HDF5_INVALID_FILE, 1);

        _file.openFile(file_name, H5F_ACC_RDONLY);
        //Remove following once HDF5 C++ api available to retrieve H5O_info_t
        //start
        _file_id = H5Fopen(file_name.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
        //end
        _is_complex = false;
    }
    catch (H5::FileIException&)
    {
        throw OML_Error(OML_ERR_HDF5_FILE_READ_FAILED);
    }
}

Currency OmlHdf5Reader::DumpHDF5DataType(const std::string& file_name)
{
    FileManager file_manager(this, file_name);
    Currency out;
    try
    {
        GROUPTRAVELINFO group_addr_local;
        const std::string group = "/";
        out = ReadDataTypeDump(group, group_addr_local);
    }
    catch (H5::GroupIException&)
    {
        throw OML_Error(OML_ERR_HDF5_GROUP_READ_FAILED);
    }
    catch (H5::FileIException&)
    {
        throw OML_Error(OML_ERR_HDF5_FILE_READ_FAILED);
    }
    catch (OML_Error & err)
    {
        throw err;
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_FILE_READ_FAILED);
    }

    return out;
}

//private function
Currency OmlHdf5Reader::GetDataTypeToDump(const std::string& child_path)
{
    DATASETPTR    dset = DATASETPTR(new H5::DataSet(_file.openDataSet(child_path)));
    H5::DataType  dtype = dset->getDataType();
    
    switch (dtype.getClass())
    {
        case H5T_INTEGER:
        {
            return Currency("H5T_INTEGER");
        }
        case H5T_FLOAT:
        {
            return Currency("H5T_FLOAT");
        }
        case H5T_STRING:
        {
            return Currency("H5T_STRING");
        }
        case H5T_TIME:
        {
            return Currency("H5T_TIME NOT_SUPPORTED");
        }
        case H5T_REFERENCE:
        {
            return Currency("H5T_REFERENCE NOT_SUPPORTED");
        }
        case H5T_ENUM:
        {
            return Currency("H5T_ENUM NOT_SUPPORTED");
        }
        case H5T_VLEN:
        {
            return Currency("H5T_VLEN NOT_SUPPORTED");
        }
        case H5T_ARRAY:
        {
            return Currency("H5T_ARRAY NOT_SUPPORTED");
        }
        case H5T_BITFIELD:
        {
            return Currency("H5T_BITFIELD NOT_SUPPORTED");
        }
        case H5T_OPAQUE:
        {
            return Currency("H5T_OPAQUE NOT_SUPPORTED");
        }
        case H5T_COMPOUND:
        {
            H5::CompType comp_dtype = dset->getCompType();
            int number_of_children = comp_dtype.getNmembers();
            HML_CELLARRAY* cell = EvaluatorInterface::allocateCellArray(number_of_children, 2);
            Currency out(cell);
            
            for (int i = 0; i < number_of_children; i++)
            {
                (*cell)(i, 0) = comp_dtype.getMemberName(i);
                Currency child_info;

                H5::DataType child_type = comp_dtype.getMemberDataType(i);

                switch (child_type.getClass())
                {
                    case H5T_INTEGER:
                    {
                        child_info = Currency("H5T_INTEGER");
                        break;
                    }
                    case H5T_FLOAT:
                    {
                        child_info =  Currency("H5T_FLOAT");
                        break;
                    }
                    case H5T_STRING:
                    {
                        child_info = Currency("H5T_STRING");
                        break;
                    }
                    case H5T_TIME:
                    {
                        child_info = Currency("H5T_TIME NOT_SUPPORTED");
                        break;
                    }
                    case H5T_REFERENCE:
                    {
                        child_info = Currency("H5T_REFERENCE NOT_SUPPORTED");
                        break;
                    }
                    case H5T_ENUM:
                    {
                        child_info = Currency("H5T_ENUM NOT_SUPPORTED");
                        break;
                    }
                    case H5T_VLEN:
                    {
                        child_info = Currency("H5T_VLEN NOT_SUPPORTED");
                        break;
                    }
                    case H5T_ARRAY:
                    {
                        if (child_type.detectClass(H5T_INTEGER))
                        {
                            child_info = Currency("array of H5T_INTEGER");
                            break;
                        }
                        else if (child_type.detectClass(H5T_FLOAT))
                        {
                            child_info = Currency("array of H5T_FLOAT");
                            break;
                        }
                        else if (child_type.detectClass(H5T_STRING))
                        {
                            child_info = Currency("array of H5T_STRING NOT_SUPPORTED");
                            break;
                        }
                        else
                        {
                            child_info = Currency("array of ??? NOT_SUPPORTED");
                            break;
                        }
                    }
                    case H5T_BITFIELD:
                    {
                        child_info = Currency("H5T_BITFIELD NOT_SUPPORTED");
                        break;
                    }
                    case H5T_OPAQUE:
                    {
                        child_info = Currency("H5T_OPAQUE NOT_SUPPORTED");
                        break;
                    }
                    case H5T_COMPOUND:
                    {
                        child_info = Currency("NESTED H5T_COMPOUND NOT_SUPPORTED");
                        break;
                    }
                    default:
                    {
                        child_info = Currency("defaultType NOT_SUPPORTED");
                        break;
                    }
                }

                (*cell)(i, 1) = child_info;
            }
            return out;
        }
        default:
        {
            return Currency("defaultType NOT_SUPPORTED");
        }
    }
}
//private function
Currency OmlHdf5Reader::ReadDataTypeDump(const std::string & group_path, GROUPTRAVELINFO & group_addr)
{
    try
    {
        if (H5O_TYPE_GROUP != _file.childObjType(group_path))
        {
            throw OML_Error(OML_ERR_HDF5_INVALID_GROUP);
        }
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_INVALID_GROUP);
    }

    H5::Group group = _file.openGroup(group_path);
    hsize_t   num_objs = group.getNumObjs();
    int       num_colum = 3;
    int       num_rows = static_cast<int>(num_objs);

    HML_CELLARRAY* cell = EvaluatorInterface::allocateCellArray(num_rows, num_colum);
    Currency out(cell);

    std::string rout_group = "";

    if ("/" != group_path)
        rout_group = group_path;

    int num_of_datasets = 0;
    for (hsize_t index = 0; index < num_objs; index++)
    {
        int row_index = static_cast<int>(index);
        std::string child_name = group.getObjnameByIdx(index);
        std::string child_path = rout_group + "/" + child_name;
        (*cell)(row_index, 1) = child_path;
        switch (group.childObjType(index))
        {
        case H5O_TYPE_GROUP:
        {
            (*cell)(row_index, 0) = "Group";
            //ToDo: Remove following once HDF5 C++ api available to retrieve H5O_info_t
            //start
            hid_t group_id = H5Gopen(_file_id, child_path.c_str(), H5P_DEFAULT);
            H5O_info_t obj_info;
            H5Oget_info(group_id, &obj_info);
            H5Gclose(group_id);
            //end
            GROUPTRAVELINFO::iterator gadd_it = group_addr.find(obj_info.addr);
            bool is_not_traversed = (group_addr.end() == gadd_it);

            if (is_not_traversed)
            {
                group_addr.emplace(obj_info.addr, std::unordered_set<std::string>({ child_name }));
            }
            else
            {
                if (gadd_it->second.end() == gadd_it->second.find(child_name))
                {
                    is_not_traversed = true;
                    gadd_it->second.insert(child_name);
                }
            }

            if (is_not_traversed)
            {
                GROUPTRAVELINFO group_addr_local;
                (*cell)(row_index, 2) = ReadDataTypeDump(child_path, group_addr_local);
            }

            break;
        }
        case H5O_TYPE_DATASET:
        {
            (*cell)(row_index, 0) = "Dataset";
            (*cell)(row_index, 2) = GetDataTypeToDump(child_path);

            break;
        }
        case H5O_TYPE_NAMED_DATATYPE:
        {
            (*cell)(row_index, 0) = "Datatype";
            (*cell)(row_index, 2) = "H5O_TYPE_NAMED_DATATYPE NOT_SUPPORTED";
            break;
        }
        default:
        {
            (*cell)(row_index, 0) = "Unknown";
            (*cell)(row_index, 2) = "default NOT_SUPPORTED";
            break;
        }
        }
    }
    return out;
}

void OmlHdf5Reader::CloseAll()
{
    _file.close();

    //ToDo: Remove following once HDF5 C++ api available to retrieve H5O_info_t
    //start
    if (-1 != _file_id)
    {
        H5Fclose(_file_id);
        _file_id = -1;
    }
    //end
    _is_complex = false;
}
