/**
* @file OmlHdf5ReaderTbox.h
* @date June 2020
* Copyright (C) 2020 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/
#ifndef __OMLHDF5READERTBOX_H__
#define __OMLHDF5READERTBOX_H__

#pragma warning(disable: 4251)

#include "OmlHdf5ReaderDefs.h"
#include "EvaluatorInt.h"

//------------------------------------------------------------------------------
//!
//! \brief oml hdf5 reader functions
//!
//------------------------------------------------------------------------------

extern "C"
{
    //!
    //! Entry point which registers hdf5 reader functions with oml
    //! \param eval Evaluator interface
    //!
    OMLHDF5READER_DECLS int InitDll(EvaluatorInterface eval);
    //!
    //! Returns toolbox version
    //! \param eval Evaluator interface
    //!
    OMLHDF5READER_DECLS double GetToolboxVersion(EvaluatorInterface eval);
}

//!
//! Reads hdf5 file structure [readhdf5toc command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlReadHdf5Toc(EvaluatorInterface           eval,
                    const std::vector<Currency>& inputs,
                    std::vector<Currency>&       outputs);

//!
//! Reads hdf5 data [readhdf5 command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlReadHdf5(EvaluatorInterface           eval,
                 const std::vector<Currency>& inputs,
                 std::vector<Currency>&       outputs);
//!
//! Reads hdf5 data [readhdf5attributes command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlReadhdf5attributes(EvaluatorInterface           eval,
                           const std::vector<Currency>& inputs,
                           std::vector<Currency>& outputs);


bool OmlDumpHDF5DataType(EvaluatorInterface           eval,
                        const std::vector<Currency>&  inputs,
                        std::vector<Currency>&        outputs);
#endif // __OMLHDF5READERTBOX_H__
