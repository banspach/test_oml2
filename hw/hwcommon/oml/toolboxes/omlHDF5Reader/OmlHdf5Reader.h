/**
* @file OmlHdf5Reader.h
* @date June 2020
* Copyright (C) 2020 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#ifndef OML_HDF5_READER_H__
#define OML_HDF5_READER_H__

#pragma warning(disable: 4251)

#include "unordered_set"
#include "unordered_map"
#include "H5Cpp.h"
#include "OmlHdf5ReaderDefs.h"
#include "EvaluatorInt.h"

typedef std::unique_ptr<unsigned char[]>  DATAPTR;
typedef std::unique_ptr<unsigned char*[]> VARSTRDATAPTR;
typedef std::unique_ptr<H5::DataSet>      DATASETPTR;
typedef std::unique_ptr<H5::Attribute>    ATTRIBUTEPTR;
typedef std::unique_ptr<H5::Group>        GROUPPTR;
typedef std::unique_ptr<hwMatrixN>        NDMATRIXPTR;
typedef std::unique_ptr<double[]>         DOUBLEDATAPTR;
typedef std::unique_ptr<hobj_ref_t[]>     REFDATAPTR;
typedef std::unordered_map<haddr_t, std::unordered_set<std::string> > GROUPTRAVELINFO;

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//! Class for providing data reading from hdf5 file
//-------------------------------------------------------------------------------------
class OMLHDF5READER_DECLS OmlHdf5Reader
{

public:
    static OmlHdf5Reader* GetInstance();
    static void ReleaseInstance();
    //!
    //! Reads hdf5 meta data [handles readhdf5toc command]
    //! \param file_name   HDF5 file name
    //! \param group_path  HDF5 group path
    //! \param read_all    reads all levels starting from group path if true else only one level
    //!    
    Currency ReadToc(const std::string& file_name,
                     const std::string& group_path,
                     const bool&        read_all);
    //!
    //! Reads hdf5 data [handles readhdf5 command]
    //! \param eval         EvaluatorInterface
    //! \param file_name    HDF5 file name
    //! \param dataset_path HDF5 dataset path
    //! \param points       Reads points mentioned by n*m matrix
    //!                     n is number of points, m is nuber of dimentions
    //! 
    Currency ReadDataset(EvaluatorInterface& eval, 
                         const std::string& file_name,
                         const std::string& dataset_path,
                         const hwMatrix*    points);
    //!
    //! Reads hdf5 data [handles readhdf5 command]
    //! \param eval         EvaluatorInterface
    //! \param file_name    HDF5 file name
    //! \param dataset_path HDF5 location (group/dataset) path
    //! 
    Currency ReadAttributes(EvaluatorInterface& eval, 
                            const std::string& file_name,
                            const std::string& location_path);

    Currency DumpHDF5DataType(const std::string& file_name);

    class FileManager {
    public:
        FileManager(OmlHdf5Reader* reader, const std::string& file_name):_reader(reader)
        {
            _reader->OpenFile(file_name);
        }

        ~FileManager()
        {
            _reader->CloseAll();
        }
    private:
        OmlHdf5Reader* _reader;
    };
private:
    /*Over loaded function*/
    Currency ReadToc(const std::string& group_path,
                     const bool&        read_all,
                     GROUPTRAVELINFO&   group_addr);

    Currency ReadDataType(EvaluatorInterface&     eval, 
                          const H5::DataType*     dtype,
                          const hssize_t&         num_of_elements,
                          const std::vector<int>& dims,
                          const ATTRIBUTEPTR&     attr,
                          std::vector<std::string>& compound_names,
                          const DATASETPTR&       dset      = nullptr,
                          const H5::DataSpace&    memdspace = H5::DataSpace::ALL,
                          const H5::DataSpace&    dspace    = H5::DataSpace::ALL,
                          const bool              is_parent_compound = false,
                          const std::string       child_name = "",
                          const H5::CompType*     parent_cmp_type = nullptr);

    Currency RowMajorToColumnMajor(NDMATRIXPTR ndmat_ptr);

    Currency ReadString(unsigned char*          data,
                        const hssize_t&         num_of_elements,
                        const std::vector<int>& dims,
                        const size_t&           sz);

    Currency ReadVarLenString(unsigned char*          data[],
                              const hssize_t&         num_of_elements,
                              const std::vector<int>& dims);

    Currency ReadArrays(DOUBLEDATAPTR data,
                        const std::vector<int>& dspace_dims,
                        int   dspace_element_count,
                        const std::vector<int>& arr_dims,
                        int   arr_element_count);

    void OpenFile(const std::string& file_name);
    void CloseAll();
    
    Currency GetDataTypeToDump(const std::string& child_path);
    Currency ReadDataTypeDump(const std::string& group_path, GROUPTRAVELINFO& group_addr);

    //! Constructor
    OmlHdf5Reader();
    //! Copy Constructor
    OmlHdf5Reader(const OmlHdf5Reader&)            = delete;
    //! Assignment Operator
    OmlHdf5Reader& operator=(const OmlHdf5Reader&) = delete;
    //! Destructor
    ~OmlHdf5Reader();

    static OmlHdf5Reader* _instance;
    H5::H5File            _file;
    //Remove following once find the
    hid_t                 _file_id;
    bool                  _is_complex;
};

#endif //OML_HDF5_READER_H__