/**
* @file OmlHdf5ReaderDefs.h
* @date June 2020
* Copyright (C) 2020 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#ifndef OMLHDF5READERDEFS_H
#define OMLHDF5READERDEFS_H

#ifdef OS_WIN
#   ifdef OMLHDF5READER_EXPORTS
#      undef  OMLHDF5READER_DECLS
#      define OMLHDF5READER_DECLS __declspec(dllexport)
#   else
#      undef  OMLHDF5READER_DECLS
#      define OMLHDF5READER_DECLS __declspec(dllimport)
#   endif  // OMLHDF5READER_EXPORTS
#else
#   undef  OMLHDF5READER_DECLS
#   define OMLHDF5READER_DECLS
#endif // OS_WIN

#ifndef NULL
#define NULL 0
#endif

#endif
