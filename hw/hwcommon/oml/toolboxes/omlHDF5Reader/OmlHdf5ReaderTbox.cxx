/**
* @file OmlHdf5ReaderTbox.cxx
* @date June 2020
* Copyright (C) 2020-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include "OmlHdf5ReaderTbox.h"

#include <cassert>

#include "OmlHdf5Reader.h"

#include "OML_Error.h"
#include "BuiltInFuncsCore.h"
#include "BuiltInFuncsUtils.h"
#include "hwMatrix_NMKL.h"

#define TBOXVERSION 2022.2

// Licensing
typedef bool(*LICFUNCPTR)();
bool g_licInitialized         = false;           // True if licensing is done
bool g_businessLibsAuthorized = false;           // True if business library is authorized
void OmlHdf5ReaderCheckFunc(const std::string&); // Throws error if not business edition

//------------------------------------------------------------------------------
// Dll entry point
//------------------------------------------------------------------------------
int InitDll(EvaluatorInterface eval)
{
    // Note: When adding new functions, the first line should call
    // OmlHdf5ReaderCheckFunc as this is a professional library
    eval.RegisterBuiltInFunction("readhdf5toc", OmlReadHdf5Toc, FunctionMetaData(-3, 1, "HDF5Reader"));
    eval.RegisterBuiltInFunction("readhdf5", OmlReadHdf5, FunctionMetaData(-3, 1, "HDF5Reader"));
    eval.RegisterBuiltInFunction("readhdf5attributes", OmlReadhdf5attributes, FunctionMetaData(-2, 1, "HDF5Reader"));
    
    eval.RegisterBuiltInFunction("dumphdf5datatype", OmlDumpHDF5DataType, FunctionMetaData(1, 1, "HDF5Reader"));
    eval.LockBuiltInFunction("dumphdf5datatype");

    return 0;
}
//------------------------------------------------------------------------------
// Gets toolbox version
//------------------------------------------------------------------------------
double GetToolboxVersion(EvaluatorInterface eval)
{
    return TBOXVERSION;
}
//------------------------------------------------------------------------------
// Implements [readhdf5toc]
//------------------------------------------------------------------------------
bool OmlReadHdf5Toc(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5ReaderCheckFunc("readhdf5toc");  // Check license

    int nargin = eval.GetNarginValue();

    if (nargin < 1 || nargin > 3)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (nargin > 1)
        if (!inputs[1].IsString())
            throw OML_Error(OML_ERR_STRING, 2);

    if (3 == nargin)
        if (!inputs[2].IsLogical())
            throw OML_Error(OML_ERR_LOGICAL, 3);


    const std::string& file_name  = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    std::string group_path = "/";
    bool        read_all   = false;

    if (nargin > 1)
        group_path = inputs[1].StringVal();

    if (3 == nargin)
        read_all = inputs[2].Scalar();

    OmlHdf5Reader* ohr = OmlHdf5Reader::GetInstance();
    outputs.push_back(ohr->ReadToc(file_name, group_path, read_all));
    return  true;
}
//------------------------------------------------------------------------------
// Implements [readhdf5]
//------------------------------------------------------------------------------
bool OmlReadHdf5(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5ReaderCheckFunc("readhdf5");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 2)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[1].IsString())
        throw OML_Error(OML_ERR_STRING, 2);

    const std::string& file_name    = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& dataset_path = inputs[1].StringVal();
    const hwMatrix* mat          = nullptr;
    hwMatrix*       temp_mat     = nullptr;
    if (3 == nargin)
    {
        if (inputs[2].IsMatrix())
        {
            if (!inputs[2].IsPositiveIntegralMatrix())
                throw OML_Error(OML_ERR_POSINTMATRIX,3);

            mat = inputs[2].Matrix();
        }
        else if (inputs[2].IsScalar())
        {
            temp_mat = EvaluatorInterface::allocateMatrix(1, 1, true);
            (*temp_mat)(0, 0) = inputs[2].Scalar();
            mat = temp_mat;
        }
        else
        {
            throw OML_Error(OML_ERR_MATRIX, 3);
        }
    }
    Currency out(temp_mat);
    OmlHdf5Reader* ohr = OmlHdf5Reader::GetInstance();
    outputs.push_back(ohr->ReadDataset(eval, file_name, dataset_path, mat));

    return  true;
}
//------------------------------------------------------------------------------
// Implements [readhdf5attributes]
//------------------------------------------------------------------------------
bool OmlReadhdf5attributes(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5ReaderCheckFunc("readhdf5attributes");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 1 || nargin > 2)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    const std::string& file_name = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    std::string location_path = "";
    if (nargin == 2)
    {
        if (!inputs[1].IsString())
            throw OML_Error(OML_ERR_STRING, 2);

        location_path = inputs[1].StringVal();
    }        

    OmlHdf5Reader* ohr = OmlHdf5Reader::GetInstance();
    outputs.push_back(ohr->ReadAttributes(eval, file_name, location_path));

    return  true;
}
//------------------------------------------------------------------------------
// Implements [dumphdf5datatype]
//------------------------------------------------------------------------------
bool OmlDumpHDF5DataType(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5ReaderCheckFunc("dumphdf5datatype");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin != 1)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    std::string file_name = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());

    OmlHdf5Reader* ohr = OmlHdf5Reader::GetInstance();
    outputs.push_back(ohr->DumpHDF5DataType(file_name));

    return  true;
}
//------------------------------------------------------------------------------
// Throws error for functions if not running in business edition mode
//------------------------------------------------------------------------------
void OmlHdf5ReaderCheckFunc(const std::string& func)
{
    if (g_businessLibsAuthorized)
    {
        return;
    }
    
    if (!g_licInitialized)
    {
        // Attempt to check out a Compose license without the user credentials
        std::string dllname("hwcomposelic");
#ifndef OS_WIN
        dllname = "lib" + dllname + ".so";
#endif
        void* handle = BuiltInFuncsCore::DyLoadLibrary(dllname);
        if (!handle)
        {
            throw OML_Error("Cannot load [" + dllname + "]");
        }
        void* symbol = BuiltInFuncsCore::DyGetFunction(handle,
            "AuthorizeBusinessEditionLibrary");
        if (!symbol)
        {
            throw OML_Error("Unable to authorize [" + func + "]");
        }
        g_licInitialized = true;
        LICFUNCPTR funcptr = (LICFUNCPTR)(symbol);
        if (!funcptr)
        {
            throw OML_Error("Invalid authorization function");
        }
        g_businessLibsAuthorized = funcptr();
    }

    if (!g_businessLibsAuthorized)
    {
        throw OML_Error(
            "Invalid command [" + func + "]; available only in Compose Business Edition");
    }
}