#include "OMLInterfacePublic.h"
#include "ipclib/ext_eval.h"
#include "ipclib/altair_ipc.h"
#include <string.h>

extern "C"
{
#ifdef OS_WIN
	__declspec(dllexport) int InitToolbox(OMLInterface* eval);
#else
    int InitToolbox(OMLInterface* eval);
#endif
}

bool callipc(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);

int InitToolbox(OMLInterface* eval)
{
	eval->RegisterFunction("callipc", callipc);
	return 0;
}

bool callipc(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    if (!inputs->Size())
    {        
        eval->ThrowError("Invalid number of arguments");
        return 0;
    }

    // ideally pass in two ostreams for messages and errors, but that wasn't working
    ExternalFunction ex_func;

    // for now, only allow vectors and scalars (add strings soon)
    for (int k = 1; k < inputs->Size(); k++)
    {
        const OMLCurrency* test_cur = inputs->Get(k);

        if (!(test_cur->IsMatrix() || test_cur->IsScalar() || test_cur->IsString()))
        {
            eval->ThrowError("Invalid argument");
            return 0;
        }
    }

    if (inputs->Get(0)->IsString())
        strcpy(ex_func.exe_name, inputs->Get(0)->GetString());

    // this is where we're filling in the data to pass to the external function
    ex_func.num_packets = (long)(inputs->Size()-1);
    ex_func.pak_val = new packet[ex_func.num_packets];

    for (int k = 1; k < inputs->Size(); k++)
    {
        const OMLCurrency* cur = inputs->Get(k);

        if (cur->IsMatrix())
        {
            const OMLMatrix* mat = cur->GetMatrix();

            if ((mat->GetCols() != 1) && (mat->GetRows() != 1))
            {
                eval->ThrowError("Input must be vector");
                return 0;
            }

            ex_func.pak_val[k-1]._num_rows = mat->GetRows()*mat->GetCols();
            ex_func.pak_val[k-1].vec = (double*)mat->GetRealData();
        }
        else if (cur->IsString())
        {   
            ex_func.pak_val[k-1].characters = (char*)cur->GetString();
        }
        else
        {
            ex_func.pak_val[k-1]._num_rows = 1;
            ex_func.pak_val[k-1].vec = new double[1];
            ex_func.pak_val[k-1].vec[0] = cur->GetScalar();
        }
    }

    // these are used to get the return vector (filled in by ExternalFunction::Evaluate)
    packet  new_pak;
    int     status;

    status = ex_func.Evaluate(&new_pak);

    delete[] ex_func.pak_val;

    //std::string error_string = error_ostr.str();

    if (status != ALTAIR_IPC_SUCCESS)
    {
        if (ex_func.GetErrorMessage())
            eval->ThrowError(ex_func.GetErrorMessage());
        else
            eval->ThrowError("Execution failed");

        return 0;
    }
    //else if (error_string.size())
    //{
    //    return 0;
    //}
    else
    {
        if (new_pak.characters)
        {
            OMLCurrency* cur = outputs->CreateCurrencyFromString(new_pak.characters);
        }
        else
        {
            OMLMatrix* mat = outputs->CreateMatrix(new_pak._num_rows, new_pak._num_cols, new_pak.vec);
            outputs->AddMatrix(mat);
        }
    }

	return 0;
}

