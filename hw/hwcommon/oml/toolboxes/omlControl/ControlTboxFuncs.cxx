/**
* @file ControlTboxFuncs.cxx
* @date July 2019
* Copyright (C) 2015-2019 Altair Engineering, Inc.
* This file is part of the OpenMatrix Language ("OpenMatrix") software.
* Open Source License Information:
* OpenMatrix is free software. You can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* OpenMatrix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Commercial License Information: 
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/

#include "ControlTboxFuncs.h"

#include <memory>  // For std::unique_ptr

#include "OML_Error.h"
#include "BuiltInFuncsUtils.h"
#include "slicotBasicFuncs.h"
#include "slicotAdvancedFuncs.h"
#include "controlUtilsFunctions.h"
#include "hwMatrix.h"

#define CONTROL "ControlSystem"
#define TBOXVERSION 2019.0

//------------------------------------------------------------------------------
// Entry point which registers omlControl toolbox with oml
//------------------------------------------------------------------------------
int InitDll(EvaluatorInterface eval)
{
    // slicotBasicFuncs.h
    eval.RegisterBuiltInFunction("slicot_sb01bd",   oml_slicot_sb01bd, 7, 10);
    eval.RegisterBuiltInFunction("slicot_ab01od",   oml_slicot_ab01od, 10, 8);
    eval.RegisterBuiltInFunction("slicot_sb02md",   oml_slicot_sb02md, 9, 8);
    eval.RegisterBuiltInFunction("slicot_mc01td",   oml_slicot_mc01td, 2, 5);
    eval.RegisterBuiltInFunction("slicot_mb02md",   oml_slicot_mb02md, 7, 6);
    eval.RegisterBuiltInFunction("slicot_mb02nd",   oml_slicot_mb02nd, 8, 8);
    eval.RegisterBuiltInFunction("slicot_mb02qd",   oml_slicot_mb02qd, 9, 6);
    eval.RegisterBuiltInFunction("slicot_mb03od",   oml_slicot_mb03od, 5, 6);
    eval.RegisterBuiltInFunction("slicot_mb03pd",   oml_slicot_mb03pd, 5, 6);
    eval.RegisterBuiltInFunction("slicot_mb04gd",   oml_slicot_mb04gd, 2, 4);
    eval.RegisterBuiltInFunction("slicot_mb04md",   oml_slicot_mb04md, 2, 4);
    eval.RegisterBuiltInFunction("slicot_ab08nd",   oml_slicot_ab08nd, 9, 11);
    eval.RegisterBuiltInFunction("slicot_ab07nd",   oml_slicot_ab07nd, 4, 6);
    eval.RegisterBuiltInFunction("slicot_ag07bd",   oml_slicot_ag07bd, 6, 6);
    eval.RegisterBuiltInFunction("slicot_ag08bd",   oml_slicot_ag08bd, 9, 14);
    eval.RegisterBuiltInFunction("slicot_sb03md",   oml_slicot_sb03md, 7, 9);
    eval.RegisterBuiltInFunction("slicot_sg03ad",   oml_slicot_sg03ad, 10, 12);
    eval.RegisterBuiltInFunction("slicot_sb04qd",   oml_slicot_sb04qd, 3, 5);
    eval.RegisterBuiltInFunction("slicot_sb04md",   oml_slicot_sb04md, 3, 5);
    eval.RegisterBuiltInFunction("slicot_tb01ud",   oml_slicot_tb01ud, 5, 9);
    eval.RegisterBuiltInFunction("slicot_sb03od",   oml_slicot_sb03od, 6, 7);
    eval.RegisterBuiltInFunction("slicot_sg03bd",   oml_slicot_sg03bd, 8, 10);
    eval.RegisterBuiltInFunction("slicot_mb05od",   oml_slicot_mb05od, 4, 5);
    eval.RegisterBuiltInFunction("slicot_ab04md",   oml_slicot_ab04md, 7, 5);
    eval.RegisterBuiltInFunction("slicot_sb10jd",   oml_slicot_sb10jd, 5, 5);
    eval.RegisterBuiltInFunction("slicot_sg02ad",   oml_slicot_sg02ad, 16, 10);
    eval.RegisterBuiltInFunction("slicot_sb02od",   oml_slicot_sb02od, 13, 9);
    eval.RegisterBuiltInFunction("slicot_ab13ad",   oml_slicot_ab13ad, 6, 6);
    eval.RegisterBuiltInFunction("slicot_mb03rd",   oml_slicot_mb03rd, 6, 7);
    eval.RegisterBuiltInFunction("slicot_ab05pd",   oml_slicot_ab05pd, 10, 6);
    eval.RegisterBuiltInFunction("slicot_tb01id",   oml_slicot_tb01id, 5, 6);
    eval.RegisterBuiltInFunction("slicot_tg01ad",   oml_slicot_tg01ad, 6, 7);
    eval.RegisterBuiltInFunction("slicot_tb05ad_1", oml_slicot_tb05ad_1, 3, 6);
    eval.RegisterBuiltInFunction("slicot_tb05ad_2", oml_slicot_tb05ad_2, 5, 2);
    // slicotAdvancedFuncs.h
    eval.RegisterBuiltInFunction("slicot_H_Norm",   oml_slicot_H_Norm, 5, 3);
    eval.RegisterBuiltInFunction("slicot_H2_Norm",  oml_slicot_H2_Norm, 6, 4);
    eval.RegisterBuiltInFunction("slicot_L_Norm",   oml_slicot_L_Norm, 11, 3);
    eval.RegisterBuiltInFunction("slicot_dhinf",    oml_slicot_dhinf, 8, 8);
    eval.RegisterBuiltInFunction("slicot_hinf",     oml_slicot_hinf, 8, 6);
    eval.RegisterBuiltInFunction("slicot_ndtoabcd", oml_slicot_ndtoabcd, 4, 6);
    eval.RegisterBuiltInFunction("slicot_abcdtond", oml_slicot_abcdtond, 6, 4);
    // ControlTboxFuncs.h
    eval.RegisterBuiltInFunction("expm",      OmlExpm,    FunctionMetaData(1, 1, CONTROL));
    eval.RegisterBuiltInFunction("logm",      OmlLogm,    FunctionMetaData(1, 1, CONTROL));
    eval.RegisterBuiltInFunction("sqrtm",     OmlSqrtm,   FunctionMetaData(1, 1, CONTROL));
    eval.RegisterBuiltInFunction("rsf2csf",   OmlRsf2csf, FunctionMetaData(2, 2, CONTROL));
   
    // Lock functions
    eval.LockBuiltInFunction("slicot_sb01bd");
    eval.LockBuiltInFunction("slicot_ab01od");
    eval.LockBuiltInFunction("slicot_sb02md");
    eval.LockBuiltInFunction("slicot_mc01td");
    eval.LockBuiltInFunction("slicot_mb02md");
    eval.LockBuiltInFunction("slicot_mb02nd");
    eval.LockBuiltInFunction("slicot_mb02qd");
    eval.LockBuiltInFunction("slicot_mb03od");
    eval.LockBuiltInFunction("slicot_mb03pd");
    eval.LockBuiltInFunction("slicot_mb04gd");
    eval.LockBuiltInFunction("slicot_mb04md");
    eval.LockBuiltInFunction("slicot_ab08nd");
    eval.LockBuiltInFunction("slicot_ab07nd");
    eval.LockBuiltInFunction("slicot_ag07bd");
    eval.LockBuiltInFunction("slicot_ag08bd");
    eval.LockBuiltInFunction("slicot_sb03md");
    eval.LockBuiltInFunction("slicot_sg03ad");
    eval.LockBuiltInFunction("slicot_sb04qd");
    eval.LockBuiltInFunction("slicot_sb04md");
    eval.LockBuiltInFunction("slicot_tb01ud");
    eval.LockBuiltInFunction("slicot_sb03od");
    eval.LockBuiltInFunction("slicot_sg03bd");
    eval.LockBuiltInFunction("slicot_mb05od");
    eval.LockBuiltInFunction("slicot_ab04md");
    eval.LockBuiltInFunction("slicot_sb10jd");
    eval.LockBuiltInFunction("slicot_sg02ad");
    eval.LockBuiltInFunction("slicot_sb02od");
    eval.LockBuiltInFunction("slicot_ab13ad");
    eval.LockBuiltInFunction("slicot_mb03rd");
    eval.LockBuiltInFunction("slicot_ab05pd");
    eval.LockBuiltInFunction("slicot_tb01id");
    eval.LockBuiltInFunction("slicot_tg01ad");
    eval.LockBuiltInFunction("slicot_tb05ad_1");
    eval.LockBuiltInFunction("slicot_tb05ad_2");
    // slicotAdvancedFuncs.h
    eval.LockBuiltInFunction("slicot_H_Norm");
    eval.LockBuiltInFunction("slicot_H2_Norm");
    eval.LockBuiltInFunction("slicot_L_Norm");
    eval.LockBuiltInFunction("slicot_dhinf");
    eval.LockBuiltInFunction("slicot_hinf");
    eval.LockBuiltInFunction("slicot_ndtoabcd");
    eval.LockBuiltInFunction("slicot_abcdtond");

    return 1;
}
//------------------------------------------------------------------------------
// Matrix exponential [expm command]
//------------------------------------------------------------------------------
bool OmlExpm(EvaluatorInterface           eval,
             const std::vector<Currency>& inputs, 
             std::vector<Currency>&       outputs)
{
    if (inputs.size() != 1)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!(inputs[0].IsMatrix() && !inputs[0].IsString()) &&
        !inputs[0].IsScalar() && !inputs[0].IsComplex())
        throw OML_Error(OML_ERR_MATRIX, 1, OML_VAR_TYPE);

    const hwMatrix* mtx = inputs[0].ConvertToMatrix();
    std::unique_ptr<hwMatrix> result(EvaluatorInterface::allocateMatrix());
    int warn;
    int info;

    BuiltInFuncsUtils::CheckMathStatus(eval, slicot_expm(*mtx, *result, warn, info));

    outputs.push_back(result.release());

    if (warn != 0)
    {
        char buffer[80];
        sprintf(buffer, "Warning: slicot::mb05od returned warn = %d", warn);
        BuiltInFuncsUtils::SetWarning(eval, std::string(buffer));
    }

    if (info != 0)
    {
        char buffer[80];
        sprintf(buffer, "Error: slicot::mb05od returned info = %d", info);
        throw OML_Error(std::string(buffer));
    }

    return true;
}
//------------------------------------------------------------------------------
// Matrix logarithm [logm command]
//------------------------------------------------------------------------------
bool OmlLogm(EvaluatorInterface           eval,
             const std::vector<Currency>& inputs,
             std::vector<Currency>&       outputs)
{
    if (inputs.size() != 1)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!(inputs[0].IsMatrix() && !inputs[0].IsString()) &&
        !inputs[0].IsScalar() && !inputs[0].IsComplex())
        throw OML_Error(OML_ERR_MATRIX, 1, OML_VAR_TYPE);

    const hwMatrix* mtx = inputs[0].ConvertToMatrix();
    std::unique_ptr<hwMatrix> result(EvaluatorInterface::allocateMatrix());

    BuiltInFuncsUtils::CheckMathStatus(eval, notslicot_logm(*mtx, *result));

    outputs.push_back(result.release());
    return true;
}
//------------------------------------------------------------------------------
// Matrix square root [sqrtm command]
//------------------------------------------------------------------------------
bool OmlSqrtm(EvaluatorInterface           eval,
              const std::vector<Currency>& inputs,
              std::vector<Currency>&       outputs)
{
    if (inputs.size() != 1)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!(inputs[0].IsMatrix() && !inputs[0].IsString()) &&
        !inputs[0].IsScalar() && !inputs[0].IsComplex())
        throw OML_Error(OML_ERR_MATRIX, 1, OML_VAR_TYPE);

    const hwMatrix* mtx = inputs[0].ConvertToMatrix();
    std::unique_ptr<hwMatrix> result(EvaluatorInterface::allocateMatrix());

    BuiltInFuncsUtils::CheckMathStatus(eval, notslicot_sqrtm(*mtx, *result));

    outputs.push_back(result.release());
    return true;
}
//------------------------------------------------------------------------------
// Schur conversion from real form to complex form [rsf2csf command]
//------------------------------------------------------------------------------
bool OmlRsf2csf(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                 std::vector<Currency>&       outputs)
{
    if (inputs.size() != 2)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!(inputs[0].IsMatrix() && !inputs[0].IsString()) &&
        !inputs[0].IsScalar() && !inputs[0].IsComplex())
        throw OML_Error(OML_ERR_MATRIX, 1, OML_VAR_TYPE);

    if (!(inputs[1].IsMatrix() && !inputs[1].IsString()) &&
        !inputs[1].IsScalar() && !inputs[1].IsComplex())
        throw OML_Error(OML_ERR_MATRIX, 2, OML_VAR_TYPE);

    const hwMatrix* UR = inputs[0].ConvertToMatrix();
    const hwMatrix* TR = inputs[1].ConvertToMatrix();
    std::unique_ptr<hwMatrix> UC(EvaluatorInterface::allocateMatrix());
    std::unique_ptr<hwMatrix> UT(EvaluatorInterface::allocateMatrix());

    BuiltInFuncsUtils::CheckMathStatus(eval, rsf2csf(*UR, *TR, *UC, *UT));

    outputs.push_back(UC.release());
    outputs.push_back(UT.release());
    return true;
}
//------------------------------------------------------------------------------
// Returns toolbox version
//------------------------------------------------------------------------------
double GetToolboxVersion(EvaluatorInterface eval)
{
    return TBOXVERSION;
}
