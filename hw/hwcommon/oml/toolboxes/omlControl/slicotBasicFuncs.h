/**
* @file slicotBasicFuncs.h
* @date January, 2013
* Copyright (C) 2017-2018 Altair Engineering, Inc.
* This file is part of the OpenMatrix Language ("OpenMatrix") software.
* Open Source License Information:
* OpenMatrix is free software. You can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* OpenMatrix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Commercial License Information:
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/

#ifndef __SLICOTBASICTBOXFUNCS_OML_H__
#define __SLICOTBASICTBOXFUNCS_OML_H__

#include "EvaluatorInt.h"

//------------------------------------------------------------------------------
//!
//! \brief omlControl toolbox basic slicot functions
//!
//------------------------------------------------------------------------------

bool oml_slicot_sb01bd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_ab01od(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_sb02md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_mc01td(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_mb02md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_mb02nd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_mb02qd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_mb03od(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_mb03pd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_mb04gd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_mb04md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_ab08nd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_ab07nd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_ag07bd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_ag08bd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_sb03md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_sg03ad(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_sb04qd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_sb04md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_tb01ud(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_sb03od(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_sg03bd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_mb05od(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_ab04md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_sb10jd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_sg02ad(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_sb02od(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_ab13ad(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_mb03rd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_ab05pd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_tb01id(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_tg01ad(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_tb05ad_1(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);
bool oml_slicot_tb05ad_2(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);

#endif // __SLICOTBASICTBOXFUNCS_OML_H__
