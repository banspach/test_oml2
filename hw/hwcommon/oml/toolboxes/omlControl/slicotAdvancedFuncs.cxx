/**
* @file slicotAdvancedFuncs.cxx
* @date January, 2013
* Copyright (C) 2017-2018 Altair Engineering, Inc.
* This file is part of the OpenMatrix Language ("OpenMatrix") software.
* Open Source License Information:
* OpenMatrix is free software. You can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* OpenMatrix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Commercial License Information:
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/

#include <stdlib.h>
#include "Evaluator.h"
#include "OML_Error.h"
#include "hwMathStatus.h"
#include <utl/hwString.h>
#include "hwMatrix.h"
#include "slicotAdvancedFuncs.h"
#include "slicotUtilsFunctions.h"
#include "controlUtilsFunctions.h"

//=============================================================================
#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()
//=============================================================================
static hwMatrix getDoubleScalarAsMatrixAdv(const Currency &input)
{
    if (!input.IsScalar())
    {
        OML_Error("A scalar expected.");
    }
    if (input.IsComplex())
    {
        OML_Error("A real scalar expected.");
    }
    hwMatrix mtx(1, 1, hwMatrix::REAL);
    mtx(0) = input.Scalar();
    return mtx;
}
//=============================================================================
static hwMatrix toMatrixAdv(const Currency &input)
{
    if (input.IsScalar())
    {
        hwMatrix mtx(1, 1, hwMatrix::REAL);
        mtx(0) = input.Scalar();
        return mtx;
    }
    else if (input.IsComplex())
    {
        hwMatrix mtx(1, 1, hwMatrix::COMPLEX);
		hwComplex cp = input.Complex();
		mtx.z(0) = cp;
        return mtx;
    }
    else if (input.IsMatrix())
    {
         const hwMatrix mtx = *input.Matrix();
         return mtx;
    }
    else
    {
        OML_Error("A numeric value expected.");
    }
    hwMatrix mtx(0, 0, hwMatrix::REAL);
    return mtx;
}
//=============================================================================
bool oml_slicot_H_Norm(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 5;
    const int NB_OUTPUTS_EXPECTED = 3;
    const std::string FUNCTION_NAME = "slicot_H_Norm";
	
    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

	if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixAdv(inputs[0]);
	
	if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixAdv(inputs[1]);

	if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixAdv(inputs[2]);
	
	if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixAdv(inputs[3]);
	
	if (!inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a scalar expected."));
    }
	hwMatrix TOL = toMatrixAdv(inputs[4]);

    hwMatrix *RESULT = new hwMatrix;
    hwMatrix *FPEAK = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_ab13cd(A, B, C, D, TOL, *INFO, *FPEAK, *RESULT);
    if (!status.IsOk())
    {
        delete INFO;
        delete FPEAK;
        delete RESULT;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(RESULT);
    outputs.push_back(FPEAK);
    outputs.push_back(INFO);
	return true;
}
//=============================================================================
bool oml_slicot_H2_Norm(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 6;
    const int NB_OUTPUTS_EXPECTED = 4;
    const std::string FUNCTION_NAME = "slicot_H2_Norm";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

	if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixAdv(inputs[0]);
	
	if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixAdv(inputs[1]);

	if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixAdv(inputs[2]);
	
	if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixAdv(inputs[3]);

    if (!inputs[4].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a string expected."));
    }
    hwString DICO = inputs[4].StringVal().c_str();
	
	if (!inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a scalar expected."));
    }
	hwMatrix TOL = toMatrixAdv(inputs[5]);

    hwMatrix *RESULT = new hwMatrix;
    hwMatrix *IWARN = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;
    hwMatrix *NQ = new hwMatrix;

    hwString JOBN("H");
    hwMathStatus status = slicot_ab13bd(DICO, JOBN, A, B, C, D, TOL, *INFO, *IWARN, *NQ, *RESULT);
    if (!status.IsOk())
    {
        delete INFO;
        delete IWARN;
        delete NQ;
        delete RESULT;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
	}

    outputs.push_back(RESULT);
    outputs.push_back(NQ);
    outputs.push_back(IWARN);
    outputs.push_back(INFO);
	return true;
}
//=============================================================================
bool oml_slicot_L_Norm(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
	// inputs arguments : A, B, C, D, E, TOL, FPEAKIN, DICO, JOBE, EQUIL, JOBD
    const int NB_INPUTS_EXPECTED = 11;
    const int NB_OUTPUTS_EXPECTED = 3;
    const std::string FUNCTION_NAME = "slicot_L_Norm";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }	
	
	if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixAdv(inputs[0]);
	
	if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixAdv(inputs[1]);

	if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixAdv(inputs[2]);
	
	if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixAdv(inputs[3]);
	
	if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix E = toMatrixAdv(inputs[4]);
	
	if (!inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a scalar expected."));
    }
	hwMatrix TOL = toMatrixAdv(inputs[5]);	

	if (!inputs[6].IsMatrix() && !inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a matrix expected."));
    }
    hwMatrix FPEAKIN = toMatrixAdv(inputs[6]);
	
    if (!inputs[7].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a string expected."));
    }
    hwString DICO = inputs[7].StringVal().c_str();	

    if (!inputs[8].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(9) + std::string(": a string expected."));
    }
    hwString JOBE = inputs[8].StringVal().c_str();	

    if (!inputs[9].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(10) + std::string(": a string expected."));
    }
    hwString EQUIL = inputs[9].StringVal().c_str();	

    if (!inputs[10].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(11) + std::string(": a string expected."));
    }
    hwString JOBD = inputs[10].StringVal().c_str();	
	
    hwMatrix *INFO = new hwMatrix;
    hwMatrix *FPEAK = new hwMatrix;
    hwMatrix *GPEAK = new hwMatrix;
    
    hwMathStatus status =  slicot_ab13dd(DICO, JOBE, EQUIL, JOBD, FPEAKIN, A, B, C, D, E, TOL, *GPEAK, *FPEAK, *INFO);
    if (!status.IsOk())
    {
        delete INFO; INFO = NULL;
        delete FPEAK; FPEAK = NULL;
        delete GPEAK; GPEAK = NULL;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(FPEAK);
    outputs.push_back(GPEAK);
    outputs.push_back(INFO);
	
	return true;
}
//=============================================================================
bool oml_slicot_dhinf(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
	// A, B, C, D, ncon, nmeas, gamma, tol
    const int NB_INPUTS_EXPECTED = 8;
    const int NB_OUTPUTS_EXPECTED = 8;
    const std::string FUNCTION_NAME = "slicot_dhinf";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }	
	
	if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixAdv(inputs[0]);
	
	if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixAdv(inputs[1]);

	if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixAdv(inputs[2]);
	
	if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixAdv(inputs[3]);	
	
	if (!inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a scalar expected."));
    }
	int ncon = (int)inputs[4].Scalar();

	if (!inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a scalar expected."));
    }
	int nmeas = (int)inputs[5].Scalar();

	if (!inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a scalar expected."));
    }
	double gamma = (double)inputs[6].Scalar();

	if (!inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a scalar expected."));
    }
	double tol = (double)inputs[7].Scalar();
	
	hwMatrix *AK = new hwMatrix;
    hwMatrix *BK = new hwMatrix;
    hwMatrix *CK = new hwMatrix;
    hwMatrix *DK = new hwMatrix;
    hwMatrix *X = new hwMatrix;
    hwMatrix *Z = new hwMatrix;
    hwMatrix *RCOND = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sb10dd(A, B, C, D, ncon, nmeas, gamma, tol, *AK, *BK, *CK, *DK, *X, *Z, *RCOND, *INFO);

    if (!status.IsOk())
    {
        delete AK; AK = NULL;
        delete BK; BK = NULL;
        delete CK; CK = NULL;
        delete DK; DK = NULL;
        delete X; X = NULL;
        delete Z; Z = NULL;
        delete RCOND; RCOND = NULL;
        delete INFO; INFO = NULL;
		throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(AK);
    outputs.push_back(BK);
    outputs.push_back(CK);
    outputs.push_back(DK);
    outputs.push_back(RCOND);
    outputs.push_back(X);
    outputs.push_back(Z);
    outputs.push_back(INFO);
	return true;
}
//=============================================================================
bool oml_slicot_hinf(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 8;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "oml_slicot_hinf";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }	
	
	if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixAdv(inputs[0]);
	
	if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixAdv(inputs[1]);

	if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixAdv(inputs[2]);
	
	if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixAdv(inputs[3]);	
	
	if (!inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a scalar expected."));
    }
	int ncon = (int)inputs[4].Scalar();

	if (!inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a scalar expected."));
    }
	int nmeas = (int)inputs[5].Scalar();

	if (!inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a scalar expected."));
    }
	double gamma = (double)inputs[6].Scalar();

	if (!inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a scalar expected."));
    }
	double tol = (double)inputs[7].Scalar();
	
	hwMatrix *AK = new hwMatrix;
    hwMatrix *BK = new hwMatrix;
    hwMatrix *CK = new hwMatrix;
    hwMatrix *DK = new hwMatrix;
    hwMatrix *RCOND = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sb10fd(A, B, C, D, ncon, nmeas, gamma, tol, *AK, *BK, *CK, *DK, *RCOND, *INFO);

    if (!status.IsOk())
    {
        delete AK; AK = NULL;
        delete BK; BK = NULL;
        delete CK; CK = NULL;
        delete DK; DK = NULL;
        delete RCOND; RCOND = NULL;
        delete INFO; INFO = NULL;
		throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(AK);
    outputs.push_back(BK);
    outputs.push_back(CK);
    outputs.push_back(DK);
    outputs.push_back(RCOND);
    outputs.push_back(INFO);	
	return true;
}
//=============================================================================
bool oml_slicot_ndtoabcd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
	// NumCoeffs, DenCoeffs, Index, tol
    const int NB_INPUTS_EXPECTED = 4;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "slicot_ndtoabcd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        throw OML_Error(OML_ERR_NUMARGIN);
        //std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        //throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        throw OML_Error(OML_ERR_NUMARGOUT);
        //std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        //throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(OML_ERR_REALMATRIX, 1, OML_VAR_DATA);
    }

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(OML_ERR_REALMATRIX, 2, OML_VAR_DATA);
    }

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(OML_ERR_REALMATRIX, 3, OML_VAR_DATA);
    }

    if (!inputs[3].IsScalar())
    {
        throw OML_Error(OML_ERR_SCALAR, 4, OML_VAR_DATA);
    }

    const hwMatrix* NumCoeffs = inputs[0].ConvertToMatrix();
    const hwMatrix* DenCoeffs = inputs[1].ConvertToMatrix();
    const hwMatrix* Index     = inputs[2].ConvertToMatrix();
	double tol                = inputs[3].Scalar();
	
    hwMatrix* A    = new hwMatrix;
    hwMatrix* B    = new hwMatrix;
    hwMatrix* C    = new hwMatrix;
    hwMatrix* D    = new hwMatrix;
    int       NR;
    int       INFO;

    hwMatrixI __INDEX(Index->M(), Index->N(), hwMatrixI::REAL);

    for (int k = 0; k < (Index->Size()); k++)
    {
        __INDEX(k) = static_cast<int> ((*Index)(k));
    }
   
    hwMathStatus status = slicot_td04ad(*NumCoeffs, *DenCoeffs, __INDEX, tol, *A, *B, *C, *D, NR, INFO);
    if (!status.IsOk())
    {
        if (status.GetArg1() > 4)
        {
            status.ResetArgs();
            delete A;
            delete B;
            delete C;
            delete D;
			throw OML_Error(FUNCTION_NAME + std::string(" failed"));
			return false;
        }
    }
    outputs.push_back(A);
    outputs.push_back(B);
    outputs.push_back(C);
    outputs.push_back(D);
    outputs.push_back(NR);
    outputs.push_back(INFO);	
	return true;
}
//=============================================================================
bool oml_slicot_abcdtond(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    // NumCoeffs, DenCoeffs, Index, tol
    const int NB_INPUTS_EXPECTED = 6;
    const int NB_OUTPUTS_EXPECTED = 5;
    const std::string FUNCTION_NAME = "slicot_abcdtond";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        throw OML_Error(OML_ERR_NUMARGIN);
        //std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        //throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        throw OML_Error(OML_ERR_NUMARGOUT);
        //std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        //throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(OML_ERR_REALMATRIX, 1, OML_VAR_DATA);
    }

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(OML_ERR_REALMATRIX, 2, OML_VAR_DATA);
    }

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(OML_ERR_REALMATRIX, 3, OML_VAR_DATA);
    }

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(OML_ERR_REALMATRIX, 4, OML_VAR_DATA);
    }

    if (!inputs[4].IsScalar())
    {
        throw OML_Error(OML_ERR_SCALAR, 5, OML_VAR_DATA);
    }

    if (!inputs[5].IsScalar())
    {
        throw OML_Error(OML_ERR_SCALAR, 6, OML_VAR_DATA);
    }

    const hwMatrix* A = inputs[0].ConvertToMatrix();
    const hwMatrix* B = inputs[1].ConvertToMatrix();
    const hwMatrix* C = inputs[2].ConvertToMatrix();
    const hwMatrix* D = inputs[3].ConvertToMatrix();
    double tol1 = inputs[4].Scalar();
    double tol2 = inputs[5].Scalar();

    hwMatrix* NumCoeffs = new hwMatrix;
    hwMatrix* DenCoeffs = new hwMatrix;
    hwMatrixI Index;
    int       NR;
    int       INFO;

    hwMathStatus status = slicot_tb04ad(*A, *B, *C, *D, *NumCoeffs, *DenCoeffs, Index, tol1, tol2, NR, INFO);
    if (!status.IsOk())
    {
        if (status.GetArg1() > 6)
        {
            status.ResetArgs();
            delete NumCoeffs;
            delete DenCoeffs;
            throw OML_Error(FUNCTION_NAME + std::string(" failed"));
            return false;
        }
    }

    hwMatrix* __INDEX = new hwMatrix(Index.M(), Index.N(), hwMatrix::REAL);

    for (int k = 0; k < (Index.Size()); k++)
    {
        (*__INDEX)(k) = static_cast<double> (Index(k));
    }

    outputs.push_back(NumCoeffs);
    outputs.push_back(DenCoeffs);
    outputs.push_back(__INDEX);
    outputs.push_back(NR);
    outputs.push_back(INFO);
    return true;
}
