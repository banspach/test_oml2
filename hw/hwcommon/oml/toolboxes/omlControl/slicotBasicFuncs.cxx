/**
* @file slicotBasicFuncs.cxx
* @date January, 2013
* Copyright (C) 2017-2018 Altair Engineering, Inc.
* This file is part of the OpenMatrix Language ("OpenMatrix") software.
* Open Source License Information:
* OpenMatrix is free software. You can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* OpenMatrix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Commercial License Information:
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/

#include <stdlib.h>
#include "Evaluator.h"
#include "OML_Error.h"
#include "hwMathStatus.h"
#include "hwMatrix.h"
#include <utl/hwString.h>
#include "slicotBasicFuncs.h"
#include "slicotUtilsFunctions.h"

//=============================================================================
#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()
//=============================================================================
static hwMatrix getDoubleScalarAsMatrixBasic(const Currency &input)
{
    if (!input.IsScalar())
    {
        OML_Error("A scalar expected.");
    }
    if (input.IsComplex())
    {
        OML_Error("A real scalar expected.");
    }
    hwMatrix mtx(1, 1, hwMatrix::REAL);
    mtx(0) = input.Scalar();
    return mtx;
}
//=============================================================================
static hwMatrix toMatrixBasic(const Currency &input)
{
    if (input.IsScalar())
    {
        hwMatrix mtx(1, 1, hwMatrix::REAL);
        mtx(0) = input.Scalar();
        return mtx;
    }
    else if (input.IsComplex())
    {
        hwMatrix mtx(1, 1, hwMatrix::COMPLEX);
		hwComplex cp = input.Complex();
		mtx.z(0) = cp;
        return mtx;
    }
    else if (input.IsMatrix())
    {
         const hwMatrix mtx = *input.Matrix();
         return mtx;
    }
    else
    {
        OML_Error("A numeric value expected.");
    }
    hwMatrix mtx(0, 0, hwMatrix::REAL);
    return mtx;
}
//=============================================================================
bool oml_slicot_sb01bd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 7;
    const int NB_OUTPUTS_EXPECTED = 10;
    const std::string FUNCTION_NAME = "slicot_sb01bd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString DICO = inputs[0].StringVal().c_str();

    if (!inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a scalar expected."));
    }
    hwMatrix ALPHA = getDoubleScalarAsMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix WR = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix WI = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a scalar expected."));
    }
    hwMatrix TOL = getDoubleScalarAsMatrixBasic(inputs[6]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *WR_OUT = new hwMatrix;
    hwMatrix *WI_OUT = new hwMatrix;
    hwMatrix *NFP = new hwMatrix;
    hwMatrix *NAP = new hwMatrix;
    hwMatrix *NUP = new hwMatrix;
    hwMatrix *F = new hwMatrix;
    hwMatrix *Z = new hwMatrix;
    hwMatrix *IWARN = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sb01bd( DICO,ALPHA,A,B,WR,WI,*NFP,*NAP,*NUP,*F,*Z,TOL,*IWARN,*INFO,*A_OUT,*WR_OUT,*WI_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete WR_OUT;
        delete WI_OUT;
        delete NFP;
        delete NAP;
        delete NUP;
        delete F;
        delete Z;
        delete IWARN;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(WR_OUT);
    outputs.push_back(WI_OUT);
    outputs.push_back(NFP);
    outputs.push_back(NAP);
    outputs.push_back(NUP);
    outputs.push_back(F);
    outputs.push_back(Z);
    outputs.push_back(IWARN);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_ab01od(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 10;
    const int NB_OUTPUTS_EXPECTED = 8;
    const std::string FUNCTION_NAME = "slicot_ab01od";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString STAGES = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString JOBU = inputs[1].StringVal().c_str();

    if (!inputs[2].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a string expected."));
    }
    hwString JOBV = inputs[2].StringVal().c_str();

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix U = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a scalar expected."));
    }
    hwMatrix NCONT = getDoubleScalarAsMatrixBasic(inputs[6]);

    if (!inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a scalar expected."));
    }
    hwMatrix INDCON = getDoubleScalarAsMatrixBasic(inputs[7]);

    if (!inputs[8].IsMatrix() && !inputs[8].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(9) + std::string(": a matrix expected."));
    }
    hwMatrix KSTAIR = toMatrixBasic(inputs[8]);

    if (!inputs[9].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(10) + std::string(": a scalar expected."));
    }
    hwMatrix TOL = getDoubleScalarAsMatrixBasic(inputs[9]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *U_OUT = new hwMatrix;
    hwMatrix *V = new hwMatrix;
    hwMatrix *NCONT_OUT = new hwMatrix;
    hwMatrix *INDCON_OUT = new hwMatrix;
    hwMatrix *KSTAIR_OUT = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_ab01od( STAGES,JOBU,JOBV,A,B,U,*V,NCONT,INDCON,KSTAIR,TOL,*INFO,*A_OUT,*B_OUT,*U_OUT,*NCONT_OUT,*INDCON_OUT,*KSTAIR_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete B_OUT;
        delete U_OUT;
        delete V;
        delete NCONT_OUT;
        delete INDCON_OUT;
        delete KSTAIR_OUT;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(U_OUT);
    outputs.push_back(V);
    outputs.push_back(NCONT_OUT);
    outputs.push_back(INDCON_OUT);
    outputs.push_back(KSTAIR_OUT);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_sb02md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 9;
    const int NB_OUTPUTS_EXPECTED = 8;
    const std::string FUNCTION_NAME = "slicot_sb02md";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString DICO = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString HINV = inputs[1].StringVal().c_str();

    if (!inputs[2].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a string expected."));
    }
    hwString UPLO = inputs[2].StringVal().c_str();

    if (!inputs[3].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a string expected."));
    }
    hwString SCAL = inputs[3].StringVal().c_str();

    if (!inputs[4].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a string expected."));
    }
    hwString SORT = inputs[4].StringVal().c_str();

    if (!inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a scalar expected."));
    }
    hwMatrix N = getDoubleScalarAsMatrixBasic(inputs[5]);

    if (!inputs[6].IsMatrix() && !inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[6]);

    if (!inputs[7].IsMatrix() && !inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a matrix expected."));
    }
    hwMatrix G = toMatrixBasic(inputs[7]);

    if (!inputs[8].IsMatrix() && !inputs[8].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(9) + std::string(": a matrix expected."));
    }
    hwMatrix Q = toMatrixBasic(inputs[8]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *Q_OUT = new hwMatrix;
    hwMatrix *U = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;
    hwMatrix *RCOND = new hwMatrix;
    hwMatrix *WR = new hwMatrix;
    hwMatrix *WI = new hwMatrix;
    hwMatrix *S = new hwMatrix;

    hwMathStatus status = slicot_sb02md( DICO,HINV,UPLO,SCAL,SORT,N,A,G,Q,*U,*INFO,*RCOND,*WR,*WI,*S,*A_OUT,*Q_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete Q_OUT;
        delete U;
        delete INFO;
        delete RCOND;
        delete WR;
        delete WI;
        delete S;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(Q_OUT);
    outputs.push_back(U);
    outputs.push_back(INFO);
    outputs.push_back(RCOND);
    outputs.push_back(WR);
    outputs.push_back(WI);
    outputs.push_back(S);
    return true;

}
//=============================================================================
bool oml_slicot_mc01td(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 2;
    const int NB_OUTPUTS_EXPECTED = 5;
    const std::string FUNCTION_NAME = "slicot_mc01td";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString DICO = inputs[0].StringVal().c_str();

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix P = toMatrixBasic(inputs[1]);

    hwMatrix *DP = new hwMatrix;
    hwMatrix *STABLE = new hwMatrix;
    hwMatrix *NZ = new hwMatrix;
    hwMatrix *IWARN = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_mc01td( DICO,*DP,P,*STABLE,*NZ,*IWARN,*INFO );

    if (!status.IsOk())
    {
        delete DP;
        delete STABLE;
        delete NZ;
        delete IWARN;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(DP);
    outputs.push_back(STABLE);
    outputs.push_back(NZ);
    outputs.push_back(IWARN);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_mb02md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 7;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "slicot_mb02md";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString JOB = inputs[0].StringVal().c_str();

    if (!inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a scalar expected."));
    }
    hwMatrix M = getDoubleScalarAsMatrixBasic(inputs[1]);

    if (!inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a scalar expected."));
    }
    hwMatrix N = getDoubleScalarAsMatrixBasic(inputs[2]);

    if (!inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a scalar expected."));
    }
    hwMatrix L = getDoubleScalarAsMatrixBasic(inputs[3]);

    if (!inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a scalar expected."));
    }
    hwMatrix RANK = getDoubleScalarAsMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a scalar expected."));
    }
    hwMatrix TOL = getDoubleScalarAsMatrixBasic(inputs[6]);

    hwMatrix *RANK_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *S = new hwMatrix;
    hwMatrix *X = new hwMatrix;
    hwMatrix *IWARN = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_mb02md( JOB,M,N,L,RANK,C,*S,*X,TOL,*IWARN,*INFO,*RANK_OUT,*C_OUT );

    if (!status.IsOk())
    {
        delete RANK_OUT;
        delete C_OUT;
        delete S;
        delete X;
        delete IWARN;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(RANK_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(S);
    outputs.push_back(X);
    outputs.push_back(IWARN);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_mb02nd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 8;
    const int NB_OUTPUTS_EXPECTED = 8;
    const std::string FUNCTION_NAME = "slicot_mb02nd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a scalar expected."));
    }
    hwMatrix M = getDoubleScalarAsMatrixBasic(inputs[0]);

    if (!inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a scalar expected."));
    }
    hwMatrix N = getDoubleScalarAsMatrixBasic(inputs[1]);

    if (!inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a scalar expected."));
    }
    hwMatrix L = getDoubleScalarAsMatrixBasic(inputs[2]);

    if (!inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a scalar expected."));
    }
    hwMatrix RANK = getDoubleScalarAsMatrixBasic(inputs[3]);

    if (!inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a scalar expected."));
    }
    hwMatrix THETA = getDoubleScalarAsMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a scalar expected."));
    }
    hwMatrix TOL = getDoubleScalarAsMatrixBasic(inputs[6]);

    if (!inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a scalar expected."));
    }
    hwMatrix RELTOL = getDoubleScalarAsMatrixBasic(inputs[7]);

    hwMatrix *RANK_OUT = new hwMatrix;
    hwMatrix *THETA_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *X = new hwMatrix;
    hwMatrix *Q = new hwMatrix;
    hwMatrix *INUL = new hwMatrix;
    hwMatrix *IWARN = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_mb02nd( M,N,L,RANK,THETA,C,*X,*Q,*INUL,TOL,RELTOL,*IWARN,*INFO,*RANK_OUT,*THETA_OUT,*C_OUT );

    if (!status.IsOk())
    {
        delete RANK_OUT;
        delete THETA_OUT;
        delete C_OUT;
        delete X;
        delete Q;
        delete INUL;
        delete IWARN;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(RANK_OUT);
    outputs.push_back(THETA_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(X);
    outputs.push_back(Q);
    outputs.push_back(INUL);
    outputs.push_back(IWARN);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_mb02qd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 9;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "slicot_mb02qd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString JOB = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString INIPER = inputs[1].StringVal().c_str();

    if (!inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a scalar expected."));
    }
    hwMatrix NRHS = getDoubleScalarAsMatrixBasic(inputs[2]);

    if (!inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a scalar expected."));
    }
    hwMatrix RCOND = getDoubleScalarAsMatrixBasic(inputs[3]);

    if (!inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a scalar expected."));
    }
    hwMatrix SVLMAX = getDoubleScalarAsMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsMatrix() && !inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[6]);

    if (!inputs[7].IsMatrix() && !inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a matrix expected."));
    }
    hwMatrix Y = toMatrixBasic(inputs[7]);

    if (!inputs[8].IsMatrix() && !inputs[8].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(9) + std::string(": a matrix expected."));
    }
    hwMatrix JPVT = toMatrixBasic(inputs[8]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *JPVT_OUT = new hwMatrix;
    hwMatrix *RANK = new hwMatrix;
    hwMatrix *SVAL = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_mb02qd( JOB,INIPER,NRHS,RCOND,SVLMAX,A,B,Y,JPVT,*RANK,*SVAL,*INFO,*A_OUT,*B_OUT,*JPVT_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete B_OUT;
        delete JPVT_OUT;
        delete RANK;
        delete SVAL;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(JPVT_OUT);
    outputs.push_back(RANK);
    outputs.push_back(SVAL);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_mb03od(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 5;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "slicot_mb03od";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString JOBQR = inputs[0].StringVal().c_str();

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix JPVT = toMatrixBasic(inputs[2]);

    if (!inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a scalar expected."));
    }
    hwMatrix RCOND = getDoubleScalarAsMatrixBasic(inputs[3]);

    if (!inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a scalar expected."));
    }
    hwMatrix SVLMAX = getDoubleScalarAsMatrixBasic(inputs[4]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *JPVT_OUT = new hwMatrix;
    hwMatrix *TAU = new hwMatrix;
    hwMatrix *RANK = new hwMatrix;
    hwMatrix *SVAL = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_mb03od( JOBQR,A,JPVT,RCOND,SVLMAX,*TAU,*RANK,*SVAL,*INFO,*A_OUT,*JPVT_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete JPVT_OUT;
        delete TAU;
        delete RANK;
        delete SVAL;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(JPVT_OUT);
    outputs.push_back(TAU);
    outputs.push_back(RANK);
    outputs.push_back(SVAL);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_mb03pd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 5;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "slicot_mb03pd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString JOBRQ = inputs[0].StringVal().c_str();

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix JPVT = toMatrixBasic(inputs[2]);

    if (!inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a scalar expected."));
    }
    hwMatrix RCOND = getDoubleScalarAsMatrixBasic(inputs[3]);

    if (!inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a scalar expected."));
    }
    hwMatrix SVLMAX = getDoubleScalarAsMatrixBasic(inputs[4]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *JPVT_OUT = new hwMatrix;
    hwMatrix *TAU = new hwMatrix;
    hwMatrix *RANK = new hwMatrix;
    hwMatrix *SVAL = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_mb03pd( JOBRQ,A,JPVT,RCOND,SVLMAX,*TAU,*RANK,*SVAL,*INFO,*A_OUT,*JPVT_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete JPVT_OUT;
        delete TAU;
        delete RANK;
        delete SVAL;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(JPVT_OUT);
    outputs.push_back(TAU);
    outputs.push_back(RANK);
    outputs.push_back(SVAL);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_mb04gd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 2;
    const int NB_OUTPUTS_EXPECTED = 4;
    const std::string FUNCTION_NAME = "slicot_mb04gd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[0]);

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix JPVT = toMatrixBasic(inputs[1]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *JPVT_OUT = new hwMatrix;
    hwMatrix *TAU = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_mb04gd( A,JPVT,*TAU,*INFO,*A_OUT,*JPVT_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete JPVT_OUT;
        delete TAU;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(JPVT_OUT);
    outputs.push_back(TAU);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_mb04md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 2;
    const int NB_OUTPUTS_EXPECTED = 4;
    const std::string FUNCTION_NAME = "slicot_mb04md";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a scalar expected."));
    }
    hwMatrix MAXRED = getDoubleScalarAsMatrixBasic(inputs[0]);

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[1]);

    hwMatrix *MAXRED_OUT = new hwMatrix;
    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *SCALE = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_mb04md( MAXRED,A,*SCALE,*INFO,*MAXRED_OUT,*A_OUT );

    if (!status.IsOk())
    {
        delete MAXRED_OUT;
        delete A_OUT;
        delete SCALE;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(MAXRED_OUT);
    outputs.push_back(A_OUT);
    outputs.push_back(SCALE);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_ab08nd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 9;
    const int NB_OUTPUTS_EXPECTED = 11;
    const std::string FUNCTION_NAME = "slicot_ab08nd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString EQUIL = inputs[0].StringVal().c_str();

    if (!inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a scalar expected."));
    }
    hwMatrix N = getDoubleScalarAsMatrixBasic(inputs[1]);

    if (!inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a scalar expected."));
    }
    hwMatrix M = getDoubleScalarAsMatrixBasic(inputs[2]);

    if (!inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a scalar expected."));
    }
    hwMatrix P = getDoubleScalarAsMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsMatrix() && !inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[6]);

    if (!inputs[7].IsMatrix() && !inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixBasic(inputs[7]);

    if (!inputs[8].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(9) + std::string(": a scalar expected."));
    }
    hwMatrix TOL = getDoubleScalarAsMatrixBasic(inputs[8]);

    hwMatrix *NU = new hwMatrix;
    hwMatrix *RANK = new hwMatrix;
    hwMatrix *DINFZ = new hwMatrix;
    hwMatrix *NKROR = new hwMatrix;
    hwMatrix *NKROL = new hwMatrix;
    hwMatrix *INFZ = new hwMatrix;
    hwMatrix *KRONR = new hwMatrix;
    hwMatrix *KRONL = new hwMatrix;
    hwMatrix *AF = new hwMatrix;
    hwMatrix *BF = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_ab08nd( EQUIL,N,M,P,A,B,C,D,*NU,*RANK,*DINFZ,*NKROR,*NKROL,*INFZ,*KRONR,*KRONL,*AF,*BF,TOL,*INFO );

    if (!status.IsOk())
    {
        delete NU;
        delete RANK;
        delete DINFZ;
        delete NKROR;
        delete NKROL;
        delete INFZ;
        delete KRONR;
        delete KRONL;
        delete AF;
        delete BF;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(NU);
    outputs.push_back(RANK);
    outputs.push_back(DINFZ);
    outputs.push_back(NKROR);
    outputs.push_back(NKROL);
    outputs.push_back(INFZ);
    outputs.push_back(KRONR);
    outputs.push_back(KRONL);
    outputs.push_back(AF);
    outputs.push_back(BF);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_ab07nd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 4;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "slicot_ab07nd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[0]);

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixBasic(inputs[3]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *D_OUT = new hwMatrix;
    hwMatrix *RCOND = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_ab07nd( A,B,C,D,*RCOND,*INFO,*A_OUT,*B_OUT,*C_OUT,*D_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete B_OUT;
        delete C_OUT;
        delete D_OUT;
        delete RCOND;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(D_OUT);
    outputs.push_back(RCOND);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_ag07bd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 6;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "slicot_ag07bd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString JOBE = inputs[0].StringVal().c_str();

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix E = toMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixBasic(inputs[5]);

    hwMatrix *AI = new hwMatrix;
    hwMatrix *EI = new hwMatrix;
    hwMatrix *BI = new hwMatrix;
    hwMatrix *CI = new hwMatrix;
    hwMatrix *DI = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_ag07bd( JOBE,A,E,B,C,D,*AI,*EI,*BI,*CI,*DI,*INFO );

    if (!status.IsOk())
    {
        delete AI;
        delete EI;
        delete BI;
        delete CI;
        delete DI;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(AI);
    outputs.push_back(EI);
    outputs.push_back(BI);
    outputs.push_back(CI);
    outputs.push_back(DI);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_ag08bd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 9;
    const int NB_OUTPUTS_EXPECTED = 14;
    const std::string FUNCTION_NAME = "slicot_ag08bd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString EQUIL = inputs[0].StringVal().c_str();

    if (!inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a scalar expected."));
    }
    hwMatrix M = getDoubleScalarAsMatrixBasic(inputs[1]);

    if (!inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a scalar expected."));
    }
    hwMatrix P = getDoubleScalarAsMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix E = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsMatrix() && !inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[6]);

    if (!inputs[7].IsMatrix() && !inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixBasic(inputs[7]);

    if (!inputs[8].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(9) + std::string(": a scalar expected."));
    }
    hwMatrix TOL = getDoubleScalarAsMatrixBasic(inputs[8]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *E_OUT = new hwMatrix;
    hwMatrix *NFZ = new hwMatrix;
    hwMatrix *NRANK = new hwMatrix;
    hwMatrix *NIZ = new hwMatrix;
    hwMatrix *DINFZ = new hwMatrix;
    hwMatrix *NKROR = new hwMatrix;
    hwMatrix *NINFE = new hwMatrix;
    hwMatrix *NKROL = new hwMatrix;
    hwMatrix *INFZ = new hwMatrix;
    hwMatrix *KRONR = new hwMatrix;
    hwMatrix *INFE = new hwMatrix;
    hwMatrix *KRONL = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_ag08bd( EQUIL,M,P,A,E,B,C,D,*NFZ,*NRANK,*NIZ,*DINFZ,*NKROR,*NINFE,*NKROL,*INFZ,*KRONR,*INFE,*KRONL,TOL,*INFO,*A_OUT,*E_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete E_OUT;
        delete NFZ;
        delete NRANK;
        delete NIZ;
        delete DINFZ;
        delete NKROR;
        delete NINFE;
        delete NKROL;
        delete INFZ;
        delete KRONR;
        delete INFE;
        delete KRONL;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(E_OUT);
    outputs.push_back(NFZ);
    outputs.push_back(NRANK);
    outputs.push_back(NIZ);
    outputs.push_back(DINFZ);
    outputs.push_back(NKROR);
    outputs.push_back(NINFE);
    outputs.push_back(NKROL);
    outputs.push_back(INFZ);
    outputs.push_back(KRONR);
    outputs.push_back(INFE);
    outputs.push_back(KRONL);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_sb03md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 7;
    const int NB_OUTPUTS_EXPECTED = 9;
    const std::string FUNCTION_NAME = "slicot_sb03md";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString DICO = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString JOB = inputs[1].StringVal().c_str();

    if (!inputs[2].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a string expected."));
    }
    hwString FACT = inputs[2].StringVal().c_str();

    if (!inputs[3].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a string expected."));
    }
    hwString TRANA = inputs[3].StringVal().c_str();

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix U = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsMatrix() && !inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[6]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *U_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *SCALE = new hwMatrix;
    hwMatrix *SEP = new hwMatrix;
    hwMatrix *FERR = new hwMatrix;
    hwMatrix *WR = new hwMatrix;
    hwMatrix *WI = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sb03md( DICO,JOB,FACT,TRANA,A,U,C,*SCALE,*SEP,*FERR,*WR,*WI,*INFO,*A_OUT,*U_OUT,*C_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete U_OUT;
        delete C_OUT;
        delete SCALE;
        delete SEP;
        delete FERR;
        delete WR;
        delete WI;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(U_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(SCALE);
    outputs.push_back(SEP);
    outputs.push_back(FERR);
    outputs.push_back(WR);
    outputs.push_back(WI);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_sg03ad(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 10;
    const int NB_OUTPUTS_EXPECTED = 12;
    const std::string FUNCTION_NAME = "slicot_sg03ad";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString DICO = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString JOB = inputs[1].StringVal().c_str();

    if (!inputs[2].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a string expected."));
    }
    hwString FACT = inputs[2].StringVal().c_str();

    if (!inputs[3].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a string expected."));
    }
    hwString TRANS = inputs[3].StringVal().c_str();

    if (!inputs[4].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a string expected."));
    }
    hwString UPLO = inputs[4].StringVal().c_str();

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsMatrix() && !inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a matrix expected."));
    }
    hwMatrix E = toMatrixBasic(inputs[6]);

    if (!inputs[7].IsMatrix() && !inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a matrix expected."));
    }
    hwMatrix Q = toMatrixBasic(inputs[7]);

    if (!inputs[8].IsMatrix() && !inputs[8].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(9) + std::string(": a matrix expected."));
    }
    hwMatrix Z = toMatrixBasic(inputs[8]);

    if (!inputs[9].IsMatrix() && !inputs[9].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(10) + std::string(": a matrix expected."));
    }
    hwMatrix X = toMatrixBasic(inputs[9]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *E_OUT = new hwMatrix;
    hwMatrix *Q_OUT = new hwMatrix;
    hwMatrix *Z_OUT = new hwMatrix;
    hwMatrix *X_OUT = new hwMatrix;
    hwMatrix *SCALE = new hwMatrix;
    hwMatrix *SEP = new hwMatrix;
    hwMatrix *FERR = new hwMatrix;
    hwMatrix *ALPHAR = new hwMatrix;
    hwMatrix *ALPHAI = new hwMatrix;
    hwMatrix *BETA = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sg03ad( DICO,JOB,FACT,TRANS,UPLO,A,E,Q,Z,X,*SCALE,*SEP,*FERR,*ALPHAR,*ALPHAI,*BETA,*INFO,*A_OUT,*E_OUT,*Q_OUT,*Z_OUT,*X_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete E_OUT;
        delete Q_OUT;
        delete Z_OUT;
        delete X_OUT;
        delete SCALE;
        delete SEP;
        delete FERR;
        delete ALPHAR;
        delete ALPHAI;
        delete BETA;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(E_OUT);
    outputs.push_back(Q_OUT);
    outputs.push_back(Z_OUT);
    outputs.push_back(X_OUT);
    outputs.push_back(SCALE);
    outputs.push_back(SEP);
    outputs.push_back(FERR);
    outputs.push_back(ALPHAR);
    outputs.push_back(ALPHAI);
    outputs.push_back(BETA);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_sb04qd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 3;
    const int NB_OUTPUTS_EXPECTED = 5;
    const std::string FUNCTION_NAME = "slicot_sb04qd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[0]);

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[2]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *Z = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sb04qd( A,B,C,*Z,*INFO,*A_OUT,*B_OUT,*C_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete B_OUT;
        delete C_OUT;
        delete Z;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(Z);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_sb04md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 3;
    const int NB_OUTPUTS_EXPECTED = 5;
    const std::string FUNCTION_NAME = "slicot_sb04md";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[0]);

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[2]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *Z = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sb04md( A,B,C,*Z,*INFO,*A_OUT,*B_OUT,*C_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete B_OUT;
        delete C_OUT;
        delete Z;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(Z);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_tb01ud(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 5;
    const int NB_OUTPUTS_EXPECTED = 9;
    const std::string FUNCTION_NAME = "slicot_tb01ud";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString JOBZ = inputs[0].StringVal().c_str();

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a scalar expected."));
    }
    hwMatrix TOL = getDoubleScalarAsMatrixBasic(inputs[4]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *NCONT = new hwMatrix;
    hwMatrix *INDCON = new hwMatrix;
    hwMatrix *NBLK = new hwMatrix;
    hwMatrix *Z = new hwMatrix;
    hwMatrix *TAU = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_tb01ud( JOBZ,A,B,C,*NCONT,*INDCON,*NBLK,*Z,*TAU,TOL,*INFO,*A_OUT,*B_OUT,*C_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete B_OUT;
        delete C_OUT;
        delete NCONT;
        delete INDCON;
        delete NBLK;
        delete Z;
        delete TAU;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(NCONT);
    outputs.push_back(INDCON);
    outputs.push_back(NBLK);
    outputs.push_back(Z);
    outputs.push_back(TAU);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_sb03od(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 6;
    const int NB_OUTPUTS_EXPECTED = 7;
    const std::string FUNCTION_NAME = "slicot_sb03od";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString DICO = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString FACT = inputs[1].StringVal().c_str();

    if (!inputs[2].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a string expected."));
    }
    hwString TRANS = inputs[2].StringVal().c_str();

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix Q = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[5]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *Q_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *SCALE = new hwMatrix;
    hwMatrix *WR = new hwMatrix;
    hwMatrix *WI = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sb03od( DICO,FACT,TRANS,A,Q,B,*SCALE,*WR,*WI,*INFO,*A_OUT,*Q_OUT,*B_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete Q_OUT;
        delete B_OUT;
        delete SCALE;
        delete WR;
        delete WI;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(Q_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(SCALE);
    outputs.push_back(WR);
    outputs.push_back(WI);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_sg03bd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 8;
    const int NB_OUTPUTS_EXPECTED = 10;
    const std::string FUNCTION_NAME = "slicot_sg03bd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString DICO = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString FACT = inputs[1].StringVal().c_str();

    if (!inputs[2].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a string expected."));
    }
    hwString TRANS = inputs[2].StringVal().c_str();

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix E = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix Q = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsMatrix() && !inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a matrix expected."));
    }
    hwMatrix Z = toMatrixBasic(inputs[6]);

    if (!inputs[7].IsMatrix() && !inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[7]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *E_OUT = new hwMatrix;
    hwMatrix *Q_OUT = new hwMatrix;
    hwMatrix *Z_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *SCALE = new hwMatrix;
    hwMatrix *ALPHAR = new hwMatrix;
    hwMatrix *ALPHAI = new hwMatrix;
    hwMatrix *BETA = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sg03bd( DICO,FACT,TRANS,A,E,Q,Z,B,*SCALE,*ALPHAR,*ALPHAI,*BETA,*INFO,*A_OUT,*E_OUT,*Q_OUT,*Z_OUT,*B_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete E_OUT;
        delete Q_OUT;
        delete Z_OUT;
        delete B_OUT;
        delete SCALE;
        delete ALPHAR;
        delete ALPHAI;
        delete BETA;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(E_OUT);
    outputs.push_back(Q_OUT);
    outputs.push_back(Z_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(SCALE);
    outputs.push_back(ALPHAR);
    outputs.push_back(ALPHAI);
    outputs.push_back(BETA);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_mb05od(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 4;
    const int NB_OUTPUTS_EXPECTED = 5;
    const std::string FUNCTION_NAME = "slicot_mb05od";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString BALANC = inputs[0].StringVal().c_str();

    if (!inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a scalar expected."));
    }
    hwMatrix NDIAG = getDoubleScalarAsMatrixBasic(inputs[1]);

    if (!inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a scalar expected."));
    }
    hwMatrix DELTA = getDoubleScalarAsMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[3]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *MDIG = new hwMatrix;
    hwMatrix *IDIG = new hwMatrix;
    hwMatrix *IWARN = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_mb05od( BALANC,NDIAG,DELTA,A,*MDIG,*IDIG,*IWARN,*INFO,*A_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete MDIG;
        delete IDIG;
        delete IWARN;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(MDIG);
    outputs.push_back(IDIG);
    outputs.push_back(IWARN);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_ab04md(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 7;
    const int NB_OUTPUTS_EXPECTED = 5;
    const std::string FUNCTION_NAME = "slicot_ab04md";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString TYPE = inputs[0].StringVal().c_str();

    if (!inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a scalar expected."));
    }
    hwMatrix ALPHA = getDoubleScalarAsMatrixBasic(inputs[1]);

    if (!inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a scalar expected."));
    }
    hwMatrix BETA = getDoubleScalarAsMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsMatrix() && !inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixBasic(inputs[6]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *D_OUT = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_ab04md( TYPE,ALPHA,BETA,A,B,C,D,*INFO,*A_OUT,*B_OUT,*C_OUT,*D_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete B_OUT;
        delete C_OUT;
        delete D_OUT;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(D_OUT);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_sb10jd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 5;
    const int NB_OUTPUTS_EXPECTED = 5;
    const std::string FUNCTION_NAME = "slicot_sb10jd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[0]);

    if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix D = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix E = toMatrixBasic(inputs[4]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *D_OUT = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sb10jd(A,B,C,D,E,*INFO,*A_OUT,*B_OUT,*C_OUT,*D_OUT);

    if (!status.IsOk())
    {
        delete A_OUT;
        delete B_OUT;
        delete C_OUT;
        delete D_OUT;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(D_OUT);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_sg02ad(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 16;
    const int NB_OUTPUTS_EXPECTED = 10;
    const std::string FUNCTION_NAME = "slicot_sg02ad";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString DICO = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString JOBB = inputs[1].StringVal().c_str();

    if (!inputs[2].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a string expected."));
    }
    hwString FACT = inputs[2].StringVal().c_str();

    if (!inputs[3].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a string expected."));
    }
    hwString UPLO = inputs[3].StringVal().c_str();

    if (!inputs[4].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a string expected."));
    }
    hwString JOBL = inputs[4].StringVal().c_str();

    if (!inputs[5].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a string expected."));
    }
    hwString SCAL = inputs[5].StringVal().c_str();

    if (!inputs[6].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a string expected."));
    }
    hwString SORT = inputs[6].StringVal().c_str();

    if (!inputs[7].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a string expected."));
    }
    hwString ACC = inputs[7].StringVal().c_str();

    if (!inputs[8].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(9) + std::string(": a scalar expected."));
    }
    hwMatrix P = getDoubleScalarAsMatrixBasic(inputs[8]);

    if (!inputs[9].IsMatrix() && !inputs[9].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(10) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[9]);

    if (!inputs[10].IsMatrix() && !inputs[10].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(11) + std::string(": a matrix expected."));
    }
    hwMatrix E = toMatrixBasic(inputs[10]);

    if (!inputs[11].IsMatrix() && !inputs[11].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(12) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[11]);

    if (!inputs[12].IsMatrix() && !inputs[12].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(13) + std::string(": a matrix expected."));
    }
    hwMatrix Q = toMatrixBasic(inputs[12]);

    if (!inputs[13].IsMatrix() && !inputs[13].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(14) + std::string(": a matrix expected."));
    }
    hwMatrix R = toMatrixBasic(inputs[13]);

    if (!inputs[14].IsMatrix() && !inputs[14].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(15) + std::string(": a matrix expected."));
    }
    hwMatrix L = toMatrixBasic(inputs[14]);

    if (!inputs[15].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(16) + std::string(": a scalar expected."));
    }
    hwMatrix TOL = getDoubleScalarAsMatrixBasic(inputs[15]);

    hwMatrix *RCONDU = new hwMatrix;
    hwMatrix *X = new hwMatrix;
    hwMatrix *ALFAR = new hwMatrix;
    hwMatrix *ALFAI = new hwMatrix;
    hwMatrix *BETA = new hwMatrix;
    hwMatrix *S = new hwMatrix;
    hwMatrix *T = new hwMatrix;
    hwMatrix *U = new hwMatrix;
    hwMatrix *IWARN = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sg02ad( DICO,JOBB,FACT,UPLO,JOBL,SCAL,SORT,ACC,P,A,E,B,Q,R,L,*RCONDU,*X,*ALFAR,*ALFAI,*BETA,*S,*T,*U,TOL,*IWARN,*INFO );

    if (!status.IsOk())
    {
        delete RCONDU;
        delete X;
        delete ALFAR;
        delete ALFAI;
        delete BETA;
        delete S;
        delete T;
        delete U;
        delete IWARN;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(RCONDU);
    outputs.push_back(X);
    outputs.push_back(ALFAR);
    outputs.push_back(ALFAI);
    outputs.push_back(BETA);
    outputs.push_back(S);
    outputs.push_back(T);
    outputs.push_back(U);
    outputs.push_back(IWARN);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_sb02od(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 13;
    const int NB_OUTPUTS_EXPECTED = 9;
    const std::string FUNCTION_NAME = "slicot_sb02od";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString DICO = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString JOBB = inputs[1].StringVal().c_str();

    if (!inputs[2].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a string expected."));
    }
    hwString FACT = inputs[2].StringVal().c_str();

    if (!inputs[3].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a string expected."));
    }
    hwString UPLO = inputs[3].StringVal().c_str();

    if (!inputs[4].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a string expected."));
    }
    hwString JOBL = inputs[4].StringVal().c_str();

    if (!inputs[5].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a string expected."));
    }
    hwString SORT = inputs[5].StringVal().c_str();

    if (!inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a scalar expected."));
    }
    hwMatrix P = getDoubleScalarAsMatrixBasic(inputs[6]);

    if (!inputs[7].IsMatrix() && !inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[7]);

    if (!inputs[8].IsMatrix() && !inputs[8].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(9) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[8]);

    if (!inputs[9].IsMatrix() && !inputs[9].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(10) + std::string(": a matrix expected."));
    }
    hwMatrix Q = toMatrixBasic(inputs[9]);

    if (!inputs[10].IsMatrix() && !inputs[10].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(11) + std::string(": a matrix expected."));
    }
    hwMatrix R = toMatrixBasic(inputs[10]);

    if (!inputs[11].IsMatrix() && !inputs[11].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(12) + std::string(": a matrix expected."));
    }
    hwMatrix L = toMatrixBasic(inputs[11]);

    if (!inputs[12].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(13) + std::string(": a scalar expected."));
    }
    hwMatrix TOL = getDoubleScalarAsMatrixBasic(inputs[12]);

    hwMatrix *RCONDU = new hwMatrix;
    hwMatrix *X = new hwMatrix;
    hwMatrix *ALFAR = new hwMatrix;
    hwMatrix *ALFAI = new hwMatrix;
    hwMatrix *BETA = new hwMatrix;
    hwMatrix *S = new hwMatrix;
    hwMatrix *T = new hwMatrix;
    hwMatrix *U = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_sb02od( DICO,JOBB,FACT,UPLO,JOBL,SORT,P,A,B,Q,R,L,*RCONDU,*X,*ALFAR,*ALFAI,*BETA,*S,*T,*U,TOL,*INFO );

    if (!status.IsOk())
    {
        delete RCONDU;
        delete X;
        delete ALFAR;
        delete ALFAI;
        delete BETA;
        delete S;
        delete T;
        delete U;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(RCONDU);
    outputs.push_back(X);
    outputs.push_back(ALFAR);
    outputs.push_back(ALFAI);
    outputs.push_back(BETA);
    outputs.push_back(S);
    outputs.push_back(T);
    outputs.push_back(U);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_ab13ad(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 6;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "slicot_ab13ad";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString DICO = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString EQUIL = inputs[1].StringVal().c_str();

    if (!inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a scalar expected."));
    }
    hwMatrix ALPHA = getDoubleScalarAsMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[5]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *NS = new hwMatrix;
    hwMatrix *HSV = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_ab13ad( DICO,EQUIL,ALPHA,A,B,C,*NS,*HSV,*INFO,*A_OUT,*B_OUT,*C_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete B_OUT;
        delete C_OUT;
        delete NS;
        delete HSV;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(NS);
    outputs.push_back(HSV);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_mb03rd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 6;
    const int NB_OUTPUTS_EXPECTED = 7;
    const std::string FUNCTION_NAME = "slicot_mb03rd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString JOBX = inputs[0].StringVal().c_str();

    if (!inputs[1].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a string expected."));
    }
    hwString SORT = inputs[1].StringVal().c_str();

    if (!inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a scalar expected."));
    }
    hwMatrix PMAX = getDoubleScalarAsMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix X = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a scalar expected."));
    }
    hwMatrix TOL = getDoubleScalarAsMatrixBasic(inputs[5]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *X_OUT = new hwMatrix;
    hwMatrix *NBLCKS = new hwMatrix;
    hwMatrix *BLSIZE = new hwMatrix;
    hwMatrix *WR = new hwMatrix;
    hwMatrix *WI = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_mb03rd( JOBX,SORT,PMAX,A,X,*NBLCKS,*BLSIZE,*WR,*WI,TOL,*INFO,*A_OUT,*X_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete X_OUT;
        delete NBLCKS;
        delete BLSIZE;
        delete WR;
        delete WI;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(X_OUT);
    outputs.push_back(NBLCKS);
    outputs.push_back(BLSIZE);
    outputs.push_back(WR);
    outputs.push_back(WI);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_ab05pd(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 10;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "slicot_ab05pd";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString OVER = inputs[0].StringVal().c_str();

    if (!inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a scalar expected."));
    }
    hwMatrix ALPHA = getDoubleScalarAsMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix A1 = toMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix B1 = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix C1 = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix D1 = toMatrixBasic(inputs[5]);

    if (!inputs[6].IsMatrix() && !inputs[6].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(7) + std::string(": a matrix expected."));
    }
    hwMatrix A2 = toMatrixBasic(inputs[6]);

    if (!inputs[7].IsMatrix() && !inputs[7].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(8) + std::string(": a matrix expected."));
    }
    hwMatrix B2 = toMatrixBasic(inputs[7]);

    if (!inputs[8].IsMatrix() && !inputs[8].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(9) + std::string(": a matrix expected."));
    }
    hwMatrix C2 = toMatrixBasic(inputs[8]);

    if (!inputs[9].IsMatrix() && !inputs[9].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(10) + std::string(": a matrix expected."));
    }
    hwMatrix D2 = toMatrixBasic(inputs[9]);

    hwMatrix *N = new hwMatrix;
    hwMatrix *A = new hwMatrix;
    hwMatrix *B = new hwMatrix;
    hwMatrix *C = new hwMatrix;
    hwMatrix *D = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_ab05pd( OVER,ALPHA,A1,B1,C1,D1,A2,B2,C2,D2,*N,*A,*B,*C,*D,*INFO );

    if (!status.IsOk())
    {
        delete N;
        delete A;
        delete B;
        delete C;
        delete D;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(N);
    outputs.push_back(A);
    outputs.push_back(B);
    outputs.push_back(C);
    outputs.push_back(D);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_tb01id(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 5;
    const int NB_OUTPUTS_EXPECTED = 6;
    const std::string FUNCTION_NAME = "slicot_tb01id";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString JOB = inputs[0].StringVal().c_str();

    if (!inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a scalar expected."));
    }
    hwMatrix MAXRED = getDoubleScalarAsMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[4]);

    hwMatrix *MAXRED_OUT = new hwMatrix;
    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *SCALE = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_tb01id( JOB,MAXRED,A,B,C,*SCALE,*INFO,*MAXRED_OUT,*A_OUT,*B_OUT,*C_OUT );

    if (!status.IsOk())
    {
        delete MAXRED_OUT;
        delete A_OUT;
        delete B_OUT;
        delete C_OUT;
        delete SCALE;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(MAXRED_OUT);
    outputs.push_back(A_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(SCALE);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_tg01ad(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    const int NB_INPUTS_EXPECTED = 6;
    const int NB_OUTPUTS_EXPECTED = 7;
    const std::string FUNCTION_NAME = "slicot_tg01ad";

    if (inputs.size() != NB_INPUTS_EXPECTED)
    {
        std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
        throw OML_Error(std::string(msg));
    }

    int nargout = eval.GetNargoutValue();
    if ((nargout == 0) || (nargout > NB_OUTPUTS_EXPECTED))
    {
        std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
        throw OML_Error(std::string(msg));
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(1) + std::string(": a string expected."));
    }
    hwString JOB = inputs[0].StringVal().c_str();

    if (!inputs[1].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(2) + std::string(": a scalar expected."));
    }
    hwMatrix THRESH = getDoubleScalarAsMatrixBasic(inputs[1]);

    if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(3) + std::string(": a matrix expected."));
    }
    hwMatrix A = toMatrixBasic(inputs[2]);

    if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(4) + std::string(": a matrix expected."));
    }
    hwMatrix E = toMatrixBasic(inputs[3]);

    if (!inputs[4].IsMatrix() && !inputs[4].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(5) + std::string(": a matrix expected."));
    }
    hwMatrix B = toMatrixBasic(inputs[4]);

    if (!inputs[5].IsMatrix() && !inputs[5].IsScalar())
    {
        throw OML_Error(FUNCTION_NAME + std::string(": Input argument #") + SSTR(6) + std::string(": a matrix expected."));
    }
    hwMatrix C = toMatrixBasic(inputs[5]);

    hwMatrix *A_OUT = new hwMatrix;
    hwMatrix *E_OUT = new hwMatrix;
    hwMatrix *B_OUT = new hwMatrix;
    hwMatrix *C_OUT = new hwMatrix;
    hwMatrix *LSCALE = new hwMatrix;
    hwMatrix *RSCALE = new hwMatrix;
    hwMatrix *INFO = new hwMatrix;

    hwMathStatus status = slicot_tg01ad( JOB,THRESH,A,E,B,C,*LSCALE,*RSCALE,*INFO,*A_OUT,*E_OUT,*B_OUT,*C_OUT );

    if (!status.IsOk())
    {
        delete A_OUT;
        delete E_OUT;
        delete B_OUT;
        delete C_OUT;
        delete LSCALE;
        delete RSCALE;
        delete INFO;
        throw OML_Error(FUNCTION_NAME + std::string(" failed"));
        return false;
    }

    outputs.push_back(A_OUT);
    outputs.push_back(E_OUT);
    outputs.push_back(B_OUT);
    outputs.push_back(C_OUT);
    outputs.push_back(LSCALE);
    outputs.push_back(RSCALE);
    outputs.push_back(INFO);
    return true;

}
//=============================================================================
bool oml_slicot_tb05ad_1(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
	const int NB_INPUTS_EXPECTED = 3;
	const int NB_OUTPUTS_EXPECTED = 6;
	const std::string FUNCTION_NAME = "slicot_tb05ad_1";

	if (inputs.size() != NB_INPUTS_EXPECTED)
	{
		std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
		throw OML_Error(std::string(msg));
	}

	if (eval.GetNargoutValue() != NB_OUTPUTS_EXPECTED)
	{
		std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
		throw OML_Error(std::string(msg));
	}

	if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
		throw OML_Error(OML_ERR_MATRIX, 1, OML_VAR_TYPE);

	if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
		throw OML_Error(OML_ERR_MATRIX, 2, OML_VAR_TYPE);

	if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
		throw OML_Error(OML_ERR_MATRIX, 3, OML_VAR_TYPE);

	const hwMatrix* A = inputs[0].ConvertToMatrix();
	const hwMatrix* B = inputs[1].ConvertToMatrix();
	const hwMatrix* C = inputs[2].ConvertToMatrix();

	std::unique_ptr<hwMatrix> AB(EvaluatorInterface::allocateMatrix(A));
    std::unique_ptr<hwMatrix> BB(EvaluatorInterface::allocateMatrix(B));
    std::unique_ptr<hwMatrix> CB(EvaluatorInterface::allocateMatrix(C));
    std::unique_ptr<hwMatrix> EVRE(EvaluatorInterface::allocateMatrix());
	std::unique_ptr<hwMatrix> EVIM(EvaluatorInterface::allocateMatrix());
	int ldwork;

	hwMathStatus status;
	
	status = slicot_tb05ad(*AB, *BB, *CB, *EVRE, *EVIM, ldwork);

	if (!status.IsOk())
	{
		throw OML_Error(FUNCTION_NAME + std::string(" failed"));
		return false;
	}

	outputs.push_back(AB.release());
    outputs.push_back(BB.release());
    outputs.push_back(CB.release());
    outputs.push_back(EVRE.release());
	outputs.push_back(EVIM.release());
	outputs.push_back(ldwork);

	return true;
}
//=============================================================================
bool oml_slicot_tb05ad_2(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
	const int NB_INPUTS_EXPECTED = 5;
	const int NB_OUTPUTS_EXPECTED = 2;
	const std::string FUNCTION_NAME = "slicot_tb05ad_2";

	if (inputs.size() != NB_INPUTS_EXPECTED)
	{
		std::string msg = FUNCTION_NAME + std::string(": function requires ") + SSTR(NB_INPUTS_EXPECTED) + std::string(" input(s).");
		throw OML_Error(std::string(msg));
	}

	if (eval.GetNargoutValue() != NB_OUTPUTS_EXPECTED)
	{
		std::string msg = FUNCTION_NAME + std::string(": wrong number of output arguments.");
		throw OML_Error(std::string(msg));
	}

	if (!inputs[0].IsMatrix() && !inputs[0].IsScalar())
		throw OML_Error(OML_ERR_MATRIX, 1, OML_VAR_TYPE);

	if (!inputs[1].IsMatrix() && !inputs[1].IsScalar())
		throw OML_Error(OML_ERR_MATRIX, 2, OML_VAR_TYPE);

	if (!inputs[2].IsMatrix() && !inputs[2].IsScalar())
		throw OML_Error(OML_ERR_MATRIX, 3, OML_VAR_TYPE);

	if (!inputs[3].IsMatrix() && !inputs[3].IsScalar())
		throw OML_Error(OML_ERR_MATRIX, 4, OML_VAR_TYPE);

	if (!inputs[4].IsPositiveInteger())
		throw OML_Error(OML_ERR_POSINTEGER, 5, OML_VAR_TYPE);

	const hwMatrix* F = inputs[0].ConvertToMatrix();
	const hwMatrix* A = inputs[1].ConvertToMatrix();
	const hwMatrix* B = inputs[2].ConvertToMatrix();
	const hwMatrix* C = inputs[3].ConvertToMatrix();
	int ldwork = static_cast<int> (inputs[4].Scalar());
	double RCOND;
	std::unique_ptr<hwMatrix> G(EvaluatorInterface::allocateMatrix());

	hwMathStatus status;

	status = slicot_tb05ad(*F, *A, *B, *C, RCOND, *G, ldwork);

	if (!status.IsOk())
	{
		throw OML_Error(FUNCTION_NAME + std::string(" failed"));
		return false;
	}

	outputs.push_back(RCOND);
	outputs.push_back(G.release());

	return true;
}
//=============================================================================
