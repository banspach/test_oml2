/**
* @file omlziputils.cxx
* @date March 2018
* Copyright (C) 2018 - 2022 Altair Engineering, Inc. All rights reserved. 
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret 
* Information. Not for use or disclosure outside of Licensee's organization. 
* The software and information contained herein may only be used internally and 
* is provided on a non-exclusive, non-transferable basis.  License may not 
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly 
* perform the software or other information provided herein, nor is Licensee 
* permitted to decompile, reverse engineer, or disassemble the software. Usage 
* of the software and other information provided by Altair (or its resellers) is 
* only as explicitly stated in the applicable end user license agreement between 
* Altair and Licensee. In the absence of such agreement, the Altair standard end 
* user license agreement terms shall govern.
*/
#include "omlziputils.h"

#include <cassert>
#include <cerrno>
#include <fstream>
#include <sstream>
#include <memory>


#include "unzip.h"
#include "zip.h"
#include "hwMatrix_NMKL.h"

#ifdef OS_WIN
#   include <wchar.h>
#   include <sys/stat.h>  

#   define USEWIN32IOAPI
#   include <Windows.h>
#   include "iowin32.h"
#else
#   include <dirent.h>
#   include <limits.h>
#   include <sys/types.h>
#   include <unistd.h>
#   ifdef PATH_MAX
#       define MAX_PATH PATH_MAX
#   else
#       define MAX_PATH 260
#   endif
#endif

#include "BuiltInFuncsUtils.h"
#include "OML_Error.h"

//------------------------------------------------------------------------------
// Returns true if successful in extracting the contents of given archive
//------------------------------------------------------------------------------
Currency OmlZipUtils::Unzip(const std::string& zipfile,
                            const std::string& dir,
                            bool               getfiles,
                            std::string&       warn)
{
    BuiltInFuncsUtils utils;

    // Make sure file exists before calling this method
    assert(utils.DoesPathExist(zipfile));

#ifdef OS_WIN
    std::wstring wstr    = utils.StdString2WString(zipfile);
    std::wstring abspath = utils.GetAbsolutePathW(wstr);
#else 
    std::string abspath = utils.GetAbsolutePath(zipfile);
#endif

    // Strip leading and trailing spaces
    std::string unzipdir(utils.LTrim(dir)); 
    unzipdir = utils.RTrim(unzipdir);
#ifndef OS_WIN
    if (unzipdir.empty())
    {
        unzipdir = utils.GetCurrentWorkingDir();
    }
    utils.Mkdir(unzipdir);
#endif
    utils.AddTrailingSlash(unzipdir);

    // Open the archive
#ifdef OS_WIN
    zlib_filefunc64_def funcdef;
    fill_win32_filefunc64W(&funcdef);

    unzFile zfile = unzOpen2_64((LPWSTR)abspath.c_str(), &funcdef); 

#else
    unzFile zfile = unzOpen64(abspath.c_str());
#endif

    if (!zfile)
    {
        throw OML_Error("Error: cannot open [" + zipfile + "]");
    }

    unz_global_info info;
    if (unzGetGlobalInfo (zfile, &info) != UNZ_OK)
    {
        unzClose(zfile);
        throw OML_Error("Error: cannot get information from [" + zipfile + "]");
    }

    int  count         = static_cast<int>(info.number_entry);
    bool fileextracted = false;
    int  status        = UNZ_OK;

    std::unique_ptr<HML_CELLARRAY> cell = nullptr;
    if (getfiles)
    {
        cell = std::unique_ptr<HML_CELLARRAY>(EvaluatorInterface::allocateCellArray(1, count));
    }
    std::ios_base::openmode mode = std::ios_base::out | std::ios_base::trunc | std::ios_base::binary;

    for (int i = 0; i < count && status == UNZ_OK; ++i)
    {
        char cname[MAX_PATH];
        memset(cname, 0, sizeof(cname));
        unz_file_info finfo;
        status = unzGetCurrentFileInfo(zfile, &finfo, cname, sizeof(cname), 
                 nullptr, 0, nullptr, 0);
        std::string fname (cname);
        void* data = nullptr;
        if (status != UNZ_OK || fname.empty())
        {
            warn += "\nCannot get file information [" + fname + "]";
        }
        else if (HasTrailingSlash(fname))  
        {   
            utils.Mkdir(unzipdir + fname); // This is a directory
            if (cell)
            {
                // Replace with forward slashes so output is consistent on all platforms
                std::string newpath(fname);
                std::replace(newpath.begin(), newpath.end(), '\\', '/');
                (*cell)(i) = fname;
            }
        }
        else
        {
            unsigned int fsize  = static_cast<unsigned int>(finfo.uncompressed_size);
            int numread = 0;
            if (utils.DoesPathExist(unzipdir + fname))
            {
                std::remove((unzipdir + fname).c_str());
            }

            status = unzOpenCurrentFile(zfile);
            if (status != UNZ_OK)
            {
                break;
            }
           size_t pos = fname.find_last_of("/\\");
            if (pos != std::string::npos && pos != 0)
            {
                std::string basedir = fname.substr(0, pos);
                utils.Mkdir(unzipdir + basedir);
            }
            
            if (fsize > 0)
            {
                // new causes a crash when deleting in certain cases with unzip
                data = malloc(fsize);  
                numread = unzReadCurrentFile(zfile, data, fsize);
            }
            std::string errstr;
            std::string name(unzipdir + fname);
            name = utils.Normpath(name);
            FILE* fp = OpenFile(name, "wb", errstr);
            if (!fp)
            {
                warn += "\nCannot extract; " + errstr + ": [" + name + "]";
            }
            else if (numread > 0)
            {
                fwrite(data, sizeof(char), fsize, fp);
                fflush(fp);
            }
            if (fp)
            {
                fclose(fp);
                if (cell)
                {
                    // Replace with forward slashes so output is consistent on all platforms
                    std::string newpath(fname);
                    std::replace(newpath.begin(), newpath.end(), '\\', '/');
                    (*cell)(i) = newpath;
                }
            }
        }

        unzCloseCurrentFile(zfile);

        if (data)
        { 
            free(data);
            data = nullptr;
        }
        status = unzGoToNextFile(zfile);
    }

    unzClose(zfile);

    if (cell)
    {
        return cell.release();
    }
    return EvaluatorInterface::allocateCellArray();
}
//------------------------------------------------------------------------------
// Returns true if the given path has trailing slash
//------------------------------------------------------------------------------
bool OmlZipUtils::HasTrailingSlash(const std::string& path)
{
    if (path.empty())
    {
        return false;
    }

    return (path[path.size() - 1] == '\\' || path[path.size() - 1] == '/');
}
//------------------------------------------------------------------------------
// Zips files and returns file list, if applicable
//------------------------------------------------------------------------------
 Currency OmlZipUtils::Zip(const std::string& name,
                           const std::string& dir,
                           bool               overwrite,
                           bool               getfilelist,
                           const Currency&    cur,
                           std::string&       warn)
{
    int mode  = (overwrite) ? APPEND_STATUS_CREATE : APPEND_STATUS_ADDINZIP;

    BuiltInFuncsUtils utils;
#ifdef OS_WIN
    std::wstring wbasedir;
    if (!dir.empty())
    {
        std::wstring wstr = utils.StdString2WString(dir);
        wbasedir = utils.GetAbsolutePathW(wstr);
        utils.AddTrailingSlashW(wbasedir);
    }
    return ZipW(name, wbasedir, mode, getfilelist, cur, warn);
#else

    zipFile zfile = zipOpen64(name.c_str(), mode);
    if (!zfile)
    {
        std::string action = (mode == APPEND_STATUS_ADDINZIP) ? "update" : "create";
        throw OML_Error("Cannot " + action + " archive [" + name + "] in argument 1");
    }

    HML_CELLARRAY* cell = cur.CellArray();
    assert(cell);
    int csize = cell->Size();

    std::string basedir;
    std::string cwd (utils.GetCurrentWorkingDir());
    utils.AddTrailingSlash(cwd);
    cwd = utils.Normpath(cwd);

    if (!dir.empty())
    {
        basedir = dir;
        utils.AddTrailingSlash(basedir);
        basedir = utils.Normpath(basedir);
    }
   
    std::vector<std::string> abspathfiles;
    std::vector<std::string> relpathfiles;
    abspathfiles.reserve(csize);
    relpathfiles.reserve(csize);
    for (int i = 0; i < csize; ++i)
    {
        Currency tmp = (*cell)(i);
        if (!tmp.IsString())
        {
            zipClose(zfile, 0);
            throw OML_Error(OML_ERR_STRING_STRINGCELL, 2, OML_VAR_VALUE);
        }
        std::string val (tmp.StringVal());
        if (val.empty())
        {
            continue;
        }
        val = utils.Normpath(val);

        bool isabspath = true;
        std::string path = utils.GetAbsolutePath(val);
        if (path.empty())
        {
            size_t pos1 = val.find_last_of("/");
            if (pos1 != std::string::npos)
            {
                path = utils.GetAbsolutePath(val.substr(0, pos1));
                path += val.substr(pos1);
            }
            else
            {
                path = basedir + val;
            }
        }
        
        GetFiles(path, basedir, name, cwd, isabspath, abspathfiles, relpathfiles);
    }

    assert(abspathfiles.size() == relpathfiles.size());

    int i = 0;
    
    std::vector<std::string> vecfiles;
    vecfiles.reserve(abspathfiles.size());
    for (std::vector<std::string>::const_iterator itr = abspathfiles.begin();
         itr != abspathfiles.end(); ++itr, ++i)
    {
        std::string abspath (*itr);
        if (abspath.empty())
        {
            continue;
        }
        if (name == abspath)
        {
            continue;  // This is the zip file that is being created
        }
        bool isemptydir = (abspath.back() == '\\' || abspath.back() == '/');

        std::string relpath = relpathfiles[i];

        // Read contents
        std::string errstr;
        FILE* file = nullptr; 
        if (!isemptydir)
        {
            file = OpenFile(abspath, "rb", errstr);
            if (!file)
            {
                warn += "\nCannot read file; " + errstr + ": [" + abspath + "]";
                continue;
            }
        }
        
        zip_fileinfo zi = {0};
        int err = zipOpenNewFileInZip(zfile, relpath.c_str(), &zi,
				NULL, 0, NULL, 0, NULL, Z_DEFLATED, Z_DEFAULT_COMPRESSION );
        if (err != ZIP_OK)
        {
            warn += "\nCannot archive [" + abspath + "]";
            if (file)
            {
                fclose(file);
            }
            continue;
        }

        if (file)
        {
            std::string buff;

            // Get the file size
            fseek(file, 0, SEEK_END);
            long filesize = ftell(file);
            rewind(file);

            char* data = new char[filesize + 1];

            if (!data)
            {
                fclose(file);
                warn += "\nCannot allocate memory to read [" + abspath + "]";
                continue;
            }
            memset(data, 0, sizeof(data));
            size_t result = fread(data, 1, filesize, file);
            if (result != filesize)
            {
                fclose(file);
                warn += "\nRead error with [" + abspath + "]";
                continue;
            }
            err = zipWriteInFileInZip(zfile, data, static_cast<int>(filesize));
            delete[] data;
            data = nullptr;
            fclose(file);
        }
        zipCloseFileInZip(zfile); 

        vecfiles.push_back(relpath);
    }
    zipClose(zfile, 0);
    
    if (vecfiles.empty() || !getfilelist)
    {
        return EvaluatorInterface::allocateCellArray();
    }

    int numfiles = static_cast<int>(vecfiles.size());
    HML_CELLARRAY* out = EvaluatorInterface::allocateCellArray(
                          static_cast<int>(vecfiles.size()), 1);
    for (i = 0; i < numfiles; ++i)
    {
        (*out)(i, 0) = vecfiles[i];
    }
    return out;
#endif
}
//------------------------------------------------------------------------------
// Opens file and returns file pointer
//------------------------------------------------------------------------------
FILE* OmlZipUtils::OpenFile(const std::string& name, 
                            const std::string& mode,
                            std::string&       err)
{
    assert(!name.empty());

    BuiltInFuncsUtils utils;
#ifdef OS_WIN
    return OpenFileW(utils.StdString2WString(name), 
                     utils.StdString2WString(mode), err);

#else  

    assert(!name.empty());

    FILE* fp = fopen(name.c_str(), mode.c_str());
    if (!fp)
    {
        char* errstr = strerror(errno);
        if (errstr)
        {
            err = errstr;
        }
    }

    return fp;
#endif
}
//------------------------------------------------------------------------------
// Returns true if given path is a directory, supports unicode on Windows
//------------------------------------------------------------------------------
bool OmlZipUtils::IsDir(const std::string& path)
{
    BuiltInFuncsUtils utils;
#ifndef OS_WIN
    return utils.IsDir(path);

#else  

    if (path.empty()) 
    {
        return false;
    }
    std::string dir (utils.Normpath(path));
    utils.StripTrailingSlash(dir);

    std::wstring widestr = utils.StdString2WString(dir);
    struct _stat64i32 filestat;
    if (_wstat(widestr.c_str(), &filestat) == -1)
    {
        return false;
    }

    int returncode = (filestat.st_mode & _S_IFDIR);
    if (returncode == 0)
    {
        return false;
    }
    return true;
#endif
}
//------------------------------------------------------------------------------
// Deletes given file, supports Unicode
//------------------------------------------------------------------------------
bool OmlZipUtils::Remove(const std::string& path)
{
    BuiltInFuncsUtils utils;
#ifndef OS_WIN
    if (std::remove(path.c_str()))
    {
        return false;
    }
#else  
    std::wstring wstr = utils.StdString2WString(path);
    if (_wremove(wstr.c_str()) == -1)
    {
        return false;
    }
#endif
    return true;
}

#ifdef OS_WIN
//------------------------------------------------------------------------------
// Opens file and returns file pointer
//------------------------------------------------------------------------------
FILE* OmlZipUtils::OpenFileW(const std::wstring& name, 
                             const std::wstring& mode,
                             std::string&       err)
{
    FILE* fp = _wfopen (name.c_str(), mode.c_str());
    if (!fp)
    {
        char* errstr = strerror(errno);
        if (errstr)
        {
            err = errstr;
        }
    }

    return fp;
}
//------------------------------------------------------------------------------
// Zips files and returns file list, if applicable
//------------------------------------------------------------------------------
 Currency OmlZipUtils::ZipW(const std::string&  name,
                            const std::wstring& dir,
                            int                 mode,
                            bool                getfilelist,
                            const Currency&     cur,
                            std::string&        warn)
{
    BuiltInFuncsUtils utils;

    std::wstring wname = utils.StdString2WString(name);
    wname = utils.GetNormpathW(wname);

    zlib_filefunc64_def funcdef;
    fill_win32_filefunc64W(&funcdef);

    zipFile zfile = zipOpen2_64((LPWSTR)wname.c_str(), mode, nullptr, &funcdef);
    if (!zfile)
    {
        std::string action = (mode == APPEND_STATUS_ADDINZIP) ? "update" : "create";
        throw OML_Error("Cannot " + action + " archive [" + name + "] in argument 1");
    }

    HML_CELLARRAY* cell = cur.CellArray();
    assert(cell);
    int csize = cell->Size();

    std::wstring basedir;
    std::wstring cwd = utils.GetCurrentWorkingDirW();
    utils.AddTrailingSlashW(cwd);
    cwd = utils.GetNormpathW(cwd);

    if (!dir.empty())
    {
        basedir = dir;
        utils.AddTrailingSlashW(basedir);
        basedir = utils.GetNormpathW(basedir);
    }
    
    std::vector<std::wstring> abspathfiles;
    std::vector<std::wstring> relpathfiles;
    abspathfiles.reserve(csize);
    relpathfiles.reserve(csize);

    std::wstring wmsg;
    for (int i = 0; i < csize; ++i)
    {
        Currency tmp = (*cell)(i);
        if (!tmp.IsString())
        {
            zipClose(zfile, 0);
            throw OML_Error(OML_ERR_STRING_STRINGCELL, 2, OML_VAR_VALUE);
        }
        std::string val (tmp.StringVal());
        if (val.empty())
        {
            continue;
        }
        std::wstring wval = utils.StdString2WString(val);
        wval = utils.GetNormpathW(wval);

        bool isabspath = true;
        std::wstring path;

        isabspath = (wval.length() > 1 && wval[1] == ':');

        path = (!isabspath) ? basedir + wval : wval;
        GetFiles(path, basedir, wname, cwd, isabspath, abspathfiles, 
                 relpathfiles, wmsg);
    }

    assert(abspathfiles.size() == relpathfiles.size());

    if (!wmsg.empty())
    {
        warn += utils.WString2StdString(wmsg);
    }
    int i = 0;
    
    std::vector<std::string> vecfiles;
    vecfiles.reserve(abspathfiles.size());
    for (std::vector<std::wstring>::const_iterator itr = abspathfiles.begin();
         itr != abspathfiles.end(); ++itr, ++i)
    {
        std::wstring abspath (*itr);
        if (abspath.empty())
        {
            continue;
        }
        if (wname == abspath)
        {
            continue;  // This is the zip file that is being created
        }

        bool isemptydir = (abspath.back() == L'\\' || abspath.back() == L'/');

        std::wstring relpath = relpathfiles[i];

        // Read contents
        std::string errstr;

        std::string absfilestr = utils.WString2StdString(abspath);
        FILE* file = nullptr;
        

        if (!isemptydir)
        {
            file = OpenFileW(abspath, std::wstring(L"rb"), errstr);
            if (!file)
            {
                warn += "\nCannot read file; " + errstr + ": [" + absfilestr + "]";
                continue;
            }
        }
        
        zip_fileinfo zi = {0};
		time_t rawtime;
		time (&rawtime);
		struct tm* timeinfo = localtime(&rawtime);
		zi.tmz_date.tm_sec = timeinfo->tm_sec;
		zi.tmz_date.tm_min = timeinfo->tm_min;
		zi.tmz_date.tm_hour = timeinfo->tm_hour;
		zi.tmz_date.tm_mday = timeinfo->tm_mday;
		zi.tmz_date.tm_mon = timeinfo->tm_mon;
		zi.tmz_date.tm_year = timeinfo->tm_year;

        std::string relfilestr (utils.WString2StdString(relpath));
        std::replace(relfilestr.begin(), relfilestr.end(), '\\', '/');

        int err = zipOpenNewFileInZip(zfile, relfilestr.c_str(), &zi,
				NULL, 0, NULL, 0, NULL, Z_DEFLATED, Z_DEFAULT_COMPRESSION );
        if (err != ZIP_OK)
        {
            warn += "\nCannot archive [" + absfilestr + "]";
            fclose(file);
            continue;
        }

        if (file)
        {
            std::string buff;

            // Get the file size
            fseek(file, 0, SEEK_END);
            long filesize = ftell(file);
            rewind(file);

            wchar_t* data = new wchar_t[filesize + 1];
            if (!data)
            {
                fclose(file);
                warn += "\nCannot allocate memory to read [" + absfilestr + "]";
                continue;
            }
            size_t result = fread(data, 1, filesize, file);
            if (result != filesize)
            {
                fclose(file);
                warn += "\nRead error with [" + absfilestr + "]";
                continue;
            }
            err = zipWriteInFileInZip(zfile, data, static_cast<int>(filesize));

            delete[] data;
            data = nullptr;
            fclose(file);
        }
        vecfiles.push_back(relfilestr);
    }
    zipClose(zfile, 0);
    
    if (vecfiles.empty() || !getfilelist)
    {
        return EvaluatorInterface::allocateCellArray();
    }

    int numfiles = static_cast<int>(vecfiles.size());
    HML_CELLARRAY* out = EvaluatorInterface::allocateCellArray(
                          static_cast<int>(vecfiles.size()), 1);
    for (i = 0; i < numfiles; ++i)
    {
        (*out)(i, 0) = vecfiles[i];
    }
    return out;
}
//------------------------------------------------------------------------------
// Gets a list of all files (recursively) in the given directory
//------------------------------------------------------------------------------
void OmlZipUtils::GetFiles(const std::wstring&        dir,
                           const std::wstring&        parentdir,
                           const std::wstring&        ignorefile,
                           const std::wstring&        cwd,
                           bool                      isabspath,
                           std::vector<std::wstring>& abspath,
                           std::vector<std::wstring>& relpath,
                           std::wstring&              warn)
{
    BuiltInFuncsUtils utils;

    bool isdir  = HasTrailingSlashW(dir);
    bool exists = utils.DoesPathExistW(dir);

    std::wstring dirtolist = dir;

    if (!isdir && exists)  // Path may not have trailing slash
    {
        struct _stat st;
        int    dirval = _wstat(dir.c_str(), &st);
        if (dirval == 0 && (st.st_mode & S_IFDIR))
        {
            isdir = true;
            utils.AddTrailingSlashW(dirtolist);
        }
    }

    if (!isdir && exists)
    {
        abspath.push_back(utils.GetAbsolutePathW(dir));

        if (!parentdir.empty() && !isabspath)
        {
            relpath.push_back(GetRelativePathW(dir, parentdir));
        }
        else
        {
            std::wstring tmp = (!parentdir.empty()) ? parentdir : cwd;
			std::wstring wdir = dir;

            std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::towlower);
			std::transform(wdir.begin(), wdir.end(), wdir.begin(), ::towlower);

            size_t pos = wdir.find(tmp);

            // Use the original string so capitalization is not lost
            if (pos == std::wstring::npos)
            {
                relpath.push_back(GetBaseNameW(dir));
            }
            else
            {
                relpath.push_back(dir.substr(pos + tmp.length()));
            }
        }
        return;
    }
    else if (isdir && exists)
    {
        dirtolist = utils.GetAbsolutePathW(dir);
        utils.AddTrailingSlashW(dirtolist);
    }

    size_t oldlen = abspath.empty() ? 0 : abspath.size();

    std::wstring tmp = (!parentdir.empty()) ? parentdir : cwd;
    ListDir(tmp, dirtolist, ignorefile, abspath, relpath);

    size_t newlen = abspath.empty() ? 0 : abspath.size();

    if (!exists && oldlen == newlen)
    {
        warn += L"\nCannot read file; No such file or directory [" + dir + L"]";
    }
    else if (isdir && oldlen == newlen)
    {
        // Directory is empty
        abspath.push_back(utils.GetAbsolutePathW(dirtolist));

        if (!parentdir.empty() && !isabspath)
        {
            relpath.push_back(GetRelativePathW(dirtolist, parentdir));
        }
        else
        {
            std::wstring tmp = (!parentdir.empty()) ? parentdir : cwd;
            std::wstring wdir = dirtolist;

            std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::towlower);
            std::transform(wdir.begin(), wdir.end(), wdir.begin(), ::towlower);

            size_t pos = wdir.find(tmp);
            if (pos == std::wstring::npos)
            {
                relpath.push_back(GetBaseNameW(wdir));
            }
            else
            {
                relpath.push_back(wdir.substr(pos + tmp.length()));
            }
        }
    }
}
//------------------------------------------------------------------------------
// Lists contents of a directory recursively, supports Unicode
//------------------------------------------------------------------------------
void OmlZipUtils::ListDir(const std::wstring&        parent,
                          const std::wstring&        base,
                          const std::wstring&        ignorefile,
                          std::vector<std::wstring>& abspath,
                          std::vector<std::wstring>& relpath)
{
    BuiltInFuncsUtils utils;
    WIN32_FIND_DATAW  find_data;

    std::wstring dir = base;
    std::wstring subdir = base;
    bool haswildcard = true;
    if (base.find(L"*") == std::wstring::npos)
    {
        dir += L"\\*";
        haswildcard = false;
    }

    HANDLE hFind = FindFirstFileW((LPCWSTR)dir.c_str(), &find_data);
    while (hFind != INVALID_HANDLE_VALUE)
    {
        std::wstring name = find_data.cFileName;
        if (name == L"." || name == L"..")
        {
            // Ignore this
        }
        else if (find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            std::wstring recdir = (haswildcard) ? parent : base;
            utils.AddTrailingSlashW(recdir);
            recdir += name;

            size_t oldsize = (abspath.empty()) ? 0 : abspath.size();

            ListDir(parent, recdir, ignorefile, abspath, relpath);

            size_t newsize = (abspath.empty()) ? 0 : abspath.size();

            if (oldsize == newsize)  // This is an empty dir
            {
                std::wstring afile = utils.GetAbsolutePathW(recdir);
                std::wstring rfile = GetRelativePathW(afile, parent);

                utils.AddTrailingSlashW(afile);
                utils.AddTrailingSlashW(rfile);

                abspath.push_back(afile);
                relpath.push_back(rfile);
            }
        }
        else if (name != ignorefile)
        {
            std::wstring thisfile = (haswildcard) ? parent : subdir;
            utils.AddTrailingSlashW(thisfile);
            thisfile += name;

            std::wstring afile = utils.GetAbsolutePathW(thisfile);
            std::wstring rfile = GetRelativePathW(afile, parent);
            abspath.push_back(afile);
            relpath.push_back(rfile);
        }        

        if (FindNextFileW(hFind, &find_data) == 0)
        {
            break;
        }
    }
    FindClose(hFind);
}
//------------------------------------------------------------------------------
// Returns true if given path has a trailing slash
//------------------------------------------------------------------------------
bool OmlZipUtils::HasTrailingSlashW(const std::wstring& path)
{
    if (path.empty())
    {
        return false;
    }

    return (path[path.size() - 1] == '\\' || path[path.size() - 1] == '/');
}
//------------------------------------------------------------------------------
// Gets relative path
//------------------------------------------------------------------------------
std::wstring OmlZipUtils::GetRelativePathW(const std::wstring& path,
                                           const std::wstring& basedir)
{
	std::wstring wpath = path;
	std::wstring wbase = basedir;
#ifdef OS_WIN
	std::transform(wpath.begin(), wpath.end(), wpath.begin(), ::tolower);
	std::transform(wbase.begin(), wbase.end(), wbase.begin(), ::tolower);
#endif
    size_t pos = wpath.find(wbase);
    if (pos != std::wstring::npos)
    {
        // Use the original path name as we don't want to change case
        std::wstring relpath = path.substr(pos + basedir.length());
        return relpath;
    }
    return path;
}
//------------------------------------------------------------------------------
// Returns the base name for the given path, supports Unicode
//------------------------------------------------------------------------------
std::wstring OmlZipUtils::GetBaseNameW(const std::wstring& path)
{
    if (path.empty()) 
    {
        return path;
    }

    size_t pos = path.find_last_of(L"/\\");
    if (pos == std::wstring::npos)
    {
        return path;
    }

    return path.substr(pos+1);
}
#else
//------------------------------------------------------------------------------
// Gets a list of all files (recursively) in the given directory
//------------------------------------------------------------------------------
void OmlZipUtils::GetFiles(const std::string&        dir,
                           const std::string&        parentdir,
                           const std::string&        ignorefile,
                           const std::string&        cwd,
                           bool                      isabspath,
                           std::vector<std::string>& abspath,
                           std::vector<std::string>& relpath)
{
    size_t pos = dir.find("*");

    std::string base;
    std::string pattern;
    if (pos != std::string::npos)
    {
        std::string tmp = dir.substr(0, pos);
        if (!tmp.empty())
        {
            size_t pos2 = tmp.find_last_of("/");
            if (pos2 != 0 && pos2 != std::string::npos)
            {
                base = tmp.substr(0, pos2);
                pattern = tmp.substr(pos2+1);
            }
            else if (pos2 == std::string::npos)
            {
                pattern += tmp;
            }

        }
        pattern += dir.substr(pos);
    }
    
    if (pattern.empty())
    {
        pattern = "*";
        base    = dir;
    }
    else if (pattern == "*.*") // Need to detect empty directories too
    {
        pattern = "*";
    }
    if (base.empty())
    {
        base = (!cwd.empty()) ? cwd : ".";
    }

    std::string command = "find \"" + base + "\" -name \"" + pattern + "\" 2>&1";

    FILE* pipe = popen(command.c_str(), "r");
    if (!pipe)
    {
        throw OML_Error(HW_ERROR_PROBOPENPIPE);
    }

    BuiltInFuncsUtils utils;
    while (1)
    {
        char buf[256];
        if (fgets(buf, sizeof(buf), pipe) <= 0)
        {
            std::cout << std::flush;
            break;
        }
        std::cout << std::flush;
        std::string str (buf);
        utils.StripTrailingNewline(str);

        bool isdir = utils.IsDir(str);

        std::string rpath;
        
        if (!parentdir.empty() && !isabspath)
        {
            rpath = BuiltInFuncsUtils::GetRelativePath(str, parentdir);
        }
        else
        {
            std::string tmp = (!parentdir.empty()) ? parentdir : cwd;

            size_t pos = str.find(tmp);
            if (pos == std::string::npos)
            {
                rpath = BuiltInFuncsUtils::GetBaseName(str);
            }
            else
            {
                rpath = str.substr(pos + tmp.length());
            }
        }
            
        if (rpath.empty())
        {
            size_t pos2 = str.find_last_of("/");
            if (pos2 == std::string::npos)
            {
                pos2 = str.find_last_of("\\");
            }
            if (pos2 != std::string::npos)
            {
                rpath = str.substr(pos2);
            }
            else
            {
                rpath = str;
            }
        }
        if (rpath == ignorefile)
        {
            continue;
        }
        if (!rpath.empty() && rpath[0] == '/')
        {
            rpath.erase(rpath.begin());
        }
        if (isdir)
        {
            if (str.back() != '/')
            {
                str += "/";
            }
            if (rpath.back() != '/')
            {
                rpath += "/";
            }

            if (abspath.empty() ||
                std::find(abspath.begin(), abspath.end(), str) == abspath.end())
            {
                abspath.push_back(str);
                relpath.push_back(rpath);
            }
        }
        else
        {
            abspath.push_back(str);
            relpath.push_back(rpath);
        }
    }

    pclose(pipe);
}
#endif
