/**
* @file omlziputils.h
* @date March 2018
* Copyright (C) 2018-2020 Altair Engineering, Inc. All rights reserved. 
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret 
* Information. Not for use or disclosure outside of Licensee's organization. 
* The software and information contained herein may only be used internally and 
* is provided on a non-exclusive, non-transferable basis.  License may not 
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly 
* perform the software or other information provided herein, nor is Licensee 
* permitted to decompile, reverse engineer, or disassemble the software. Usage 
* of the software and other information provided by Altair (or its resellers) is 
* only as explicitly stated in the applicable end user license agreement between 
* Altair and Licensee. In the absence of such agreement, the Altair standard end 
* user license agreement terms shall govern.
*/
#ifndef __OMLZIPUTILS_H__
#define __OMLZIPUTILS_H__

#include <string>
#include <vector>

#include "Currency.h"

//------------------------------------------------------------------------------
//! \class OmlZipUtils
//! \brief Wrapper class for zip/unzip functions in minizip/zlib
//------------------------------------------------------------------------------
class OmlZipUtils
{
public:
    //!
    //! Constructor
    //!
    OmlZipUtils() {}
    //!
    //! Destructor
    //!
    ~OmlZipUtils() {}

    //!
    //! Returns true if successful in extracting the contents of given archive
    //! \param zipfile   Archive name
    //! \param dir       Directory where extracted files/directories are located
    //! \param getfiles  True if extracted file list needs to be retrieved
    //! \param files     List of files in archive
    //! \param warn      Warnings when extracting file(s)
    //!
    Currency Unzip(const std::string& zipfile,
                   const std::string& dir,
                   bool               getfiles,
                   std::string&       warn);
    //!
    //! Archives given files and returns a cell array with files list
    //! \param zipfile     Archive name
    //! \param dir         Directory where extracted files/directories are located
    //! \param overwrite   True if files/directories on disk will be overwritten
    //! \param getfilelist True if file list needs to be returned
    //! \param files       List of files in archive
    //! \param msgs        List of warnings/errors when extracting files
    //!
    Currency Zip(const std::string& zipfile,
                 const std::string& dir,
                 bool               overwrite,
                 bool               getfilelist,
                 const Currency&    cur,
                 std::string&       warn);

    // Utilites for Unicode support

    //!
    //! Returns true if given path is a directory, supports Unicode
    //! \param path Given path
    //!
    bool IsDir(const std::string& path);
    //!
    //! Deletes given file, supports Unicode
    //! \param file Given file
    //!
    bool Remove(const std::string& file);

private:
    //!
    //! Returns true if the given path has trailing slash
    //! \param path Given path
    //!
    bool HasTrailingSlash(const std::string& path);
    //!
    //! Opens file and returns file pointer
    //! \param name File name
    //! \param mode File mode for fopen
    //! \param err  Error string
    //!
    FILE* OpenFile(const std::string& name,
                   const std::string& mode,
                   std::string&       err);


#ifdef OS_WIN
    //! 
    //! Returns the base name for the given path, supports Unicode
    //! \param path Given path
    //!
    std::wstring GetBaseNameW(const std::wstring& path);
    //! 
    //! Gets relative path, supports Unicode
    //! \param path    Given path
    //! \param basedir Base directory
    //!
    std::wstring GetRelativePathW(const std::wstring& path,
                                  const std::wstring& basedir);
    //!
    //! Returns true if given path has a trailing slash
    //! \param path
    //!
    bool HasTrailingSlashW(const std::wstring& path);
    //!
    //! Opens file and returns file pointer, supports Unicode
    //! \param name File name
    //! \param mode File mode for fopen
    //! \param err  Error string
    //!
    FILE* OpenFileW(const std::wstring& name,
                    const std::wstring& mode,
                    std::string&       err);
    //!
    //! Gets a list of all files (recursively) in the given directory
    //! \param dir        Given path
    //! \param parentdir  Parent directory
    //! \param ignorefile Ignore this file
    //! \param cwd        Currenct working dir
    //! \param isabspath  True if given path is an absolute path
    //! \param abspath    Absolute path of files
    //! \param relpath    Relative path of files
    //! \param warn       Warning message
    //!
    void GetFiles(const std::wstring&        dir,
                  const std::wstring&        parentdir,
                  const std::wstring&        ignorefile,
                  const std::wstring&        cwd,
                  bool                       isabspath,
                  std::vector<std::wstring>& abspath,
                  std::vector<std::wstring>& relpath,
                  std::wstring&              warn);
    //!
    //! Lists contents of a directory recursively, supports Unicode
    //! \param parent     Parent directory
    //! \param base       Base directory
    //! \param ignorefile File to ignore
    //! \param abspath    List of files with absolute path
    //! \param relpath    List of files with path relative to parent directory
    void ListDir(const std::wstring&        parent,
                 const std::wstring&        base,
                 const std::wstring&        ignorefile,
                 std::vector<std::wstring>& abspath,
                 std::vector<std::wstring>& relpath);
    //!
    //! Archives given files and returns a cell array with files list
    //! \param zipfile     Archive name
    //! \param dir         Directory where extracted files/directories are located
    //! \param mode        Zip file mode
    //! \param getfilelist True if file list needs to be returned
    //! \param files       List of files in archive
    //! \param msgs        List of warnings/errors when extracting files
    //!
    Currency ZipW(const std::string&  zipfile,
                 const  std::wstring& dir,
                 int                  mode,
                 bool                 getfilelist,
                 const Currency&      cur,
                 std::string&         warn);

#else
    //!
    //! Gets a list of all files (recursively) in the given directory
    //! \param dir        Given path
    //! \param parentdir  Parent directory
    //! \param ignorefile Ignore this file
    //! \param cwd        Currenct working dir
    //! \param isabspath  True if given path is an absolute path
    //! \param abspath    Absolute path of files
    //! \param relpath    Relative path of files
    //!
    void GetFiles(const std::string&        dir,
                  const std::string&        parentdir,
                  const std::string&        ignorefile,
                  const std::string&        cwd,
                  bool                      isabspath,
                  std::vector<std::string>& abspath,
                  std::vector<std::string>& relpath);
#endif
};
#endif
