/**
* @file omlziptboxfuncs.h
* @date February 2018
* Copyright (c) 2018 Altair Engineering Inc Inc. All Rights Reserved. Contains trade 
* secrets of Altair Engineering Inc. Copyright notice does not imply publication. 
* Decompilation or disassembly of this software is strictly prohibited.
*/
#ifndef __OMLZIPTBOXFUNCS_H__
#define __OMLZIPTBOXFUNCS_H__

#include "omlziptboxdefs.h"

#include "EvaluatorInt.h"

//!
//! Entry point which registers toolbox with oml
//! \param eval Evaluator interface
//!
extern "C" OMLZIPTBOX_DECLS int InitDll(EvaluatorInterface eval);
//!
//! Returns toolbox version
//! \param eval Evaluator interface
//!
extern "C" OMLZIPTBOX_DECLS double GetToolboxVersion(EvaluatorInterface eval);


//------------------------------------------------------------------------------
//! \class OmlZipTBoxFuncs
//! \brief Utility class for zip/unzip functions in oml
//------------------------------------------------------------------------------
class OMLZIPTBOX_DECLS OmlZipTBoxFuncs
{
public:
    //!
    //! Destructor
    //!
    ~OmlZipTBoxFuncs() {}

    //!
    //! Returns true after zipping the given file(s) [zip command]
    //! \param eval    Evaluator interface
    //! \param inputs  Vector of inputs
    //! \param outputs Vector of outputs
    //!
    static bool Zip(EvaluatorInterface           eval,
                       const std::vector<Currency>& inputs,
                       std::vector<Currency>&       outputs);
    //!
    //! Returns true after unzipping the given file(s) [zip command]
    //! \param eval    Evaluator interface
    //! \param inputs  Vector of inputs
    //! \param outputs Vector of outputs
    //!
    static bool Unzip(EvaluatorInterface           eval,
                      const std::vector<Currency>& inputs,
                      std::vector<Currency>&       outputs);

private:
    //!
    //! Constructor
    //!
    OmlZipTBoxFuncs() {}
    //!
    //! Stubbed out copy constructor
    //!
	OmlZipTBoxFuncs(const OmlZipTBoxFuncs&);
    //!
    //! Stubbed out assignment operator
    //!
    OmlZipTBoxFuncs& operator=(const OmlZipTBoxFuncs&);
    //!
    //! Gets a list of all files (recursively) in the given directory
    //! \param dir       Given directory
    //! \param parentdir Parent directory
    //! \param vecfiles  List of files in directory/sub-directories
    //!
    void GetFilesInDirectory(const std::string&        dir,
                             const std::string&        parentdir,
                             std::vector<std::string>& vecfiles);

};
#endif
