/**
* @file omlziptboxfuncs.cxx
* @date February 2018
* Copyright (C) 2018 - 2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include "omlziptboxfuncs.h"

#include <cassert>

#include "omlziputils.h"

#include "BuiltInFuncsUtils.h"
#include "OML_Error.h"
#include "hwMatrix_NMKL.h"

#define TBOXVERSION 2022.1

//------------------------------------------------------------------------------
// Entry point which registers toolbox with oml
//------------------------------------------------------------------------------
int InitDll(EvaluatorInterface eval)
{
    eval.RegisterBuiltInFunction("zip", &OmlZipTBoxFuncs::Zip,
        FunctionMetaData(1, -3, "FileIO"));
    eval.RegisterBuiltInFunction("unzip", &OmlZipTBoxFuncs::Unzip,
        FunctionMetaData(1, -2, "FileIO"));
    return 1;
}
//------------------------------------------------------------------------------
// Returns true after zipping the given file(s)
//------------------------------------------------------------------------------
bool OmlZipTBoxFuncs::Zip(EvaluatorInterface           eval,
                          const std::vector<Currency>& inputs,
                          std::vector<Currency>&       outputs)
{
    int nargin = (inputs.empty()) ? 0 : static_cast<int>(inputs.size());
    if (nargin < 2) 
    {
        throw OML_Error(OML_ERR_NUMARGIN);
    }

    if (!inputs[0].IsString())
    {
        throw OML_Error(OML_ERR_STRING, 1, OML_VAR_TYPE);
    }
    std::string zipfile (inputs[0].StringVal());
    if (zipfile.empty())
    {
        throw OML_Error(OML_ERR_NONEMPTY_STR, 1, OML_VAR_VALUE);
    }
    std::string ext = BuiltInFuncsUtils::GetFileExtension(zipfile);
    if (ext.empty())
    {
        zipfile += ".zip";
    }
    zipfile = BuiltInFuncsUtils::Normpath(zipfile);

	Currency cur2 = inputs[1];   // Specifies the list of files/directories
    if (cur2.IsString())
    {
        HML_CELLARRAY* cell = EvaluatorInterface::allocateCellArray(1, 1);
        (*cell)(0, 0) = cur2.StringVal();
        cur2 = Currency(cell);   // Work with cells
    }
    else if (!cur2.IsCellArray())
    {
        throw OML_Error(OML_ERR_STRING_STRINGCELL, 2, OML_VAR_TYPE);
    }
        
    OmlZipUtils zutils;

    std::string  basedir;       // Specifies a root directory
    if (nargin >= 3)
    {
        if (!inputs[2].IsString())
        {
            throw OML_Error(OML_ERR_STRING, 3, OML_VAR_TYPE);
        }
        basedir = inputs[2].StringVal();
        BuiltInFuncsUtils::Normpath(basedir);
        if (!zutils.IsDir(basedir))
        {
            throw OML_Error(std::string(HW_ERROR_INVDIR) + " [" + 
                basedir + "] in argument 3");
        }
        basedir = BuiltInFuncsUtils::GetAbsolutePath(basedir);
        BuiltInFuncsUtils::AddTrailingSlash(basedir);
    }

    bool overwrite = true;
    if (nargin >= 4)
    {
        if (!inputs[3].IsString())
        {
            throw OML_Error(OML_ERR_STRING, 4, OML_VAR_TYPE);
        }
        std::string opt = inputs[3].StringVal();
        if (!opt.empty())
        {
            std::transform(opt.begin(), opt.end(), opt.begin(), ::tolower);
        }
        if (opt == "append")
        {
            overwrite = false;
        }
        else if (opt != "overwrite")
        {
            std::string msg = "Error: invalid option specified in argument 4; ";
            msg += "must be 'overwrite' or 'append'";
            throw OML_Error(msg);
        }
    }
    BuiltInFuncsUtils utils;
    if (overwrite && utils.DoesPathExist(zipfile) && !zutils.Remove(zipfile))
    {
        throw OML_Error("Error: failed to delete existing archive [ " +
                        zipfile + "]");
    }

    std::string warn;
    bool        getfilelist = (eval.GetNargoutValue() > 0);

    Currency filelist = zutils.Zip(zipfile, basedir, overwrite, getfilelist,
                        cur2, warn);
    if (getfilelist)
    {
        outputs.push_back(filelist);
    }
    if (!warn.empty())
    {
        utils.SetWarning(eval, "Warning(s):" + warn);
    }
    return true;
}
//------------------------------------------------------------------------------
// Returns true after unzipping the given file(s)
//------------------------------------------------------------------------------
bool OmlZipTBoxFuncs::Unzip(EvaluatorInterface           eval,
                            const std::vector<Currency>& inputs,
                            std::vector<Currency>&       outputs)
{
    int nargin = (inputs.empty()) ? 0 : static_cast<int>(inputs.size());
    if (nargin < 1) 
    {
        throw OML_Error(OML_ERR_NUMARGIN);
    }

    Currency cur1 = inputs[0];  // Specifies the zip file name
    if (!cur1.IsString())
    {
        throw OML_Error(OML_ERR_STRING, 1, OML_VAR_TYPE);
    }
    std::string zipfile (cur1.StringVal());
    if (zipfile.empty())
    {
        throw OML_Error(OML_ERR_NONEMPTY_STR, 1, OML_VAR_VALUE);
    }
    std::string ext = BuiltInFuncsUtils::GetFileExtension(zipfile);
    if (ext.empty())
    {
        zipfile += ".zip";
    }

    OmlZipUtils omlzip;
    BuiltInFuncsUtils utils;

    std::string zipdir;       // Specifies a root directory
    if (nargin > 1)
    {
        Currency cur2 = inputs[1];
        if (!cur2.IsString())
        {
            throw OML_Error(OML_ERR_STRING, 2, OML_VAR_TYPE);
        }
        zipdir = utils.Normpath(cur2.StringVal());
        utils.Mkdir(zipdir);
        if (!utils.DoesPathExist(zipdir))
        {
            throw OML_Error("Error: cannot extract to directory [" + zipdir +
                + "] in argument 2");
        }
    }

    bool getfiles = (eval.GetNargoutValue() > 0);

    std::string warn;
    Currency files = omlzip.Unzip(zipfile, zipdir, getfiles, warn);

    // Get the list of files in the archive
    if (getfiles)
    {
        outputs.push_back(files);
    }

    if (!warn.empty())
    {
        BuiltInFuncsUtils::SetWarning(eval, "Warning(s):" + warn);
    }

    return true;
}
//------------------------------------------------------------------------------
// Returns toolbox version
//------------------------------------------------------------------------------
double GetToolboxVersion(EvaluatorInterface eval)
{
    return TBOXVERSION;
}
