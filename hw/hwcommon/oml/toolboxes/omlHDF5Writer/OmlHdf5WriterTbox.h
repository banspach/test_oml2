/**
* @file OmlHdf5WriterTbox.h
* @date September 2020
* Copyright (C) 2020 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/
#ifndef __OMLHDF5WRITERTBOX_H__
#define __OMLHDF5WRITERTBOX_H__

#pragma warning(disable: 4251)

#include "OmlHdf5WriterDefs.h"
#include "EvaluatorInt.h"

//------------------------------------------------------------------------------
//!
//! \brief oml hdf5 Writer functions
//!
//------------------------------------------------------------------------------

extern "C"
{
    //!
    //! Entry point which registers hdf5 writer functions with oml
    //! \param eval Evaluator interface
    //!
    OMLHDF5WRITER_DECLS int InitDll(EvaluatorInterface eval);
    //!
    //! Returns toolbox version
    //! \param eval Evaluator interface
    //!
    OMLHDF5WRITER_DECLS double GetToolboxVersion(EvaluatorInterface eval);
}

//!
//! Creates hdf5 group [createhdf5group command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlCreateHdf5Group(EvaluatorInterface           eval,
                        const std::vector<Currency>& inputs,
                        std::vector<Currency>&       outputs);

//!
//! Renames hdf5 group [renamehdf5group command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlRenameHdf5Group(EvaluatorInterface           eval,
                        const std::vector<Currency>& inputs,
                        std::vector<Currency>&       outputs);

//!
//! Removes hdf5 group [removehdf5group command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlRemoveHdf5Group(EvaluatorInterface           eval,
                        const std::vector<Currency>& inputs,
                        std::vector<Currency>&       outputs);

//!
//! Create hdf5 dataset [createhdf5dataset command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlCreateHdf5Dataset(EvaluatorInterface           eval,
                          const std::vector<Currency>& inputs,
                          std::vector<Currency>&       outputs);

//!
//! Writes hdf5 dataset [writehdf5dataset command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlWriteHdf5Dataset(EvaluatorInterface           eval,
                         const std::vector<Currency>& inputs,
                         std::vector<Currency>&       outputs);
//!
//! Renames hdf5 dataset [renamehdf5dataset command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlRenameHdf5Dataset(EvaluatorInterface           eval,
                         const std::vector<Currency>& inputs,
                         std::vector<Currency>&       outputs);

//!
//! Removes hdf5 dataset [removehdf5dataset command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlRemoveHdf5Dataset(EvaluatorInterface           eval,
                          const std::vector<Currency>& inputs,
                          std::vector<Currency>&       outputs);


//!
//! Create hdf5 attribute [createhdf5attribute command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlCreateHdf5Attribute(EvaluatorInterface          eval,
                           const std::vector<Currency>& inputs,
                           std::vector<Currency>&       outputs);

//!
//! Writes hdf5 attribute [writehdf5attribute command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlWriteHdf5Attribute(EvaluatorInterface          eval,
                          const std::vector<Currency>& inputs,
                          std::vector<Currency>&       outputs);

//!
//! Renames hdf5 attribute [renamehdf5attribute command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlRenameHdf5Attribute(EvaluatorInterface           eval,
                            const std::vector<Currency>& inputs,
                            std::vector<Currency>&       outputs);

//!
//! Removes hdf5 attribute [removehdf5attribute command]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlRemoveHdf5Attribute(EvaluatorInterface           eval,
                            const std::vector<Currency>& inputs,
                            std::vector<Currency>&       outputs);

#endif // __OMLHDF5WRITERTBOX_H__
