/**
* @file OmlHdf5Writer.h
* @date September 2020
* Copyright (C) 2020 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#ifndef OML_HDF5_WRITER_H__
#define OML_HDF5_WRITER_H__

#pragma warning(disable: 4251)

#include "H5Cpp.h"
#include "OmlHdf5WriterDefs.h"
#include "EvaluatorInt.h"

typedef std::unique_ptr<char[]>           DATAPTR;
typedef std::unique_ptr<char*[]>          VARSTRDATAPTR;
typedef std::unique_ptr<H5::DataSet>      DATASETPTR;
typedef std::unique_ptr<H5::Attribute>    ATTRIBUTEPTR;
typedef std::unique_ptr<H5::Group>        GROUPPTR;
typedef std::unique_ptr<H5::DataType>     DATATYPEPTR;
typedef std::unique_ptr<double[]>         DOUBLEDATAPTR;
typedef std::unique_ptr<double>           DOUBLEPTR;
typedef std::unique_ptr<hsize_t>          DIMSPTR;
typedef std::unique_ptr<hobj_ref_t[]>     REFDATAPTR;

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//! Class for providing data writing to hdf5 file
//-------------------------------------------------------------------------------------
class OMLHDF5WRITER_DECLS OmlHdf5Writer
{

public:
    static OmlHdf5Writer* GetInstance();
    static void ReleaseInstance();

    void CreateGroup(const std::string& file_name,
                         const std::string& group_location);

    void RenameGroup(const std::string& file_name,
                         const std::string& src_grp_location,
                         const std::string& dest_grp_location);

    void RemoveGroup(const std::string& file_name,
                         const std::string& grp_location);


    void WriteDataset(const std::string& file_name,
                      const std::string& dset_location,
                      const Currency&    data,
                      const bool&        create,
                      const hwMatrix*    chunk = EvaluatorInterface::allocateMatrix());

    void RenameDataset(const std::string& file_name,
                       const std::string& src_dset_location,
                       const std::string& dest_dset_location);

    void RemoveDataset(const std::string& file_name,
                       const std::string& dset_location);

    void WriteAttribute(const std::string& file_name,
                        const std::string& location,
                        const std::string& attr_name,
                        const Currency&    data,
                        const bool&        create = false);

    void RenameAttribute(const std::string& file_name,
                         const std::string& src_attr_name,
                         const std::string& dest_attr_name,
                         const std::string& location = "");

    void RemoveAttribute(const std::string& file_name,
                         const std::string& attr_name,
                         const std::string& location = "");

    class FileManager {
    public:
        FileManager(OmlHdf5Writer* writer, const std::string& file_name, bool create=false) :_writer(writer)
        {
            _writer->OpenFile(file_name, create);
        }

        ~FileManager()
        {
            _writer->CloseAll();
        }
    private:
        OmlHdf5Writer* _writer;
    };
private:
    //! Constructor
    OmlHdf5Writer();
    //! Copy Constructor
    OmlHdf5Writer(const OmlHdf5Writer&)            = delete;
    //! Assignment Operator
    OmlHdf5Writer& operator=(const OmlHdf5Writer&) = delete;
    //! Destructor
    ~OmlHdf5Writer();

    void OpenFile(const std::string& file_name, bool create);
    void CloseAll();

    void ValidateGroup(const std::string& grp_location);
    void ValidateDatasetLocation(const std::string& dset_location);
    void CreateDataset(const std::string& dset_location, const Currency& data, const hwMatrix* chunk);
    
    void WriteData(const Currency& data, const H5::DataType* file_dtype, std::vector<std::string>& compound_names,
                    const bool parent_is_compound = false, std::string child_name = std::string(),
                    const H5::CompType* parent_cmp_type = nullptr);
    
    void WriteDataToFile(const void* data_buf, const H5::DataType& mem_dtype);

    void GetDataTypeInfo(const Currency& data, std::vector<int>& dims, H5::DataType& dt, size_t& size, bool is_root = false);

    void CreateAttribute(const std::string& attr_name, const std::string& location, const Currency& data);
    void ValidateAttributeLocation(const std::string& attr_name, const std::string& location, bool create = false);

    void CalculateChunkSize(const int& rank, const size_t& datatype_size, hsize_t* chunk_dims);

    H5::DataType GetCompoundDataType(const H5::DataType premitive_type, const std::vector<std::string>& compound_names);

    void AddMetadataToComplexDataset();

    static OmlHdf5Writer* _instance;
    H5::H5File*            _file;
    H5::DataSet*           _dset;
    H5::Attribute*         _attr;
};

#endif //OML_HDF5_WRITER_H__