/**
* @file OmlHdf5WriterTbox.cxx
* @date September 2020
* Copyright (C) 2020-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/
#include "OmlHdf5WriterTbox.h"

#include <cassert>

#include "OmlHdf5Writer.h"
#include "OML_Error.h"
#include "Currency.h"
#include "BuiltInFuncsCore.h"
#include "BuiltInFuncsUtils.h"

#define TBOXVERSION 2022.2

// Licensing
typedef bool(*LICFUNCPTR)();
bool g_licInitialized = false;                   // True if licensing is done
bool g_businessLibsAuthorized = false;           // True if business library is authorized
void OmlHdf5WriterCheckFunc(const std::string&); // Throws error if not business edition

//------------------------------------------------------------------------------
// Dll entry point
//------------------------------------------------------------------------------
int InitDll(EvaluatorInterface eval)
{
    // Note: When adding new functions, the first line should call
    // OmlHdf5WriterCheckFunc as this is a professional lib
    eval.RegisterBuiltInFunction("createhdf5group", OmlCreateHdf5Group, FunctionMetaData(2, 0, "HDF5Writer"));
    eval.RegisterBuiltInFunction("renamehdf5group", OmlRenameHdf5Group, FunctionMetaData(3, 0, "HDF5Writer"));
    eval.RegisterBuiltInFunction("removehdf5group", OmlRemoveHdf5Group, FunctionMetaData(2, 0, "HDF5Writer"));
    
    eval.RegisterBuiltInFunction("createhdf5dataset", OmlCreateHdf5Dataset, FunctionMetaData(-4, 0, "HDF5Writer"));
    eval.RegisterBuiltInFunction("writehdf5dataset",  OmlWriteHdf5Dataset, FunctionMetaData(3, 0, "HDF5Writer"));
    eval.RegisterBuiltInFunction("renamehdf5dataset", OmlRenameHdf5Dataset, FunctionMetaData(3, 0, "HDF5Writer"));
    eval.RegisterBuiltInFunction("removehdf5dataset", OmlRemoveHdf5Dataset, FunctionMetaData(2, 0, "HDF5Writer"));

    eval.RegisterBuiltInFunction("createhdf5attribute", OmlCreateHdf5Attribute, FunctionMetaData(-4, 0, "HDF5Writer"));
    eval.RegisterBuiltInFunction("writehdf5attribute",  OmlWriteHdf5Attribute, FunctionMetaData(-4, 0, "HDF5Writer"));
    eval.RegisterBuiltInFunction("renamehdf5attribute", OmlRenameHdf5Attribute, FunctionMetaData(-4, 0, "HDF5Writer"));
    eval.RegisterBuiltInFunction("removehdf5attribute", OmlRemoveHdf5Attribute, FunctionMetaData(-3, 0, "HDF5Writer"));

    return 0;
}
//------------------------------------------------------------------------------
// Gets toolbox version
//------------------------------------------------------------------------------
double GetToolboxVersion(EvaluatorInterface eval)
{
    return TBOXVERSION;
}
//------------------------------------------------------------------------------
// Implementation of oml function [createhdf5group]
//------------------------------------------------------------------------------
bool OmlCreateHdf5Group(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("createhdf5group");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 2)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[1].IsString())
        throw OML_Error(OML_ERR_STRING, 2);

    const std::string& file_name     = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& group_location = inputs[1].StringVal();

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();
    ohw->CreateGroup(file_name, group_location);
    return  true;
}
//------------------------------------------------------------------------------
// Implementation of oml function [renamehdf5group]
//------------------------------------------------------------------------------
bool OmlRenameHdf5Group(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("renamehdf5group");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 3)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[1].IsString())
        throw OML_Error(OML_ERR_STRING, 2);

    if (!inputs[2].IsString())
        throw OML_Error(OML_ERR_STRING, 3);

    const std::string& file_name         = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& src_grp_location  = inputs[1].StringVal();
    const std::string& dest_grp_location = inputs[2].StringVal();

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();
    ohw->RenameGroup(file_name, src_grp_location, dest_grp_location);

    return  true;

}
//------------------------------------------------------------------------------
// Implementation of oml function [removehdf5group]
//------------------------------------------------------------------------------
bool OmlRemoveHdf5Group(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("removehdf5group");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 2)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[1].IsString())
        throw OML_Error(OML_ERR_STRING, 2);

    const std::string& file_name    = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& grp_location = inputs[1].StringVal();

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();
    ohw->RemoveGroup(file_name, grp_location);

    return  true;
}
//------------------------------------------------------------------------------
// Implementation of oml function [createhdf5dataset]
//------------------------------------------------------------------------------
bool OmlCreateHdf5Dataset(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("createhdf5dataset");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 3 || nargin > 4)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[1].IsString())
        throw OML_Error(OML_ERR_STRING, 2);

    const std::string& file_name     = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& dset_location = inputs[1].StringVal();
    
    Currency chu(EvaluatorInterface::allocateMatrix());
    const hwMatrix* chunk = chu.Matrix();

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();

    if (4 == nargin)
    { 
        if (!inputs[3].IsPositiveIntegralMatrix())
            throw OML_Error(OML_ERR_POSINTMATRIX, 4);

        chunk = inputs[3].Matrix();
    }

    ohw->WriteDataset(file_name, dset_location, inputs[2], true, chunk);

    return true;
}
//------------------------------------------------------------------------------
// Implementation of oml function [writehdf5dataset]
//------------------------------------------------------------------------------
bool OmlWriteHdf5Dataset(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("writehdf5dataset");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin != 3)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[1].IsString())
        throw OML_Error(OML_ERR_STRING, 2);

    const std::string& file_name     = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& dset_location = inputs[1].StringVal();

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();
    ohw->WriteDataset(file_name, dset_location, inputs[2], false);

    return true;
}
//------------------------------------------------------------------------------
// Implementation of oml function [renamehdf5dataset]
//------------------------------------------------------------------------------
bool OmlRenameHdf5Dataset(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("renamehdf5dataset");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 3)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[1].IsString())
        throw OML_Error(OML_ERR_STRING, 2);

    if (!inputs[2].IsString())
        throw OML_Error(OML_ERR_STRING, 3);

    const std::string& file_name          = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& src_dset_location  = inputs[1].StringVal();
    const std::string& dest_dset_location = inputs[2].StringVal();

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();
    ohw->RenameDataset(file_name, src_dset_location, dest_dset_location);

    return  true;

}
//------------------------------------------------------------------------------
// Implementation of oml function [removehdf5dataset]
//------------------------------------------------------------------------------
bool OmlRemoveHdf5Dataset(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("removehdf5dataset");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 2)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[1].IsString())
        throw OML_Error(OML_ERR_STRING, 2);

    const std::string& file_name     = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& dset_location = inputs[1].StringVal();

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();
    ohw->RemoveDataset(file_name, dset_location);

    return  true;
}
//------------------------------------------------------------------------------
// Implementation of oml function [createhdf5attribute]
//------------------------------------------------------------------------------
bool OmlCreateHdf5Attribute(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("createhdf5attribute");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 3 || nargin > 4)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[nargin - 2].IsString())
        throw OML_Error(OML_ERR_STRING, nargin - 1);



    std::string attr_location    = "";
    const std::string& file_name = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& attr_name = inputs[nargin - 2].StringVal();

    if (4 == nargin)
    {
        if (!inputs[nargin - 3].IsString())
            throw OML_Error(OML_ERR_STRING, nargin - 2);

        attr_location = inputs[nargin - 3].StringVal();
    }

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();
    ohw->WriteAttribute(file_name, attr_location, attr_name, inputs[nargin-1], true);

    return true;
}
//------------------------------------------------------------------------------
// Implementation of oml function [writehdf5attribute]
//------------------------------------------------------------------------------
bool OmlWriteHdf5Attribute(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("writehdf5attribute");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 3 || nargin > 4)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[nargin - 2].IsString())
        throw OML_Error(OML_ERR_STRING, nargin - 1);


    std::string location = "";
    const std::string& file_name = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    std::string attr_location = location;
    const std::string& attr_name = inputs[nargin - 2].StringVal();

    if (4 == nargin)
    {
        if (!inputs[nargin - 3].IsString())
            throw OML_Error(OML_ERR_STRING, nargin - 2);

        attr_location = inputs[nargin - 3].StringVal();
    }

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();
    ohw->WriteAttribute(file_name, attr_location, attr_name, inputs[nargin - 1]);

    return true;
}
//------------------------------------------------------------------------------
// Implementation of oml function [renamehdf5attribute]
//------------------------------------------------------------------------------
bool OmlRenameHdf5Attribute(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("renamehdf5attribute");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 3 || nargin > 4)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[1].IsString())
        throw OML_Error(OML_ERR_STRING, 2);

    if (!inputs[2].IsString())
        throw OML_Error(OML_ERR_STRING, 3);

    const std::string& file_name      = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& src_attr_name  = inputs[nargin-2].StringVal();
    const std::string& dest_attr_name = inputs[nargin-1].StringVal();
    std::string location        = std::string("");

    if (4 == nargin)
    {
        if (!inputs[3].IsString())
            throw OML_Error(OML_ERR_STRING, 4);

        location = inputs[1].StringVal();
    }

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();
    ohw->RenameAttribute(file_name, src_attr_name, dest_attr_name, location);

    return  true;
}
//------------------------------------------------------------------------------
// Implementation of oml function [removehdf5attribute]
//------------------------------------------------------------------------------
bool OmlRemoveHdf5Attribute(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs)
{
    OmlHdf5WriterCheckFunc("removehdf5attribute");  // Check license

    int nargin = eval.GetNarginValue();
    if (nargin < 2 || nargin > 3)
        throw OML_Error(OML_ERR_NUMARGIN);

    if (!inputs[0].IsString())
        throw OML_Error(OML_ERR_STRING, 1);

    if (!inputs[1].IsString())
        throw OML_Error(OML_ERR_STRING, 2);

    const std::string& file_name = BuiltInFuncsUtils::Normpath(inputs[0].StringVal());
    const std::string& attr_name = inputs[nargin-1].StringVal();
    std::string  location  = std::string("");

    if (3 == nargin)
    {
        if (!inputs[2].IsString())
            throw OML_Error(OML_ERR_STRING, 3);

        location = inputs[1].StringVal();        
    }

    OmlHdf5Writer* ohw = OmlHdf5Writer::GetInstance();
    ohw->RemoveAttribute(file_name, attr_name, location);

    return  true;
}
//------------------------------------------------------------------------------
// Throws error for functions if not running in business edition mode
//------------------------------------------------------------------------------
void OmlHdf5WriterCheckFunc(const std::string& func)
{
    if (g_businessLibsAuthorized)
    {
        return;
    }

    if (!g_licInitialized)
    {
        // Attempt to check out a Compose license without the user credentials
        std::string dllname("hwcomposelic");
#ifndef OS_WIN
        dllname = "lib" + dllname + ".so";
#endif
        void* handle = BuiltInFuncsCore::DyLoadLibrary(dllname);
        if (!handle)
        {
            throw OML_Error("Cannot load [" + dllname + "]");
        }
        void* symbol = BuiltInFuncsCore::DyGetFunction(handle,
            "AuthorizeBusinessEditionLibrary");
        if (!symbol)
        {
            throw OML_Error("Unable to authorize [" + func + "]");
        }
        g_licInitialized = true;
        LICFUNCPTR funcptr = (LICFUNCPTR)(symbol);
        if (!funcptr)
        {
            throw OML_Error("Invalid authorization function");
        }
        g_businessLibsAuthorized = funcptr();
    }

    if (!g_businessLibsAuthorized)
    {
        throw OML_Error(
            "Invalid command [" + func + "]; available only in Compose Business Edition");
    }
}