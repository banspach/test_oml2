/**
* @file OmlHdf5Writer.cxx
* @date September 2020
* Copyright (C) 2020 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include "OmlHdf5Writer.h"
#include "OML_Error.h"
#include "Currency.h"
#include "BuiltInFuncsUtils.h"
#include "StructData.h"
#include "hwMatrix_NMKL.h"
#include "hwMatrixN_NMKL.h"

#define MAX_CHUNK_SIZE 1048576 //HDF5 default cache size of dataset is 1MB
#define MIN_CHUNK_VALUE 1024
#define REAL_FIELD_NAME "Real"
#define IMAGINARY_FIELD_NAME "Imag"
#define OML_COMPLEX_ATTRIBUTE "omlcomplex"

OmlHdf5Writer* OmlHdf5Writer::_instance = NULL;

OmlHdf5Writer* OmlHdf5Writer::GetInstance()
{
    if (!_instance)
    {
        _instance = new OmlHdf5Writer();
        H5::Exception::dontPrint();
    }
    return _instance;
}

void OmlHdf5Writer::ReleaseInstance()
{
    delete _instance;
    _instance = NULL;
}

//! Constructor
OmlHdf5Writer::OmlHdf5Writer() : _file(nullptr), _dset(nullptr), _attr(nullptr)
{
}

//! Destructor
OmlHdf5Writer::~OmlHdf5Writer()
{
}

void OmlHdf5Writer::OpenFile(const std::string& file_name, bool create)
{
    BuiltInFuncsUtils utils;

#ifdef OS_WIN
    if (utils.HasWideChars(file_name))
    {
        throw OML_Error(OML_ERR_UNICODE_FILENAME);
    }
#endif

    if (utils.DoesPathExist(file_name))
    {
        try
        {
            if (!H5::H5File::isHdf5(file_name))
                throw OML_Error(OML_ERR_HDF5_INVALID_FILE, 1);

            _file = new H5::H5File(file_name, H5F_ACC_RDWR);
        }
        catch (H5::FileIException&)
        {
            throw OML_Error(OML_ERR_HDF5_FILE_READ_FAILED);
        }
    }
    else
    {
        if (create)
        {
            try
            {
                _file = new H5::H5File(file_name, H5F_ACC_TRUNC);
                return;
            }
            catch (...)
            {
                throw OML_Error(OML_ERR_HDF5_FILE_CREATION_FAILED);
            }
        }
        else
        {
            throw OML_Error(OML_ERR_FILE_NOTFOUND, 1);
        }
    }


}


void OmlHdf5Writer::CloseAll()
{
    if (_file)
    {
        _file->close();
        _file = nullptr;
    }

    if (_dset)
    {
        _dset->close();
        _dset = nullptr;
    }

    if (_attr)
    {
        _attr->close();
        _attr = nullptr;
    }
}

void OmlHdf5Writer::CreateGroup(const std::string& file_name,
                                const std::string& group_location)
{
    FileManager file_manager(this, file_name, true);

    try
    {
        if (!(H5Lexists(_file->getId(), group_location.c_str(), H5P_DEFAULT) > 0))
        {
            _file->createGroup(group_location);
        }
        else if (H5O_TYPE_GROUP == _file->childObjType(group_location))
        {
            throw OML_Error(OML_ERR_HDF5_GROUP_EXIST);
        }
        else
        {
            throw OML_Error(OML_ERR_HDF5_INVALID_GROUP);
        }
    }
    catch (OML_Error& err)
    {
        throw err;
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_GROUP_CREATION_FAILED);
    }
}

void OmlHdf5Writer::RenameGroup(const std::string& file_name,
                                const std::string& src_grp_location,
                                const std::string& dest_grp_location)
{
    FileManager file_manager(this, file_name);
    ValidateGroup(src_grp_location);

    try
    {
        _file->move(src_grp_location, dest_grp_location);
    }
    catch (...) // move throws H5::FileIException and H5::GroupIException
    {
        throw OML_Error(OML_ERR_HDF5_GROUP_RENAME_FAILED);
    }
}

void OmlHdf5Writer::RemoveGroup(const std::string& file_name,
                                const std::string& grp_location)
{
    FileManager file_manager(this, file_name);

    ValidateGroup(grp_location);

    try
    {
        _file->unlink(grp_location);
    }
    catch (...) // unlink throws H5::FileIException and H5::GroupIException
    {
        throw OML_Error(OML_ERR_HDF5_GROUP_REMOVE_FAILED);
    }
    
}

void OmlHdf5Writer::ValidateGroup(const std::string& grp_location)
{
    if ((!(H5Lexists(_file->getId(), grp_location.c_str(), H5P_DEFAULT) > 0))
        || (H5O_TYPE_GROUP != _file->childObjType(grp_location)))
    {
        throw OML_Error(OML_ERR_HDF5_INVALID_GROUP);
    }
}

void OmlHdf5Writer::WriteDataset(const std::string& file_name,
                                 const std::string& dset_location,
                                 const Currency& data,
                                 const bool& create,
                                 const hwMatrix* chunk)
{
    FileManager file_manager(this, file_name, create);
    
    if (!create)
    {
        ValidateDatasetLocation(dset_location);
    }
    else
    {
        CreateDataset(dset_location, data, chunk);
    }

    try
    {
        _dset = new H5::DataSet(_file->openDataSet(dset_location));

        //Extend dataspace start
        if (!create)
        {
           H5::DSetCreatPropList prop_list = _dset->getCreatePlist();
            if (H5D_CHUNKED == prop_list.getLayout())
            {

                std::vector<hsize_t> dims;
                std::vector<int> original_dims;
                H5::DataType dt;
                size_t size = 0;
                GetDataTypeInfo(data, original_dims, dt, size, true);

                for (auto& member : original_dims) {
                    dims.push_back(member);
                }
                _dset->extend(dims.data());
            }
        }
        //Extend dataspace end
    }
    catch (H5::FileIException&)
    {
        throw OML_Error(OML_ERR_HDF5_FILE_READ_FAILED);
    }
    catch (H5::GroupIException&)
    {
        throw OML_Error(OML_ERR_HDF5_GROUP_READ_FAILED);
    }

    try
    {
        H5::DataType dt = _dset->getDataType();
        std::vector<std::string> comp_field_names;
        WriteData(data, &dt, comp_field_names);
    }
    catch (OML_Error & err)
    {
        throw err;
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);
    }
    
}

/*ToDo: Can club RenameGroup and RenameDataset functions into one, except the error messages*/
void OmlHdf5Writer::RenameDataset(const std::string& file_name,
                                  const std::string& src_dset_location,
                                  const std::string& dest_dset_location)
{
    FileManager file_manager(this, file_name);
    ValidateDatasetLocation(src_dset_location);

    try
    {
        _file->move(src_dset_location, dest_dset_location);
    }
    catch (...) // move throws H5::FileIException and H5::GroupIException
    {
        throw OML_Error(OML_ERR_HDF5_DATASET_RENAME_FAILED);
    }
}

void OmlHdf5Writer::RemoveDataset(const std::string& file_name,
                                  const std::string& dset_location)
{
    FileManager file_manager(this, file_name);

    ValidateDatasetLocation(dset_location);

    try
    {
        _file->unlink(dset_location);
    }
    catch (...) // unlink throws H5::FileIException and H5::DIException
    {
        throw OML_Error(OML_ERR_HDF5_DATASET_REMOVE_FAILED);
    }

}

void OmlHdf5Writer::ValidateDatasetLocation(const std::string& dset_location)
{
    if ((!(H5Lexists(_file->getId(), dset_location.c_str(), H5P_DEFAULT) > 0))
        || (H5O_TYPE_DATASET != _file->childObjType(dset_location)))
    {
        throw OML_Error(OML_ERR_HDF5_INVALID_DATASET);
    }
}

void OmlHdf5Writer::CreateDataset(const std::string& dset_location, const Currency& data, const hwMatrix* chunk)
{
    if ((H5Lexists(_file->getId(), dset_location.c_str(), H5P_DEFAULT) > 0))
    {
        throw OML_Error(OML_ERR_HDF5_DATASET_EXIST);
    }

    std::vector<int> original_dims;
    H5::DataType dt;
    size_t size = 0;
    GetDataTypeInfo(data, original_dims, dt, size, true);

    int rank = static_cast<int>(original_dims.size());

    DIMSPTR maxdims    = DIMSPTR(new hsize_t[rank]);
    DIMSPTR dims       = DIMSPTR(new hsize_t[rank]);
    DIMSPTR chunk_dims = DIMSPTR(new hsize_t[rank]);

    int index = 0;
    for (auto& member : original_dims) {
        (dims.get())[index] = member;
        (chunk_dims.get())[index] = member;
        (maxdims.get())[index] = H5S_UNLIMITED;
        index++;
    }

    if (chunk->IsEmpty())
    {
        CalculateChunkSize(rank, dt.getSize(), chunk_dims.get());
    }
    else
    {
        int rows = chunk->M();
        if (1 != rows)
            throw OML_Error(OML_ERR_HDF5_CHUNK_MATRIX_INVALID_ROW);

        int cols = chunk->N();

        if (rank != cols)
            throw OML_Error(OML_ERR_HDF5_CHUNK_MATRIXDIM_INVALID);

        for (int i = 0; i < cols; i++)
            (chunk_dims.get())[i] = static_cast<hsize_t>((*chunk)(0, i));

    }

    H5::DataSpace dataspace(rank, dims.get(), maxdims.get());
    H5::DSetCreatPropList prop;
    prop.setChunk(rank, chunk_dims.get());

    try
    {
        _file->createDataSet(dset_location.c_str(), dt, dataspace, prop);
    }
    catch (...) //  createDataSet throws H5::FileIException and H5::GroupIException
    {
        throw OML_Error(OML_ERR_HDF5_DATASET_CREATION_FAILED);
    }    
}

void OmlHdf5Writer::CalculateChunkSize(const int& rank, const size_t& datatype_size, hsize_t* chunk_dims)
{
    size_t chunk_size = datatype_size;

    for (int i = 0; i < rank; i++)
    {
        chunk_size *= chunk_dims[i];
    }

    do
    {
        if (chunk_size < MAX_CHUNK_SIZE)
            break;
        else if ((chunk_size/datatype_size) <= MIN_CHUNK_VALUE) 
            //to exit when data type size is major contributer in 1M and chunk dims are small, since reducing the cunk dims alone does not help
            break;
        else
            chunk_size = datatype_size;

        for (int i = 0; i < rank; i++)
        {
            chunk_dims[i] = ((chunk_dims[i] / 2) + (chunk_dims[i] % 2));
            chunk_size *= chunk_dims[i];
        }
    } while (true);

}

void OmlHdf5Writer::GetDataTypeInfo(const Currency& data, std::vector<int>& dims, H5::DataType& dt, size_t& size, bool is_root)
{
    if (data.IsComplex())
    {
        dims.push_back(1);
        if (data.IsLogical())
            dt = H5::DataType(H5::PredType::NATIVE_HBOOL);
        else
            dt = H5::DataType(H5::PredType::NATIVE_DOUBLE);

        H5::CompType mem_cmp_dtype(dt.getSize() * 2);
        size_t offset = dt.getSize();
        mem_cmp_dtype.insertMember(REAL_FIELD_NAME, 0, dt);
        mem_cmp_dtype.insertMember(IMAGINARY_FIELD_NAME, offset, dt);
        dt = mem_cmp_dtype;
        size = dt.getSize();
    }
    else if (data.IsScalar())
    {
        dims.push_back(1);

        if (data.IsLogical())
            dt = H5::DataType(H5::PredType::NATIVE_HBOOL);
        else
            dt = H5::DataType(H5::PredType::NATIVE_DOUBLE);

        size = dt.getSize();
    }
    else if (data.IsString())
    {
        dims.push_back(1);
        dt = H5::DataType(H5::StrType(H5::PredType::C_S1, H5T_VARIABLE));
        size = dt.getSize();
    }
    else if (data.IsMatrix() || data.IsNDMatrix())
    {
        bool is_matrix = data.IsMatrix();
        bool is_real   = false;
        const hwMatrix*  mat    = nullptr;
        const hwMatrixN* nd_mat = nullptr;

        if (is_matrix)
        {
            mat = data.Matrix();
            if (mat->IsEmpty())
                throw OML_Error(OML_ERR_HDF5_EMPTYMATRIX);

            dims.push_back(mat->M());
            dims.push_back(mat->N());
            is_real = mat->IsReal();
        }
        else
        {
            nd_mat = data.MatrixN();
            if (nd_mat->IsEmpty())
                throw OML_Error(OML_ERR_HDF5_EMPTYMATRIX);

            dims = nd_mat->Dimensions();
            is_real = nd_mat->IsReal();
        }

        H5::PredType type_class = H5::PredType::NATIVE_DOUBLE;
        if (data.IsLogical())
            type_class = H5::PredType::NATIVE_HBOOL;

        dt = H5::DataType(type_class);

        if (!is_real)
        {
            H5::CompType mem_cmp_dtype(dt.getSize() * 2);
            size_t offset = dt.getSize();
            mem_cmp_dtype.insertMember(REAL_FIELD_NAME, 0, dt);
            mem_cmp_dtype.insertMember(IMAGINARY_FIELD_NAME, offset, dt);
            dt = mem_cmp_dtype;
        }

        if (!is_root)
        {
            std::vector<hsize_t> arr_dims;
            for (int dim_index = 0; dim_index < dims.size(); dim_index++)
                arr_dims.push_back(dims[dim_index]);

            dt = H5::ArrayType(dt, static_cast<int>(dims.size()), arr_dims.data());
        }

        size = dt.getSize();
    }
    else if (data.IsCellArray())
    {
        HML_CELLARRAY* cells = data.CellArray();

        if (cells->IsEmpty())
            throw OML_Error(OML_ERR_HDF5_EMPTYCELL); 

        dims.push_back(cells->M());
        dims.push_back(cells->N());
        std::vector<int> child_dims;

        for (int i = 0; i < cells->Size(); i++)
        {
            std::vector<int> temp_dims;
            GetDataTypeInfo((*cells)(i), temp_dims, dt, size, false);

            if (child_dims.empty())
            {
                child_dims = temp_dims;
            }
            else
            {
                if (child_dims != temp_dims)
                {
                    throw OML_Error(OML_ERR_HDF5_INVALID_CELL_MEMBERS);
                }
            }
        }
        size = dt.getSize();
    }
    else if (data.IsNDCellArray())
    {
        HML_ND_CELLARRAY* nd_cells = data.CellArrayND();

        if (nd_cells->IsEmpty())
            throw OML_Error(OML_ERR_HDF5_EMPTYCELL); 

        dims = nd_cells->Dimensions();

        std::vector<int> child_dims;

        for (int i = 0; i < nd_cells->Size(); i++)
        {
            std::vector<int> temp_dims;
            GetDataTypeInfo((*nd_cells)(0), temp_dims, dt, size, false);

            if (child_dims.empty())
            {
                child_dims = temp_dims;
            }
            else
            {
                if (child_dims != temp_dims)
                {
                    throw OML_Error(OML_ERR_HDF5_INVALID_CELL_MEMBERS);
                }
            }
        }
        size = dt.getSize();

    }
    else if (data.IsStruct())
    {
        StructData* sd = data.Struct();

        if (sd->IsEmpty() || (1 != sd->M()) || (1 != sd->N()))
            throw OML_Error(OML_ERR_HDF5_STRUCT_INVALID_DIMS);

        std::map<std::string, int> field_names = sd->GetFieldNames();
        std::map<std::string, int>::iterator iter;
        
        std::vector<int> child_dims;        
        H5::DataType child_dt;        
        std::map<std::string, H5::DataType> child_info;

        for (iter = field_names.begin(); iter != field_names.end(); iter++)
        {
            std::vector<int> temp_dims;
            size_t  child_size = 0;
            GetDataTypeInfo(sd->GetValue(0, 0, iter->first), temp_dims, child_dt, child_size, true);
            
            if (child_dims.empty())
            {
                child_dims = temp_dims;
            }   
            else
            {
                if (child_dims != temp_dims)
                {
                    throw OML_Error(OML_ERR_HDF5_INVALID_STRUCT_MEMBERS);
                }
            }            
            child_info.insert(std::make_pair(iter->first, child_dt));

            size += child_size;
        }
        
        if (is_root)
            dims = child_dims;

        H5::CompType mem_cmp_dtype(size);

        std::map<std::string, H5::DataType>::iterator child_iter;
        size_t offset = 0;
        for (child_iter = child_info.begin(); child_iter != child_info.end(); child_iter++)
        {
            try
            {
                mem_cmp_dtype.insertMember(child_iter->first, offset, child_iter->second);
            }
            catch (H5::DataTypeIException&)
            {
                throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);
            }
            
            offset += (child_iter->second).getSize();
        }
        dt = mem_cmp_dtype;
    }
    else
    {
        throw OML_Error(OML_ERR_HDF5_UNSUPPORT_DATA);
    }
}

void OmlHdf5Writer::WriteDataToFile(const void* data_buf, const H5::DataType& mem_dtype)
{
    try
    {
        if (_dset)
            _dset->write(data_buf, mem_dtype);
        else if(_attr)
            _attr->write(mem_dtype, data_buf);
        else
            throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);
    }
    catch (OML_Error & err)
    {
        throw err;
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);
    }

}

void OmlHdf5Writer::WriteData(const Currency& data, 
                              const H5::DataType* file_dtype,
                              std::vector<std::string>& compound_names,
                              const bool parent_is_compound,
                              std::string child_name,
                              const H5::CompType* parent_cmp_type)
{
    void* ret = nullptr;

    if (data.IsComplex())
    {
        if (_attr)
        {
            throw OML_Error(OML_ERR_HDF5_INVALID_ATTRIBUTE_DATA);
        }

        const hwComplex& cplx = data.Complex();
        DOUBLEPTR data_buf_uptr = DOUBLEPTR(new double[2]);
        double data_buf_real = cplx.Real();
        double data_buf_imag = cplx.Imag();


        H5::DataType dt(H5::PredType::NATIVE_DOUBLE);
        
        H5::CompType cmp_mem_dtype_real(dt.getSize());
        cmp_mem_dtype_real.insertMember(REAL_FIELD_NAME, 0, dt);

        H5::CompType cmp_mem_dtype_imag(dt.getSize());
        cmp_mem_dtype_imag.insertMember(IMAGINARY_FIELD_NAME, 0, dt);

        if (parent_is_compound)
        {
            WriteDataToFile(&data_buf_real, GetCompoundDataType(cmp_mem_dtype_real, compound_names));
            WriteDataToFile(&data_buf_imag, GetCompoundDataType(cmp_mem_dtype_imag, compound_names));
        }
        else
        {
            WriteDataToFile(&data_buf_real, cmp_mem_dtype_real);
            WriteDataToFile(&data_buf_imag, cmp_mem_dtype_imag);
        }
        AddMetadataToComplexDataset();
    }
    else if (data.IsScalar())
    {
        double data_buf = data.Scalar();
        H5::DataType dt(H5::PredType::NATIVE_DOUBLE);

        bool is_refobj = false;
        switch (file_dtype->getClass())
        {
            case H5T_REFERENCE:
            {
                dt = H5::DataType(H5::PredType::STD_REF_OBJ);
                if (dt == (*file_dtype))
                    is_refobj = true;
                else
                    throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);

                break;
            }
            default:
                break;
        }

        if (is_refobj)
        {
            hobj_ref_t ref_data_buf = static_cast<hobj_ref_t>(data_buf);
            if (parent_is_compound)
            {
                WriteDataToFile(&ref_data_buf, GetCompoundDataType(dt, compound_names));
            }
            else
            {
                WriteDataToFile(&ref_data_buf, dt);
            }
        }
        else
        {
            if (parent_is_compound)
            {
                WriteDataToFile(&data_buf, GetCompoundDataType(dt, compound_names));
            }
            else
            {
                WriteDataToFile(&data_buf, dt);
            }
        }
    }
    else if (data.IsString())
    {
        std::string data_buf = data.StringVal();

        if (file_dtype->isVariableStr())
        {
            H5::StrType mem_dtype(H5::PredType::C_S1, H5T_VARIABLE);
            VARSTRDATAPTR data_buf_uptr = VARSTRDATAPTR(new char* [sizeof(char*)]);
            char** data_buf = data_buf_uptr.get();
#ifdef OS_WIN
            data_buf[0] = _strdup(data.StringVal().c_str());
#else
            data_buf[0] = strdup(data.StringVal().c_str());
#endif
          
            if (parent_is_compound)
            {
                WriteDataToFile(data_buf, GetCompoundDataType(mem_dtype, compound_names));
            }
            else
            {
                WriteDataToFile(data_buf, mem_dtype);
            }
        }
        else
        {
            H5::StrType mem_dtype(H5::PredType::C_S1, data.StringVal().size());
            if (parent_is_compound)
            {
                WriteDataToFile(data.StringVal().c_str(), GetCompoundDataType(mem_dtype, compound_names));
            }
            else
            {
                WriteDataToFile(data.StringVal().c_str(), mem_dtype);
            }
        }
    }
    else if (data.IsMatrix())
    {
        const hwMatrix* mat = data.Matrix();

        if (mat->IsEmpty())
            throw OML_Error(OML_ERR_HDF5_EMPTYMATRIX);

        H5::DataType dt(H5::PredType::NATIVE_DOUBLE);
        
        if (mat->IsReal())
        {
            bool is_refobj = false;
            switch (file_dtype->getClass())
            {
                case H5T_REFERENCE:
                {
                    dt = H5::DataType(H5::PredType::STD_REF_OBJ);
                    if (dt == (*file_dtype))
                        is_refobj = true;
                    else
                        throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);

                    break;
                }
                default:
                    break;
            }

            /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
            //////////////////start
            hwMatrix trans_mat;
            trans_mat.Transpose(*mat);
            double* data_buf = trans_mat.GetRealData();
            ////////end

            if (is_refobj)
            {
                int num_elements = trans_mat.Size();
                REFDATAPTR ref_data_buf = REFDATAPTR(new hobj_ref_t[num_elements]);
                hobj_ref_t* raw_ref_buf = ref_data_buf.get();
                for (int i = 0; i < num_elements; i++)
                {
                    raw_ref_buf[i] = static_cast<hobj_ref_t>(data_buf[i]);
                }

                if (parent_is_compound)
                {
                    WriteDataToFile(raw_ref_buf, GetCompoundDataType(dt, compound_names));
                }
                else
                {
                    WriteDataToFile(raw_ref_buf, dt);
                }
            }
            else
            {
                if (parent_is_compound)
                {
                    WriteDataToFile(data_buf, GetCompoundDataType(dt, compound_names));
                }
                else
                {
                    WriteDataToFile(data_buf, dt);
                }
            }
        }
        else
        {
            if (_attr)
            {
                throw OML_Error(OML_ERR_HDF5_INVALID_ATTRIBUTE_DATA);
            }

            H5::CompType mem_cmp_dtype_real(dt.getSize());
            mem_cmp_dtype_real.insertMember(REAL_FIELD_NAME, 0, dt);

            H5::CompType mem_cmp_dtype_imag(dt.getSize());
            mem_cmp_dtype_imag.insertMember(IMAGINARY_FIELD_NAME, 0, dt);

            /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
            //////////////////start
            hwMatrix real;
            hwMatrix imag;
            mat->UnpackComplex(&real, &imag);

            hwMatrix trans_mat_real;
            trans_mat_real.Transpose(real);
            double* data_buf_real = trans_mat_real.GetRealData();

            hwMatrix trans_mat_imag;
            trans_mat_imag.Transpose(imag);
            double* data_buf_imag = trans_mat_imag.GetRealData();
            ////////end

            if (parent_is_compound)
            {
                WriteDataToFile(data_buf_real, GetCompoundDataType(mem_cmp_dtype_real, compound_names));
                WriteDataToFile(data_buf_imag, GetCompoundDataType(mem_cmp_dtype_imag, compound_names));
            }
            else
            {
                WriteDataToFile(data_buf_real, mem_cmp_dtype_real);
                WriteDataToFile(data_buf_imag, mem_cmp_dtype_imag);
            }
            AddMetadataToComplexDataset();
        }
    }
    else if (data.IsNDMatrix())
    {
        const hwMatrixN* nd_mat = data.MatrixN();

        if (nd_mat->IsEmpty())
            throw OML_Error(OML_ERR_HDF5_EMPTYMATRIX);

        const std::vector<int>& original_dims = nd_mat->Dimensions();
        /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
        //////////////////start
        std::vector<int> permute_vect;
        for (int index = static_cast<int>(original_dims.size()); index > 0; index--)
        {
            permute_vect.push_back(index - 1);
        }
        ////////end

        H5::DataType dt(H5::PredType::NATIVE_DOUBLE);
        if (nd_mat->IsReal())
        {
            bool is_refobj = false;
            switch (file_dtype->getClass())
            {
                case H5T_REFERENCE:
                {
                    dt = H5::DataType(H5::PredType::STD_REF_OBJ);
                    if (dt == (*file_dtype))
                        is_refobj = true;
                    else
                        throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);

                    break;
                }
                default:
                    break;
            }

            /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
            //////////////////start
            hwMatrixN trans_nd_mat;
            trans_nd_mat.Permute(*nd_mat, permute_vect);
            ////////end

            double* raw_buf = trans_nd_mat.GetRealData();

            if (is_refobj)
            {
                int num_elements = trans_nd_mat.Size();

                REFDATAPTR ref_data_buf = REFDATAPTR(new hobj_ref_t[num_elements]);
                hobj_ref_t* raw_ref_buf = ref_data_buf.get();

                for (int i = 0; i < num_elements; i++)
                {
                    raw_ref_buf[i] = static_cast<hobj_ref_t>(raw_buf[i]);
                }

                if (parent_is_compound)
                {
                    WriteDataToFile(raw_ref_buf, GetCompoundDataType(dt, compound_names));
                }
                else
                {
                    WriteDataToFile(raw_ref_buf, dt);
                }
            }
            else
            {
                if (parent_is_compound)
                {
                    WriteDataToFile(raw_buf, GetCompoundDataType(dt, compound_names));
                }
                else
                {
                    WriteDataToFile(raw_buf, dt);
                }
            }
        }
        else
        {
            if (_attr)
            {
                throw OML_Error(OML_ERR_HDF5_INVALID_ATTRIBUTE_DATA);
            }

            H5::CompType mem_cmp_dtype_real(dt.getSize());
            mem_cmp_dtype_real.insertMember(REAL_FIELD_NAME, 0, dt);

            H5::CompType mem_cmp_dtype_imag(dt.getSize());
            mem_cmp_dtype_imag.insertMember(IMAGINARY_FIELD_NAME, 0, dt);

            /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
            //////////////////start
            hwMatrixN nd_mat_real;
            hwMatrixN nd_mat_imag;
            nd_mat->UnpackComplex(&nd_mat_real, &nd_mat_imag);

            hwMatrixN trans_nd_mat_real;
            trans_nd_mat_real.Permute(nd_mat_real, permute_vect);

            hwMatrixN trans_nd_mat_imag;
            trans_nd_mat_imag.Permute(nd_mat_imag, permute_vect);
            ////////end

            if (parent_is_compound)
            {
                WriteDataToFile(trans_nd_mat_real.GetRealData(), GetCompoundDataType(mem_cmp_dtype_real, compound_names));
                WriteDataToFile(trans_nd_mat_imag.GetRealData(), GetCompoundDataType(mem_cmp_dtype_imag, compound_names));
            }
            else
            {
                WriteDataToFile(trans_nd_mat_real.GetRealData(), mem_cmp_dtype_real);
                WriteDataToFile(trans_nd_mat_imag.GetRealData(), mem_cmp_dtype_imag);
            }
            AddMetadataToComplexDataset();
        }
    }
    else if (data.IsCellArray() || data.IsNDCellArray())
    {
        const bool is_cell = data.IsCellArray();

        HML_CELLARRAY*    cell    = nullptr;
        HML_ND_CELLARRAY* nd_cell = nullptr;
        if (is_cell)
        {
            cell = data.CellArray();
            if (cell->IsEmpty())
                throw OML_Error(OML_ERR_HDF5_EMPTYCELL);
        }
        else
        {
            nd_cell = data.CellArrayND();

            if (nd_cell->IsEmpty())
                throw OML_Error(OML_ERR_HDF5_EMPTYCELL);
        }

        Currency& cur = is_cell? (*cell)(0) : (*nd_cell)(0);

        if (!(cur.IsString() || cur.IsMatrix() || cur.IsNDMatrix()))
        {
            throw OML_Error(OML_ERR_HDF5_UNSUPPORT_DATA);
        }

        HML_CELLARRAY trans_cell;
        HML_ND_CELLARRAY trans_nd_cell;
        if (is_cell)
        {
            /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
            //////////////////start
            trans_cell.Transpose(*cell);
            cell = &trans_cell;
            ////////end
        }
        else
        {
            /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
            //start
            const std::vector<int>& dims = nd_cell->Dimensions();
            std::vector<int> permute_vect;
            for (int index = static_cast<int>(dims.size()); index > 0; index--)
            {
                permute_vect.push_back(index - 1);
            }

            trans_nd_cell.Permute(*nd_cell, permute_vect);
            nd_cell = &trans_nd_cell;
            //end
        }


        int total_elements = is_cell ? cell->Size() : nd_cell->Size();
        if (cur.IsString())
        {
            if (file_dtype->isVariableStr())
            {
                H5::StrType mem_dtype(H5::PredType::C_S1, H5T_VARIABLE);
                VARSTRDATAPTR data_buf_uptr = VARSTRDATAPTR(new char* [total_elements * sizeof(char*)]);
                char** data_buf = data_buf_uptr.get();
                for (int i = 0; i < total_elements; i++)
                {
#ifdef OS_WIN
                    if (is_cell)
                        data_buf[i] = _strdup(((*cell)(i)).StringVal().c_str());
                    else
                        data_buf[i] = _strdup(((*nd_cell)(i)).StringVal().c_str());
#else
                    if (is_cell)
                        data_buf[i] = strdup(((*cell)(i)).StringVal().c_str());
                    else
                        data_buf[i] = strdup(((*nd_cell)(i)).StringVal().c_str());
#endif
                }

                 if (parent_is_compound)
                 {
                     WriteDataToFile(data_buf, GetCompoundDataType(mem_dtype, compound_names));
                 }
                 else
                 {
                     WriteDataToFile(data_buf, mem_dtype);
                 }
            }
            else
            {
                size_t size = file_dtype->getSize();
                H5::StrType mem_dtype(H5::PredType::C_S1, size);                
                DATAPTR data_buf_uptr = DATAPTR(new char[total_elements * size]);
                char* data_buf = data_buf_uptr.get();
                memset(data_buf, 0, (total_elements* size));
                char* data_buf_itr = data_buf;
                for (int i = 0; i < total_elements; i++)
                {
                    const std::string& current_string = is_cell? ((*cell)(i)).StringVal(): ((*nd_cell)(i)).StringVal();
                    current_string.copy(data_buf_itr, current_string.length());
                    data_buf_itr += size;
                }

                if (parent_is_compound)
                {
                    WriteDataToFile(data_buf, GetCompoundDataType(mem_dtype, compound_names));
                }
                else
                {
                    WriteDataToFile(data_buf, mem_dtype);
                }
            }
        }
        else if (cur.IsMatrix())
        {
            const hwMatrix* mat = cur.Matrix();
            
            if (mat->IsEmpty())
                throw OML_Error(OML_ERR_HDF5_EMPTYMATRIX);

            H5::PredType type_class = H5::PredType::NATIVE_DOUBLE;
            std::vector<hsize_t> arr_dims;
            arr_dims.push_back(mat->M());
            arr_dims.push_back(mat->N());
            int num_dims = 2;
            H5::DataType dat(H5::PredType::NATIVE_DOUBLE);
            H5::ArrayType mem_arr_type(dat, num_dims, arr_dims.data());
            size_t ele_count = mat->Size();

            if (mat->IsReal())
            {
                DOUBLEDATAPTR data_buf = DOUBLEDATAPTR(new double[total_elements * ele_count]);
                int data_buf_index = 0;
                for (int i = 0; i < total_elements; i++)
                {
                    mat = is_cell ? ((*cell)(i)).Matrix() : ((*nd_cell)(i)).Matrix();
                    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
                    //////////////////start
                    hwMatrix trans_mat;
                    trans_mat.Transpose(*mat);
                    ////////end
                    double* data_d = trans_mat.GetRealData();
                    for (int mat_index = 0; mat_index < ele_count; mat_index++)
                    {
                        data_buf[data_buf_index] = data_d[mat_index];
                        data_buf_index++;
                    }
                }


                if (parent_is_compound)
                {
                    WriteDataToFile(data_buf.get(), GetCompoundDataType(mem_arr_type, compound_names));
                }
                else
                {
                    WriteDataToFile(data_buf.get(), mem_arr_type);
                }
            }
            else
            {
                if (_attr)
                {
                    throw OML_Error(OML_ERR_HDF5_INVALID_ATTRIBUTE_DATA);
                }
                DOUBLEDATAPTR data_buf_real = DOUBLEDATAPTR(new double[total_elements * ele_count]);
                DOUBLEDATAPTR data_buf_imag = DOUBLEDATAPTR(new double[total_elements * ele_count]);

                int data_buf_index = 0;
                for (int i = 0; i < total_elements; i++)
                {
                    mat = is_cell ? ((*cell)(i)).Matrix() : ((*nd_cell)(i)).Matrix();
                    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
                    //////////////////start
                    hwMatrix real;
                    hwMatrix imag;
                    mat->UnpackComplex(&real, &imag);

                    hwMatrix trans_mat_real;
                    hwMatrix trans_mat_imag;
                    trans_mat_real.Transpose(real);
                    trans_mat_imag.Transpose(imag);
                    ////////end
                    double* data_real = trans_mat_real.GetRealData();
                    double* data_imag = trans_mat_imag.GetRealData();

                    for (int mat_index = 0; mat_index < ele_count; mat_index++)
                    {
                        data_buf_real[data_buf_index] = data_real[mat_index];
                        data_buf_imag[data_buf_index] = data_imag[mat_index];
                        data_buf_index++;
                    }
                }

                H5::DataType dt(mem_arr_type);
                H5::CompType mem_cmp_dtype_real(dt.getSize());
                mem_cmp_dtype_real.insertMember(REAL_FIELD_NAME, 0, dt);

                H5::CompType mem_cmp_dtype_imag(dt.getSize());
                mem_cmp_dtype_imag.insertMember(IMAGINARY_FIELD_NAME, 0, dt);

                if (parent_is_compound)
                {
                    WriteDataToFile(data_buf_real.get(), GetCompoundDataType(mem_cmp_dtype_real, compound_names));
                    WriteDataToFile(data_buf_imag.get(), GetCompoundDataType(mem_cmp_dtype_imag, compound_names));
                }
                else
                {
                    WriteDataToFile(data_buf_real.get(), mem_cmp_dtype_real);
                    WriteDataToFile(data_buf_imag.get(), mem_cmp_dtype_imag);
                }
                AddMetadataToComplexDataset();
            }
        }
        else if (cur.IsNDMatrix())
        {
            
            const hwMatrixN* nd_mat = cur.MatrixN();
            if (nd_mat->IsEmpty())
                throw OML_Error(OML_ERR_HDF5_EMPTYMATRIX);

            H5::PredType type_class = H5::PredType::NATIVE_DOUBLE;
            size_t ele_count = nd_mat->Size();

            if (nd_mat->IsReal())
            {
                DOUBLEDATAPTR data_buf = DOUBLEDATAPTR(new double[total_elements * ele_count]);
                const std::vector<int>& original_dims = nd_mat->Dimensions();

                /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
                //////////////////start
                std::vector<int> permute_vect;
                for (int index = static_cast<int>(original_dims.size()); index > 0; index--)
                {
                    permute_vect.push_back(index - 1);
                }
                ////////end

                int data_buf_index = 0;
                for (int i = 0; i < total_elements; i++)
                {
                    nd_mat = is_cell ? ((*cell)(i)).MatrixN() : ((*nd_cell)(i)).MatrixN();
                    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
                    //////////////////start
                    hwMatrixN trans_nd_mat;
                    trans_nd_mat.Permute(*nd_mat, permute_vect);
                    double* data_d = trans_nd_mat.GetRealData();
                    ////////end

                    for (int mat_index = 0; mat_index < ele_count; mat_index++)
                    {
                        data_buf[data_buf_index] = data_d[mat_index];
                        data_buf_index++;
                    }
                }

                if (parent_is_compound)
                {
                    WriteDataToFile(data_buf.get(), GetCompoundDataType(type_class, compound_names));
                }
                else
                {
                    WriteDataToFile(data_buf.get(), type_class);
                }
            }
            else
            {
                if (_attr)
                {
                    throw OML_Error(OML_ERR_HDF5_INVALID_ATTRIBUTE_DATA);
                }
                DOUBLEDATAPTR data_buf_real = DOUBLEDATAPTR(new double[total_elements * ele_count]);
                DOUBLEDATAPTR data_buf_imag = DOUBLEDATAPTR(new double[total_elements * ele_count]);
                
                const std::vector<int>& original_dims = nd_mat->Dimensions();
                /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
                //////////////////start
                std::vector<int> permute_vect;
                for (int index = static_cast<int>(original_dims.size()); index > 0; index--)
                {
                    permute_vect.push_back(index - 1);
                }
                ////////end

                int data_buf_index = 0;
                for (int i = 0; i < total_elements; i++)
                {
                    nd_mat = is_cell ? ((*cell)(i)).MatrixN() : ((*nd_cell)(i)).MatrixN();

                    
                    /*ToDo: After VSM-7079 implementatoin, remove code between start and end tags bellow*/
                    //////////////////start
                    hwMatrixN nd_mat_real;
                    hwMatrixN nd_mat_imag;
                    nd_mat->UnpackComplex(&nd_mat_real, &nd_mat_imag);

                    hwMatrixN trans_nd_mat_real;
                    hwMatrixN trans_nd_mat_imag;

                    trans_nd_mat_real.Permute(nd_mat_real, permute_vect);
                    trans_nd_mat_imag.Permute(nd_mat_imag, permute_vect);

                    double* data_real = trans_nd_mat_real.GetRealData();
                    double* data_imag = trans_nd_mat_imag.GetRealData();
                    ////////end

                    for (int mat_index = 0; mat_index < ele_count; mat_index++)
                    {
                        data_buf_real[data_buf_index] = data_real[mat_index];
                        data_buf_imag[data_buf_index] = data_imag[mat_index];
                        data_buf_index++;
                    }
                }

                H5::DataType dt(type_class);
                H5::CompType mem_cmp_dtype_real(dt.getSize());
                mem_cmp_dtype_real.insertMember(REAL_FIELD_NAME, 0, dt);

                H5::CompType mem_cmp_dtype_imag(dt.getSize());
                mem_cmp_dtype_imag.insertMember(IMAGINARY_FIELD_NAME, 0, dt);

                if (parent_is_compound)
                {
                    WriteDataToFile(data_buf_real.get(), GetCompoundDataType(mem_cmp_dtype_real, compound_names));
                    WriteDataToFile(data_buf_imag.get(), GetCompoundDataType(mem_cmp_dtype_imag, compound_names));
                }
                else
                {
                    WriteDataToFile(data_buf_real.get(), mem_cmp_dtype_real);
                    WriteDataToFile(data_buf_imag.get(), mem_cmp_dtype_imag);
                }
                AddMetadataToComplexDataset();
            }
        }
        else
        {
            throw OML_Error(OML_ERR_HDF5_UNSUPPORT_DATA);
        }
    }
    else if (data.IsStruct())
    {
        if (_attr)
        {
            throw OML_Error(OML_ERR_HDF5_INVALID_ATTRIBUTE_DATA);
        }

        switch (file_dtype->getClass())
        {
            case H5T_COMPOUND:
                break;
            default:
                throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);
        }
        
        H5::CompType comp_dtype;

        if (parent_is_compound)
        {
            if (parent_cmp_type)
                comp_dtype = parent_cmp_type->getMemberCompType(parent_cmp_type->getMemberIndex(child_name));
            else
                throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);
        }
        else
        {
            if (nullptr != _dset)
            {

                comp_dtype = _dset->getCompType();
            }
            else
            {
                comp_dtype = _attr->getCompType();
            }
        }

        StructData* sd = data.Struct();
        if ((1 != sd->M()) || (1 != sd->N()))
            throw OML_Error(OML_ERR_HDF5_STRUCT_INVALID_DIMS);

        std::map<std::string, int> field_names = sd->GetFieldNames();
        std::map<std::string, int>::iterator iter;

        H5::DataType child_dt;
        std::map<std::string, H5::DataType> child_info;

        for (iter = field_names.begin(); iter != field_names.end(); iter++)
        {
            if (comp_dtype.getMemberIndex(iter->first) < 0)
            {
                throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);
            }
            std::vector<int> temp_dims;
            size_t  child_size = 0;
            GetDataTypeInfo(sd->GetValue(0, 0, iter->first), temp_dims, child_dt, child_size, true);
            child_info.insert(std::make_pair(iter->first, child_dt));
        }

        std::map<std::string, H5::DataType>::iterator child_iter;
        for (child_iter = child_info.begin(); child_iter != child_info.end(); child_iter++)
        {
            try
            {
                compound_names.push_back(child_iter->first);
                H5::DataType child_file_dtype= comp_dtype.getMemberDataType(comp_dtype.getMemberIndex(child_iter->first));

                if (parent_is_compound)
                {
                    WriteData(sd->GetValue(0, 0, child_iter->first),
                        &child_file_dtype, compound_names,true, child_iter->first, &comp_dtype);
                }
                else
                {
                    WriteData(sd->GetValue(0, 0, child_iter->first),
                              &child_file_dtype, compound_names, true,  child_iter->first, &comp_dtype);
                }
                compound_names.pop_back();
            }
            catch (H5::DataTypeIException&)
            {
                throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);
            }
        }
    }
    else
    {
        throw OML_Error(OML_ERR_HDF5_UNSUPPORT_DATA);
    }
}

void OmlHdf5Writer::WriteAttribute(const std::string& file_name,
                                   const std::string& location,
                                   const std::string& attr_name,
                                   const Currency&    data,
                                   const bool&        create)
{
    FileManager file_manager(this, file_name, create);

    ValidateAttributeLocation(attr_name, location, create);

    if (create)
    {
        CreateAttribute(attr_name, location, data);
    }
    else
    {
        if ("" == location)
        {
            _attr = new H5::Attribute(_file->openAttribute(attr_name.c_str()));
        }
        else
        {
            if (H5O_TYPE_GROUP == _file->childObjType(location))
            {
                H5::Group grp = _file->openGroup(location.c_str());
                _attr = new H5::Attribute(grp.openAttribute(attr_name.c_str()));
            }
            else if (H5O_TYPE_DATASET == _file->childObjType(location))
            {
                H5::DataSet dset = _file->openDataSet(location.c_str());
                _attr = new H5::Attribute(dset.openAttribute(attr_name.c_str()));
            }
            else
            {
                //do nothing
            }
        }
    }


    try
    {
        H5::DataType dt = _attr->getDataType();
        std::vector<std::string> comp_field_names;
        WriteData(data, &dt, comp_field_names);
    }
    catch (OML_Error & err)
    {
        throw err;
    }
    catch (...)
    {
        throw OML_Error(OML_ERR_HDF5_WRITE_FAILED);
    }
}

void OmlHdf5Writer::CreateAttribute(const std::string& attr_name, const std::string& location, const Currency& data)
{
    std::vector<int> original_dims;
    H5::DataType dt;
    size_t size = 0;
    GetDataTypeInfo(data, original_dims, dt, size, true);

    int rank = static_cast<int>(original_dims.size());

    DIMSPTR maxdims = DIMSPTR(new hsize_t[rank]);
    DIMSPTR dims = DIMSPTR(new hsize_t[rank]);

    int index = 0;
    for (auto& member : original_dims) {
        (dims.get())[index] = member;
        (maxdims.get())[index] = H5S_UNLIMITED;
        index++;
    }

    H5::DataSpace dataspace(rank, dims.get(), maxdims.get());

    try
    {
        if ("" == location)
        {
            _attr = new H5::Attribute(_file->createAttribute(attr_name.c_str(), dt, dataspace));
        }
        else
        {
            if (H5O_TYPE_GROUP == _file->childObjType(location))
            {
                H5::Group grp = _file->openGroup(location.c_str());
                _attr = new H5::Attribute(grp.createAttribute(attr_name.c_str(), dt, dataspace));
            }
            else if (H5O_TYPE_DATASET == _file->childObjType(location))
            {
                H5::DataSet dset = _file->openDataSet(location.c_str());
                _attr = new H5::Attribute(dset.createAttribute(attr_name.c_str(), dt, dataspace));
            }
            else
            {
                //do nothing
            }
        }
    }
    catch (...) 
    {
        throw OML_Error(OML_ERR_HDF5_ATTRIBUTE_CREATION_FAILED);
    }
}

void OmlHdf5Writer::RenameAttribute(const std::string& file_name,
                                    const std::string& src_attr_name,
                                    const std::string& dest_attr_name,
                                    const std::string& location)
{
    FileManager file_manager(this, file_name);
    ValidateAttributeLocation(src_attr_name, location);

    if ("" == location)
    {
        _file->renameAttr(src_attr_name.c_str(), dest_attr_name.c_str());
    }
    else
    {
        if (H5O_TYPE_GROUP == _file->childObjType(location))
        {
            H5::Group grp = _file->openGroup(location.c_str());
            grp.renameAttr(src_attr_name.c_str(), dest_attr_name.c_str());
        }
        else if (H5O_TYPE_DATASET == _file->childObjType(location))
        {
            H5::DataSet dset = _file->openDataSet(location.c_str());
            dset.renameAttr(src_attr_name.c_str(), dest_attr_name.c_str());
        }
        else
        {
            //do nothing
        }
    }
}


void OmlHdf5Writer::RemoveAttribute(const std::string& file_name,
                                    const std::string& attr_name,
                                    const std::string& location)
{
    FileManager file_manager(this, file_name);
    ValidateAttributeLocation(attr_name, location);

    if ("" == location)
    {
        _file->removeAttr(attr_name.c_str());
    }
    else
    {
        if (H5O_TYPE_GROUP == _file->childObjType(location))
        {
            H5::Group grp = _file->openGroup(location.c_str());
            grp.removeAttr(attr_name.c_str());
        }
        else if (H5O_TYPE_DATASET == _file->childObjType(location))
        {
            H5::DataSet dset = _file->openDataSet(location.c_str());
            dset.removeAttr(attr_name.c_str());
        }
        else
        {
            //do nothing
        }
    }
}


void OmlHdf5Writer::ValidateAttributeLocation(const std::string& attr_name, const std::string& location, bool create)
{
    bool is_exist = false;
    if ("" == location)
    {
        is_exist = _file->attrExists(attr_name);

        if (create && is_exist)
        {
            throw OML_Error(OML_ERR_HDF5_ATTRIBUTE_EXIST);
        }

        if (!create && !is_exist)
        { 
            throw OML_Error(OML_ERR_HDF5_INVALID_ATTRIBUTE);
        }
    }
    else
    {
        is_exist = (H5Lexists(_file->getId(), location.c_str(), H5P_DEFAULT) > 0);
        
        if (!is_exist ||
            !((H5O_TYPE_DATASET == _file->childObjType(location)) || 
            (H5O_TYPE_GROUP == _file->childObjType(location))))
        {
            throw OML_Error(OML_ERR_HDF5_NEITHER_DATASET_NOR_GROUP);
        }
       
        if (H5O_TYPE_GROUP == _file->childObjType(location))
        {
            H5::Group grp = _file->openGroup(location.c_str());
            is_exist = grp.attrExists(attr_name);
        }

        if (H5O_TYPE_DATASET == _file->childObjType(location))
        {
            H5::DataSet dset = _file->openDataSet(location.c_str());
            is_exist = dset.attrExists(attr_name);
        }

        if (create && is_exist)
            throw OML_Error(OML_ERR_HDF5_ATTRIBUTE_EXIST);

        if (!create && !is_exist)
            throw OML_Error(OML_ERR_HDF5_INVALID_ATTRIBUTE);
            
    }
}

H5::DataType OmlHdf5Writer::GetCompoundDataType(const H5::DataType premitive_type,const std::vector<std::string>& compound_names)
{
    size_t depth = compound_names.size();
    size_t data_size = premitive_type.getSize();

    H5::CompType mem_cmp_dtype(data_size);
    mem_cmp_dtype.insertMember(compound_names[depth - 1], 0, premitive_type);

    if (depth > 1)
    {
        for (size_t index = depth - 1; index > 0; index--)
        {
            H5::CompType temp_mem_cmp_dtype(data_size);
            temp_mem_cmp_dtype.insertMember(compound_names[index - 1], 0, mem_cmp_dtype);
            mem_cmp_dtype = temp_mem_cmp_dtype;
        }
    }
    return mem_cmp_dtype;
}

void OmlHdf5Writer::AddMetadataToComplexDataset()
{
    if (_dset)
    {
        if (!_dset->attrExists(OML_COMPLEX_ATTRIBUTE))
        { 
            hsize_t dims = 1;
            int data_buf = true;
            H5::DataSpace dataspace(1, &dims);
            std::string attribute_name(OML_COMPLEX_ATTRIBUTE);
            H5::Attribute attr = _dset->createAttribute(attribute_name.c_str(), H5::PredType::NATIVE_INT, dataspace);
            attr.write(H5::PredType::NATIVE_INT, &data_buf);
        }
    }
}
