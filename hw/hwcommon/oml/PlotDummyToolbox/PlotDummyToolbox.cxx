/**
* @file PlotDummyToolbox.cxx
* @date March 2016
* Copyright (C) 2016-2021 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  Licensee may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include "PlotDummyToolbox.h"
#include "EvaluatorInt.h" 
#include "Currency.h"

#define TBOXVERSION 2022.2

// Used for functions implemented in GUI only, registered so they don't result
// in script errors when called from console/batch
bool DummyVoidFunc(EvaluatorInterface eval, 
                   const std::vector<Currency>& in,
                   std::vector<Currency>&       out)
{
    return true;
}



int InitDll(EvaluatorInterface evl)
{
    evl.RegisterBuiltInFunction("plot", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("bar", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("hist", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("area", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("polar", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("line", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("scatter", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("plot3", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("fill", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("stem", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("scatter3", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("surf", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("mesh", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("waterfall", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("contour3", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("contour", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("loglog", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("semilogx", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("semilogy", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("figure", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("subplot", vml_doNothing, FunctionMetaData(3, 1, "Plotting"));
    evl.RegisterBuiltInFunction("axes", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("get", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("set", vml_doNothing, FunctionMetaData(3, 0, "Plotting"));
    evl.RegisterBuiltInFunction("gcf", vml_doNothing, FunctionMetaData(0, 1, "Plotting"));
    evl.RegisterBuiltInFunction("gca", vml_doNothing, FunctionMetaData(0, 1, "Plotting"));
    evl.RegisterBuiltInFunction("clf", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("cla", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("close", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("hold", vml_doNothing, FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("ishold", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("ishandle", vml_doNothing, FunctionMetaData(1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("isfigure", vml_doNothing, FunctionMetaData(1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("isaxes", vml_doNothing, FunctionMetaData(1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("grid", vml_doNothing, FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("title", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("xlabel", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("ylabel", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("zlabel", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("axis", vml_doNothing, FunctionMetaData(-1, -1, "Plotting"));
    evl.RegisterBuiltInFunction("xlim", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("ylim", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("zlim", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("legend", vml_doNothing, FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("text", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("saveas", vml_doNothing, FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("view", vml_doNothing, FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("uicontrol", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("uibuttongroup", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("uipanel", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("uitable", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));

    evl.RegisterBuiltInFunction("uiwait", DummyVoidFunc, 
                                FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("uiresume", DummyVoidFunc,
        FunctionMetaData(1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("waitfor", DummyVoidFunc, FunctionMetaData(-5, 0, "Plotting"));

	evl.RegisterBuiltInFunction("bar3", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
	evl.RegisterBuiltInFunction("colorbar", vml_doNothing, FunctionMetaData(-1, -1, "Plotting"));
	evl.RegisterBuiltInFunction("dock", DummyVoidFunc, FunctionMetaData(-1, 1, "Plotting"));
	evl.RegisterBuiltInFunction("undock", DummyVoidFunc, FunctionMetaData(-1, 1, "Plotting"));
	evl.RegisterBuiltInFunction("drawnow", DummyVoidFunc, FunctionMetaData(0, 0, "Plotting"));
	evl.RegisterBuiltInFunction("plotyy", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
	evl.RegisterBuiltInFunction("ellipse", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
	evl.RegisterBuiltInFunction("polyline", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
	evl.RegisterBuiltInFunction("findobj", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
	evl.RegisterBuiltInFunction("getappdata", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
	evl.RegisterBuiltInFunction("setappdata", DummyVoidFunc, FunctionMetaData(-1, 1, "Plotting"));
	evl.RegisterBuiltInFunction("rectangle", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
	evl.RegisterBuiltInFunction("colormap", vml_doNothing, FunctionMetaData(-1, -1, "Plotting"));
    evl.RegisterBuiltInFunction("getmousepos", DummyVoidFunc, FunctionMetaData(0, 2, "Plotting"));
    evl.RegisterBuiltInFunction("box", DummyVoidFunc, FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("fanplot", DummyVoidFunc, FunctionMetaData(3, -1, "Plotting"));
	evl.RegisterBuiltInFunction("patch", DummyVoidFunc, FunctionMetaData(-1, -1, "Plotting"));
    evl.RegisterBuiltInFunction("uitabgroup", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("uitab", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("uicontextmenu", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("uicontextmenuitem", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("uimenu", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("uimenuitem", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("waitforbuttonpress", vml_doNothing, FunctionMetaData(0, 1, "Plotting"));
    evl.RegisterBuiltInFunction("guidata", vml_doNothing, FunctionMetaData(-2, -1, "Plotting"));
    evl.RegisterBuiltInFunction("ginput", vml_doNothing, FunctionMetaData(-3, -3, "Plotting"));
    evl.RegisterBuiltInFunction("xline", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("yline", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("stem3", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("copystyle", vml_doNothing, FunctionMetaData(1, -1, "Plotting"));
    evl.RegisterBuiltInFunction("pastestyle", vml_doNothing, FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("getframe", vml_doNothing, FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("movie", vml_doNothing, FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("waitbar", vml_doNothing, FunctionMetaData(-3, 1, "Plotting"));
    evl.RegisterBuiltInFunction("quiver", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("savefig", vml_doNothing, FunctionMetaData(-1, 0, "Plotting"));
    evl.RegisterBuiltInFunction("openfig", vml_doNothing, FunctionMetaData(1, -1, "Plotting"));
    evl.RegisterBuiltInFunction("uitree", vml_doNothing, FunctionMetaData(-7, 1, "Plotting"));
    evl.RegisterBuiltInFunction("uitreenode", vml_doNothing, FunctionMetaData(-7, 1, "Plotting"));
    evl.RegisterBuiltInFunction("hist3", vml_doNothing, FunctionMetaData(-2, -1, "Plotting"));
    evl.RegisterBuiltInFunction("datetick", vml_doNothing, FunctionMetaData(-4, 0, "Plotting"));
    evl.RegisterBuiltInFunction("findall", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));

    // colormaps
    evl.RegisterBuiltInFunction("autumn", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("bone", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("cividis", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("cool", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("copper", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("cubehelix", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("flag", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("gray", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("hot", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("hsv", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("inferno", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("jet", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("lines", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("magma", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("ocean", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("pink", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("plasma", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("prism", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("rainbow", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("spring", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("summer", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("twilight", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("viridis", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("white", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));
    evl.RegisterBuiltInFunction("winter", vml_doNothing, FunctionMetaData(-1, 1, "Plotting"));

	return 0;
}
//------------------------------------------------------------------------------
// Dummy function
//------------------------------------------------------------------------------
bool vml_doNothing(EvaluatorInterface           eval,
                   const std::vector<Currency>& inputs,
                   std::vector<Currency>&       outputs)
{
    outputs.push_back(0);
	return true;
}
//------------------------------------------------------------------------------
// Returns toolbox version
//------------------------------------------------------------------------------
double GetToolboxVersion(EvaluatorInterface eval)
{
    return TBOXVERSION;
}
