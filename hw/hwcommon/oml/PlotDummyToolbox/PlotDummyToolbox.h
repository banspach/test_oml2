//-*-c++-*-
/* $\Id$
** -----------------------------------------------------------------------------
** PlotToolbox.h - Definition of Plot Commands class.
** -----------------------------------------------------------------------------
** Copyright (c) 2020 Altair Engineering Inc. All Rights Reserved
** Contains trade secrets of Altair Engineering, Inc.  Copyright notice
** does not imply publication.  Decompilation or disassembly of this
** software is strictly prohibited.
** ----------------------------------------------------------------------------
*/


#include <Evaluator.h>
#include "PlotDummyToolboxDefs.h"

extern "C" 
{
    DUMMYPLOTOMLTBOX_DECLS int InitDll(EvaluatorInterface eval);
    //!
    //! Returns toolbox version
    //! \param eval Evaluator interface
    //!
    DUMMYPLOTOMLTBOX_DECLS double GetToolboxVersion(EvaluatorInterface eval);
}

bool vml_doNothing(EvaluatorInterface eval, const std::vector<Currency>& inputs, std::vector<Currency>& outputs);