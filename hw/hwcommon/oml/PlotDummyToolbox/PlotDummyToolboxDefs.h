////////////////////////////////////////////////////////////////////
// File   : PlotDummyToolboxDefs.h
// Date   : March, 2016
// Copyright (c) 2020 Altair Engineering Inc.  All Rights
// Reserved.  Contains trade secrets of Altair Engineering, Inc.
// Copyright notice does not imply publication.  Decompilation or
// disassembly of this software is strictly prohibited.
////////////////////////////////////////////////////////////////////
//:---------------------------------------------------------------------------
//:Description
//
//  Macros for exporting from the library
//
//:---------------------------------------------------------------------------

#ifndef _dummyPlot_OML_Tbox_Defs_h
#define _dummyPlot_OML_Tbox_Defs_h

// Windows export macro
#ifdef OS_WIN
  #ifdef DUMMYPLOTOMLTBOX_EXPORT
    #undef  DUMMYPLOTOMLTBOX_DECLS
    #define DUMMYPLOTOMLTBOX_DECLS __declspec(dllexport)
  #else
    #undef  DUMMYPLOTOMLTBOX_DECLS
    #define DUMMYPLOTOMLTBOX_DECLS __declspec(dllimport)
  #endif  // DUMMYPLOTOMLTBOX_EXPORT
#else
  #undef  DUMMYPLOTOMLTBOX_DECLS
  #define DUMMYPLOTOMLTBOX_DECLS
#endif // OS_WIN

#endif // _dummyPlot_OML_Tbox_Defs_h
