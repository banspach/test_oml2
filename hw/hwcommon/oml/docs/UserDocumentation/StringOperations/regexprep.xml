<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>regexprep</title>
  <shortdesc>Returns the result after replacing the regular expression, <varname>pat</varname> 
    with <varname>rep</varname> in the input, <varname>src</varname>.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>String Operations Commands</category>
      <keywords>
        <indexterm>regexprep</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>R = regexprep<var>(src, pat, rep)</var></synph></p>
      <p><synph>R = regexprep<var>(src, pat, rep, options...)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>src</varname></pt>
          <pd>Type: <keyword keyref="string_cell"/></pd>
        </plentry>
        <plentry>
          <pt><varname>pattern</varname></pt>
          <pd>Type: <keyword keyref="string_cell"/></pd>          
        </plentry>
        <plentry>
          <pt><varname>rep</varname></pt>
          <pd>Type: <keyword keyref="string_cell"/></pd>          
        </plentry>
        <plentry>
          <pt><varname>options</varname> (optional)</pt>
          <pd>Arguments specifying the search/replace pattern. Valid options are: <dl>
              <dlentry>
                <dt><synph>once</synph></dt>
                <dd>Replaces only the first occurrence of the matching pattern.</dd>
              </dlentry>
              <dlentry>
                <dt><synph>ignorecase</synph></dt>
                <dd>Ignores the case of the matching pattern.</dd>
              </dlentry>
              <dlentry>
                <dt><synph>emptymatch</synph></dt>
                <dd>Matches empty sequences.</dd>
              </dlentry>
            </dl></pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
        <p>Replacement with default options:
        <codeblock>R = regexprep ('www123.com', '(1)', '.$1')</codeblock><codeblock>R = www.123.com</codeblock></p>
        <p>Case-insensitive replacement:
        <codeblock>R = regexprep ('1(a)3[A]5-6{7}', '[^A-Z0-9_]', ':', 'ignorecase')</codeblock><codeblock>R = 1:a:3:A:5:6:7:</codeblock></p>
        <p>Case-insensitive, single replacement:
        <codeblock>R = regexprep ('1(a)3[A]5-6{7}', '[^A-Z0-9_]', ':', 'ignorecase', 'once')</codeblock><codeblock>1:a)3[A]5-6{7}</codeblock></p>
        <p>Replacement with cell array inputs:
        <codeblock>R = regexprep ({'123', '312'}, {'3', '2'}, {'xy', 'abc'})</codeblock><codeblock>R =
{
[1,1] 1abcxy
[2,1] xy1abc
}</codeblock></p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: regexp, strfind, strrep</draft-comment>
    </section>
  </refbody>
</reference>
