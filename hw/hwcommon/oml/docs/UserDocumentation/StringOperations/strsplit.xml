<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>strsplit</title>
  <shortdesc>Tokenizes the string <varname>s</varname> with the given delimiter
      <varname>delim</varname>. </shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>String Operations Commands</category>
      <keywords>
        <indexterm>strsplit</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>R = strsplit<var>(s)</var></synph></p>
      <p><synph>R = strsplit<var>(s, delim)</var></synph></p>
      <p><synph>R = strsplit<var>(s, delim, 'collapsedelimiters', 'value')</var></synph>
      </p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>s</varname></pt>
          <pd>String to be split.</pd>
          <pd>Type: <keyword keyref="string"/></pd>          
        </plentry>
        <plentry>
          <pt><varname>delim</varname> (optional)</pt>
          <pd>Delimiter. Whitespace is the default delimiter.</pd>
          <pd>Type: Any string <keyword keyref="string_cell"/></pd>          
        </plentry>
        <plentry>
          <pt>'collapsedelimiters', <varname>value</varname> (optional)</pt>
          <pd>Name-value pair for specifying output contents. Valid options for
              <varname>value</varname> are <option>true</option> or <option>false</option>. The
            default option if <varname>value</varname> is not specified is
            <option>true</option>.</pd>
          <pd>Type: <keyword keyref="log"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Type: <keyword keyref="cell"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>Using default
        delimiter:<codeblock>R = strsplit('string1  string2       string3')</codeblock><codeblock>R =
{
[1,1] string1
[1,2] string2
[1,3] string3
}</codeblock></p>
      <p>Using a cell array of strings as a
        delimiter:<codeblock>R = strsplit('Split this string at the letter t', {' ', 't'})</codeblock><codeblock>R =
{
[1,1] Spli
[1,2] his
[1,3] s
[1,4] ring
[1,5] a
[1,6] he
[1,7] le
[1,8] er
[1,9]
}</codeblock></p>
      <p>Using a cell array of strings as a delimiter, with '<option>collapsedelimiters</option>' of
          <varname>value</varname>
        <option>false</option>:<codeblock>R = strsplit('Split this string at the letter t', {' ', 't'}, 'collapsedelimiters', false)</codeblock><codeblock>R =
{
[1,1] Spli
[1,2]
[1,3]
[1,4] his
[1,5] s
[1,6] ring
[1,7] a
[1,8]
[1,9]
[1,10] he
[1,11] le
[1,12]
[1,13] er
[1,14]
[1,15]
}</codeblock></p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: strjoin, strtok.</draft-comment>
    </section>
  </refbody>
</reference>
