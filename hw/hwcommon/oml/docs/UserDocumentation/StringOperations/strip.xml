<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>strip</title>
  <shortdesc>Trims leading and trailing characters from strings.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>String Operations Commands</category>
      <keywords>
        <indexterm>strip</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>R = strip<var>(s, mode)</var></synph></p>
      <p><synph>R = strip<var>(s, mode, trim)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>s</varname></pt>
          <pd>Input string(s) to be trimmed. If <varname>s</varname> is a <keyword keyref="cell"/>,
            all elements should be of type <keyword keyref="string"/>.</pd>
          <pd>Type: <keyword keyref="string_cell"/></pd>         
        </plentry>
        <plentry>
          <pt><varname>mode</varname></pt>
          <pd>Indicates whether leading and/or trailing characters should be trimmed from
              <varname>s</varname>. Valid values are <option>'both'</option>,
              <option>'left'</option>, <option>'right'</option>.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>trim</varname></pt>
          <pd>Optional string specifying characters(s) to be trimmed. If 
            <varname>trim</varname> is a <keyword keyref="cell"/>, all elements
            should be of type <keyword keyref="string"/>. Whitespace is the default
            if no string is specified.</pd>
            <pd>Type: <keyword keyref="string_cell"/></pd>
          </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd><synph>R</synph> will have the trimmed strings. If <varname>s</varname> is a <keyword
              keyref="cell"/>, <synph>R</synph> will also be a <keyword keyref="cell"/>.</pd>
          <pd>Type: <keyword keyref="string_cell"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section id="section_zq1_vgm_hlb">
      <title>Examples</title>
      <p>Trims both leading and trailing characters from a string:
        <codeblock>R = strip(',,,this is a test,,', 'both', ',')</codeblock><codeblock>R = this is a test</codeblock></p>
      <p>Trims leading characters from a cell array of strings:
        <codeblock>R = strip({',,,this is a test****0123******????***','foo2*'}, 'right', {',', ' ', '?', '*', '0123'})</codeblock><codeblock>R =
  [1,1] ,,,this is a test
  [1,2] foo2
}</codeblock></p>
    </section>
    <section>
      <draft-comment author="denby">See Also: strtrim, deblank</draft-comment>
    </section>
  </refbody>
</reference>
