<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>interp2</title>
  <shortdesc>Two dimensional interpolation.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Polynomial Math Commands</category>
      <keywords>
        <indexterm>interp2</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>zi = interp2<var>(x,y,z,xi,yi)</var></synph></p>
      <p><synph>zi = interp2<var>(x,y,z,xi,yi,method)</var></synph></p>
      <p><synph>zi = interp2<var>(x,y,z,xi,yi,method,extrap)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>x,y,z</varname></pt>
          <pd>The point coordinate values with which to interpolate.</pd>
          <pd>See Comments for the dimension options.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>z</varname></pt>
          <pd>The grid of function values with which to interpolate.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>xi,yi</varname></pt>
          <pd>The domain points at which to interpolate.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>method</varname></pt>
          <pd>The available options are:<dl>
              <dlentry>
                <dt>'<option>linear</option>'</dt>
                <dd>Returns the bilinear interpolation from the nearest neighbors.</dd>
              </dlentry>
              <dlentry>
                <dt>'<option>spline</option>'</dt>
                <dd>Returns the bicubic spline interpolation from the nearest neighbors.</dd>
              </dlentry>
            </dl></pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>extrap</varname></pt>
          <pd>The available options are:<dl>
              <dlentry>
                <dt>'<option>extrap</option>'</dt>
                <dd>Allows extrapolation beyond the intervals.</dd>
              </dlentry>
              <dlentry>
                <dt>'<option>noextrap</option>'</dt>
                <dd>Does not allow extrapolation (default).</dd>
              </dlentry>
            </dl></pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>zi</synph></pt>
          <pd>The interpolated z values corresponding to (<varname>xi,yi</varname>) pairs.</pd>
          <pd>Dimension: <keyword keyref="mat"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>With '<option>linear</option>' method:</p>
      <codeblock>x = [2, 5, 6, 10, 12, 17];
y = [10, 13, 17, 18, 23, 25, 29];
z = [98  184  214  328   386   528;
129  242  281  431   507   694;
170  319  370  568   668   915;
181  339  393  603   709   971;
232  435  504  774   910  1247;
253  474  549  843   991  1358;
294  551  638  980  1152  1579];
xi = [3, 5, 7, 11, 16];
yi = [11, 16, 21, 29]';
zi = interp2(x,y,z,xi,yi,'linear')</codeblock>
      <codeblock>zi = [Matrix] 4 x 5
140.00000  203.33333  267.83333   394.33333   551.93333
206.41667  299.75000  394.25000   580.75000   813.35000
273.26667  396.60000  521.10000   767.60000  1075.20000
379.66667  551.00000  723.50000  1066.00000  1493.60000</codeblock>
      <p>With '<option>spline</option>' method:</p>
      <codeblock>x = [2, 5, 6, 10, 12, 17];
y = [10, 13, 17, 18, 23, 25, 29];
z = [98  184  214  328   386   528;
129  242  281  431   507   694;
170  319  370  568   668   915;
181  339  393  603   709   971;
232  435  504  774   910  1247;
253  474  549  843   991  1358;
294  551  638  980  1152  1579];
xi = [3, 5, 7, 11, 16];
yi = [11, 16, 21, 29]';
zi = interp2(x,y,z,xi,yi,'spline')</codeblock>
      <codeblock>zi = [Matrix] 4 x 5
138.57956  203.66379  268.77725   394.48736   553.65523
204.24151  299.32574  394.43920   580.14931   814.31718
271.79883  396.88307  521.99653   767.70664  1076.87451
377.91577  551.00000  724.11346  1065.82357  1494.99144</codeblock>
    </section>
    <section>
      <title>Comments</title>
      <p>If <varname>x</varname> and <varname>y</varname> are vectors, then their lengths must equal
        the number of columns and rows of <varname>z</varname>, respectively. If
          <varname>x</varname> and <varname>y</varname> are matrices, then the rows of
          <varname>x</varname> are assumed to be identical, and likewise for the columns of
          <varname>y</varname>.</p>
      <p>If <varname>xi</varname> and <varname>yi</varname> are vectors, then their lengths
        determine the number of columns and rows of <varname>zi</varname>, respectively. If
          <varname>xi</varname> and <varname>yi</varname> are matrices, then they must have the same
        dimensions, which will also be the dimensions of <varname>zi</varname>.</p>
      <p>The '<option>spline</option>' method uses not-a-knot cubic splines on the grid, together
        with a bicubic interpolation between the grid lines.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: interp1, spline</draft-comment>
    </section>
  </refbody>
</reference>
 