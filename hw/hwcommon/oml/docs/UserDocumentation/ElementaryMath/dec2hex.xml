<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>dec2hex</title>
  <shortdesc>Converts non-negative integers to their hexadecimal equivalents.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Elementary Math Commands</category>
      <keywords>
        <indexterm>dec2hex</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>R = dec2hex<var>(n)</var></synph></p>
      <p><synph>R = dec2hex<var>(n, len)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>n</varname></pt>
          <pd>Finite, non-negative integer, matrix, or cell array of finite, non-negative
            integers.</pd>
          <pd>Type: <keyword keyref="cell_mat_int"/></pd>          
        </plentry>
        <plentry>
          <pt><varname>len</varname> (optional)</pt>
          <pd>Specifies the minimum length of each element if <varname>n</varname> is a matrix or a
            cell array. Valid values are finite, positive integers.</pd>
          <pd>Type: Finite, positive <keyword keyref="int"/></pd>
        </plentry>
      </parml><?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>If <varname>n</varname> is a matrix or cell array, <varname>R</varname> will be a
            string array with one row for each element. Every element will be the same width as the
            widest element, with leading zeros padding the value.</pd>
          <pd>Type: <keyword keyref="mat_string"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>Integer to
        hexadecimal:<codeblock>R = dec2hex(9876543210)</codeblock><codeblock>R = 24CB016EA</codeblock></p>
      <p>Matrix to
        hexadecimal:<codeblock>R = dec2hex([97, 65, 1])</codeblock><codeblock>R =
61
41
01</codeblock></p>
      <p>Cell array, with minimum output length specified, to a
        hexadecimal:<codeblock>R = dec2hex({700, 555, 1}, 8)</codeblock><codeblock>R =
000002BC
0000022B
00000001</codeblock></p>
    </section>
    <section>
      <draft-comment author="denby">See Also: hex2dec, dec2bin </draft-comment>
    </section>
  </refbody>
</reference>
