<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>diff</title>
  <shortdesc>Returns a vector or matrix of differences.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Elementary Math Commands</category>
      <keywords>
        <indexterm>diff</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>R = diff<var>(x)</var></synph></p>
      <p><synph>R = diff<var>(x, k)</var></synph></p>
      <p><synph>R = diff<var>(x, k, dim)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>x</varname></pt>
          <pd>The matrix from which to compute differences.</pd>
          <pd>Type: <keyword keyref="dou_int_log_struct_cell"/></pd>
          <pd>Dimension: <keyword keyref="scal_vec_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>k</varname></pt>
          <pd>The number of differences to compute.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>dim</varname></pt>
          <pd>The dimension along which to compute differences.</pd>
          <pd>Default: first non-singleton dimension.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>The returned matrix of k<sup>th</sup> differences.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>Basic <cmdname>diff</cmdname> example:</p>
      <codeblock>R = diff([1,4,1,2])</codeblock>
      <codeblock>R = [ 3 -3 1 ]</codeblock>
      <p>k<sup>th</sup>
        <cmdname>diff</cmdname> example:</p>
      <codeblock>R = diff([1,5,2;8,3,4],1)</codeblock>
      <codeblock>R = [Matrix] 1 x 3
7  -2  2</codeblock>
      <p>k<sup>th</sup>
        <cmdname>diff</cmdname> example with <varname>dim</varname>:</p>
      <codeblock>R = diff([1,5,2;8,3,4],1,2)</codeblock>
      <codeblock>R = [Matrix] 2 x 2
 4  -3
-5   1</codeblock>
    </section>
    <section>
      <title>Comments</title>
      <p>When <varname>k</varname> is provided, but <varname>dim</varname> is not, if
          <varname>k</varname> equals or exceeds the size of the relevant dimension, then the extra
        differences will be applied to the next non-singleton dimension.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: </draft-comment>
    </section>
  </refbody>
</reference>
