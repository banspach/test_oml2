<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>ipermute</title>
  <shortdesc>Generalized reversing transpose of a matrix, using a permutation vector.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Elementary Math Commands</category>
      <keywords>
        <indexterm>ipermute</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>ipermute<var>(A, P)</var></synph></p><?STOP?></section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>A</varname></pt>
          <pd>Type: <keyword keyref="dou_int_char_string_log"/></pd>
          <pd>Dimension: <keyword keyref="scal_vec_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>P</varname></pt>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>The permuted input.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <codeblock>m(:,:,1) = [111, 121, 131, 141; 211, 221, 231, 241; 311, 321, 331, 341];
m(:,:,2) = [112, 122, 132, 142; 212, 222, 232, 242; 312, 322, 332, 342];
m(:,:,3) = [113, 123, 133, 143; 213, 223, 233, 243; 313, 323, 333, 343];
m(:,:,4) = [114, 124, 134, 144; 214, 224, 234, 244; 314, 324, 334, 344];
m(:,:,5) = [115, 125, 135, 145; 215, 225, 235, 245; 315, 325, 335, 345];
p = ipermute(m, [2,3,1])</codeblock>
      <codeblock>p =
slice(:, :, 1) =
[Matrix] 5 x 3
111  211  311
112  212  312
113  213  313
114  214  314
115  215  315

slice(:, :, 2) =
[Matrix] 5 x 3
121  221  321
122  222  322
123  223  323
124  224  324
125  225  325

slice(:, :, 3) =
[Matrix] 5 x 3
131  231  331
132  232  332
133  233  333
134  234  334
135  235  335

slice(:, :, 4) =
[Matrix] 5 x 3
141  241  341
142  242  342
143  243  343
144  244  344
145  245  345</codeblock>
    </section>
    <section>
      <title>Comments</title>
      <p>The vector <varname>P</varname> must contain all explicit dimensions of
        <varname>A</varname>, and may also contain implicit higher singleton dimensions.</p>
      <p><cmdname>ipermute</cmdname> reverses the effect of <cmdname>permute</cmdname> when
        called with the same <varname>P</varname>.</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: circshift, permute, randperm, shiftdim</draft-comment>
    </section>
  </refbody>
</reference>
