<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>dsacpsdplot</title>
  <shortdesc>Plot MotionSolve Dynamic Signal Analysis (DSA) result component in time and frequency domain.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Multibody Processing</category>
      <keywords>
        <indexterm>dsacpsdplot</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>dsacpsdplot<var>(par,val,typ,res,com,reft,refr,refc)</var></synph></p>
      <p><synph>dsacpsdplot<var>(par,val,typ,res,com,reft,refr,refc,tw)</var></synph></p>
	  <p><synph>dsacpsdplot<var>(par,val,typ,res,com,reft,refr,refc,tw,few)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
		<plentry>
          <pt><varname>par</varname></pt>
          <pd>Dataset member name.</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string_cell"/></pd>
        </plentry>
		<plentry>
          <pt><varname>val</varname></pt>
          <pd>Values.</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string_cell"/></pd>
        </plentry>
		<plentry>
          <pt><varname>typ</varname></pt>
          <pd>Result type.</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
        </plentry>
		<plentry>
          <pt><varname>res</varname></pt>
          <pd>Result name(s) / request(s).</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string_cell"/></pd>
        </plentry>
		<plentry>
          <pt><varname>com</varname></pt>
          <pd>Result component(s).</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string_cell"/></pd>
        </plentry>
        <plentry>
          <pt><varname>reft</varname></pt>
          <pd>Reference result type.</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
        </plentry>
		<plentry>
          <pt><varname>refr</varname></pt>
          <pd>Reference result name / request.</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
        </plentry>
		<plentry>
          <pt><varname>refc</varname></pt>
          <pd>Reference result component.</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
        </plentry>
		<plentry>
          <pt><varname>tw</varname></pt>
          <pd>Time window window in a 2-element vector form.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
		  <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>few</varname></pt>
          <pd>Frequency exponent window in a 2-element vector form.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
		  <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section id="section_l3k_q3g_gdb">
      <title>Examples</title>
      <p>Example with results from a rotating blade model.</p>
      <codeblock>dsacpsdplot('./RotatingBlade/blade_1.dat.rpm',{'180';'300';'420'},'Expressions','blade_1 DEF 33 %','F7','Expressions','blade_1 LOAD','F4',[0.2,10],[0.5,2.5]);</codeblock>
    </section>
	<section>
      <title>Comments</title>
	  <p>This function creates a comparison plot of MotionView dsarun results in time and frequency domains by estimated FRF between multiple outputs signals and one single input signal (leveraging cross power spectral density).</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: cpsdplot, dsafftplot, frfplot, fftplot, dsacpsdplot.</draft-comment>
    </section>
  </refbody>
</reference>
