<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>cpsdplot</title>
  <shortdesc>Plot MotionSolve Frequency Response Functions (FRFs) estimated from output/input signals <cmdname>cpsd</cmdname> versus input signals <cmdname>pwelch</cmdname>.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Multibody Processing</category>
      <keywords>
        <indexterm>cpsdplot</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>cpsdplot<var>(xml,typ,res,com,reft,refr,refc)</var></synph></p>
      <p><synph>cpsdplot<var>(xml,typ,res,com,reft,refr,refc,tw)</var></synph></p>
	  <p><synph>cpsdplot<var>(xml,typ,res,com,reft,refr,refc,tw,few)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>xml</varname></pt>
          <pd>XML file names (without extension).</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="cell"/></pd>
        </plentry>
		<plentry>
          <pt><varname>typ</varname></pt>
          <pd>Result type.</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
        </plentry>
		<plentry>
          <pt><varname>res</varname></pt>
          <pd>Result name(s) / request(s).</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string_cell"/></pd>
        </plentry>
		<plentry>
          <pt><varname>com</varname></pt>
          <pd>Result component(s).</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string_cell"/></pd>
        </plentry>
		<plentry>
          <pt><varname>reft</varname></pt>
          <pd>Reference result type.</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
        </plentry>
		<plentry>
          <pt><varname>refr</varname></pt>
          <pd>Reference result name / request.</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
        </plentry>
		<plentry>
          <pt><varname>refc</varname></pt>
          <pd>Reference result component.</pd>
		  <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
        </plentry>
		<plentry>
          <pt><varname>tw</varname></pt>
          <pd>Time window window in a 2-element vector form.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
		  <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>few</varname></pt>
          <pd>Frequency exponent window in a 2-element vector form.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
		  <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section id="section_l3k_q3g_gdb">
      <title>Examples</title>
      <p>Example with results from a landing gear model.</p>
      <codeblock>cpsdplot({'./LandingGear_Retracted/ldg_retracted';'./LandingGear_Deployed/ldg_deployed'},'Expressions','dxyz',{'DXleft';'DYleft';'DZleft'},'Expressions','drag','FXright',[0.2,10],[0.5,2.5]);</codeblock>
    </section>
	<section>
      <title>Comments</title>
	  <p>This function creates a comparison plot of MotionView results in time and frequency domains by estimated FRF between multiple outputs signals and one single input signal (leveraging cross power spectral density).</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: dsafrfplot, dsacpsdplot, dsafftplot, frfplot, fftplot.</draft-comment>
    </section>
  </refbody>
</reference>
