<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>invfreqz</title>
  <shortdesc>Compute digital filter coefficients from frequency response values.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Signal Processing Commands</category>
      <keywords>
        <indexterm>invfreqz</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>[b,a] = invfreqz<var>(h,f,nb,na)</var></synph></p>
      <p><synph>[b,a] = invfreqz<var>(h,f,nb,na,w)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>h</varname></pt>
          <pd>The complex frequency response values.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>f</varname></pt>
          <pd>The frequencies corresponding to <varname>h</varname>. The values in Hz must be
            normalized relative to<equation-inline> fs/(2*pi)</equation-inline>, where
              <equation-inline>fs</equation-inline> is the sampling frequency, so that the Nyquist
            frequency corresponds to a value of pi.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>nb</varname></pt>
          <pd>The filter numerator polynomial order. </pd>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>na</varname></pt>
          <pd>The filter denominator polynomial order. </pd>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>w</varname></pt>
          <pd>Optional weights applied to achieve a weighted fitting of the response values.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><varname>b</varname></pt>
          <pd>The estimated numerator polynomial coefficients of the filter.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>a</varname></pt>
          <pd>The estimated denominator polynomial coefficients of the filter.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <p>Recover coefficients from the output of a digital Chebyshev I filter.</p>
      <codeblock>order = 3;
fc = 200;
fs = 1000;
[b1,a1] = cheby1(order, 1, fc/(fs/2), 'z')
f = [0:0.2:2] * fc;
h = freqz(b1,a1,f,fs);
[b2,a2] = invfreqz(h,pi*f/(fs/2),order,order)</codeblock>
      <codeblock>b1 = [Matrix] 1 x 4
0.07360  0.22079  0.22079  0.07360
a1 = [Matrix] 1 x 4
1.00000  -0.97613  0.85676  -0.29186
b2 = [Matrix] 1 x 4
0.07360  0.22079  0.22079  0.07360
a2 = [Matrix] 1 x 4
1.00000  -0.97613  0.85676  -0.29186</codeblock>
    </section>
    <section>
      <title>Comments</title>
      <p>It is recommended to use <cmdname>freqz</cmdname> to assess the quality of the fitted
        filter coefficients.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: freqz, invfreqs</draft-comment>
    </section>
  </refbody>
</reference>
