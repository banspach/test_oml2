<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>besself</title>
  <shortdesc>Create a Bessel filter.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Signal Processing Commands</category>
      <keywords>
        <indexterm>besself</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>[b,a] = besself<var>(n,Wp)</var></synph></p>
      <p><synph>[b,a] = besself<var>(n,Wp,band)</var></synph></p>
      <p><synph>[b,a] = besself<var>(n,Wp,domain)</var></synph></p>
      <p><synph>[b,a] = besself<var>(n,Wp,band,domain)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>n</varname></pt>
          <pd>The filter order. </pd>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>Wp</varname></pt>
          <pd>A scalar specifying the frequency band of a low or high pass filter, or a two element
            vector specifying the band of a bandpass or bandstop filter. For a digital filter, the
            values (in Hz) are normalized relative to the Nyquist frequency. For an analog filter,
            the values are in radians/sec. See the Comments below.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="scal_vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>band</varname></pt>
          <pd>The band type of the filter. Omit for low pass or bandpass. Use
            '<varname>high</varname>' for high pass, and '<varname>stop</varname>' for
            bandstop.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>domain</varname></pt>
          <pd>
            <ul>
              <li>Use <option>'z'</option> for digital filters (default).</li>
              <li>Use <option>'s'</option> for analog filters.</li>
            </ul>
          </pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>b</synph></pt>
          <pd>The numerator polynomial coefficients of the filter.</pd>
          <pd>Type: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><synph>a</synph></pt>
          <pd>The denominator polynomial coefficients of the filter.</pd>
          <pd>Type: <keyword keyref="vec"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <p>Create a fourth order Bessel low pass digital filter with a 300 Hz cutoff frequency
        and a 1000 Hz sampling frequency.</p>
      <codeblock>[b,a] = besself(4,300/500)</codeblock>
      <codeblock>b = [Matrix] 1 x 5
0.14044  0.56175  0.84263  0.56175  0.14044
a = [Matrix] 1 x 5
1.00000  0.72195  0.42631  0.08850  0.01025</codeblock>
    </section>
    <section>
      <title>Comments</title>
      <p>The cutoff frequency is not the 3dB attenuation frequency, but specifies the band for which
        the group delay will be nearly constant. The filter has the same asymptotic behavior as the
        Butterworth filter with the same 3dB cutoff frequency. Compare with <cmdname>besself3</cmdname>.</p>
      <p>Bessel filters are desirable for their property of having a maximally linear phase response
        in the pass band of an analog filter. For digital filters, this property is lost unless the
        sampling frequency is substantially higher than the cutoff frequency.</p>
      <p>Filters can become unstable for high orders, and more easily so for bandpass or stopband
        filters.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: besself3, butter, freqs, freqz, freqs, cheby1, cheby2,
        ellip</draft-comment>
    </section>
  </refbody>
</reference>
