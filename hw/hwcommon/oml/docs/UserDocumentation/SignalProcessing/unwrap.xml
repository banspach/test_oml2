<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>unwrap</title>
  <shortdesc>Unwrap a vector of phase angles.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Signal Processing Commands</category>
      <keywords>
        <indexterm>unwrap</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>u = unwrap<var>(p)</var></synph></p>
      <p><synph>u = unwrap<var>(p,tol)</var></synph></p>
      <p><synph>u = unwrap<var>(p,tol,dim)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>p</varname></pt>
          <pd>A vector or matrix of phase angles in radians.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>tol</varname></pt>
          <pd>A scalar threshold that sets the maximum step between adjacent angles in radians.
            Steps larger than <varname>tol</varname> are reduced to less than tol by adding or subtracting
            multiples of 2*pi. The default is pi, and smaller values are treated as pi.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>dim</varname></pt>
          <pd>Dimension on which to perform the calculation.</pd>
          <pd>(default: first non-singular dimension).</pd>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>y</synph></pt>
          <pd>The vector of unwrapped phase ngles.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <codeblock>p = tan(-3:0.5:3)
u = unwrap(p)</codeblock>
      <codeblock>p = [Matrix] 1 x 13
0.14255  0.74702  2.18504  -14.10142  -1.55741  -0.54630  0.00000  0.54630
1.55741  14.10142  -2.18504  -0.74702  -0.14255
u = [Matrix] 1 x 13
0.14255  0.74702  2.18504  4.74814  4.72578  5.73688  6.28319  6.82949  7.84059
7.81823  10.38133  11.81935  12.42382</codeblock>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: atan, atan2</draft-comment>
    </section>
  </refbody>
</reference>
