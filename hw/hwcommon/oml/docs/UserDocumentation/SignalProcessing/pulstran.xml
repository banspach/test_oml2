<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>pulstran</title>
  <shortdesc>Generate a pulse train, with the pulse defined either by a function or a sampled pulse.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Signal Processing Commands</category>
      <keywords>
        <indexterm>pulstran</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>pt = pulstran<var>(t, d, 'func')</var></synph></p>
      <p><synph>pt = pulstran<var>(t, d, 'func', p1, p2, ...)</var></synph></p>
      <p><synph>pt = pulstran<var>(t, d, p)</var></synph></p>
      <p><synph>pt = pulstran<var>(t, d, p, fsp)</var></synph></p>
      <p><synph>pt = pulstran<var>(t, d, p, fsp, method)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>t</varname></pt>
          <pd>The times at which to generate the pulse train.</pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>d</varname></pt>
          <pd>When d is a vector it contains the time delay for each pulse in the train. See comments.</pd>
          <pd>When d is a matrix, the first column contains the time delay for each pulse, and
            the second column contains the amplitude for each pulse.</pd>
          <pd>Dimension: <keyword keyref="vec_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>'func'</varname></pt>
          <pd>The function to generate the individual pulses.</pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
          <pd>The options are as follows.
            <ul>
              <li><option>'rectpulse'</option> (default)</li>
              <li><option>'tripuls'</option></li>
              <li><option>'gauspuls'</option></li>
            </ul>
          </pd>
        </plentry>
        <plentry>
          <pt><varname>p1, p2, ...</varname></pt>
          <pd>The optional parameters to pass to <varname>'func'</varname>.</pd>
          <pd>Dimension: <keyword keyref="scalar"/></pd>
        </plentry>
        <plentry>
          <pt><varname>p</varname></pt>
          <pd>The sampled pulse to be replicated in the train.</pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>fsp</varname></pt>
          <pd>The sampling frequency for <varname>p</varname>.</pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>'method'</varname></pt>
          <pd>The interpolation method used with replicated <varname>p</varname> pulses.</pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
          <pd>The options, as follows, are those for <cmdname>interp1</cmdname>.
            <ul>
              <li><option>'linear'</option> (default)</li>
              <li><option>'pchip'</option></li>
              <li><option>'spline'</option></li>
            </ul>
          </pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>pt</synph></pt>
          <pd>The sampled pulse train.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example 1</title>
      <p>Plot a train of 2 ms rectangular pulses at 5 ms intervals.</p>
      <codeblock>
fs = 20000;		% sampling rate of the output
ts = 1 / fs;
fp = 200;		% pulse rate
tp = 1 / fp;
t = [0:ts:4*tp];
d = [0:tp:4*tp];
w = .4 * tp;
p = pulstran (t, d, 'rectpuls', w);
plot(1000 * t, p);
title ('Train of 2 ms rectangular pulses at 5 ms intervals');
xlabel ('Time (ms)');
ylabel ('Amplitude');
</codeblock>
      <fig>
        <title>pulstran figure 1</title>
        <image placement="break"
          href="images/pulstran_help_figure1.png"
          height="350" width="350" scale="40"/>
      </fig>
    </section>
    <section>
      <title>Example 2</title>
      <p>Plot a train of user defined at 5 ms intervals, with increasing amplitude.</p>
      <codeblock>
fs = 10000;		% sampling frequency of the output
ts = 1 / fs;
fp = 200;		% pulse frequency
tp = 1 / fp;
t = [0:ts:4*tp];
d = [0:tp:4*tp] + 0.5*tp;
np = length(d);
d = [d',[1:np]'];
p = [0.010 0.134 0.409 0.800 1.000 0.800 0.409 0.134 0.010];
fsp = 3200;		% sampling frequency of p
x = pulstran (1000 * t, d, p, fsp, 'pchip');
plot(t, x);
xlabel ('Time (ms)');
ylabel ('Amplitude');
      </codeblock>
      <fig>
        <title>pulstran figure 1</title>
        <image placement="break"
          href="images/pulstran_help_figure2.png"
          id="image" height="350" width="350" scale="40"/>
      </fig>
    </section>
    <section>
      <title>Comments</title>
      <p>It is important to note that when using <varname>'func'</varname> the delay
        times apply to the centers of the pulses, but when using <varname>p</varname>
        the delay times apply to the left endpoints of the pulses.</p>
      <p>When using <varname>'func'</varname> = 'gauspuls',
        <cmdname>gauspuls</cmdname><varname>('cutoff', fc, bw, bwr, tpr)</varname> is called
        to set the pulse width, so <varname>'tpr'</varname> can be passed to
        <cmdname>pulstran</cmdname> as <varname>'p4'</varname>.</p>
      <draft-comment author="eenyeart">gauspuls, rectpuls, tripuls</draft-comment>
    </section>
    
  </refbody>
</reference>
