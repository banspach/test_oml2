<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>regress</title>
  <shortdesc> Perform multiple linear regression using the model <synph>y = X * beta + e</synph>. </shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Statistical Analysis Commands</category>
      <keywords>
        <indexterm>regress</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>[b,bi,r,ri,stats]=regress<var>(y,X)</var></synph></p>
      <p><synph>[b,bi,r,ri,stats]=regress<var>(y,X,alpha)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>y</varname></pt>
          <pd>The response values.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>X</varname></pt>
          <pd>The regressor variable values. The first column is a vector of 1 values.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>alpha</varname></pt>
          <pd>The level of significance (default: 0.05).</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>b</synph></pt>
          <pd>The regression coefficients (beta estimates).</pd>
        </plentry>
        <plentry>
          <pt><synph>bci</synph></pt>
          <pd>The regression coefficient confidence intervals.</pd>
        </plentry>
        <plentry>
          <pt><synph>r</synph></pt>
          <pd>The residuals.</pd>
        </plentry>
        <plentry>
          <pt><synph>rci</synph></pt>
          <pd>The residuals confidence intervals.</pd>
        </plentry>
        <plentry>
          <pt><synph>stats</synph></pt>
          <pd>Regression statistics. The following values are returned:</pd>
          <pd>R^2, the coefficient of determination</pd>
          <pd>F, the F statistic</pd>
          <pd>p, the p value</pd>
          <pd>v, the estimated variance of the random errors</pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <codeblock>y = [6.59; 7.89; 8.49; 3.5; 6.7; 6.9; 4.99; 5.09; 8.09]
X = [1.0, -0.69, 2.0; 1.0, -0.69, 3.0; 1.0, -0.69, 4.0;
1.0, 0.0, 2.0; 1.0, 0.0, 3.0; 1.0, 0.0, 4.0;
1.0, 0.41, 2.0; 1.0, 0.41, 3.0; 1.0, 0.41, 4.0]
[b,bi,r,ri,stats]=regress(y,X)</codeblock>
      <codeblock>b = [Matrix] 3 x 1
 2.12192
-1.59846
 1.40000
bi = [Matrix] 3 x 2
-0.82651  5.07035
-3.30187  0.10494
 0.45306  2.34694
r = [Matrix] 9 x 1
 0.56514
 0.46514
-0.33486
-1.42192
 0.37808
-0.82192
 0.72345
-0.57655
 1.02345
ri = [Matrix] 9 x 2
-1.17863  2.30891
-1.59401  2.52430
-2.14839  1.47867
-2.90897  0.06513
-1.97507  2.73122
-2.77729  1.13345
-1.05248  2.49938
-2.68810  1.53499
-0.56601  2.61290
stats = [Matrix] 1 x 4
0.75369  9.17986  0.01494  0.89858</codeblock>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: qr</draft-comment>
    </section>
  </refbody>
</reference>
