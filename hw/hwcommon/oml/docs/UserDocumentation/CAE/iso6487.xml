<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>iso6487</title>
  <shortdesc>Filters a signal with an ISO6487 filter.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Signal Processing Commands</category>
      <keywords>
        <indexterm>iso6487</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>y = iso6487<var>(x,fs,cfc)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>x</varname></pt>
          <pd>The signal to be filtered. If <varname>x</varname> is a matrix, each column is
            filtered.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>fs</varname></pt>
          <pd>The sampling frequency.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>cfc</varname></pt>
          <pd>The channel frequency class of the filter (typically 60 or 180).</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>y</synph></pt>
          <pd>The filtered signal.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <p>Filter a 100 Hz signal bi-directionally, sampled at 10,000 Hz, with a class 60 filter.</p>
      <codeblock>t = [0:0.0001:0.02];
input = sin(2*pi*100*t);
plot(t,input);
hold on;</codeblock>
      <codeblock><systemoutput>output = iso6487(input, 10000, 60);
plot(t,output);
legend('input','output');</systemoutput></codeblock>
      <fig id="fig_t5n_v4r_bt">
        <title>filter</title>
        <image placement="break"
          href="images/iso6487_help_figure1.png"/>
      </fig>
    </section>
    <section>
      <title>Comments</title>
      <p><cmdname>iso6487</cmdname> is based on the ISO 6487 standard. It uses a bi-directional,
        second order low pass Butterworth filter with mirror padding. The <varname>cfc</varname> is
        approximately 60% of the 3dB cutoff frequency.</p>
      <p><varname>fs</varname> should be at least 10 times <varname>cfc</varname>.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: saefilt95, saefilter.</draft-comment>
    </section>
  </refbody>
</reference>
