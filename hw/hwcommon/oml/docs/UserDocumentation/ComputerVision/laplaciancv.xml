<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>laplaciancv</title>
  <shortdesc>Apply a Laplace filter on the image, <varname>handle</varname>.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>ComputerVision Commands</category>
      <keywords>
        <indexterm>laplaciancv</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>R = laplaciancv(<var>handle</var>, <var>depth</var>...)</synph></p>
      <p><synph>R = laplaciancv(<var>handle</var>, <var>depth</var>, <var>aperturesize</var>, <var>scale</var>, <var>delta</var>, <var>type</var>)</synph></p>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>handle</varname></pt>
          <pd>Handle of an image.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>depth</varname></pt>
          <pd>Desired depth of the output image, <varname>R</varname>.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>aperturesize</varname></pt>
          <pd>Optional aperture size for the computation of second-derivative filters. 
            Must be a positive and odd integer. Default value is 1.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>scale</varname></pt>
          <pd>Optional scale factor. Default value is 1.0, with no scaling applied.</pd>
          <pd>Type: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>delta</varname></pt>
          <pd>Optional delta value to be added to <varname>R</varname>. Default value is 0.0</pd>
          <pd>Type: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>type</varname></pt>
          <pd>Optional type specifying the pixel extrapolation method. Default value is 4. Valid values are:
            <dl outputclass="table">
              <dlentry>
                <dt><option>0</option></dt>
                <dd>Type cv::BORDER_CONSTANT.</dd>
              </dlentry>
              <dlentry>
                <dt><option>1</option></dt>
                <dd>Type cv::BORDER_REPLICATE</dd>
              </dlentry>
              <dlentry>
                <dt><option>2</option></dt>
                <dd>Type cv::BORDER_REFLECT</dd>
              </dlentry>
              <dlentry>
                <dt><option>3</option></dt>
                <dd>Type cv::BORDER_WRAP</dd>
              </dlentry>
              <dlentry>
                <dt><option>4</option></dt>
                <dd>Type cv::BORDER_DEFAULT - default</dd>
              </dlentry>
              <dlentry>
                <dt><option>16</option></dt>
                <dd>Type cv::BORDER_ISOLATED</dd>
              </dlentry>
            </dl>
          </pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Handle of image with Laplacian filter applied.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
        <p>Apply a Laplace filter with default options:
<codeblock>
handle = imreadcv('owl.jpg');
depth = 3;
R = laplaciancv(handle, 3);</codeblock>
            <fig>
                <title>Input image</title>
                <image placement="break"
                  href="images/laplaciancv_fig1.png"/>
            </fig>
          <fig>
            <title>Input image</title>
            <image placement="break"
              href="images/laplaciancv_fig2.PNG"/>
          </fig></p>
    </section>
    <section><draft-comment author="denby">
      ComputerVision commands are available only in the Business edition.
      See Also: imreadcv, bilateralfiltercv, filter2Dcv, gaussianblurcv</draft-comment>
    </section>
  </refbody>
</reference>
