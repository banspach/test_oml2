<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>erodecv</title>
  <shortdesc>Erode the given image, <varname>handle</varname>.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>ComputerVision Commands</category>
      <keywords>
        <indexterm>erodecv</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>R = erodecv(<var>handle</var>, <var>kernel</var>...)</synph></p>
      <p><synph>R = erodecv(<var>handle</var>, <var>kernel</var>, <var>anchor</var>, <var>iterations</var>, <var>type</var>, <var>color</var>)</synph></p>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>handle</varname></pt>
          <pd>Handle of an image.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>kernel</varname></pt>
          <pd>Structuring element used for dilation. This can be created using the command <synph>getstructuringelementcv</synph>.
            An empty matrix, [] can also be used and a default structuring element will be used.</pd>
          <pd>Type: <keyword keyref="int_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>anchor</varname></pt>
          <pd>Optional vector of 2 integers specifying the anchor position.
            Default value is [-1 -1], indicating the anchor is at the center of the element.</pd>
          <pd>Type: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>iterations</varname></pt>
          <pd>Optional parameter specifying the number of times erosion is applied. Default value is 1.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>type</varname></pt>
          <pd>Optional parameter specifying the pixel extrapolation method. Default value is 0.
            Valid values are:
            <dl outputclass="table">
              <dlentry>
                <dt><option>0</option></dt>
                <dd>Type cv::BORDER_CONSTANT - default value</dd>
              </dlentry>
              <dlentry>
                <dt><option>1</option></dt>
                <dd>Type cv::BORDER_REPLICATE</dd>
              </dlentry>
              <dlentry>
                <dt><option>2</option></dt>
                <dd>Type cv::BORDER_REFLECT</dd>
              </dlentry>
              <dlentry>
                <dt><option>4</option></dt>
                <dd>Type cv::BORDER_REFLECT_101</dd>
              </dlentry>
              <dlentry>
                <dt><option>16</option></dt>
                <dd>Type cv::BORDER_ISOLATED</dd>
              </dlentry>
            </dl>
          </pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>color</varname></pt>
          <pd>Optional 3-element vector of integers representing blue, green, red (BGR) colors, if <varname>type</varname>
            is <synph>0</synph>. Black will be the default color if nothing is specified with <varname>type</varname>.</pd>
          <pd>Type: <keyword keyref="vec"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>r</synph></pt>
          <pd>Handle of the resulting image.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
        <p>Erode an image with default options:
<codeblock>
handle = imreadcv('image1.jpg');
R = erodecv(handle, []);</codeblock></p>
      <p>Erode an image with a specified structural element:
<codeblock>
handle = imreadcv('image1.jpg');
kernel = getstructuringelementcv(1, [7 7]);
R = eerodecv(handle, kernel, [-1 -1], 1, 0);</codeblock></p>
    </section>
    <section>
      <draft-comment author="denby">ComputerVision commands are available only in the Business edition.
        See Also: dilatecv, getstructuringelementcv, morphologyexcv, imreadcv, imshowcv</draft-comment>
    </section>
  </refbody>
</reference>
