<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>thresholdcv</title>
  <shortdesc>Applies a threshold to each element of the given image, <varname>handle</varname>,
    resulting in a grayscale image, <varname>R</varname>, whose pixels represent the elements
    exceeding the threshold.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>ComputerVision Commands</category>
      <keywords>
        <indexterm>resizecv</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>[computedthreshold, R] = thresholdcv(<var>handle</var>, <var>threshold</var>, <var>max</var>, <var>type</var>)</synph></p>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>handle</varname></pt>
          <pd>Handle of an image.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>threshold</varname></pt>
          <pd>Real vector of [width height] representing the dimensions of
            <varname>R</varname>.</pd>
          <pd>Type: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>fx</varname></pt>
          <pd>Threshold value.</pd>
          <pd>Type: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>max</varname></pt>
          <pd>Maximum value to use with <varname>type</varname> value of 8 or 16.</pd>
          <pd>Type: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>type</varname></pt>
          <pd>Threshold type. Valid values are:
            <dl outputclass="table">
              <dlentry>
                <dt><option>0</option></dt>
                <dd><varname>R</varname>(x,y) = <varname>max</varname> if
                  <varname>handle</varname>(x,y) > <varname>threshold</varname> and 0
                  otherwise.</dd>
              </dlentry>
              <dlentry>
                <dt><option>1</option></dt>
                <dd><varname>R</varname>(x,y) = 0 if <varname>handle</varname>(x,y) >
                    <varname>threshold</varname> and <varname>max</varname> otherwise.</dd>
              </dlentry>
              <dlentry>
                <dt><option>2</option></dt>
                <dd><varname>R</varname>(x,y) = <varname>threshold</varname> if
                    <varname>handle</varname>(x,y) > <varname>threshold</varname> and
                    <varname>handle</varname>(x,y) otherwise.</dd>
              </dlentry>
              <dlentry>
                <dt><option>3</option></dt>
                <dd><varname>R</varname>(x,y) = <varname>handle</varname>(x,y) if
                    <varname>handle</varname>(x,y) > <varname>threshold</varname> and 0
                  otherwise.</dd>
              </dlentry>
              <dlentry>
                <dt><option>4</option></dt>
                <dd><varname>R</varname>(x,y) = 0 if <varname>handle</varname>(x,y) >
                    <varname>threshold</varname> and <varname>handle</varname>(x,y) otherwise.</dd>
              </dlentry>
              <dlentry>
                <dt><option>8</option></dt>
                <dd>Uses Otsu algorithm for optimal threshold.</dd>
              </dlentry>
              <dlentry>
                <dt><option>16</option></dt>
                <dd>Uses Triangle algorithm for optimal threshold.</dd>
              </dlentry>
            </dl>
          </pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>computedthreshold</synph></pt>
          <pd>Computed threshold value.</pd>
          <pd>
            Type: <keyword keyref="scal"/>
          </pd>
        </plentry>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Handle of the resized image.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
        <p>Apply a threshold to an image:
<codeblock>handle = imreadcv('bird4.jpg', 0);
[computedthreshold, mask] = thresholdcv(handle, 10, 255, 0);</codeblock>
            <fig>
                <title>Input image</title>
                <image placement="break"
                  href="images/thresholdcv_fig1.png"/>
            </fig>
            <fig>
                <title>Mask</title>
                <image placement="break"
                  href="images/thresholdcv_fig2.png"/>
            </fig>
        </p>
		<p>Apply a threshold to an image containing numbers written in grayscale equal to the number itself with threshold equals to zero and maximum value equals to 255 (white):
<codeblock>src = imreadcv('thresholdcv_fig1.png');
figure(1);
imshowcv(src);

thresh = 0;
maxValue = 255;

[computedthreshold, mask] = thresholdcv(src, thresh, maxValue, 0); %Binary threshold

figure(2);
imshowcv(mask);</codeblock>
            <fig>
                <title>Input image</title>
                <image placement="break"
                  href="images/thresholdcv_fig3.png"/>
            </fig>
            <fig>
                <title>Mask</title>
                <image placement="break"
                  href="images/thresholdcv_fig4.png"/>
            </fig>
        </p>
		<p>Apply a threshold to the same image as described above containing numbers with threshold equals to 127, which removes all numbers less than or equal to 127:
<codeblock>src = imreadcv('thresholdcv_fig1.png');
figure(1);
imshowcv(src);

thresh = 127;
maxValue = 255;

[computedthreshold, mask] = thresholdcv(src, thresh, maxValue, 0); %Binary threshold

figure(2);
imshowcv(mask);</codeblock>
            <fig>
                <title>Mask</title>
                <image placement="break"
                  href="images/thresholdcv_fig5.png"/>
            </fig>
        </p>
		<p>Apply a threshold as described in the previous example with a maximum value of 150, to set the value of the thresholded regions to 150:
<codeblock>src = imreadcv('thresholdcv_fig1.png');
figure(1);
imshowcv(src);

thresh = 127;
maxValue = 150;

[computedthreshold, mask] = thresholdcv(src, thresh, maxValue, 0); %Binary threshold

figure(2);
imshowcv(mask);</codeblock>
            <fig>
                <title>Mask</title>
                <image placement="break"
                  href="images/thresholdcv_fig6.png"/>
            </fig>
        </p>
		<p>Apply a threshold to the same image as described above with threshold equals to zero and maximum value equals to 255 (white), but using the inverse binary thresholding:
<codeblock>src = imreadcv('thresholdcv_fig1.png');
figure(1);
imshowcv(src);

thresh = 0;
maxValue = 255;

[computedthreshold, mask] = thresholdcv(src, thresh, maxValue, 1); %Inverse binary threshold

figure(2);
imshowcv(mask);</codeblock>
            <fig>
                <title>Mask</title>
                <image placement="break"
                  href="images/thresholdcv_fig7.png"/>
            </fig>
        </p>
		<p>Apply a threshold to the same image as described above with threshold equals to 150 and maximum value equals to 255 (white), using the truncate thresholding. All values above the threshold are set to the threshold value and all values below remain unchanged:
<codeblock>src = imreadcv('thresholdcv_fig1.png');
figure(1);
imshowcv(src);

thresh = 150;
maxValue = 255;

[computedthreshold, mask] = thresholdcv(src, thresh, maxValue, 2); %Inverse binary threshold

figure(2);
imshowcv(mask);</codeblock>
            <fig>
                <title>Mask</title>
                <image placement="break"
                  href="images/thresholdcv_fig8.png"/>
            </fig>
        </p>
    </section>
    <section>
      <draft-comment author="denby">ComputerVision commands are available only in the Business edition.
        See Also: imreadcv, imshowcv, adaptivethresholdcv</draft-comment>
    </section>
  </refbody>
</reference>
