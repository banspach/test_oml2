<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>goodfeaturestotrackcv</title>
  <shortdesc>Finds prominent corners in an image.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>ComputerVision Commands</category>
      <keywords>
        <indexterm>goodfeaturestotrackcv</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>R = goodfeaturestotrackcv(<var>handle</var>, <var>maxCorners</var>, <var>quality</var>, <var>minDistance</var>...)</synph></p>
      <p><synph>R = goodfeaturestotrackcv(<var>handle</var>, <var>maxCorners</var>, <var>quality</var>, <var>minDistance</var>, <var>mask</var>, <var>blockSize</var>, <var>useHDetector</var>, <var>freeParam</var>)</synph></p>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>handle</varname></pt>
          <pd>Handle of an image.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>maxCorners</varname></pt>
          <pd>Maximum prominent corners to detect. If value is less less than 1, all prominent corners will be detected.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>quality</varname></pt>
          <pd>Minimal accepted quality of detected corners.</pd>
          <pd>Type: <keyword keyref="scalar"/></pd>
        </plentry>
        <plentry>
          <pt><varname>minDistance</varname></pt>
          <pd>Minimum possible Euclidean distance between the detected corners.</pd>
          <pd>Type: <keyword keyref="scalar"/></pd>
        </plentry>
        <plentry>
          <pt><varname>mask</varname></pt>
          <pd>Optional handle of an 8-bit single channel image or a 2D matrix of natural numbers
              representing the region of interest where prominent corners are detected in <varname>R</varname>. If specified
              it must have the same dimensions as <varname>handle</varname>.</pd>
          <pd>Type: <keyword keyref="int_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>blockSize</varname></pt>
          <pd>Optional parameter specifying average block size for computation of
            a derivative covariation matrix in each pixel neighborhood. Defaults to 3.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>useHDetector</varname></pt>
          <pd>Optional value set to true if Harris detector should be used. Default value is
            <synph>false</synph>.</pd>
          <pd>Type: <keyword keyref="logical"/></pd>
        </plentry>
        <plentry>
          <pt><varname>freeParam</varname></pt>
          <pd>Optional parameter specifying the free parameter of the Harris detector. 
            Default value is 0.04.</pd>
          <pd>Type: <keyword keyref="scal"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Handle of the resulting gray scale image showing prominent corners.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
        <p>Compute prominent corners in an image with default parameters:
<codeblock>
handle = imreadcv('cv4.jpg');
maxCorners = 0;
qualityLevel = 0.01;
minDistance = 10;
R = goodfeaturestotrackcv(handle, maxCorners, qualityLevel, minDistance);</codeblock>
<fig>
    <title>Input image</title>
    <image placement="break"
        href="images/goodfeaturestotrackcv_fig1.png"/>
</fig>
<fig>
    <title>Output image</title>
    <image placement="break"
      href="images/goodfeaturestotrackcv_fig2.png"/>
</fig>
        </p>
    </section>
    <section>
      <draft-comment author="denby">ComputerVision commands are available only in the Business edition.</draft-comment>
    </section>
  </refbody>
</reference>
