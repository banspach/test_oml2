<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>distancetransformcv</title>
  <shortdesc>Calculates the distance to the closest zero pixel and normalizes the output image.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>ComputerVision Commands</category>
      <keywords>
        <indexterm>distancetransformcv</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>R = distancetransformcv(<var>handle</var>, <var>dtype</var>, <var>msize</var>...)</synph></p>
      <p><synph>R = distancetransformcv(<var>handle</var>, <var>dtype</var>, <var>msize</var>, <var>type</var>)</synph></p>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>handle</varname></pt>
          <pd>Handle of a single channel grayscale image.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>dtype</varname></pt>
          <pd>Distance type. Valid values are:
            <dl outputclass="table">
              <dlentry>
                <dt><option>-1</option></dt>
                <dd>cv::DIST_USER</dd>
              </dlentry>
              <dlentry>
                <dt><option>1</option></dt>
                <dd>cv::DIST_L1</dd>
              </dlentry>
              <dlentry>
                <dt><option>2</option></dt>
                <dd>cv::DIST_L2</dd>
              </dlentry>
              <dlentry>
                <dt><option>3</option></dt>
                <dd>cv::DIST_C</dd>
              </dlentry>
              <dlentry>
                <dt><option>4</option></dt>
                <dd>cv::DIST_L12</dd>
              </dlentry>
              <dlentry>
                <dt><option>5</option></dt>
                <dd>cv::DIST_FAIR</dd>
              </dlentry>
              <dlentry>
                <dt><option>6</option></dt>
                <dd>cv::WELSCH</dd>
              </dlentry>
              <dlentry>
                <dt><option>7</option></dt>
                <dd>cv::DIST_HUBER</dd>
              </dlentry>
            </dl></pd>          
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>msize</varname></pt>
          <pd>Size of the mask. Valid values are:
            <dl outputclass="table">
              <dlentry>
                <dt><option>3</option></dt>
                <dd>cv::DIST_MASK_3.</dd>
              </dlentry>
              <dlentry>
                <dt><option>5</option></dt>
                <dd>cv::DIST_MASK_5</dd>
              </dlentry>
              <dlentry>
                <dt><option>0</option></dt>
                <dd>cv::DIST_MASK_PRECISE</dd>
              </dlentry>
            </dl></pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>type</varname></pt>
          <pd>Optional parameter specifying the output type. Default value is 
            5. Valid values are:
            <dl outputclass="table">
              <dlentry>
                <dt><option>5</option></dt>
                <dd>CV_32F: Default value.</dd>
              </dlentry>
              <dlentry>
                <dt><option>0</option></dt>
                <dd>CV_8U: Output can be this type only if <varname>dtype</varname> has a value of
                    <synph>1</synph> (cv::DIST_L1)</dd>
              </dlentry>
            </dl>
          </pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Handle of the output image.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
        <p>Apply distance transformation on an image:
<codeblock>
handle = imreadcv('and1.jpeg', 0);
dtype = 1;
msize = 3;
type = 5;
R = distancetransformcv(handle, dtype, msize, type);</codeblock></p>
</section>
    <section>
      <draft-comment author="denby">ComputerVision commands are available only in the Business edition.
        See Also: imreadcv, thresholdcv, gaussianblurcv, adaptivethresholdcv</draft-comment>
    </section>
  </refbody>
</reference>
