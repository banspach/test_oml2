<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>applycolormapcv</title>
  <shortdesc>Converts an image to the specified color map.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>ComputerVision Commands</category>
      <keywords>
        <indexterm>applycolormapcv</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>R = applycolormapcv(<var>handle</var>, <var>colormap</var>)</synph></p>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>handle</varname></pt>
          <pd>Handle of a single channel gray scale image.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>colormap</varname></pt>
          <pd>Value of the color map. Valid values are:
            <dl outputclass="table">
              <dlentry>
                <dt><option>0</option></dt>
                <dd>cv::COLORMAP_AUTUMN</dd>
              </dlentry>
              <dlentry>
                <dt><option>1</option></dt>
                <dd>cv::COLORMAP_BONE</dd>
              </dlentry>
              <dlentry>
                <dt><option>2</option></dt>
                <dd>cv::COLORMAP_JET</dd>
              </dlentry>
              <dlentry>
                <dt><option>3</option></dt>
                <dd>cv::COLORMAP_WINTER</dd>
              </dlentry>
              <dlentry>
                <dt><option>4</option></dt>
                <dd>cv::COLORMAP_RAINBOW</dd>
              </dlentry>
              <dlentry>
                <dt><option>5</option></dt>
                <dd>cv::COLORMAP_OCEAN</dd>
              </dlentry>
              <dlentry>
                <dt><option>6</option></dt>
                <dd>cv::COLORMAP_SUMMER</dd>
              </dlentry>
              <dlentry>
                <dt><option>7</option></dt>
                <dd>cv::COLORMAP_SPRING</dd>
              </dlentry>
              <dlentry>
                <dt><option>8</option></dt>
                <dd>cv::COLORMAP_COOL</dd>
              </dlentry>
              <dlentry>
                <dt><option>9</option></dt>
                <dd>cv::COLORMAP_HSV</dd>
              </dlentry>
              <dlentry>
                <dt><option>10</option></dt>
                <dd>cv::COLORMAP_PINK</dd>
              </dlentry>
              <dlentry>
                <dt><option>11</option></dt>
                <dd>cv::COLORMAP_HOT</dd>
              </dlentry>
              <dlentry>
                <dt><option>12</option></dt>
                <dd>cv::COLORMAP_PARULA</dd>
              </dlentry>
            </dl></pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Handle of the output image.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
        <p>Apply the specified color map:
<codeblock>
handle = imreadcv('bird2.jpg', 0);
R = applycolormapcv(handle, 7);</codeblock>
            <fig>
                <title>Input image</title>
                <image placement="break"
                  href="images/applycolormapcv_fig1.png"
                  id="image_qhv_b4t_knb"/>
            </fig>
            <fig>
                <title>Output image</title>
                <image placement="break"
                href="images/applycolormapcv_fig2.png"
                id="image_qhv_b4t_knb2"/>
            </fig>
        </p>
    </section>
    <section>
      <draft-comment author="denby">ComputerVision commands are available only in the Business edition.
        See Also: imreadcv</draft-comment>
    </section>
  </refbody>
</reference>
