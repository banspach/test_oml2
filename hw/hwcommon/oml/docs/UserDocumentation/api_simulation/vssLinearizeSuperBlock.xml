<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>vssLinearizeSuperBlock</title>
  <shortdesc>Perform linearization of Super Block.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Blocks Simulation</category>
      <keywords>
        <indexterm>vssLinearizeSuperBlock</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
	  <p><synph>[A,B,C,D] = vssLinearizeSuperBlock<var>(mdlfilename,blkname,inputs,outputs)</var></synph></p>
	  <p><synph>[A,B,C,D] = vssLinearizeSuperBlock<var>(mdlfilename,blkname,inputs,outputs,tf)</var></synph></p>
	  <p><synph>[A,B,C,D] = vssLinearizeSuperBlock<var>(mdlfilename,blkname,inputs,outputs,tf,ctx)</var></synph></p><?STOP?></section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>mdlfilename</varname></pt>
          <pd>Filename of the model or the model handler.</pd>
          <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
        </plentry>
		<plentry>
          <pt><varname>blkname</varname></pt>
          <pd>Fullname of the superblock to be linearized.</pd>
          <pd>Type: <keyword keyref="char"/></pd>
          <pd>Dimension: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>inputs</varname></pt>
          <pd>Inputs to be used for the linearization, with indices greater than 1.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>outputs</varname></pt>
          <pd>Outputs to be used for the linearization, with indices greater than 1.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
		<plentry>
          <pt><varname>tf</varname></pt>
          <pd>Optional argument with the time to linearize around. Default value: 0.</pd>
          <pd>Type: <keyword keyref="dou_int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
		<plentry>
          <pt><varname>ctx</varname></pt>
          <pd>A structure that defines the external context. Default value is an empty struct.</pd>
          <pd>Type: context</pd>
          <pd>Dimension: <keyword keyref="struct"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>A</synph></pt>
          <pd>System matrix.</pd>
		  <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="mat"/></pd>
        </plentry>
		<plentry>
          <pt><synph>B</synph></pt>
          <pd>Control matrix.</pd>
		  <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="mat"/></pd>
        </plentry>
		<plentry>
          <pt><synph>C</synph></pt>
          <pd>Output matrix.</pd>
		  <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="mat"/></pd>
        </plentry>
		<plentry>
          <pt><synph>D</synph></pt>
          <pd>Free-forward matrix.</pd>
		  <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="mat"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <p>Get q variable from the base. If not found, value 0 is used.</p>
      <codeblock>inps = 1; % vector of input port indices considered for linearization
outs = [1,2]; % vector of output port indices considered for linearization
% Modified context
ctx=struct; 
ctx.z0 = 0;
ctx.th0 = 0;
ctx.phi = 0;
model=bdeGetCurrentModel;
superblock = 'pendulum'; % Selected Super block to linearize
[A,B,C,D] = vssLinearizeSuperBlock(model,superblock,inps,outs,0,ctx);
</codeblock>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: vssRunSimulation</draft-comment>
    </section>
  </refbody>
</reference>
