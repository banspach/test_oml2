﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>warndlg</title>
  <shortdesc>Shows a dialog box with label(s), <varname>s</varname> for text 
    field(s) that require user input.</shortdesc>
  <prolog>
    <metadata>
        <category><keyword keyref="product"/> Language</category>
        <category>Gui Commands</category>
        <keywords>
            <indexterm>inputdlg</indexterm>
        </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>R = inputdlg<var>(s)</var></synph></p>
      <p><synph>R = inputdlg<var>(s, c)</var></synph></p>
      <p><synph>R = inputdlg<var>(s, c, dims)</var></synph></p>
      <p><synph>R = inputdlg<var>(s, c, dims, values)</var></synph></p>        
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>s</varname></pt>
          <pd>Label(s) for text field(s) that require user input.</pd>
          <pd>Type: <keyword keyref="string_cell"/></pd>
        </plentry>
        <plentry>
          <pt><varname>c</varname></pt>
          <pd>Optional argument specifying the caption to be displayed for the
            dialog. If no caption is specified, default caption is 'Input Dialog'.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>dims</varname></pt>
          <pd>Optional argument specifying dimensions for text field(s). If no
              value is given, each text field will span one row. An integer value 
              specifies the number of rows the text fields will span over. 
              A vector of integers specifies the number of rows each text field 
              will span over. A matrix specifies the dimensions of each text 
              field; the first column gives the number of rows the text field 
              will span and the second column gives a hint for the number of 
              columns the text field will span. All text fields will however 
              have the same width and will span the largest number of columns in
              the dialog.</pd>
           <pd>Type: <keyword keyref="int_vec_mat"/></pd>
        </plentry>
        <plentry>
              <pt><varname>values</varname></pt>
              <pd>Optional argument specifying default values to display in the
                  text field(s). The type should be a cell array of strings.</pd>
              <pd>Type: <keyword keyref="cell"/></pd>
        </plentry>          
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
            <pd>The result is a cell array of strings. If the dialog is dismissed 
              by clicking the 'OK', button or pressing 'Enter' in any text field, 
              <varname>R</varname> will contain the string(s) entered by the user
              in the text field(s). If the dialog is closed or dismissed using 
              the 'Cancel' button, <varname>R</varname> will be a cell array of 
              empty strings, with one entry per text field.</pd>
          <pd>Type: <keyword keyref="cell"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
        <p>String input:
          <codeblock>R = inputdlg('Name')

R =
{
[1,1] User name
}
The dialog displayed has one text field with the label 'Name' and caption
'Input Dialog'.</codeblock></p>
        <p>Cell input with caption and scalar dimension:
          <codeblock>R = inputdlg({'Name', 'Address'}, 'Information', 3)

R =
{
[1,1] User name
[2,1] User address
}
The dialog displayed has two text fields with the label 'Name' and 'Address',
spanning three rows. The caption displayed is 'Information'.</codeblock></p>
        <p>Cell input with caption and vector dimension
          <codeblock>R = inputdlg({'Name', 'Address'}, 'Information', [2, 4])

R =
{
[1,1] User name
[2,1] User address
}
The dialog displayed has two text fields with the label 'Name' and 'Address'.
The 'Name' text field spans two rows and the 'Address' text field spans four
rows. The caption displayed is 'Information'.</codeblock></p>
        <p>Cell input with caption and matrix dimension
            <codeblock>R = inputdlg({'Name', 'Address'}, 'Information', [1, 2; 3, 4])

R =
{
[1,1] User name
[2,1] User address
}
The dialog displayed has two text fields with the label 'Name' and 'Address'.
The 'Name' text field spans one row and the 'Address' text field spans three rows.
Both text fields will span four columns, as this is the widest width. The caption 
displayed is 'Information'.</codeblock></p>
        <p>Cell input with caption, scalar dimension and default values:
            <codeblock>R = inputdlg({'Name', 'Address'}, 'Information', 3, {'Username', 'Useraddress'})

R =
{
[1,1] Username
[2,1] Useraddress
}
The dialog displayed has two text fields with the label 'Name' and 'Address'.
Both text fields span three rows and one column. The text fields 'Name' and 'Address'
display default values of 'Username' and 'Useraddress' respectively. The caption
displayed is 'Information'.</codeblock></p>
    </section>        
    <section>
      <title>Version History</title>
      <p><keyword keyref="version"/></p>
      <draft-comment author="denby">See Also: errordlg, msgbox, questdlg, warndlg</draft-comment>
    </section>
  </refbody>
</reference>        
    