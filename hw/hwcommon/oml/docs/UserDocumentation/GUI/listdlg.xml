﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>warndlg</title>
  <shortdesc>Shows a dialog box with a list of strings which the user can pick.
    This command is only available in the GUI.</shortdesc>
  <prolog>
    <metadata>
        <category><keyword keyref="product"/> Language</category>
        <category>Gui Commands</category>
        <keywords>
            <indexterm>listdlg</indexterm>
        </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>[R, status] = listdlg<var>('ListString', data...)</var></synph></p>
      <p><synph>[R, status] = listdlg<var>('ListString', data, 'ListSize', size, 
        'SelectionMode', mode, 'Name', caption, 'InitialValue', value, 
        'PromptString', prompt, 'OkString', ok, 'CancelString', cancel)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>data</varname></pt>
          <pd>Value for the key <synph>'ListString'</synph>, giving the list of strings 
            which is displayed in the list box for the user to select. 
            <varname>data</varname> can either be a string or a cell array of strings.</pd>
          <pd>Type: <keyword keyref="string_cell"/></pd>
        </plentry>
        <plentry>
          <pt><varname>size</varname></pt>
          <pd>Optional value for the key <synph>'ListSize'</synph>, specifying the minimum size
            of the list box. <varname>size</varname> needs to be a 2-element vector
            of integers.</pd>
          <pd>Type: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>mode</varname></pt>
          <pd>Optional value for the key <synph>'SelectionMode'</synph>argument specifying 
            whether the selection type is <synph>'single'</synph> or 
            <synph>'multiple'</synph>. By default, the selection is mode is 
             <synph>'multiple</synph>.</pd>
           <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>caption</varname></pt>
          <pd>Optional value for the key <synph>'Name'</synph>argument specifying
          the caption of the list box.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>value</varname></pt>
          <pd>Optional value for the key <synph>'InitialValue'</synph>argument 
            specifying the entries in the list box which are preselected. The 
            value will be a one-based vector of indicies corresponding to the 
            selected entries in <varname>data</varname>. By default, the first
            entry is selected.</pd>
            <pd>Type: <keyword keyref="vec"/></pd>
          </plentry>
          <plentry>
            <pt><varname>prompt</varname></pt>
            <pd>Optional value for the key <synph>'PromptString'</synph>argument
              specifying the label(s) to be displayed above the list box.
              <varname>prompt</varname> can be a string or a cell array of strings.</pd>
            <pd>Type: <keyword keyref="string_cell"/></pd></plentry>
          <plentry>
            <pt><varname>ok</varname></pt>
            <pd>Optional value for the key <synph>'OkString'</synph>argument specifying
              the caption of the 'OK' button, which accepts the user's selection in the
              dialog box. The default caption for the button is 'OK'.</pd>
            <pd>Type: <keyword keyref="string"/></pd>
          </plentry>
          <plentry>
            <pt><varname>cancel</varname></pt>
            <pd>Optional value for the key <synph>'CancelString'</synph>argument specifying
                the caption of the 'Cancel' button, which discards the user's selection 
                in the dialog box. The default caption for the button is 'Cancel'.</pd>
            <pd>Type: <keyword keyref="string"/></pd>
          </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
            <pd>The result is matrix of one-based indicies of the user selected entries
              of the dialog box.</pd>
          <pd>Type: <keyword keyref="int_mat"/></pd>
        </plentry>
        <plentry>
          <pt><synph>status</synph></pt>
          <pd><varname>status</varname> specifies how the list dialog is dismissed by 
            the user. The value is 1 if the user clicked on <synph>OK</synph> button
            and 0 if the user dismissed the dialog box by clicking on <synph>Cancel</synph>
            button.</pd>
            <pd>Type: <keyword keyref="int"/></pd>
          </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
        <p>List dialog dismissed by the user pressing the 'OK' button:
<codeblock>
[R, status] = listdlg('ListString', {'String1', 'String2', 'String3'})

R = [Matrix] 1 x 2
2  3
status = 1</codeblock></p>
        <p>List dialog with caption, preselected values, promt and
            custom strings for ok/cancel buttons
<codeblock>
[R, status] = listdlg('ListString', {'Name', 'Address', 'Other'}, 'Name', 'Field Value',
              'InitialValue', [1, 2], 'PromptString', 'Values:', 'OkString', 'Select',
              'CancelString', 'Clear')

R = 1
status = 1</codeblock></p>
        <p>List dialog with single selection mode and the user dismissing dialog
        with the cancel button
<codeblock>
[R, status] = listdlg('ListString', {'Name', 'Address'}, 'SelectionMode', 'single')

R = [Matrix] 0 x 0
status = 0</codeblock></p>
    </section>        
    <section>
      <title>Version History</title>
      <p><keyword keyref="version"/></p>
      <draft-comment author="denby">See Also: inputdlg, errordlg, msgbox, questdlg, warndlg</draft-comment>
    </section>
  </refbody>
</reference>        
    