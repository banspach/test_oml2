<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>svcfit</title>
  <shortdesc>C-Support Vector Classification.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Machine Learning</category>
      <keywords>
        <indexterm>svcfit</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>parameters = svcfit<var>(X,y)</var></synph></p>
      <p><synph>parameters = svcfit<var>(X,y,options)</var></synph></p><?STOP?></section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>X</varname></pt>
          <pd>Training data.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>y</varname></pt>
          <pd>Target values.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec_mat"/></pd>
        </plentry>
		<plentry>
          <pt><varname>options</varname></pt>
          <pd>Type: <keyword keyref="struct"/></pd>
          <pd>
            <dl>
              <dlentry>
                <dt><synph>C</synph></dt>
                <dd>Penalty parameter C of the error term (default: 1).</dd>
                <dd>Type: <keyword keyref="dou"/></dd>
				<dd>Dimension: <keyword keyref="scal"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>kernel</synph></dt>
                <dd>Kernel type to be used in the algorithm. Allowed values are 'linear', 'poly', 'rbf' (default), 'sigmoid'.</dd>
                <dd>Type: <keyword keyref="char"/></dd>
				<dd>Dimension: <keyword keyref="string"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>degree</synph></dt>
                <dd>Degree of the polynomial kernel function ('poly'). This parameter is ignored by other kernels. Default: 3</dd>
                <dd>Type: <keyword keyref="int"/></dd>
				<dd>Dimension: <keyword keyref="scal"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>gamma</synph></dt>
                <dd>Kernel coefficient for 'rbf', 'poly' and 'sigmoid' kernel. Allowed values are any float. If gamma is not assigned, then 1 / (n_features * variance(X)) is taken as gamma.</dd>
                <dd>Type: <keyword keyref="dou"/></dd>
				<dd>Dimension: <keyword keyref="scal"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>coef0</synph></dt>
                <dd>Independent term in kernel function (default: 0). It is only significant in 'poly' and 'sigmoid'.</dd>
                <dd>Type: <keyword keyref="dou_int"/></dd>
				<dd>Dimension: <keyword keyref="scal"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>shrinking</synph></dt>
                <dd>Whether to use the shrinking heuristic.</dd>
                <dd>Type: <keyword keyref="bool"/></dd>
				<dd>Dimension: <keyword keyref="dou_int"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>tol</synph></dt>
                <dd>Tolerance of stopping criterion (default: 1e-3).</dd>
                <dd>Type: <keyword keyref="dou"/></dd>
				<dd>Dimension: <keyword keyref="scal"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>cache_size</synph></dt>
                <dd>Specify the size of kernel cache (in MB).</dd>
                <dd>Type: <keyword keyref="dou"/></dd>
				<dd>Dimension: <keyword keyref="scal"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>max_iter</synph></dt>
                <dd>Hard limit on iterations within solver (default: -1).</dd>
                <dd>Type: <keyword keyref="int"/></dd>
				<dd>Dimension: <keyword keyref="scal"/></dd>
              </dlentry>
            </dl>
          </pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>parameters</synph></pt>
          <pd>Contains all the values passed to svcfit method as options. Additionally it has below key-value pairs.</pd>
		  <pd>Type: <keyword keyref="struct"/></pd>
		  <pd>
            <dl>
              <dlentry>
                <dt><synph>scorer</synph></dt>
                <dd>Function handle pointing to 'accuracy' function.</dd>
                <dd>Type: function handle</dd>
              </dlentry>
			  <dlentry>
                <dt><synph>support</synph></dt>
                <dd>Indices of support vectors.</dd>
                <dd>Type: <keyword keyref="int"/></dd>
				<dd>Dimension: <keyword keyref="vec"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>support_vectors</synph></dt>
                <dd>Support vectors.</dd>
                <dd>Type: <keyword keyref="dou"/></dd>
				<dd>Dimension: <keyword keyref="mat"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>dual_coef</synph></dt>
                <dd>Coefficients of the support vector in the decision function.</dd>
                <dd>Type: <keyword keyref="int"/></dd>
				<dd>Dimension: <keyword keyref="mat"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>intercept</synph></dt>
                <dd>Constants in decision function.</dd>
                <dd>Type: <keyword keyref="dou"/></dd>
				<dd>Dimension: <keyword keyref="mat"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>coef</synph></dt>
                <dd>Weights assigned to features (coefficients in primal problem). This is available only if kernel is set to 'linear'. It is derived from dual_coef and support_vectors.</dd>
                <dd>Type: <keyword keyref="dou"/></dd>
				<dd>Dimension: <keyword keyref="vec"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>n_samples</synph></dt>
                <dd>Number of rows in the training data.</dd>
                <dd>Type: <keyword keyref="int"/></dd>
				<dd>Dimension: <keyword keyref="scal"/></dd>
              </dlentry>
			  <dlentry>
                <dt><synph>n_features</synph></dt>
                <dd>Number of columns in the training data.</dd>
                <dd>Type: <keyword keyref="int"/></dd>
				<dd>Dimension: <keyword keyref="scal"/></dd>
              </dlentry>
            </dl>
          </pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <p>Usage of <cmdname>svcfit</cmdname></p>
      <codeblock>data = dlmread('digits.csv', ',');
X = data(:, 1:end-1);
y = data(:, end);

parameters = svcfit(X, y)</codeblock>
      <codeblock>parameters = struct [
  C: 1
  coef: [Matrix] 45 x 64
  ...
</codeblock>
	  
    </section>
	<section>
      <title>Comments</title>
      <p>The implementation is based on libsvm. The fit time complexity is more than quadratic with number of samples. This makes it hard to scale to dataset with more than a couple of 10000 samples. Multiclass support is handled by one-vs-one scheme. 
	Output 'parameters' should be passed as input to svcpredict function</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: svcpredict</draft-comment>
    </section>
  </refbody>
</reference>
