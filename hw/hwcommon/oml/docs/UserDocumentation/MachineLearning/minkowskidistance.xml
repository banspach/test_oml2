<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>minkowskidistance</title>
  <shortdesc>It computes Lp Norm between two vectors or matrices of same length. If one of the inputs has one row and the other has 'm' rows, then distance is computed between one row and every other row.
	If p = 1, it becomes Manhattan distance. If p = 2, it becomes Euclidean distance. If p = n, then it becomes Ln Norm. It is a generalized formula for computing distances based on p value.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Machine Learning</category>
      <keywords>
        <indexterm>minkowskidistance</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>Distance = minkowskidistance<var>(X,Y,p)</var></synph></p><?STOP?></section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>X</varname></pt>
          <pd>First input vector or matrix.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>Y</varname></pt>
          <pd>Second input vector or matrix.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec_mat"/></pd>
        </plentry>
		<plentry>
          <pt><varname>p</varname></pt>
          <pd>Represents the Lp norm (default: 2).</pd>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>Distance</synph></pt>
          <pd>The distance between two inputs.</pd>
		  <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec_mat"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <p>Example of <cmdname>minkowskidistance</cmdname></p>
      <codeblock>x = [1, 2, 3];
y = [4, 5, 6];
%%When p = 1, it becomes L1 Norm or Manhattan Distance
printf('%f %f \n', minkowskidistance(x, y, 1), manhattanDistance(x, y));
</codeblock>
      <codeblock>9.000000 9.000000</codeblock>
    </section>
    <section>
      <draft-comment author="denby">See Also: cosinedistance, cosinesimilarity, euclideandistance, manhattandistance, naneuclideandistance, squaredeuclideandistance</draft-comment>
    </section>
  </refbody>
</reference>
