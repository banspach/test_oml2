<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>textscan</title>
  <shortdesc>Returns result <synph>R</synph> after reading a formatted data from
      a file stream \, <varname>f</varname>, or string, <varname>s</varname>.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>File I/O Commands</category>
      <keywords>
        <indexterm>textscan</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>R = textscan<var>(f, fmt)</var></synph></p>
      <p><synph>R = textscan<var>(f, fmt, n)</var></synph></p>
      <p><synph>R = textscan<var>(f, fmt, param, value, ...)</var></synph></p>
      <p><synph>R = textscan<var>(f, fmt, n, param, value, ...)</var></synph></p>
      <p><synph>R = textscan<var>(s, ...)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>f</varname></pt>
          <pd>An open file stream which needs to be read.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>s</varname></pt>
          <pd>Input stream which needs to be read.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>fmt</varname></pt>
          <pd>Format to be applied to the input file or string. Valid formats are:<ul
              id="ul_zqr_vlw_2gb">
              <li>'%d'</li>
              <li>'%f'</li>
              <li>'%g'</li>
              <li>'%c'</li>
              <li>%n</li>
              <li>'%s'</li>
            </ul>If no <varname>fmt</varname> is specified, '%f' or the float format is used as a
            default. The format %n is similar to %d, where a value is read as an integer.</pd>
          <pd>The format specifications %c, %d, %f and %n can be an optional integer width
            specification. </pd>
          <pd>For %c, the width specification gives the number of characters to read.</pd>
          <pd>%f formats can also be specified as %a.bf, where a and b are integers, representing
            numbers to be read before and after the decimal point, respectively.</pd>
          <pd>To include certain characters, %[...] can be used. To exclude certain characters,
            %[^...] can be used.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>n</varname> (optional)</pt>
          <pd>When reading from a file, <varname>f</varname>, <varname>n</varname> specifies the
            number of lines to be read. When reading from a string, <varname>s</varname> specifies
            the number of times the format <varname>fmt</varname> is repeated. If
              <varname>n</varname> is 0, nothing is read. If <varname>n</varname> is -1, which is
            the default, data is read until the format <varname>fmt</varname> is applicable to the
            input.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>param-value</varname> (optional)</pt>
            <pd>Optional name-value pairs which specify the behavior of <cmdname>textscan</cmdname>.
            The following are the valid parameter names and their values:<dl>
              <dlentry>
                <dt><synph>headerlines</synph>: <varname>linestoskip</varname></dt>
                <dd>The number of lines to skip before applying the format.</dd>
                <dd>Type: Finite, positive <keyword keyref="int"/></dd>
              </dlentry>
              <dlentry>
                <dt><synph>delimter</synph>:<varname>delim</varname></dt>
                <dd>The delimiter to be used. If nothing is specified, whitespace is used a
                  delimiter.</dd>
                <dd>Type: <keyword keyref="string"/></dd>
              </dlentry>
              <dlentry>
                <dt><synph>endofline</synph>:<varname>eol</varname></dt>
                <dd>The character to be used for end of line. If nothing is specified, '\r\n' will
                  be used. Valid values are '\r', '\r\n', or a single character.</dd>
                <dd>Type: <keyword keyref="string"/></dd>
              </dlentry>
              <dlentry>
                <dt><synph>returnOnError</synph>:<varname>val</varname></dt>
                <dd>If <systemoutput>true</systemoutput> or 1, the output contains all data until
                  the format cannot be applied or there is no more data to be read. This is the
                  default option. If <systemoutput>false</systemoutput> or 0, an error is returned
                  if the format cannot be applied to the data.</dd>
                <dd>Type: <keyword keyref="log"/></dd>
              </dlentry>
              <dlentry>
               <dt><synph>whitespace</synph>:<varname>space</varname></dt>
               <dd>The character to be used as whitespace. If nothing is specified, ' '
                 will be used.</dd>
               <dd>Type: <keyword keyref="string"/></dd></dlentry>
            </dl></pd>
        </plentry>        
       </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>A new column is created in the output, <synph>R</synph>, for every
            format specified in <varname>t</varname></pd>
          <pd>Type: <keyword keyref="cell"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
        <p>Reads formatted data from a file, with headerlines and delimiters. The contents of
          '<filepath>textscanfile.txt</filepath>' are: ## A ID = 02476 ## YKZ Timestamp Temp
        Humidity Wind Weather 06-Sep-2013 01:00:00 6.6 89 4 clear 06-Sep-2013 09:00:00 15.6 51 5
        mainly clear 06-Sep-2013 17:00:00 22.4 41 9 mostly cloudy ## B ID = 02477 ## YVR Timestamp
        Temp Humidity Wind Weather 09-Sep-2013 01:00:00 15.2 91 8 clear 09-Sep-2013 05:00:00 19.1 94
        7 n/a ## C ID = 02478 ## YYZ Timestamp Temp Humidity Wind Weather
        <codeblock>f = fopen('textscanfile.txt');
t = '%s %f %f %f %s';
R1 = textscan(f, t, 'headerlines',2,'delimiter', '\t')
R2 = textscan(f, t, 'headerlines',1,'delimiter', '\t')
fclose(fid);</codeblock><codeblock>R1 =
{
{
[1,1]
{
[1,1] 06-Sep-2013 01:00:00
}
{
[2,1] 06-Sep-2013 09:00:00
}
{
[3,1] 06-Sep-2013 17:00:00
}
{
[4,1] ## B
}
}
{
[1,2] [Matrix] 3 x 1
6.60000
15.60000
22.40000
}
{
[1,3] [Matrix] 3 x 1
89
51
41
}
{
[1,4] [Matrix] 3 x 1
4
5
9
}
[1,5]
{
[1,1] clear
}
{
[2,1] mainly clear
}
{
[3,1] mostly cloudy
}
}
R2 =
{
{
[1,1]
{
[1,1] 09-Sep-2013 01:00:00
}
{
[2,1] 09-Sep-2013 05:00:00
}
{
[3,1] ## C
}
}
{
[1,2] [Matrix] 2 x 1
15.20000
19.10000
}
{
[1,3] [Matrix] 2 x 1
91
94
}
{
[1,4] [Matrix] 2 x 1
8
7
}
{
[1,5]
{
[1,1] clear
}
{
[2,1] n/a
}
}
}</codeblock></p>
          <p>Reads formatted data from a string, using repeat parameter.
        <codeblock>str='one two 12 three four 24.7e12 five six 56';
textscan(str, '%s %s %f', 2)   % Apply format twice
textscan(str, '%s %s %f', -1)  % Read to the end of the string
              
ans =
{
[1,1]
{
[1,1] one
[2,1] three
}
[1,2]
{
[1,1] two
[2,1] four
}
[1,3] [Matrix] 2 x 1
1.20000e+01
2.47000e+13
}
ans =
{
[1,1]
{
[1,1] one
[2,1] three
[3,1] five
}
[1,2]
{
[1,1] two
[2,1] four
[3,1] six
}
[1,3] [Matrix] 3 x 1
1.20000e+01
2.47000e+13
5.60000e+01
}</codeblock></p>
        <p>Reads formatted data from a file, skipping new
        lines.<codeblock>str='12.345678/1/2018 hello world!!';
textscan(str, '%2.3f/%d/%2n %2c %s')</codeblock><codeblock>ans =
{
[1,1] 12.346
[1,2] 1
[1,3] 20
[1,4]
{
[1,1] 18
}
[1,5]
{
[1,1] hello
}
}</codeblock></p>
      <p>Reads formatted data from a string, using %[..] to include
        characters.<codeblock>f = textscan('one threes four','%s %[thre]')</codeblock><codeblock>f =
{
  [1,1]
  {
    [1,1] one
    [2,1] s
    [3,1] fou
  }
  [1,2]
  {
    [1,1] three
    [2,1] r
  }
}</codeblock></p>
      <p>Reads formatted data from a string, using %[..] to exclude characters.
        <codeblock>f = textscan('one threes four','%s %[^thre]')</codeblock><codeblock>f = 
{
  [1,1] 
  {
    [1,1] one
    [2,1] three
    [3,1] r
  }
  [1,2] 
  {
    [1,1] s
    [2,1] fou
  }
}</codeblock></p>
    
</section>
    <section>
      <draft-comment author="denby">See Also: textread</draft-comment>
    </section>
  </refbody>
</reference>