<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>nargin</title>
  <shortdesc>Returns the number of arguments required by a function, or the number of arguments
    received by a function, as detailed below.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <keywords>
        <indexterm>nargin</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
	  <p><synph>R = nargin<var>('functionname')</var></synph></p>
	  <p><synph>R = nargin<var>(@functionname)</var></synph></p>
      <p><synph>R = nargin<var>()</var></synph></p>
	  
	  <?STOP?></section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>'functionname'</varname></pt>
          <pd>Text of the function name.</pd>
          <pd>Type: <keyword keyref="char_string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>@functionname</varname></pt>
          <pd>A handle to the function name.</pd>
          <pd>Type: <keyword keyref="char_string"/></pd>
        </plentry>
      </parml>
      <?STOPINP?></section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>The number of arguments required (Case 1) or provided (Case 2).<dl outputclass="table">
              <dlentry>
                <dt>Case 1</dt>
                <dd>When <cmdname>nargin</cmdname> is called with a function handle or function
                  name, it returns the number of arguments required by the function. 0 means the
                  function accepts no arguments. A positive number indicates a required, minimum
                  argument count. A negative number indicates the first optional argument.</dd>
              </dlentry>
              <dlentry>
                <dt>Case 2</dt>
                <dd>When <cmdname>nargin</cmdname> (with no arguments) is called inside a function,
                  it returns a positive number indicating the number of arguments received by the
                  function.</dd>
              </dlentry>
            </dl></pd>
          <pd>Type: <keyword keyref="int"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
	
    <section>
      <title>Example</title>
      <codeblock>printf('nargin count for function ''cos'' is %d\n', nargin(@cos))</codeblock>
      <codeblock>nargin count for function 'cos' is 1</codeblock>
	  <codeblock>printf('nargin count for function ''clear'' is %d\n', nargin('clear'))</codeblock>
      <codeblock>nargin count for function 'clear' is -1</codeblock>
	  <codeblock>printf('nargin count for function ''sprintf'' is %d\n', nargin('sprintf'))</codeblock>
      <codeblock>nargin count for function 'sprintf' is -2</codeblock>

	  <codeblock>function testfunc(firstarg, secondarg, thirdarg)
	    printf('arguments received %d\n', nargin())
      end

      printf('function %s takes %d arguments\n', 'testfunc', nargin('testfunc'))</codeblock>
      <codeblock>function testfunc takes 3 arguments</codeblock>

	  <codeblock>printf('function %s takes %d arguments\n', 'testfunc', nargin(@testfunc))</codeblock>
      <codeblock>function testfunc takes 3 arguments</codeblock>

	  <codeblock>printf('call testfunc with 0 arguments - ')
      testfunc()</codeblock>
      <codeblock>call testfunc with 0 arguments - arguments received 0</codeblock>

	  <codeblock>printf('call testfunc with 1 arguments - ')
      testfunc(1)</codeblock>
      <codeblock>call testfunc with 1 arguments - arguments received 1</codeblock>

	  <codeblock>printf('call testfunc with 2 arguments - ')
      testfunc(1, 2)</codeblock>
      <codeblock>call testfunc with 2 arguments - arguments received 2</codeblock>
	  
	  <codeblock>printf('call testfunc with 3 arguments - ')
      testfunc(1, 2, 3)</codeblock>
      <codeblock>call testfunc with 3 arguments - arguments received 3</codeblock>
	  </section>
	
    <section>
      <draft-comment author="denby">See Also: </draft-comment>
    </section>
  </refbody>
</reference>
