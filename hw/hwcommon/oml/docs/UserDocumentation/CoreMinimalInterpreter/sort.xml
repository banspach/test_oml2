<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>sort</title>
  <shortdesc>Sorts elements of <varname>x</varname> into ascending order by default.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Core Minimal Interpreter Commands</category>
      <keywords>
        <indexterm>sort</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>s = sort<var>(x)</var></synph></p>
      <p><synph>s = sort<var>(x)</var></synph></p>
      <p><synph>s = sort<var>(x, dim)</var></synph></p>
      <p><synph>s = sort<var>(x, 'ascend')</var></synph></p>
      <p><synph>s = sort<var>(x, 'descend')</var></synph></p>
      <p><synph>s = sort<var>(x, dim, 'ascend')</var></synph></p>
      <p><synph>s = sort<var>(x, dim, 'descend')</var></synph></p>
      <p><synph>[s, idx] = sort<var>(...)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>x</varname></pt>
          <pd>A matrix, an array of char, or a cell.</pd>
          <pd>Type: <keyword keyref="dou_int_char_string_log_struct_cell"/></pd>
          <pd>Dimension: <keyword keyref="scal_string_vec_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>dim</varname></pt>
          <pd>The dimension on which to sort</pd>
          <pd>Default: first non-singleton dimension.</pd>
          <pd>Type: <keyword keyref="int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>s</synph></pt>
          <pd>Sorted elements.</pd>
        </plentry>
        <plentry>
          <pt><synph>idx</synph></pt>
          <pd>Index vector such that <synph>s = x<var>(idx)</var>.</synph>
          </pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section id="section_wy3_jcf_3db">
      <title>Examples</title>
      <p>One output with default dimension and direction input:</p>
      <codeblock>R = sort([4,3,2,1])</codeblock>
      <codeblock><systemoutput>R = [Matrix] 1 x 4
1 2 3 4</systemoutput></codeblock>
      <p>Two outputs with default dimension and direction:</p>
      <codeblock>[s,idx]=sort([4,3,2,1])</codeblock>
      <codeblock><systemoutput>s = [ 1 2 3 4 ]
idx = [ 4 3 2 1 ]</systemoutput></codeblock>
      <p>Two outputs with <option>ascend</option> specified:</p>
      <codeblock>[s,idx]=sort([1,2,3,4],'ascend')</codeblock>
      <codeblock><systemoutput>s = [ 1 2 3 4 ]
idx = [ 1 2 3 4 ]</systemoutput></codeblock>
      <p>Two outputs with <varname>dim</varname> and <option>descend</option> specified:</p>
      <codeblock>[s,idx]=sort([4,3,2,1;3,6,1,2;2,2,2,1],2,'descend')</codeblock>
      <codeblock><systemoutput>s = [ 4 3 2 1 ; 6 3 2 1 ; 2 2 2 1 ]
idx = [ 1 2 3 4 ; 2 1 4 3 ; 1 2 3 4 ]</systemoutput></codeblock>
    </section>
    <section>
      <title>Comments</title>
      <p>The default sort direction is <option>ascend</option>.</p>
      <p><cmdname>NaN</cmdname> values are sorted to the high end of the list, as if they are
        greater than all other values, including <cmdname>inf</cmdname>.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: sortrows, issorted (not sortrows 7/5/15) </draft-comment>
    </section>
  </refbody>
</reference>
