<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>imagesc</title>
  <shortdesc>Display a matrix as an image with scaled colors.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Image Commands</category>
      <keywords>
        <indexterm>imagesc</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>h = imagesc<var>(z)</var></synph></p>
      <p><synph>h = imagesc<var>(x, y, z)</var></synph></p>
      <p><synph>h = imagesc<var>(..., [cmin cmax])</var></synph></p>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
          <plentry>
            <pt><varname>z</varname></pt>
            <pd>Matrix specifying the data for the matrix to be displayed.</pd>
            <pd>Type: <keyword keyref="vec_mat"/></pd>
          </plentry>
          <plentry>
              <pt><varname>x</varname></pt>
              <pd>Optional vector to specify the minimum (first element of the vector) and maximum (last element of the vector) values of the x-axis. These values may be used to move or scale the image on the x-axis.</pd>
              <pd>Type: <keyword keyref="vec_mat"/></pd>
          </plentry>
          <plentry>
              <pt><varname>y</varname></pt>
              <pd>Optional vector to specify the minimum (first element of the vector) and maximum (last element of the vector) values of the y-axis. These values may be used to move or scale the image on the y-axis.</pd>
              <pd>Type: <keyword keyref="vec_mat"/></pd>
          </plentry>
        <plentry>
          <pt><varname>cmin, cmax</varname></pt>
          <pd>Range of the colormap.</pd>
          <pd>Type: <keyword keyref="dou_int"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>h</synph></pt>
          <pd>Handle of the <cmdname>imagesc</cmdname> graphics object.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>Simple <cmdname>imagesc</cmdname> example:</p>
      <codeblock>cla;
z = [1 2 3; 4 5 6; 7 8 9];
imagesc(z)
colorbar</codeblock>
      <fig product="act_pro activate_basic compose_basic compose_pro">
        <title>Imagesc simple example</title>
        <image href="images/imagescExample.png"/>
      </fig>
      <p>Define the range of the colormap:</p>
      <codeblock>cla;
z = [1 2 3; 4 5 6; 7 8 9];
imagesc(z, [3 7]);
colorbar</codeblock>
      <fig product="act_pro activate_basic compose_basic compose_pro">
        <title>Colormap range set.</title>
        <image href="images/imagescLimitsExample.png"/>
      </fig>
      <p><cmdname>imagesc</cmdname> with <varname>x</varname> and <varname>y</varname> matrices
        example:
        <codeblock>cla;
im1 = imagesc(rand(10,10));
hold on;
im2 = imagesc(11:20, 11:20, rand(10,10));
</codeblock></p>
        <fig product="act_pro activate_basic compose_basic compose_pro">
        <title>Image repositioned on x and y axis.</title>
        <image href="images/imagescXYExample1.png"/>
      </fig>
      <p><varname>x</varname> and <varname>y</varname> may be used to scale an image:
        <codeblock>cla;
im1 = imagesc(rand(10,10));
hold on;
im2 = imagesc(11:15, 11:15, rand(10,10));
</codeblock></p>
        <fig product="act_pro activate_basic compose_basic compose_pro">
        <title>Scaled and repositioned image.</title>
        <image href="images/imagescXYExample2.png"/>
      </fig>
    </section>
    <section>
      <title>Comments</title>
      <p>If there are no axes, they will be created first. If <varname>x</varname> and
          <varname>y</varname> are omitted, the index of the <varname>z</varname> columns is used
        for the x coordinates and the index of the <varname>z</varname> rows is used for the y
        coordinates.</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: properties, image, imshow, imread,
        imwrite</draft-comment>
    </section>
  </refbody>
</reference>
