<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>imfinfo</title>
  <shortdesc>Gets information from the image, without reading pixel data, using functions defined in
    the <synph>omlimgtoolbox</synph>.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Image Commands</category>
      <keywords>
        <indexterm>imfinfo</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>R = imfinfo<var>(file)</var></synph></p>
      <p><synph>R = imfinfo<var>(file, ext)</var></synph></p>
    <?STOP?></section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>file</varname></pt>
          <pd>Name of the image file.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
          </plentry>
		<plentry>
          <pt><varname>ext</varname> (optional)</pt>
          <pd>Specifies the extension to use for the image file to be read. If this option is used,
              <varname>file</varname> should be the base name with no extension specified.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>The resulting structure with information about the image in <varname>file</varname>.
              <varname>R</varname> contains the following fields: <dl>
              <dlentry>
                <dt>BitDepth</dt>
                <dd>Bit depth of the image.</dd>
              </dlentry>
              <dlentry>
                <dt>ByteOrder</dt>
                <dd>Byte order stored. Values are '<option>big-endian</option>' if bytes are packed
                  with the most significant first, '<option>little-endian</option>' otherwise, or
                    '<option>undefined</option>' if it cannot be determined.</dd>
              </dlentry>
              <dlentry>
                <dt>ColorType</dt>
                <dd>Color type of the image. Values are '<option>grayscale</option>' for grayscale,
                    '<option>indexed</option>' if an image indexes into a color map, and
                    '<option>truecolor</option>' otherwise.</dd>
              </dlentry>
              <dlentry>
                <dt>Comment</dt>
                <dd>Text, if any, associated with the image.</dd>
              </dlentry>
              <dlentry>
                <dt>Compression</dt>
                <dd>Compression type, if available.</dd>
              </dlentry>
              <dlentry>
                <dt>DelayTime</dt>
                <dd>Delay in milliseconds before reading the next image for image formats that
                  support animation.</dd>
              </dlentry>
              <dlentry>
                <dt>DisposalMethod</dt>
                <dd>Specifies how succeeding frames are rendered for image formats that support
                  animation.</dd>
              </dlentry>
              <dlentry>
                <dt>FileModDate</dt>
                <dd>Last modification date of the image file on disk.</dd>
              </dlentry>
              <dlentry>
                <dt>FileSize</dt>
                <dd>Size of the image file on disk.</dd>
              </dlentry>
              <dlentry>
                <dt>Filename</dt>
                <dd>Absolute, normalized path to the image.</dd>
              </dlentry>
              <dlentry>
                <dt>Format</dt>
                <dd>Image type.</dd>
              </dlentry>
              <dlentry>
                <dt>Height</dt>
                <dd>Height of the image in pixels.</dd>
              </dlentry>
              <dlentry>
                <dt>LoopCount</dt>
                <dd>Number of loop iterations for an animation.</dd>
              </dlentry>
              <dlentry>
                <dt>Quality</dt>
                <dd>Quality of the image, with values from 0-100, 100 being the highest
                  quality.</dd>
              </dlentry>
              <dlentry>
                <dt>Width</dt>
                <dd>Width of the image in pixels.</dd>
              </dlentry>
            </dl></pd>
          <pd>Type: <keyword keyref="struct"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>      
    <section>
      <title>Examples</title>
      <p>Obtain information on the image file:
        <codeblock>addlibrary('omlimgtoolbox')
R = imfinfo('img_4871.jpg');
info</codeblock><codeblock>info = struct [
BitDepth: 32
ByteOrder: undefined
ColorType: truecolor
Comment: 
Compression: undefined
DelayTime: 0
DisposalMethod: 
FileModDate: Tue Jul 11 09:16:24 2017
FileSize: 9055297
Filename: c:\tests\oml\img_4871.jpg
Format: JPEG
Height: 3096
LoopCount: 0
Quality: 99
Width: 3870
]</codeblock></p>      
    </section>
    <section>
    <draft-comment author="denby">See Also: imread, imwrite, isgray, isbw</draft-comment>
  </section>
  </refbody>
</reference>
