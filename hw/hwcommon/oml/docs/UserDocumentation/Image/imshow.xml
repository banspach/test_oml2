<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>imshow</title>
  <shortdesc>Displays an image file in a figure.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Image Commands</category>
      <keywords>
        <indexterm>imshow</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>h = imshow<var>(input)</var></synph></p>
      <p><synph>h = imshow<var>(input, property, value, ...)</var></synph></p>
      <p><synph>h = imshow<var>(parent, ...)</var></synph></p>
      <p><synph>h = imshow<var>(input)</var></synph></p>  
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>input</varname></pt>
          <pd>Path of the file that is displayed in a figure or matrix.</pd>
          <pd>Type: <keyword keyref="string_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>property, value</varname></pt>

            <pd>
            <dl outputclass="table">
              <dlentry>
                <dt><synph>'position'</synph></dt>
                <dd>Position and size of <varname>h</varname>. Value is specified as a vector of the
                  form: [left top width height].</dd>
                <dd>If <synph>'units'</synph> has a value of <synph>'normalized'</synph>, values
                  must be between 0 to 1.</dd>
                <dd>Type: <keyword keyref="vec"/></dd>
              </dlentry>
              <dlentry>
                <dt><synph>'units'</synph></dt>
                <dd>Specifies units of measurement. Valid values are
                  <synph>'pixels'</synph>(default) and <synph>'normalized'</synph>. </dd>
                <dd>Value <synph>'pixel'</synph> indicates that <varname>h</varname> has a fixed
                  size and position specified by <synph>'position'</synph>. </dd>
                <dd>Value <synph>'normalized'</synph> indicates that <varname>h</varname> will be
                  resized if <varname>parent</varname> is resized.</dd>
                <dd>Type: <keyword keyref="string"/></dd>
              </dlentry>
            </dl>
          </pd>
            
        </plentry>
        <plentry>
          <pt><varname>parent</varname></pt>
          <pd>Handle of a container object, which could be an axis, figure, uipanel, or uitab.</pd>
          <pd>Type: <keyword keyref="dou_int"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>h</synph></pt>
          <pd>Handle of the image shown.</pd>
          <pd>Type: <keyword keyref="dou_int"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
        <p>Display an image from a file:
        <codeblock>close all;
h = imshow('img1.PNG');</codeblock></p>
<p>Display an image on an axis. Using hold 'on', data values can be drawn over the image:
        <codeblock>close all;
axes;
h = imshow(gca, 'img1.PNG');
hold('on');
plot(1:100,cos(1:100));
</codeblock></p>
    </section>
    <section>
      <title>Comments</title>
      <p>If the parent is an axis element then the image will be drawn in the grid area, the Y axis will be reversed and the axes method will be set to 'equal' by default.</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: figure, uicontrol, uipanel, uitabgroup, uitab,
        imattributes</draft-comment>
    </section>
  </refbody>
</reference>
