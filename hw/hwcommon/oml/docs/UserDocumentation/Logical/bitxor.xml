<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>bitxor</title>
  <shortdesc>Executes bitwise XOR operation on the inputs, <varname>x</varname>
    and <varname>y</varname>. <varname>x</varname> and <varname>y</varname>
    must be finite, non-negative natural numbers or matrices with such elements.

    If either <varname>x</varname> or <varname>y</varname> are matrices, the
    result, <varname>R</varname>, will also be a matrix. If both <varname>x</varname>
    or <varname>y</varname> are matrices, their sizes must match.
  </shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Logical Commands</category>
      <keywords>
        <indexterm>bitxor</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>R = bitxor<var>(x, y)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>x</varname></pt>
          <pd>Valid values are finite, non-negative natural numbers or matrices
            with such elements. If both <varname>x</varname> or <varname>y</varname>
            are matrices, their sizes must match.</pd>
          <pd>Type: Finite, non-negative <keyword keyref="int_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>y</varname></pt>
          <pd>Valid values are finite, non-negative natural numbers or matrices
            with such elements. If both <varname>x</varname> or <varname>y</varname>
            are matrices, their sizes must match.</pd>
          <pd>Type: Finite, non-negative <keyword keyref="int_mat"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Type: <keyword keyref="int_mat"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>Non-negative integer inputs:</p>
      <codeblock>R = bitxor(1,2)</codeblock>
      <codeblock>R = 3</codeblock>
      <p>Matrix and non-negative integer
        inputs:<codeblock>R = bitxor([3,2,1], 1)</codeblock><codeblock>R = [Matrix] 1 x 3
2  3  0</codeblock></p>
      <p>Matrix
        inputs:<codeblock>R = bitxor([1,2,3;4,5,6;6,7,8],[2,3,4;1,2,3;5,6,7])</codeblock><codeblock>R = [Matrix] 3 x 3
3  1   7
5  7   5
3  1  15</codeblock></p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: bitand, bitor</draft-comment>
    </section>
  </refbody>
</reference>
