<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>dlyapchol</title>
  <shortdesc>Solves discrete-time Lyapunov equations with a square-root solver.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Control System Commands</category>
      <keywords>
        <indexterm>dlyapchol</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>X = dlyapchol(<var>A, B</var>)</synph></p>
      <p><synph>X = dlyapchol(<var>A, B, E</var>)</synph></p>
      <?STOP?>
    </section>
    <section>
      <title>Inputs</title>
      <parml>
        <plentry>
          <pt><varname>A</varname></pt>
          <pd>Real square matrix.</pd>
        </plentry>
        <plentry>
          <pt><varname>B</varname></pt>
          <pd>A real matrix.</pd>
        </plentry>
        <plentry>
          <pt><varname>E</varname></pt>
          <pd>Real square matrix.</pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title>Outputs</title>
      <parml>
        <plentry>
          <pt><synph>X</synph></pt>
          <pd>A real matrix.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
	  <p>Example based on a Lyapunov matrix equation:
        <codeblock>A = [-5.0           0        -5.0;
       0.5           0           0;
         0           0         0.5];

 B = [ 0.5           1         0.5;
       1.5           1           1;
         1           1         1.5];

 U1 = dlyapchol (A, B)</codeblock><codeblock><systemoutput>U1 = [Matrix] 3 x 3
0.50000  1.50000  1.00000
1.00000  1.00000  1.00000
0.50000  1.00000  1.50000</systemoutput></codeblock></p>
<p>Example based on a Sylvester equation:
        <codeblock>A = [-5.0           0        -5.0;
       0.5           0           0;
         0           0         0.5];

 B = [ 0.5           1         0.5;
       1.5           1           1;
         1           1         1.5];
		 
 E = [2           2           4;
      1           0           5;
      3           1           1];
	  
 U2 = dlyapchol (A, B, E)</codeblock><codeblock><systemoutput>U2 = [Matrix] 3 x 3
0.50000  1.50000  1.00000
1.00000  1.00000  1.00000
0.50000  1.00000  0.00000</systemoutput></codeblock></p>
   </section>
    <section>
      <title>Comments</title>
      <p><synph>R = dlyapchol(<var>A, B</var>)</synph> solves <equation-inline>AU'UA' - U'U =
          -BB'</equation-inline> (Lyapunov matrix equation).</p>
      <p><synph>X = dlyapchol(<var>A, B, E</var>)</synph> solves <equation-inline>AU'UA' - EU'UE' =
          -B B'</equation-inline> (Sylvester equation). </p>
      <p>Based on the SLICOT library functions <cmdname>SB03OD</cmdname> and
          <cmdname>SG03BD</cmdname>.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: lyap, lyapchol</draft-comment>
    </section>
  </refbody>
</reference>
