<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>ss</title>
  <shortdesc>Constructs a state-space model.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Control System Commands</category>
      <keywords>
        <indexterm>ss</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>SYS = ss(<var>A, B, C, D</var>)</synph></p>
      <p><synph>SYS = ss(<var>A, B, C, D, Ts</var>)</synph></p>
      <p><synph>SYS = ss(<var>SYSIN</var>)</synph></p>
      <p><synph>SYS = ss(<var>SYSIN, 'minimal'</var>)</synph></p>
      <p><synph>SYS = ss(<var>D</var>)</synph></p>
      <?STOP?>
    </section>
    <section>
      <title>Inputs</title>
      <parml>
        <plentry>
          <pt><varname>A</varname></pt>
          <pd>The state matrix (n x n), where n is the number of states.</pd>
        </plentry>
        <plentry>
          <pt><varname>B</varname></pt>
          <pd>The input matrix (n x p), where p is the number of inputs.</pd>
        </plentry>
        <plentry>
          <pt><varname>C</varname></pt>
          <pd>The output matrix (q x n), where q is the number of outputs.</pd>
        </plentry>
        <plentry>
          <pt><varname>D</varname></pt>
          <pd>The direct transmission matrix (q x p).</pd>
        </plentry>
        <plentry>
          <pt><varname>Ts</varname></pt>
          <pd>Sampling time (in seconds).</pd>
          <pd><varname>Ts</varname> = <option>-2</option>: Static gain model. Example:
              <synph>ss(3)</synph></pd>
          <pd><varname>Ts</varname> = <option>-1</option>: Discrete-time model with unspecified
            sampling time.</pd>
          <pd><varname>Ts</varname> = <option>0</option>: Continuous-time model (default). </pd>
          <pd><varname>Ts</varname> > <option>0</option>: Discrete-time model. </pd>
          <pd>Changing this property will not discretize or re-sample the model.</pd>
        </plentry>
        <plentry>
          <pt><varname>SYSIN</varname></pt>
          <pd>Continuous-time state-space or transfer function model.</pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title>Outputs</title>
      <parml>
        <plentry>
          <pt><synph>SYS</synph></pt>
          <pd>Continuous-time state-space model.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
       <p>State matrices as input to the function:
        <codeblock>A = [0 1; -2 -3];
B = [0;  1];
C = [1 0];
D = 0;
sys1 = ss(A, B, C, D)</codeblock><codeblock><systemoutput>sys1 = object [
Scaled: 0
TimeUnit: seconds
ts: 0
a: [Matrix] 2 x 2
 0   1
-2  -3
b: [Matrix] 2 x 1
0
1
c: [Matrix] 1 x 2
1  0
d: 0
e: [Matrix] 0 x 0
type: StateSpaceModel
]</systemoutput></codeblock></p>
<p>Transfer function model as an input to the function ss:
        <codeblock>sys_tf = tf([1],[1 9 2]);
sys = ss(sys_tf)</codeblock><codeblock><systemoutput>sys = object [
Scaled: 0
TimeUnit: seconds
ts: 0
a: [Matrix] 2 x 2
-9  -2
 1   0
b: [Matrix] 2 x 1
1
0
c: [Matrix] 1 x 2
0  1
d: 0
e: [Matrix] 0 x 0
type: StateSpaceModel
]</systemoutput></codeblock></p>
    </section>
    <section>
      <title>Comments</title>
        <p>Constructs a state-space model with real or complex-valued matrices. <synph>SYS =
            ss(<var>A, B, C, D</var>)</synph> creates the continuous-time state-space model
        associated with matrices A,B,C,D.</p>
      <p><synph>SYS = ss(<var>SYSIN, 'minimal'</var>)</synph> computes a state-space model
        realization without unobservable and uncontrollable states.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: dss, tf, zpk</draft-comment>
    </section>
  </refbody>
</reference>
