<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>hsvd</title>
  <shortdesc>Hankel singular values of a state-space or transfer function model.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Control System Commands</category>
      <keywords>
        <indexterm>hsvd</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>HSV = hsvd(<var>SYS</var>)</synph></p>
      <p><synph>HSV = hsvd(<var>SYS, 'offset', OFFSET</var>)</synph></p>
      <p><synph>HSV = hsvd(<var>SYS, 'alpha', ALPHA</var>)</synph></p>
      <?STOP?>
    </section>
    <section>
      <title>Inputs</title>
      <parml>
        <plentry>
          <pt><varname>SYS</varname></pt>
          <pd>A state-space or transfer function model.</pd>
        </plentry>
        <plentry>
          <pt><varname>OFFSET</varname></pt>
          <pd>An offset for the stable/unstable boundary.</pd>
        </plentry>
        <plentry>
          <pt><varname>ALPHA</varname></pt>
          <pd>The ALPHA-stability boundary for the eigenvalues of the state dynamic.</pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title>Outputs</title>
      <parml>
        <plentry>
          <pt><synph>HSV</synph></pt>
          <pd>Hankel singular values.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
	  <p>Hankel singular values for stable/unstable boundary condition:
        <codeblock>sys1 = tf([1, 0.5],[1 2 2e-06]);
hsv1 = hsvd(sys1, 'offset', 0.001)</codeblock><codeblock><systemoutput>hsv1 = 0.187500156</systemoutput></codeblock></p>
	  <p>Hankel singular values for ALPHA-stability boundary condition:
        <codeblock>
a = [ -0.04165  0.0000  4.9200  -4.9200  0.0000  0.0000  0.0000;
      -5.2100  -12.500  0.0000   0.0000  0.0000  0.0000  0.0000;
       0.0000   3.3300 -3.3300   0.0000  0.0000  0.0000  0.0000;
       0.5450   0.0000  0.0000   0.0000 -0.5450  0.0000  0.0000;
       0.0000   0.0000  0.0000   4.9200 -0.04165 0.0000  4.9200;
       0.0000   0.0000  0.0000   0.0000 -5.2100 -12.500  0.0000;
       0.0000   0.0000  0.0000   0.0000  0.0000  3.3300 -3.3300];

b = [  0.0000   0.0000;
      12.5000   0.0000;
       0.0000   0.0000;
       0.0000   0.0000;
       0.0000   0.0000;
       0.0000   12.500;
       0.0000   0.0000];
       
c = [  1.0000   0.0000  0.0000   0.0000  0.0000  0.0000  0.0000;
       0.0000   0.0000  0.0000   1.0000  0.0000  0.0000  0.0000;
       0.0000   0.0000  0.0000   0.0000  1.0000  0.0000  0.0000];

sys2 = ss(a, b, c, []);
hsv2 = hsvd(sys2, 'alpha', 0.0)</codeblock><codeblock>
<systemoutput>hsv2 = [Matrix] 7 x 1
2.51388
2.08456
1.91780
0.76664
0.54729
0.02527
0.02458</systemoutput></codeblock></p>
    </section>
    <section>
      <title>Comments</title>
      <p>Based on the SLICOT library function <cmdname>AB13AD</cmdname>.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: hankel</draft-comment>
    </section>
  </refbody>
</reference>
