<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>obsvf</title>
  <shortdesc>Calculates the observability staircase form.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Control System Commands</category>
      <keywords>
        <indexterm>obsvf</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>[ABAR, BBAR, CBAR, T, K] = obsvf(<var>A, B, C</var>)</synph></p>
      <p><synph>[ABAR, BBAR, CBAR, T, K] = obsvf(<var>A, B, C, TOL</var>)</synph></p>
      <?STOP?>
    </section>
    <section>
      <title>Inputs</title>
      <parml>
        <plentry>
          <pt><varname>A</varname></pt>
          <pd>The state matrix (n x n), where n is the number of states.</pd>
        </plentry>
        <plentry>
          <pt><varname>B</varname></pt>
          <pd>The input matrix (n x p), where p is the number of inputs.</pd>
        </plentry>
        <plentry>
          <pt><varname>C</varname></pt>
          <pd>The output matrix (q x n), where q is the number of outputs.</pd>
        </plentry>
        <plentry>
          <pt><varname>TOL</varname></pt>
          <pd>A scalar real (tolerance). By default, <equation-inline>10 * n * norm(a, 1) *
              eps()</equation-inline>.</pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title>Outputs</title>
      <parml>
        <plentry>
          <pt><synph>ABAR</synph></pt>
          <pd>The observability staircase state matrix.</pd>
        </plentry>
        <plentry>
          <pt><synph>BBAR</synph></pt>
          <pd>The observability staircase input matrix.</pd>
        </plentry>
        <plentry>
          <pt><synph>CBAR</synph></pt>
          <pd>The observability staircase output matrix.</pd>
        </plentry>
        <plentry>
          <pt><synph>T</synph></pt>
          <pd>The similarity transform matrix.</pd>
        </plentry>
        <plentry>
          <pt><synph>K</synph></pt>
          <pd>A vector containing the number of observable states.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
	  <p>Observability staircase form of state matrices:
        <codeblock>A = [5     4;
     4    -2];

B = [1    -2;
     1    -2];

C = [1.5     0;
     0     1.5];
         
[Abar, Bbar, Cbar, T, k] = obsvf(A, B, C)</codeblock><codeblock><systemoutput>Abar = [Matrix] 2 x 2
5   4
4  -2
Bbar = [Matrix] 2 x 2
1  -2
1  -2
Cbar = [Matrix] 2 x 2
1.50000  0.00000
0.00000  1.50000
T = [Matrix] 2 x 2
1  0
0  1
k = [Matrix] 1 x 2
2  0</systemoutput></codeblock></p>
<p>Observability staircase form of transfer function model:
        <codeblock>sys_tf = tf([1],[2 3 1]);
sys = ss(sys_tf);
[Abar, Bbar, Cbar, T, k] = obsvf(sys.a, sys.b, sys.c)</codeblock><codeblock><systemoutput>Abar = [Matrix] 2 x 2
-1.50000  -0.50000
 1.00000   0.00000
Bbar = [Matrix] 2 x 1
-0.50000
 0.00000
Cbar = [Matrix] 1 x 2
0  -1
T = [Matrix] 2 x 2
-1   0
 0  -1
k = [Matrix] 1 x 2
1  1</systemoutput></codeblock></p>
    </section>
    <section>
      <title>Comments</title>
      <p><synph>[ABAR, BBAR, CBAR, T, K] = obsvf(<var>A, B, C</var>)</synph> computes the
        observability staircase form of A, B, C.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: crtbf, contr, is obsv</draft-comment>
    </section>
  </refbody>
</reference>
