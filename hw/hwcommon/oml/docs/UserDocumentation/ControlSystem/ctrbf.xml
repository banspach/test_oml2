<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>ctrbf</title>
  <shortdesc>Calculates the controllability staircase form.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Control System Commands</category>
      <keywords>
        <indexterm>crtbf</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>[ABAR, BBAR, CBAR, T, K] = ctrbf(<var>A, B, C</var>)</synph></p>
      <p><synph>[ABAR, BBAR, CBAR, T, K] = ctrbf(<var>A, B, C, TOL</var>)</synph></p>
      <?STOP?>
    </section>
    <section>
      <title>Inputs</title>
      <parml>
        <plentry>
          <pt><varname>A</varname></pt>
          <pd>The state matrix (n x n), where n is the number of states.</pd>
        </plentry>
        <plentry>
          <pt><varname>B</varname></pt>
          <pd>The input matrix (n x p), where p is the number of inputs.</pd>
        </plentry>
        <plentry>
          <pt><varname>C</varname></pt>
          <pd>The output matrix (q x n), where q is the number of outputs.</pd>
        </plentry>
        <plentry>
          <pt><varname>TOL</varname></pt>
          <pd>The tolerance scalar. Default =
              <equation-inline>size(A,1)*norm(A,1)*eps()</equation-inline>.</pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title>Outputs</title>
      <parml>
        <plentry>
          <pt><synph>ABAR</synph></pt>
          <pd>The controllability staircase state matrix.</pd>
        </plentry>
        <plentry>
          <pt><synph>BBAR</synph></pt>
          <pd>The controllability staircase input matrix.</pd>
        </plentry>
        <plentry>
          <pt><synph>CBAR</synph></pt>
          <pd>The controllability staircase output matrix.</pd>
        </plentry>
        <plentry>
          <pt><synph>T</synph></pt>
          <pd>The similarity transform matrix.</pd>
        </plentry>
        <plentry>
          <pt><synph>K</synph></pt>
          <pd>A vector containing the number of controllable states factored from the transformation matrix.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
	  <p>Matrices as input:
        <codeblock>A = [10 5; 9 -20];
B = [3 -10; 10 -2];
C = [25 0; 0 15];
[Abar, Bbar, Cbar, T, k] = ctrbf(A, B, C);</codeblock><codeblock><systemoutput>Abar = [Matrix] 2 x 2
10    5
 9  -20
Bbar = [Matrix] 2 x 2
 3  -10
10   -2
Cbar = [Matrix] 2 x 2
25   0
 0  15
T = [Matrix] 2 x 2
1  0
0  1
k = [Matrix] 1 x 2
2  0
%The decomposed system Abar shows an uncontrollable mode located at -20 and controllable modes at 10, 5 and 9.</systemoutput></codeblock></p>
<p>Inputs from the state-space model:
        <codeblock>sys_tf=tf([1],[1 5 6 0]);
sys=ss(sys_tf);
[Abar, Bbar, Cbar, T, k] = ctrbf(sys.a, sys.b, sys.c)</codeblock><codeblock><systemoutput>Abar = [Matrix] 3 x 3
0  1   0
0  0  -2
0  3  -5
Bbar = [Matrix] 3 x 1
0.00000
0.00000
0.50000
Cbar = [Matrix] 1 x 3
-1  0  0
T = [Matrix] 3 x 3
0   0  -1
0  -1   0
1   0   0
k = [Matrix] 1 x 3
1  1  1
%The decomposed system Abar shows an uncontrollable modes located at -2 and -5 and controllable modes at 1 and 3.</systemoutput></codeblock></p>
    </section>
    <section>
      <title>Comments</title>
      <p><synph>[ABAR,BBAR,CBAR,T,K] = ctrbf(<var>A,B,C</var>)</synph> calculates the
        controllability staircase form.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: crtb, iscrtb, obsvf</draft-comment>
    </section>
  </refbody>
</reference>
