<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>weekday</title>
  <shortdesc>Returns the day of the week for the given date.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Time Commands</category>
      <keywords>
        <indexterm>weekday</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>[d, s] = weekday<var>(date)</var></synph></p>
      <p><synph>[d, s] = weekday<var>(date, format)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>date</varname></pt>
          <pd>The date as a number (<cmdname>datenum</cmdname>) or as a string. If the date is a
            string, it must be in a format that <cmdname>datevec</cmdname> can read.</pd>
          <pd>Type: <keyword keyref="dou"/> | <keyword keyref="string"/></pd>
          <pd>Dimension: <keyword keyref="scal_mat"/> | <keyword keyref="cell"/>  </pd>
        </plentry>
        <plentry>
          <pt><varname>format</varname></pt>
          <pd>The format of the output <synph>s</synph>. The value can be either 'short' or 'long'
            with 'short' being the default. If <varname>format</varname> is 'short' then the
            abbreviated day name is returned. If it is 'long' then the full day name is
            returned.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>d</synph></pt>
          <pd>The day of the week, [1-7] with 1 being Sunday.</pd>
        </plentry>
        <plentry>
          <pt><synph>s</synph></pt>
          <pd>The name of the day. Full name or abbreviation depending on the <varname>format</varname> input.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>Get the day of the week from a date number:</p>
      <codeblock>[d, s] = weekday(now)</codeblock>
      <codeblock>
d = 6
s = Fri
</codeblock>
<p>Get the full day name for each date number in a matrix:</p>
      <codeblock>[d, s] = weekday(now+[1:6], 'long')</codeblock>
      <codeblock>d = [Matrix] 1 x 6
7  1  2  3  4  5
s = 
Saturday 
Sunday   
Monday   
Tuesday  
Wednesday
Thursday 
</codeblock>
<p>Get the day of the week from a date string:</p>
      <codeblock>[d, s] = weekday('20-Aug-2022')</codeblock>
      <codeblock>d = 7
s = Sat
</codeblock>
<p>Get the day of the week for each date string in a cell of strings:</p>
      <codeblock>[d, s] = weekday({'Aug.04,2022', '20-Sep-2022'})</codeblock>
      <codeblock>d = [Matrix] 1 x 2
5  3
s = 
Thu
Tue 
</codeblock>
    </section>
    <section>
      <draft-comment author="denby">See Also: datevec, datestr</draft-comment>
    </section>
  </refbody>
</reference>
