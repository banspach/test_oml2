<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>trapz</title>
  <shortdesc>Numerical integration of discrete data using the trapezoid rule.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Calculus Commands</category>
      <keywords>
        <indexterm>trapz</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>area=trapz<var>(y)</var></synph></p>
      <p><synph>area=trapz<var>(x,y)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>x</varname></pt>
          <pd>Values of the domain variable.</pd>
          <pd>When omitted, a vector with unit increments is supplied.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>y</varname></pt>
          <pd>Values of the range variable.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>area</synph></pt>
          <pd>The estimated area.</pd>
          <pd>Dimension: <keyword keyref="scal_mat"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <codeblock>x = [0:0.05:1]*pi; area = trapz(x,sin(x))</codeblock>
      <codeblock><systemoutput>area = 1.99588597</systemoutput></codeblock>
    </section>
    <section>
      <title>Comments</title>
      <p><cmdname>trapz</cmdname> is appropriate for use with sampled data when the function is not
        available.</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: cumtrapz, quad, quadv</draft-comment>
    </section>
  </refbody>
</reference>
