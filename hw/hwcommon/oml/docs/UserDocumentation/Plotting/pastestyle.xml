<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="pastestyle">
  <title>pastestyle</title>
  <shortdesc>Sets the style properties to a plot object.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Plotting Commands</category>
      <keywords>
        <indexterm>pastestyle</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>pastestyle<var>(handle)</var></synph></p>
      <p><synph>pastestyle<var>(handle, style)</var></synph></p>  
    </section>
    <section> 
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>handle</varname></pt>
          <pd>Handle of an axes, line, surface, hggroup or text object.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>style</varname></pt>
          <pd>A structure exported by the <cmdname>copystyle</cmdname> command.</pd>
          <pd>Type: <keyword keyref="struct"/></pd>
        </plentry>
      </parml><?STOPINP?>
    </section>
    <section>
      <title>Examples</title>
      <p>Paste the style to a line object:</p>
      <codeblock>clf;
lh = plot(rand(10,2));
legend;
% you may change the style of the first line using the set command or the graphical interface
set(lh(1),{'marker','linestyle','linewidth'},{'o',':',3});
% copy the style of the first line
copystyle(lh(1));
% paste the style to the second line
pastestyle(lh(2))
      </codeblock>
      <fig id="fig_njn_bcp_fpb">
          <title>Paste properties to a line object</title>
          <image placement="break" href="./images/pastestyle1.png"/>
      </fig>
      <p>Paste the style to an axes object:</p>
      <codeblock>clf;
subplot(1,2,1)
x = [0:0.2:2*pi];
y1 = cos(x);
lh = plot([x;x+1]',[y1;y1]','b:');
set(gca,{'fontweight', 'fontsize','xgrid','ygrid'},{'bold',8,'on','on'});
xl = xlabel('X Axis');
yl = ylabel('Y Axis');
legend;
% get the handle of the first axes
ax1 = gca;

subplot(1,2,2);
y2 = sin(x);
plot([x;x+1;x+2;x+3]',[y2;y2;y2;y2]');
% paste the style of the first axes to the second axes - 
% Note that the style of the 3rd and 4th line doesn't change because the first axes has only 2 lines
ax2 = gca;
pastestyle(ax2, copystyle(ax1));
      </codeblock>
      <fig id="fig_upd_4lw_kqb">
        <title>Paste properties to an axes object</title>
        <image placement="break" href="./images/pastestyle2.png" id="image_omg_4lw_kqb"/>
      </fig>
    </section>
    <section>
      <title>Comments</title>
      <p>The object that the style is pasted to must be the same type as the object that the style was copied from.</p>
      <p>If the <varname>style</varname> argument is omitted then the style saved in memory will be applied to the object.</p>
      <p>The <cmdname>pastestyle</cmdname> command will not create new objects, it will apply the style properties to existing objects. For example, if the style is copied from an axes object which has 3 lines and is pasted to an axes object which has 2 lines then the style of the 3rd line will be ignored.</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: copystyle</draft-comment>
    </section>
  </refbody>
</reference>
