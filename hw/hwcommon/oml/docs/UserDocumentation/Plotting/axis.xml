<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>axis</title>
  <shortdesc>Sets the scaling and range of x, y, and z axes (x by default). Called without
    arguments, <cmdname>axis</cmdname> turns <varname>autoscaling</varname> on.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Plotting Commands</category>
      <keywords>
        <indexterm>axis</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>axis<var>()</var></synph></p>
      <p><synph>axis<var>([X_lo X_hi])</var></synph></p>
      <p><synph>axis<var>([X_lo X_hi Y_lo Y_hi])</var></synph></p>
      <p><synph>axis<var>([X_lo X_hi Y_lo Y_hi Z_lo Z_hi])</var></synph></p>
      <p><synph>axis<var>(option)</var></synph></p>
      <?STOP?></section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>X_lo, X_hi</varname></pt>
          <pd>Lowest and highest x ranges.</pd>
          <pd>Type: <keyword keyref="dou_int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>Y_lo, Y_hi</varname></pt>
          <pd>Lowest and highest y ranges.</pd>
          <pd>Type: <keyword keyref="dou_int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>Z_lo, Z_hi</varname></pt>
          <pd>Lowest and highest z ranges.</pd>
          <pd>Type: <keyword keyref="dou_int"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
        <plentry>
          <pt><varname>option</varname></pt>
          <pd>Takes one of the following values:<dl outputclass="table">
              <dlentry>
                <dt>'bestfit'</dt>
                <dd>The length of each axis is adjusted to maximize the graphics area (3D plots
                  only).</dd>
              </dlentry>
              <dlentry>
                <dt>'cubical'</dt>
                <dd>The length of each axis is the same (3D plots only). This is the default
                  value.</dd>
              </dlentry>
              <dlentry>
                <dt><synph>'equal'</synph></dt>
                <dd>Sets uniform aspect ratio to the axes (2D plots only).</dd>
              </dlentry>
              <dlentry>
                <dt><synph>'normal'</synph></dt>
                <dd>Resets the aspect ratio (2D plots only).</dd>
              </dlentry>
              <dlentry>
                <dt><synph>'on'</synph></dt>
                <dd>Enables the visibility of the axes' tics and labels.</dd>
              </dlentry>
              <dlentry>
                <dt><synph>'off'</synph></dt>
                <dd>Disables the visibility of the axes' tics and labels.</dd>
              </dlentry>
              <dlentry>
                <dt><synph>'square'</synph></dt>
                <dd>Sets square aspect ratio to the axes (2D plots only).</dd>
              </dlentry>
              <dlentry>
                <dt>'tight'</dt>
                <dd>Sets the axes range equal to the data limits.</dd>
              </dlentry>
              <dlentry>
                <dt>'unscaled'</dt>
                <dd>The length of each axis is based on the axis values (3D plots only).</dd>
              </dlentry>
            </dl></pd>
        </plentry>
      </parml><?STOPINP?>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>Simple <cmdname>axis</cmdname>
          example:<codeblock>clf;
x=linspace(-pi,pi, 100);
plot(x,sin(x));
axis ([-4 4 -1.5 1.5])</codeblock><fig
          id="fig_xyy_mvq_55">
          <title>Simple axis example</title>
          <image placement="break" href="../../../../images/math_solutions/plots/axis.PNG"
            id="image_d54_hxp_rv"/>
        </fig></p>
      <p><cmdname>axis('equal')</cmdname>
          example:<codeblock>clf;
x=linspace(-pi,pi, 100);
plot(x,sin(x));
axis('equal');</codeblock><fig
          id="fig_tdv_tn3_nmb">
          <title>Uniform aspect ratio</title>
          <image placement="break" href="../../../../images/math_solutions/plots/axisEqual.png"
            id="image_wfv_tn3_nmb"/>
        </fig></p>
      <p><cmdname>axis('square')</cmdname>
          example:<codeblock>clf;
x=linspace(-pi,pi, 100);
plot(x,sin(x));
axis('square');</codeblock><fig
          id="fig_tpg_5n3_nmb">
          <title>Square aspect ratio</title>
          <image placement="break" href="../../../../images/math_solutions/plots/axisSquare.png"
            id="image_upg_5n3_nmb"/>
        </fig></p>
        <p><cmdname>axis('tight')</cmdname> example: <codeblock id="codeblock_akg_btc_lqb">clf;
x=linspace(-pi,pi, 100);
plot(x,sin(x));
axis('tight');
</codeblock></p>
      <p>
        <fig id="fig_bkg_btc_lqb">
          <title>Tight fit to curve data</title>
          <image placement="break" href="./images/axisTight.png" id="image_ckg_btc_lqb"/>
        </fig>
      </p>
      <p><cmdname>axis('bestfit')</cmdname>
          example:<codeblock>clf;
x=[0:0.1:2*pi];
y=x;
z=sin(x')*cos(y);
s=surf(x, y, z)
axis('bestfit');</codeblock><fig
          id="fig_xdm_n1l_mnb">
          <title>Best fit axes</title>
          <image placement="break" href="../../../../images/math_solutions/plots/axisBestfit.png"
            id="image_ydm_n1l_mnb"/>
        </fig></p>
      <p><cmdname>axis('unscaled')</cmdname> example:<fig id="fig_kdm_n1l_mnb">
          <title>Unscaled axes</title>
          <image placement="break" href="../../../../images/math_solutions/plots/axisUnscaled.png"
            id="image_ldm_n1l_mnb"/>
        </fig><codeblock>clf;
x=[0:0.1:2*pi];
y=x;
z=sin(x')*cos(y);
s=surf(x, y, z)
axis('unscaled');</codeblock></p>
    </section>
    <section>
      <draft-comment author="denby">See Also: </draft-comment>
    </section>
  </refbody>
</reference>
