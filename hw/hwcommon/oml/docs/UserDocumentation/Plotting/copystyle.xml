<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="copystyle">
  <title>copystyle</title>
  <shortdesc>Creates a copy of the style properties of a plot object.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Plotting Commands</category>
      <keywords>
        <indexterm>copystyle</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>copystyle<var>(handle)</var></synph></p>
      <p><synph>style = copystyle<var>(handle)</var></synph></p></section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>handle</varname></pt>
          <pd>Handle of an axes, line, surface, hggroup or text object.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
      </parml><?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>style</synph></pt>
          <pd>A structure that contains the style properties of the object.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>Copy the style of a line object:</p>
      <codeblock>clf;
lh = plot(rand(10,1));
set(lh,{'marker','linestyle','linewidth'},{'o',':',3});
style = copystyle(lh);
      </codeblock>
      <fig id="fig_njn_bcp_fpb">
          <title>Style properties of a line object</title>
          <image placement="break" href="./images/copystyle1.png"/>
      </fig>
      <p>Copy the style of an axes object:</p>
      <codeblock>clf;
lh = plot(rand(10,2),'r:o');
set(gca,{'fontweight', 'fontsize','xgrid','ygrid'},{'bold',14,'on','on'});
style = copystyle(gca);
      </codeblock>
      <p>Note that the style of the axes object includes the style of its children:</p>
      <fig id="fig_lks_lkw_kqb">
        <title>Style properties of an axes object</title>
        <image placement="break" href="./images/copystyle2.png" id="image_wtr_mkw_kqb"/>
      </fig>
    </section>
    <section>
      <title>Comments</title>
      <p>The purpose of <cmdname>copystyle</cmdname> command is to copy the style (colors, fonts, etc.) of an axes object or a child element of the axes object. The position of the axes, the data values of line/surfaces or the position of text boxes will not be copied.</p>
      <p>If an output argument is not provided then the style will be saved in memory to be used with the <cmdname>pastestyle</cmdname> command.</p>
      <p>The <cmdname>copystyle</cmdname> command is not supported for uicontrol objects.</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: pastestyle</draft-comment>
    </section>
  </refbody>
</reference>
