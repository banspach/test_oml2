<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>getmousepos</title>
  <shortdesc>Returns the mouse position in the figure area.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Plotting Commands</category>
      <keywords>
        <indexterm>getmousepos</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section><title>Syntax</title>
      <p><synph>[x, y] = getmousepos()</synph></p>
      <p><synph>getmousepos<var>('print', status)</var></synph></p>
      <p><synph>getmousepos('print', <var>status</var>, 'parent', <var>handle</var>)</synph></p>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml id="parml_azt_dvl_vpb">
        <plentry>
          <pt><varname>status</varname></pt>
          <pd>If the value is <synph>'on'</synph>, the mouse position will be printed in the command
            window, whenever there is a right mouse click in a figure that has only GUI elements and
            no axes. This property is useful when building a gui as both the normalized and pixel
            positions are printed. If the value is <synph>'off'</synph>, mouse position will not be
            printed on right click.</pd>
          <pd>Type: <keyword keyref="off_on"/></pd>
        </plentry>
        <plentry>
          <pt><varname>handle</varname></pt>
          <pd>Optional input which specifies the handle of a ui element that will be used as a
            parent (reference) when calculating the normalized position printed. If no
              <synph>parent</synph> property is given, the current &lt;cmdname>gcf&lt;/cmdname>
            handle will be used as reference. If the mouse position is not contained in the parent
              <varname>handle</varname>, the normalized position will be <synph>[NaN NaN]</synph> in
            the message printed.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
        </plentry>
      </parml>
    </section>
      <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>x, y</synph></pt>
          <pd>The position of the mouse.</pd>
          <pd>If the mouse is over a 2D plot, <varname>x</varname> and <varname>y</varname> are in
            the plot's coordinates system, which is defined by the x and y axes.</pd>
          <pd>If the mouse is not over a 2D plot, then <varname>x</varname> and <varname>y</varname>
            are in the figure's coordinate system where the axes exists, the (0,0) point is in the upper-left corner
            of the figure window.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>In the following example, <cmdname>getmousepos</cmdname> is used along with the
          '<option>mouseclickcallback</option>' property to obtain the mouse position everytime you
        click on the
        plot:<codeblock>close all;
plot(rand(100,1));
set(gca, 'mouseclickcallback',@get_mouse_pos);

function get_mouse_pos(handle, callbackdata)
  [x,y] = getmousepos() 
  callbackdata
end</codeblock></p>
      <p>The next example demonstrates the two coordinate systems. The mouse coordinates are printed
        in the console as the mouse moves over the
        figure.<codeblock>close all;
figure();
subplot(1,2,2);
plot(rand(10,1));

ellipse(gcf,'pos',[0, 0, 20, 20], 'edgecolor', 'r','linewidth',2);

for i=1:100 
  [x,y] = getmousepos()
  pause(1);
end</codeblock></p>
 <p><cmdname>getmousepos</cmdname> and the '<option>mouseclickcallback</option>' property are
        combined in the following example to create new elements in the
        figure:<codeblock>clear all, close all;
axes('position',[0,0,0,0]);
set(gca,'mouseclickcallback',@mouseclicked);

function mouseclicked(h,callbackdata)
  [x,y]=getmousepos();
  ellipse(gcf,'pos',[x-10, y-10, 20, 20]) 
end</codeblock></p>
    <p><cmdname>getmousepos</cmdname> with printing in a figure used for GUI elements only
<codeblock>
getmousepos('print', 'on')
f = gcf();
% Right click in the figure once it is created. The following message will be printed
Mouse position: normalized [0.45 0.44], pixels [307 143], parent [1.000000]
getmousepos('print', 'off')</codeblock></p>
    <p><cmdname>getmousepos</cmdname> with printing in a figure used for GUI elements only, with a parent reference
<codeblock>
getmousepos('print', 'on')
f = gcf();
% Right click in the figure once it is created. The following message will be printed
Mouse position: normalized [0.45 0.44], pixels [307 143], parent [1.000000]
% Use the normalized mouse position to create a frame inside gcf
frame1 = uipanel(gcf(), 'title', 'Frame1', 'units', 'normalized', 'position', [0.45 0.44 0.5 0.4]);
getmousepos('print', 'on', 'parent', frame1)
% Right click in the parent frame to get the position where a button needs to be created
Mouse position: normalized [0.12 0.38], pixels [348 193], parent [17.819334]
% Use the normalized mouse position to create a button inside parent object
button = uicontrol(frame1, 'style', 'pushbutton', 'string', 'Next', 'units', 'normalized', 'position', [0.12 0.38 0.25 0.18]);
getmousepos('print', 'off')</codeblock></p>

</section>
    <section>
      <title>Comments</title>
      <p>If the mouse is not inside the figure area when <cmdname>getmousepos</cmdname> is called,
        the outputs will be <cmdname>NaN</cmdname>.</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: figure, uicontrol, uitab, uipanel, waitforbuttonpress</draft-comment>
    </section>
  </refbody>
</reference>
