<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>bsxfun</title>
  <shortdesc>Evaluates the function <varname>func</varname> on the elements (element-wise operation) of the input matrices.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Data Structures Commands</category>
      <keywords>
        <indexterm>bsxfun</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>R = bsxfun<var>(func, input1, input2)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>func</varname></pt>
          <pd>Function to be evaluated. The function must accept two scalar inputs.</pd>
          <pd>Type: <keyword keyref="char_string_handle"/></pd>
        </plentry>
        <plentry>
          <pt><varname>input1</varname></pt>
          <pd>Matrix or scalar to be operated on.</pd>
          <pd>Type: <keyword keyref="scal_mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>input2</varname></pt>
          <pd>Matrix or scalar to be operated on.</pd>
          <pd>Type: <keyword keyref="scal_mat"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Resulting output.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section><title>Examples</title>
      <p>Evaluate function on scalar and matrix inputs:
        <codeblock>function z = my_fun(a,b)
  z = 2*max(a,b)+b;
end

R = bsxfun(@my_fun, 2, [2 3 4;5 6 7])</codeblock>
<codeblock>R = [Matrix] 2 x 3
 6   9  12
15  18  21</codeblock></p>
        
      <p>Evaluate function on matrix inputs:
        <codeblock>function z = my_fun(a,b)
  z = 2*max(a,b)+b;
end

R = bsxfun(@my_fun, [1 2 3;10 11 12], [2 3 4;5 6 7])</codeblock>
<codeblock>R = [Matrix] 2 x 3
 6   9  12
25  28  31</codeblock></p>
<p>Evaluate function on nd matrix and matrix inputs:
        <codeblock>function z = my_fun(a,b)
  z = 2*max(a,b)+b;
end

a = [1 2 3;4 5 6];
a(:,:,2) = ones(2, 3);
b = [2 3 4;5 6 7];
R = bsxfun(@my_fun, a, b)</codeblock>
<codeblock>R = [Matrix] 2 x 3
 6   9  12
25  28  31</codeblock></p>
      </section>
      <section>
      <title>Comments</title>
      <p>If both <varname>input1</varname> and <varname>input2</varname> are matrices then their dimensions must be the same.</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: cellfun</draft-comment>
    </section>
  </refbody>
</reference>
