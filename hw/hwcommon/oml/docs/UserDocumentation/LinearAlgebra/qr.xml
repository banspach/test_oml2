<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>qr</title>
  <shortdesc>QR decomposition.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Linear Algebra Commands</category>
      <keywords>
        <indexterm>qr</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>[Q,R] = qr<var>(A)</var></synph></p>
      <p><synph>[Q,R] = qr<var>(A,econ)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>A</varname></pt>
          <pd>The matrix to decompose.</pd>
          <pd>Dimension: <keyword keyref="mat"/></pd>
        </plentry>
        <plentry>
          <pt><varname>econ</varname></pt>
          <pd>Economy decomposition flag. Set to 0.</pd>
          <pd>Type: integer</pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>Q</synph></pt>
          <pd>Orthonormal matrix.</pd>
          <pd>Dimension: <keyword keyref="mat"/></pd>
        </plentry>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Upper triangular matrix.</pd>
          <pd>Dimension: <keyword keyref="mat"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <p>Matrix input:</p>
      <codeblock>[Q,R] = qr([1,2,3;4,5,6;7,8,10;11,12,15],0)</codeblock>
      <codeblock>Q = [Matrix] 4 x 3
-0.073127 -0.8104  0.56383
-0.29251  -0.46944 -0.78362
-0.51189  -0.12848 -0.038225
-0.8044   0.32614  0.25802
R = [Matrix] 3 x 3
-13.675 -15.357 -19.159
0       -1.0822 -1.6406
0       0       0.47782</codeblock>
    </section>
    <section>
      <title>Comments</title>
      <p><synph>[Q,R] = qr(A)</synph> computes matrices Q and R such that <synph>A = QR</synph>.</p>
      <p>QR decomposition is often used for solving over-determined linear least squares equations
          <synph>Ax = b</synph>. For the economy decomposition of an MxN matrix A with
          <synph>m>n</synph>, Q is MxN and R is NxN.</p>
      <p><cmdname>qr</cmdname> uses the LAPACK routines 'dgeqrf' for real matrices and 'zgeqrf' for
        complex matrices.</p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: chol, lu, eig, schur, svd</draft-comment>
    </section>
  </refbody>
</reference>
