<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>convhull</title>
  <shortdesc>Computes the 2D convex hull.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>Geometry Commands</category>
      <keywords>
        <indexterm>convhull</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>h = convhull<var>(x,y)</var></synph></p>
      <p><synph>h = convhull<var>(x,y,options)</var></synph></p>
      <p><synph>[h,a] = convhull<var>(...)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>x</varname></pt>
          <pd>The x coordinates.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>y</varname></pt>
          <pd>The y coordinates.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><varname>options</varname></pt>
          <pd>The Qhull options. A string with options separated by spaces, or a cell of strings.
            Omit or use [] for the default. For no options, use an empty string.</pd>
          <pd>Default: '<option>Qt'</option></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>h</synph></pt>
          <pd>The indices of the convex hull vertices.</pd>
          <pd>Dimension: <keyword keyref="vec"/></pd>
        </plentry>
        <plentry>
          <pt><synph>a</synph></pt>
          <pd>The area of the convex hull.</pd>
          <pd>Type: <keyword keyref="dou"/></pd>
          <pd>Dimension: <keyword keyref="scal"/></pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <codeblock>ang = [1:1:60] * (2*pi/60);
x = 8 .* (1 + 0.1*cos(8*ang)) .* cos(ang);
y = 6 .* (1 + 0.1*cos(8*ang)) .* sin(ang);
h = convhull(x, y);
plot(x,y);
hold on;
plot(x(h),y(h));
legend('shape', 'hull');</codeblock>
      <p>
        <fig id="fig_t5n_v4r_bt">
          <title>convhull figure 1</title>
          <image placement="break"
            href="../../../../images/math_solutions/plots/convhull_help_figure1.png"
            id="image_flt_lxm_c5" height="350" width="350"/>
        </fig>
      </p>
    </section>
    <section>
      <title>Comments</title>
      <p><cmdname>convhulln</cmdname> uses the Qhull package. For details, see:</p>
      <xref href="http://www.qhull.org/html/qh-quick.htm" format="html" scope="external"/>
      <xref href="http://www.qhull.org/html/qh-optq.htm" format="html" scope="external"/>
      <p>Using <varname>options</varname> overwrites the default, so the default must be repeated if
        it is to be included.</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: convhulln, delaunay</draft-comment>
    </section>
  </refbody>
</reference>
