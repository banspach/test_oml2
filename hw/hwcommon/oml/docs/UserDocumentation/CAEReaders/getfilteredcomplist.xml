<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>getfilteredcomplist</title>
  <shortdesc>Returns the component list for a given data type and request. The function has two forms.
	  Use the first form when there is zero or at most one <varname>subcase</varname> and second form when there are multiple subcases.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>HW Readers Commands</category>
      <keywords>
        <indexterm>getfilteredcomplist</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>getfilteredcomplist<var>(filename, datatype, request)</var></synph></p>
      <p><synph>getfilteredcomplist<var>(filename, subcase, datatype, request)</var></synph></p>
	  <p><synph>R = getfilteredcomplist<var>(...)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>filename</varname></pt>
          <pd>Path of the file.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>subcase</varname></pt>
          <pd>Subcase name or index in the file.</pd>
          <pd>Type: <keyword keyref="string_int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>datatype</varname></pt>
          <pd>Data type name or index in the file.</pd>
          <pd>Type: <keyword keyref="string_int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>request</varname></pt>
          <pd>Request name or index in the file.</pd>
          <pd>Type: <keyword keyref="char_string_int"/></pd>
          <pd>Dimension: <keyword keyref="scal_string"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Cell array of component names.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Examples</title>
      <p>Basic <cmdname>getfilteredcomplist</cmdname> example without
        <varname>subcase</varname>:</p>
      <codeblock>getfilteredcomplist('Path/To/File/ANGACC','Angular Acceleration','50th% Hybrid3   - LOWER TORSO')</codeblock>
      <codeblock><systemoutput>R = {
[1,1] Res. ang. acc.
[1,2] X-comp. ang. acc.
[1,3] Y-comp. ang. acc.
[1,4] Z-comp. ang. acc.
}</systemoutput></codeblock>
      <p>Basic <cmdname>getfilteredcomplist</cmdname> example with
        <varname>subcase</varname>:<codeblock>getfilteredcomplist('Path/To/File/bezel.res','Scalar','Velocities (c)','Node 10000')</codeblock><codeblock><systemoutput>R = {
[1,1] MagX
[1,2] MagY
[1,3] MagZ
[1,4] PhX
[1,5] PhY
[1,6] PhZ
}</systemoutput></codeblock></p>
    </section>
    <section>
      
      <draft-comment author="denby">See Also: getcomplist, getcompindex, getcompname,
        getnumcomps</draft-comment>
    </section>
  </refbody>
</reference>
