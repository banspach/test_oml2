<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_wpv_kyv_pr">
  <title>gettimesteplist</title>
  <shortdesc>Returns a list of time steps associated with the given subcase, type, request, and
    component.</shortdesc>
  <prolog>
    <metadata>
      <category><keyword keyref="product"/> Language</category>
      <category>HW Readers Commands</category>
      <keywords>
        <indexterm>gettimesteplist</indexterm>
      </keywords>
    </metadata>
  </prolog>
  <refbody>
    <section>
      <title>Syntax</title>
      <p><synph>gettimesteplist<var>(filename, datatype, request, component)</var></synph></p>
      <p><synph>gettimesteplist<var>(filename, subcase, datatype, request, component)</var></synph></p>
	  <p><synph>R = gettimesteplist<var>(...)</var></synph></p>
      <?STOP?>
    </section>
    <section>
      <title><keyword keyref="inputs"/></title>
      <parml>
        <plentry>
          <pt><varname>filename</varname></pt>
          <pd>Path of file to be read.</pd>
          <pd>Type: <keyword keyref="string"/></pd>
        </plentry>
        <plentry>
          <pt><varname>subcase</varname></pt>
          <pd>Subcase name or index in the file to be read.</pd>
          <pd>Type: <keyword keyref="string_int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>datatype</varname></pt>
          <pd>Data type name or index in the file to be read.</pd>
          <pd>Type: <keyword keyref="string_int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>request</varname></pt>
          <pd>Request name or index in the file to be read.</pd>
          <pd>Type: <keyword keyref="string_int"/></pd>
        </plentry>
        <plentry>
          <pt><varname>component</varname></pt>
          <pd>Component name or index in the file to be read.</pd>
          <pd>Type: <keyword keyref="string_int"/></pd>
        </plentry>
      </parml>
      <?STOPINP?>
    </section>
    <section>
      <title><keyword keyref="outputs"/></title>
      <parml>
        <plentry>
          <pt><synph>R</synph></pt>
          <pd>Output is a 1xN matrix. N is the number of time steps.</pd>
        </plentry>
      </parml>
      <?STOPOUT?>
    </section>
    <section>
      <title>Example</title>
      <p>Basic <cmdname>gettimesteplist</cmdname> example without
        <varname>subcase</varname>:<codeblock>gettimesteplist('Path/To/File/ANGACC',1,1,1)</codeblock><codeblock>ans = [Matrix] 1 x 151
0  1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44  45  46  47  48  49  50  51  52  53  54  55  56  57  58  59  60  61  62  63  64  65  66  67  68  69  70  71  72  73  74  75  76  77  78  79  80  81  82  83  84  85  86  87  88  89  90  91  92  93  94  95  96  97  98  99  100  101  102  103  104  105  106  107  108  109  110  111  112  113  114  115  116  117  118  119  120  121  122  123  124  125  126  127  128  129  130  131  132  133  134  135  136  137  138  139  140  141  142  143  144  145  146  147  148  149  150</codeblock></p>
      <p>Basic <cmdname>gettimesteplist</cmdname> example with
        <varname>subcase</varname>:<codeblock>gettimesteplist('Path/To/File/control_arm_sol111.pch',1,2,1,2)</codeblock><codeblock>ans = [Matrix] 1 x 22
10.00000  10.25000  10.50000  10.75000  11.00000  11.25000  11.50000  11.75000  12.00000  12.25000  12.50000  12.75000  13.00000  13.25000  13.50000  13.75000  14.00000  14.25000  14.50000  14.75000  15.00000  15.25000</codeblock></p>
    </section>
    <section id="section_pp1_pfk_t3b">
      <title>Comments</title>
      <p>The function has two forms. Use the first form when there is zero or at most one
          <varname>subcase</varname>. Use the second form when there are multiple subcases.</p>
    </section>
    <section>
      <draft-comment author="denby">See Also: getsubcaselist, gettypelist, getreqlist, getcomplist</draft-comment>
    </section>
  </refbody>
</reference>
