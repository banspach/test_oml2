/**
* @file JupyterWrapper.cpp
* @date May 2019
* Copyright (C) 2019-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

// Begin defines/includes

#include "JupyterWrapper.h"
#include "SignalHandler.h"
#include "Runtime/BuiltInFuncsUtils.h"
#include "Runtime/Interpreter.h"
#include "Runtime/OutputFormat.h"
#include <cassert>
#include <sstream>

#ifdef OS_WIN
#   include <Windows.h>
#else
#   include <unistd.h>
#   include <glob.h>
#   include <dirent.h>
#   include <dlfcn.h>
#   include <sys/resource.h>
#   include <sys/times.h>
#endif

// function ptr def for license library
typedef int(*lic_auth_fptr) (const std::string& user, const std::string& pwd, int mode);
typedef int(*lic_act_fptr) (const std::string& user, const std::string& pwd, 
                            const bool& offline, const std::string& lic_file,
                            const std::string& lic_server);
typedef bool(*lic_ispro_fptr)();
typedef void(*lic_checkin_fptr)();
typedef bool (*lic_status_fptr)();

typedef bool (*lic_edition_fptr)(std::string& editionStr, bool appendEdition);

typedef void (*lic_get_string) (std::string&);
typedef bool (*is_offline_fptr)();
typedef bool (*lic_contribute)(bool activate);
typedef bool (*is_active_fptr)();
typedef void (*lic_get_proxysettings)(std::string&, int&, std::string&, bool&);
typedef bool (*lic_set_proxysettings)(const std::string&, const int&, const std::string&, const std::string&);
typedef bool (*lic_reset_proxysettings)();

typedef bool (*OmlCheckoutLibraryProc)(const std::string&, std::string&, std::string&);
typedef void (*OmlCheckinLibraryProc)(const std::string&);
typedef void (*OmlTestLibraryProc)(const std::string&, std::string&);
typedef void (*OmlFuncPtrVoid)();
typedef void (*OmlSetProductVersionProc)(const std::string&);

std::string JupyterWrapper::OmlVersion = "2022.3";
//------------------------------------------------------------------------------
//! Constructor
//! \param[in] interpretor
//------------------------------------------------------------------------------
JupyterWrapper::JupyterWrapper(Interpreter* interp)
    : m_interp(interp), m_licHandle(NULL)
{
    LoadLicenseLib();
    ConnectOmlSignalHandler();
}
//------------------------------------------------------------------------------
//! Destructor
//------------------------------------------------------------------------------
JupyterWrapper::~JupyterWrapper()
{
    BuiltInFuncsUtils::CloseOutputLog();
    DisconnectOmlSignalHandler();
#ifndef _DEBUG
    CheckInLicense();
    CheckinLibraries();
    DyFreeLibrary(m_licHandle);
    m_licHandle = nullptr;
#endif
}
//------------------------------------------------------------------------------
//! Slot called when printing result to console
//! \param[in] cur Currency to print
//------------------------------------------------------------------------------
void JupyterWrapper::HandleOnPrintResult(const Currency& cur)
{	
    assert(m_interp);
    const OutputFormat* format = m_interp->GetOutputFormat();
    PrintToStdout(cur.GetOutputString(format));
}

//------------------------------------------------------------------------------
//! Slot called when interpreter is deleted
//------------------------------------------------------------------------------
void JupyterWrapper::HandleOnSaveOnExit(int returnCode)
{
    DisconnectOmlSignalHandler();

    CheckInLicense();
    CheckinLibraries();

    delete m_interp;
    m_interp = NULL;
    exit(returnCode);
}

//------------------------------------------------------------------------------
//! Slot which displays a prompt and gets user input
//! \param[in]  prompt    Prompt to display to user
//! \param[in]  type      Type, if specified
//! \param[out] userInput Input from user
//------------------------------------------------------------------------------
void JupyterWrapper::HandleOnGetUserInput(const std::string& prompt,
                                                     const std::string& type,
													 std::string&       userInput)
{
    //fflush(stdout);
	std::cout << prompt + USER_INPUT_PROMPT << std::flush;
    //std::cin >> userInput;
    std::getline(std::cin, userInput);
    if (type == "s" || type == "S")
    {
        userInput += "'";
        userInput.insert(0, "'");
    }
}

//------------------------------------------------------------------------------
//! Prints message at standard o/p
//! \param[in] msg Message to print
//------------------------------------------------------------------------------
void JupyterWrapper::PrintToStdout(const std::string& msg)
{
    std::cout << msg << std::endl;
    BuiltInFuncsUtils::SaveToOutputLog(msg + '\n');
    //fflush(stdout);
}

//------------------------------------------------------------------------------
//! Connects signals
//------------------------------------------------------------------------------
void JupyterWrapper::ConnectOmlSignalHandler()
{
    assert(m_interp);

    // Set the signal handler
    SignalHandler* handler = new SignalHandler;
    assert(handler);
    handler->SetWrapper(this);
    m_interp->SetSignalHandler(handler);
}
//------------------------------------------------------------------------------
//! Disconnect signal handler
//------------------------------------------------------------------------------
void JupyterWrapper::DisconnectOmlSignalHandler()
{
    assert(m_interp);
    SignalHandlerBase* handler = m_interp->GetSignalHandler();
    if (!handler) return;

    delete handler;
    m_interp->SetSignalHandler(NULL);
}

//------------------------------------------------------------------------------------------------------------
//! Authorize Console application
//------------------------------------------------------------------------------------------------------------
bool JupyterWrapper::Authorize(const std::string& user,
    const std::string& pwd,
    std::string& errMsg)
{
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "Authorize");
        if (symbol)
        {
            lic_auth_fptr licfptr = (lic_auth_fptr)(symbol);
            int retValue = licfptr(user, pwd, 0);

            return (retValue == 0);
        }
    }
    return false;
}

//------------------------------------------------------------------------------------------------------------
//! Activate Jupyter application
//------------------------------------------------------------------------------------------------------------
bool JupyterWrapper::Activate(const std::string& user, const std::string& pwd, 
                                const bool& offline, const std::string& lic_file,
                                const std::string& lic_server)
{
    bool status = false;
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "Activate");
        if (symbol)
        {
            lic_act_fptr licfptr = (lic_act_fptr)(symbol);
            status = licfptr(user, pwd, offline, lic_file, lic_server);
        }
    }

    if (status)
    {
        OmlSetProductVersionProc funcptr =
            (OmlSetProductVersionProc)DyGetFunction(m_licHandle, "OmlSetProductVersion");
        if (funcptr)
        {
            funcptr(OmlVersion);
        }
    }

    return status;
}


//------------------------------------------------------------------------------
//! check if professional edition
//------------------------------------------------------------------------------
bool JupyterWrapper::IsProfessionalEdition()
{
    if (!m_licHandle) return false;

    void* symbol = DyGetFunction(m_licHandle, "IsProEdition");
    if (!symbol) return false;

    lic_ispro_fptr fptr = (lic_ispro_fptr)(symbol);
    return fptr();
}

//------------------------------------------------------------------------------
//! Check in product license
//------------------------------------------------------------------------------
void JupyterWrapper::CheckInLicense()
{
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "CheckInLic");
        if (symbol)
        {
            lic_checkin_fptr fptr = (lic_checkin_fptr)(symbol);
            fptr();
        }
    }
}

bool JupyterWrapper::CheckLicStatus()
{
    bool status = false;
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "CheckLicStatus");
        if (symbol)
        {
            lic_status_fptr fptr = (lic_status_fptr)(symbol);
            status = fptr();
        }
    }
    return status;
}

std::string JupyterWrapper::EditionString(bool appendEdition)
{
    std::string edition;
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "EditionString");
        if (symbol)
        {
            lic_edition_fptr fptr = (lic_edition_fptr)(symbol);
            fptr(edition, appendEdition);
        }
    }
    return edition;
}

//------------------------------------------------------------------------------
//! Load license library
//------------------------------------------------------------------------------
void JupyterWrapper::LoadLicenseLib()
{
    //load license library
    std::string importDll = "hwcomposelic";
#ifndef OS_WIN
    importDll = "lib" + importDll + ".so";
#endif
    m_licHandle = DyLoadLibrary(importDll.c_str());
}

//------------------------------------------------------------------------------
//! Dynamically loads a library and returns handle to it
//------------------------------------------------------------------------------
void* JupyterWrapper::DyLoadLibrary(const std::string& name)
{
#ifdef OS_WIN
    return (void*)LoadLibrary(name.c_str());
#else  // OS_UNIX
    return dlopen(name.c_str(), RTLD_LAZY);
#endif  // OS_UNIX
}

//------------------------------------------------------------------------------
//! Returns function pointer from a dynamically loaded library
//------------------------------------------------------------------------------
void* JupyterWrapper::DyGetFunction(void* handle, const std::string& name)
{
    if (!handle || name.empty())
        return nullptr;

#ifdef OS_WIN
    return GetProcAddress((HINSTANCE)handle, name.c_str());
#else  
    return dlsym(handle, name.c_str());
#endif
}

//------------------------------------------------------------------------------
//! Release dynamically loaded library
//------------------------------------------------------------------------------
void JupyterWrapper::DyFreeLibrary(void* handle)
{
    if (!handle)
        return;

#ifdef OS_WIN
    FreeLibrary((HINSTANCE)handle);
#else  // OS_UNIX
    dlclose(handle);
#endif  // OS_UNIX
}

//------------------------------------------------------------------------------
// Gets argv at the given index
//------------------------------------------------------------------------------
std::string JupyterWrapper::GetArgv(int idx) const
{
	if (idx < 0 || _argv.empty() || idx >= static_cast<int>(_argv.size()))
		return "";

	return _argv[idx];
}
//------------------------------------------------------------------------------
// Gets argc
//------------------------------------------------------------------------------
int JupyterWrapper::GetArgc() const
{
	return (_argv.empty()) ? 0 : static_cast<int>(_argv.size());
}


//------------------------------------------------------------------------------
// Gets license log information
//------------------------------------------------------------------------------
void JupyterWrapper::GetLicenseLog(std::string& log)
{
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "GetLicenseLog");
        if (symbol)
        {
            lic_get_string liclogfptr = (lic_get_string)(symbol);
            liclogfptr(log);
        }
    }
}

//!
//! Returns if license server mode is offline or not
//!
bool JupyterWrapper::IsOffline()
{
    bool is_offline = false;
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "IsOffline");
        if (symbol)
        {
            is_offline_fptr isofflinefptr = (is_offline_fptr)(symbol);
            is_offline = isofflinefptr();
        }
    }
    return is_offline;

}

//------------------------------------------------------------------------------
// Gets license Server
//------------------------------------------------------------------------------
void JupyterWrapper::GetLicenseServer(std::string& server)
{
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "GetLicenseServer");
        if (symbol)
        {
            lic_get_string licserverfptr = (lic_get_string)(symbol);
            licserverfptr(server);
        }
    }
}

//------------------------------------------------------------------------------
// Gets license file
//------------------------------------------------------------------------------
void JupyterWrapper::GetLicenseFile(std::string& file)
{
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "GetLicenseFile");
        if (symbol)
        {
            lic_get_string licfilefptr = (lic_get_string)(symbol);
            licfilefptr(file);
        }
    }
}

//------------------------------------------------------------------------------
// Contributes Compose features to license
//------------------------------------------------------------------------------
bool JupyterWrapper::LicContribute(const bool activate)
{
    bool status = false;
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "Contribute");
        if (symbol)
        {
            lic_contribute liccontributefptr = (lic_contribute)(symbol);
            status = liccontributefptr(activate);
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Check if is activated with required features
//------------------------------------------------------------------------------
bool JupyterWrapper::IsLicActive()
{
    bool status = false;
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "IsLicActive");
        if (symbol)
        {
            is_active_fptr islicactive = (is_active_fptr)(symbol);
            status = islicactive();
        }
    }
    return status;
}


void JupyterWrapper::GetProxySettings(std::string& host, int& port, std::string& user, bool& hasproxy)
{
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "GetLicProxySettings");
        if (symbol)
        {
            lic_get_proxysettings getproxysettings = (lic_get_proxysettings)(symbol);
            getproxysettings(host, port, user, hasproxy);
        }
    }
}

bool JupyterWrapper::SetProxySettings(const std::string& host, const int& port, const std::string& user, const std::string& pwd)
{
    bool status = false;
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "SetLicProxySettings");
        if (symbol)
        {
            lic_set_proxysettings setproxysettings = (lic_set_proxysettings)(symbol);
            status = setproxysettings(host, port, user, pwd);
        }
    }
    return status;
}

bool JupyterWrapper::ResetProxySettings()
{
    bool status = false;
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "ResetLicProxySettings");
        if (symbol)
        {
            lic_reset_proxysettings resetproxysettings = (lic_reset_proxysettings)(symbol);
            status = resetproxysettings();
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Returns true after checking in oml libary license license 
//------------------------------------------------------------------------------
bool JupyterWrapper::CheckoutLibrary(const std::string& libinfo,
    std::string& out,
    std::string& err)
{
    bool result = false;
    if (m_licHandle)
    {
        void* symbol = DyGetFunction(m_licHandle, "OmlCheckoutLibrary");
        assert(symbol);
        if (symbol)
        {
            OmlCheckoutLibraryProc fptr = (OmlCheckoutLibraryProc)(symbol);
            result = fptr(libinfo, out, err);
        }
    }
    return result;
}
//------------------------------------------------------------------------------
// Helper method to check in oml library license
//------------------------------------------------------------------------------
void JupyterWrapper::CheckinLibrary(const std::string& in)
{
    if (!m_licHandle)
    {
        return;
    }
    void* symbol = DyGetFunction(m_licHandle, "OmlCheckinLibrary");
    assert(symbol);

    if (symbol)
    {
        OmlCheckinLibraryProc fptr = (OmlCheckinLibraryProc)(symbol);
        fptr(in);
    }
}
//------------------------------------------------------------------------------
// Checks in all library licenses
//------------------------------------------------------------------------------
void JupyterWrapper::CheckinLibraries()
{
    if (!m_licHandle)
    {
        return;
    }
    void* symbol = DyGetFunction(m_licHandle, "OmlCheckinLibraries");
    assert(symbol);

    if (symbol)
    {
        OmlFuncPtrVoid fptr = (OmlFuncPtrVoid)(symbol);
        fptr();
    }
}
//------------------------------------------------------------------------------
// Helper method to test oml library license
//------------------------------------------------------------------------------
std::string JupyterWrapper::TestLibrary(const std::string& in)
{
    if (!m_licHandle)
    {
        return "";
    }
    void* symbol = DyGetFunction(m_licHandle, "OmlTestLibrary");
    assert(symbol);

    if (symbol)
    {
        OmlTestLibraryProc fptr = (OmlTestLibraryProc)(symbol);
        std::string result;
        fptr(in, result);
        return result;
    }
    return "";
}