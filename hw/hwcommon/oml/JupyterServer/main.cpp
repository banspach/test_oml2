/**
* @file main.cpp
* @date May, 2019
* Copyright (c) 2019-2021 Altair Engineering Inc. All Rights Reserved. Contains trade 
* secrets of Altair Engineering Inc. Copyright notice does not imply publication. 
* Decompilation or disassembly of this software is strictly prohibited.
*/
#include <iostream>
#include "Interpreter.h"
#include "JupyterWrapper.h"
#include "JupyterOmlFuncs.h"

#define READY_PROMPT std::cout << PROMPT << std::flush;
#define MIN_PORT_NUM 1
#define MAX_PORT_NUM 65535
using namespace std;
Interpreter* interp = NULL;
JupyterWrapper* wrapper = NULL;

void SetUserInterrupt()
{
    if (interp)
        interp->TriggerInterrupt();
}

void CheckInLicense()
{
    wrapper->CheckInLicense();
    wrapper->CheckinLibraries();
}

#if defined(OS_WIN)
#include <Windows.h>
#include <fcntl.h>
#include <io.h>

BOOL OnControlKeyDown(DWORD controlType)
{
    if (controlType == CTRL_C_EVENT)
    {
        SetUserInterrupt();
    }
    else if (controlType == CTRL_CLOSE_EVENT ||
             controlType == CTRL_BREAK_EVENT)
    {
        CheckInLicense();
    }
    return true;
}
#else
#   include <signal.h>
#   include <time.h>
#   include <stdio.h>
#   include <unistd.h>
// Called when control key is down
// \param controlType Control type
//------------------------------------------------------------------------------
// Called when control key is down
//------------------------------------------------------------------------------
void OnControlKeyDown(int controlType)
{

    // This routine may be called in a separate thread. So, just set interrupt
    // flag instead of calling exit handler which will destroy interpreter
    if (controlType == SIGINT)
    {
        SetUserInterrupt();
    }
    else if (controlType == SIGHUP)
    {
        CheckInLicense();
        // Force exit. It may not exit via SIGHUP/KILL in some systems.
        exit(EXIT_SUCCESS);
    }
}
#endif

//------------------------------------------------------------------------------
// Helper method to set environment variables
//------------------------------------------------------------------------------
void SetEnvVariable(const std::string& name, const std::string& val)
{
    if (name.empty())
    {
        return;
    }
#ifdef OS_WIN
    _putenv_s(name.c_str(), val.c_str());
#else
    setenv(name.c_str(), val.c_str(), 1);
#endif
}

/*int do_license_check()
{
    std::string user_name = "";
    std::string passwd = "";
    std::string err_msg = "";
    bool success = false;
    READY_PROMPT
    std::getline(std::cin, user_name);
    READY_PROMPT
    std::getline(std::cin, passwd);
    success = wrapper->Authorize(user_name, passwd, err_msg);
    std::cout << success << std::endl;
    return success;
}*/


bool activate_license(bool readinputs=true)
{
    std::string user_name, pwd, offline, lic_file, lic_server;
    bool is_offline = false;
    bool success = false;
    if (readinputs)
    {
        READY_PROMPT
        std::getline(std::cin, user_name);
        READY_PROMPT
        std::getline(std::cin, pwd);
        READY_PROMPT
        std::getline(std::cin, offline);

        if ("offline" == offline)
        {
            is_offline = true;
        }

        if (wrapper->IsProfessionalEdition())
        {
            READY_PROMPT
            std::getline(std::cin, lic_file);
            READY_PROMPT
            std::getline(std::cin, lic_server);
        }
    }
    else
    {
       //Todo: EarliestExpirationDays
        wrapper->LicContribute(false);
        is_offline = wrapper->IsOffline();
        if (wrapper->IsProfessionalEdition())
        {
            wrapper->GetLicenseFile(lic_file);
            wrapper->GetLicenseServer(lic_server);
        }
    }     
    success = wrapper->Activate(user_name, pwd, is_offline, lic_file, lic_server);
    std::cout << success << std::endl;
    return success;

}

void handle_non_oml_jupyter_command()
{
    std::string input;
    READY_PROMPT
    std::getline(std::cin, input);
    if ("oml_jupyter_license_setup" == input)
    {
        activate_license();
    }
    else if ("get_oml_jupyter_license_log" == input)
    {
        std::string log;
        wrapper->GetLicenseLog(log);
        std::cout << log << std::endl;
    }
    else if ("get_oml_jupyter_license_proxy" == input)
    {
        std::string host, user, output;
        bool hasproxy = false;
        int port = 8080;
        wrapper->GetProxySettings(host, port, user, hasproxy);
        output += "{";
        output += "\"host\":\"" + host + "\"";
        output += ",\"port\":\"" + std::to_string(port) + "\"";
        output += ",\"user\":\"" + user + "\"";
        output += ",\"hasproxy\":\""+ std::to_string(hasproxy) +"\"";
        output += "}";
        std::cout << output << std::endl;
    }
    else if ("set_oml_jupyter_license_proxy" == input)
    {
        std::string host, user, pwd, msg, input, output = "{";
        int port = -1;
        bool status = false, setproxy = false;

        READY_PROMPT
        std::getline(std::cin, input);
        if (!input.empty())
            setproxy = std::stoi(input);
        
        if (setproxy)
        {
            input.clear();
            READY_PROMPT
            std::getline(std::cin, host);
            READY_PROMPT
            std::getline(std::cin, input);
            if (!input.empty())
            {
                port = std::stoi(input);
            }
            READY_PROMPT
            std::getline(std::cin, user);
            READY_PROMPT
            std::getline(std::cin, pwd);

            if (host.empty())
            {
                msg = "Proxy HOST should not be empty.";
            }
            else if (port < MIN_PORT_NUM || port > MAX_PORT_NUM)
            {
                msg = "Invalid Proxy PORT.";
            }
            else if (user.empty() && !pwd.empty())
            {
                msg = "Proxy USER is not set.";
            }
            else
            {
                status = wrapper->SetProxySettings(host, port, user, pwd);
            }
        }
        else
        {
            status = wrapper->ResetProxySettings();
        }

        if (!status && msg.empty())
        {
            msg  = "Could not update proxy settings";
        }

        output += "\"status\":\"" + std::to_string(status) +"\"";
        output += ",\"msg\":\"" + msg + "\"";
        output += "}";
        std::cout << output << std::endl;
    }
    else if ("get_oml_jupyter_license_server" == input)
    {
        std::string server;
        wrapper->GetLicenseServer(server);
        std::cout << server << std::endl;
    }
    else if ("get_oml_jupyter_license_file" == input)
    {
        std::string file;
        wrapper->GetLicenseFile(file);
        std::cout << file << std::endl;

    }
    else if ("get_oml_jupyter_license_status" == input)
    {
        std::cout << wrapper->IsLicActive() << std::endl;
    }
    else if ("get_oml_jupyter_license_mode" == input)
    {
        std::cout << wrapper->IsOffline() << std::endl;
    }
    else if ("get_oml_jupyter_license_info" == input)
    {
        std::string server;
        wrapper->GetLicenseServer(server);
        std::string file;
        wrapper->GetLicenseFile(file);

        std::string output = "{";
        output += "\"status\":";
        output += "\"" + std::to_string(wrapper->IsLicActive()) + "\"";
        output += ",\"mode\":";
        output += "\""+ std::to_string(wrapper->IsOffline()) +"\"";

        if (wrapper->IsProfessionalEdition())
        {
            output += ",\"file\":\"";
            output += file + "\"";
            output += ",\"server\":\"";
            output += server + "\"";
        }
        output += "}";
        std::cout << output << std::endl;

    }
    else if ("open_oml_jupyter_notebook" == input)
    {
        READY_PROMPT
        std::string notebook_file;
        std::getline(std::cin, notebook_file);
#ifdef OS_WIN
        const char* user_home = getenv("USERPROFILE");
#else
        const char* user_home = getenv("HOME");
#endif
        std::string user_home_str(user_home);
        std::string temp("/Documents/ComposeNotebooks");
        user_home_str = user_home_str + temp;

        std::replace(user_home_str.begin(), user_home_str.end(), '\\', '/');
        std::replace(notebook_file.begin(), notebook_file.end(), '\\', '/');

        std::size_t found = notebook_file.find(user_home_str);
        std::string output_string;

        if (0 == found && notebook_file.length() > user_home_str.length())
            output_string = notebook_file.substr(user_home_str.length() + 1);
        else
            output_string = notebook_file;

        std::cout << output_string << std::endl;
    }
}

int main(int argc, char* argv[])
{
    interp = new Interpreter();
    InitEnvVariables();
    const char* unity_root = getenv("HW_UNITY_ROOTDIR");

    if (!unity_root)
    {
        READY_PROMPT
        std::cerr << "HW_UNITY_ROOTDIR must be set." << std::endl;        
        exit(EXIT_FAILURE);
    }

    wrapper = new JupyterWrapper(interp);
    std::string err_msg = "";
    bool skip_remaining = false;
    if (!activate_license(false))
    {
        std::cerr << "ALTAIR LICENSE ERROR." << std::endl;
        READY_PROMPT
		std::string input;
        std::string script;
		while (std::getline(std::cin, input))
		{
			if ("oml_jupyter_code_cell_begin " == input)
			{
                READY_PROMPT
				continue;
			}
            else if ("oml_jupyter_control_cmd" == input)
            {
                input.clear();
                handle_non_oml_jupyter_command();
                READY_PROMPT
                continue;
            }            
            else if ("oml_jupyter_code_cell_end" != input)
            {
                if (script == "")
                    script = input;
                else
                    script = script + "\n" + input;

                READY_PROMPT
                continue;
            }
            else
            {
                script.clear();
                input.clear();
            }

            if (!wrapper->IsLicActive())
            {
                std::cerr << "ALTAIR LICENSE ERROR." << std::endl;
                READY_PROMPT
            }
            else
            {
                break;
            }
		} 
    }
	
    //If license check is not sucess it should not reach here, if it reaches, safe to exit the application
    //Don't delete this safe guard
    if (!wrapper->IsLicActive())
    {
        exit(EXIT_FAILURE);
    }
	//Best location for following is, pass arguments from 
	//unity\scripts\python\hwx\compose\composenotebook\modules\oml_kernel\omlkernel.py
	//Adding arguments here to avoid hardcoding OML version in file omlkernel.py
	std::vector<std::string> argsv;
	argsv.reserve(5);
	argsv.push_back(std::string(argv[0]));
	argsv.push_back(std::string("-c"));
	argsv.push_back(std::string("Compose"));	
	argsv.push_back(std::string("-v"));
	std::string comp_versoin = "Compose";
	comp_versoin = comp_versoin + JupyterWrapper::OmlVersion;
	argsv.push_back(comp_versoin);
	wrapper->SetArgv(argsv);

    RegisterBuiltInFuncs();

    std::string unity_root_str = unity_root;
    std::string init_script    = unity_root_str + "/plugins/compose/mathtoolbox/init_notebook.oml";
    interp->DoFile(init_script);
    interp->SetApplicationDir("");
    interp->DoString("librarymanager('launch')");

#ifdef OS_WIN
    //On windows Py_Initialize changes the console mode from O_TEXT to O_BINARY
    //refer https://docs.python.org/3/c-api/init.html
    //refer https://bugs.python.org/issue16587
    //To force python initialisation and resetting the console mode to O_TEXT    
    interp->DoString("evalpythonscript('')");    
    _setmode(_fileno(stdin), O_TEXT);
    _setmode(_fileno(stdout), O_TEXT);
    _setmode(_fileno(stderr), O_TEXT);
    SetConsoleCtrlHandler(NULL, FALSE);
    AttachConsole(ATTACH_PARENT_PROCESS);
    SetConsoleCtrlHandler((PHANDLER_ROUTINE)OnControlKeyDown, TRUE);
#else
    signal(SIGINT,OnControlKeyDown);
    signal(SIGHUP,OnControlKeyDown);
#endif   
    READY_PROMPT
    size_t lic_check_counter = 1;
    try
    {
        while (1)
        {
            std::string input;
            std::string script;
            while (std::getline(std::cin, input))
            {
                if ( "oml_jupyter_code_cell_begin" == input)
                {
                    READY_PROMPT
                    continue;
                }
                else if ("oml_jupyter_control_cmd" == input)
                {
                    input.clear();
                    handle_non_oml_jupyter_command();
                    skip_remaining = true;
                    READY_PROMPT
                    continue;
                }
                else if ("oml_jupyter_code_cell_end" != input)
                {
                    if (script == "")
                        script = input;
                    else
                        script = script + "\n" + input;
                }
                else
                {
                    try 
                    {
#ifndef _DEBUG
                        if (lic_check_counter >= 50)
                        {
                            bool status = wrapper->CheckLicStatus();
                            if (!status)
                            {
                                if (lic_check_counter == 50)
                                {
                                    std::cerr << "The connection to the license server is lost. Pls reconnect to the license server, otherwise application will be terminated." << std::endl;
                                    script.clear();
                                    input.clear();
                                    ++lic_check_counter;
                                    READY_PROMPT
                                    continue;
                                }
                                else
                                {
                                    exit(EXIT_FAILURE);
                                }                                    
                            }
                            else
                            {
                                lic_check_counter = 0;
                            }
                        }
                        ++lic_check_counter;
#endif
                        if (!wrapper->IsLicActive())
                        {
                            std::cerr << "ALTAIR LICENSE ERROR." << std::endl;
                        }
                        else
                        {
                            if (!skip_remaining)
                            {
                                interp->DoString(script);
                            }
                            else
                            {
                                skip_remaining = false;
                            }
                        }
                    }
                    catch (...)
                    {
                        script.clear();
                        input.clear();
                    }
                    script.clear();
                    input.clear();
                }
                READY_PROMPT
            }
        }
    }
    catch (...)
    {
        CheckInLicense();
        std::cerr << "Exception occured. Exiting the application." << std::flush;
    }

    return 0;
}
