/**
* @file JupyterWrapper.h
* @date May 2019
* Copyright (C) 2019-2021 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#ifndef __JUPYTER_WRAPPER__
#define __JUPYTER_WRAPPER__

#define PROMPT "_OMLJUPYTERPROMPT>>>>"
#define USER_INPUT_PROMPT "_OMLJUPYTERINPUTPROMPT<<<<"

class Interpreter;
#include "Currency.h"
// End defines/includes

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//! Class for handling interpretor signals and methods
//------------------------------------------------------------------------------
class JupyterWrapper
{
public:
    //! Constructor
    //! \param[in] interpretor
    JupyterWrapper(Interpreter* interp);

    //! Destructor
    ~JupyterWrapper();

    //! Prints currency to console
    //! \param[in] cur Currency to print
	void HandleOnPrintResult( const Currency& cur);

    //! Displays a prompt and gets user input
	//! \param[in]  prompt    Prompt to display to user
    //! \param[in]  type      Type, if specified
	//! \param[out] userInput Input from user
    void HandleOnGetUserInput( const std::string& prompt,
                               const std::string& type,
		                       std::string&       userInput);
    //! Initiates a user defined pause
    //! \param[in] msg  User message to display
    //! \param[in] wait True if waiting for a keystroke input from user
	void HandleOnPauseStart( const std::string& msg, 
                             bool               wait);

    //! Handles application exit
    //! \param returnCode Code to exit the application with
    void HandleOnSaveOnExit(int returnCode = EXIT_SUCCESS);

    //! Dynamically loads a library and returns handle to it
    //! \param name Library name
    static void* DyLoadLibrary(const std::string& name);
    //! Returns function pointer from a dynamically loaded library
    //! \param handle Handle to dynamically loaded library
    //! \param name   Function name
    //!
    static void* DyGetFunction(void*              handle,
                               const std::string& name);
    //!
    //! Releases dynamically loaded library
    //! \param handle Handle to dynamically loaded library
    //!
    static void DyFreeLibrary(void* handle);

    //! Authorize Console application
    bool Authorize(const std::string& user,
        const std::string& pwd,
        std::string& errMsg);

    //------------------------------------------------------------------------------------------------------------
    //! Activate Jupyter application
    //------------------------------------------------------------------------------------------------------------
    bool Activate(const std::string& user, const std::string& pwd,
                const bool& offline, const std::string& lic_file,
                const std::string& lic_server);

    //! Check if professional edition
    bool IsProfessionalEdition();

    //! Check in product license
    void CheckInLicense();
    //!
    //! check the license status
    //!
    bool CheckLicStatus();
    //!
    //! Get license edition string
    //! \param appendEdition  append "Edition"
    //!
    std::string EditionString(bool appendEdition = false);

	//!
	//! Gets argc
	//!
	int GetArgc() const;
	//!
	//! Gets argv at the given index
	//! \param idx 1-based index
	//!
	std::string GetArgv(int idx) const;
	//!
	//! Sets argv
	//! \param args Args
	//!
	void SetArgv(const std::vector<std::string>& args) { _argv = args; }
    //! Retrieves license log information
    //! \param[in]  log  returns license log information
    void GetLicenseLog(std::string& log);
    //!
    //! Returns if license server mode is offline or not
    //!
    bool IsOffline();
    //! Retrieves license server information
    //! \param[in]  server returns license server information
    void GetLicenseServer(std::string& server);
    //! Retrieves license file information
    //! \param[in]  file  returns license file information
    void GetLicenseFile(std::string& file);
    //! Contributes Compose features to license
    //! \param[in]  activate  decides to activate license or not
    bool LicContribute(const bool activate);
    //! Check if is activated with required features
    bool IsLicActive();
    //! Get Proxy settings previously set by SetProxySettings() in this session
    //! @param host - proxy host
    //! @param port - proxy server port
    //! @param user - proxy server user name
    //! @param hasproxy- is true if Proxy settings are being used for authorization ese false
    void GetProxySettings(std::string&, int&, std::string&, bool&);
    //! Set Proxy settings to be used for authorization
    //! @param host - proxy host
    //! @param port - proxy server port
    //! @param user - proxy server user name
    //! @param pwd  - proxy server user password
    //! Return true if success, otherwise false
    bool SetProxySettings(const std::string&, const int&, const std::string&, const std::string&);
    //! Reset Proxy settings
    bool ResetProxySettings();

    //!
    //! Returns true after checking library license
    //! \param Library id
    //! \param Result
    //! \param Error
    //!
    bool CheckoutLibrary(const std::string&, std::string&, std::string&);
    //!
    //! Checks in library license
    //! \param Library id
    //!
    void CheckinLibrary(const std::string&);
    //!
    //! Checks in all library licenses
    //!
    void CheckinLibraries();
    //!
    //! Gets encoded result after testing oml library license
    //! \param Library info
    //!
    std::string TestLibrary(const std::string&);

    static std::string OmlVersion;                //!< Product version
private:

    //! Private default constructor
    JupyterWrapper() {}

    //! Prints to stdout
    //! \param[in] msg Message to print
    void PrintToStdout( const std::string& msg);
  
    //! Load license library
    void LoadLicenseLib();

    Interpreter* m_interp;  //! Interpreter
    void* m_licHandle;      //! Handle to license library

	std::vector<std::string> _argv; //!< Arg v

protected:

    //! Connects oml signal handler
    void ConnectOmlSignalHandler();
    //! Disconnect oml signal handler
    void DisconnectOmlSignalHandler();
};


#endif

// End of file:
