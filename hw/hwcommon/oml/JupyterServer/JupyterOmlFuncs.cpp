/**
* @file JupyterOmlFuncs.cpp
* @date January 2020
* Copyright (C) 2020-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/


// Begin defines/includes

#include "../Runtime/BuiltInFuncsCore.h"
#include "../Runtime/BuiltInFuncsUtils.h"
#include "../Runtime/CurrencyDisplay.h"
#include "../Runtime/Interpreter.h"
#include "../Runtime/StructData.h"
#include "../Runtime/ErrorInfo.h"
#include "../Runtime/OML_Error.h"

#include "JupyterOmlFuncs.h"
#include "JupyterWrapper.h"

#include <cassert>

extern JupyterWrapper* wrapper;
extern Interpreter* interp;

// End defines/includes

//------------------------------------------------------------------------------
// Initializes environment variables
//------------------------------------------------------------------------------
void InitEnvVariables()
{
    BuiltInFuncsUtils utils;
    std::string envvar("$HW_UNITY_ROOTDIR");
    const char* unity_root = getenv("HW_UNITY_ROOTDIR");

    if (getenv("OML_APPDIR"))
    {
        std::string path(getenv("OML_APPDIR"));
        size_t pos = path.find(envvar);

        if (pos != std::string::npos)
        {
            path.replace(pos, envvar.length(), unity_root);
            path = utils.StripMultipleSlashesAndNormalize(path);
            utils.SetEnvVariable("OML_APPDIR", path);
        }
        interp->SetApplicationDir(path);
    }

    if (getenv("OML_HELP"))
    {
        std::string path(getenv("OML_HELP"));
        size_t pos = path.find(envvar);

        if (pos != std::string::npos)
        {
            path.replace(pos, envvar.length(), unity_root);
            path = utils.StripMultipleSlashesAndNormalize(path);
            utils.SetEnvVariable("OML_HELP", path);
        }
    }
}

//------------------------------------------------------------------------------
// Gets library authorization [getlibraryauthorization]
//------------------------------------------------------------------------------
bool OmlCheckLicense(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
#ifdef _DEBUG
    outputs.push_back("");
    return true;
#endif

    try
    {
        if (inputs.empty())
        {
            throw OML_Error(OML_ERR_NUMARGIN);
        }

        if (!inputs[0].IsString())
        {
            throw OML_Error(OML_ERR_STRING, 1);
        }

        std::string libinfo(inputs[0].StringVal());
        if (libinfo.empty())
        {
            throw OML_Error(OML_ERR_NONEMPTY_STR, 1);
        }

        std::string out;
        assert(wrapper);
        if (!wrapper)
        {
            throw OML_Error(OML_ERR_INTERNAL);
        }

        std::string err;
        if (!wrapper->CheckoutLibrary(libinfo, out, err))
        {
            if (!err.empty() && eval.GetVerbose() > 0)
            {
                BuiltInFuncsUtils::SetWarning(eval, err);
            }
            throw OML_Error(OML_ERR_AUTHENTICATE, 1);
        }

        outputs.push_back(out);
    }
    catch (const OML_Error & e)
    {
        throw OML_Error(e.GetErrorMessage(), false);
    }

    return true;
}
//------------------------------------------------------------------------------
// Returns libary authorization [returnlibraryauthorization]
//------------------------------------------------------------------------------
bool OmlCheckinLicense(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
#ifdef _DEBUG
    return true;
#endif

    try
    {
        if (inputs.empty())
        {
            throw OML_Error(OML_ERR_NUMARGIN);
        }

        if (!inputs[0].IsString())
        {
            throw OML_Error(OML_ERR_STRING, 1);
        }

        std::string id(inputs[0].StringVal());
        if (id.empty())
        {
            throw OML_Error(OML_ERR_NONEMPTY_STR, 1);
        }

        assert(wrapper);
        if (!wrapper)
        {
            throw OML_Error(OML_ERR_INTERNAL);
        }

        wrapper->CheckinLibrary(id);
    }
    catch (const OML_Error & e)
    {
        throw OML_Error(e.GetErrorMessage(), false);
    }

    return true;
}
//------------------------------------------------------------------------------
// Tests library authorization [testlibraryauthorization]
//------------------------------------------------------------------------------
bool OmlTestLicense(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
#ifdef _DEBUG
    return true;
#endif

    try
    {
        if (inputs.empty())
        {
            throw OML_Error(OML_ERR_NUMARGIN);
        }

        if (!inputs[0].IsString())
        {
            throw OML_Error(OML_ERR_STRING, 1);
        }
        std::string id(inputs[0].StringVal());
        if (id.empty())
        {
            throw OML_Error(OML_ERR_NONEMPTY_STR, 1);
        }

        assert(wrapper);
        std::string info(wrapper->TestLibrary(id));
        if (info.empty())
        {
            throw OML_Error(OML_ERR_AUTHENTICATE, 1);
        }

        if (eval.GetVerbose() > 0)
        {
            eval.PrintResult("Testing library [" + info + "]");
        }
    }
    catch (const OML_Error & e)
    {
        throw OML_Error(e.GetErrorMessage(), false);
    }

    return true;
}


//------------------------------------------------------------------------------
// Registers built in functions
//------------------------------------------------------------------------------
void RegisterBuiltInFuncs()
{
    assert(interp);

    interp->RegisterBuiltInFunction("version", OmlVersion,
        FunctionMetaData(0, 1, "CoreMinimalInterpreter"));
	if (wrapper)
	{
		// Override getargc/getargv only if there is a wrapper
		interp->RegisterBuiltInFunction("getargc", OmlGetArgC,
			FunctionMetaData(1, 0, "CoreMinimalInterpreter"));
		interp->RegisterBuiltInFunction("getargv", OmlGetArgV,
			FunctionMetaData(1, 1, "CoreMinimalInterpreter"));

        if (wrapper->IsProfessionalEdition())
        {
            // Register and lock library authorization functions
            interp->RegisterBuiltInFunction("getlibraryauthorization",
                OmlCheckLicense, FunctionMetaData(1, 1, "Core"));
            interp->RegisterBuiltInFunction("returnlibraryauthorization",
                OmlCheckinLicense, FunctionMetaData(1, 0, "Core"));
            interp->RegisterBuiltInFunction("testlibraryauthorization",
                OmlTestLicense, FunctionMetaData(1, 0, "Core"));

            interp->LockBuiltInFunction("getlibraryauthorization");
            interp->LockBuiltInFunction("returnlibraryauthorization");
            interp->LockBuiltInFunction("testlibraryauthorization");
        }
	}

    //Dummy functions for jupyter notebook
    interp->RegisterBuiltInFunction("clc", DummyVoidFunc,
        FunctionMetaData(0, 1, "CoreMinimalInterpreter"));
    interp->RegisterBuiltInFunction("edit", DummyVoidFunc,
        FunctionMetaData(0, 1, "Gui"));
    interp->RegisterBuiltInFunction("errordlg", DummyIntFunc,
        FunctionMetaData(-2, -1, "Gui"));
    interp->RegisterBuiltInFunction("inputdlg", DummyCellFunc,
        FunctionMetaData(-4, 1, "Gui"));
    interp->RegisterBuiltInFunction("listdlg", DummyMatrixFunc,
        FunctionMetaData(-16, -2, "Gui"));
    interp->RegisterBuiltInFunction("msgbox", DummyIntFunc,
        FunctionMetaData(-2, -1, "Gui"));
    interp->RegisterBuiltInFunction("questdlg", DummyStringFunc,
        FunctionMetaData(-6, 1, "Gui"));
    interp->RegisterBuiltInFunction("uigetdir", DummyStringFunc,
        FunctionMetaData(-2, 1, "Gui"));
    interp->RegisterBuiltInFunction("uigetfile", DummyUiGetFile,
        FunctionMetaData(-3, -3, "Gui"));
    interp->RegisterBuiltInFunction("uiputfile", DummyStringFunc,
        FunctionMetaData(-3, -3, "Gui"));
    interp->RegisterBuiltInFunction("warndlg", DummyIntFunc,
        FunctionMetaData(-2, -1, "Gui"));
    interp->RegisterBuiltInFunction("getnumofcmdinputs", DummyIntFunc,
        FunctionMetaData(0, 1, "CoreMinimalInterpreter"));
    interp->RegisterBuiltInFunction("getcmdinput", DummyStringFunc,
        FunctionMetaData(1, 1, "CoreMinimalInterpreter"));
    interp->RegisterBuiltInFunction("startupscript_path", OmlStartupscriptPath,
        FunctionMetaData(1, 0, "System"));
    interp->RegisterBuiltInFunction("startupscript_edit", DummyVoidFunc,
        FunctionMetaData(0, 0, "System"));

    // Lock api_utility functions
    interp->LockBuiltInFunction("clearenvvalue");
    interp->LockBuiltInFunction("cloneenv");
    interp->LockBuiltInFunction("getbaseenv");
    interp->LockBuiltInFunction("getcurrentenv");
    interp->LockBuiltInFunction("getenvvalue");
    interp->LockBuiltInFunction("getnewenv");
    interp->LockBuiltInFunction("importenv");
    interp->LockBuiltInFunction("importenvin");
    interp->LockBuiltInFunction("setenvvalue");
}

//------------------------------------------------------------------------------
// Returns true if successful in getting the version string [version]
//------------------------------------------------------------------------------
bool OmlVersion(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
    std::string version(OML_PRODUCT);

    version += std::string(" ") + wrapper->EditionString(true);
    version += std::string(" ") + JupyterWrapper::OmlVersion;

    std::string vfile = eval.GetApplicationDir();
    if (!vfile.empty())
        vfile += "/";
    vfile += "config/compose/version.xml";

    version += BuiltInFuncsCore::GetBuildNumber(vfile);

    Currency out(version);
    out.DispOutput();

    outputs.push_back(out);
    return true;
}

//------------------------------------------------------------------------------
// Gets argc (getargc command)
//------------------------------------------------------------------------------
bool OmlGetArgC(EvaluatorInterface           eval,
	const std::vector<Currency>& inputs,
	std::vector<Currency>& outputs)
{
	assert(wrapper);

	outputs.push_back(wrapper->GetArgc());
	return true;
}

//------------------------------------------------------------------------------
// Gets argv at the given index (getargv command)
//------------------------------------------------------------------------------
bool OmlGetArgV(EvaluatorInterface           eval,
	const std::vector<Currency>& inputs,
	std::vector<Currency>& outputs)
{
	assert(wrapper);

	if (inputs.size() != 1)
	{
		throw OML_Error(OML_ERR_NUMARGIN);
	}
	if (!inputs[0].IsPositiveInteger())
	{
		throw OML_Error(OML_ERR_POSINTEGER, 1, OML_VAR_VALUE);
	}
	int idx = static_cast<int>(inputs[0].Scalar()) - 1;
	int argc = wrapper->GetArgc();
	if (idx > argc)
	{
		std::string msg("Error: invalid input in argument 1; value must be ");
		if (argc == 1)
		{
			msg += "1";
		}
		else
		{
			msg += "in the range of 1 and " + std::to_string(static_cast<long long>(argc));
		}
		throw OML_Error(msg);
	}
	outputs.push_back(wrapper->GetArgv(idx));
	return true;
}

//------------------------------------------------------------------------------
// Returns the name of the startup script file
//------------------------------------------------------------------------------
std::string GetStartupScriptPath()
{
    std::string envVar;
#ifdef OS_WIN
    envVar = "USERPROFILE";
#else
    envVar = "HOME";
#endif

    char* env = getenv(envVar.c_str());
    if (!env)
    {
        std::cout << "Warning: Environment variable [" << envVar
            << "] is not set" << std::endl;
        return "";
    }

    std::string startfile(env);
    startfile += "/.altair/Compose";
    startfile += JupyterWrapper::OmlVersion;
    startfile += "/hwx/Compose_startup.oml";

    startfile = BuiltInFuncsUtils::Normpath(startfile);
    return startfile;
}

//------------------------------------------------------------------------------
// Returns the path of the startup script file
//------------------------------------------------------------------------------
bool OmlStartupscriptPath(EvaluatorInterface eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
    outputs.push_back(GetStartupScriptPath());
    return true;
}

//------------------------------------------------------------------------------
// Dummy method returning empty string if needed - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyStringFunc(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
    int numoutputs = eval.GetNargoutValue();
    if (numoutputs > 0)
    {
        outputs.push_back("");
    }
    if (numoutputs > 1)
    {
        outputs.push_back("");
    }
    return true;
}
//------------------------------------------------------------------------------
// Dummy method, returning 1 if needed - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyIntFunc(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
    if (eval.GetNargoutValue() > 0)
    {
        outputs.push_back("1");
    }
    return true;
}
//------------------------------------------------------------------------------
//! Dummy method returning empty cell if needed - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyCellFunc(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
    if (eval.GetNargoutValue())
    {
        outputs.push_back(EvaluatorInterface::allocateCellArray());
    }
    return true;
}
//------------------------------------------------------------------------------
// Dummy method returning true - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyVoidFunc(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
    return true;
}
//------------------------------------------------------------------------------
// Dummy method returning true - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyMatrixFunc(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
    int nargout = eval.GetNargoutValue();
    if (nargout > 0)
    {
        outputs.push_back(EvaluatorInterface::allocateMatrix());
    }
    if (nargout > 1)
    {
        outputs.push_back(false);
    }
    return true;
}

//------------------------------------------------------------------------------
// Dummy method - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyUiGetFile(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs)
{
    outputs.push_back("");
    outputs.push_back("");
    outputs.push_back(0);
    return true;

}