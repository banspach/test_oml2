/**
* @file SignalHandler.h
* @date May 2019
* Copyright (C) 2019-2020 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/


#ifndef __JUPYTER_SIGNALHANDLER_H__
#define __JUPYTER_SIGNALHANDLER_H__

// Begin defines/includes
#include "Runtime/SignalHandlerBase.h"

class JupyterWrapper;
// End defines/includes

//------------------------------------------------------------------------------
//! Class for handling actions from oml core in client
//------------------------------------------------------------------------------
class SignalHandler : public SignalHandlerBase
{
public:
    SignalHandler() : _src (NULL) {}                  //! Constructor
    virtual ~SignalHandler() {}                       //! Destructor    

    //! Creates clone
    virtual SignalHandlerBase* CreateClone();
    //! Returns true of this is a clone
    virtual bool IsClone() const { return _src ? true : false; }
    
    //! Sets the console wrapper
    void SetWrapper( JupyterWrapper* wrapper) { m_jupyterwrapper = wrapper; }

    //! Returns class info
    virtual const char* ClassName() const { return "SignalHandler"; }

    //! Clear results
    virtual void OnClearResultsHandler();
    //! Print result
    //! \param[in] cur Result to print
    virtual void OnPrintResultHandler( const Currency& cur);
    //! Start pause
    //! \param[in] msg  User message to display
    //! \param[in] wait True if waiting for a keystroke input from user
    virtual void OnPauseStartHandler( const std::string& msg,
                                      bool               wait);
    //! Prints prompt for save on exit
    //! \param returnCode Code to exit the application with
    virtual void OnSaveOnExitHandler(int returnCode);
    //! Adds nested display for pagination
    //! \param[in] display Nested display to add
    virtual void OnAddDisplayHandler( CurrencyDisplay* display);
    //! Get user input
    //! \param[in]  prompt Prompt to display to user
    //! \param[in]  type   Type, if specified
    //! \param[out] input  Input from user
    virtual void OnUserInputHandler( const std::string& prompt,
                                     const std::string& type,
                                     std::string&       input);

private:
    const SignalHandler*       _src;            //! Source, if this is a clone                 
    JupyterWrapper* m_jupyterwrapper; //! Console wrapper

    SignalHandler( const SignalHandler* src);   //! Copy constructor
};

#endif
// End of file:
