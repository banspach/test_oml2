/**
* @file SignalHandler.cpp
* @date May 2019
* Copyright (C) 2019-2020 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/


// Begin defines/includes
#include "SignalHandler.h"

#include <cassert>

#include "JupyterWrapper.h"

// End defines/includes

//------------------------------------------------------------------------------
//! Copy constructor
//------------------------------------------------------------------------------
SignalHandler::SignalHandler(const SignalHandler* src) 
    : _src (src)
    , m_jupyterwrapper (NULL)
{
    // Set the console wrapper for handling actions from oml core in client
    if (_src)
        m_jupyterwrapper = _src->m_jupyterwrapper;
}
//------------------------------------------------------------------------------
//! Clone signal handler
//------------------------------------------------------------------------------
SignalHandlerBase* SignalHandler::CreateClone()
{
    return (new SignalHandler(this));
}
//------------------------------------------------------------------------------
//! Emits signal to clear results
//------------------------------------------------------------------------------
void SignalHandler::OnClearResultsHandler()
{
    //! Clear results only once, in the signal handler which is not cloned
    //if (!_src && m_jupyterwrapper)  
        //m_jupyterwrapper->HandleOnClearResults();
}
//------------------------------------------------------------------------------
//! Prints prompt for save on exit
//------------------------------------------------------------------------------
void SignalHandler::OnSaveOnExitHandler(int returnCode)
{
//    if (m_jupyterwrapper)  
  //      m_jupyterwrapper->HandleOnSaveOnExit(returnCode);
}
//------------------------------------------------------------------------------
//! Start pause
//! \param[in] msg  User message to display
//! \param[in] wait True if waiting for a keystroke input from user
//------------------------------------------------------------------------------
void SignalHandler::OnPauseStartHandler(const std::string& msg, bool wait)
{
    //if (m_jupyterwrapper)  
      //  m_jupyterwrapper->HandleOnPauseStart(msg, wait);
}
//------------------------------------------------------------------------------
//! Get user input
//! \param[in]  prompt Prompt to display to user
//! \param[in]  type   Type, if specified
//! \param[out] input  Input from user
//------------------------------------------------------------------------------
void SignalHandler::OnUserInputHandler(const std::string& prompt,
                                       const std::string& type,
                                       std::string&       input)
{
    if (m_jupyterwrapper)  
        m_jupyterwrapper->HandleOnGetUserInput(prompt, type, input);
}
//------------------------------------------------------------------------------
//! Print result
//! \param[in] cur Result to print
//------------------------------------------------------------------------------
void SignalHandler::OnPrintResultHandler(const Currency& cur)
{
    // Printing is done only by the source console wrapper
    if (!_src && m_jupyterwrapper)  
        m_jupyterwrapper->HandleOnPrintResult(cur);
}
//------------------------------------------------------------------------------
//! Add nested display
//! \param[in] display Nested display to add
//------------------------------------------------------------------------------
void SignalHandler::OnAddDisplayHandler(CurrencyDisplay* display)
{
    // if (!_src && m_jupyterwrapper)  
      //  m_jupyterwrapper->HandleOnAddDisplay(display);
}

// End of file:
