/**
* @file JupyterOmlFuncs.h
* @date January 2020
* Copyright (C) 2020-2021 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#ifndef __JUPYTER_OML_FUNCS_H__
#define __JUPYTER_OML_FUNCS_H__

#define OML_PRODUCT "Altair Compose"
//#define OML_VERSION "2021.1" - use JupyterWrapper::OmlVersion

//!
//! Initializes environment variables
//!
void InitEnvVariables();

//! 
//! Registers oml built in functions
//! 
void RegisterBuiltInFuncs();

//! Returns true if successful in getting the version string (version command)
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlVersion(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs);

//!
//! Gets argc (getargc command)
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlGetArgC(EvaluatorInterface           eval,
	const std::vector<Currency>& inputs,
	std::vector<Currency>& outputs);
//!
//! Gets argv at the given index (getargv command)
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlGetArgV(EvaluatorInterface           eval,
	const std::vector<Currency>& inputs,
	std::vector<Currency>& outputs);

//!
//! Returns the name of the startup script file (startupscript_path command)
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlStartupscriptPath(EvaluatorInterface eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs);

//!
//! Dummy method, returning an empty cell if needed - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyCellFunc(EvaluatorInterface           eval,
                  const std::vector<Currency>& inputs,
                  std::vector<Currency>&       outputs);
//!
//! Dummy method, returning 1 if needed - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyIntFunc(EvaluatorInterface           eval,
                  const std::vector<Currency>& inputs,
                  std::vector<Currency>&       outputs);
//!
//! Dummy method returning empty string if needed - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyStringFunc(EvaluatorInterface           eval,
                     const std::vector<Currency>& inputs,
                     std::vector<Currency>&       outputs);
//!
//! Dummy method returning true - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyVoidFunc(EvaluatorInterface           eval,
                   const std::vector<Currency>& inputs,
                   std::vector<Currency>&       outputs);
//!
//! Dummy method returning true - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyMatrixFunc(EvaluatorInterface           eval,
                     const std::vector<Currency>& inputs,
                     std::vector<Currency>&       outputs);

//!
//! Dummy method returning empty string if needed - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyUiGetFile(EvaluatorInterface           eval,
    const std::vector<Currency>& inputs,
    std::vector<Currency>& outputs);

#endif

// End of file:

