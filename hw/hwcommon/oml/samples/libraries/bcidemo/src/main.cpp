#include "OMLInterfacePublic.h"
#include "bcidemoDefs.h"

bool myfunc(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);

extern "C" BCIDEMOLIB_DECLS int InitToolbox(OMLInterface* eval)
{
	eval->RegisterFunction("bcidemo1", myfunc); 
	return 0;
}

bool myfunc(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
		// check proper number of inputs
	if (inputs->Size() != 2)
		eval->ThrowError("Incorrect number of inputs");

	const OMLCurrency* input1 = inputs->Get(0);
	const OMLCurrency* input2 = inputs->Get(1);

	// check proper input types
	if (input1->IsScalar() && input2->IsScalar())
	{
		double d1 = input1->GetScalar();
		double d2 = input2->GetScalar();
		outputs->AddScalar(d1+d2);
	}
	else
	{
		eval->ThrowError("Invalid input type");
	}
	return true;
}

