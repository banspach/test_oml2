//-*-c++-*-
/* $\Id$
** ----------------------------------------------------------------------------
** BCIDEMODefs.h - General definitions which are included everywhere.
** ----------------------------------------------------------------------------
** Copyright (c) 2019 Altair Engineering Inc. All Rights Reserved.
** Contains trade secrets of Altair Engineering, Inc.  Copyright notice does
** not imply publication.	 Decompilation or disassembly of this software is
** strictly prohibited.
**  ---------------------------------------------------------------------------
*/

#ifndef __BCIDEMODEFS_H__
#define __BCIDEMODEFS_H__

//------------------------------------------------------------------------------
//!
//! \brief Contains macro definitions for exporting functions in the library
//!
//------------------------------------------------------------------------------

#ifdef OS_WIN
#ifdef BCIDEMOLIB_EXPORT
#undef  BCIDEMOLIB_DECLS
#define BCIDEMOLIB_DECLS __declspec(dllexport)
#else
#undef  BCIDEMOLIB_DECLS
#define BCIDEMOLIB_DECLS __declspec(dllimport)
#endif  // BCIDEMOLIB_EXPORT
#else
#undef  BCIDEMOLIB_DECLS
#define BCIDEMOLIB_DECLS
#endif // OS_WIN


#endif // __BCIDEMODEFS_H__

