grammar ExprCppTree;

options {
    language = C;
    output = AST;
    ASTLabelType = pANTLR3_BASE_TREE;
	backtrack = true;
	memoize = true;
}

tokens {
    FUNC;
	MR_FUNC;
	PARAM_LIST;
	CELL_PARAM_LIST;
	ID_LIST;
	UMINUS;
	STATEMENT_LIST;
    FUNC_DEF;
    CONDITIONAL;
    ELSE;
	VECTOR;
	MATRIX;
    CELL_ARRAY;
    CELL_VAL;
	CELL_ASSIGN;
	DUMMY;
    HML_STRING;
    TRANSP;
    CTRANSP;
    FUNC_HANDLE;
    INLINE_INDEX;
	INLINE_INDEX_CELL;
    STRUCT;
    FIELD;
	DYN_FIELD;
	STMT;
    CELL_EXTRACT;
	UGLY1;
	UGLY2;
	UGLY3;
	UGLY4;
	INPLACE;
	QUOTE2;
	IF2;
	FUNC_LIST;
}

@header {
   	#include "../../Runtime/ANTLRoverride.h"
}

@parser::apifuncs {
	RECOGNIZER->displayRecognitionError = displayRecognitionErrorNew;
}

@lexer::header {
	#include "../../Runtime/ANTLRoverride.h"
}

@lexer::apifuncs {
	RECOGNIZER->displayRecognitionError = displayRecognitionErrorNew;
}

// The suffix '^' means make it a root.
// The suffix '!' means ignore it.

IF	:   'if';
WHILE:	'while';
FOR:	'for';
SWITCH:	'switch';
CASE:	'case';
OTHERWISE:	'otherwise';
GLOBAL:	'global';
PERSISTENT: 'persistent';
RETURN:	'return';
END:	'end';
TRY:	'try';
FUNCTION: 'function';
CLEAR: 'clear';
ELSEIF: 'elseif';
OTHER: 'else';
CLASSDEF: 'classdef';
METHODS: 'methods';
PROPERTIES: 'properties';

EQUAL:  '==';
NEQUAL:	'~=';
PLUS:   '+';
MINUS:  '-';
TIMES:  '*';
DIV:    '/';
ETIMES: '.*';
EDIV:	'./';
LDIV:	'\\';
ELDIV:	'.\\';
QUOTE:  '\'';
EQUOTE: '\'\'';
COLON:	':';
POW:    '^';
LTHAN:	'<';
GTHAN:	'>';
LEQ:	'<=';
GEQ:	'>=';
DOT:	'.';
DOTPOW:	'.^';
AND	:	'&';
OR	:	'|';
LAND:	'&&';
LOR	:	'||';
NEGATE: '~';
SEMIC:  ';';
COMMA:  ',';
DOTDOT:	'..';
LBRACKET: '[';
RBRACKET: ']';
LPAREN: '(';
RPAREN: ')';
LCURLY: '{';
RCURLY: '}';
PERCENT: '%';
AMP: '@';

PCTLCURLY: '%{';
RCURLYPCT: '%}';

nl_or_sc: SEMIC | NEWLINE -> ;

nl_or_sc_or_comment: ((SEMIC comment?) | comment | NEWLINE) -> ;	 

matrix	:	LBRACKET NEWLINE* exprlist (nl_or_sc_or_comment comment_or_newline_strict* exprlist)* comment_or_newline_strict* RBRACKET -> ^(MATRIX exprlist+)
        |   LBRACKET comma_or_sc* RBRACKET                  -> ^(MATRIX);

cell 	:	LCURLY comment_or_newline_strict* exprlist (nl_or_sc_or_comment comment_or_newline_strict* exprlist)* comment_or_newline_strict* RCURLY -> ^(CELL_ARRAY exprlist+)
        |   LCURLY RCURLY                               -> ^(CELL_ARRAY);
  
cell_value:	IDENT (LCURLY params RCURLY)+ -> ^(CELL_VAL IDENT params+);

comma_or_ws:	COMMA? WS? -> ;

comma_or_sc:	(COMMA | SEMIC) -> COMMA? SEMIC?;

exprlist:	comma_or_sc* expr (comma_or_ws expr)* comma_or_sc* -> ^(VECTOR expr+);

expr	:	expr2 ((OR^ | LOR^) expr2)*;

expr2: ineqExpr ((AND^ | LAND^) ineqExpr)*; 

ineqExpr:	colonExpr ((EQUAL^ | NEQUAL^ | LTHAN^ | GTHAN^ | LEQ^ | GEQ^) colonExpr)*;

colonExpr:	addExpr (COLON^ addExpr)*;

addExpr_or_end: (addExpr | END) -> addExpr? END?;

addExpr: multExpr ((PLUS^ | MINUS^) multExpr)*;

multExpr: unaryExpr ((TIMES^ | DIV^ | ETIMES^ | EDIV^ | LDIV^ | ELDIV^) unaryExpr)*;

unaryExpr: MINUS unaryExpr -> ^(UMINUS unaryExpr)
	     | PLUS  unaryExpr -> unaryExpr
		 | NEGATE unaryExpr -> ^(NEGATE unaryExpr)
		 | powExpr -> powExpr;

powExpr	: indexExpr ((POW^ | DOTPOW^) MINUS? PLUS? indexExpr)*;

indexExpr: (atom->atom) ( LPAREN params RPAREN -> ^(FUNC $indexExpr params)
						| LPAREN RPAREN -> ^(FUNC $indexExpr ^(PARAM_LIST))
                        | LCURLY params2 RCURLY -> ^(CELL_VAL $indexExpr params2)
                        | DOT IDENT -> ^(STRUCT $indexExpr IDENT)
			| DOT LPAREN expr RPAREN -> ^(STRUCT $indexExpr ^(DYN_FIELD expr))
                        )*
         | postExpr -> postExpr;  

postExpr: (atom ugly)=> atom ugly atom -> ^(ugly ^(TRANSP atom) atom)
		| (atom QUOTE)=> atom QUOTE -> ^(CTRANSP atom)
        | (atom DOT)=>atom DOT QUOTE -> ^(TRANSP atom)
		| atom        -> atom;

ugly1: QUOTE POW -> ^(UGLY1);
ugly2: QUOTE DOTPOW -> ^(UGLY2);
ugly3: DOT QUOTE POW -> ^(UGLY3);
ugly4: DOT QUOTE DOTPOW -> ^(UGLY4);

ugly: (ugly1 | ugly2 | ugly3 | ugly4) -> ugly1? ugly2? ugly3? ugly4?;

string	: QUOTE chars* QUOTE -> ^(HML_STRING QUOTE chars* QUOTE)
		| EQUOTE EQUOTE EQUOTE -> ^(HML_STRING QUOTE EQUOTE EQUOTE QUOTE)
		| EQUOTE EQUOTE -> ^(HML_STRING QUOTE EQUOTE QUOTE)
        | EQUOTE -> ^(HML_STRING);

chars_or_quote: (chars | QUOTE) -> chars? QUOTE?;

comment	: PERCENT chars_or_quote* (NEWLINE | EOF) -> ^(DUMMY chars_or_quote*);

keywords : (IF | END | FOR | WHILE | SWITCH | CASE | TRY | OTHERWISE | RETURN | GLOBAL | PERSISTENT | FUNCTION | OTHER | ELSEIF | 'catch' | IF2 | PROPERTIES | METHODS | CLASSDEF) -> IF? END? FOR? WHILE? SWITCH? CASE? TRY? OTHERWISE? RETURN? GLOBAL? PERSISTENT? FUNCTION? OTHER? ELSEIF? 'catch'? IF2? PROPERTIES? METHODS? CLASSDEF?;

operators : (EDIV | ELDIV | '=' | '*' | '+' | '-' | '/' | '\\' | COLON | POW | LTHAN | GTHAN | LEQ | GEQ | AND | OR | NEGATE | EQUAL | NEQUAL | LAND | LOR | DOTPOW | ETIMES) -> EDIV? ELDIV? '='? '*'? '+'? '-'? '/'? '\\'? COLON? POW? LTHAN? GTHAN? LEQ? GEQ? AND? OR? NEGATE? EQUAL? NEQUAL? LAND? LOR? DOTPOW? ETIMES?;

specials: (DOTDOT | EQUOTE | COMMA | '.' | '"' | LPAREN | RPAREN | LBRACKET | RBRACKET | LCURLY | RCURLY | PERCENT | SEMIC | NUMBERHACK | '?' | '!' | '...' | '_' | AMP | '#' | '$' | '`' | CONTHACK | CLEAR | PCTLCURLY | RCURLYPCT) -> DOTDOT? EQUOTE? COMMA? '.'? '"'? LPAREN? RPAREN? LBRACKET? RBRACKET? LCURLY? RCURLY? PERCENT? SEMIC? NUMBERHACK? '?'? '!'? '...'? '_'? AMP? '#'? '$'? '`'? CONTHACK? CLEAR? PCTLCURLY? RCURLYPCT?;

chars   : (IDENT | NUMBER | HEXVAL | UNICHAR_IDENT | keywords | operators | specials | HACK | HACKB) -> IDENT? NUMBER? UNICHAR_IDENT? keywords? operators? specials? HACK? HACKB?;  

path_chars: ('.' | DOTDOT | EDIV | ELDIV | '/' | '\\' | COLON) -> '.'? DOTDOT? EDIV? ELDIV? '/'? '\\'? COLON?;

path	:	(IDENT? path_chars+ IDENT?)+;

wildcard:	 (TIMES? IDENT TIMES)+ IDENT?
        |    (TIMES IDENT TIMES?)+
	    |    IDENT -> IDENT;    

ident_or_global: (wildcard | GLOBAL) -> wildcard? GLOBAL?;

atom: NUMBER
    | HEXVAL
    | IDENT
    | string
    | LPAREN! expr RPAREN!
	| matrix
	| cell
    | func_handle
    ;

end_or_eof:	END SEMIC? -> END
          | EOF -> EOF;

funcdef1 : FUNCTION (f1=IDENT ASSIGN)? f2=IDENT LPAREN id_list3 RPAREN SEMIC? comment_or_newline_strict statement_list? end_or_eof -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) id_list3 statement_list? end_or_eof)
         | FUNCTION (f1=IDENT ASSIGN)? f2=IDENT (LPAREN RPAREN)? SEMIC? comment_or_newline_strict statement_list? end_or_eof -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) ID_LIST statement_list? end_or_eof);

funcdef2: FUNCTION (f1=IDENT ASSIGN)? f2=IDENT ( LPAREN id_list RPAREN comma_or_sc? substmt stmt2* SEMIC? END -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) id_list ^(STMT substmt stmt2 SEMIC?))
                                               | (LPAREN RPAREN)? comma_or_sc? substmt stmt2* SEMIC? END -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) ID_LIST ^(STMT substmt stmt2 SEMIC?)));

funcdef: (funcdef1 | funcdef2) -> funcdef1? funcdef2?;

mr_funcdef: FUNCTION LBRACKET f1=id_list RBRACKET ASSIGN IDENT LPAREN f2=id_list RPAREN SEMIC? comment_or_newline_strict statement_list end_or_eof -> ^(FUNC_DEF IDENT $f1 $f2 statement_list end_or_eof)
          | FUNCTION LBRACKET RBRACKET ASSIGN IDENT LPAREN f2=id_list RPAREN SEMIC? comment_or_newline_strict statement_list end_or_eof -> ^(FUNC_DEF IDENT ID_LIST $f2 statement_list end_or_eof)
          | FUNCTION LBRACKET f1=id_list RBRACKET ASSIGN IDENT (LPAREN RPAREN)? SEMIC? comment_or_newline_strict statement_list end_or_eof -> ^(FUNC_DEF IDENT $f1 ID_LIST statement_list end_or_eof)
		  | FUNCTION LBRACKET RBRACKET ASSIGN IDENT (LPAREN RPAREN)? SEMIC? comment_or_newline_strict statement_list end_or_eof -> ^(FUNC_DEF IDENT ID_LIST ID_LIST statement_list end_or_eof);

func_handle:	AMP LPAREN id_list RPAREN expr -> ^(FUNC_HANDLE ^(IDENT["anonymous"]) ^(ID_LIST) id_list ^(STATEMENT_LIST expr))
           |    AMP LPAREN RPAREN expr -> ^(FUNC_HANDLE ^(IDENT["anonymous"]) ^(ID_LIST) ^(ID_LIST) ^(STATEMENT_LIST expr))
           |    AMP IDENT -> ^(FUNC_HANDLE IDENT);
		      
fcall: IDENT (LPAREN params RPAREN)+ -> ^(FUNC IDENT params+)
     | CLEAR LPAREN params? RPAREN -> ^(FUNC IDENT["clear"] params?)
	 | IDENT LPAREN RPAREN -> ^(FUNC IDENT);

ident_or_number: (ident_or_keyword NUMBER?) -> ident_or_keyword NUMBER?
            | NUMBER -> NUMBER;

ident_group:   ident_or_number+ -> ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] ident_or_number QUOTE["\'"])+);

ident_group2: path+ -> ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] path QUOTE["\'"])+);

ident_group3: ident_or_global+ -> ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] ident_or_global QUOTE["\'"])+);

alt_fcall: IDENT ( string -> ^(FUNC IDENT ^(PARAM_LIST string))
                 | ident_group -> ^(FUNC IDENT ident_group?));

alt_fcall2: IDENT ident_group2 -> ^(FUNC IDENT ident_group2);

clear:	CLEAR ident_group3 -> ^(FUNC IDENT["clear"] ident_group3)
     |  CLEAR GLOBAL -> ^(FUNC IDENT["clear"] ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] GLOBAL QUOTE["\'"])))
     |  CLEAR -> ^(FUNC IDENT["clear"]);

mr_fcall: LBRACKET id_list2 RBRACKET ASSIGN IDENT (LPAREN params? RPAREN)? -> ^(MR_FUNC IDENT id_list2 params?);

cell_extract: LBRACKET id_list RBRACKET ASSIGN ( cell_value -> ^(CELL_EXTRACT id_list cell_value)
                                               | indexExpr -> ^(CELL_EXTRACT id_list indexExpr));

flexible_index:	LPAREN params RPAREN -> params
              |   LPAREN RPAREN -> ^(PARAM_LIST);

ident_or_keyword: (IDENT | keywords) -> IDENT? keywords?;
            
substruct:	DOT ident_or_keyword LCURLY cell_params RCURLY substruct -> ^(FIELD ident_or_keyword cell_params substruct)
		 |	DOT ident_or_keyword flexible_index? substruct? -> ^(FIELD ident_or_keyword flexible_index? substruct?)
         |	DOT LPAREN expr RPAREN flexible_index? substruct? -> ^(DYN_FIELD expr flexible_index? substruct?);

struct_member:	IDENT (LPAREN params? RPAREN)? substruct -> ^(STRUCT ^(IDENT) params? substruct)
             |  cell_value substruct -> ^(STRUCT cell_value substruct);

expr_or_colon:	(expr | COLON) -> expr? COLON?;

params: expr_or_colon (COMMA NEWLINE? expr_or_colon)* -> ^(PARAM_LIST expr_or_colon+);

params2: expr_or_colon (COMMA NEWLINE? expr_or_colon)* -> ^(PARAM_LIST expr_or_colon+);

cell_params: expr_or_colon (COMMA NEWLINE? expr_or_colon)* -> ^(CELL_PARAM_LIST expr_or_colon+);

id_list: IDENT (comma_or_ws IDENT)* -> ^(ID_LIST IDENT+);

id_or_fcall: (struct_member | fcall | cell_value | IDENT | NEGATE ) -> struct_member? fcall? cell_value? IDENT? NEGATE? ;

id_list2: id_or_fcall (comma_or_ws id_or_fcall)* -> ^(ID_LIST id_or_fcall+);

ident_default: IDENT (ASSIGN expr)? -> ^(IDENT expr?);

id_list3: ident_default (comma_or_ws ident_default)* -> ^(ID_LIST ident_default+);

caseblock:	CASE expr COMMA? ( comment_or_newline_strict+ statement_list? -> ^(CASE expr statement_list?)
							 | stmt_group2 comment_or_newline_strict+ -> ^(CASE expr ^(STATEMENT_LIST stmt_group2)));

otherwiseblock:	OTHERWISE COMMA? ( comment_or_newline_strict+ statement_list? -> ^(OTHERWISE statement_list?)
								 | stmt_group2 comment_or_newline_strict+ -> ^(OTHERWISE ^(STATEMENT_LIST stmt_group2)));

switchcase	:	SWITCH e1=expr COMMA? comment_or_newline_strict+ caseblock+ otherwiseblock? END comment_or_newline? -> ^(SWITCH expr caseblock+ otherwiseblock?); // can't call this switch
	
else_clause: else_clause1 -> else_clause1
           | else_clause2 NEWLINE -> else_clause2;

else_clause1: OTHER comment_or_newline sz=statement_list? -> $sz?;

else_clause2: OTHER comma_or_sc? sz=stmt_group2? -> ^(STATEMENT_LIST $sz)?;

elseif_clause: elseif_clause1 -> elseif_clause1
             | elseif_clause2 NEWLINE? -> elseif_clause2;

elseif_clause1: ELSEIF e1=expr comma_or_sc? comment_or_newline s1=statement_list? -> ^(IF $e1 $s1?);

elseif_clause2: ELSEIF comma_or_sc? e2+= expr comma_or_sc? s2=stmt_group2? -> ^(IF $e2 ^(STATEMENT_LIST $s2))?;

if_clause: if_clause1 -> if_clause1
         | if_clause2 -> if_clause2
		 | if_clause3 -> if_clause3;

if_clause1: IF e1=expr comma_or_sc? comment_or_newline s1=statement_list? -> ^(IF $e1 $s1);

if_clause2: IF2 e1=expr comma_or_sc? s1=stmt_group2 -> ^(IF $e1 ^(STATEMENT_LIST $s1));

if_clause3: IF e1=expr comma_or_sc? s1=stmt_group2 comment_or_newline -> ^(IF $e1 ^(STATEMENT_LIST $s1));

conditional
	:	 if_clause elseif_clause* else_clause? END comment_or_newline -> ^(CONDITIONAL if_clause elseif_clause* ^(ELSE else_clause?));

conditional2
	:	if_clause2  elseif_clause2* else_clause2? END SEMIC? -> ^(CONDITIONAL if_clause2 elseif_clause2* ^(ELSE else_clause2?));

whileloop
	:	WHILE expr comment_or_newline statement_list END SEMIC? NEWLINE? -> ^(WHILE expr statement_list);

whileloop2
	:	WHILE expr comma_or_sc? substmt stmt2* cs=comma_or_sc? END -> ^(WHILE expr ^(STMT substmt stmt2* $cs?));

forloop:	FOR LPAREN* IDENT ASSIGN expr RPAREN* comma_or_sc? comment_or_newline? statement_list? END comment_or_newline? -> ^(FOR IDENT expr statement_list?);

forloop2: FOR IDENT ASSIGN expr comma_or_sc? substmt stmt2* cs=comma_or_sc? END -> ^(FOR IDENT expr ^(STMT substmt stmt2* $cs?));

assign	:	IDENT ( (LPAREN params RPAREN)? ASSIGN expr -> ^(ASSIGN IDENT expr params?)
				  |	(LCURLY params RCURLY)+ ASSIGN expr -> ^(CELL_ASSIGN IDENT expr params+))
		|   struct_member ( ASSIGN expr -> ^(ASSIGN struct_member expr)
						  | (LCURLY cell_params RCURLY)+ ASSIGN expr -> ^(CELL_ASSIGN struct_member expr cell_params+))
		|   cell_value LPAREN params RPAREN ASSIGN expr -> ^(ASSIGN cell_value expr params);

global	:	GLOBAL IDENT+ comment_or_newline -> ^(GLOBAL IDENT+);

global2	:	GLOBAL IDENT+ -> ^(GLOBAL IDENT+);

persistent:	PERSISTENT IDENT+ comment_or_newline -> ^(PERSISTENT IDENT+);

comment_or_newline	:	SEMIC? (NEWLINE | comment | EOF) -> SEMIC?;

comment_or_newline_strict: (NEWLINE | comment);

trycatch:	TRY NEWLINE s1=statement_list ('catch' IDENT? NEWLINE s2=statement_list?)? END -> ^(TRY $s1 $s2? IDENT?);

substmt	: mr_fcall
        | assign
		| alt_fcall
		| clear
		| expr
		| forloop2
		| whileloop2
		| conditional2
		| global2
		| RETURN;

stmt2	: comma_or_sc substmt -> comma_or_sc substmt;

stmt_group: s1=substmt stmt2* COMMA? comment_or_newline -> ^(STMT $s1 stmt2* comment_or_newline?);

stmt_group2: s1=substmt stmt2* comma_or_sc? -> ^(STMT $s1 stmt2* comma_or_sc?);

properties: PROPERTIES (LPAREN p1=IDENT ASSIGN p2=IDENT RPAREN)? comment_or_newline+ (p3+=IDENT comment_or_newline+)+ END comment_or_newline* -> ^(PROPERTIES ^(ID_LIST $p3+) $p1? $p2?);

func_or_mrfunc_def: (funcdef | mr_funcdef) -> funcdef? mr_funcdef?;

methods: METHODS comment_or_newline+ (func_or_mrfunc_def comment_or_newline+)+ END -> ^(FUNC_LIST func_or_mrfunc_def+);

classdef: CLASSDEF IDENT (LTHAN id_list)? comment_or_newline+ properties* comment_or_newline* methods comment_or_newline+ END -> ^(CLASSDEF IDENT properties* methods id_list?);

stmt: 
    stmt_group
	| conditional
	| switchcase
    | whileloop
    | forloop
	| funcdef NEWLINE? -> funcdef
    | mr_funcdef
    | cell_extract comment_or_newline -> ^(STMT cell_extract comment_or_newline?)
	| global
    | persistent
	| trycatch
    | NEWLINE -> DUMMY
	| comment
	| alt_fcall2 SEMIC? -> alt_fcall2
	| classdef
	| RETURN comment_or_newline
    ;

ASSIGN: '=';

prog: statement_list EOF -> statement_list
    | EOF -> DUMMY;

statement_list : stmt+ -> ^(STATEMENT_LIST stmt+);

fragment LETTER:  'a'..'z' | 'A'..'Z';
fragment UNICHAR: '\u00a0'..'\ufffe';
fragment DIGIT:   '0'..'9';

IDENT: (LETTER|'_')(LETTER|DIGIT|'_')* ;
UNICHAR_IDENT: (LETTER|UNICHAR)(LETTER|DIGIT|'_'|UNICHAR)* ;
NUMBERHACK: DIGIT+ '.' DIGIT+ '.';
NUMBER: DIGIT+ ('.' (DIGIT)*)? (('E'|'e'|'D'|'d')('-'|'+')?(DIGIT)+)? ('i'|'j'|'I'|'J')?
      | '.'DIGIT+ (('E'|'e'|'D'|'d')('-'|'+')?(DIGIT)+)? ('i'|'j'|'I'|'J')?;
HEXVAL: '0x'(DIGIT|'a'..'f'|'A'..'F')+;
NEWLINE: '\r'? '\n' ;
WS : (' '|'\t')+ {$channel = HIDDEN;}; 
CONTHACK: '...' WS;
CONT: '...' WS? NEWLINE {$channel = HIDDEN;};
HACK: DIGIT+ ('.' (DIGIT)*)? ('E'|'e'|'D'|'d')('-'|'+')?;
HACKB: '.'DIGIT+ ('E'|'e'|'D'|'d');