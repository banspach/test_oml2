grammar ExprJavaTree;

options {
    language = Java;
    output = AST;
    ASTLabelType=CommonTree;
    backtrack = true;
}

tokens {
    FUNC;
    MR_FUNC;
    PARAM_LIST;
    PARAM_CELL;
    ID_LIST;
    UMINUS;
    STATEMENT_LIST;
    FUNC_DEF;
    CONDITIONAL;
    ELSE;
    VECTOR;
    MATRIX;
    CELL_ARRAY;
    CELL_VAL;
    STRING;
    TRANSP;
    CTRANSP;
    FUNC_HANDLE;
    INLINE_INDEX;
    STRUCT;
    FIELD;
    CELL_ASSIGN;
}

@header {
    // #include <assert.h>
}

// The suffix '^' means make it a root.
// The suffix '!' means ignore it.



IF	   :	'if';
WHILE	   :	'while';
FOR	   :	'for';
SWITCH     :	'switch';
CASE       :	'case';
OTHERWISE  :	'otherwise';
GLOBAL	   :	'global';
RETURN	   :	'return';
BREAK	   :	'break';

EQUAL	:	'==';
NEQUAL	:	'~=';
PLUS    : '+';
MINUS   : '-';
QUOTE	:	'\'';
COLON	:	':';
POW     :	'^';
LTHAN	:	'<';
GTHAN	:	'>';
DOT     :	'.';

expr:	colonExpr ((EQUAL^ | NEQUAL^ | LTHAN^ | GTHAN^) colonExpr)?;

colonExpr:	addExpr (COLON^ addExpr)*;

addExpr: multExpr ((PLUS^ | MINUS^) multExpr)*;
    
multExpr: unaryExpr ((TIMES^ | DIV^) unaryExpr)*;

unaryExpr: MINUS indexExpr -> ^(UMINUS indexExpr)
	 | PLUS?  indexExpr -> indexExpr;
	 
indexExpr: postExpr '(' params ')' -> ^(INLINE_INDEX postExpr params)
         | postExpr                -> postExpr;	 

postExpr: powExpr QUOTE -> ^(TRANSP powExpr)
        | powExpr DOT QUOTE -> ^(CTRANSP powExpr)
        | powExpr       -> powExpr;

powExpr	: atom (POW^ atom)*;
   
TIMES: '*';
DIV: '/';

exprlist:	expr (',' expr)* -> ^(VECTOR expr+);

nl_or_sc: ';' | NEWLINE -> ;

matrix	:	'[' exprlist (nl_or_sc exprlist)* ']' -> ^(MATRIX exprlist+)
        |       '['']'                                     -> ^(MATRIX);
        
cell 	:	'{' exprlist (nl_or_sc exprlist)*'}' -> ^(CELL_ARRAY exprlist+);
        
string	:	QUOTE chars* QUOTE -> ^(STRING chars* );

chars   :	(ID | NUMBER | IF | ',' | '=' | '.' | '"' | '*' | '+') -> ID? NUMBER? IF? ','? '='? '.'? '"'? '+'? '*'?;     

atom: NUMBER
    | struct_member
    | fcall
    | cell_value
    | ID
    | string
    | '('! expr ')'!
    | matrix
    | cell
    | func_handle
    ;
       
funcdef	: 'function' (f1=ID ASSIGN)? f2=ID '(' id_list ')' NEWLINE statement_list 'end' -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) id_list statement_list)
        | 'function' (f1=ID ASSIGN)? f2=ID ('(' ')')? NEWLINE statement_list 'end' -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) ID_LIST statement_list);

mr_funcdef: 'function' '[' f1=id_list ']' ASSIGN ID '(' f2=id_list ')' NEWLINE statement_list 'end' -> ^(FUNC_DEF ID $f1 $f2 statement_list);
    
fcall: ID '(' params? ')' -> ^(FUNC ID params?);

mr_fcall: '[' id_list ']' ASSIGN ID '(' params? ')' -> ^(MR_FUNC ID id_list params?);

cell_value:	ID '{' params '}' -> ^(CELL_VAL ID params);

kludge_index:	'(' params ')' -> params
            |   '{' cell_params '}' -> cell_params;

substruct:	 DOT ID kludge_index? substruct? -> ^(FIELD ID kludge_index? substruct?);

struct_member:	ID ('(' params ')')? (substruct)+ -> ^(STRUCT ^(ID) params? substruct+);

func_handle:	'@' '(' id_list ')' expr -> ^(FUNC_HANDLE ^(ID["anonymous"]) ^(ID_LIST) id_list ^(STATEMENT_LIST expr))
           |    '@' ID -> ^(FUNC_HANDLE ID);

expr_or_colon:	(expr | COLON) -> expr? COLON?;

params: expr_or_colon (',' expr_or_colon)* -> ^(PARAM_LIST expr_or_colon+);

cell_params: expr_or_colon (',' expr_or_colon)* -> ^(PARAM_CELL expr_or_colon+);

id_list: ID (',' ID)* -> ^(ID_LIST ID+);

switchcase	:	SWITCH e1=expr NEWLINE (CASE en+=expr NEWLINE sn+=statement_list)+ (OTHERWISE NEWLINE ot=statement_list)? 'end' NEWLINE -> ^(SWITCH expr ^(CASE $en $sn)+ ^(OTHERWISE $ot)?); // can't call this switch
	
conditional
	:	IF e1=expr NEWLINE s1=statement_list ('elseif' e2+=expr NEWLINE s2+=statement_list)* ('else' NEWLINE sz=statement_list)? 'end' NEWLINE -> ^(CONDITIONAL ^(IF $e1 $s1) ^(IF $e2 $s2)* ^(ELSE $sz?));
	
whileloop
	:	WHILE  expr NEWLINE statement_list 'end' NEWLINE -> ^(WHILE expr statement_list);	
	
forloop :	FOR ID ASSIGN expr NEWLINE statement_list? 'end' NEWLINE -> ^(FOR ID expr statement_list?);

assign	:	ID ('(' params ')')? ASSIGN expr -> ^(ASSIGN ID expr params?)
	|	ID '{' params '}' ASSIGN expr -> ^(CELL_ASSIGN ID expr params)
	|       struct_member ASSIGN expr -> ^(ASSIGN struct_member expr);

global	:	GLOBAL ID+ NEWLINE -> ^(GLOBAL ID+);

comment_or_newline	:	COMMENT | NEWLINE -> ;

COMMENT	:	'%' ( options {greedy=false;} : .)* NEWLINE;

stmt: 
    expr comment_or_newline -> expr   
    | assign comment_or_newline -> assign
    | NEWLINE ->   // ignore
    | conditional
    | switchcase
    | whileloop
    | forloop
    | funcdef
    | mr_funcdef
    | mr_fcall
    | global
    | RETURN
    | BREAK
    | COMMENT NEWLINE -> // ignore
    ;

ASSIGN: '=';

prog: statement_list;

statement_list
    : stmt+ -> ^(STATEMENT_LIST stmt+)
    ;

ID: ('a'..'z'|'A'..'Z')('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;
NUMBER: '0'..'9'+ ('.' ('0'..'9')*)? 'i'?;
NEWLINE: '\r'? '\n' ;
WS : (' '|'\t')+ {$channel = HIDDEN;}; 

