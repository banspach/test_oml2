grammar ExprCppTree;

options {
    language = C;
    output = AST;
    ASTLabelType = pANTLR3_BASE_TREE;
	backtrack = true;
	memoize = true;
}

tokens {
    FUNC;
	MR_FUNC;
	PARAM_LIST;
	ID_LIST;
	UMINUS;
	STATEMENT_LIST;
    FUNC_DEF;
    CONDITIONAL;
    ELSE;
	VECTOR;
	MATRIX;
    CELL_ARRAY;
    CELL_VAL;
	CELL_ASSIGN;
	DUMMY;
    HML_STRING;
    TRANSP;
    CTRANSP;
    FUNC_HANDLE;
    INLINE_INDEX;
	INLINE_INDEX_CELL;
    STRUCT;
    FIELD;
	DYN_FIELD;
	STMT;
    CELL_EXTRACT;
	UGLY1;
	UGLY2;
	UGLY3;
	UGLY4;
	INPLACE;
	QUOTE2;
	IF2;
	FUNC_LIST;
}

@header {
   	#include "../../Runtime/ANTLRoverride.h"
}

@parser::apifuncs {
	RECOGNIZER->displayRecognitionError = displayRecognitionErrorNew;
}

@lexer::header {
	#include "../../Runtime/ANTLRoverride.h"
}

@lexer::apifuncs {
	RECOGNIZER->displayRecognitionError = displayRecognitionErrorNew;
}

// The suffix '^' means make it a root.
// The suffix '!' means ignore it.

IF	:   'if';
WHILE:	'while';
FOR:	'for';
SWITCH:	'switch';
CASE:	'case';
OTHERWISE:	'otherwise';
GLOBAL:	'global';
PERSISTENT: 'persistent';
RETURN:	'return';
END:	'end';
TRY:	'try';
FUNCTION: 'function';
CLEAR: 'clear';
ELSEIF: 'elseif';
OTHER: 'else';
CLASSDEF: 'classdef';
METHODS: 'methods';
PROPERTIES: 'properties';

EQUAL:  '==';
NEQUAL:	'~=';
PLUS:   '+';
MINUS:  '-';
TIMES:  '*';
DIV:    '/';
ETIMES: '.*';
EDIV:	'./';
LDIV:	'\\';
ELDIV:	'.\\';
QUOTE:  '\'';
EQUOTE: '\'\'';
COLON:	':';
POW:    '^';
LTHAN:	'<';
GTHAN:	'>';
LEQ:	'<=';
GEQ:	'>=';
DOT:	'.';
DOTPOW:	'.^';
AND	:	'&';
OR	:	'|';
LAND:	'&&';
LOR	:	'||';
NEGATE: '~';
SEMIC:  ';';
COMMA:  ',';
DOTDOT:	'..';
LBRACKET: '[';
RBRACKET: ']';
LPAREN: '(';
RPAREN: ')';
LCURLY: '{';
RCURLY: '}';
PERCENT: '%';

PCTLCURLY: '%{';
RCURLYPCT: '}%';

ugly1: QUOTE POW -> ^(UGLY1);
ugly2: QUOTE DOTPOW -> ^(UGLY2);
ugly3: DOT QUOTE POW -> ^(UGLY3);
ugly4: DOT QUOTE DOTPOW -> ^(UGLY4);

ugly: (ugly1 | ugly2 | ugly3 | ugly4) -> ugly1? ugly2? ugly3? ugly4?;

nl_or_sc: SEMIC | NEWLINE -> ;

nl_or_sc_or_comment: ((SEMIC comment?) | NEWLINE) -> ;	 

matrix	:	LBRACKET NEWLINE* exprlist (nl_or_sc exprlist)* NEWLINE* RBRACKET -> ^(MATRIX exprlist+)
        |   LBRACKET comma_or_sc* RBRACKET                  -> ^(MATRIX);

cell 	:	LCURLY NEWLINE? exprlist (nl_or_sc_or_comment exprlist)* NEWLINE? RCURLY -> ^(CELL_ARRAY exprlist+)
        |   LCURLY RCURLY                               -> ^(CELL_ARRAY);
  
cell_value:	IDENT (LCURLY params RCURLY)+ -> ^(CELL_VAL IDENT params+);

comma_or_ws:	COMMA? WS? -> ;

comma_or_sc:	(COMMA | SEMIC) -> COMMA? SEMIC?;

exprlist:	comma_or_sc* expr (comma_or_ws expr)* comma_or_sc* -> ^(VECTOR expr+);

expr	:	ineqExpr ((AND^ | OR^ | LAND^ | LOR^) ineqExpr)*;

ineqExpr:	colonExpr ((EQUAL^ | NEQUAL^ | LTHAN^ | GTHAN^ | LEQ^ | GEQ^) colonExpr)*;

colonExpr:	addExpr (COLON^ addExpr)*;

addExpr_or_end: (addExpr | END) -> addExpr? END?;

addExpr: multExpr ((PLUS^ | MINUS^) multExpr)*;

multExpr: unaryExpr ((TIMES^ | DIV^ | ETIMES^ | EDIV^ | LDIV^ | ELDIV^) unaryExpr)*;

unaryExpr: MINUS indexExpr -> ^(UMINUS indexExpr)
	 | PLUS?  indexExpr -> indexExpr
	 | NEGATE indexExpr -> ^(NEGATE indexExpr)
	 | MINUS unaryExpr -> ^(UMINUS unaryExpr)
	 | PLUS unaryExpr -> unaryExpr
	 | NEGATE unaryExpr -> ^(NEGATE unaryExpr);
	 
indexExpr: postExpr LPAREN params RPAREN -> ^(INLINE_INDEX postExpr params)
		 | postExpr LCURLY params RCURLY -> ^(INLINE_INDEX_CELL postExpr params)
         | postExpr                -> postExpr;	 

postExpr: powExpr2 -> powExpr2
		| powExpr QUOTE -> ^(CTRANSP powExpr)
        | powExpr DOT QUOTE -> ^(TRANSP powExpr)
		| powExpr        -> powExpr;

powExpr2 : powExpr ugly powExpr -> ^(ugly ^(TRANSP powExpr) powExpr);

powExpr	: atom ((POW^ | DOTPOW^) MINUS? PLUS? atom)*;

string	: QUOTE chars* QUOTE -> ^(HML_STRING QUOTE chars* QUOTE)
		| EQUOTE EQUOTE -> ^(HML_STRING QUOTE EQUOTE QUOTE)
        | EQUOTE -> ^(HML_STRING);

comment	: PERCENT (chars | QUOTE)* (NEWLINE | EOF) -> ^(DUMMY);

keywords : (IF | END | FOR | WHILE | SWITCH | CASE | TRY | OTHERWISE | RETURN | GLOBAL | PERSISTENT | FUNCTION | OTHER | ELSEIF | 'catch' | IF2) -> IF? END? FOR? WHILE? SWITCH? CASE? TRY? OTHERWISE? RETURN? GLOBAL? PERSISTENT? FUNCTION? OTHER? ELSEIF? 'catch'? IF2?;

operators : (EDIV | ELDIV | '=' | '*' | '+' | '-' | '/' | '\\' | COLON | POW | LTHAN | GTHAN | LEQ | GEQ | AND | OR | NEGATE | EQUAL | NEQUAL | LAND | LOR | DOTPOW | ETIMES) -> EDIV? ELDIV? '='? '*'? '+'? '-'? '/'? '\\'? COLON? POW? LTHAN? GTHAN? LEQ? GEQ? AND? OR? NEGATE? EQUAL? NEQUAL? LAND? LOR? DOTPOW? ETIMES?;

specials: (DOTDOT | EQUOTE | COMMA | '.' | '"' | LPAREN | RPAREN | LBRACKET | RBRACKET | LCURLY | RCURLY | PERCENT | SEMIC | '?' | '!' | '...' | '_' | '@' | '#' | '$' | '`' | CONTHACK | CLEAR | PCTLCURLY | RCURLYPCT) -> DOTDOT? EQUOTE? COMMA? '.'? '"'? LPAREN? RPAREN? LBRACKET? RBRACKET? LCURLY? RCURLY? PERCENT? SEMIC? '?'? '!'? '...'? '_'? '@'? '#'? '$'? '`'? CONTHACK? CLEAR? PCTLCURLY? RCURLYPCT?;

chars   : (IDENT | NUMBER | UNICHAR | keywords | operators | specials | HACK | HACKB) -> IDENT? NUMBER? UNICHAR? keywords? operators? specials? HACK? HACKB?;  

path_chars: ('.' | DOTDOT | EDIV | ELDIV | '/' | '\\' | COLON) -> '.'? DOTDOT? EDIV? ELDIV? '/'? '\\'? COLON?;

path	:	(IDENT? path_chars+ IDENT?)+;

wildcard:	 (TIMES? IDENT TIMES)+ IDENT?
        |    (TIMES IDENT TIMES?)+
	    |    IDENT -> IDENT;    

ident_or_global: (wildcard | GLOBAL) -> wildcard? GLOBAL?;

atom: NUMBER
    | struct_member
    | fcall
	| cell_value
    | IDENT
    | string
    | LPAREN! expr RPAREN!
	| matrix
	| cell
    | func_handle
    ;

end_or_eof:	(END | EOF) -> END? EOF?;

funcdef1	: FUNCTION (f1=IDENT ASSIGN)? f2=IDENT LPAREN id_list RPAREN SEMIC? NEWLINE statement_list end_or_eof -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) id_list statement_list end_or_eof)
        | FUNCTION (f1=IDENT ASSIGN)? f2=IDENT (LPAREN RPAREN)? SEMIC? NEWLINE statement_list end_or_eof -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) ID_LIST statement_list end_or_eof);

funcdef2: FUNCTION (f1=IDENT ASSIGN)? f2=IDENT LPAREN id_list RPAREN comma_or_sc? substmt stmt2* SEMIC? END -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) id_list ^(STMT substmt stmt2 SEMIC?));

funcdef3: FUNCTION (f1=IDENT ASSIGN)? f2=IDENT (LPAREN RPAREN)? comma_or_sc? substmt stmt2* SEMIC? END -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) ID_LIST ^(STMT substmt stmt2 SEMIC?));

funcdef: (funcdef1 | funcdef2 | funcdef3) -> funcdef1? funcdef2? funcdef3?;

mr_funcdef: FUNCTION LBRACKET f1=id_list RBRACKET ASSIGN IDENT LPAREN f2=id_list RPAREN SEMIC? NEWLINE statement_list end_or_eof -> ^(FUNC_DEF IDENT $f1 $f2 statement_list end_or_eof)
          | FUNCTION LBRACKET f1=id_list RBRACKET ASSIGN IDENT (LPAREN RPAREN)? SEMIC? NEWLINE statement_list end_or_eof -> ^(FUNC_DEF IDENT $f1 ID_LIST statement_list end_or_eof)
		  | FUNCTION LBRACKET RBRACKET ASSIGN IDENT (LPAREN RPAREN)? SEMIC? NEWLINE statement_list end_or_eof -> ^(FUNC_DEF IDENT ID_LIST ID_LIST statement_list end_or_eof);

func_handle:	'@' LPAREN id_list RPAREN expr -> ^(FUNC_HANDLE ^(IDENT["anonymous"]) ^(ID_LIST) id_list ^(STATEMENT_LIST expr))
           |    '@' LPAREN RPAREN expr -> ^(FUNC_HANDLE ^(IDENT["anonymous"]) ^(ID_LIST) ^(ID_LIST) ^(STATEMENT_LIST expr))
           |    '@' IDENT -> ^(FUNC_HANDLE IDENT);
		      
fcall: IDENT (LPAREN params RPAREN)+ -> ^(FUNC IDENT params+)
     | CLEAR LPAREN params? RPAREN -> ^(FUNC IDENT["clear"] params?)
	 | IDENT LPAREN RPAREN -> ^(FUNC IDENT);

ident_or_number: (IDENT NUMBER?) -> IDENT NUMBER?
            | NUMBER -> NUMBER;

ident_group:   ident_or_number+ -> ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] ident_or_number QUOTE["\'"])+);

ident_group2: path+ -> ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] path QUOTE["\'"])+);

ident_group3: ident_or_global+ -> ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] ident_or_global QUOTE["\'"])+);

alt_fcall: IDENT ident_group -> ^(FUNC IDENT ident_group);

alt_fcall2: IDENT ident_group2 -> ^(FUNC IDENT ident_group2);

clear:	CLEAR ident_group3 -> ^(FUNC IDENT["clear"] ident_group3)
     |  CLEAR GLOBAL -> ^(FUNC IDENT["clear"] ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] GLOBAL QUOTE["\'"])))
     |  CLEAR -> ^(FUNC IDENT["clear"]);

mr_fcall: LBRACKET id_list2 RBRACKET ASSIGN IDENT (LPAREN params? RPAREN)? -> ^(MR_FUNC IDENT id_list2 params?);

cell_extract: LBRACKET id_list RBRACKET ASSIGN cell_value -> ^(CELL_EXTRACT id_list cell_value)
            | LBRACKET id_list RBRACKET ASSIGN indexExpr -> ^(CELL_EXTRACT id_list indexExpr);

kludge_index:	LPAREN params RPAREN -> params
            |   LPAREN RPAREN -> ^(PARAM_LIST);

ident_or_keyword: (IDENT | keywords) -> IDENT? keywords?;
            
substruct:	DOT ident_or_keyword kludge_index? substruct? -> ^(FIELD ident_or_keyword kludge_index? substruct?)
         |	DOT LPAREN expr RPAREN kludge_index? substruct? -> ^(DYN_FIELD expr kludge_index? substruct?);

struct_member:	IDENT (LPAREN params? RPAREN)? (substruct)+ -> ^(STRUCT ^(IDENT) params? substruct+)
             |  cell_value (substruct)+ -> ^(STRUCT cell_value substruct+);

expr_or_colon:	(expr | COLON) -> expr? COLON?;

params: expr_or_colon (COMMA NEWLINE? expr_or_colon)* -> ^(PARAM_LIST expr_or_colon+);

id_list: IDENT (comma_or_ws IDENT)* -> ^(ID_LIST IDENT+);

id_or_fcall: (IDENT | fcall) -> IDENT? fcall?;

id_list2: id_or_fcall (comma_or_ws id_or_fcall)* -> ^(ID_LIST id_or_fcall+);

switchcase	:	SWITCH e1=expr comment_or_newline_strict+ (CASE en+=expr comment_or_newline_strict+ sn+=statement_list)+ (OTHERWISE comment_or_newline_strict+ ot=statement_list)? END comment_or_newline? -> ^(SWITCH $e1 ^(CASE $en $sn)+ ^(OTHERWISE $ot)?); // can't call this switch
	
conditional
	:	IF e1=expr comment_or_newline s1=statement_list (ELSEIF e2+=expr comment_or_newline s2+=statement_list)* (OTHER comment_or_newline sz=statement_list?)? END comment_or_newline -> ^(CONDITIONAL ^(IF $e1 $s1) ^(IF $e2 $s2)* ^(ELSE $sz?));

conditional2
	:	IF2 e1=expr comma_or_sc? s1=stmt_group2  (ELSEIF comma_or_sc? e2+=expr s2+=stmt_group2)* (OTHER comma_or_sc? sz=stmt_group2?)? END -> ^(CONDITIONAL ^(IF $e1 ^(STATEMENT_LIST $s1)) ^(IF $e2 ^(STATEMENT_LIST $s2))* ^(ELSE ^(STATEMENT_LIST $sz)?));

whileloop
	:	WHILE expr comment_or_newline statement_list END NEWLINE? -> ^(WHILE expr statement_list);

forloop:	FOR LPAREN* IDENT ASSIGN expr RPAREN* SEMIC? comment_or_newline? statement_list? END comment_or_newline? -> ^(FOR IDENT expr statement_list?);

forloop2: FOR IDENT ASSIGN expr comma_or_sc? substmt stmt2* cs=comma_or_sc? END -> ^(FOR IDENT expr ^(STMT substmt stmt2* $cs?));

assign	:	IDENT (LPAREN params RPAREN)? ASSIGN expr -> ^(ASSIGN IDENT expr params?)
	    |	IDENT (LCURLY params RCURLY)+ ASSIGN expr -> ^(CELL_ASSIGN IDENT expr params+)
		|   struct_member ASSIGN expr -> ^(ASSIGN struct_member expr)
		|   struct_member (LCURLY params RCURLY) ASSIGN expr -> ^(CELL_ASSIGN struct_member expr params);

global	:	GLOBAL IDENT+ comment_or_newline -> ^(GLOBAL IDENT+);

persistent:	PERSISTENT IDENT+ comment_or_newline -> ^(PERSISTENT IDENT+);

comment_or_newline	:	SEMIC? (comment | NEWLINE | EOF) -> SEMIC?;

comment_or_newline_strict: (comment | NEWLINE);

trycatch:	TRY NEWLINE s1=statement_list ('catch' IDENT? NEWLINE s2=statement_list)? END -> ^(TRY $s1 $s2? IDENT?);

substmt	: mr_fcall
        | assign
		| alt_fcall
		| clear
		| expr
		| forloop2
		| conditional2;

stmt2	: comma_or_sc substmt -> comma_or_sc substmt;

stmt_group: s1=substmt stmt2* COMMA? comment_or_newline -> ^(STMT $s1 stmt2* comment_or_newline?);

stmt_group2: s1=substmt stmt2* comma_or_sc? -> ^(STMT $s1 stmt2* comma_or_sc?);

properties: PROPERTIES NEWLINE (IDENT NEWLINE)+ END -> ^(ID_LIST IDENT+);

methods: METHODS NEWLINE (funcdef NEWLINE)+ END -> ^(FUNC_LIST funcdef+);

classdef: CLASSDEF IDENT NEWLINE properties NEWLINE methods NEWLINE END -> ^(CLASSDEF IDENT properties methods);

stmt: 
    stmt_group
	| conditional
	| switchcase
    | whileloop
    | forloop
	| funcdef NEWLINE? -> funcdef
    | mr_funcdef
    | cell_extract comment_or_newline -> ^(STMT cell_extract comment_or_newline?)
	| global
    | persistent
	| trycatch
    | RETURN comment_or_newline
    | NEWLINE -> DUMMY
	| comment -> DUMMY
	| alt_fcall2
	| classdef
    ;

ASSIGN: '=';

prog: statement_list EOF -> statement_list
    | EOF -> DUMMY;

//statement_list
//    : (stmt {pANTLR3_STRING s = $stmt.tree->toStringTree($stmt.tree);
//             assert(s->chars);
//             printf(" tree \%s\n", s->chars);
//            }
//        )+ -> ^(STATEMENT_LIST stmt+)
//    ;

statement_list : stmt+ -> ^(STATEMENT_LIST stmt+);

IDENT: ('a'..'z'|'A'..'Z')('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ;
NUMBER: '0'..'9'+ ('.' ('0'..'9')*)? (('E'|'e'|'D'|'d')('-'|'+')?('0'..'9')+)? ('i'|'j'|'I'|'J')?
      | '.''0'..'9'+ (('E'|'e'|'D'|'d')('-'|'+')?('0'..'9')+)? ('i'|'j'|'I'|'J')?;
NEWLINE: '\r'? '\n' ;
WS : (' '|'\t')+ {$channel = HIDDEN;}; 
CONTHACK: '...' WS;
CONT: '...' WS? NEWLINE {$channel = HIDDEN;};
UNICHAR	: ('\u00a0'..'\ufffe')+;
HACK: '0'..'9'+ ('.' ('0'..'9')*)? ('E'|'e'|'D'|'d')('-'|'+')?;
HACKB: '.''0'..'9'+ ('E'|'e'|'D'|'d');