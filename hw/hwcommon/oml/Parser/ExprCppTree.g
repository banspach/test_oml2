grammar ExprCppTree;

options {
    language = C;
    output = AST;
    ASTLabelType = pANTLR3_BASE_TREE;
	backtrack = true;
	memoize = true;
}

tokens {
    FUNC;
	MR_FUNC;
	PARAM_LIST;
	CELL_PARAM_LIST;
	ID_LIST;
	UMINUS;
	STATEMENT_LIST;
    FUNC_DEF;
    CONDITIONAL;
    ELSE;
	VECTOR;
	MATRIX;
    CELL_ARRAY;
    CELL_VAL;
	DUMMY;
    HML_STRING;
    TRANSP;
    CTRANSP;
    FUNC_HANDLE;
    INLINE_INDEX;
	INLINE_INDEX_CELL;
    STRUCT;
    FIELD;
	DYN_FIELD;
	STMT;
	UGLY1;
	UGLY2;
	UGLY3;
	UGLY4;
	INPLACE;
	QUOTE2;
	FUNC_LIST;
	PARSE_DUMMY;
}

@header {
   	#include "../../Runtime/ANTLRoverride.h"
}

@parser::apifuncs {
	RECOGNIZER->displayRecognitionError = displayRecognitionErrorNew;
}

@lexer::header {
	#include "../../Runtime/ANTLRoverride.h"
}

@lexer::apifuncs {
	RECOGNIZER->displayRecognitionError = displayRecognitionErrorNew;
}

// The suffix '^' means make it a root.
// The suffix '!' means ignore it.

IF	:   'if';
WHILE:	'while';
FOR:	'for';
PARFOR: 'parfor';
SWITCH:	'switch';
CASE:	'case';
OTHERWISE:	'otherwise';
GLOBAL:	'global';
PERSISTENT: 'persistent';
RETURN:	'return';
END:	'end';
TRY:	'try';
FUNCTION: 'function';
CLEAR: 'clear';
CLEARVARS: 'clearvars';
ELSEIF: 'elseif';
OTHER: 'else';
CLASSDEF: 'classdef';
METHODS: 'methods';
PROPERTIES: 'properties';

EQUAL:  '==';
NEQUAL:	'~=';
PLUS:   '+';
MINUS:  '-';
TIMES:  '*';
DIV:    '/';
ETIMES: '.*';
EDIV:	'./';
LDIV:	'\\';
ELDIV:	'.\\';
QUOTE:  '\'';
EQUOTE: '\'\'';
COLON:	':';
POW:    '^';
LTHAN:	'<';
GTHAN:	'>';
LEQ:	'<=';
GEQ:	'>=';
DOT:	'.';
DOTPOW:	'.^';
AND	:	'&';
OR	:	'|';
LAND:	'&&';
LOR	:	'||';
NEGATE: '~';
SEMIC:  ';';
COMMA:  ',';
DOTDOT:	'..';
LBRACKET: '[';
RBRACKET: ']';
LPAREN: '(';
RPAREN: ')';
LCURLY: '{';
RCURLY: '}';
PERCENT: '%';
AMP: '@';

PCTLCURLY: '%{';
RCURLYPCT: '%}';

nl_or_sc: SEMIC | NEWLINE -> ;

nl_or_sc_or_comment: ((SEMIC comment?) | comment | NEWLINE) -> ;	 

matrix	:	LBRACKET comment_or_newline_strict* exprlist (nl_or_sc_or_comment comment_or_newline_strict* exprlist)* comment_or_newline_strict* RBRACKET -> ^(MATRIX exprlist+)
        |   LBRACKET comma_or_sc* RBRACKET                  -> ^(MATRIX);

cell 	:	LCURLY comment_or_newline_strict* exprlist (nl_or_sc_or_comment comment_or_newline_strict* exprlist)* comment_or_newline_strict* RCURLY -> ^(CELL_ARRAY exprlist+)
        |   LCURLY RCURLY                               -> ^(CELL_ARRAY);

comma_or_sc:	(COMMA | SEMIC) -> COMMA? SEMIC?;

exprlist:	comma_or_sc* expr (COMMA? expr)* comma_or_sc* -> ^(VECTOR expr+);

expr	:	expr2 ((OR^ | LOR^) expr2)*;

expr2: ineqExpr ((AND^ | LAND^) ineqExpr)*; 

ineqExpr:	colonExpr ((EQUAL^ | NEQUAL^ | LTHAN^ | GTHAN^ | LEQ^ | GEQ^) colonExpr)*;

colonExpr:	addExpr (COLON^ addExpr)*;

addExpr_or_end: (addExpr | END) -> addExpr? END?;

addExpr: multExpr ((PLUS^ | MINUS^) multExpr)*;

multExpr: unaryExpr ((TIMES^ | DIV^ | ETIMES^ | EDIV^ | LDIV^ | ELDIV^) unaryExpr)*;

unaryExpr: MINUS unaryExpr -> ^(UMINUS unaryExpr)
	     | PLUS  unaryExpr -> unaryExpr
		 | NEGATE unaryExpr -> ^(NEGATE unaryExpr)
		 | powExpr -> powExpr;

powExpr	: postExpr2 ((POW^ | DOTPOW^) MINUS? PLUS? postExpr2)*;

postExpr2: (indexExpr->indexExpr) ( QUOTE  -> ^(CTRANSP $postExpr2)
         						| DOT QUOTE  -> ^(TRANSP $postExpr2)
         						)
		 | string LPAREN params RPAREN -> ^(FUNC string params)
		 | string           -> string
         | indexExpr        -> indexExpr;

indexExpr: (postExpr->postExpr) ( LPAREN params RPAREN -> ^(FUNC $indexExpr params)
						| LPAREN RPAREN -> ^(FUNC $indexExpr ^(PARAM_LIST))
                        | LCURLY params RCURLY -> ^(CELL_VAL $indexExpr params)
                        | DOT ident_or_keyword -> ^(STRUCT $indexExpr ident_or_keyword)
			            | DOT LPAREN expr RPAREN -> ^(STRUCT $indexExpr ^(DYN_FIELD expr))
                        )*
         | postExpr -> postExpr; 

postExpr: atom ugly atom -> ^(ugly ^(TRANSP atom) atom)
		| atom QUOTE -> ^(CTRANSP atom)
        | atom DOT QUOTE -> ^(TRANSP atom)
		| atom        -> atom;

ugly1: QUOTE POW -> ^(UGLY1);
ugly2: QUOTE DOTPOW -> ^(UGLY2);
ugly3: DOT QUOTE POW -> ^(UGLY3);
ugly4: DOT QUOTE DOTPOW -> ^(UGLY4);

ugly: (ugly1 | ugly2 | ugly3 | ugly4) -> ugly1? ugly2? ugly3? ugly4?;

equotes : EQUOTE+;

string	: QUOTE chars* QUOTE -> ^(HML_STRING QUOTE chars* QUOTE)
		| EQUOTE equotes -> ^(HML_STRING QUOTE equotes QUOTE)
        | EQUOTE -> ^(HML_STRING);

chars_or_quote: (chars | QUOTE) -> chars? QUOTE?;

comment	: PERCENT chars_or_quote* (NEWLINE | EOF) -> ^(DUMMY chars_or_quote*);

keywords : (keywords2 | END) -> keywords2? END?;

keywords2: (IF | FOR | WHILE | SWITCH | CASE | TRY | OTHERWISE | RETURN | GLOBAL | PERSISTENT | FUNCTION | OTHER | ELSEIF | 'catch' | PROPERTIES | METHODS | CLASSDEF | PARFOR | CLEAR | CLEARVARS) -> IF? FOR? WHILE? SWITCH? CASE? TRY? OTHERWISE? RETURN? GLOBAL? PERSISTENT? FUNCTION? OTHER? ELSEIF? 'catch'? PROPERTIES? METHODS? CLASSDEF? PARFOR? CLEAR? CLEARVARS?;

operators : (EDIV | ELDIV | '=' | '*' | '+' | '-' | '/' | '\\' | COLON | POW | LTHAN | GTHAN | LEQ | GEQ | AND | OR | NEGATE | EQUAL | NEQUAL | LAND | LOR | DOTPOW | ETIMES) -> EDIV? ELDIV? '='? '*'? '+'? '-'? '/'? '\\'? COLON? POW? LTHAN? GTHAN? LEQ? GEQ? AND? OR? NEGATE? EQUAL? NEQUAL? LAND? LOR? DOTPOW? ETIMES?;

specials: (DOTDOT | EQUOTE | COMMA | '.' | '"' | LPAREN | RPAREN | LBRACKET | RBRACKET | LCURLY | RCURLY | PERCENT | CLEAR | CLEARVARS | SEMIC | NUMBERHACK | '?' | '!' | '...' | '_' | AMP | '#' | '$' | '`' | PCTLCURLY | RCURLYPCT) -> DOTDOT? EQUOTE? COMMA? '.'? '"'? LPAREN? RPAREN? LBRACKET? RBRACKET? LCURLY? RCURLY? PERCENT? CLEAR? CLEARVARS? SEMIC? NUMBERHACK? '?'? '!'? '...'? '_'? AMP? '#'? '$'? '`'? PCTLCURLY? RCURLYPCT?;

chars   : (IDENT | IDENT2 | IDENT2HACK | NUMBER | HEXVAL | HEXHACK | UNICHAR_IDENT | keywords | operators | specials | HACK | HACKB) -> IDENT? IDENT2? IDENT2HACK? NUMBER? HEXVAL? HEXHACK? UNICHAR_IDENT? keywords? operators? specials? HACK? HACKB?;  

path_chars: ('.' | DOTDOT | EDIV | ELDIV | '/' | '\\' | COLON) -> '.'? DOTDOT? EDIV? ELDIV? '/'? '\\'? COLON?;

path	:	(IDENT? path_chars+ IDENT?)+;

wildcard:	 (TIMES? IDENT TIMES)+ IDENT?
        |    (TIMES IDENT TIMES?)+
	    |    IDENT -> IDENT;    

option: MINUS IDENT;

ident_or_global: (wildcard | GLOBAL | option) -> wildcard? GLOBAL? option?;

atom: NUMBER
    | HEXVAL
    | IDENT
	| IDENT2
    | LPAREN! expr RPAREN!
	| matrix
	| cell
    | func_handle
    ;

end_or_eof:	END SEMIC? -> END
          | EOF -> EOF;

func_begin: FUNCTION (f1=IDENT ASSIGN)? f2=IDENT -> $f2 ^(ID_LIST $f1?);

mrfunc_begin: FUNCTION LBRACKET f1=id_list RBRACKET ASSIGN IDENT -> IDENT $f1;

funcdef1 : func_begin ( LPAREN id_list3 RPAREN SEMIC? comment_or_newline_strict statement_list? end_or_eof -> ^(FUNC_DEF func_begin id_list3 statement_list? end_or_eof)
                      | (LPAREN RPAREN)? SEMIC? comment_or_newline_strict statement_list? end_or_eof -> ^(FUNC_DEF func_begin ID_LIST statement_list? end_or_eof));

funcdef2: FUNCTION (f1=IDENT ASSIGN)? f2=IDENT ( LPAREN id_list RPAREN COMMA? SEMIC? substmt stmt2* comma_or_sc? END SEMIC? -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) id_list ^(STMT substmt stmt2 comma_or_sc?))
                                               | (LPAREN RPAREN)? COMMA? SEMIC? substmt stmt2* comma_or_sc? END SEMIC? -> ^(FUNC_DEF $f2 ^(ID_LIST $f1?) ID_LIST ^(STMT substmt stmt2 comma_or_sc?)));

funcdef: (funcdef1 | funcdef2) -> funcdef1? funcdef2?;

parse_dummy: parse_dummy2
			 | IF expr -> ^(PARSE_DUMMY expr)
			 | ELSEIF expr -> ^(PARSE_DUMMY expr)
			 | OTHER
			 | func_begin LPAREN id_list3? RPAREN-> ^(PARSE_DUMMY)
			 | mrfunc_begin LPAREN id_list3? RPAREN-> ^(PARSE_DUMMY)
			 | class_begin -> ^(PARSE_DUMMY)
			 | methods_begin -> ^(PARSE_DUMMY);

parse_dummy2: FOR LPAREN* IDENT ASSIGN expr RPAREN* -> ^(PARSE_DUMMY expr)
			 | WHILE expr -> ^(PARSE_DUMMY expr);


mr_funcdef: FUNCTION LBRACKET f1=id_list RBRACKET ASSIGN IDENT LPAREN f2=id_list RPAREN SEMIC? comment_or_newline_strict statement_list end_or_eof -> ^(FUNC_DEF IDENT $f1 $f2 statement_list end_or_eof)
          | FUNCTION LBRACKET RBRACKET ASSIGN IDENT LPAREN f2=id_list RPAREN SEMIC? comment_or_newline_strict statement_list end_or_eof -> ^(FUNC_DEF IDENT ID_LIST $f2 statement_list end_or_eof)
          | FUNCTION LBRACKET f1=id_list RBRACKET ASSIGN IDENT (LPAREN RPAREN)? SEMIC? comment_or_newline_strict statement_list end_or_eof -> ^(FUNC_DEF IDENT $f1 ID_LIST statement_list end_or_eof)
		  | FUNCTION LBRACKET RBRACKET ASSIGN IDENT (LPAREN RPAREN)? SEMIC? comment_or_newline_strict statement_list end_or_eof -> ^(FUNC_DEF IDENT ID_LIST ID_LIST statement_list end_or_eof);

func_handle:	AMP LPAREN id_list RPAREN expr -> ^(FUNC_HANDLE ^(IDENT["anonymous"]) ^(ID_LIST) id_list ^(STATEMENT_LIST expr))
           |    AMP LPAREN RPAREN expr -> ^(FUNC_HANDLE ^(IDENT["anonymous"]) ^(ID_LIST) ^(ID_LIST) ^(STATEMENT_LIST expr))
           |    AMP IDENT -> ^(FUNC_HANDLE IDENT);
		      
ident_or_number: (ident_or_keyword NUMBER?) -> ident_or_keyword NUMBER?
            | NUMBER -> NUMBER;

ident_group:   ident_or_number+ -> ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] ident_or_number QUOTE["\'"])+);

ident_group2: path+ -> ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] path QUOTE["\'"])+);

ident_group3: ident_or_global+ -> ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] ident_or_global QUOTE["\'"])+);

alt_fcall: IDENT ident_group -> ^(FUNC IDENT ident_group);

alt_fcall2: IDENT ident_group2 -> ^(FUNC IDENT ident_group2);

clear:	CLEAR ident_group3 -> ^(FUNC CLEAR ident_group3)
     |  CLEAR LPAREN params? RPAREN -> ^(FUNC IDENT["clear"] params?)
     |  CLEAR GLOBAL -> ^(FUNC IDENT["clear"] ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] GLOBAL QUOTE["\'"])))
     |  CLEAR -> ^(FUNC CLEAR);

clearvars:	CLEARVARS ident_group3 -> ^(FUNC CLEARVARS ident_group3)
     |  CLEARVARS LPAREN params? RPAREN -> ^(FUNC IDENT["clearvars"] params?)
     |  CLEARVARS GLOBAL -> ^(FUNC IDENT["clearvars"] ^(PARAM_LIST ^(HML_STRING QUOTE["\'"] GLOBAL QUOTE["\'"])))
     |  CLEARVARS -> ^(FUNC CLEARVARS);

mult_assign: LBRACKET id_list2 RBRACKET ASSIGN indexExpr -> ^(MR_FUNC id_list2 indexExpr);                                       

flexible_index:	LPAREN params RPAREN -> params
              |   LPAREN RPAREN -> ^(PARAM_LIST);

ident_or_keyword: (IDENT | keywords2) -> IDENT? keywords2?;
            
expr_or_colon:	(expr | COLON) -> expr? COLON?;

params: expr_or_colon (COMMA NEWLINE? expr_or_colon)* -> ^(PARAM_LIST expr_or_colon+);

cell_params: expr_or_colon (COMMA NEWLINE? expr_or_colon)* -> ^(CELL_PARAM_LIST expr_or_colon+);

id_list: IDENT (COMMA? IDENT)* -> ^(ID_LIST IDENT+);

base_class_list: IDENT (AND? IDENT)* -> ^(ID_LIST IDENT+);

special_id: (indexExpr | NEGATE) -> indexExpr? NEGATE?;

id_list2: special_id (COMMA? special_id)* -> ^(ID_LIST special_id+);

ident_default: IDENT (ASSIGN expr)? -> ^(IDENT expr?)
             | NEGATE -> NEGATE;

id_list3: ident_default (COMMA ident_default)* -> ^(ID_LIST ident_default+);

caseblock:	CASE expr COMMA? ( comment_or_newline_strict+ statement_list? -> ^(CASE expr statement_list?)
							 | stmt_group comment_or_newline_strict+ -> ^(CASE expr ^(STATEMENT_LIST stmt_group)));

otherwiseblock:	OTHERWISE COMMA? ( comment_or_newline_strict+ statement_list? -> ^(OTHERWISE statement_list?)
								 | stmt_group comment_or_newline_strict+ -> ^(OTHERWISE ^(STATEMENT_LIST stmt_group)));

switchcase	:	SWITCH e1=expr COMMA? comment_or_newline_strict+ caseblock+ otherwiseblock? END comment_or_newline? -> ^(SWITCH expr caseblock+ otherwiseblock?); // can't call this switch
	
else_clause: else_clause1 -> else_clause1
           | else_clause2 NEWLINE -> else_clause2;

else_clause1: OTHER COMMA? comment_or_newline sz=statement_list2? -> $sz?;

else_clause2: OTHER comma_or_sc? sz=stmt_group? -> ^(STATEMENT_LIST $sz)?;

elseif_clause: elseif_clause1 -> elseif_clause1
             | elseif_clause2 NEWLINE? -> elseif_clause2;

elseif_clause1: ELSEIF e1=expr comma_or_sc? comment_or_newline s1=statement_list2? -> ^(IF $e1 $s1?);

elseif_clause2: ELSEIF comma_or_sc? e2+= expr comma_or_sc? s2=stmt_group? comment_or_newline2* -> ^(IF $e2 ^(STATEMENT_LIST $s2))?;

if_clause: if_clause1 -> if_clause1
         | if_clause2 -> if_clause2;

if_clause1: IF e1=expr comma_or_sc? comment_or_newline2+ s1=statement_list2? -> ^(IF $e1 $s1);

if_clause2: IF e1=expr comma_or_sc? s1=stmt_group comment_or_newline2* -> ^(IF $e1 ^(STATEMENT_LIST $s1));

conditional
	:	 if_clause elseif_clause* else_clause? END COMMA? comment_or_newline -> ^(CONDITIONAL if_clause elseif_clause* ^(ELSE else_clause?));

conditional2
	:	if_clause2  elseif_clause2* else_clause2? END -> ^(CONDITIONAL if_clause2 elseif_clause2* ^(ELSE else_clause2?));

whileloop
	:	WHILE expr COMMA? comment_or_newline statement_list END SEMIC? NEWLINE? -> ^(WHILE expr statement_list);

whileloop2
	:	WHILE expr comma_or_sc? substmt stmt2* cs=comma_or_sc? END -> ^(WHILE expr ^(STMT substmt stmt2* $cs?));

forloop:	FOR LPAREN* IDENT ASSIGN expr RPAREN* comma_or_sc? comment_or_newline? statement_list? END comment_or_newline? -> ^(FOR IDENT expr statement_list?);

forloop2: FOR IDENT ASSIGN expr comma_or_sc? substmt stmt2* cs=comma_or_sc? END -> ^(FOR IDENT expr ^(STMT substmt stmt2* $cs?));

forloop3: FOR IDENT ASSIGN expr comma_or_sc? END -> ^(FOR IDENT expr);

parforloop:	PARFOR LPAREN* IDENT ASSIGN expr RPAREN* comma_or_sc? comment_or_newline? statement_list? END comment_or_newline? -> ^(PARFOR IDENT expr statement_list?);

assign : indexExpr ASSIGN expr -> ^(ASSIGN indexExpr expr);

global	:	GLOBAL IDENT+ comment_or_newline -> ^(GLOBAL IDENT+);

global2	:	GLOBAL IDENT+ -> ^(GLOBAL IDENT+);

persistent:	PERSISTENT IDENT+ comment_or_newline -> ^(PERSISTENT IDENT+);

comment_or_newline	:	SEMIC? (NEWLINE | comment | EOF) -> SEMIC?;

comment_or_newline2	:	SEMIC? (NEWLINE | comment) -> SEMIC?;

comment_or_newline_strict: (NEWLINE | comment);

trycatch:	TRY comment_or_newline s1=statement_list ('catch' IDENT? comment_or_newline s2=statement_list?)? END -> ^(TRY $s1 $s2? IDENT?);

substmt	: mult_assign
        | assign
		| alt_fcall
		| clear
		| clearvars
		| expr
		| forloop2
		| forloop3
		| whileloop2
		| conditional2
		| global2
		| RETURN;

stmt2	: comma_or_sc+ substmt -> comma_or_sc substmt;

stmt_group: s1=substmt stmt2* comma_or_sc* -> ^(STMT $s1 stmt2* comma_or_sc?);

properties: PROPERTIES (LPAREN p1=IDENT ASSIGN p2=IDENT RPAREN)? comment_or_newline2+ (p3+=ident_default comment_or_newline2+)+ END comment_or_newline2* -> ^(PROPERTIES ^(ID_LIST $p3+) $p1? $p2?)
          | PROPERTIES comment_or_newline2+ END comment_or_newline2 -> ^(PROPERTIES);

func_or_mrfunc_def: (funcdef | mr_funcdef) -> funcdef? mr_funcdef?;

methods_begin: METHODS -> METHODS; 

methods: methods_begin (LPAREN p1=IDENT ASSIGN p2=IDENT RPAREN)? comment_or_newline2+ (func_or_mrfunc_def comment_or_newline2+)+ END comment_or_newline2* -> ^(METHODS ^(FUNC_LIST func_or_mrfunc_def+) $p1? $p2?);

class_begin: CLASSDEF IDENT (LTHAN base_class_list)? -> IDENT base_class_list?;

classdef: class_begin comment_or_newline2+ properties* comment_or_newline2* methods* comment_or_newline2* END -> ^(CLASSDEF class_begin properties* methods*);

nl_or_eof: (comment_or_newline_strict | EOF) -> comment_or_newline_strict? EOF?;

stmt: 
    base_stmt
	| SEMIC -> DUMMY
	| parse_dummy
    ;

alt_stmt: 
    base_stmt
	| parse_dummy2
    ;

base_stmt:
    stmt_group nl_or_eof -> stmt_group
	| conditional
	| switchcase
    | whileloop
    | forloop
	| parforloop
	| funcdef NEWLINE? -> funcdef
    | mr_funcdef
	| global
    | persistent
	| trycatch
    | NEWLINE -> DUMMY
	| comment
	| alt_fcall2 SEMIC? -> alt_fcall2
	| classdef
	| RETURN comment_or_newline;

ASSIGN: '=';

prog: statement_list EOF -> statement_list
    | EOF -> DUMMY;

statement_list : stmt+ -> ^(STATEMENT_LIST stmt+);

statement_list2 : alt_stmt+ -> ^(STATEMENT_LIST alt_stmt+);

fragment LETTER:  'a'..'z' | 'A'..'Z';
fragment UNICHAR: '\u00a0'..'\ufffe';
fragment DIGIT:   '0'..'9';

IDENT: (LETTER|'_')(LETTER|DIGIT|'_')*;
IDENT2: (LETTER|'_')(LETTER|DIGIT|'_')*AMP(LETTER|'_')(LETTER|DIGIT|'_')*;
IDENT2HACK: (LETTER|'_')(LETTER|DIGIT|'_')*AMP;
UNICHAR_IDENT: (LETTER|UNICHAR)(LETTER|DIGIT|'_'|UNICHAR)* ;
NUMBERHACK: DIGIT+ '.' DIGIT+ '.';
NUMBER: DIGIT+ ('.' (DIGIT)*)? (('E'|'e'|'D'|'d')('-'|'+')?(DIGIT)+)? ('i'|'j'|'I'|'J')?
      | '.'DIGIT+ (('E'|'e'|'D'|'d')('-'|'+')?(DIGIT)+)? ('i'|'j'|'I'|'J')?;
HEXVAL: '0x'(DIGIT|'a'..'f'|'A'..'F')+;
HEXHACK: '0x';
NEWLINE: '\r'? '\n' ;
WS : (' '|'\t')+ {$channel = HIDDEN;}; 
CONT: '...';
HACK: DIGIT+ ('.' (DIGIT)*)? ('E'|'e'|'D'|'d')('-'|'+')?;
HACKB: '.'DIGIT+ ('E'|'e'|'D'|'d');