/**
* @file main.cpp
* @date November 2018
* Copyright (C) 2018-2022 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include <fstream>
#include <iostream>

#ifdef OS_WIN
#    include <windows.h>
#    include <tchar.h>
#    include <system_error>
	BOOL ExitSignalHandler(DWORD);
#else
#    include <csignal>
	void ExitSignalHandler(int);

#endif

#include "BuiltInFuncsCore.h"
#include "Interpreter.h"
#include "BridgeServer.h"
#include "hwMatrix_NMKL.h"


hwMatrix*      ReadMatrix(FILE* file);
std::string    ReadString(FILE* file);
int            ReadInt(FILE* file);
double         ReadDouble(FILE* file);
HML_CELLARRAY* ReadStringList(FILE* file);

void WriteCurrency(FILE* file, const Currency& cur);
void WriteCurrencyList(FILE* file, const Currency& cur);

void WriteInt(FILE* file, int val);
void WriteDouble(FILE* file, double val);
void WriteString(FILE* file, const char* str);
void WriteMatrix(FILE* file, const double* data, int nrows, int ncols);
void WriteStringList(FILE* file, char** data, int nstrings);
void WriteError(FILE* file, const char* str);

Currency EvaluateString(Interpreter& interp, FILE* file);
Currency CallFunction(Interpreter& interp, FILE* file);
Currency CallMRFunction(Interpreter& interp, FILE* file);
Currency SetValue(Interpreter& interp, FILE* file);
Currency GetValue(Interpreter& interp, FILE* file);

bool OmlVersion(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs);

void Cleanup();      // Cleanup, checkin, closing error logs
void Authorize();    // Authorization
void Initialize();   // Initialization, redirection of stderr

// Global variables/typedefs
#define OML_PRODUCT "Altair Compose"
#define OML_VERSION "2022.3"

std::string g_stderrfile; // Stderr file
std::string g_unityroot;  // Unity root path

// Licensing function pointers and variables
Interpreter g_interp;

void*       g_licHandle      = nullptr;
bool        g_isProfessional = false;
std::string g_edition;
typedef int (*lic_auth_fptr) (const std::string&, const std::string&, int);
typedef bool (*lic_ispro_fptr)();
typedef void (*lic_checkin_fptr)();
typedef bool (*lic_edition_fptr)(std::string&, bool);
typedef void (*lic_resetedition_fptr)(const std::string&);

std::fstream g_errfile;

class OMLBridgeError
{
public:
	OMLBridgeError() {}
};

//------------------------------------------------------------------------------
// Oml bridge
//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	if (argc < 3)
		return -1;

	// Redirects std::cerr, sets up env variables
	Initialize(); 

	FILE* file_in  = fdopen(atoi(argv[1]), "rb");
	FILE* file_out = fdopen(atoi(argv[2]), "wb");

	double version = ReadDouble(file_in);
	int mode = 0;

	try
	{
		while (1)
		{
			mode = ReadInt(file_in);

			Currency result(-1.0, Currency::TYPE_NOTHING);

			if (mode == MODE_EVALUATE_STRING)       // Register function
				result = EvaluateString(g_interp, file_in);
			else if (mode == MODE_SET_VALUE)
				result = SetValue(g_interp, file_in);
			else if (mode == MODE_GET_VALUE)
				result = GetValue(g_interp, file_in);
			else if (mode == MODE_CALL_FUNCTION)   // Calls oml function
			{
				Authorize();
				result = CallFunction(g_interp, file_in);
			}
			else if (mode == MODE_CALL_MR_FUNCTION)   // Calls oml function
			{
				Authorize();
				result = CallMRFunction(g_interp, file_in);
			}
            else if (mode == MODE_EXIT)
            {
				std::cerr << "In exit mode..." << std::endl;
				Cleanup();
#ifdef OS_WIN
                exit(0);
#else
                quick_exit(0);
#endif
            }
			else
				result = Currency(-99.999);

			// write the result back to the other file
			WriteCurrency(file_out, result);
            fflush(stderr);
		}
	}
	catch (...)
	{
		fflush(stderr);
		WriteError(file_in, "Error");
	}

	Cleanup(); // License cleanup
	return 0;
}
void WriteCurrency(FILE* file, const Currency& cur)
{
	if (cur.IsMatrix())
	{
		const hwMatrix* mtx = cur.Matrix();

		// need to switch data to row major

		int index_rm;
		int index_cm;
		
		int dim_1 = mtx->M();
		int dim_2 = mtx->N();

		if (mtx->IsReal())
		{
			const double* orig = mtx->GetRealData();

			double* fixed = new double[mtx->Size()];

			for (int j=0; j<dim_1; j++)
			{
				for (int k=0; k<dim_2; k++)
				{
					index_rm = k + j*dim_2;
					index_cm = j + k*dim_1;

					fixed[index_rm] = orig[index_cm];
				}
			}

			WriteMatrix(file, fixed, mtx->M(), mtx->N());
		}
		else
		{
			WriteError(file, "Complex data not supported");
		}
	}
	else if (cur.IsScalar())
	{
		WriteDouble(file, cur.Scalar());
	}
	else if (cur.IsComplex())
	{
		WriteError(file, "Complex data not supported");
	}
	else if (cur.IsString())
	{
		WriteString(file, cur.StringVal().c_str());
	}
	else if (cur.IsCellList())
	{
		WriteCurrencyList(file, cur);
	}
	else if (cur.IsCellArray())
	{
		HML_CELLARRAY *cells = cur.CellArray();

		char** text = new char*[cells->Size()];

		for (int j=0; j<cells->Size(); j++)
		{
			Currency cur = (*cells)(j);

#ifdef OS_WIN
			if (cur.IsString())
				text[j] = _strdup(cur.StringVal().c_str());
			else
				text[j] = _strdup("");
#else
			if (cur.IsString())
				text[j] = strdup(cur.StringVal().c_str());
			else
				text[j] = strdup("");
#endif
		}

		WriteStringList(file, text, cells->Size());
	}
	else if (cur.IsError())
	{
		WriteError(file, cur.Message().c_str());
	}
	else if (cur.IsNothing())
	{
		// we need to send something back or else the caller will be waiting indefinitely
		WriteDouble(file, 0.0);
	}

	fflush(file);
}

void WriteCurrencyList(FILE* file, const Currency& cur)
{
	size_t res;
	int type = TYPE_CELL_LIST;

	res = fwrite(&type, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	HML_CELLARRAY* cells = cur.CellArray();
	int num_rets = cells->Size();

	res = fwrite(&num_rets, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	for (int j = 0; j < cells->Size(); j++)
	{
		Currency cur = (*cells)(j);
		WriteCurrency(file, cur);
	}
}

hwMatrix* ReadMatrix(FILE* file)
{
	size_t res;

	int num_dimensions;
	std::vector<int> dimensions;

	res = fread(&num_dimensions, sizeof(int), 1, file);

	if (res == 0) throw OMLBridgeError();

	int data_size = 1;

	for (int j=0; j<num_dimensions; j++)
	{
		int dim;
		res = fread(&dim, sizeof(int), 1, file);
		if (res == 0) throw OMLBridgeError();

		dimensions.push_back(dim);
		data_size *= dim;
	}

	if (num_dimensions <= 2)
	{
		int dim_1 = dimensions[0];
		int dim_2 = 1;
		
		if (num_dimensions == 2)
			dim_2 = dimensions[1];

		double* val = new double[data_size];
		res = fread(val, sizeof(double), data_size, file);	
		if (res == 0) throw OMLBridgeError();

		hwMatrix* mtx = new hwMatrix(dim_1, dim_2, val, hwMatrix::REAL);
		mtx->OwnData(true);

		return mtx;
	}
	else
	{
		// unsupported for now
	}

	return NULL;
}

std::string ReadString(FILE* file)
{
	int num_chars = 0;
	size_t res = fread(&num_chars, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	if (num_chars == 0)
		return "";

	char* my_string = new char[num_chars+1];
	res = fread(my_string, sizeof(char), num_chars, file);
	if (res == 0)
	{
		delete[] my_string;
		my_string = nullptr;
		throw OMLBridgeError();
	}

	my_string[num_chars] = '\0';
	std::string ret_str = my_string;

	delete [] my_string;
	my_string = nullptr;

	return ret_str;
}

HML_CELLARRAY* ReadStringList(FILE* file)
{
	size_t res;

	int num_strings;
	res = fread(&num_strings, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();
	
	HML_CELLARRAY* cells = new HML_CELLARRAY(num_strings, 1, HML_CELLARRAY::REAL);
					
	std::string my_string;
					
	for (int j=0; j<num_strings; j++)
	{
		int data_type = ReadInt(file); // check to make sure it's a string?
		my_string = ReadString(file);
		(*cells)(j) = my_string;
	}

	return cells;
}

int ReadInt(FILE* file)
{
	int result = 0;

	size_t res = fread(&result, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	return result;
}

double ReadDouble(FILE* file)
{
	double my_dbl;
	size_t res = fread(&my_dbl, sizeof(double), 1, file);
	if (res == 0) throw OMLBridgeError();

	return my_dbl;
}

Currency EvaluateString(Interpreter& interp, FILE* file)
{
	int data_type = ReadInt(file); // check to make sure it's a string?
	std::string string_to_evaluate = ReadString(file);
	
	Currency result = interp.DoString(string_to_evaluate);

	return result;
}

Currency SetValue(Interpreter& interp, FILE* file)
{
	int data_type = ReadInt(file); // check to make sure it's a string?
	std::string varname = ReadString(file);

	data_type = ReadInt(file);

	if (data_type == TYPE_MATRIX)
	{
		hwMatrix* mtx = ReadMatrix(file);

		if (mtx)
			interp.SetValue(varname, mtx);
	}
	else if (data_type == TYPE_SCALAR)
	{
		double my_dbl = ReadDouble(file);

		interp.SetValue(varname, my_dbl);
	}
	else if (data_type == TYPE_STRING) 
	{
		std::string str = ReadString(file);

		interp.SetValue(varname, str);
	}
	else if (data_type == TYPE_STRING_LIST) 
	{
		HML_CELLARRAY* cells = ReadStringList(file);

		interp.SetValue(varname, cells);
	}

	return 0.0;
}

Currency GetValue(Interpreter& interp, FILE* file)
{
	int data_type = ReadInt(file); // check to make sure it's a string?
	std::string varname = ReadString(file);
	return interp.GetValue(varname);
}

Currency CallFunction(Interpreter& interp, FILE* file)
{
	int data_type = ReadInt(file); // check to make sure it's a string?
	std::string func_name  = ReadString(file);
	int         num_inputs = ReadInt(file);

	std::vector<Currency> inputs;

	for (int j=0; j<num_inputs; j++)
	{
		int data_type = ReadInt(file);

		if (data_type == TYPE_MATRIX)
		{
			hwMatrix* mtx = ReadMatrix(file);
			inputs.push_back(mtx);
		}
		else if (data_type == TYPE_SCALAR)
		{
			double dbl = ReadDouble(file);
			inputs.push_back(dbl);
		}
		else if (data_type == TYPE_STRING)
		{
			std::string str = ReadString(file);
			inputs.push_back(str);
		}
	}

	Currency result = interp.CallFunction(func_name, inputs);

	return result;
}

Currency CallMRFunction(Interpreter& interp, FILE* file)
{
	int data_type = ReadInt(file); // check to make sure it's a string?
	std::string func_name = ReadString(file);
	int         num_inputs = ReadInt(file);
	int         num_outputs = ReadInt(file);

	std::vector<Currency> inputs;

	for (int j = 0; j < num_inputs; j++)
	{
		int data_type = ReadInt(file);

		if (data_type == TYPE_MATRIX)
		{
			hwMatrix* mtx = ReadMatrix(file);
			inputs.push_back(mtx);
		}
		else if (data_type == TYPE_SCALAR)
		{
			double dbl = ReadDouble(file);
			inputs.push_back(dbl);
		}
		else if (data_type == TYPE_STRING)
		{
			std::string str = ReadString(file);
			inputs.push_back(str);
		}
	}

	std::vector<Currency> outputs;

	interp.CallFunction(func_name, inputs, outputs, num_outputs);

	HML_CELLARRAY* cells = new HML_CELLARRAY(num_outputs, 1, HML_CELLARRAY::REAL);

	for (int j = 0; j < num_outputs; ++j)
		(*cells)(j) = outputs[j];

	Currency result(cells);
	result.SetMask(Currency::MASK_CELL_LIST);

	return result;
}

void WriteInt(FILE* file, int val)
{
	size_t res = fwrite(&val, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();
}

void WriteDouble(FILE* file, double val)
{
	size_t res;

	int type = TYPE_SCALAR;
	res = fwrite(&type, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	res = fwrite(&val, sizeof(double), 1, file);
	if (res == 0) throw OMLBridgeError();
}

void WriteString(FILE* file, const char* string)
{
	size_t res;
	int type      = TYPE_STRING;
	size_t num_chars = strlen(string);
	res = fwrite(&type, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	res = fwrite(&num_chars, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	res = fwrite(string, sizeof(char), num_chars, file);
	if (res == 0) throw OMLBridgeError();
}

void WriteError(FILE* file, const char* string)
{
	size_t res;
	int type      = TYPE_ERROR;
	size_t num_chars = strlen(string);
	res = fwrite(&type, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	res = fwrite(&num_chars, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	res = fwrite(string, sizeof(char), num_chars, file);
	if (res == 0) throw OMLBridgeError();
}

void WriteMatrix(FILE* file, const double* data, int nrows, int ncols)
{
	size_t res;
	int type      = TYPE_MATRIX;
	int num_dims = 2; // number of dimensions is always 2

	res = fwrite(&type, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	res = fwrite(&num_dims, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	res = fwrite(&nrows, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	res = fwrite(&ncols, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	res = fwrite(data, sizeof(double), nrows*ncols, file);
	if (res == 0) throw OMLBridgeError();
}

void WriteStringList(FILE* file, char** data, int nstrings)
{
	size_t res;
	int type      = TYPE_STRING_LIST;
	res = fwrite(&type, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	res = fwrite(&nstrings, sizeof(int), 1, file);
	if (res == 0) throw OMLBridgeError();

	for (int j=0; j<nstrings; j++)
		WriteString(file, data[j]);
}
//------------------------------------------------------------------------------
// Returns true if successful in getting the version string [version]
//------------------------------------------------------------------------------
bool OmlVersion(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs)
{
	std::string version(OML_PRODUCT);
	std::string edition(g_edition);
	if (g_edition.empty() && g_licHandle)
	{
		void* symbol = BuiltInFuncsCore::DyGetFunction(g_licHandle, "EditionString");
		if (symbol)
		{
			lic_edition_fptr fptr = (lic_edition_fptr)(symbol);
			if (fptr)
			{
				fptr(g_edition, true);
				edition = g_edition;
			}
		}
	}
	else if (!g_licHandle)
	{
		// CallFunction where authorization is called has not yet happened.
		// So assume business edition
		edition = "Business Edition";
	}
	version += ' ' + edition;
	version += std::string(" ") + OML_VERSION;

	std::string vfile = eval.GetApplicationDir();
	if (!vfile.empty())
		vfile += "/";
	vfile += "config/compose/version.xml";
	version += BuiltInFuncsCore::GetBuildNumber(vfile);

	Currency out(version);
	out.DispOutput();

	outputs.push_back(out);
	return true;
}
//------------------------------------------------------------------------------
// Load the license library and attempt to check out a Compose license.
//------------------------------------------------------------------------------
void Initialize()
{
	// Redirect stderr to capture license errors
    std::string os_dir;
	std::string sep;
#ifdef OS_WIN
	TCHAR tmpbuf[MAX_PATH];
	DWORD dwret = GetTempPathA(MAX_PATH, tmpbuf);
	if (dwret < MAX_PATH && dwret != 0)
	{
		g_stderrfile = tmpbuf;
	}
#ifdef _DEBUG
    os_dir = "win64d";
#else
    os_dir = "win64";
#endif
	sep = ";";
#else
	char const* tmpdir = getenv("TMPDIR");
	if (tmpdir)
	{
		g_stderrfile = std::string(tmpdir) + '/';
	}
	else
	{
		g_stderrfile = "/tmp/";
	}
    os_dir = "linux64";
	sep = ":";
#endif
	g_stderrfile += "omlbridgeserver_err.txt";
	remove(g_stderrfile.c_str());                // Remove previous file
	g_errfile.open(g_stderrfile.c_str(), std::ios::out);
	std::cerr.rdbuf(g_errfile.rdbuf());          // Redirect cerr to file

	const char* unity_root = getenv("HW_UNITY_ROOTDIR");
	if (!unity_root)
	{
		std::cerr << "HW_UNITY_ROOTDIR must be set" << std::endl;
		exit(1);
	}
	g_unityroot = unity_root;

	// Read the commands and perform the operations
	g_interp.SetApplicationDir(g_unityroot);
	// Set framework binaries root directory and add framework bin directory to environment variable PATH
    std::string framework_env("HW_FRAMEWORK");
	std::string path_env("PATH");
    std::string framework_root(g_unityroot + "/../../common/framework/" + os_dir + "/hwx");		
	std::string new_path = framework_root + "/bin/" + os_dir;
	const char* path = getenv(path_env.c_str());

	if (path)
	{
		new_path += sep;
		new_path += path;
	}

#ifdef OS_WIN
    _putenv_s(framework_env.c_str(), framework_root.c_str());
	_putenv_s(path_env.c_str(), new_path.c_str());
#else
    setenv(framework_env.c_str(), framework_root.c_str(), 1);
	setenv(path_env.c_str(), new_path.c_str(), 1);
	//Adding framework binaries to LD_LIBRARY_PATH
	std::string ld_path_env("LD_LIBRARY_PATH");
	std::string new_ld_path = framework_root + "/bin/" + os_dir;
	const char* ld_path = getenv(ld_path_env.c_str());
	if (ld_path)
	{
		new_ld_path += sep;
		new_ld_path += ld_path;
	}
	setenv(ld_path_env.c_str(), new_ld_path.c_str(), 1);
#endif
	// Built in functions on the client side
	g_interp.RegisterBuiltInFunction("version", OmlVersion,
		FunctionMetaData(0, 1, "CoreMinimalInterpreter"));

	// Reverting to 2021.2 behavior where libraries will be loaded always
	// with _sys_bridge_init_variable. For 2022, licensing will be moved to
	// the professional libraries.
	std::string init_script(g_unityroot + "/plugins/compose/mathtoolbox/init_console.oml");
	
	g_interp.SetGlobalValue("_sys_bridge_init_", 1.0);
	g_interp.DoFile(init_script);
	g_interp.Clear("_sys_bridge_init_");

	std::cerr << "BridgeServer initialized, libraries loaded, waiting for authorization..." << std::endl;
}
//------------------------------------------------------------------------------
// Utility for license checkin and clean before exit
//------------------------------------------------------------------------------
void Cleanup()
{
	if (g_licHandle)
	{
		void* symbol = BuiltInFuncsCore::DyGetFunction(g_licHandle, "CheckInLic");
		if (symbol)
		{
			lic_checkin_fptr fptr = (lic_checkin_fptr)(symbol);
			fptr();
			std::cerr << "BridgeServer checked in" << std::endl;
		}
		BuiltInFuncsCore::DyFreeLibrary(g_licHandle);
		g_licHandle = nullptr;
	}

	// Redirect cout back to screen
	std::cerr.rdbuf(nullptr);
	g_errfile.close();
}
//------------------------------------------------------------------------------
// Authorizes bridge server
//------------------------------------------------------------------------------
void Authorize()
{
	if (g_licHandle)
	{
		return;
	}

	std::string hwEditionEnvName("HW_EDITION");
	std::string hwEditionEnvVal("Business");

	bool hwEditionExists = true;
	if (!getenv(hwEditionEnvName.c_str()))
	{
		hwEditionExists = false;
		// HW_EDITION is not set, using business edition by default.
#ifdef OS_WIN
		_putenv_s(hwEditionEnvName.c_str(), hwEditionEnvVal.c_str());
#else
		setenv(hwEditionEnvName.c_str(), hwEditionEnvVal.c_str(), 1);
#endif
	}

	// Attempt to check out a Compose license without the user credentials
	std::string importDll("hwcomposelic");

#ifndef OS_WIN
	importDll = "lib" + importDll + ".so";
#endif

	g_licHandle = BuiltInFuncsCore::DyLoadLibrary(importDll);
	if (!g_licHandle)
	{
		std::cerr << "Error: BridgeServer cannot load [" << importDll << "]." << std::endl;
		std::cerr << "Unity root: [" << g_unityroot << "]" << std::endl;
#ifdef OS_WIN
		DWORD error = ::GetLastError();
		std::cerr << "[" << error << "]: " << std::system_category().message(error) << std::endl;
#endif
		exit(1);
	}
	void* symbol = BuiltInFuncsCore::DyGetFunction(g_licHandle, "Authorize");
	if (!symbol)
	{
		std::cerr << "Error: BridgeServer cannot load authorization function." << std::endl;
		exit(1);
	}

	lic_auth_fptr licfptr = (lic_auth_fptr)(symbol);
	int result = licfptr("", "", 1);
	if (result != 0)
	{
		// Try and see if there can be a checkout with the personal edition
		if (!hwEditionExists)
		{
			void* funcsymbol = BuiltInFuncsCore::DyGetFunction(
				g_licHandle, "ResetEditionEnvVariable");
			if (funcsymbol)
			{
				lic_resetedition_fptr envptr = (lic_resetedition_fptr)(funcsymbol);
				envptr("Personal");

				result = licfptr("", "", 1);
			}
		}
	}
	if (result != 0)
	{
		std::cerr << "Error: BridgeServer authorization failure." << std::endl;
		exit(1);
	}
	std::cerr << "BridgeServer authorized" << std::endl;
	symbol = BuiltInFuncsCore::DyGetFunction(g_licHandle, "IsProEdition");
	if (symbol)
	{
		lic_ispro_fptr fptr = (lic_ispro_fptr)(symbol);
		g_isProfessional = fptr();
	}
}