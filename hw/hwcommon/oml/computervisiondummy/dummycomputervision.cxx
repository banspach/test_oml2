/**
* @file dummycomputervision.cxx
* @date May 2021
* Copyright (C) 2021 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#include "dummycomputervision.h"

#include "Currency.h"

#define TBOXVERSION 2021.2

//------------------------------------------------------------------------------
// Returns toolbox version
//------------------------------------------------------------------------------
double GetToolboxVersion(EvaluatorInterface eval)
{
    return TBOXVERSION;
}
//------------------------------------------------------------------------------
// Dummy method, returning 1 if needed - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyIntFunc(EvaluatorInterface           eval,
                  const std::vector<Currency>& inputs,
                  std::vector<Currency>&       outputs)
{
    if (eval.GetNargoutValue() > 0)
    {
        outputs.push_back(0);
    }
    return true;
}
//------------------------------------------------------------------------------
// Entry point which registers toolbox with oml
//------------------------------------------------------------------------------
int InitDll(EvaluatorInterface eval)
{
    // Returns dummy values for functions that cannot be run in console/batch
    eval.RegisterBuiltInFunction("imshowcv", &DummyIntFunc, FunctionMetaData(-3, 1, "ComputerVision"));

    return 1;
}

