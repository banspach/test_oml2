/**
* @file omluidesignerdummydefs.h
* @date September 2021
* Copyright (C) 2021 Altair Engineering, Inc. All rights reserved.
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret
* Information. Not for use or disclosure outside of Licensee's organization.
* The software and information contained herein may only be used internally and
* is provided on a non-exclusive, non-transferable basis.  License may not
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly
* perform the software or other information provided herein, nor is Licensee
* permitted to decompile, reverse engineer, or disassemble the software. Usage
* of the software and other information provided by Altair (or its resellers) is
* only as explicitly stated in the applicable end user license agreement between
* Altair and Licensee. In the absence of such agreement, the Altair standard end
* user license agreement terms shall govern.
*/

#ifndef __OMLUIDESIGNERDUMMYDEFS_H__
#define __OMLUIDESIGNERDUMMYDEFS_H__

//------------------------------------------------------------------------------
//!
//! \brief Contains macro definitions for exporting functions in the library
//!
//------------------------------------------------------------------------------
// Windows export macro
#ifdef OS_WIN
#ifdef OMLUIDESIGNERDUMMY_EXPORT
#undef  OMLUIDESIGNERDUMMY_DECLS
#define OMLUIDESIGNERDUMMY_DECLS __declspec(dllexport)
#else
#undef  OMLUIDESIGNERDUMMY_DECLS
#define OMLUIDESIGNERDUMMY_DECLS __declspec(dllimport)
#endif  // OMLUIDESIGNERDUMMY_EXPORT
#else
#undef  OMLUIDESIGNERDUMMY_DECLS
#define OMLUIDESIGNERDUMMY_DECLS
#endif // OS_WIN

#endif // __OMLUIDESIGNERDUMMYDEFS_H__

