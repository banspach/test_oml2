/**
* @file BetterCalcFuncs.cpp
* @date March 2015
* Copyright (C) 2015-2020 Altair Engineering, Inc. All rights reserved. 
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret 
* Information. Not for use or disclosure outside of Licensee's organization. 
* The software and information contained herein may only be used internally and 
* is provided on a non-exclusive, non-transferable basis.  License may not 
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly 
* perform the software or other information provided herein, nor is Licensee 
* permitted to decompile, reverse engineer, or disassemble the software. Usage 
* of the software and other information provided by Altair (or its resellers) is 
* only as explicitly stated in the applicable end user license agreement between 
* Altair and Licensee. In the absence of such agreement, the Altair standard end 
* user license agreement terms shall govern.
*/

// Begin defines/includes

#include "../Runtime/BuiltInFuncsCore.h"
#include "../Runtime/BuiltInFuncsUtils.h"
#include "../Runtime/CurrencyDisplay.h"
#include "../Runtime/Interpreter.h"
#include "../Runtime/StructData.h"
#include "../Runtime/ErrorInfo.h"
#include "../Runtime/OML_Error.h"

#include "BetterCalc.h"
#include "ConsoleInterpreterWrapper.h"

#include <cassert>

#include "BuiltInFuncsSystem.h"

extern Interpreter*               interp;
extern ConsoleInterpreterWrapper* interpWrapper;

#define OML_PRODUCT "Altair Compose"
// End defines/includes

//------------------------------------------------------------------------------
// Prints help
//------------------------------------------------------------------------------
void OML_help()
{
	// This could be added as a OML function if needed. I did not implement as such because:
	// The requested argument to invoke this help list was -help, not a valid function name.
	// A help function already exists in OML.DLL in the BuildInFuncs.cpp module.
	std::cout << OML_PRODUCT << " Console Help:" << std::endl;
	std::cout << "-e \"any valid OML command\" --> execute the OML command in " << OML_PRODUCT << " and then quit." << std::endl;
	std::cout << "-f foo.oml --> load and execute the OML script called foo.oml in " << OML_PRODUCT << " and then quit" << std::endl;
	std::cout << "-help      --> give the list of supported optional arguments" << std::endl;
	std::cout << "-version   --> give the version of " << OML_PRODUCT << std::endl;
}
//------------------------------------------------------------------------------
//! Gets the version string of the application
//------------------------------------------------------------------------------
std::string GetVersion(const std::string& appdir)
{
    std::string version (OML_PRODUCT);

    version += std::string(" ") + interpWrapper->EditionString(true);
    version += std::string(" ") + ConsoleInterpreterWrapper::OmlVersion;

    std::string vfile = appdir;
    if (!vfile.empty())
        vfile += "/";
    vfile += "config/compose/version.xml";

    version += BuiltInFuncsCore::GetBuildNumber(vfile);
    return version;
}
//------------------------------------------------------------------------------
//! Returns true if successful in getting the version string
//! \param[in]  eval    Evaluator interface
//! \param[in]  inputs  Vector of inputs
//! \param[out] outputs Vector of outputs
//------------------------------------------------------------------------------
bool OmlVersion(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs)
{
    std::string version = GetVersion(eval.GetApplicationDir());
    Currency out (version);
    out.DispOutput();

    outputs.push_back(out);
    return true;
}
//------------------------------------------------------------------------------
// Gets number of command input arguments
//------------------------------------------------------------------------------
bool OmlCommandInputsNumber(EvaluatorInterface           eval,
                            const std::vector<Currency>& inputs,
                            std::vector<Currency>&       outputs)
{
    assert(interpWrapper);

    outputs.push_back(interpWrapper->GetNumberOfInputArgs());
    return true;
}
//------------------------------------------------------------------------------
// Gets command input at the given index
//------------------------------------------------------------------------------
bool OmlGetCommandInput(EvaluatorInterface           eval,
                        const std::vector<Currency>& inputs,
                        std::vector<Currency>&       outputs)
{
    assert(interpWrapper);

    if (inputs.empty())
         throw OML_Error(OML_ERR_NUMARGIN);

    Currency cur = inputs[0];
    if (!cur.IsPositiveInteger())
        throw OML_Error(OML_ERR_INVALID_INDEX, 1, OML_VAR_TYPE);
        
    int idx   = static_cast<int>(cur.Scalar());
    int total = interpWrapper->GetNumberOfInputArgs();
    if (idx > total)
    {
        std::string msg ("Error: invalid index; ");
        if (total > 0)
        {
            msg += "must be between 1 and ";
            msg += std::to_string(static_cast<long long>(total));
        }
        else
        {
            msg += "no input arguments are specified";
        }
        throw OML_Error(msg);
    }

    std::string arg = interpWrapper->GetInputArg(idx - 1);
    outputs.push_back(arg);
    return true;
}
//------------------------------------------------------------------------------
// Returns the name of the startup script file
//------------------------------------------------------------------------------
std::string GetStartupScriptPath()
{
    std::string envVar;
#ifdef OS_WIN
    envVar = "USERPROFILE";
#else
    envVar = "HOME";
#endif

    char* env = getenv(envVar.c_str());
    if (!env)
    {
        std::cout << "Warning: Environment variable [" <<  envVar 
                  << "] is not set" << std::endl;
        return "";
    }

    std::string startfile (env);
    startfile += "/.altair/Compose";
    startfile += ConsoleInterpreterWrapper::OmlVersion;
    startfile += "/hwx/Compose_startup.oml";

    startfile = BuiltInFuncsUtils::Normpath(startfile);
    return startfile;
}
//------------------------------------------------------------------------------
// Returns the path of the startup script file
//------------------------------------------------------------------------------
bool OmlStartupscriptPath(EvaluatorInterface           eval,
                          const std::vector<Currency>& inputs,
                          std::vector<Currency>&       outputs)
{
    outputs.push_back(GetStartupScriptPath());
    return true;
}
//------------------------------------------------------------------------------
// Gets argc (getargc command)
//------------------------------------------------------------------------------
bool OmlGetArgC(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs)
{
    assert(interpWrapper);

    outputs.push_back(interpWrapper->GetArgc());
    return true;
}
//------------------------------------------------------------------------------
// Gets argv at the given index (getargv command)
//------------------------------------------------------------------------------
bool OmlGetArgV(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs)
{
    assert(interpWrapper);

    if (inputs.size() != 1)
    {
        throw OML_Error(OML_ERR_NUMARGIN);
    }
    if (!inputs[0].IsPositiveInteger())
    {
        throw OML_Error(OML_ERR_POSINTEGER, 1, OML_VAR_VALUE);
    }
    int idx  = static_cast<int>(inputs[0].Scalar()) - 1;
    int argc = interpWrapper->GetArgc();
    if (idx > argc)
    {
        std::string msg ("Error: invalid input in argument 1; value must be ");
        if (argc == 1)
        {
            msg += "1";
        }
        else
        {
            msg += "in the range of 1 and " + std::to_string(static_cast<long long>(argc));
        }
        throw OML_Error(msg);
    }
    outputs.push_back(interpWrapper->GetArgv(idx));
    return true;
}
//------------------------------------------------------------------------------
// Dummy method returning empty string if needed - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyStringFunc(EvaluatorInterface           eval,
                     const std::vector<Currency>& inputs,
                     std::vector<Currency>&       outputs)
{ 
    int numoutputs = eval.GetNargoutValue();
    if (numoutputs > 0)
    {
        outputs.push_back("");
    }
    if (numoutputs > 1)
    {
        outputs.push_back("");
    }
    return true; 
}
//------------------------------------------------------------------------------
// Dummy method, returning 1 if needed - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyIntFunc(EvaluatorInterface           eval,
                  const std::vector<Currency>& inputs,
                  std::vector<Currency>&       outputs)
{ 
    if (eval.GetNargoutValue() > 0)
    {
        outputs.push_back("1");
    }
    return true; 
}
//------------------------------------------------------------------------------
//! Dummy method returning empty cell if needed - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyCellFunc(EvaluatorInterface           eval,
               const std::vector<Currency>& inputs,
               std::vector<Currency>&       outputs)
{ 
    if (eval.GetNargoutValue())
    {
        outputs.push_back(EvaluatorInterface::allocateCellArray());
    }
    return true; 
}
//------------------------------------------------------------------------------
// Dummy method returning true - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyVoidFunc(EvaluatorInterface           eval,
                   const std::vector<Currency>& inputs, 
                   std::vector<Currency>&       outputs)
{
    return true;
}
//------------------------------------------------------------------------------
// Dummy method returning true - implemented in GUI only
//------------------------------------------------------------------------------
bool DummyMatrixFunc(EvaluatorInterface           eval,
                     const std::vector<Currency>& inputs,
                     std::vector<Currency>&       outputs)
{
    int nargout = eval.GetNargoutValue();
    if (nargout > 0)
    {
        outputs.push_back(EvaluatorInterface::allocateMatrix());
    }
    if (nargout > 1)
    {
        outputs.push_back(false);
    }
    return true;
}
//------------------------------------------------------------------------------
// Initializes environment variables
//------------------------------------------------------------------------------
void InitEnvVariables()
{
    BuiltInFuncsUtils utils;

    std::string unitydir;
    if (!getenv("HW_UNITY_ROOTDIR"))
    {
        if (getenv("ALTAIR_HOME"))
        {
            unitydir = getenv("ALTAIR_HOME");
            unitydir += "/unity";
            unitydir = utils.StripMultipleSlashesAndNormalize(unitydir);
            utils.SetEnvVariable("HW_UNITY_ROOTDIR", unitydir);
        }
    }
    else
    {
        unitydir = getenv("HW_UNITY_ROOTDIR");
    }

    std::string envvar("$HW_UNITY_ROOTDIR");

    if (getenv("OML_APPDIR"))
    {
        std::string path(getenv("OML_APPDIR"));
        size_t pos = path.find(envvar);

        if (pos != std::string::npos)
        {
            path.replace(pos, envvar.length(), unitydir);
            path = utils.StripMultipleSlashesAndNormalize(path);
            utils.SetEnvVariable("OML_APPDIR", path);
        }
        interp->SetApplicationDir(path);
    }

    if (getenv("OML_HELP"))
    {
        std::string path(getenv("OML_HELP"));
        size_t pos = path.find(envvar);

        if (pos != std::string::npos)
        {
            path.replace(pos, envvar.length(), unitydir);
            path = utils.StripMultipleSlashesAndNormalize(path);
            utils.SetEnvVariable("OML_HELP", path);
        }
    }
}
//------------------------------------------------------------------------------
// Gets library authorization [getlibraryauthorization]
//------------------------------------------------------------------------------
bool OmlCheckLicense(EvaluatorInterface           eval,
                     const std::vector<Currency>& inputs,
                     std::vector<Currency>&       outputs)
{
#ifdef _DEBUG
    outputs.push_back("");
    return true;
#endif

    try
    {
        if (inputs.empty())
        {
            throw OML_Error(OML_ERR_NUMARGIN);
        }

        if (!inputs[0].IsString())
        {
            throw OML_Error(OML_ERR_STRING, 1);
        }

        std::string libinfo(inputs[0].StringVal());
        if (libinfo.empty())
        {
            throw OML_Error(OML_ERR_NONEMPTY_STR, 1);
        }

        std::string out;
        assert(interpWrapper);
        if (!interpWrapper)
        {
            throw OML_Error(OML_ERR_INTERNAL);
        }

        std::string err;
        if (!interpWrapper->CheckoutLibrary(libinfo, out, err))
        {
            if (!err.empty() && eval.GetVerbose() > 0)
            {
                BuiltInFuncsUtils::SetWarning(eval, err);
            }
            throw OML_Error(OML_ERR_AUTHENTICATE, 1);
        }

        outputs.push_back(out);
    }
    catch (const OML_Error & e)
    {
        throw OML_Error(e.GetErrorMessage(), false);
    }

    return true;
}
//------------------------------------------------------------------------------
// Returns libary authorization [returnlibraryauthorization]
//------------------------------------------------------------------------------
bool OmlCheckinLicense(EvaluatorInterface           eval,
                       const std::vector<Currency>& inputs,
                       std::vector<Currency>&       outputs)
{
#ifdef _DEBUG
    return true;
#endif

    try
    {
        if (inputs.empty())
        {
            throw OML_Error(OML_ERR_NUMARGIN);
        }

        if (!inputs[0].IsString())
        {
            throw OML_Error(OML_ERR_STRING, 1);
        }

        std::string id(inputs[0].StringVal());
        if (id.empty())
        {
            throw OML_Error(OML_ERR_NONEMPTY_STR, 1);
        }

        assert(interpWrapper);
        if (!interpWrapper)
        {
            throw OML_Error(OML_ERR_INTERNAL);
        }

        interpWrapper->CheckinLibrary(id);
    }
    catch (const OML_Error & e)
    {
        throw OML_Error(e.GetErrorMessage(), false);
    }

    return true;
}
//------------------------------------------------------------------------------
// Tests library authorization [testlibraryauthorization]
//------------------------------------------------------------------------------
bool OmlTestLicense(EvaluatorInterface           eval,
                    const std::vector<Currency>& inputs,
                    std::vector<Currency>&       outputs)
{
#ifdef _DEBUG
    return true;
#endif

    try
    {
        if (inputs.empty())
        {
            throw OML_Error(OML_ERR_NUMARGIN);
        }

        if (!inputs[0].IsString())
        {
            throw OML_Error(OML_ERR_STRING, 1);
        }
        std::string id(inputs[0].StringVal());
        if (id.empty())
        {
            throw OML_Error(OML_ERR_NONEMPTY_STR, 1);
        }

        assert(interpWrapper);
        std::string info(interpWrapper->TestLibrary(id));
        if (info.empty())
        {
            throw OML_Error(OML_ERR_AUTHENTICATE, 1);
        }

        if (eval.GetVerbose() > 0)
        {
            eval.PrintResult("Testing library [" + info + "]");
        }
    }
    catch (const OML_Error & e)
    {
        throw OML_Error(e.GetErrorMessage(), false);
    }

    return true;
}
