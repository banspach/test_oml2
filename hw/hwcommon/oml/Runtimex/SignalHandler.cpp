/**
* @file SignalHandler.cpp
* @date June 2016
* Copyright (C) 2016-2021 Altair Engineering, Inc. All rights reserved. 
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret 
* Information. Not for use or disclosure outside of Licensee's organization. 
* The software and information contained herein may only be used internally and 
* is provided on a non-exclusive, non-transferable basis.  License may not 
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly 
* perform the software or other information provided herein, nor is Licensee 
* permitted to decompile, reverse engineer, or disassemble the software. Usage 
* of the software and other information provided by Altair (or its resellers) is 
* only as explicitly stated in the applicable end user license agreement between 
* Altair and Licensee. In the absence of such agreement, the Altair standard end 
* user license agreement terms shall govern.
*/

// Begin defines/includes
#include "SignalHandler.h"

#include <cassert>
#include <fstream>

#include "ConsoleInterpreterWrapper.h"
#include "CurrencyDisplay.h"

// End defines/includes
//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
SignalHandler::~SignalHandler()
{
    if (_src)
    {
        _consolewrapper->SetChildSignalHandler(nullptr);
    }
}
//------------------------------------------------------------------------------
// Copy constructor
//------------------------------------------------------------------------------
SignalHandler::SignalHandler(const SignalHandler* src) 
    : _src (src)
    , _consolewrapper (NULL)
    , _batchmode(true)
{
    // Set the console wrapper for handling actions from oml core in client
    if (_src)
    {
        _batchmode = _src->_batchmode;
        _consolewrapper = _src->_consolewrapper;
    }
}
//------------------------------------------------------------------------------
// Clone signal handler
//------------------------------------------------------------------------------
SignalHandlerBase* SignalHandler::CreateClone()
{
    return (new SignalHandler(this));
}
//------------------------------------------------------------------------------
// Emits signal to clear results
//------------------------------------------------------------------------------
void SignalHandler::OnClearResultsHandler()
{
    // Clear results only once, in the signal handler which is not cloned
    if (!_src && _consolewrapper)  
        _consolewrapper->HandleOnClearResults();
}
//------------------------------------------------------------------------------
// Prints prompt for save on exit
//------------------------------------------------------------------------------
void SignalHandler::OnSaveOnExitHandler(int returnCode)
{
    if (_consolewrapper)  
        _consolewrapper->HandleOnSaveOnExit(returnCode);
}
//------------------------------------------------------------------------------
// Start pause
//------------------------------------------------------------------------------
void SignalHandler::OnPauseStartHandler(const std::string& msg, bool wait)
{
    if (_consolewrapper)  
        _consolewrapper->HandleOnPauseStart(msg, wait);
}
//------------------------------------------------------------------------------
// Get user input
//------------------------------------------------------------------------------
void SignalHandler::OnUserInputHandler(const std::string& prompt,
                                       const std::string& type,
                                       std::string&       input)
{
    if (_consolewrapper)  
        _consolewrapper->HandleOnGetUserInput(prompt, type, input);
}
//------------------------------------------------------------------------------
// Print result
//------------------------------------------------------------------------------
void SignalHandler::OnPrintResultHandler(const Currency& cur)
{
    if (!_src)  // This is the main interpreter
    {
        assert(_consolewrapper);
        _consolewrapper->HandleOnPrintResult(cur);
    }
}
//------------------------------------------------------------------------------
// Add nested display
//------------------------------------------------------------------------------
void SignalHandler::OnAddDisplayHandler(CurrencyDisplay* display)
{
     if (!_src && _consolewrapper)  
        _consolewrapper->HandleOnAddDisplay(display);
}
// End of file:
