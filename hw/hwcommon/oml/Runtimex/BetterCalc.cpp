﻿/**
* @file BetterCalc.cpp
* @date November 2014
* Copyright (C) 2014-2022 Altair Engineering, Inc. All rights reserved. 
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret 
* Information. Not for use or disclosure outside of Licensee's organization. 
* The software and information contained herein may only be used internally and 
* is provided on a non-exclusive, non-transferable basis.  Licensee may not 
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly 
* perform the software or other information provided herein, nor is Licensee 
* permitted to decompile, reverse engineer, or disassemble the software. Usage 
* of the software and other information provided by Altair (or its resellers) is 
* only as explicitly stated in the applicable end user license agreement between 
* Altair and Licensee. In the absence of such agreement, the Altair standard end 
* user license agreement terms shall govern.
*/
#include "../Runtime/OMLDll.h"
#include "../Runtime/Currency.h"
#include "../Runtime/CurrencyDisplay.h"
#include "../Runtime/Interpreter.h"
#include "../Runtime/EvaluatorDebug.h"
#include "../Runtime/BuiltInFuncsUtils.h"
#include "../Runtime/StructData.h"	

#include "BuiltInFuncsSystem.h"

#include <cassert>
#include <clocale>
#include <iostream>
#include <fstream>

#include "BetterCalc.h"
#include "ConsoleInterpreterWrapper.h"
#include "SignalHandler.h"

// Shows new prompt on console
// \param Interpreter wrapper
void CallNewConsolePrompting(ConsoleInterpreterWrapper*);
// Runs input file(s)
// \param String containing input files
void RunInputFiles(const std::string&);

// global variables
Interpreter*               interp        = nullptr;
ConsoleInterpreterWrapper* interpWrapper = nullptr;
std::string                dummyFilename;
bool                       ClearCommandString = false;

// User interrupt related variables, decls
static int g_numUserInterrupts = 0;  //! Number of control-C interrrupts pressed
#ifdef OS_WIN
#   include <Windows.h>
// Called when control key is down
// \param controlType Control type
BOOL OnControlKeyDown(DWORD controlType);
#else
#   include <signal.h>
#   include <time.h>
#   include <stdio.h>
#   include <unistd.h>
// Called when control key is down
// \param controlType Control type
void OnControlKeyDown(int controlType);
#endif

//------------------------------------------------------------------------------
// Display license error msg
//------------------------------------------------------------------------------
static void LicenseError(const std::string &msg)
{
    if (!msg.empty())
    {
        std::cout << msg << std::endl;
    }
    else
    {
        std::cout << "ALTAIR LICENSE ERROR! Exiting the application..." << std::endl;
    }
 
#ifdef OS_WIN
    Sleep(3000); // Sleep to give the user a chance to see message
#else
    usleep(3000);
#endif

	delete interpWrapper;
	delete interp;
}

//------------------------------------------------------------------------------
// Entry point for console
//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
#ifdef OS_WIN  // Disables sync with printf - speed improvement
    std::ios_base::sync_with_stdio(false);
#endif

    interp = new Interpreter;

    InitEnvVariables();

    // Wrapper for interpreter signals and methods
    interpWrapper = new ConsoleInterpreterWrapper (interp);

#ifndef _DEBUG
    //read -user and -passwd info from command line if provided
    std::string user;
    std::string pwd;
    for (int ix = 1; ix < argc; ++ix)
	{
		std::string arg (argv[ix]);
        std::string lower_str = arg;
		std::transform(lower_str.begin(), lower_str.end(), lower_str.begin(), ::tolower);
        if(lower_str == "-user" && ((ix+1) < argc))
        {
            user = argv[ix+1];
        }
        else if(lower_str == "-passwd" && ((ix+1) < argc))
        {
            pwd = argv[ix+1];
        }
    }

    std::string errMsg;
    bool success = interpWrapper->Authorize(user, pwd, errMsg);
    if(! success ) 
    {
        LicenseError(errMsg);
        return 1;
    }
#endif

    CurrencyDisplay::PAGINATE paginateVal = CurrencyDisplay::GetPaginate();
    CurrencyDisplay::SetPaginate(CurrencyDisplay::PAGINATE_OFF);  // Disable pagination

    char* clocale = std::setlocale(LC_ALL, "");
    std::setlocale(LC_NUMERIC, "C");

    // Flags for command line arguments
    bool bannerprinted = false;
	bool continueAfterScript = false; // Needed if commands/files are in cmd line
    bool continueRequired    = false; // Needed if commands/files are in cmd line	  
	bool toolbox_load        = false; // True if toolboxes need to be loaded
#if !defined(_DEBUG)
	toolbox_load = true;              // always load the toolboxes in release
#endif 

    // File to execute, if specified
	std::string scriptPath;

    // First pass through arguments.
    std::vector<std::string> inputArgs;
    inputArgs.reserve(argc - 1);
    bool        setInputArgs  = false;
    int         numOpenBraces = 0;
    std::string inpArgWithBraces;

    std::vector<std::string> argsv;
    argsv.reserve(argc);
    assert(argc > 0);
    argsv.push_back(std::string(argv[0]));

    std::vector<std::string> argsToProcess;
    argsToProcess.reserve(argc);

	for (int ix = 1; ix < argc; ++ix)
	{
		std::string arg (argv[ix]);
        argsv.push_back(arg);

        // convert command line argument to lower case for comparison
		std::string lower_str = arg;
		std::transform(lower_str.begin(), lower_str.end(), lower_str.begin(), ::tolower);

#ifndef _DEBUG  // Don't process input args in debug
        if (lower_str == "-input")
        {
            setInputArgs = true;   // Save inputs only between -input and next -<command>
            continue;
        }

        if (setInputArgs)
        {
            if ((!lower_str.empty()        && lower_str[0] == '-')      ||
                  lower_str == "/toolbox"  || lower_str == "/notoolbox" ||
                  lower_str == "/x"        || lower_str == "/continue"  || 
                  lower_str == "/filename"  || 
                  lower_str == "/v")
            {
                setInputArgs = false;
            }
            else if (arg.empty())
            {
                inputArgs.push_back(arg);
                continue;
            }
            else
            {
                size_t len       = arg.size();
                bool   hasbraces = false;
                for (size_t k = 0; k < len; ++k)
                {
                    char ch = arg[k];
                    if (ch == '[' || ch == '{')
                    {
                        numOpenBraces++;
                        hasbraces = true;
                    }
                    else if (ch == ']' || ch == '}')
                    {
                        numOpenBraces--;
                        hasbraces = true;
                    }
                }

                if (hasbraces || numOpenBraces > 0)
                {
                    if (!inpArgWithBraces.empty())
                    {
                        inpArgWithBraces += " ";
                    }
                    inpArgWithBraces += arg;
                }

                if (numOpenBraces == 0 && !inpArgWithBraces.empty())
                {
                    inputArgs.push_back(inpArgWithBraces);
                    inpArgWithBraces = "";
                }
                else if (inpArgWithBraces.empty())
                {
                    numOpenBraces = 0;
                    inputArgs.push_back(arg);
                }
            }
            continue;
        }
#endif
        std::string cmd;
        if (!lower_str.empty() && (lower_str[0] == '-' || lower_str[0] == '/'))
        {
            cmd = (lower_str.size() != 1) ? lower_str.substr(1) : "";
        }
		if (cmd == "toolbox")
		{
			toolbox_load = true;  // always load the toolboxes
		}
		else if (cmd == "notoolbox")
		{
			toolbox_load = false;  // provide capability to not load toolboxes
		}
		else if(cmd == "x")
		{
            interp->SetExperimental(true);
		}
        else if (cmd == "continue")
        {
			continueAfterScript = true;
        }
		else if (cmd == "version")
		{
            std::cout << GetVersion(interp->GetApplicationDir()) << std::endl;
			continueRequired = true;
		}
		else if (cmd == "help")
		{
			OML_help();
		    delete interpWrapper;
		    delete interp;
			return 0;
		}
        else
        {
            argsToProcess.push_back(arg);
        }
	}

    interpWrapper->SetInputArgs(inputArgs);
    interpWrapper->SetArgv(argsv);

    RegisterBuiltInFuncs();

	if (toolbox_load)
	{
        std::string mathtoolbox = interp->GetApplicationDir();
        std::string libraryscript;
        if (!mathtoolbox.empty())
        {
            mathtoolbox += "/";
        }
        libraryscript = mathtoolbox + "/plugins/compose/mathtoolbox/init_console.oml";  
        interp->DoFile(libraryscript);
        interp->SetApplicationDir("");  // VSM-5385 Save current paths for later restore.
        libraryscript = mathtoolbox + "/scripts/oml/librarymanager/librarymanagerlaunch.oml";
        interp->DoFile(libraryscript);

        if (!mathtoolbox.empty())
        {
            std::string dir (mathtoolbox + "/scripts/oml/Table");
            dir = BuiltInFuncsUtils::Normpath(dir);
            std::vector<Currency> in;
            in.emplace_back(dir);
            interp->CallFunction("addpath", in);
        }
	}
    else
    {
        interp->SetApplicationDir("");
    }

    // Run the startup script, if applicable. Turned off in debug
#ifndef _DEBUG
    std::string startfile (GetStartupScriptPath());
    if (BuiltInFuncsUtils::FileExists(startfile))
    {
        interp->DoFile(startfile);
    }
#endif

    // Set the interrupt handler after the toolboxes are loaded as this 
    // interferes with fortran libraries in toolboxes and how they handle
    // control C
    SetUserInterruptHandler();

	bool e_argument_flag = false;  // -e command line argument
	bool f_argument_flag = false;  // -f command line argument
    bool ignoreUser = false;       // -user command line argument
    bool ignorePwd = false;        // -passwd command line argument
	// second argument parse - evaluate all other arguments
    for (std::vector<std::string>::const_iterator itr = argsToProcess.begin();
         itr != argsToProcess.end(); ++itr)
    {
		std::string arg (*itr);
        if (arg.empty())
        {
            continue;
        }

		// convert command line argument to lower case for comparison
		std::string lower_str = arg;
        std::transform(lower_str.begin(), lower_str.end(), lower_str.begin(), ::tolower);

        char ch = arg[0];
        if (ch == '-')
		{
			e_argument_flag = false;
            f_argument_flag = false;  // clear -e and -f 
		}

		if (lower_str == "-e")
		{
			// process arguments following as oml commands until the next argument which begins with a dash '-'
			e_argument_flag = true;
			continue;
		}
		else if(e_argument_flag)
		{
			// process the argument as a oml command
            if (!bannerprinted)
            {
                PrintBanner();
                bannerprinted = true;
            }
			interp->DoString(arg);
			continueRequired = true;
		}
		else if (lower_str == "-f")
		{
			// process arguments following as script files until the next argument which begins with a dash '-'
			f_argument_flag = true;
			continue;
		}
        else if (lower_str == "-v")
        {
            // Added for python, ignore this and the next argument
            itr++;
            if (itr == argsToProcess.end())
            {
                break;
            }
            continue;
        }
		else if(f_argument_flag)
		{
            // process the argument as a oml command
            if (!bannerprinted)
            {
                PrintBanner();
                bannerprinted = true;
            }
            RunInputFiles(arg);
			continueRequired = true;
		}

		else if(lower_str.compare(0,10,"/filename=") == 0)
        {
			dummyFilename = arg.substr(10, arg.length()-10);
        }
        else if (lower_str == "-user")
        {
            ignoreUser = true;
        }
        else if (lower_str == "-passwd")
        {
            ignorePwd = true;
        }
        else if(ignoreUser)
        {
            ignoreUser = false;
        }
        else if(ignorePwd)
        {
            ignorePwd = false;
        }
        else
			scriptPath = arg; 
	}


    if (!scriptPath.empty())
	{
        RunInputFiles(scriptPath);
        continueRequired = true;
	}
	
	if(continueRequired && !continueAfterScript)
	{
		delete interpWrapper;
		delete interp;
		return 0;
	}

    // Start of interactive mode
    CurrencyDisplay::SetPaginate(paginateVal);  // Reset pagination value
    interpWrapper->InitCommandWindowInfo();

    if (!bannerprinted)
    {
        PrintBanner();
    }
    SignalHandler* handler = static_cast<SignalHandler*>(interp->GetSignalHandler());
    if (handler)
    {
        handler->SetConsoleBatchMode(false);
    }
	CallNewConsolePrompting(interpWrapper);

    delete interpWrapper;
	delete interp;

	return 0;
}
//------------------------------------------------------------------------------
// Shows new prompt on console
//------------------------------------------------------------------------------
void CallNewConsolePrompting(ConsoleInterpreterWrapper* interpWrapper)
{
    assert(interp);
    assert(interpWrapper);

	size_t licCheckCounter = 1;
	while (1)
    {
        if (std::cin.fail())
        {
            std::cin.clear();
        }

        bool breakloop = HandleUserInterrupt(interpWrapper);
        if (breakloop)
            break;

        bool isPaginating = interpWrapper->IsPaginating();
        if (isPaginating)
        {
            interpWrapper->Paginate();
            continue;
        }

        if (std::cin.fail())
        {
            std::cin.clear();
        }
        interpWrapper->PrintNewPrompt();   
        
        std::string strCommand = GetInputCommand(interpWrapper);
        if (!CurrencyDisplay::IsPaginateOff())
        {
            interpWrapper->SetWindowSize(true);
        }
        Currency output = interp->DoString(strCommand);

#ifndef OS_WIN
        if (strCommand == "\r\n")
        {
            std::cout << std::endl;
        }
#endif
#ifndef _DEBUG
		if (licCheckCounter >= 50) 
		{
			bool status = interpWrapper->CheckLicStatus();
			if (!status)
			{
                if (licCheckCounter == 50)
                {
                    interpWrapper->Print(
                        "Warning: Lost connection with the license server. Reconnect to restore the session, otherwise the application will exit.\n", true);
                }
                else
                {
                    delete interpWrapper;    // Checks in licenses for libraries
                    interpWrapper = nullptr;
                    exit(1);
                }
			}
			else
			{
				licCheckCounter = 0;
			}
            interpWrapper->CheckLibraries();  // Library license check
		}
		++licCheckCounter;
#endif
    }
}
//------------------------------------------------------------------------------
// Gets input command
//------------------------------------------------------------------------------
std::string GetInputCommand(ConsoleInterpreterWrapper* interpwrapper)
{
    assert(interpwrapper);

    std::string strCommand;
    while (1)
    {
		if (ClearCommandString)
		{
			strCommand.clear();
			ClearCommandString = false;
			std::cin.clear();
		}

        if (!strCommand.empty())
        {
            interpwrapper->PrintToStdout("??>");
            std::cout << std::flush;
        }
        std::string partialCommand;
        std::getline(std::cin, partialCommand);

#ifndef OS_WIN  // Try and trap control+D on Linux
        if (std::cin.eof())
        {
            std::cin.clear();
        }
#endif
        if (!partialCommand.empty())
            g_numUserInterrupts = 0; // Reset control-C interrupt attempts

        strCommand += partialCommand;

        if (!strCommand.empty())
        {
            strCommand += "\r\n";
        }

        bool isPartialExpression = interp->IsPartialExpression(strCommand);
        if (!isPartialExpression) break;
			
	}
    if (!strCommand.empty())
        g_numUserInterrupts = 0; // Reset control-C interrupt attempts

    strCommand.append("\r\n");
    return strCommand;
}
#ifdef OS_WIN
//------------------------------------------------------------------------------
// Called when control key is down
//------------------------------------------------------------------------------
BOOL OnControlKeyDown(DWORD controlType)
{
    fflush(stdout);
    ClearCommandString = true;

    // This routine may be called in a separate thread. So, just set interrupt
    // flag instead of calling exit handler which will destroy interpreter
    if (controlType == CTRL_C_EVENT)
    {
        SetUserInterrupt();
    }
    else if (controlType == CTRL_CLOSE_EVENT)
    {
        interpWrapper->CheckInLicense();
        interpWrapper->CheckinLibraries();
    }

    return true;
}
#else
//------------------------------------------------------------------------------
// Called when control key is down
//------------------------------------------------------------------------------
void OnControlKeyDown(int controlType)
{
	fflush(stdout);
    ClearCommandString = true;

    // This routine may be called in a separate thread. So, just set interrupt
    // flag instead of calling exit handler which will destroy interpreter
    if (controlType == SIGINT)
    {
        SetUserInterrupt();
    }
	else if (controlType == SIGHUP)
    {
		interpWrapper->CheckInLicense();
        interpWrapper->CheckinLibraries();
		// Force exit. It may not exit via SIGHUP/KILL in some systems.
		exit(EXIT_SUCCESS);
	}
}
#endif
//------------------------------------------------------------------------------
// Prints banner
//------------------------------------------------------------------------------
void PrintBanner()
{
#ifndef _DEBUG

    char* val = getenv("OML_HIDEBANNER");
    if (val)
    {
        std::string tmp(val);
        std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);
        if (tmp == "1" || tmp == "true")
        {
            return;
        }
    }
    std::string line ("//----------------------------------------------------------------------");

    std::cout << line << '\n'
        << "// " << GetVersion(interp->GetApplicationDir()) << '\n' << '\n'
        << "// Copyright (C) 2007-2022 Altair Engineering, Inc. All rights reserved.\n"
        << "// Copyright notice does not imply publication. ALTAIR ENGINEERING INC.\n"
        << "// Proprietary and Confidential. Contains Trade Secret Information. Not\n"
        << "// for use or disclosure outside of Licensee's organization. The software\n"
        << "// and information contained herein may only be used internally and is\n"
        << "// provided on a non-exclusive, non-transferable basis. Licensee may not\n"
        << "// sublicense, sell, lend, assign, rent, distribute, publicly display or\n"
        << "// publicly perform the software or other information provided herein, nor\n"
        << "// is Licensee permitted to decompile, reverse engineer, or disassemble\n"
        << "// the software. Usage of the software and other information provided by\n"
        << "// Altair (or its resellers) is only as explicitly stated in the applicable\n"
        << "// end user license agreement between Altair and Licensee. In the absence\n"
        << "// of such agreement, the Altair standard end user license agreement\n"
        << "// terms shall govern.\n"
        << line << std::endl;
#endif
}
//------------------------------------------------------------------------------
// Sets user interrupt
//------------------------------------------------------------------------------
void SetUserInterrupt()
{
    g_numUserInterrupts++;

    // First interruption will cause interpreter to interrupt. Subsequent 
    // interrupts will cause application to quit
    if (interp)
    {
        if (g_numUserInterrupts == 1)
        {
            interp->TriggerInterrupt();
        }
        std::cin.clear();
    }
}
//------------------------------------------------------------------------------
// Handles user interrupt returns true if application needs to quit
//------------------------------------------------------------------------------
bool HandleUserInterrupt(ConsoleInterpreterWrapper* omlWrapper)
{
    if (g_numUserInterrupts <= 0) 
    {
        return false;
    }

    assert(omlWrapper);
    if (!omlWrapper) 
    {
        return true;
    }

    
    if (g_numUserInterrupts == 1)
    {
        omlWrapper->PrintToStdout("\n");
        omlWrapper->HandleOnClearResults();
        return false;  // Only an interrupt was requested
    }
    omlWrapper->PrintToStdout("Are you sure you want to exit the application? Press 'C' to cancel...");
    fflush(stdout);

    std::string userInput;
    std::cin.clear();
    std::getline(std::cin, userInput);
    if (!userInput.empty())
    {
        std::transform(userInput.begin(), userInput.end(), userInput.begin(), ::tolower);
    }

    if (userInput == "c")
    {
        g_numUserInterrupts = 0;
        omlWrapper->HandleOnClearResults();
        fflush(stdout);
        return false;  // Ignoring the request to quit the application
    }

    fflush(stdout);
    std::cout << std::endl;
    omlWrapper->PrintNewPrompt();
    omlWrapper->PrintToStdout("Exiting the application...");
    omlWrapper->HandleOnSaveOnExit();

    return true;
}
//------------------------------------------------------------------------------
// Sets user interrupt handler - hooks to control C
//------------------------------------------------------------------------------
void SetUserInterruptHandler()
{
#ifdef OS_WIN
	SetConsoleCtrlHandler((PHANDLER_ROUTINE) OnControlKeyDown, TRUE);
#else
    signal(SIGINT, OnControlKeyDown);
    signal(SIGHUP, OnControlKeyDown); //Add method to handle terminal close?
#endif
}
//------------------------------------------------------------------------------
// Registers built in functions
//------------------------------------------------------------------------------
void RegisterBuiltInFuncs()
{
    assert(interp);

    interp->RegisterBuiltInFunction("version",            OmlVersion, 
        FunctionMetaData(0, 1, "CoreMinimalInterpreter"));
    interp->RegisterBuiltInFunction("getnumofcmdinputs",  OmlCommandInputsNumber,
        FunctionMetaData(0, 1, "CoreMinimalInterpreter"));
    interp->RegisterBuiltInFunction("getcmdinput",        OmlGetCommandInput, 
        FunctionMetaData(1, 1, "CoreMinimalInterpreter"));
	interp->RegisterBuiltInFunction("startupscript_path", OmlStartupscriptPath, 
        FunctionMetaData(1 ,0 , "System"));
	interp->RegisterBuiltInFunction("getargc",            OmlGetArgC,
        FunctionMetaData(1, 0, "CoreMinimalInterpreter"));
	interp->RegisterBuiltInFunction("getargv",            OmlGetArgV, 
        FunctionMetaData(1, 1, "CoreMinimalInterpreter"));

    // Functions available only in GUI. They are just added as dummy functions
    // here so that same scripts that are run in GUI/Console will not cause an
    // error
    interp->RegisterBuiltInFunction("errordlg", DummyIntFunc, 
                                     FunctionMetaData(-2, -1, "Gui"));
    interp->RegisterBuiltInFunction("inputdlg", DummyCellFunc, 
                                     FunctionMetaData(-4, 1, "Gui"));
    interp->RegisterBuiltInFunction("msgbox", DummyIntFunc, 
                                     FunctionMetaData(-2, -1, "Gui"));
    interp->RegisterBuiltInFunction("questdlg", DummyStringFunc, 
                                     FunctionMetaData(-6, 1, "Gui"));

	interp->RegisterBuiltInFunction("uigetdir", DummyStringFunc,
                                     FunctionMetaData(-2, 1, "Gui"));
    interp->RegisterBuiltInFunction("uigetfile", DummyStringFunc, 
                                     FunctionMetaData(-3, -3, "Gui"));
    interp->RegisterBuiltInFunction("uiputfile", DummyStringFunc, 
                                     FunctionMetaData(-3, -3, "Gui"));

    interp->RegisterBuiltInFunction("warndlg", DummyIntFunc, 
                                     FunctionMetaData(-2, -1, "Gui"));

    interp->RegisterBuiltInFunction("edit", DummyVoidFunc, 
                                     FunctionMetaData(0, 1, "Gui"));
    interp->RegisterBuiltInFunction("startupscript_edit", DummyVoidFunc, 
                                     FunctionMetaData(0, 0, "System"));
    interp->RegisterBuiltInFunction("listdlg", DummyMatrixFunc,
        FunctionMetaData(-16, -2, "Gui"));

    assert(interpWrapper);
    if (interpWrapper->IsProfessionalEdition())
    {
        // Register and lock library authorization functions
        interp->RegisterBuiltInFunction("getlibraryauthorization",
            OmlCheckLicense, FunctionMetaData(1, 1, "Core"));
        interp->RegisterBuiltInFunction("returnlibraryauthorization",
            OmlCheckinLicense, FunctionMetaData(1, 0, "Core"));
        interp->RegisterBuiltInFunction("testlibraryauthorization",
            OmlTestLicense, FunctionMetaData(1, 0, "Core"));

        interp->LockBuiltInFunction("getlibraryauthorization");
        interp->LockBuiltInFunction("returnlibraryauthorization");
        interp->LockBuiltInFunction("testlibraryauthorization");
    }
    // Lock api_utility functions added in the core for Activate. 
    // They should not show up anywhere in Compose GUI - VSM 8530
    // Added here so that funclist counts are the same in different modes
    interp->LockBuiltInFunction("clearenvvalue");
    interp->LockBuiltInFunction("cloneenv");
    interp->LockBuiltInFunction("getbaseenv");
    interp->LockBuiltInFunction("getcurrentenv");
    interp->LockBuiltInFunction("getenvvalue");
    interp->LockBuiltInFunction("getnewenv");
    interp->LockBuiltInFunction("importenv");
    interp->LockBuiltInFunction("importenvin");
    interp->LockBuiltInFunction("setenvvalue");
}
//------------------------------------------------------------------------------
// Runs input file(s)
//------------------------------------------------------------------------------
void RunInputFiles(const std::string& files)
{
    assert(interp);

    std::string in(files);
    while (!in.empty())
    {
        size_t pos = in.find(',');
        if (pos == std::string::npos)
        {
            interp->DoFile(in);
            std::cout << std::flush;
            break;
        }
        std::string fileToRun(in.substr(0, pos));
        interp->DoFile(fileToRun);
        std::cout << std::flush;
        in = in.substr(pos + 1);
    }
}
