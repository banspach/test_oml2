/**
* @file SignalHandler.h
* @date June 2016
* Copyright (C) 2016-2020 Altair Engineering, Inc. All rights reserved. 
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret 
* Information. Not for use or disclosure outside of Licensee's organization. 
* The software and information contained herein may only be used internally and 
* is provided on a non-exclusive, non-transferable basis.  License may not 
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly 
* perform the software or other information provided herein, nor is Licensee 
* permitted to decompile, reverse engineer, or disassemble the software. Usage 
* of the software and other information provided by Altair (or its resellers) is 
* only as explicitly stated in the applicable end user license agreement between 
* Altair and Licensee. In the absence of such agreement, the Altair standard end 
* user license agreement terms shall govern.
*/

#ifndef __SIGNALHANDLER_H__
#define __SIGNALHANDLER_H__

// Begin defines/includes
#include "Runtime/SignalHandlerBase.h"

class ConsoleInterpreterWrapper;
// End defines/includes

//------------------------------------------------------------------------------
//!
//! \class SignalHandler
//! \brief Class for handling actions from oml core in console
//!
//------------------------------------------------------------------------------
class SignalHandler : public SignalHandlerBase
{
public:
    //!
    //! Constructor
    //!
    SignalHandler() : _src (NULL) {} 
    //!
    //! Destructor
    //!
    virtual ~SignalHandler();

    //!
    //! Creates clone
    //!
    virtual SignalHandlerBase* CreateClone();
    //!
    //! Returns true of this is a clone
    //!
    virtual bool IsClone() const { return _src ? true : false; }
    
    //!
    //! Sets the console wrapper
    //! \param wrapper Wrapper pointer
    //!
    void SetConsoleWrapper(ConsoleInterpreterWrapper* wrapper) { _consolewrapper = wrapper; }

    //!
    //! Returns class info
    //!
    virtual const char* ClassName() const { return "SignalHandler"; }

    //!
    //! Clear results
    //!
    virtual void OnClearResultsHandler();
    //!
    //! Print result
    //! \param cur Result to print
    //!
    virtual void OnPrintResultHandler( const Currency& cur);
    //!
    //! Start pause
    //! \param msg  User message to display
    //! \param wait True if waiting for a keystroke input from user
    //!
    virtual void OnPauseStartHandler( const std::string& msg,
                                      bool               wait);
    //!
    //! Prints prompt for save on exit
    //! \param Return code to exit the application with
    //!
    virtual void OnSaveOnExitHandler(int);
    //!
    //! Adds nested display for pagination
    //! \param display Nested display to add
    //!
    virtual void OnAddDisplayHandler( CurrencyDisplay* display);
    //!
    //! Get user input
    //! \param prompt Prompt to display to user
    //! \param type   Type, if specified
    //! \param input  Input from user
    //!
    virtual void OnUserInputHandler( const std::string& prompt,
                                     const std::string& type,
                                     std::string&       input);
    //!
    //! Helper method which returns true if application is in Console mode
    //!
    virtual bool IsInConsoleMode() const { return true; }
    //!
    //! Returns true if in Console, non-interactive mode
    //!
    virtual bool IsInConsoleBatchMode() const { return _batchmode; }
    //! 
    //! Sets batch mode option
    //! \param val True if application is in batch mode
    //!
    void SetConsoleBatchMode(bool val) { _batchmode = val; }

private:
    
    const SignalHandler*       _src;            //!< Source, if this is a clone                 
    ConsoleInterpreterWrapper* _consolewrapper; //!< Console wrapper
    bool                       _batchmode;      //!< True if application is in batch mode

    //!
    //! Copy constructor
    //! \param src Source
    //!
    SignalHandler( const SignalHandler* src);
};

#endif
// End of file:
