/**
* @file BetterCalc.h
* @date November 2015
* Copyright (C) 2015-2020 Altair Engineering, Inc. All rights reserved. 
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret 
* Information. Not for use or disclosure outside of Licensee's organization. 
* The software and information contained herein may only be used internally and 
* is provided on a non-exclusive, non-transferable basis.  License may not 
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly 
* perform the software or other information provided herein, nor is Licensee 
* permitted to decompile, reverse engineer, or disassemble the software. Usage 
* of the software and other information provided by Altair (or its resellers) is 
* only as explicitly stated in the applicable end user license agreement between 
* Altair and Licensee. In the absence of such agreement, the Altair standard end 
* user license agreement terms shall govern.
*/
#ifndef __BETTERCALC_H__
#define __BETTERCALC_H__

class ConsoleInterpreterWrapper;

//!
//! Handles user interrupt returns true if application needs to quit
//! \param omlWrapper Oml interpreter wrapper
//!
bool HandleUserInterrupt(ConsoleInterpreterWrapper* omlWrapper);
//!
//! Sets user interrupt
//!
void SetUserInterrupt();
//!
//! Sets user interrupt handler - hooks to control C
//!
void SetUserInterruptHandler();
//!
//! Prints banner
//!
void PrintBanner();
//!
//! Gets input command
//! \param interpwrapper Oml interp wrapper
//!
std::string GetInputCommand(ConsoleInterpreterWrapper* interpwrapper);
//!
//! Encrypts given file (encrypt command)
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool oml_encryptfile(EvaluatorInterface           eval, 
                     const std::vector<Currency>& inputs, 
                     std::vector<Currency>&       outputs);
//!
//! Prints help
//!
void OML_help();
//!
//! Gets the version string of the application
//! \param appdir Application directory
//!
std::string GetVersion(const std::string& appdir);
//! Returns true if successful in getting the version string (version command)
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlVersion(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs);
//! 
//! Registers oml built in functions
//! 
void RegisterBuiltInFuncs();
//!
//! Gets command input at the given index
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlGetCommandInput(EvaluatorInterface           eval,
                        const std::vector<Currency>& inputs,
                        std::vector<Currency>&       outputs);
//!
//! Gets number of command input arguments
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlCommandInputsNumber(EvaluatorInterface           eval,
                            const std::vector<Currency>& inputs,
                            std::vector<Currency>&       outputs);
//!
//! Gets the path of the startup script (startupscript_path command)
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlStartupscriptPath(EvaluatorInterface           eval,
                          const std::vector<Currency>& inputs,
                          std::vector<Currency>&       outputs);
//!
//! Gets argc (getargc command)
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlGetArgC(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs);
//!
//! Gets argv at the given index (getargv command)
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlGetArgV(EvaluatorInterface           eval,
                const std::vector<Currency>& inputs,
                std::vector<Currency>&       outputs);
//!
//! Gets the path of the start up script
//!
std::string GetStartupScriptPath();
//!
//! Dummy method, returning an empty cell if needed - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyCellFunc(EvaluatorInterface           eval,
                  const std::vector<Currency>& inputs,
                  std::vector<Currency>&       outputs);
//!
//! Dummy method, returning 1 if needed - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyIntFunc(EvaluatorInterface           eval,
                  const std::vector<Currency>& inputs,
                  std::vector<Currency>&       outputs);
//!
//! Dummy method returning empty string if needed - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyStringFunc(EvaluatorInterface           eval,
                     const std::vector<Currency>& inputs,
                     std::vector<Currency>&       outputs);
//!
//! Dummy method returning true - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyVoidFunc(EvaluatorInterface           eval,
                   const std::vector<Currency>& inputs,
                   std::vector<Currency>&       outputs);
//!
//! Dummy method returning true - implemented in GUI only
//! \param eval
//! \param inputs
//! \param outputs
//!
bool DummyMatrixFunc(EvaluatorInterface           eval,
                     const std::vector<Currency>& inputs,
                     std::vector<Currency>&       outputs);
//!
//! Initializes environment variables
//!
void InitEnvVariables();
//!
//! Returns true after getting library authorization [getlibraryauthorization]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlCheckLicense(EvaluatorInterface           eval,
                     const std::vector<Currency>& inputs,
                     std::vector<Currency>&       outputs);
//!
//! Returns true after returning library authorization [returnlibraryauthorization]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlCheckinLicense(EvaluatorInterface           eval,
                       const std::vector<Currency>& inputs,
                       std::vector<Currency>&       outputs);
//!
//! Returns true after testing library authorization [testlibraryauthorization]
//! \param eval    Evaluator interface
//! \param inputs  Vector of inputs
//! \param outputs Vector of outputs
//!
bool OmlTestLicense(EvaluatorInterface           eval,
                    const std::vector<Currency>& inputs,
                    std::vector<Currency>&       outputs);
#endif

// End of file:

