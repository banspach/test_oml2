/**
* @file ConsoleInterpreterWrapper.h
* @date November 2015
* Copyright (C) 2015-2022 Altair Engineering, Inc. All rights reserved. 
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret 
* Information. Not for use or disclosure outside of Licensee's organization. 
* The software and information contained herein may only be used internally and 
* is provided on a non-exclusive, non-transferable basis.  License may not 
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly 
* perform the software or other information provided herein, nor is Licensee 
* permitted to decompile, reverse engineer, or disassemble the software. Usage 
* of the software and other information provided by Altair (or its resellers) is 
* only as explicitly stated in the applicable end user license agreement between 
* Altair and Licensee. In the absence of such agreement, the Altair standard end 
* user license agreement terms shall govern.
*/

#ifndef __CONSOLEINTERPRETERWRAPPER__
#define __CONSOLEINTERPRETERWRAPPER__

// Begin defines/includes
#include "Runtime/Currency.h"

#include <map>
#include <stack>
#include <string>
#include <vector>

#ifdef OS_WIN
#   include <Windows.h>
#endif

class CurrencyDisplay;
class Interpreter;
class SignalHandler;
// End defines/includes

//------------------------------------------------------------------------------
//! 
//! \class ConsoleInterpreterWrapper
//! \brief Handles interpretor signals and methods for console
//!
//------------------------------------------------------------------------------
class ConsoleInterpreterWrapper
{
public:
    //!
    //! Constructor
    //! \param[in] interpretor
    //!
    ConsoleInterpreterWrapper(Interpreter* interp);
    //!
    //! Destructor
    //!
    ~ConsoleInterpreterWrapper();

    //!
    //! Returns true if pagination is in process
    //!
    bool IsPaginating();
    //!
    //! Paginates matrix
    //!
    void Paginate();

    //!
    //! Prints new prompt and resets append flags
    //!
    void PrintNewPrompt();
    //!
    //! Prints to stdout
    //! \param msg Message to print
    //!
    void PrintToStdout(const std::string& msg);
    //!
    //! Prints to stdout
    //! \param msg Message to print
    //! \param forceFlush True if stdout needs to be flushed
    //!
    void Print(const std::string& msg,
               bool               forceFlush);


    //!
    //! Prints currency to console
    //! \param cur Currency to print
    //!
	void HandleOnPrintResult(const Currency& cur);
    //!
    //! Clears results and pagination related to results
    //!
    void HandleOnClearResults();
    //!
    //! Handles application exit
    //! \param Return code to exit the application with 
    //!
	void HandleOnSaveOnExit(int returnCode = EXIT_SUCCESS);
    //!
    //! Displays a prompt and gets user input
	//! \param prompt    Prompt to display to user
    //! \param type      Type, if specified
	//! \param userInput Input from user
    //!
    void HandleOnGetUserInput(const std::string& prompt,
                              const std::string& type,
		                      std::string&       userInput);
    //!
    //! Initiates a user defined pause
    //! \param msg  User message to display
    //! \param wait True if waiting for a keystroke input from user
    //!
	void HandleOnPauseStart(const std::string& msg, 
                            bool               wait);
    //!
    //! Handles nested displays during pagination
    //! \param display Display to be added
    //!
    void HandleOnAddDisplay(CurrencyDisplay* display);

    //!
    //! Gets the number of input arguments
    //!
    int GetNumberOfInputArgs() const;
    //! 
    //! Gets the command input at the given index
    //! \param idx 1-based index of the command input 
    //!
    std::string GetInputArg(int idx) const;
    //!
    //! Sets the input args
    //! \param args Input args
    //!
    void SetInputArgs(const std::vector<std::string>& args) { _inputArgs = args; }

    //!
    //! Gets argc
    //!
    int GetArgc() const;
    //!
    //! Gets argv at the given index
    //! \param idx 1-based index
    //!
    std::string GetArgv(int idx) const;
    //!
    //! Sets argv
    //! \param args Args
    //!
    void SetArgv(const std::vector<std::string>& args) { _argv = args; }

    // Licensing APIs

    //!
    //! Authorize Console application
	//! \param user    user id
    //! \param pwd     password
	//! \param errMsg  license error msg
    //!
    bool Authorize(const std::string& user, 
                   const std::string& pwd, 
                   std::string&       errMsg);
    //!
    //! Returns true if given oml module could be authorized
    //! \param module Module info
    //! \param out    Encoded output
    //!
    bool AuthorizeOmlModule(const std::string& module,
                            std::string&       out);

    //!
    //! check if professional edition
    //!
    bool IsProfessionalEdition();
    //!
    //! check in product license
    //!
    void CheckInLicense();
    //!
    //! check the license status
    //!
    bool CheckLicStatus();
    //!
    //! Get license edition string
    //! \param appendEdition  append "Edition"
    //!
    std::string EditionString(bool appendEdition = false);

    //!
    //! Returns true after checking library license
    //! \param Library id
    //! \param Result
    //! \param Error
    //!
    bool CheckoutLibrary(const std::string&, std::string&, std::string&);
    //!
    //! Checks in library license
    //! \param Library id
    //!
    void CheckinLibrary(const std::string&);
    //!
    //! Checks in all library licenses
    //!
    void CheckinLibraries();
    //!
    //! Gets encoded result after testing oml library license
    //! \param Library info
    //!
    std::string TestLibrary(const std::string&);
    //!
    //! Checks status of all libraries
    //!
    void CheckLibraries();

    static std::string OmlVersion;                //!< Product version
        //!
    //! Initializes command window screen buffer for interactive mode
    //!
    void InitCommandWindowInfo();
    //!
    //! Gets visible rows and columns from command window screen size
    //!
    void GetCommandWindowInfo();
    void SetWindowSize(bool val) { _getWindowSize = val; }

    //!
    //! Sets child signal handler
    //!
    void SetChildSignalHandler(SignalHandler* handler) { _childHandler = handler; }

private:
    Interpreter* _interp;                         //!< Interpreter

    void* _licHandle;                             //!< handle to license library

    bool _appendOutput;                           //!< True if output is appended
    bool _addnewline;                             //!< True if new line should be added

    std::vector<Currency>        _resultsToPrint; //!< Results to print
    std::stack<CurrencyDisplay*> _displayStack;   //!< Stack of currenct displays

    std::vector<std::string> _inputArgs;          //!< Input args
    std::vector<std::string> _argv;               //!< Arg v

    bool _getWindowSize;                          //!< True when size needs to be updated

#ifdef OS_WIN
    HANDLE _inhandle;                             //!< Stdin  handle
    HANDLE _outhandle;                            //!< Stdout handle
    WORD   _defaultWinAttr;                       //!< Console window  attribute
#endif
    SignalHandler* _childHandler;                 //!< Child signal handler
    //!
    // Private default constructor
    //!
    ConsoleInterpreterWrapper() : _interp(0), _childHandler(0) {}
    //!
    //! Stubbed out copy constructor
    //!
    ConsoleInterpreterWrapper(const ConsoleInterpreterWrapper&);
    //!
    //! Stubbed out assignment operator
    //!
    ConsoleInterpreterWrapper& operator=(const ConsoleInterpreterWrapper&);

    //!
    //! Load license library
    //!
    void LoadLicenseLib();

    //!
    //! Connects oml signal handler
    //!
    void ConnectOmlSignalHandler();
    //!
    //! Disconnect oml signal handler
    //!
    void DisconnectOmlSignalHandler();

    //!
    //! Cleans up after pagination like clearing display
    //! \param printMsg True if end of pagination message needs to be printed
    //!
    void EndPagination(bool printMsg = true);
    //!
    //! Processes pagination
    //!
    void ProcessPagination();
    //!
    //! Print currency. Return false if this currency could not be printed and 
    //! needs to be processed later
    //! \param cur Currency (result) to print
    //!
    bool PrintResult(const Currency& cur);
    //!
    //! Prints string to console
    //! \param result        Result
    //! \param isprintoutput True if this is a result from printf/fprintf
    //! \param forceflush    Force flush if true
    //!
    void PrintToConsole(const std::string& result,
                        bool               isprintOutput,
                        bool               forceflush);
    //!
    //! Prints message for continuing/skipping pagination
    //! \param colPaginate True if column control keys are dispalyed
    //!
    void PrintPaginationMessage(bool showColControl);
    //!
    //! Prints end of pagination message
    //!
    void PrintPaginationMessage();
    //!
    //! Prints info message (in different color than results) to command window
    //! \param msg  Message to print
    //!
    void PrintInfoToOmlWindow(const std::string& msg);
    //!
    //! Initializes and caches display given the currency
    //! \param cur Given currency
    //!
    void CacheDisplay(const Currency& cur);
    //!
    //! Clears display
    //!
    void ClearDisplay();
    //!
    //! Gets current display
    //!
    CurrencyDisplay* GetCurrentDisplay() const;
    //!
    //! Deletes chained displays, if any
    //! \param display Given display
    //!
    void DeleteChainedDisplays(CurrencyDisplay* display);
    //!
    //! Gets password
    //! 
    std::string GetPassword();
    //!
    //! Splits input prompt by new linesand returns the tokens
    //! \param Input prompt
    //! 
    std::vector<std::string> SplitInputPrompt(const std::string&) const;

};


#endif

// End of file:
