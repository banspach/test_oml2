﻿/**
* @file ConsoleInterpreterWrapper.cpp
* @date November 2015
* Copyright (C) 2015-2022 Altair Engineering, Inc. All rights reserved. 
* Copyright notice does not imply publication.
* ALTAIR ENGINEERING INC. Proprietary and Confidential. Contains Trade Secret 
* Information. Not for use or disclosure outside of Licensee's organization. 
* The software and information contained herein may only be used internally and 
* is provided on a non-exclusive, non-transferable basis.  License may not 
* sublicense, sell, lend, assign, rent, distribute, publicly display or publicly 
* perform the software or other information provided herein, nor is Licensee 
* permitted to decompile, reverse engineer, or disassemble the software. Usage 
* of the software and other information provided by Altair (or its resellers) is 
* only as explicitly stated in the applicable end user license agreement between 
* Altair and Licensee. In the absence of such agreement, the Altair standard end 
* user license agreement terms shall govern.
*/

// Begin defines/includes

#include "ConsoleInterpreterWrapper.h"

#include "SignalHandler.h"
#include "hwMatrix_NMKL.h"

#include <algorithm>
#include <cassert>
#include <sstream>

#ifndef OS_WIN
#   include <sys/ioctl.h>
#   include <termios.h>
#   include <unistd.h>
#else
#   include <conio.h>
#endif

#include "Runtime/BuiltInFuncsUtils.h"
#include "Runtime/BuiltInFuncsCore.h"
#include "Runtime/CurrencyDisplay.h"
#include "Runtime/Interpreter.h"
#include "Runtime/OutputFormat.h"

//# define CONSOLEINTERPRETERWRAPPER_DBG 1  // Uncomment to print debug info
#ifdef CONSOLEINTERPRETERWRAPPER_DBG
#    define CONSOLEINTERPRETERWRAPPER_PRINT(m) { std::cout << m; }
#ifndef OS_WIN
#    define CONSOLEINTERPRETERWRAPPER_ATTACH   0
#else
#    define CONSOLEINTERPRETERWRAPPER_ATTACH { MessageBox(NULL, (LPCSTR)"Attach debugger", (LPCSTR)"Compose", MB_DEFBUTTON2); }
#endif 
#else
#    define CONSOLEINTERPRETERWRAPPER_PRINT(m) 0
#    define CONSOLEINTERPRETERWRAPPER_ATTACH   0
#endif

int maxcols = 0;

#ifndef OS_WIN
enum ARROWKEY
{
    ARROWKEY_UP    = 65,
    ARROWKEY_LEFT  = 68,
    ARROWKEY_RIGHT = 67,
    ARROWKEY_DOWN  = 66
};
#endif
// End defines/includes

// Function pointers for licensing
typedef int (*lic_auth_fptr) (const std::string& user, const std::string& pwd, int mode);
typedef bool (*lic_ispro_fptr)();
typedef bool (*lic_status_fptr)();
typedef void (*lic_checkin_fptr)();
typedef bool (*lic_edition_fptr)(std::string& editionStr, bool appendEdition);

// Function pointers for library licensing
typedef void (*OmlFuncPtrVoid)();
typedef bool (*OmlCheckoutLibraryProc)(const std::string&, std::string&, std::string&);
typedef void (*OmlCheckinLibraryProc)(const std::string&);
typedef void (*OmlTestLibraryProc)(const std::string&, std::string&);
typedef void (*OmlSetProductVersionProc)(const std::string&);
std::string ConsoleInterpreterWrapper::OmlVersion = "2022.3";

const size_t g_buffsize = 256 * 1024;
static char  g_buf[g_buffsize + 1];
// Utility to flush output buffer
void FlushStdout()
{
    fflush(stdout);
    memset(g_buf, 0, sizeof(g_buf));
}

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
ConsoleInterpreterWrapper::ConsoleInterpreterWrapper(Interpreter* interp)
    : _interp           (interp)
    , _licHandle        (nullptr)
    , _appendOutput     (false)
    , _addnewline       (false)
    , _getWindowSize    (false)
    , _childHandler     (nullptr)
{
    memset(g_buf, 0, sizeof(g_buf));
    std::cout.rdbuf()->pubsetbuf(g_buf, g_buffsize);
    std::cin.tie(nullptr);

    ConnectOmlSignalHandler();

#ifndef _DEBUG
    LoadLicenseLib();
#endif
}
//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
ConsoleInterpreterWrapper::~ConsoleInterpreterWrapper()
{
    BuiltInFuncsUtils::CloseOutputLog();

    std::cout.rdbuf()->pubsetbuf(nullptr, 0);

    DisconnectOmlSignalHandler();

#ifndef _DEBUG
	CheckInLicense();
    CheckinLibraries();
    BuiltInFuncsCore::DyFreeLibrary(_licHandle);
    _licHandle = nullptr;
#endif
}
//------------------------------------------------------------------------------
// load license library
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::LoadLicenseLib()
{   
    //load license library
    std::string importDll = "hwcomposelic";
#ifndef OS_WIN
    importDll = "lib" + importDll + ".so";
#endif
    _licHandle = BuiltInFuncsCore::DyLoadLibrary(importDll.c_str());
}
//------------------------------------------------------------------------------
// Connects signals
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::ConnectOmlSignalHandler()
{
    assert(_interp);

    // Set the signal handler
    SignalHandler* handler = new SignalHandler;
    assert(handler);
    handler->SetConsoleWrapper(this);
    _interp->SetSignalHandler(handler);

    CONSOLEINTERPRETERWRAPPER_ATTACH;
}
//------------------------------------------------------------------------------
// Disconnect signal handler
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::DisconnectOmlSignalHandler()
{
    assert(_interp);
    SignalHandlerBase* handler = _interp->GetSignalHandler();
    if (!handler) return;

    delete handler;
    _interp->SetSignalHandler(NULL);
}
//------------------------------------------------------------------------------
// Slot called when printing result to console
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::HandleOnPrintResult(const Currency& cur)
{	
    bool islog = cur.IsLogOutput();
    bool closelog = false;
    if (islog && !BuiltInFuncsUtils::IsOutputLogOpen() && _displayStack.empty())
    {
        BuiltInFuncsUtils::OpenOutputLogForAppend(); // This currency is coming from a child evaluator
        closelog = true;
    }

    // Force to paginate on for large matrices so that output is displayed
    // faster and the output string is not too big
    if (cur.IsMatrix())
    {
        const hwMatrix* mtx = cur.Matrix();
        if (mtx && mtx->Size() >= CurrencyDisplay::GetSkipFormat())
        {
            CurrencyDisplay* disp = cur.GetDisplay();
            if (disp)
            {
                disp->SetLocalPaginate();
            }
        }
    }

    bool couldPrint = PrintResult(cur);
    if (closelog)
    {
        BuiltInFuncsUtils::CloseOutputLog();
    }
    if (!couldPrint)
        _resultsToPrint.push_back(cur);  // Print later, once pagination is done
}
//------------------------------------------------------------------------------
// Gets visible rows and columns from command window screen size
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::GetCommandWindowInfo()
{
    if (!_getWindowSize)
    {
        return;
    }
    
    int rows = 0;
    int cols = 0;

    maxcols = 0;
#ifdef OS_WIN  // Windows specific code
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    GetConsoleScreenBufferInfo(_outhandle, &csbi);

    // Lines and columns use the screen size and not the buffer size
    rows = csbi.srWindow.Bottom - csbi.srWindow.Top;
    cols = csbi.srWindow.Right  - csbi.srWindow.Left;
#else
    struct winsize w;
    ioctl(0, TIOCGWINSZ, &w);
    rows = w.ws_row;
    if (rows > 2)
    {
        rows--;  // Reduce one row to account for pagination message
    }
    cols = w.ws_col;
#endif    

    maxcols = cols;
    CurrencyDisplay::SetMaxCols(cols);
	CurrencyDisplay::SetMaxRows(rows);

    _getWindowSize = false;
}
//------------------------------------------------------------------------------
// Print currency. Return false if this currency could not be printed and 
// needs to be processed later
//------------------------------------------------------------------------------
bool ConsoleInterpreterWrapper::PrintResult(const Currency& cur)
{
    assert(_interp);
    const OutputFormat* format = _interp->GetOutputFormat();

    if (!_displayStack.empty())
    {
        return false;  // Can't print yet as display stack is not empty
    }

    bool canPaginate = CurrencyDisplay::CanPaginate(cur);
    bool forceflush = (cur.IsPrintfOutput() || cur.IsDispOutput() || cur.IsError());
    if (!canPaginate)
    {
        // There is no print in progress, so don't need to worry
        PrintToConsole(cur.GetOutputString(format), cur.IsPrintfOutput(), forceflush);
        return true;
    }
    GetCommandWindowInfo();
    if (!CurrencyDisplay::IsValidDisplaySize()) // No window size available so just print
    {
        PrintToConsole(cur.GetOutputString(format), cur.IsPrintfOutput(), forceflush);
        return true;
    }

    // At this point, the currency that can paginate. Cache if possible
    CacheDisplay(cur);
    bool wasPaginating = false;
   
    if (_interp->IsInterrupt())  // Check if there has been an interrupt
    {
        PrintPaginationMessage();
        HandleOnClearResults();
        return true;  // Done with printing
    }

    CurrencyDisplay* display = GetCurrentDisplay();
    assert(display);
    PrintToConsole(cur.GetOutputString(format), cur.IsPrintfOutput(), forceflush);

    CurrencyDisplay* topdisplay = GetCurrentDisplay();
    assert(topdisplay);

    if (topdisplay != display)
    {
        // Nested pagination
        Currency newcur = const_cast<Currency&>(topdisplay->GetCurrency());
        newcur.SetDisplay(topdisplay);
        if (topdisplay)
        {
            wasPaginating = (topdisplay->IsPaginatingRows() || topdisplay->IsPaginatingCols());
        }
        else
        {
            wasPaginating = false;
        }
    }

    if (topdisplay->IsPaginatingCols() || topdisplay->IsPaginatingRows())
    {
        PrintPaginationMessage(topdisplay->CanPaginateColumns());
        return true;               // Done printing for pagination
    }

    EndPagination(wasPaginating);           // Done paginating

    if (!_displayStack.empty())
    {
        display = _displayStack.top();
        assert(display);
        if (display->IsPaginatingCols() || display->IsPaginatingRows())
            PrintPaginationMessage(display->CanPaginateColumns());
        else
            EndPagination(true);  // Done paginating
        return true;
    }

    return true;
}
//------------------------------------------------------------------------------
// Prints message for continuing/skipping pagination
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::PrintPaginationMessage(bool showColControl)
{
    if (CurrencyDisplay::IsPaginateInteractive())
    {
        std::string msg("-- (f)orward, (b)ack, (q)uit, (e)xit");

        msg += ", (up), (down)";
        if (showColControl)
        {
            msg += ", (left), (right)";
        }

        FlushStdout();

#ifdef OS_WIN

        // Gray background with white letters
        SetConsoleTextAttribute(_outhandle, BACKGROUND_INTENSITY |
            FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
        
        Print(msg, true);
        FlushStdout();

        SetConsoleTextAttribute(_outhandle, _defaultWinAttr); // Reset color info
#else
        Print(msg, true);
#endif
    }
}
//------------------------------------------------------------------------------
// Prints end of pagination message
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::PrintPaginationMessage()
{
    CurrencyDisplay* display = GetCurrentDisplay();
    if (display)
    {
        std::string strmsg;
        display->GetPaginationEndMsg(strmsg);
        if (!strmsg.empty())
        {
            Print(strmsg + '\n', true);
        }
    }
}
//------------------------------------------------------------------------------
// Prints string to console
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::PrintToConsole(const std::string& result,
                                               bool               isprintoutput,
                                               bool               forceflush)
{	
    if (!result.empty())
    {
        std::string msg;
        if ((!_appendOutput && _addnewline) || (!isprintoutput && _appendOutput))
        {
            msg += '\n';
        }

        bool hasTrailingNewline = (result.back() == '\n');
        if (!isprintoutput)
        {
            msg += result;
            if (!hasTrailingNewline)
            {
                msg += '\n';
            }
            _appendOutput = false;
            _addnewline   = false;
            PrintToStdout(msg);
            if (forceflush)
            {
                FlushStdout();
            }

            return;
        }

        if (!hasTrailingNewline)
        {
            msg += result;
        }
        else
        {
            std::string tmp(result);
            tmp.pop_back();
            msg += tmp;
            _addnewline = true;
        }
        PrintToStdout(msg);
        if (forceflush)
        {
            FlushStdout();
        }

        _appendOutput = !hasTrailingNewline;
    }
}
//------------------------------------------------------------------------------
// Clears results and pagination related to data
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::HandleOnClearResults()
{
    CurrencyDisplay::ClearLineCount();
    _appendOutput = false;

    _resultsToPrint.clear();

    while (!_displayStack.empty())
    {
        EndPagination(false); 
    }
}
//------------------------------------------------------------------------------
// Processes pagination
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::ProcessPagination()
{
    CurrencyDisplay* display = GetCurrentDisplay();
    if (!display)
    {
        return;
    }
    else if (_interp->IsInterrupt())
    {
        HandleOnClearResults();   // Quit all printing
        return;
    }

    if (display->GetMode() == CurrencyDisplay::DISPLAYMODE_EXIT)
    {
        if (CurrencyDisplay::IsPaginateInteractive())
        {
            std::cout << '\r' << '\n';
            BuiltInFuncsUtils::SaveToOutputLog("\n");
        }
        HandleOnClearResults();   // Quit all printing
    }
    else
    {
        bool printmsg = true;
        if (display->GetMode() != CurrencyDisplay::DISPLAYMODE_QUIT)
        {
	        // Print to console
            display->SetModeData();

            const Currency& curBeingPrinted = display->GetCurrency();
            const_cast<Currency&>(curBeingPrinted).SetDisplay(display);

            std::string out = curBeingPrinted.GetOutputString(_interp->GetOutputFormat());
            if (CurrencyDisplay::IsPaginateInteractive())
            {
                if (display->GetDeleteLine())
                {
                    std::cout << '\r';
                    display->SetDeleteLine(false);
                }
                else
                {
                    std::cout << '\n';
                    BuiltInFuncsUtils::SaveToOutputLog("\n");
                }
            }
            bool isPrintOutput = curBeingPrinted.IsPrintfOutput();
            bool forceflush = (curBeingPrinted.IsDispOutput() || 
                               curBeingPrinted.IsError()      || 
                               curBeingPrinted.IsPrintfOutput());
            // Reget display as there could be nested pagination
            CurrencyDisplay* topdisplay = GetCurrentDisplay();
            assert(topdisplay);
            if (topdisplay != display)
            {
                Currency newcur = const_cast<Currency&>(topdisplay->GetCurrency());
                newcur.SetDisplay(topdisplay);
            }

            PrintToConsole(out, isPrintOutput, forceflush);
	        if (topdisplay->IsPaginatingCols() || topdisplay->IsPaginatingRows()) 
            {
                PrintPaginationMessage(topdisplay->CanPaginateColumns());
                return; // Still printing
            }
        }
        else
        {
            if (CurrencyDisplay::IsPaginateInteractive())
            {
                std::cout << '\r' << '\n';
                display->SetDeleteLine(false);
            }
            printmsg = false;
        }
        EndPagination(printmsg);  // Done with pagination

        if (!_displayStack.empty())
        {
            display = _displayStack.top();
            if (display && display->IsPaginatingCols() || display->IsPaginatingRows())
            {
                if (!display->CanPrintRows() || CurrencyDisplay::IsPaginateInteractive())
                {
                    PrintPaginationMessage(display->CanPaginateColumns());
                    return;  // Nothing more to print
                }
                // Print the next rows
                display->SetModeData();
                const Currency& curBeingPrinted = display->GetCurrency();
                const_cast<Currency&>(curBeingPrinted).SetDisplay(display);
            
                std::string out = curBeingPrinted.GetOutputString(_interp->GetOutputFormat());
                bool        isprintoutput = curBeingPrinted.IsPrintfOutput();
                bool forceflush = (curBeingPrinted.IsDispOutput() ||
                    curBeingPrinted.IsError() ||
                    curBeingPrinted.IsPrintfOutput());

                PrintToConsole(out, isprintoutput, forceflush);

                CurrencyDisplay* topdisplay = GetCurrentDisplay();
                assert(topdisplay);
                if (topdisplay != display)
                {
                    // Nested pagination
                    Currency newcur = const_cast<Currency&>(topdisplay->GetCurrency());
                    newcur.SetDisplay(topdisplay);
                    display = topdisplay;
                }

                if (topdisplay->IsPaginatingCols() || topdisplay->IsPaginatingRows())
                {
                    PrintPaginationMessage(topdisplay->CanPaginateColumns());
                    return;
                }
                
            }
            if (display && !(display->IsPaginatingCols() || display->IsPaginatingRows()))
            {
                EndPagination(printmsg);
            }
            if (!_displayStack.empty())
            {
                if (CurrencyDisplay::IsPaginateInteractive())
                {
                    display = GetCurrentDisplay();
                    if (display && (
                        (display->IsPaginatingRows() || display->IsPaginatingCols()) ||
                        display->IsNDCellDisplay()))
                    {
                        PrintPaginationMessage(display->CanPaginateColumns());
                        return;               // Done printing for pagination
                    }
                }
            }

        }
    }

    // Process all the other results
    for (std::vector<Currency>::iterator itr = _resultsToPrint.begin();
         itr != _resultsToPrint.end();)
    {
        if (IsPaginating()) break;

        EndPagination(true);

        bool couldPrint = PrintResult(*itr);
        if (!couldPrint)
            ++itr;
        else
            itr = _resultsToPrint.erase(itr);
    }
}
//------------------------------------------------------------------------------
// Paginates matrix and returns true if user has not pressed quit 
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::Paginate()
{
    assert(_interp);
    assert(!_displayStack.empty());

#ifdef OS_WIN
    // Go in a loop till we either get the pagination controls or a quit
    while(1)
    {   
        CurrencyDisplay* display = _displayStack.top();
        bool canPaginateCols = display ? display->CanPaginateColumns() : false;

        if (CurrencyDisplay::IsPaginateInteractive())
        {
            DWORD        charToRead = 1;  // Num chars to read
            DWORD        numEvents = 0;  // Num events read
            INPUT_RECORD input;	          // Input buffer data

            // Stop from echoing.
            FlushConsoleInputBuffer(_inhandle);
            BOOL read = ReadConsoleInput(_inhandle, &input, charToRead, &numEvents);
            if (!read)
            {
                CONSOLEINTERPRETERWRAPPER_PRINT("Error reading console input\n");
                HandleOnClearResults();
                return;
            }

            if (_interp->IsInterrupt()) return;

            if (input.EventType != KEY_EVENT || !input.Event.KeyEvent.bKeyDown)
                continue; // Not a key event with key down

            WORD key = input.Event.KeyEvent.wVirtualKeyCode;

            if (key == 0x46 || key == 0x66 || key == 0x0D)      // (f)orward pagination
                display->SetMode(CurrencyDisplay::DISPLAYMODE_FORWARD);

            else if (key == 0x51 || key == 0x71)      // (q)uit
            {
                DeleteChainedDisplays(display);
                display->SetMode(CurrencyDisplay::DISPLAYMODE_QUIT);
            }

            else if (key == 0x42 || key == 0x62)      // (b)ack pagination
                display->SetMode(CurrencyDisplay::DISPLAYMODE_BACK);

            else if (key == VK_RIGHT)                // right pagination
            {
                if (canPaginateCols)
                {
                    display->SetMode(CurrencyDisplay::DISPLAYMODE_RIGHT);
                }
                else
                {
                    display->SetMode(CurrencyDisplay::DISPLAYMODE_FORWARD);
                }
            }
            else if (key == VK_LEFT)                // left pagination
            {
                if (canPaginateCols)
                {
                    display->SetMode(CurrencyDisplay::DISPLAYMODE_LEFT);
                }
                else
                {
                    display->SetMode(CurrencyDisplay::DISPLAYMODE_BACK);
                }
                break;
            }

            else if (key == VK_UP)                // Up pagination
                display->SetMode(CurrencyDisplay::DISPLAYMODE_UP);

            else if (key == VK_DOWN)                // Down pagination
                display->SetMode(CurrencyDisplay::DISPLAYMODE_DOWN);

            else if (key == 0x45 || key == 0x65)    // (e)xit
            {
                DeleteChainedDisplays(display);
                display->SetMode(CurrencyDisplay::DISPLAYMODE_EXIT);
            }

            else
            {
                // Treat any other key as a quit command
                DeleteChainedDisplays(display);
                display->SetMode(CurrencyDisplay::DISPLAYMODE_QUIT);
            }

            // Delete the last line has the pagination message
            std::string deletedline("\r");
            for (int i = 1; i < maxcols - 1; ++i)
            {
                deletedline += " ";
            }
            std::cout << deletedline;
        }
        else
        {
            display->SetMode(CurrencyDisplay::DISPLAYMODE_FORWARD);
        }
		ProcessPagination();
        bool isPaginating = IsPaginating();
        if (!isPaginating) 
        {
            return;    // Done paginating
        }
    }
#else
    // Get the terminal settings so there is no echo of input during 
    // pagination and user does not have to press the enter key

    struct termios savedt;
    tcgetattr(STDIN_FILENO, &savedt);
    struct termios tmpt = savedt;
    tmpt.c_lflag &= ~(ICANON | ECHO);  // Disable echo and waiting for EOL
    tcsetattr(STDIN_FILENO, TCSANOW, &tmpt);

    while (1)
    {        
        if (_interp->IsInterrupt()) break;

        CurrencyDisplay* display = _displayStack.top();
        assert(display);
        if (!display) break;
        bool canPaginateCols = display ? display->CanPaginateColumns() : false;

        if (CurrencyDisplay::IsPaginateInteractive())
        {
            int key = getchar();
            if (key == EOF)
                break;

            if (key == 0x46 || key == 0x66)           // (f)orward pagination
                display->SetMode(CurrencyDisplay::DISPLAYMODE_FORWARD);

            else if (key == 0x51 || key == 0x71)      // (q)uit
            {
                DeleteChainedDisplays(display);
                display->SetMode(CurrencyDisplay::DISPLAYMODE_QUIT);
            }

            else if (key == 0x42 || key == 0x62)      // (b)ack pagination
                display->SetMode(CurrencyDisplay::DISPLAYMODE_BACK);

            else if (key == 0x45 || key == 0x65)    // (e)xit
            {
                DeleteChainedDisplays(display);
                display->SetMode(CurrencyDisplay::DISPLAYMODE_EXIT);
            }

            else if (key == 27) // ANSI escape sequence for arrow keys
            {
                key = getchar();
                if (key == 91)
                {
                    key = getchar();
                }

                switch (key)
                {
                    case ARROWKEY_UP:
                        display->SetMode(CurrencyDisplay::DISPLAYMODE_UP);
                        break;

                    case ARROWKEY_DOWN:
                        display->SetMode(CurrencyDisplay::DISPLAYMODE_DOWN);
                        break;

                    case ARROWKEY_LEFT:
                        if (canPaginateCols)
                        {
                            display->SetMode(CurrencyDisplay::DISPLAYMODE_LEFT);
                        }
                        else
                        {
                            display->SetMode(CurrencyDisplay::DISPLAYMODE_BACK);
                        }
                        break;

                    case ARROWKEY_RIGHT:
                        if (canPaginateCols)
                        {
                            display->SetMode(CurrencyDisplay::DISPLAYMODE_RIGHT);
                        }
                        else
                        {
                            display->SetMode(CurrencyDisplay::DISPLAYMODE_FORWARD);
                        }
                        break;

                    default:
                        // Treat any other key as quit
                        DeleteChainedDisplays(display);
                        display->SetMode(CurrencyDisplay::DISPLAYMODE_QUIT);
                        break;
                }
            }
            else
            {
                // Treat any other key as quit
                DeleteChainedDisplays(display);
                display->SetMode(CurrencyDisplay::DISPLAYMODE_QUIT);
            }
        }
        else
        {
            display->SetMode(CurrencyDisplay::DISPLAYMODE_FORWARD);
        }
		
		ProcessPagination();

        bool isPaginating = IsPaginating();

        if (!isPaginating) break;    // Done paginating
    }
    tcsetattr(STDIN_FILENO, TCSANOW, &savedt);
#endif
}
//------------------------------------------------------------------------------
// Slot called when interpreter is deleted
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::HandleOnSaveOnExit(int returnCode)
{	
    DisconnectOmlSignalHandler();
    
    CheckInLicense();
    CheckinLibraries();

    // Do not delete the interpreter. If the exit command is in a function, this
    // causes a crash as the function is not complete and the memory scope is still 
    // open. This is not a solution, just a band-aid fix. We should ideally be
    // setting a flag here and stop further execution of commands
    //delete _interp;
    //_interp = NULL;

    exit(returnCode);
}
//------------------------------------------------------------------------------
// Slot which displays a prompt and gets user input
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::HandleOnGetUserInput(const std::string& prompt,
                                                     const std::string& type,
													 std::string&       userInput)
{
    bool waspaginating = false;
    while (1)
    {
        if (IsPaginating())
        {
            waspaginating = true;
            Paginate();
            continue;
        }
        break;  // Nothing pending at this point
    }
    if (waspaginating)
    {
        std::cout << ">>>";
        FlushStdout();
    }
    
    // Split prompt by new lines and just display them
    std::vector<std::string> prompts(SplitInputPrompt(prompt));
    std::string inputprompt(prompt);

    if (!prompts.empty())
    {
        inputprompt = prompts.back();
        prompts.pop_back();
        for (std::vector<std::string>::const_iterator itr = prompts.begin();
            itr != prompts.end(); ++itr)
        {
            std::vector<Currency> inputs;
            inputs.emplace_back(*itr);
            _interp->CallFunction("disp", inputs);
        }
    }

    std::cout << inputprompt;
    FlushStdout();
    std::getline(std::cin, userInput);
}
//------------------------------------------------------------------------------
// Initializes and caches display for the given currency
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::CacheDisplay(const Currency& cur)
{
    if (!_displayStack.empty()) return;

    CurrencyDisplay* display = cur.GetDisplay();
    assert(display);

    assert(_interp);

    display->Initialize(_interp->GetOutputFormat(), _interp);

    _displayStack.push(display);  // This becomes the new top display
}
//------------------------------------------------------------------------------
// Clears display
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::ClearDisplay()
{
    if (_displayStack.empty()) return;
    CurrencyDisplay* display = GetCurrentDisplay();
    if (display)
    {
        CurrencyDisplay::DeleteDisplay(display);
        _displayStack.pop();
    }
    if (_displayStack.empty())
    {
        CurrencyDisplay::ClearLineCount();
    }
}
//------------------------------------------------------------------------------
// Gets current display
//------------------------------------------------------------------------------
CurrencyDisplay* ConsoleInterpreterWrapper::GetCurrentDisplay() const
{
    if (_displayStack.empty()) return 0;

    CurrencyDisplay* display = _displayStack.top();
    assert(display);
    
    return display;
}
//------------------------------------------------------------------------------
// Returns true if pagination is in process
//------------------------------------------------------------------------------
bool ConsoleInterpreterWrapper::IsPaginating()
{
    if (_displayStack.empty()) return false;

    CurrencyDisplay* display = GetCurrentDisplay();
    assert(display);

    if (display->IsPaginatingCols() || display->IsPaginatingRows()) return true;

    return false;
}
//------------------------------------------------------------------------------
// Slot for when a new nested display needs to be added
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::HandleOnAddDisplay(CurrencyDisplay* display)
{
    if (!display) return;

    assert(_interp);

    display->Initialize(_interp->GetOutputFormat(), _interp, GetCurrentDisplay());

    _displayStack.push(display);
}
//------------------------------------------------------------------------------
// Slot called when initiating a user defined pause. Any character typed will
// break out of the pause
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::HandleOnPauseStart(const std::string& msg, 
                                                   bool               wait)
{
    bool waspaginating = false;
    while (1)
    {
        if (IsPaginating())
        {
            waspaginating = true;
            Paginate();
            continue;
        }
        break;  // Nothing pending at this point
    }
    //PrintInfoToOmlWindow(msg); // Printed earlier
    
    if (!wait)
    {
        return;
    }

#ifdef OS_WIN
    // Go in a loop till we either get the pagination controls or a quit
    while(1)
    {        
        DWORD        charToRead = 1;  // Num chars to read
        DWORD        numEvents  = 0;  // Num events read
        INPUT_RECORD input;	          // Input buffer data
   
		// Stop from echoing.
		FlushConsoleInputBuffer(_inhandle);
	    BOOL read = ReadConsoleInput(_inhandle, &input, charToRead, &numEvents);
        if (!read)
		{
			CONSOLEINTERPRETERWRAPPER_PRINT("Error reading console input\n");
			HandleOnClearResults();
			return;
		}

        if (_interp->IsInterrupt()) return;

		if (input.EventType != KEY_EVENT || !input.Event.KeyEvent.bKeyDown)  
			continue; // Not a key event with key down

        break;
    }
#else

	fflush(stdout);
    std::string userInput;
    std::getline(std::cin, userInput);
#endif
}
//------------------------------------------------------------------------------
// Cleans up after pagination like clearing display
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::EndPagination(bool printMsg)
{
    if (_displayStack.empty()) return;

    CurrencyDisplay* display = GetCurrentDisplay();
    if (display)
    {
        if (printMsg)
            PrintPaginationMessage();

        CurrencyDisplay::DeleteDisplay(display);
        _displayStack.pop();
    }
}
//------------------------------------------------------------------------------
// Prints info message (in different color than results) to command window
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::PrintInfoToOmlWindow(const std::string& msg)
{
#ifdef OS_WIN
    // Gray background with black letters
    SetConsoleTextAttribute(_outhandle, BACKGROUND_INTENSITY | 0);

    Print(msg + '\n', true);
    SetConsoleTextAttribute(_outhandle, _defaultWinAttr); // Reset color info
#else
    Print(msg + '\n', true);
#endif

}
//------------------------------------------------------------------------------
// Prints info message
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::PrintToStdout(const std::string& msg)
{
    size_t len = msg.size();
    if (len < g_buffsize)
    {
        Print(msg, false);
        return;
    }

    size_t numprinted = 0;
    while (numprinted < len)
    {
        FlushStdout(); // Flush before printing large output
        if (numprinted + g_buffsize > len)
        {
            Print(msg.substr(numprinted), false);
            break;
        }
        else
        {

            Print(msg.substr(numprinted, g_buffsize), false);
            numprinted += g_buffsize;
        }
    }
    FlushStdout();
}
//------------------------------------------------------------------------------
// Prints new prompt, resets append flags
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::PrintNewPrompt()
{
    std::cout << std::flush;

    std::string msg;
    if (_appendOutput)
    {
        msg = '\n';    // Add newline, in case there was a printf
        _appendOutput = false;     // Reset printf output
    }
    else if (_addnewline)
    {
        msg = '\n';    // Printf with newline
        _addnewline = false;
    }
    BuiltInFuncsUtils::SaveToOutputLog(msg);
    msg += ">>>";
    std::cout << msg << std::flush;        // New prompt
    
}
//------------------------------------------------------------------------------
// Gets the number of input arguments
//------------------------------------------------------------------------------
int ConsoleInterpreterWrapper::GetNumberOfInputArgs() const
{
    return (_inputArgs.empty()) ? 0 : static_cast<int>(_inputArgs.size());
}
//------------------------------------------------------------------------------
// Gets the input argument at the given 0-based index
//------------------------------------------------------------------------------
std::string ConsoleInterpreterWrapper::GetInputArg(int idx) const
{
    if (idx < 0 || _inputArgs.empty() || idx >= static_cast<int>(_inputArgs.size()))
        return "";

    return _inputArgs[idx];
}
//------------------------------------------------------------------------------
// Gets argv at the given index
//------------------------------------------------------------------------------
std::string ConsoleInterpreterWrapper::GetArgv(int idx) const
{
    if (idx < 0 || _argv.empty() || idx >= static_cast<int>(_argv.size()))
        return "";

    return _argv[idx];
}
//------------------------------------------------------------------------------
// Gets argc
//------------------------------------------------------------------------------
int ConsoleInterpreterWrapper::GetArgc() const
{
    return (_argv.empty()) ? 0 : static_cast<int>(_argv.size());
}
//------------------------------------------------------------------------------
// Deletes chained displays, if any
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::DeleteChainedDisplays(CurrencyDisplay* display)
{
    if (!display || _resultsToPrint.empty())
    {
        return;
    }

    for (std::vector<Currency>::iterator itr = _resultsToPrint.begin();
         itr != _resultsToPrint.end();)
    {
        Currency cur = *itr;
        CurrencyDisplay* curdisp = cur.GetDisplay();
        if (display->IsChainedDisplay(curdisp))
        {
            CurrencyDisplay::DeleteDisplay(curdisp);
            cur.SetDisplay(nullptr);
            itr = _resultsToPrint.erase(itr);
        }
        else
        {
            ++itr;
        }
    }
}
//------------------------------------------------------------------------------------------------------------
// Authorize Console application
//------------------------------------------------------------------------------------------------------------
bool ConsoleInterpreterWrapper::Authorize(const std::string& user, const std::string& pwd, std::string& errMsg)
{
    if(! _licHandle) 
    {
        errMsg = "ALTAIR LICENSE ERROR";
        return false;
    }
    void* symbol = BuiltInFuncsCore::DyGetFunction(_licHandle, "Authorize");
    if (!symbol)
    {
        errMsg = "ALTAIR LICENSE ERROR";
        return false;
    }

    bool credentialsGiven = !(user.empty() || pwd.empty());

    lic_auth_fptr licfptr = (lic_auth_fptr)(symbol);
    int retValue = licfptr(user, pwd, 0);
    if(retValue == 2)
    {
        // If user and pwd was not supplied. Ask for it and then Authorize again
        if(! credentialsGiven) 
        {
            std::string usr;
            std::cout<<"Enter user id : ";
            FlushStdout();
            std::cin>>usr;
            

            std::string pw(GetPassword());
            std::cout << "Authorization in Progress. Please Wait...." << std::endl;
            retValue = licfptr(usr, pw, 0);
        }
    }

    bool result = (retValue == 0); //success
    if (result)
    {
        OmlSetProductVersionProc funcptr = 
            (OmlSetProductVersionProc)BuiltInFuncsCore::DyGetFunction(
                _licHandle, "OmlSetProductVersion");
        if (funcptr)
        {
            funcptr(OmlVersion);
        }
    }
    return result;
}
//------------------------------------------------------------------------------
// check if professional edition
//------------------------------------------------------------------------------
bool ConsoleInterpreterWrapper::IsProfessionalEdition()
{
    if(! _licHandle) return false;

    void* symbol = BuiltInFuncsCore::DyGetFunction(_licHandle, "IsProEdition");
    if (!symbol) return false;

    lic_ispro_fptr fptr = (lic_ispro_fptr)(symbol);
    return fptr();
}
//------------------------------------------------------------------------------
// Check in product license
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::CheckInLicense()
{
    if(_licHandle)
    {
        void* symbol = BuiltInFuncsCore::DyGetFunction(_licHandle, "CheckInLic");
        if(symbol)
        {
            lic_checkin_fptr fptr = (lic_checkin_fptr)(symbol);
            fptr();
        }
    }
}

bool ConsoleInterpreterWrapper::CheckLicStatus()
{
	bool status = false;
	if (_licHandle)
	{
		void* symbol = BuiltInFuncsCore::DyGetFunction(_licHandle, "CheckLicStatus");
		if (symbol)
		{
			lic_status_fptr fptr = (lic_status_fptr)(symbol);
			status = fptr();
		}
	}
	return status;
}

std::string ConsoleInterpreterWrapper::EditionString(bool appendEdition)
{
	std::string edition;
	if (_licHandle)
	{
		void* symbol = BuiltInFuncsCore::DyGetFunction(_licHandle, "EditionString");
		if (symbol)
		{
			lic_edition_fptr fptr = (lic_edition_fptr)(symbol);
			fptr(edition, appendEdition);
		}
	}
	return edition;
}
//------------------------------------------------------------------------------
// Returns true after checking in oml libary license license 
//------------------------------------------------------------------------------
bool ConsoleInterpreterWrapper::CheckoutLibrary(const std::string& libinfo,
                                                std::string&       out,
                                                std::string&       err)
{
    bool result = false;
    if (_licHandle)
    {
        void* symbol = BuiltInFuncsCore::DyGetFunction(_licHandle, 
                       "OmlCheckoutLibrary");
        assert(symbol);
        if (symbol)
        {
            OmlCheckoutLibraryProc fptr = (OmlCheckoutLibraryProc)(symbol);
            result = fptr(libinfo, out, err);
        }
    }
    return result;
}
//------------------------------------------------------------------------------
// Helper method to check in oml library license
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::CheckinLibrary(const std::string& in)
{
    if (!_licHandle)
    {
        return;
    }
    void* symbol = BuiltInFuncsCore::DyGetFunction(_licHandle,
        "OmlCheckinLibrary");
    assert(symbol);

    if (symbol)
    {
        OmlCheckinLibraryProc fptr = (OmlCheckinLibraryProc)(symbol);
        fptr(in);
    }
}
//------------------------------------------------------------------------------
// Checks in all library licenses
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::CheckinLibraries()
{
    if (!_licHandle)
    {
        return;
    }
    void* symbol = BuiltInFuncsCore::DyGetFunction(_licHandle,
        "OmlCheckinLibraries");
    assert(symbol);

    if (symbol)
    {
        OmlFuncPtrVoid fptr = (OmlFuncPtrVoid)(symbol);
        fptr();
    }
}
//------------------------------------------------------------------------------
// Helper method to test oml library license
//------------------------------------------------------------------------------
std::string ConsoleInterpreterWrapper::TestLibrary(const std::string& in)
{
    if (!_licHandle)
    {
        return "";
    }
    void* symbol = BuiltInFuncsCore::DyGetFunction(_licHandle,
        "OmlTestLibrary");
    assert(symbol);

    if (symbol)
    {
        OmlTestLibraryProc fptr = (OmlTestLibraryProc)(symbol);
        std::string result;
        fptr(in, result);
        return result;
    }
    return "";
}
//------------------------------------------------------------------------------
// Checks library licenses
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::CheckLibraries()
{
    if (!_licHandle)
    {
        return;
    }

    void* symbol = BuiltInFuncsCore::DyGetFunction(_licHandle,
        "OmlCheckLibraryStatus");
    assert(symbol);

    if (symbol)
    {
        OmlFuncPtrVoid fptr = (OmlFuncPtrVoid)(symbol);
        fptr();
    }
}
//------------------------------------------------------------------------------
// Initializes command window screen buffer for interactive mode
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::InitCommandWindowInfo()
{
#ifdef OS_WIN
    _inhandle  = GetStdHandle(STD_INPUT_HANDLE); 
    _outhandle = GetStdHandle(STD_OUTPUT_HANDLE);

    CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
    GetConsoleScreenBufferInfo(_outhandle, &csbiInfo);
    _defaultWinAttr = csbiInfo.wAttributes; // Cache color info

#endif
}
//------------------------------------------------------------------------------
// Prints to stdout
//------------------------------------------------------------------------------
void ConsoleInterpreterWrapper::Print(const std::string& msg, 
                                      bool               forceFlush)
{
    if (!_childHandler)  // Main interp, print to stdout
    {
        std::cout << msg;
        if (forceFlush)
        {
            FlushStdout();
        }
        BuiltInFuncsUtils::SaveToOutputLog(msg);
    }
}
//------------------------------------------------------------------------------
// Helper method to get password and obfuscate the characters
//------------------------------------------------------------------------------
std::string ConsoleInterpreterWrapper::GetPassword()
{
    std::string pw;

    std::cout << "Enter password : ";
    FlushStdout();

#ifdef OS_WIN
    while (1)
    {
        char ch = _getch();
        if (ch == 0x0D)
        {
            break;
        }
        std::cout << '*';
        FlushStdout();
        pw += ch;
    }

#else
    struct termios cachedTerm;
    tcgetattr(STDIN_FILENO, &cachedTerm);
    struct termios tmp = cachedTerm;
    tmp.c_lflag &= ~ECHO;  // Disable echo
    tcsetattr(STDIN_FILENO, TCSANOW, &tmp);
    std::cin >> pw;
    fflush(stdin);
    tcsetattr(STDIN_FILENO, TCSANOW, &cachedTerm);
    std::string dummy(pw.length(), '*');
    std::cout << dummy;
#endif

    std::cout << std::endl;
    return pw;
}
//------------------------------------------------------------------------------
// Splits input prompt by new lines and returns the tokens
//------------------------------------------------------------------------------
std::vector<std::string> ConsoleInterpreterWrapper::SplitInputPrompt(const std::string& in) const
{
    if (in.empty())
    {
        return std::vector<std::string>();
    }

    std::vector<std::string> tokens;
    std::string prompt = in;

    size_t len = std::string("\\n").length();
    while (!prompt.empty())
    {
        size_t pos = prompt.find("\\n");
        if (pos == std::string::npos)
        {
            if (!prompt.empty())
            {
                tokens.emplace_back(prompt);
            }
            break;
        }
        tokens.emplace_back(prompt.substr(0, pos));
        tokens.emplace_back("");
        prompt = prompt.substr(pos + len);
    }
    return tokens;
}
