#include "OMLInterfacePublic.h"

class OMLInterface;

extern "C" 
{
    __declspec(dllexport) int InitToolbox(OMLInterface* eval);
}

bool CreateApplication(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
bool DeleteApplication(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);

bool AddResource(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
bool RemoveResource(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
bool GetResourceList(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);

bool CreateModel(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
bool DeleteModel(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);

bool CreateDerivedValue(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
bool RemoveDerivedValue(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);

bool GetNumParts(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
bool GetNumElements(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
bool GetNumNodes(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);

bool GetParts(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
bool GetElements(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);
bool GetNodes(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);

bool GetValues(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs);


#include <boost/shared_ptr.hpp>
#include <map>

#include <rData/Tools/Bridge.h>


using namespace rData;
using namespace rData::Tools;


boost::shared_ptr<Bridge::Factory> _factory;

std::unordered_map<std::string, boost::shared_ptr<Bridge::Application> > _apps;
std::unordered_map<std::string, boost::shared_ptr<Bridge::Model> > _models;


int InitToolbox(OMLInterface* eval)
{
    eval->RegisterFunction("createapplication", CreateApplication);
    eval->RegisterFunction("deleteapplication", DeleteApplication);
    
    eval->RegisterFunction("addresource", AddResource);
    eval->RegisterFunction("removeresource", RemoveResource);
    eval->RegisterFunction("getresourcelist", GetResourceList);

    eval->RegisterFunction("createmodel", CreateModel);
    eval->RegisterFunction("deletemodel", DeleteModel);
    
    eval->RegisterFunction("createderivedvalue", CreateDerivedValue);
    eval->RegisterFunction("removederivedvalue", RemoveDerivedValue);

    eval->RegisterFunction("getnumparts", GetNumParts);
    eval->RegisterFunction("getnumelements", GetNumElements);
    eval->RegisterFunction("getnumnodes", GetNumNodes);

    eval->RegisterFunction("getparts", GetParts);
    eval->RegisterFunction("getelements", GetElements);
    eval->RegisterFunction("getnodes", GetNodes);

    eval->RegisterFunction("getvalues", GetValues);
    
    _factory.reset(new Bridge::Factory());

    return 0;
}

bool CreateApplication(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string appname = inputs->Get(0)->GetString();
    std::string console = "false";
    if (inputs->Get(1))
        console = inputs->Get(1)->GetString();
    
    AttributeListBuilder args;
    if (console == "true")
        args.Add("~argv", "--console");

    boost::shared_ptr<Bridge::Application> app(new Bridge::Application(_factory->CreateApplication(appname.c_str(), args.GetAttributes())));
    std::string app_id = std::to_string((uint64_t)app->CoreObject()->GetApplication()->GetID());
    _apps[app_id] = app;


    outputs->AddString(app_id.c_str());

    return true;
}

bool DeleteApplication(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id = inputs->Get(0)->GetString();

    _apps.erase(app_id);

    return true;
}

bool AddResource(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id      = inputs->Get(0)->GetString();
    std::string res_label   = inputs->Get(1)->GetString();
    std::string path        = inputs->Get(2)->GetString();

    _apps[app_id]->AddResource(res_label.c_str(), AttributeListBuilder().Add("path", path.c_str()).GetAttributes());

    outputs->AddString(res_label.c_str());

    return true;
}

bool RemoveResource(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id      = inputs->Get(0)->GetString();
    std::string res_label   = inputs->Get(1)->GetString();

    _apps[app_id]->RemoveResource(res_label.c_str());

    outputs->AddString("ok");

    return true;
}

bool GetResourceList(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id = inputs->Get(0)->GetString();

    Bridge::ResourceListPtr r_list = _apps[app_id]->GetResourceList();
    
    std::string r_str("[ ");

    for (r_list->First(); r_list->Valid(); r_list->Next())
    {
        r_str.append(r_list->Key());
        r_str.append(" ");
    }
    r_str.append("]");

    outputs->AddString(r_str.c_str());

    return true;
}

bool CreateModel(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id          = inputs->Get(0)->GetString();
    std::string model_label     = inputs->Get(1)->GetString();
    std::string res_label       = inputs->Get(2)->GetString();
    std::string channel         = inputs->Get(3)->GetString();
    const OMLCellArray * args   = inputs->Get(4) ? inputs->Get(4)->GetCellArray() : nullptr;

    std::string model_id = model_label + app_id;

    AttributeListBuilder a;
    a.Add("resource", res_label.c_str());
    a.Add("channel", channel.c_str());
    a.Add("name", channel.c_str());
    a.Add("label", model_label.c_str());
    a.Add("step", 0);
    a.Add("subcase", 0);
    a.Add("database", "main");
    a.Add("lod", 0);

    if (args)
    {
        const int rows = args->GetRows();
        for (int r = 0; r < rows; ++r)
        {
            const std::string key = args->GetValue(r, 0)->GetString();
            const std::string val = args->GetValue(r, 1)->GetString();
            a.Add(key.c_str(), val.c_str());
        }
    }

    _models[model_id].reset(new Bridge::Model(_apps[app_id]->CreateModel(channel.c_str(), a.GetAttributes())));

    outputs->AddString(model_label.c_str());

    return true;
}

bool DeleteModel(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id = inputs->Get(0)->GetString();
    std::string model_label = inputs->Get(1)->GetString();

    std::string model_id = model_label + app_id;

    _models[model_id]->Dispose();
    _models.erase(model_id);

    outputs->AddString("ok");

    return true;
}

bool CreateDerivedValue(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id = inputs->Get(0)->GetString();
    std::string dv_label = inputs->Get(1)->GetString();
    const OMLCellArray * args = inputs->Get(2) ? inputs->Get(2)->GetCellArray() : nullptr;
    
    AttributeListBuilder a;
    if (args)
    {
        const int rows = args->GetRows();
        for (int r = 0; r < rows; ++r)
        {
            const std::string key = args->GetValue(r, 0)->GetString();
            const std::string val = args->GetValue(r, 1)->GetString();
            a.Add(key.c_str(), val.c_str());
        }
    }

    _apps[app_id]->CreateDerivedValue(dv_label.c_str(), a.GetAttributes());

    outputs->AddString(dv_label.c_str());

    return true;
}

bool RemoveDerivedValue(OMLInterface*, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id = inputs->Get(0)->GetString();
    std::string dv_label = inputs->Get(1)->GetString();

    _apps[app_id]->RemoveDerivedValue(dv_label.c_str());

    return true;
}

bool GetNumParts(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id      = inputs->Get(0)->GetString();
    std::string model_label = inputs->Get(1)->GetString();

    std::string model_id = model_label + app_id;

    if (_models.find(model_id) == _models.end())
    {
        outputs->AddString("Error: model doesn't exists");
        return false;
    }

    size_t num = _models[model_id]->GetMesh().GetNumParts();
    outputs->AddString(std::to_string(num).c_str());

    return true;
}

bool GetNumElements(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id      = inputs->Get(0)->GetString();
    std::string model_label = inputs->Get(1)->GetString();

    std::string model_id = model_label + app_id;

    if (_models.find(model_id) == _models.end())
    {
        outputs->AddString("Error: model doesn't exists");
        return false;
    }

    size_t num = _models[model_id]->GetMesh().GetNumElems();
    outputs->AddString(std::to_string(num).c_str());

    return true;
}

bool GetNumNodes(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id      = inputs->Get(0)->GetString();
    std::string model_label = inputs->Get(1)->GetString();

    std::string model_id = model_label + app_id;

    if (_models.find(model_id) == _models.end())
    {
        outputs->AddString("Error: model doesn't exists");
        return false;
    }

    size_t num = _models[model_id]->GetMesh().GetNumNodes();
    outputs->AddString(std::to_string(num).c_str());

    return true;
}

bool GetParts(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id = inputs->Get(0)->GetString();
    std::string model_label = inputs->Get(1)->GetString();

    std::string model_id = model_label + app_id;

    if (_models.find(model_id) == _models.end())
    {
        outputs->AddString("Error: model doesn't exists");
        return false;
    }

    Bridge::Mesh mesh = _models[model_id]->GetMesh();
    size_t part_num = mesh.GetNumParts();
    OMLCellArray * parts = outputs->CreateCellArray((int)part_num, 2);
    for (size_t idx = 0; idx < part_num; ++idx)
    {
        Bridge::Mesh::Part p = mesh.GetPart(idx);
        parts->SetValue((int)idx, 0, outputs->CreateCurrencyFromString(std::to_string((uint64_t)(p.GetID())).c_str()));
        parts->SetValue((int)idx, 1, outputs->CreateCurrencyFromString(p.GetLabel()));
    }

    outputs->AddCellArray(parts);

    return true;
}

bool GetElements(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id = inputs->Get(0)->GetString();
    std::string model_label = inputs->Get(1)->GetString();

    std::string model_id = model_label + app_id;

    if (_models.find(model_id) == _models.end())
    {
        outputs->AddString("Error: model doesn't exists");
        return false;
    }

    Bridge::Mesh mesh = _models[model_id]->GetMesh();
    size_t elem_num = mesh.GetNumElems();
    OMLCellArray * elems = outputs->CreateCellArray((int)elem_num, 4);
    for (size_t idx = 0; idx < elem_num; ++idx)
    {
        Bridge::Mesh::Elem e = mesh.GetElem(idx);
        // element ID
        elems->SetValue((int)idx, 0, outputs->CreateCurrencyFromString(std::to_string((uint64_t)(e.GetID())).c_str()));
        
        // element Configuration
        elems->SetValue((int)idx, 1, outputs->CreateCurrencyFromString(std::to_string(e.GetConfig()).c_str()));
        
        // element Connectivity
        IArray<uint32_t> &nodes = e.GetNodes();
        size_t nodes_num = nodes.size();
        OMLCellArray * connectivity = outputs->CreateCellArray((int)nodes_num, 1);
        for (size_t n = 0; n < nodes_num; ++n)
        {
            connectivity->SetValue((int)n, outputs->CreateCurrencyFromString(std::to_string(nodes[n]).c_str()));
        }
        elems->SetValue((int)idx, 2, connectivity->GetCurrency());

        // element's Part Index
        elems->SetValue((int)idx, 3, outputs->CreateCurrencyFromString(std::to_string(e.GetPart()).c_str()));
    }
    
    outputs->AddCellArray(elems);

    return true;
}

bool GetNodes(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id = inputs->Get(0)->GetString();
    std::string model_label = inputs->Get(1)->GetString();

    std::string model_id = model_label + app_id;

    if (_models.find(model_id) == _models.end())
    {
        outputs->AddString("Error: model doesn't exists");
        return false;
    }

    Bridge::Mesh mesh = _models[model_id]->GetMesh();
    size_t nodes_num = mesh.GetNumNodes();
    OMLCellArray * nodes = outputs->CreateCellArray((int)nodes_num, 1);
    for (size_t idx = 0; idx < nodes_num; ++idx)
    {
        Bridge::Mesh::Node n = mesh.GetNode(idx);
        nodes->SetValue((int)idx, 0, outputs->CreateCurrencyFromString(std::to_string((uint64_t)(n.GetID())).c_str()));
    }

    outputs->AddCellArray(nodes);

    return true;
}

bool GetValues(OMLInterface* eval, const OMLCurrencyList* inputs, OMLCurrencyList* outputs)
{
    std::string app_id      = inputs->Get(0)->GetString();
    std::string model_label = inputs->Get(1)->GetString();
    std::string value_str   = inputs->Get(2)->GetString();
    std::string subcase_str = inputs->Get(3)->GetString();
    std::string step_str    = inputs->Get(4)->GetString();

    std::string model_id = model_label + app_id;
    if (_models.find(model_id) == _models.end())
    {
        outputs->AddString("Error: model doesn't exists");
        return false;
    }
    
    Bridge::Values vals = _models[model_id]->GetValues(value_str.c_str(), AttributeListBuilder().
                                                             Add("subcase", subcase_str.c_str()).
                                                             Add("step", step_str.c_str()).
                                                             GetAttributes());

    Bridge::Mesh mesh = _models[model_id]->GetMesh();

    const size_t rec_num = vals.GetNumRecords();
    OMLCellArray * table = outputs->CreateCellArray((int)rec_num, 2);
    for (size_t r = 0; r < rec_num; ++r)
    {
        Bridge::Mesh::Node n = mesh.GetNode(r);
        table->SetValue((int)r, 0, outputs->CreateCurrencyFromString(std::to_string((uint64_t)(n.GetID())).c_str()));

        auto rec = vals.GetRecord(r);
        const float dummy_data[] = { 0.0f, 0.0f, 0.0f };
        const float * data = rec ? rec.GetData() : dummy_data;
        const uint32_t item_num = rec ? rec.GetNumItems() : 1;
        const uint32_t stride_num = rec ? rec.GetStride() : 3;
        OMLCellArray * row = outputs->CreateCellArray((int)item_num, 1);
        for (uint32_t it = 0; it < item_num; ++it)
        {
            OMLCellArray * stride = outputs->CreateCellArray((int)stride_num, 1);
            for (uint32_t s = 0; s < stride_num; ++s)
            {
                stride->SetValue((int)s, outputs->CreateCurrencyFromDouble((double)(*data)));
                ++data;
            }
            row->SetValue((int)it, stride->GetCurrency());
        }
        table->SetValue((int)r, 1, row->GetCurrency());
    }
    outputs->AddCellArray(table);
    
    vals.Dispose();

    return true;
}

