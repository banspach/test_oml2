/**
* @file slicotUtilsFunctions.h
* @date December 2013
* Copyright (C) 2007-2019 Altair Engineering, Inc.
* This file is part of the OpenMatrix Language ("OpenMatrix") software.
* Open Source License Information:
* OpenMatrix is free software. You can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* OpenMatrix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Commercial License Information:
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/
#ifndef __SLICOTUTILSFUNCTIONS_H__
#define __SLICOTUTILSFUNCTIONS_H__

#include "slicotExports.h"

// forward declarations
class hwString;
class hwMathStatus;
template <typename T> class hwTComplex;
typedef hwTComplex<double> hwComplex;
template <typename T1, typename T2> class hwTMatrix;
typedef hwTMatrix<double, hwTComplex<double> > hwMatrix;

SLICOT_DECLS hwMathStatus slicot_sb01bd( const hwString &DICO, const hwMatrix &ALPHA, hwMatrix &A, const hwMatrix &B, hwMatrix &WR, hwMatrix &WI, hwMatrix &NFP, hwMatrix &NAP, hwMatrix &NUP, hwMatrix &F, hwMatrix &Z, const hwMatrix &TOL, hwMatrix &IWARN, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &WR_out, hwMatrix &WI_out );
SLICOT_DECLS hwMathStatus slicot_ab01od( const hwString &STAGES, const hwString &JOBU, const hwString &JOBV, hwMatrix &A, hwMatrix &B, hwMatrix &U, hwMatrix &V, hwMatrix &NCONT, hwMatrix &INDCON, hwMatrix &KSTAIR, const hwMatrix &TOL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &U_out, hwMatrix &NCONT_out, hwMatrix &INDCON_out, hwMatrix &KSTAIR_out );
SLICOT_DECLS hwMathStatus slicot_sb02md( const hwString &DICO, const hwString &HINV, const hwString &UPLO, const hwString &SCAL, const hwString &SORT, const hwMatrix &N, hwMatrix &A, const hwMatrix &G, hwMatrix &Q, hwMatrix &U, hwMatrix &INFO, hwMatrix &RCOND, hwMatrix &WR, hwMatrix &WI, hwMatrix &S, hwMatrix &A_out, hwMatrix &Q_out );
SLICOT_DECLS hwMathStatus slicot_mc01td( const hwString &DICO, hwMatrix &DP, const hwMatrix &P, hwMatrix &STABLE, hwMatrix &NZ, hwMatrix &IWARN, hwMatrix &INFO );
SLICOT_DECLS hwMathStatus slicot_mb02md( const hwString &JOB, const hwMatrix &M, const hwMatrix &N, const hwMatrix &L, hwMatrix &RANK, hwMatrix &C, hwMatrix &S, hwMatrix &X, const hwMatrix &TOL, hwMatrix &IWARN, hwMatrix &INFO, hwMatrix &RANK_out, hwMatrix &C_out );
SLICOT_DECLS hwMathStatus slicot_mb02nd( const hwMatrix &M, const hwMatrix &N, const hwMatrix &L, hwMatrix &RANK, hwMatrix &THETA, hwMatrix &C, hwMatrix &X, hwMatrix &Q, hwMatrix &INUL, const hwMatrix &TOL, const hwMatrix &RELTOL, hwMatrix &IWARN, hwMatrix &INFO, hwMatrix &RANK_out, hwMatrix &THETA_out, hwMatrix &C_out );
SLICOT_DECLS hwMathStatus slicot_mb02qd( const hwString &JOB, const hwString &INIPER, const hwMatrix &NRHS, const hwMatrix &RCOND, const hwMatrix &SVLMAX, hwMatrix &A, hwMatrix &B, const hwMatrix &Y, hwMatrix &JPVT, hwMatrix &RANK, hwMatrix &SVAL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &JPVT_out );
SLICOT_DECLS hwMathStatus slicot_mb03od( const hwString &JOBQR, hwMatrix &A, hwMatrix &JPVT, const hwMatrix &RCOND, const hwMatrix &SVLMAX, hwMatrix &TAU, hwMatrix &RANK, hwMatrix &SVAL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &JPVT_out );
SLICOT_DECLS hwMathStatus slicot_mb03pd( const hwString &JOBRQ, hwMatrix &A, hwMatrix &JPVT, const hwMatrix &RCOND, const hwMatrix &SVLMAX, hwMatrix &TAU, hwMatrix &RANK, hwMatrix &SVAL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &JPVT_out );
SLICOT_DECLS hwMathStatus slicot_mb04gd( hwMatrix &A, hwMatrix &JPVT, hwMatrix &TAU, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &JPVT_out );
SLICOT_DECLS hwMathStatus slicot_mb04md( hwMatrix &MAXRED, hwMatrix &A, hwMatrix &SCALE, hwMatrix &INFO, hwMatrix &MAXRED_out, hwMatrix &A_out );
SLICOT_DECLS hwMathStatus slicot_ab08nd( const hwString &EQUIL, const hwMatrix &N, const hwMatrix &M, const hwMatrix &P, const hwMatrix &A, const hwMatrix &B, const hwMatrix &C, const hwMatrix &D, hwMatrix &NU, hwMatrix &RANK, hwMatrix &DINFZ, hwMatrix &NKROR, hwMatrix &NKROL, hwMatrix &INFZ, hwMatrix &KRONR, hwMatrix &KRONL, hwMatrix &AF, hwMatrix &BF, const hwMatrix &TOL, hwMatrix &INFO );
SLICOT_DECLS hwMathStatus slicot_ab07nd( hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &D, hwMatrix &RCOND, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out, hwMatrix &D_out );
SLICOT_DECLS hwMathStatus slicot_ag07bd( const hwString &JOBE, const hwMatrix &A, const hwMatrix &E, const hwMatrix &B, const hwMatrix &C, const hwMatrix &D, hwMatrix &AI, hwMatrix &EI, hwMatrix &BI, hwMatrix &CI, hwMatrix &DI, hwMatrix &INFO );
SLICOT_DECLS hwMathStatus slicot_ag08bd( const hwString &EQUIL, const hwMatrix &M, const hwMatrix &P, hwMatrix &A, hwMatrix &E, const hwMatrix &B, const hwMatrix &C, const hwMatrix &D, hwMatrix &NFZ, hwMatrix &NRANK, hwMatrix &NIZ, hwMatrix &DINFZ, hwMatrix &NKROR, hwMatrix &NINFE, hwMatrix &NKROL, hwMatrix &INFZ, hwMatrix &KRONR, hwMatrix &INFE, hwMatrix &KRONL, const hwMatrix &TOL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &E_out );
SLICOT_DECLS hwMathStatus slicot_sb03md( const hwString &DICO, const hwString &JOB, const hwString &FACT, const hwString &TRANA, hwMatrix &A, hwMatrix &U, hwMatrix &C, hwMatrix &SCALE, hwMatrix &SEP, hwMatrix &FERR, hwMatrix &WR, hwMatrix &WI, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &U_out, hwMatrix &C_out );
SLICOT_DECLS hwMathStatus slicot_sg03ad( const hwString &DICO, const hwString &JOB, const hwString &FACT, const hwString &TRANS, const hwString &UPLO, hwMatrix &A, hwMatrix &E, hwMatrix &Q, hwMatrix &Z, hwMatrix &X, hwMatrix &SCALE, hwMatrix &SEP, hwMatrix &FERR, hwMatrix &ALPHAR, hwMatrix &ALPHAI, hwMatrix &BETA, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &E_out, hwMatrix &Q_out, hwMatrix &Z_out, hwMatrix &X_out );
SLICOT_DECLS hwMathStatus slicot_sb04qd( hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &Z, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out );
SLICOT_DECLS hwMathStatus slicot_sb04md( hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &Z, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out );
SLICOT_DECLS hwMathStatus slicot_tb01ud( const hwString &JOBZ, hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &NCONT, hwMatrix &INDCON, hwMatrix &NBLK, hwMatrix &Z, hwMatrix &TAU, const hwMatrix &TOL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out );
SLICOT_DECLS hwMathStatus slicot_sb03od( const hwString &DICO, const hwString &FACT, const hwString &TRANS, hwMatrix &A, hwMatrix &Q, hwMatrix &B, hwMatrix &SCALE, hwMatrix &WR, hwMatrix &WI, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &Q_out, hwMatrix &B_out );
SLICOT_DECLS hwMathStatus slicot_sg03bd( const hwString &DICO, const hwString &FACT, const hwString &TRANS, hwMatrix &A, hwMatrix &E, hwMatrix &Q, hwMatrix &Z, hwMatrix &B, hwMatrix &SCALE, hwMatrix &ALPHAR, hwMatrix &ALPHAI, hwMatrix &BETA, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &E_out, hwMatrix &Q_out, hwMatrix &Z_out, hwMatrix &B_out );
SLICOT_DECLS hwMathStatus slicot_mb05od( const hwString &BALANC, const hwMatrix &NDIAG, const hwMatrix &DELTA, hwMatrix &A, hwMatrix &MDIG, hwMatrix &IDIG, hwMatrix &IWARN, hwMatrix &INFO, hwMatrix &A_out );
SLICOT_DECLS hwMathStatus slicot_ab04md( const hwString &TYPE, const hwMatrix &ALPHA, const hwMatrix &BETA, hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &D, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out, hwMatrix &D_out );
SLICOT_DECLS hwMathStatus slicot_sb10jd( hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &D, hwMatrix &E, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out, hwMatrix &D_out );
SLICOT_DECLS hwMathStatus slicot_sg02ad( const hwString &DICO, const hwString &JOBB, const hwString &FACT, const hwString &UPLO, const hwString &JOBL, const hwString &SCAL, const hwString &SORT, const hwString &ACC, const hwMatrix &P, const hwMatrix &A, const hwMatrix &E, const hwMatrix &B, const hwMatrix &Q, const hwMatrix &R, const hwMatrix &L, hwMatrix &RCONDU, hwMatrix &X, hwMatrix &ALFAR, hwMatrix &ALFAI, hwMatrix &BETA, hwMatrix &S, hwMatrix &T, hwMatrix &U, const hwMatrix &TOL, hwMatrix &IWARN, hwMatrix &INFO );
SLICOT_DECLS hwMathStatus slicot_sb02od( const hwString &DICO, const hwString &JOBB, const hwString &FACT, const hwString &UPLO, const hwString &JOBL, const hwString &SORT, const hwMatrix &P, const hwMatrix &A, const hwMatrix &B, const hwMatrix &Q, const hwMatrix &R, const hwMatrix &L, hwMatrix &RCONDU, hwMatrix &X, hwMatrix &ALFAR, hwMatrix &ALFAI, hwMatrix &BETA, hwMatrix &S, hwMatrix &T, hwMatrix &U, const hwMatrix &TOL, hwMatrix &INFO );
SLICOT_DECLS hwMathStatus slicot_ab13ad( const hwString &DICO, const hwString &EQUIL, const hwMatrix &ALPHA, hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &NS, hwMatrix &HSV, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out );
SLICOT_DECLS hwMathStatus slicot_mb03rd( const hwString &JOBX, const hwString &SORT, const hwMatrix &PMAX, hwMatrix &A, hwMatrix &X, hwMatrix &NBLCKS, hwMatrix &BLSIZE, hwMatrix &WR, hwMatrix &WI, const hwMatrix &TOL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &X_out );
SLICOT_DECLS hwMathStatus slicot_ab05pd( const hwString &OVER, const hwMatrix &ALPHA, const hwMatrix &A1, const hwMatrix &B1, const hwMatrix &C1, const hwMatrix &D1, const hwMatrix &A2, const hwMatrix &B2, const hwMatrix &C2, const hwMatrix &D2, hwMatrix &N, hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &D, hwMatrix &INFO );
SLICOT_DECLS hwMathStatus slicot_tb01id( const hwString &JOB, hwMatrix &MAXRED, hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &SCALE, hwMatrix &INFO, hwMatrix &MAXRED_out, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out );
SLICOT_DECLS hwMathStatus slicot_tg01ad( const hwString &JOB, const hwMatrix &THRESH, hwMatrix &A, hwMatrix &E, hwMatrix &B, hwMatrix &C, hwMatrix &LSCALE, hwMatrix &RSCALE, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &E_out, hwMatrix &B_out, hwMatrix &C_out );
SLICOT_DECLS hwMathStatus slicot_tb05ad( hwMatrix& A, hwMatrix& B, hwMatrix& C, hwMatrix& EVRE, hwMatrix& EVIM, int& ldwork );
SLICOT_DECLS hwMathStatus slicot_tb05ad( const hwMatrix& FREQ, const hwMatrix& AB, const hwMatrix& BB, const hwMatrix& CB, double& RCOND, hwMatrix& G, int ldwork);

#endif /* __SLICOTUTILSFUNCTIONS_H__ */
