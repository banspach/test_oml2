/**
* @file ControlUtilsFunctions.cxx
* @date December 2013
* Copyright (C) 2007-2018 Altair Engineering, Inc.
* This file is part of the OpenMatrix Language ("OpenMatrix") software.
* Open Source License Information:
* OpenMatrix is free software. You can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* OpenMatrix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Commercial License Information:
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/
#include <stdlib.h>
#include "controlUtilsFunctions.h"
#include "hwMatrix.h"
#include <utl/hwString.h>

extern "C"
{
    #undef doublecomplex
    typedef struct { double r, i; } doublecomplex;

    extern double ab13cd_(int *N, int *M, int *NP, double *A, int *LDA, double *B, int *LDB, double *C,
        int *LDC, double *D, int *LDD, double *TOL, int *IWORK, double *DWORK,
        int *LDWORK, doublecomplex *CWORK, int *LCWORK, int *BWORK, int *INFO);

    extern double ab13bd_(char *DICO, char *JOBN,
                          int *N, int *M, int *P,
                          double *A, int *LDA, 
                          double *B, int *LDB,
                          double *C, int *LDC,
                          double *D, int *LDD,
                          int *NQ, double *TOL,
                          double *DWORK, int *LDWORK,
                          int *IWARN, int *INFO);

    extern int td04ad_(char* ROWCOL,
                       int* M, int* P,
                       int* INDEX,
                       double* DCOEFF, int* LDDCOE,
                       double* UCOEFF, int* LDUCO1, int* LDUCO2,
                       int* NR,
                       double* A, int* LDA,
                       double* B, int* LDB,
                       double* C, int* LDC,
                       double* D, int* LDD,
                       double* TOL,
                       int* IWORK,
                       double* DWORK, int* LDWORK,
                       int* INFO);

    extern int tb04ad_(char* ROWCOL,
                       int* N, int* M, int* P,
                       double* A, int* LDA,
                       double* B, int* LDB,
                       double* C, int* LDC,
                       double* D, int* LDD,
                       int* NR,
                       int* INDEX,
                       double* DCOEFF, int* LDDCOE,
                       double* UCOEFF, int* LDUCO1, int* LDUCO2,
                       double* TOL1,
                       double* TOL2,
                       int* IWORK,
                       double* DWORK,
                       int* LDWORK,
                       int* INFO);

    extern int ab13dd_(char *DICO, char *JOBE, char *EQUIL, char *JOBD,
                        int *N, int *M, int *P, double *FPEAK,
                        double *A, int *LDA,
                        double *E, int *LDE,
                        double *B, int *LDB,
                        double *C, int *LDC,
                        double *D, int *LDD,
                        double *GPEAK,
                        double *TOL,
                        int *IWORK,
                        double *DWORK, int *LDWORK,
                        doublecomplex *CWORK, int *LCWORK,
                        int *INFO);
    extern int sb10dd_(int *N, int *M, int *NP,
                        int *NCON, int *NMEAS, double *GAMMA,
                        double *A, int *LDA,
                        double *B, int *LDB, 
                        double *C, int *LDC,
                        double *D, int *LDD,
                        double *AK, int *LDAK,
                        double *BK, int *LDBK,
                        double *CK, int *LDCK,
                        double *DK, int *LDDK,
                        double *X, int *LDX,
                        double *Z, int *LDZ,
                        double *RCOND,
                        double *TOL,
                        int *IWORK,
                        double *DWORK, int *LDWORK, 
                        int *BWORK, int *INFO);

    extern int sb10fd_(int *N, int *M, int *NP,
                        int *NCON, int *NMEAS, double *GAMMA,
                        double *A, int *LDA,
                        double *B, int *LDB,
                        double *C, int *LDC,
                        double *D, int *LDD,
                        double *AK, int *LDAK,
                        double *BK, int *LDBK,
                        double *CK, int *LDCK,
                        double *DK, int *LDDK,
                        double *RCOND,
                        double *TOL,
                        int *IWORK, 
                        double *DWORK, int *LDWORK,
                        int *BWORK, int *INFO);
                        
    extern int mb05od_ (char *balanc,
                        int *n, int *ndiag, double *delta,
                        double *a, int *lda,
                        int *mdig, int *idig,
                        int *iwork,
                        double *dwork, int *ldwork,
                        int *iwarn, int *info );

};
//---------------------------------------------------------------------------
hwMathStatus slicot_ab13cd(const hwMatrix& A,
                           const hwMatrix& B,
                           const hwMatrix& C,
                           const hwMatrix& D, 
                           const hwMatrix& TOL,
                           hwMatrix&       INFO,
                           hwMatrix&       FPEAK,
                           hwMatrix&       RESULT)
{
    hwMathStatus status;

    if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 1);
    if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 1);
    hwMatrix __A(A); /* copy matrix in/out */

    if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
    if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
    hwMatrix __B(B); /* copy matrix in/out */

    if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
    if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
    hwMatrix __C(C); /* copy matrix in/out */

    if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
    if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
    hwMatrix __D(D); /* copy matrix in/out */

    int Nx = A.M();
    int Nu = B.N();
    int Ny = C.M();

    if (Nx != A.N()) return status(HW_MATH_ERR_ARRAYSIZE, 1);
    if (Nx != B.M()) return status(HW_MATH_ERR_ARRAYSIZE, 2);
    if (Nx != C.N()) return status(HW_MATH_ERR_ARRAYSIZE, 3);
    if (Ny != D.M()) return status(HW_MATH_ERR_ARRAYSIZE, 4);
    if (Nu != D.N()) return status(HW_MATH_ERR_ARRAYSIZE, 4);

    double tol = 0;
    if ((TOL.M() != TOL.N()) && (TOL.M() != 1)) return status(HW_MATH_ERR_ARRAYSIZE, 1);
    tol = TOL(0);

    int n = (int)(A.M());
    int m = (int)(B.N());
    int np = (int)(C.M());
    int lda = _max(1, n);
    int ldb = _max(1, n);
    int ldc = _max(1, np);
    int ldd = _max(1, np);
    int lcwork = 2 * _max(1, (n + m) * (n + np) + 3 * _max(m, np));
    int ldwork = _max(2, 4*n*n+2*m*m+3*m*n+m*np+2*(n+np)*np+10*n+6*_max(m,np));

    double dresult = 0.0;
    int info = 0;

    doublecomplex *__CWORK = new doublecomplex[lcwork];
    hwMatrixI __IBWORK((int)1, (int)2 * n, hwMatrixI::REAL);   
    int* p__IBWORK = __IBWORK.GetRealData();

    hwMatrixI __IWORK((int)1, (int)n, hwMatrixI::REAL);   
    int* p__IWORK = __IWORK.GetRealData();
    hwMatrix __DWORK((int)1, ldwork, hwMatrix::REAL);   

    try
    {
        dresult = ab13cd_(&n, &m, &np,
            __A.GetRealData(), &lda,
            __B.GetRealData(), &ldb,
            __C.GetRealData(), &ldc,
            __D.GetRealData(), &ldd,
            &tol,
            p__IWORK,
            __DWORK.GetRealData(),
            &ldwork,
            __CWORK,
            &lcwork,
            p__IBWORK,
            &info);
    }
    catch (const int)
    {
        if (__CWORK)
        {
            delete [] __CWORK;
            __CWORK = nullptr;
        }
        return status(HW_MATH_ERR_NOTALLOWED);
    }  
    if (__CWORK)
    {
        delete [] __CWORK;
        __CWORK = nullptr;
    }

    status = INFO.Dimension(1, 1, hwMatrix::REAL);
    if (!status.IsOk())
    {
        status.ResetArgs();
        return status;
    }
    INFO(0) = (double)info;

    double *fpeak = __DWORK.GetRealData();
    status = FPEAK.Dimension(1, 1, hwMatrix::REAL);
    if (!status.IsOk())
    {
        status.ResetArgs();
        return status;
    }
    FPEAK(0) = (double)fpeak[1];

    status = RESULT.Dimension(1, 1, hwMatrix::REAL);
    if (!status.IsOk())
    {
        status.ResetArgs();
        return status;
    }
    RESULT(0) = (double)dresult;

    return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_ab13bd(const hwString& DICO,
                           const hwString& JOBN,
                           const hwMatrix& A,
                           const hwMatrix& B,
                           const hwMatrix& C,
                           const hwMatrix& D, 
                           const hwMatrix& TOL,
                           hwMatrix&       INFO,
                           hwMatrix&       IWARN,
                           hwMatrix&       NQ,
                           hwMatrix&       RESULT)
{
    hwMathStatus status;
    
    char *dico = (char*)DICO.c_str();
    char *jobn = (char*)JOBN.c_str();

    if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 1);
    if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 1);
    hwMatrix __A(A); /* copy matrix in/out */

    if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
    if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
    hwMatrix __B(B); /* copy matrix in/out */

    if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
    if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
    hwMatrix __C(C); /* copy matrix in/out */

    if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
    if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
    hwMatrix __D(D); /* copy matrix in/out */
    
    int Nx = A.M();
    int Nu = B.N();
    int Ny = C.M();

    if (Nx != A.N()) return status(HW_MATH_ERR_ARRAYSIZE, 1);
    if (Nx != B.M()) return status(HW_MATH_ERR_ARRAYSIZE, 2);
    if (Nx != C.N()) return status(HW_MATH_ERR_ARRAYSIZE, 3);
    if (Ny != D.M()) return status(HW_MATH_ERR_ARRAYSIZE, 4);
    if (Nu != D.N()) return status(HW_MATH_ERR_ARRAYSIZE, 4);

    double tol = 0;
    if ((TOL.M() != TOL.N()) && (TOL.M() != 1)) return status(HW_MATH_ERR_ARRAYSIZE, 1);
    tol = TOL(0);
    
    int n = Nx;
    int m = Nu;
    int p = Ny;
    
    int lda = _max(1, n);
    int ldb = _max(1, n);
    int ldc = _max(1, p);
    int ldd = _max(1, p);
    
    int ldwork1 = m*(n+m) + _max(_max( n*(n+5), m*(m+2)), 4*p );
    int ldwork2 = n*( _max( n, p ) + 4 ) + _min( n, p );
    int ldwork =  _max(_max( 1, ldwork1), ldwork2);
    
    int nq = 0;
    double dresult = 0.;
    hwMatrix __DWORK((int)1, (int)ldwork, hwMatrix::REAL);   
    
    int info = 0;
    int iwarn = 0;
    
    try
    {
     dresult = ab13bd_(dico, jobn,
                       &n, &m, &p,
                       __A.GetRealData(), &lda,
                       __B.GetRealData(), &ldb,
                       __C.GetRealData(), &ldc,
                       __D.GetRealData(), &ldd,
                       &nq, &tol,
                       __DWORK.GetRealData(), &ldwork,
                       &iwarn, &info);
    }
    catch (const int)
    {
        return status(HW_MATH_ERR_NOTALLOWED);
    }  

    status = NQ.Dimension(1, 1, hwMatrix::REAL);
    if (!status.IsOk())
    {
        status.ResetArgs();
        return status;
    }
    NQ(0) = (double)nq;

    status = INFO.Dimension(1, 1, hwMatrix::REAL);
    if (!status.IsOk())
    {
        status.ResetArgs();
        return status;
    }
    INFO(0) = (double)info;

    status = IWARN.Dimension(1, 1, hwMatrix::REAL);
    if (!status.IsOk())
    {
        status.ResetArgs();
        return status;
    }
    IWARN(0) = (double)iwarn;

    status = RESULT.Dimension(1, 1, hwMatrix::REAL);
    if (!status.IsOk())
    {
        status.ResetArgs();
        return status;
    }
    RESULT(0) = (double)dresult;

    return status;
}
//---------------------------------------------------------------------------
SLICOT_DECLS hwMathStatus slicot_td04ad(const hwMatrix&  UCOEFF,
                                        const hwMatrix&  DCOEFF,
                                        const hwMatrixI& INDEX,
                                        double           tol,
                                        hwMatrix&        A,
                                        hwMatrix&        B,
                                        hwMatrix&        C,
                                        hwMatrix&        D,
                                        int&             NR,
                                        int&             INFO)
{
    if (!UCOEFF.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 1);

    if (!DCOEFF.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 1);

    if (!INDEX.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 1);

    hwMatrix Ucopy(UCOEFF);
    hwMatrix Dcopy(DCOEFF);

    int q = Dcopy.M();      // number of outputs
    int p = Ucopy.M() / q;  // number of inputs

    if (Ucopy.M() != p * q)
        return hwMathStatus(HW_MATH_ERR_ARRAYSIZE, 1, 2);

    if (INDEX.Size() != q)
        return hwMathStatus(HW_MATH_ERR_ARRAYSIZE, 2, 3);

    int n = 0;
    int lddcoe = _max(1, q);
    int lduco1 = _max(1, q);
    int lduco2 = _max(1, p);

    for (int ii = 0; ii < q; ++ii)
        n += INDEX(ii);

    int lda = n;
    int ldb = n;
    int ldc = _max(p, q);
    int ldd = q;

    hwMathStatus status;

    status = A.Dimension(lda, n, hwMatrix::REAL);
    status = B.Dimension(ldb, _max(p, q), hwMatrix::REAL);
    status = C.Dimension(ldc, n, hwMatrix::REAL);
    status = D.Dimension(ldd, p, hwMatrix::REAL);

    if (p == 0 || q == 0)
    {
        INFO = 0;
        return status;
    }

    if (n == 0) // SLICOT won't handle this
    {
        for (int i = 0; i < q; ++i)
        {
            for (int j = 0; j < p; ++j)
                D(i, j) = Ucopy(i + j, 0) / Dcopy(i, 0);
        }

        status = B.Dimension(n, p, hwMatrix::REAL);
        status = C.Dimension(q, n, hwMatrix::REAL);
        INFO = 0;
        return status;
    }

    int ldwork = _max (1, n + _max(n, _max(3*p, 3*q)));

    hwMatrixI __IWORK(n + _max(p, q), hwMatrixI::REAL);
    hwMatrix __DWORK(ldwork, hwMatrix::REAL);

    char* rowcol = "R";
    
    try
    {
        td04ad_(rowcol,
                &p, &q,
                const_cast<int*> (INDEX.GetRealData()),
                Dcopy.GetRealData(), &lddcoe,
                Ucopy.GetRealData(), &lduco1, &lduco2,
                &NR,
                A.GetRealData(), &lda,
                B.GetRealData(), &ldb,
                C.GetRealData(), &ldc,
                D.GetRealData(), &ldd,
                &tol,
                __IWORK.GetRealData(),
                __DWORK.GetRealData(), &ldwork,
                &INFO);
    }
    catch (const int)
    {
        return status(HW_MATH_ERR_NOTALLOWED);
    }  

    if (INFO == 0)
    {
        status = A.Resize(NR, NR, true);
        if (!status.IsOk())
        {
            status.ResetArgs();
            return status;
        }
        status = B.Resize(NR, p, true);
        if (!status.IsOk())
        {
            status.ResetArgs();
            return status;
        }
        status = C.Resize(q, NR, true);
        if (!status.IsOk())
        {
            status.ResetArgs();
            return status;
        }
        status = D.Resize(q, p, true);
        if (!status.IsOk())
        {
            status.ResetArgs();
            return status;
        }
    }

    return status;
}
//---------------------------------------------------------------------------
SLICOT_DECLS hwMathStatus slicot_tb04ad(const hwMatrix& A,
                                        const hwMatrix& B,
                                        const hwMatrix& C,
                                        const hwMatrix& D,
                                        hwMatrix&       UCOEFF,
                                        hwMatrix&       DCOEFF,
                                        hwMatrixI&      INDEX,
                                        double          tol1,
                                        double          tol2,
                                        int&            NR,
                                        int&            INFO)
{
    if (!A.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 1);

    if (!A.IsSquare())
        return hwMathStatus(HW_MATH_ERR_MTXNOTSQUARE, 1);

    if (!B.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 2);

    if (!C.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 3);

    if (!D.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 4);

    int n = A.M();  // number of states

    if (B.M() != n)
        return hwMathStatus(HW_MATH_ERR_ARRAYSIZE, 1, 2);

    int p = B.N();  // number of inputs

    if (C.N() != n)
        return hwMathStatus(HW_MATH_ERR_ARRAYSIZE, 1, 3);

    int q = C.M();  // number of outputs

    if (D.N() != p)
        return hwMathStatus(HW_MATH_ERR_ARRAYSIZE, 2, 4);

    if (D.M() != q)
        return hwMathStatus(HW_MATH_ERR_ARRAYSIZE, 3, 4);

    char* rowcol = "R";

    hwMathStatus status;
    int qp = q * p;

    if (qp == 0)
        n = -1;

    status = UCOEFF.Dimension(qp, n + 1, hwMatrix::REAL);
    status = DCOEFF.Dimension(q, n + 1, hwMatrix::REAL);
    status = INDEX.Dimension(q, 1, hwMatrixI::REAL);

    if (qp == 0)
    {
        INFO = 0;
        return status;
    }

    hwMatrix Acopy(A);
    hwMatrix Bcopy(B);
    hwMatrix Ccopy(C);
    hwMatrix Dcopy(D);

    double* a = Acopy.GetRealData();
    double* b = Bcopy.GetRealData();
    double* c = Ccopy.GetRealData();
    double* d = Dcopy.GetRealData();
    double* up = UCOEFF.GetRealData();
    double* dp = DCOEFF.GetRealData();
    int* index = INDEX.GetRealData();

    hwMatrixI IWORK(n + _max(p,q), 1, hwMatrixI::REAL);

    int ldwork = n*(n + 1) + _max(_max(n*p + 2 * n + _max(n, p), 3 * p), q);

    hwMatrix DWORK(ldwork, 1, hwMatrix::REAL);

    try
    {
        tb04ad_(rowcol,
                &n, &p, &q,
                Acopy.GetRealData(), &n,
                Bcopy.GetRealData(), &n,
                Ccopy.GetRealData(), &q,
                Dcopy.GetRealData(), &q,
                &NR,
                INDEX.GetRealData(),
                DCOEFF.GetRealData(), &q,
                UCOEFF.GetRealData(), &q, &p,
                &tol1, &tol2,
                IWORK.GetRealData(),
                DWORK.GetRealData(),
                &ldwork,
                &INFO);
    }
    catch (const int)
    {
        return status(HW_MATH_ERR_NOTALLOWED);
    }

    if (INFO == 0)
    {
        int maxIndex = INDEX(0);

        for (int ii = 1; ii < q; ++ii)
        {
            if (INDEX(ii) > maxIndex)
                maxIndex = INDEX(ii);
        }

        status = UCOEFF.Resize(qp, maxIndex + 1, true);
        if (!status.IsOk())
        {
            status.ResetArgs();
            return status;
        }
        status = DCOEFF.Resize(q, maxIndex + 1, true);
        if (!status.IsOk())
        {
            status.ResetArgs();
            return status;
        }
    }

    return status;
}
//---------------------------------------------------------------------------
SLICOT_DECLS hwMathStatus slicot_ab13dd(const hwString& DICO,
                                        const hwString& JOBE,
                                        const hwString& EQUIL,
                                        const hwString& JOBD,
                                        const hwMatrix& FPEAKIN,
                                        const hwMatrix& A,
                                        const hwMatrix& B,
                                        const hwMatrix& C,
                                        const hwMatrix& D,
                                        const hwMatrix& E,
                                        const hwMatrix& TOL,
                                        hwMatrix&       GPEAK,
                                        hwMatrix&       FPEAKOUT,
                                        hwMatrix&       INFO)
{
    hwMathStatus status;

    if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 1);
    if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 1);
    hwMatrix __A(A); /* copy matrix in/out */
    
    if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
    if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
    hwMatrix __B(B); /* copy matrix in/out */

    if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
    if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
    hwMatrix __C(C); /* copy matrix in/out */

    if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
    if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
    hwMatrix __D(D); /* copy matrix in/out */
    
    if (E.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
    if (!E.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
    hwMatrix __E(E); /* copy matrix in/out */

    if (FPEAKIN.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
    if (!FPEAKIN.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
    if (FPEAKIN.M() * FPEAKIN.N() != 2) return status(HW_MATH_ERR_ARRAYSIZE, 6);
    hwMatrix __FPEAKIN(FPEAKIN); /* copy matrix in/out */

    double tol = 0;
    if ((TOL.M() != TOL.N()) && (TOL.M() != 1)) return status(HW_MATH_ERR_ARRAYSIZE, 1);
    tol = TOL(0);

    char *dico = (char*)DICO.c_str();
    char *jobe = (char*)JOBE.c_str();
    char *equil = (char*)EQUIL.c_str();
    char *jobd = (char*)JOBD.c_str();
    int info = 0;
    
    int n = __A.M();      // n: number of states
    int m = __B.N();      // m: number of inputs
    int p = __C.M();      // p: number of outputs
        
    int lda = _max(1, n);
    int lde = _max(1, n);
    int ldb = _max(1, n);
    int ldc = _max(1, p);
    int ldd = _max(1, p);
    
    int ldwork = _max(1, 15 * n * n + p * p + m * m + (6 * n + 3) * (p + m) + 4 * p * m + n * m + 22 * n + 7 * _min(p, m));
    int lcwork = _max(1, (n + m) * (n + p) + 2 * _min(p, m) + _max(p, m));

    hwMatrix __GPEAKLOCAL((int)1, (int)2, hwMatrix::REAL); 
    hwMatrixI __IWORK((int)1, (int)n, hwMatrixI::REAL); 
    hwMatrix __DWORK((int)1, ldwork, hwMatrix::REAL); 
    doublecomplex *__CWORK = nullptr;
    try
    {
        __CWORK = new doublecomplex[lcwork];
    }
    catch(std::bad_alloc&) 
    {
        return hwMathStatus(HW_MATH_ERR_ALLOCFAILED);
    }
    
    try
    {
        ab13dd_(dico, jobe, equil, jobd,
                &n, &m, &p, __FPEAKIN.GetRealData(),
                __A.GetRealData(), &lda,
                __E.GetRealData(), &lde,
                __B.GetRealData(), &ldb,
                __C.GetRealData(), &ldc,
                __D.GetRealData(), &ldd,
                __GPEAKLOCAL.GetRealData(),
                &tol,
                __IWORK.GetRealData(),
                __DWORK.GetRealData(), &ldwork,
                __CWORK, &lcwork,
                &info);

    }
    catch (const int)
    {
        if (__CWORK)
        {
            delete [] __CWORK;
            __CWORK = nullptr;
        }
        return status(HW_MATH_ERR_NOTALLOWED);
    }  
    
    if (__CWORK)
    {
        delete [] __CWORK;
        __CWORK = nullptr;
    }

    GPEAK = __GPEAKLOCAL;
    FPEAKOUT = __FPEAKIN;
    
    status = INFO.Dimension(1, 1, hwMatrix::REAL);
    if (!status.IsOk())
    {
        status.ResetArgs();
        return status;
    }
    INFO(0) = (double)info;
    
    return status;
}
//---------------------------------------------------------------------------
SLICOT_DECLS hwMathStatus slicot_sb10dd(const hwMatrix& A,
                                        const hwMatrix& B,
                                        const hwMatrix& C,
                                        const hwMatrix& D,
                                        int             NCON,
                                        int             NMEAS,
                                        double          GAMMA,
                                        double          TOL,
                                        hwMatrix&       AK,
                                        hwMatrix&       BK,
                                        hwMatrix&       CK,
                                        hwMatrix&       DK,
                                        hwMatrix&       X,
                                        hwMatrix&       Z,
                                        hwMatrix&       RCOND,
                                        hwMatrix&       INFO)
{
    hwMathStatus status;
    
    if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 1);
    if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 1);
    hwMatrix __A(A); /* copy matrix in/out */

    if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
    if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
    hwMatrix __B(B); /* copy matrix in/out */

    if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
    if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
    hwMatrix __C(C); /* copy matrix in/out */

    if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
    if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
    hwMatrix __D(D); /* copy matrix in/out */

    int N = __A.M(); // n: number of states
    int M = __B.N(); // m: number of inputs
    int NP = __C.M();// np: number of outputs

    if (N == 0 || M == 0 || NP == 0)
    {
        // returns empty matrices in this case
        status = INFO.Dimension(1, 1, hwMatrix::REAL);
        if (!status.IsOk())
        {
            status.ResetArgs();
            return status;
        }
        INFO(0) = (double)0.;
        return status;
    }

    int LDA = _max(1, N);
    int LDB = _max(1, N);
    int LDC = _max(1, NP);
    int LDD = _max(1, NP);
        
    int LDAK = _max(1, N);
    int LDBK = _max(1, N);
    int LDCK = _max(1, NCON);
    int LDDK = _max(1, NCON);
        
    int LDX = _max(1, N);
    int LDZ = _max(1, N);
    
    // arguments out
    hwMatrix __AK(LDAK, N, hwMatrix::REAL);
    hwMatrix __BK(LDBK, NMEAS, hwMatrix::REAL);
    hwMatrix __CK(LDCK, N, hwMatrix::REAL);
    hwMatrix __DK(LDDK, NMEAS, hwMatrix::REAL);
    hwMatrix __X(LDX, N, hwMatrix::REAL);
    hwMatrix __Z(LDZ, N, hwMatrix::REAL);
    hwMatrix __RCOND(8, 1, hwMatrix::REAL);

    // workspace
    int M2 = NCON;
    int M1 = M - M2;
    int NP1 = NP - NMEAS;
    int NP2 = NMEAS;
        
    int LIWORK = _max(2 * _max (M2, N), _max (M, _max(M2 + NP2, N * N)));
    int Q = _max(_max(M1, M2), _max (NP1, NP2));
    
    int k1 = (N+Q)*(N+Q+6);
    int v1 = 14*N+23;
    int v2 = 6*N;
    int v3 = 2*N + _max(M, 2*Q);
    int v4 = 3*_max(M, 2*Q);
    int k2 = _max(_max(v1, v2), _max(v3, v4));
    int LDWORK = _max(k1, 13*N*N + M*M + 2*Q*Q + N*(M+Q) + _max(M*(M+7*N), 2*Q*(8*N+M+2*Q)) + 6*N + k2);

    hwMatrixI __IWORK(1, LIWORK, hwMatrixI::REAL);
    hwMatrix __DWORK(1, LDWORK, hwMatrix::REAL);
    hwMatrixI __BWORK(1, 2 * N, hwMatrixI::REAL);

    int info = 0;
    try
    {
        sb10dd_(&N, &M, &NP,
                &NCON, &NMEAS,
                &GAMMA,
                __A.GetRealData(), &LDA,
                __B.GetRealData(), &LDB,
                __C.GetRealData(), &LDC,
                __D.GetRealData(), &LDD,
                __AK.GetRealData(), &LDAK,
                __BK.GetRealData(), &LDBK,
                __CK.GetRealData(), &LDCK,
                __DK.GetRealData(), &LDDK,
                __X.GetRealData(), &LDX,
                __Z.GetRealData(), &LDZ,
                __RCOND.GetRealData(),
                &TOL,
                __IWORK.GetRealData(),
                __DWORK.GetRealData(), &LDWORK,
                __BWORK.GetRealData(),
                &info);
    }
    catch (const int)
    {
        return status(HW_MATH_ERR_NOTALLOWED);
    }  
            
    if (info == 0)
    {
        AK = __AK;
        BK = __BK;
        CK = __CK;
        DK = __DK;
        RCOND = __RCOND; 
        X = __X; 
        Z = __Z; 
    }
    
    status = INFO.Dimension(1, 1, hwMatrix::REAL);
    if (!status.IsOk())
    {
        status.ResetArgs();
        return status;
    }
    INFO(0) = (double)info;
    
    return status;
}
//---------------------------------------------------------------------------
SLICOT_DECLS hwMathStatus slicot_sb10fd(const hwMatrix& A,
                                        const hwMatrix& B,
                                        const hwMatrix& C,
                                        const hwMatrix& D,
                                        int             NCON,
                                        int             NMEAS,
                                        double          GAMMA,
                                        double          TOL,
                                        hwMatrix&       AK,
                                        hwMatrix&       BK,
                                        hwMatrix&       CK,
                                        hwMatrix&       DK,
                                        hwMatrix&       RCOND,
                                        hwMatrix&       INFO)
{
    hwMathStatus status;
    if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 1);
    if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 1);
    hwMatrix __A(A); /* copy matrix in/out */

    if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
    if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
    hwMatrix __B(B); /* copy matrix in/out */

    if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
    if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
    hwMatrix __C(C); /* copy matrix in/out */

    if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
    if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
    hwMatrix __D(D); /* copy matrix in/out */

    int N = __A.M(); // n: number of states
    int M = __B.N(); // m: number of inputs
    int NP = __C.M();// np: number of outputs

    if (N == 0 || M == 0 || NP == 0)
    {
        // returns empty matrices in this case
        status = INFO.Dimension(1, 1, hwMatrix::REAL);
        if (!status.IsOk())
        {
            status.ResetArgs();
            return status;
        }
        INFO(0) = (double)0;
        return status;
    }

    int LDA = _max(1, N);
    int LDB = _max(1, N);
    int LDC = _max(1, NP);
    int LDD = _max(1, NP);

    int LDAK = _max(1, N);
    int LDBK = _max(1, N);
    int LDCK = _max(1, NCON);
    int LDDK = _max(1, NCON);
        
    int LDX = _max(1, N);
    int LDZ = _max(1, N);
    
    // arguments out
    hwMatrix __AK(LDAK, N, hwMatrix::REAL);
    hwMatrix __BK(LDBK, NMEAS, hwMatrix::REAL);
    hwMatrix __CK(LDCK, N, hwMatrix::REAL);
    hwMatrix __DK(LDDK, NMEAS, hwMatrix::REAL);
    hwMatrix __RCOND(4, 1, hwMatrix::REAL);
        
    // workspace
    int M2 = NCON;
    int M1 = M - M2;
    int NP1 = NP - NMEAS;
    int NP2 = NMEAS;

    int LIWORK = _max(2*_max(_max(N, M-NCON), _max(NP-NMEAS, NCON)), N*N);
    int Q = _max(_max(M1, M2), _max(NP1, NP2));
    int v1 = (N+Q)*(N+Q+6);
    int v2 = Q*(Q + _max(_max(N, Q), 5) + 1);
    int v3 = 2*N*(N+2*Q) + _max(_max(1, 4*Q*Q + _max (2*Q, 3*N*N + _max (2*N*Q, 10*N*N+12*N+5))), Q*(3*N + 3*Q + _max (2*N, 4*Q + _max (N, Q))));
    int LDWORK = 2*Q*(3*Q+2*N) + _max(_max(1, v1), _max(v2, v3));

    hwMatrixI __IWORK(1, LIWORK, hwMatrixI::REAL);
    hwMatrix __DWORK(1, LDWORK, hwMatrix::REAL);
    hwMatrixI __BWORK(1, 2 * N, hwMatrixI::REAL);
        
    // error indicator
    int info = 0;
    try
    {
        sb10fd_(&N, &M, &NP,
                &NCON, &NMEAS,
                &GAMMA,
                __A.GetRealData(), &LDA,
                __B.GetRealData(), &LDB,
                __C.GetRealData(), &LDC,
                __D.GetRealData(), &LDD,
                __AK.GetRealData(), &LDAK,
                __BK.GetRealData(), &LDBK,
                __CK.GetRealData(), &LDCK,
                __DK.GetRealData(), &LDDK,
                __RCOND.GetRealData(),
                &TOL,
                __IWORK.GetRealData(),
                __DWORK.GetRealData(), &LDWORK,
                __BWORK.GetRealData(),
                &info);
    }
    catch (const int)
    {
        return status(HW_MATH_ERR_NOTALLOWED);
    }  
    
    if (info == 0)
    {
        AK = __AK; 
        BK = __BK; 
        CK = __CK; 
        DK = __DK; 
        RCOND = __RCOND; 
    }
    
    status = INFO.Dimension(1, 1, hwMatrix::REAL);
    if (!status.IsOk())
    {
        status.ResetArgs();
        return status;
    }
    INFO(0) = (double)info;
    return status;
}
//-----------------------------------------------------------------------------
SLICOT_DECLS hwMathStatus slicot_expm(const hwMatrix& A, 
                                      hwMatrix&       X,
                                      int&            warn,
                                      int&            info)
{
    hwMathStatus status;

    if (!A.IsSquare())
    {
        return status(HW_MATH_ERR_MTXNOTSQUARE, 1);
    }

    warn = 0;
    info = 0;

    if (A.IsEmpty())
    {
        X.Dimension(0, 0, hwMatrix::REAL);
        return status;
    }
    
    double delta = 1.0;
    char balanc = 'S';
    int ndiag = 9;
    int mdig = 0;
    int idig = 0;

    if (A.IsReal())
    {
        hwMatrix __A(A); // copy matrix
        int n = A.N();
        int lda = _max(1, n);
        hwMatrixI __IWORK(1, n, hwMatrixI::REAL);
        hwMatrix __DWORK(1, n * (2 * n + ndiag + 1) + ndiag, hwMatrix::REAL);
        int ldwork = n * (2 * n + ndiag + 1) + ndiag;

        try
        {
            mb05od_(&balanc, &n, &ndiag, &delta, __A.GetRealData(), &lda, &mdig,
                    &idig,__IWORK.GetRealData(), __DWORK.GetRealData(), &ldwork,
                    &warn, &info);
        }
        catch (const int)
        {
            return status(HW_MATH_ERR_NOTALLOWED);
        }

        X = __A;
    }
    else
    {
        int n = A.N();
        int nu = 2 * n;
        hwMatrix U(nu, nu, hwMatrix::REAL);
        int lda = _max(1, nu);
        hwMatrixI __IWORK(1, nu, hwMatrixI::REAL);
        hwMatrix __DWORK(1, nu * (2 * nu + ndiag + 1) + ndiag, hwMatrix::REAL);
        int ldwork = nu * (2 * nu + ndiag + 1) + ndiag;
        hwMatrix __A(n, n, hwMatrix::COMPLEX);

        int k = 0;
        for (int i = 0; i < n; i++)
        {
           for (int j = 0; j < n; j++)
           {
               k = i + j*nu;
               U(k) =  A.z(i+j*n).Real();
               k = i + (n+j)*nu;
               U(k) =  A.z(i+j*n).Imag();
               k = (n+i) + (j)*nu;
               U(k) = -A.z(i+j*n).Imag();
               k = (n+i) + (n+j)*nu;
               U(k) =  A.z(i+j*n).Real();
           }
        }

        try
        {
            mb05od_(&balanc, &nu, &ndiag, &delta, U.GetRealData(), &lda, &mdig,
                    &idig, __IWORK.GetRealData(), __DWORK.GetRealData(),
                    &ldwork, &warn, &info);
        }
        catch (const int)
        {
            return status(HW_MATH_ERR_NOTALLOWED);
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                double rp, ip;
                k = i + j*nu;
                rp = U(k);
                k = i + (n+j)*nu;
                ip = U(k);
                __A.z(i+j*n).Set(rp, ip);
            }
        }

        X = __A;
    }
    
    return status;
}
//-----------------------------------------------------------------------------
static hwMathStatus iterationsLogm(hwMatrix& s,
                                   int&      m,
                                   int&      nbIterations)
{
    // function[s, m, nbIterations] = iterationsLogm(s)
    // 1. The Matrix Logarithm : from Theory to Computation
    //    6th European Congress of Mathematics, July 2012
    // 2. Functions of Matrices Theory - Theory and Computation
    //    Nicholas J.Higham University of Manchester
    //    page 277 table 11.1

    std::vector<double> pade_degree_max = { 1.10e-5, 1.82e-3, 1.62e-2, 5.39e-2, 1.14e-1, 1.87e-1, 2.64e-1 };

    // Inverse scaling and squaring algorithm with Schur decomposition
    int nbIterationsMax = 100;
    int p = 0;
    int n = s.M();
    int k1, k2;
    hwMatrix temp;
    hwMathStatus status;

    // Algorithm 11.9 (page 277)
    m = static_cast<int> (pade_degree_max.size());
    nbIterations = 0;
    temp = s;

    while (nbIterations < nbIterationsMax)
    {
        if (temp.IsReal())
        {
            for (int i = 0; i < n; ++i)
                temp(i, i) -= 1.0;
        }
        else
        {
            for (int i = 0; i < n; ++i)
                temp.z(i, i) -= 1.0;
        }

        double tau;
        status = temp.Norm(tau, 1);
        
        if (tau <= pade_degree_max[6])
        {
            p = p + 1;

            for (int i = 0; i < m; ++i)
            {
                if (tau <= pade_degree_max[i])
                {
                    k1 = i;
                    break;
                }
            }

            for (int i = 0; i < m; ++i)
            {
                if (0.5 * tau <= pade_degree_max[i])
                {
                    k2 = i;
                    break;
                }
            }

            if (k1 - k2 <= 1 || p == 2)
            {
                m = k1 + 1;
                break;
            }
        }

        nbIterations = nbIterations + 1;
        notslicot_sqrtm(s, temp);
        s = temp;
    }

    if (nbIterations >= nbIterationsMax)
        status(HW_MATH_WARN_MAXSQRTM);

    return status;
}
//-----------------------------------------------------------------------------
static hwMathStatus logm_pade(const hwMatrix& a,
                              int             m,
                              hwMatrix&       r)
{
    // function r = logm_pade(a, m)
    // Evaluating Pade Approximants
    // SIAM J.MATRIX ANAL.APPL. 2001
    // Vol. 22, No. 4, page 1126 to 1135
    hwMatrix v(m, m, hwMatrix::REAL);
    hwMatrix V;
    hwMatrix D;

    v.SetElements(0.0);

    for (int k = 1; k < m; ++k)
    {
        double val = k / sqrt(static_cast<double> (4*k*k - 1));
        v(k - 1, k) = val;
        v(k, k - 1) = val;
    }

    hwMathStatus status = v.EigenSH(&V, D);     // V, D will be real

    hwMatrix w;
    status = V.ReadRow(0, w);
    w.Transpose();

    for (int k = 0; k < m; ++k)
        w(k) = w(k) * w(k);

    for (int k = 0; k < m; ++k)
        D(k) = (D(k) + 1.0) / 2.0;

    int n = a.M();
    hwMatrix I(n, n, hwMatrix::REAL);

    r.Dimension(n, n, hwMatrix::REAL);
    r.SetElements(0.0);
    I.Identity();

    for (int k = 0; k < m; ++k)
        r = r + w(k) * (a / (I + D(k) * a));

    return status;
}
//-----------------------------------------------------------------------------
SLICOT_DECLS hwMathStatus notslicot_logm(const hwMatrix& A,
                                         hwMatrix&       X)
{
    hwMathStatus status;

    if (!A.IsSquare())
        return status(HW_MATH_ERR_MTXNOTSQUARE, 1);

    if (A.IsEmpty())
    {
        X.Dimension(0, 0, hwMatrix::REAL);
        return status;
    }

    if (!A.IsFinite())
        return status(HW_MATH_ERR_NONFINITEDATA, 1);

    hwMatrix U;
    hwMatrix S;

    status = A.Schur(A.IsReal(), U, S);

    if (A.IsReal())
    {
        hwMatrix UC;
        hwMatrix SC;
        status = rsf2csf(U, S, UC, SC);
        U = UC;
        S = SC;
    }

    hwMatrix eigv;

    status = eigv.Diag(S, 0);

    if (!eigv.IsReal())
    {
        hwMatrix temp;
        eigv.UnpackComplex(&temp, nullptr);
        eigv = temp;
    }

    int n = eigv.M();
    bool realpos = true;

    for (int i = 0; i < n; ++i)
    {
        if (eigv(i) < 0.0)
        {
            realpos = false;
            break;
        }
    }
    
    int m;
    int nbIterations;

    status = iterationsLogm(S, m, nbIterations);

    hwMatrix I(n, n, hwMatrix::REAL);

    I.Identity();
    X = S - I;

    if (m > 1)
    {
        hwMatrix temp;
        status = logm_pade(X, m, temp);
        X = temp;
    }

    hwMatrix UH;
    UH.Hermitian(U);

    X = U * X * UH * static_cast<double> (pow(2, nbIterations));

    if (!X.IsReal() && A.IsReal() && realpos)
    {
        hwMatrix temp;
        X.UnpackComplex(&temp, nullptr);
        X = temp;
    }

    return status;
}
//-----------------------------------------------------------------------------
static hwMathStatus upperTriangularSquareRoot(int             n,
                                              const hwMatrix& T,
                                              hwMatrix&       R)
{
    // function R = upperTriangularSquareRoot(n, T)
    // based on the articles :
    // 1. N.J.Higham, Computing real square roots of a real matrix,
    //    Linear Algebra and Appl., 88 / 89 (1987), pp. 405 - 430.
    // 2. A.Bjorck and S.Hammarling, A Schur method for the square root of a matrix,
    //    Linear Algebra and Appl., 52 / 53 (1983), pp. 127 - 140
    hwMathStatus status;
    bool realR = true;

    if (T.IsReal())
    {
        for (int j = 0; j < n; ++j)
        {
            if (T(j, j) < 0.0)
            {
                realR = false;
                break;
            }
        }
    }
    else
    {
        realR = false;
    }

    if (realR)
        status = R.Dimension(n, n, hwMatrix::REAL);
    else
        status = R.Dimension(n, n, hwMatrix::COMPLEX);

    R.SetElements(0.0);

    if (T.IsReal() && R.IsReal())
    {
        for (int j = 0; j < n; ++j)
        {
            R(j, j) = sqrt(T(j, j));

            for (int i = j - 1; i > -1; --i)
            {
                double sum = 0.0;

                for (int k = i + 1; k < j; ++k)
                    sum += R(i, k) * R(k, j);

                R(i, j) = (T(i, j) - sum) / (R(i, i) + R(j, j));
            }
        }
    }
    else if (T.IsReal() && !R.IsReal())
    {
        for (int j = 0; j < n; ++j)
        {
            if (T(j, j) < 0.0)
                R.z(j, j) = hwComplex::sqrt_c(T(j, j));
            else
                R.z(j, j) = sqrt(T(j, j));

            for (int i = j - 1; i > -1; --i)
            {
                hwComplex sum(0.0, 0.0);

                for (int k = i + 1; k < j; ++k)
                    sum += R.z(i, k) * R.z(k, j);

                R.z(i, j) = (T(i, j) - sum) / (R.z(i, i) + R.z(j, j));
            }
        }
    }
    else    // !T.IsReal() && !R.IsReal()
    {
        for (int j = 0; j < n; ++j)
        {
            R.z(j, j) = hwComplex::sqrt(T.z(j, j));

            for (int i = j - 1; i > -1; --i)
            {
                hwComplex sum(0.0, 0.0);

                for (int k = i + 1; k < j; ++k)
                    sum += R.z(i, k) * R.z(k, j);

                R.z(i, j) = (T.z(i, j) - sum) / (R.z(i, i) + R.z(j, j));
            }
        }
    }

    return status;
}
//-----------------------------------------------------------------------------
hwMathStatus rsf2csf(const hwMatrix& UR,
                     const hwMatrix& TR,
                     hwMatrix&       UC,
                     hwMatrix&       TC)
{
    // Transforms a real Schur form to a complex Schur form.
    hwMathStatus status;

    if (!UR.IsSquare())
        return status(HW_MATH_ERR_MTXNOTSQUARE, 1);

    if (!UR.IsReal())
        return status(HW_MATH_ERR_COMPLEX, 1);

    if (!TR.IsSquare())
        return status(HW_MATH_ERR_MTXNOTSQUARE, 2);

    if (!TR.IsReal())
        return status(HW_MATH_ERR_COMPLEX, 2);

    UC = UR;
    TC = TR;

    int n = UR.M();
    hwMatrix subM;
    hwMatrix V;
    hwMatrix D;
    hwMatrix K(2, 2, hwMatrix::COMPLEX);
    bool zero;

    for (int l = n - 1; l > 0; --l)
    {
        if (TC.IsReal())
        {
            if (TC(l, l - 1) == 0.0)
                zero = true;
            else
                zero = false;
        }
        else
        {
            if (TC.z(l, l - 1) == 0.0)
                zero = true;
            else
                zero = false;
        }

        if (!zero)
        {
            status = subM.ReadSubmatrix(l-1, l-1, 2, 2, TC);
            status = subM.Eigen(true, &V, D);

            if (D.IsReal() && TC.IsReal())
            {
                double mu1 = D(0) - TC(l, l);
                double r = _hypot(mu1, TC(l, l - 1));

                K(0, 0) =  mu1 / r;
                K(0, 1) =  TC(l, l - 1) / r;
                K(1, 0) = -K(0, 1);
                K(1, 1) =  K(0, 0);
            }
            else
            {
                D.MakeComplex();
                UC.MakeComplex();
                TC.MakeComplex();
                K.MakeComplex();

                hwComplex mu1 = D.z(0) - TC.z(l, l);
                double r = _hypot(mu1.Mag(), TC.z(l, l - 1).Mag());

                K.z(0, 0) =  mu1.Conjugate() / r;
                K.z(0, 1) =  TC.z(l, l - 1) / r;
                K.z(1, 0) = -K.z(0, 1);
                K.z(1, 1) =  mu1 / r;
            }

            status = subM.ReadSubmatrix(l - 1, l - 1, 2, n - l + 1, TC);
            subM = K * subM;
            status = TC.WriteSubmatrix(l - 1, l - 1, subM);
            
            K.Hermitian();
            status = subM.ReadSubmatrix(0, l - 1, l + 1, 2, TC);
            subM *= K;
            status = TC.WriteSubmatrix(0, l - 1, subM);

            status = subM.ReadSubmatrix(0, l - 1, n, 2, UC);
            subM *= K;
            status = UC.WriteSubmatrix(0, l - 1, subM);

            if (TC.IsReal())
            {
                TC(l, l-1) = 0.0;
            }
            else
            {
                TC.z(l, l - 1) = 0.0;
            }
        }
    }

    return status;
}
//-----------------------------------------------------------------------------
SLICOT_DECLS hwMathStatus notslicot_sqrtm(const hwMatrix& A,
                                          hwMatrix&       X)
{
    hwMathStatus status;

    if (!A.IsSquare())
        return status(HW_MATH_ERR_MTXNOTSQUARE, 1);

    if (A.IsEmpty())
    {
        X.Dimension(0, 0, hwMatrix::REAL);
        return status;
    }

    if (!A.IsFinite())
        return status(HW_MATH_ERR_NONFINITEDATA, 1);

    int n = A.M();
    hwMatrix U;
    hwMatrix T;

    if (A.IsReal())
    {
        hwMatrix UR;
        hwMatrix TR;

        status = A.Schur(true, UR, TR);
        status = rsf2csf(UR, TR, U, T);
    }
    else
    {
        status = A.Schur(false, U, T);
    }

    hwMatrix R;

    if (T.IsDiag())
    {
        hwMatrix D;
        status = D.Diag(T, 0);

        if (D.IsReal())
        {
            for (int ii = 0; ii < n; ++ii)
                D(ii) = sqrt(D(ii));
        }
        else
        {
            for (int ii = 0; ii < n; ++ii)
                D.z(ii) = hwComplex::sqrt(D.z(ii));
        }

        status = R.Diag(D, 0);
    }
    else
    {
        status = upperTriangularSquareRoot(n, T, R);
    }

    if (T.IsReal())
    {
        for (int ii = 0; ii < n; ++ii)
        {
            if (T(ii, ii) == 0.0)
            {
                status(HW_MATH_WARN_NOSQRTM, 1);
                break;
            }
        }
    }
    else
    {
        for (int ii = 0; ii < n; ++ii)
        {
            if (T.z(ii, ii) == 0.0)
            {
                status(HW_MATH_WARN_NOSQRTM, 1);
                break;
            }
        }
    }
    
    hwMatrix UH;
    UH.Hermitian(U);
    X = U * R * UH;

    double toleranceDesired = 10 * n * MACHEP2;
    hwMatrix Ximag;
    X.UnpackComplex(nullptr, &Ximag);
    double norm1;
    double norm2;
    Ximag.Norm(norm1, 1);
    X.Norm(norm2, 1);
    double errorEstimated = norm1 / norm2;

    if (!X.IsReal() && A.IsReal() && errorEstimated <= toleranceDesired)
    {
        hwMatrix Xreal;
        X.UnpackComplex(&Xreal, nullptr);
        X = Xreal;
    }

    return status;
}
