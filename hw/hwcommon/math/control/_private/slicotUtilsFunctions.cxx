/**
* @file slicotUtilsFunctions.cxx
* @date December 2013
* Copyright (C) 2007-2018 Altair Engineering, Inc.
* This file is part of the OpenMatrix Language ("OpenMatrix") software.
* Open Source License Information:
* OpenMatrix is free software. You can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* OpenMatrix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Commercial License Information:
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/
#include <stdlib.h>
#include "slicotUtilsFunctions.h"
#include "slicot.h"
#include "hwMatrix.h"
#include <utl/hwString.h>

//---------------------------------------------------------------------------
hwMathStatus slicot_sb01bd( const hwString &DICO, const hwMatrix &ALPHA, hwMatrix &A, const hwMatrix &B, hwMatrix &WR, hwMatrix &WI, hwMatrix &NFP, hwMatrix &NAP, hwMatrix &NUP, hwMatrix &F, hwMatrix &Z, const hwMatrix &TOL, hwMatrix &IWARN, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &WR_out, hwMatrix &WI_out )
{
  hwMathStatus status;
// Input variables
  char *dico = (char*)DICO.c_str();
  double alpha = (double)ALPHA(0);
  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
  hwMatrix __B(B); /* copy matrix */
  double tol = (double)TOL(0);


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __A(A); /* copy matrix */

  if (WR.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!WR.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __WR(WR); /* copy matrix */

  if (WI.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!WI.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __WI(WI); /* copy matrix */

// Local variables
  int n = (int)(A.M());
  int m = (int)(B.N());
  int np = (int)(_max(WR.M(), WR.N()));
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1, n));
  int ldf = (int)(_max(1, m));
  int ldz = (int)(_max(1, n));
  hwMatrix __DWORK((int)1, (int)_max(_max(_max( 1,5*m),5*n),2*n+4*m), hwMatrix::REAL);
  int ldwork = (int)(_max(_max(_max( 1,5*m),5*n),2*n+4*m));
// Output variables
  int nfp = 0;
  int nap = 0;
  int nup = 0;
  hwMatrix __F((int)_max(1, m), (int)n, hwMatrix::REAL);
  hwMatrix __Z((int)_max(1, n), (int)n, hwMatrix::REAL);
  int iwarn = 0;
  int info = 0;

  try
  {
    sb01bd_( dico,&n,&m,&np,&alpha,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__WR.GetRealData(),__WI.GetRealData(),&nfp,&nap,&nup,__F.GetRealData(),&ldf,__Z.GetRealData(),&ldz,&tol,__DWORK.GetRealData(),&ldwork,&iwarn,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  WR_out = __WR;
  WI_out = __WI;
  status = NFP.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NFP(0) = (double)nfp;

  status = NAP.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NAP(0) = (double)nap;

  status = NUP.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NUP(0) = (double)nup;

  F = __F;
  Z = __Z;
  status = IWARN.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  IWARN(0) = (double)iwarn;

  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_ab01od( const hwString &STAGES, const hwString &JOBU, const hwString &JOBV, hwMatrix &A, hwMatrix &B, hwMatrix &U, hwMatrix &V, hwMatrix &NCONT, hwMatrix &INDCON, hwMatrix &KSTAIR, const hwMatrix &TOL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &U_out, hwMatrix &NCONT_out, hwMatrix &INDCON_out, hwMatrix &KSTAIR_out )
{
  hwMathStatus status;
// Input variables
  char *stages = (char*)STAGES.c_str();
  char *jobu = (char*)JOBU.c_str();
  char *jobv = (char*)JOBV.c_str();
  double tol = (double)TOL(0);


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __A(A); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __B(B); /* copy matrix */

  if (U.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!U.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __U(U); /* copy matrix */

  double _dncont = (double)NCONT(0);
  int ncont = (int)(_dncont);
  double _dindcon = (double)INDCON(0);
  int indcon = (int)(_dindcon);
  if (KSTAIR.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 10);
  if (!KSTAIR.IsReal()) return status(HW_MATH_ERR_COMPLEX, 10);
  hwMatrixI __IKSTAIR((int)A.M(), (int)1, hwMatrixI::REAL);
  for (int i = 0; i < (int)(A.M() * 1); i++)
  {
    __IKSTAIR(i) = (int)KSTAIR(i);
  }
// Local variables
  int n = (int)(A.M());
  int m = (int)(B.N());
  int lda = (int)(_max(1, A.M()));
  int ldb = (int)(_max(1, A.M()));
  int ldu = (int)(_max(1, A.M()));
  int ldv = (int)(_max(1, B.N()));
  hwMatrixI __IWORK((int)1, (int)2 * B.N(), hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)_max(1, _max(A.M(), B.N()) + _max(A.M(), 3*B.N())), hwMatrix::REAL);
  int ldwork = (int)(_max(1, _max(A.M(),B.N()) + _max(A.M(), 3*B.N())));
// Output variables
  hwMatrix __V((int)_max(1, B.N()), (int)B.N(), hwMatrix::REAL);
  int info = 0;

  try
  {
    ab01od_( stages,jobu,jobv,&n,&m,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__U.GetRealData(),&ldu,__V.GetRealData(),&ldv,&ncont,&indcon,__IKSTAIR.GetRealData(),&tol,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  B_out = __B;
  U_out = __U;
  status = NCONT_out.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NCONT_out(0) = (double)ncont;
  status = INDCON_out.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INDCON_out(0) = (double)indcon;
  status = KSTAIR_out.Dimension((int)(A.M() * 1), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(A.M() * 1); i++)
  {
    KSTAIR_out(i) = (double)__IKSTAIR(i);
  }
  V = __V;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_sb02md( const hwString &DICO, const hwString &HINV, const hwString &UPLO, const hwString &SCAL, const hwString &SORT, const hwMatrix &N, hwMatrix &A, const hwMatrix &G, hwMatrix &Q, hwMatrix &U, hwMatrix &INFO, hwMatrix &RCOND, hwMatrix &WR, hwMatrix &WI, hwMatrix &S, hwMatrix &A_out, hwMatrix &Q_out )
{
  hwMathStatus status;
// Input variables
  char *dico = (char*)DICO.c_str();
  char *hinv = (char*)HINV.c_str();
  char *uplo = (char*)UPLO.c_str();
  char *scal = (char*)SCAL.c_str();
  char *sort = (char*)SORT.c_str();
  double _dn = (double)N(0);
  int n = (int)(_dn);
  if (G.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!G.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __G(G); /* copy matrix */


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 8);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 8);
  hwMatrix __A(A); /* copy matrix */

  if (Q.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 9);
  if (!Q.IsReal()) return status(HW_MATH_ERR_COMPLEX, 9);
  hwMatrix __Q(Q); /* copy matrix */

// Local variables
  int lda = (int)(_max(1, n));
  int ldg = (int)(_max(1, n));
  int ldq = (int)(_max(1, n));
  int ldu = (int)(_max(1, 2 * n));
  int lds = (int)(_max(1, 2 * n));
  int iwork = 0;
  hwMatrix __DWORK((int)1, (int)_max(3,6*n), hwMatrix::REAL);
  int ldwork = (int)(_max(3,6*n));
  hwMatrixI __IBWORK((int)1, (int)2 * n, hwMatrixI::REAL);
// Output variables
  hwMatrix __U((int)2 * n, (int)2 * n, hwMatrix::REAL);
  int info = 0;
  double rcond = 0.;
  hwMatrix __WR((int)1, (int)2 * n, hwMatrix::REAL);
  hwMatrix __WI((int)1, (int)2 * n, hwMatrix::REAL);
  hwMatrix __S((int)2 * n, (int)2*n, hwMatrix::REAL);

  try
  {
    sb02md_( dico,hinv,uplo,scal,sort,&n,__A.GetRealData(),&lda,__G.GetRealData(),&ldg,__Q.GetRealData(),&ldq,__U.GetRealData(),&ldu,&info,&rcond,__WR.GetRealData(),__WI.GetRealData(),__S.GetRealData(),&lds,&iwork,__DWORK.GetRealData(),&ldwork,__IBWORK.GetRealData() );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  Q_out = __Q;
  U = __U;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;

  status = RCOND.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  RCOND(0) = (double)rcond;

  WR = __WR;
  WI = __WI;
  S = __S;

  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_mc01td( const hwString &DICO, hwMatrix &DP, const hwMatrix &P, hwMatrix &STABLE, hwMatrix &NZ, hwMatrix &IWARN, hwMatrix &INFO )
{
  hwMathStatus status;
// Input variables
  char *dico = (char*)DICO.c_str();
  if (P.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
  if (!P.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
// Input/Output variables
  int dp = (int)(P.Size());
// Output variables
  int stable = 0;
  int nz = 0;
  int iwarn = 0;
  int info = 0;
// Local variables
  hwMatrix __DWORK(1, 2*dp, hwMatrix::REAL);
  hwMatrix __Plocal(dp, hwMatrix::REAL);

  for (int i = 0; i < dp; ++i)  // change to increasing order
      __Plocal(i) = P(dp-1-i);
  
  --dp;  // decement to polynomial order

  try
  {
    mc01td_( dico,&dp,__Plocal.GetRealData(),&stable,&nz,__DWORK.GetRealData(),&iwarn,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  status = DP.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  DP(0) = (double)dp;

  status = STABLE.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  STABLE(0) = (double)stable;

  status = NZ.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NZ(0) = (double)nz;

  status = IWARN.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  IWARN(0) = (double)iwarn;

  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_mb02md( const hwString &JOB, const hwMatrix &M, const hwMatrix &N, const hwMatrix &L, hwMatrix &RANK, hwMatrix &C, hwMatrix &S, hwMatrix &X, const hwMatrix &TOL, hwMatrix &IWARN, hwMatrix &INFO, hwMatrix &RANK_out, hwMatrix &C_out )
{
  hwMathStatus status;
// Input variables
  char *job = (char*)JOB.c_str();
  double _dm = (double)M(0);
  int m = (int)(_dm);
  double _dn = (double)N(0);
  int n = (int)(_dn);
  double _dl = (double)L(0);
  int l = (int)(_dl);
  double tol = (double)TOL(0);


// Input/Output variables
  double _drank = (double)RANK(0);
  int rank = (int)(_drank);
  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __C(C); /* copy matrix */

// Local variables
  int ldc = (int)(_max(_max(1, m), n + l));
  int ldx = (int)(_max(1, n));
  hwMatrixI __IWORK((int)1, (int)l, hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)m *(n + l ) + _max( 3*_min(m, n + l) + _max(_max(m, n + l), 5*_min(m, n + l)), 3 * l ), hwMatrix::REAL);
  int ldwork = (int)(m *(n + l ) + _max( 3*_min(m, n + l) + _max(_max(m, n + l), 5*_min(m, n + l)), 3 * l ));
// Output variables
  hwMatrix __S((int)1, (int)_min(m, n+l), hwMatrix::REAL);
  hwMatrix __X((int)l, (int)_max(1, n), hwMatrix::REAL);
  int iwarn = 0;
  int info = 0;

  try
  {
    mb02md_( job,&m,&n,&l,&rank,__C.GetRealData(),&ldc,__S.GetRealData(),__X.GetRealData(),&ldx,&tol,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&iwarn,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  status = RANK_out.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  RANK_out(0) = (double)rank;
  C_out = __C;
  S = __S;
  X = __X;
  status = IWARN.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  IWARN(0) = (double)iwarn;

  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_mb02nd( const hwMatrix &M, const hwMatrix &N, const hwMatrix &L, hwMatrix &RANK, hwMatrix &THETA, hwMatrix &C, hwMatrix &X, hwMatrix &Q, hwMatrix &INUL, const hwMatrix &TOL, const hwMatrix &RELTOL, hwMatrix &IWARN, hwMatrix &INFO, hwMatrix &RANK_out, hwMatrix &THETA_out, hwMatrix &C_out )
{
  hwMathStatus status;
// Input variables
  double _dm = (double)M(0);
  int m = (int)(_dm);
  double _dn = (double)N(0);
  int n = (int)(_dn);
  double _dl = (double)L(0);
  int l = (int)(_dl);
  double tol = (double)TOL(0);
  double reltol = (double)RELTOL(0);


// Input/Output variables
  double _drank = (double)RANK(0);
  int rank = (int)(_drank);
  double theta = (double)THETA(0);
  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 8);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 8);
  hwMatrix __C(C); /* copy matrix */

// Local variables
  int ldc = (int)(_max(_max(1,m), n + l));
  int ldx = (int)(_max(1, n));
  hwMatrixI __IWORK((int)1, (int)n + 2 * l, hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)_max(_max(2, _max(m, n+l) + 2 * _min(m, n+l)), _min( m, n+l) + _max( ( n+l )*(n+l-1)/2, m*( n+l-( m-1 )/2 ) ) + _max( 6*(n+l)-5, l*l + _max( n+l, 3*l ) ) ) , hwMatrix::REAL);
  int ldwork = (int)(_max(_max(2, _max(m, n+l) + 2 * _min(m, n+l)), _min( m, n+l) + _max( ( n+l )*(n+l-1)/2, m*( n+l-( m-1 )/2 ) ) + _max( 6*(n+l)-5, l*l + _max( n+l, 3*l ) ) ) );
  hwMatrixI __IBWORK((int)1, (int)n+l, hwMatrixI::REAL);
// Output variables
  hwMatrix __X((int)l, (int)_max(1, n), hwMatrix::REAL);
  hwMatrix __Q((int)1, (int)2 * _min(m, n + l) - 1, hwMatrix::REAL);
  hwMatrixI __IINUL((int)n+l, (int)1, hwMatrixI::REAL);
  int iwarn = 0;
  int info = 0;

  try
  {
    mb02nd_( &m,&n,&l,&rank,&theta,__C.GetRealData(),&ldc,__X.GetRealData(),&ldx,__Q.GetRealData(),__IINUL.GetRealData(),&tol,&reltol,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,__IBWORK.GetRealData(),&iwarn,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  status = RANK_out.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  RANK_out(0) = (double)rank;
  status = THETA_out.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  THETA_out(0) = (double)theta;
  C_out = __C;
  X = __X;
  Q = __Q;
  status = INUL.Dimension((int)(n+l * 1), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(n+l * 1); i++)
  {
    INUL(i) = ((double)__IINUL(i) == 0.0) ? 0:1;
  }
  status = IWARN.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  IWARN(0) = (double)iwarn;

  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_mb02qd( const hwString &JOB, const hwString &INIPER, const hwMatrix &NRHS, const hwMatrix &RCOND, const hwMatrix &SVLMAX, hwMatrix &A, hwMatrix &B, const hwMatrix &Y, hwMatrix &JPVT, hwMatrix &RANK, hwMatrix &SVAL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &JPVT_out )
{
  hwMathStatus status;
// Input variables
  char *job = (char*)JOB.c_str();
  char *iniper = (char*)INIPER.c_str();
  double _dnrhs = (double)NRHS(0);
  int nrhs = (int)(_dnrhs);
  double rcond = (double)RCOND(0);
  double svlmax = (double)SVLMAX(0);
  if (Y.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!Y.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __Y(Y); /* copy matrix */


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __A(A); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 8);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 8);
  hwMatrix __B(B); /* copy matrix */

  if (JPVT.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 9);
  if (!JPVT.IsReal()) return status(HW_MATH_ERR_COMPLEX, 9);
  hwMatrixI __IJPVT((int)1, (int)A.N(), hwMatrixI::REAL);
  for (int i = 0; i < (int)(1 * A.N()); i++)
  {
    __IJPVT(i) = (int)JPVT(i);
  }
// Local variables
  int m = (int)(A.M());
  int n = (int)(A.N());
  int lda = (int)(_max(1, A.M()));
  int ldb = (int)(_max(1, _max(A.M(), A.N())));
  hwMatrix __DWORK((int)1, (int)_max(_min(A.M(), A.N()) + 3 * A.N() + 1, 2* _min(A.M(), A.N()) + nrhs), hwMatrix::REAL);
  int ldwork = (int)(_max(_min(A.M(), A.N()) + 3 * A.N() + 1, 2* _min(A.M(), A.N()) + nrhs));
// Output variables
  int rank = 0;
  hwMatrix __SVAL((int)1, (int)3, hwMatrix::REAL);
  int info = 0;

  try
  {
    mb02qd_( job,iniper,&m,&n,&nrhs,&rcond,&svlmax,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__Y.GetRealData(),__IJPVT.GetRealData(),&rank,__SVAL.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  B_out = __B;
  status = JPVT_out.Dimension((int)(1 * A.N()), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * A.N()); i++)
  {
    JPVT_out(i) = (double)__IJPVT(i);
  }
  status = RANK.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  RANK(0) = (double)rank;

  SVAL = __SVAL;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_mb03od( const hwString &JOBQR, hwMatrix &A, hwMatrix &JPVT, const hwMatrix &RCOND, const hwMatrix &SVLMAX, hwMatrix &TAU, hwMatrix &RANK, hwMatrix &SVAL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &JPVT_out )
{
  hwMathStatus status;
// Input variables
  char *jobqr = (char*)JOBQR.c_str();
  double rcond = (double)RCOND(0);
  double svlmax = (double)SVLMAX(0);


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __A(A); /* copy matrix */

  if (JPVT.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!JPVT.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrixI __IJPVT((int)1, (int)A.N(), hwMatrixI::REAL);
  for (int i = 0; i < (int)(1 * A.N()); i++)
  {
    __IJPVT(i) = (int)JPVT(i);
  }
// Local variables
  int m = (int)(A.M());
  int n = (int)(A.N());
  int lda = (int)(_max(1, A.M()));
  hwMatrix __DWORK((int)1, (int)3 * A.N() + 1, hwMatrix::REAL);
  int ldwork = (int)(3 * A.N() + 1);
// Output variables
  hwMatrix __TAU((int)1, (int)_min(A.M(), A.N()), hwMatrix::REAL);
  int rank = 0;
  hwMatrix __SVAL((int)1, (int)3, hwMatrix::REAL);
  int info = 0;

  try
  {
    mb03od_( jobqr,&m,&n,__A.GetRealData(),&lda,__IJPVT.GetRealData(),&rcond,&svlmax,__TAU.GetRealData(),&rank,__SVAL.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  status = JPVT_out.Dimension((int)(1 * A.N()), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * A.N()); i++)
  {
    JPVT_out(i) = (double)__IJPVT(i);
  }
  TAU = __TAU;
  status = RANK.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  RANK(0) = (double)rank;

  SVAL = __SVAL;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_mb03pd( const hwString &JOBRQ, hwMatrix &A, hwMatrix &JPVT, const hwMatrix &RCOND, const hwMatrix &SVLMAX, hwMatrix &TAU, hwMatrix &RANK, hwMatrix &SVAL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &JPVT_out )
{
  hwMathStatus status;
// Input variables
  char *jobrq = (char*)JOBRQ.c_str();
  double rcond = (double)RCOND(0);
  double svlmax = (double)SVLMAX(0);


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __A(A); /* copy matrix */

  if (JPVT.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!JPVT.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrixI __IJPVT((int)1, (int)A.M(), hwMatrixI::REAL);
  for (int i = 0; i < (int)(1 * A.M()); i++)
  {
    __IJPVT(i) = (int)JPVT(i);
  }
// Local variables
  int m = (int)(A.M());
  int n = (int)(A.N());
  int lda = (int)(_max(1, A.M()));
  hwMatrix __DWORK((int)1, (int)3 * A.M(), hwMatrix::REAL);
// Output variables
  hwMatrix __TAU((int)1, (int)_min(A.M(), A.N()), hwMatrix::REAL);
  int rank = 0;
  hwMatrix __SVAL((int)1, (int)3, hwMatrix::REAL);
  int info = 0;

  try
  {
    mb03pd_( jobrq,&m,&n,__A.GetRealData(),&lda,__IJPVT.GetRealData(),&rcond,&svlmax,__TAU.GetRealData(),&rank,__SVAL.GetRealData(),__DWORK.GetRealData(),&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  status = JPVT_out.Dimension((int)(1 * A.M()), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * A.M()); i++)
  {
    JPVT_out(i) = (double)__IJPVT(i);
  }
  TAU = __TAU;
  status = RANK.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  RANK(0) = (double)rank;

  SVAL = __SVAL;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_mb04gd( hwMatrix &A, hwMatrix &JPVT, hwMatrix &TAU, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &JPVT_out )
{
  hwMathStatus status;
// Input variables


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 1);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 1);
  hwMatrix __A(A); /* copy matrix */

  if (JPVT.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
  if (!JPVT.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
  hwMatrixI __IJPVT((int)1, (int)_max(1, A.M()), hwMatrixI::REAL);
  for (int i = 0; i < (int)(1 * _max(1, A.M())); i++)
  {
    __IJPVT(i) = (int)JPVT(i);
  }
// Local variables
  int m = (int)(A.M());
  int n = (int)(A.N());
  int lda = (int)(_max(1, A.M()));
  hwMatrix __DWORK((int)1, (int)3 * A.M(), hwMatrix::REAL);
// Output variables
  hwMatrix __TAU((int)1, (int)_min(A.M(), A.N()), hwMatrix::REAL);
  int info = 0;

  try
  {
    mb04gd_( &m,&n,__A.GetRealData(),&lda,__IJPVT.GetRealData(),__TAU.GetRealData(),__DWORK.GetRealData(),&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  status = JPVT_out.Dimension((int)(1 * _max(1, A.M())), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * _max(1, A.M())); i++)
  {
    JPVT_out(i) = (double)__IJPVT(i);
  }
  TAU = __TAU;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_mb04md( hwMatrix &MAXRED, hwMatrix &A, hwMatrix &SCALE, hwMatrix &INFO, hwMatrix &MAXRED_out, hwMatrix &A_out )
{
  hwMathStatus status;
// Input variables


// Input/Output variables
  double maxred = (double)MAXRED(0);
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
  hwMatrix __A(A); /* copy matrix */

// Local variables
  int n = (int)(A.N());
  int lda = (int)(_max(1,A.N()));
// Output variables
  hwMatrix __SCALE((int)1, (int)A.N(), hwMatrix::REAL);
  int info = 0;

  try
  {
    mb04md_( &n,&maxred,__A.GetRealData(),&lda,__SCALE.GetRealData(),&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  status = MAXRED_out.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  MAXRED_out(0) = (double)maxred;
  A_out = __A;
  SCALE = __SCALE;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_ab08nd( const hwString &EQUIL, const hwMatrix &N, const hwMatrix &M, const hwMatrix &P, const hwMatrix &A, const hwMatrix &B, const hwMatrix &C, const hwMatrix &D, hwMatrix &NU, hwMatrix &RANK, hwMatrix &DINFZ, hwMatrix &NKROR, hwMatrix &NKROL, hwMatrix &INFZ, hwMatrix &KRONR, hwMatrix &KRONL, hwMatrix &AF, hwMatrix &BF, const hwMatrix &TOL, hwMatrix &INFO )
{
  hwMathStatus status;
// Input variables
  char *equil = (char*)EQUIL.c_str();
  double _dn = (double)N(0);
  int n = (int)(_dn);
  double _dm = (double)M(0);
  int m = (int)(_dm);
  double _dp = (double)P(0);
  int p = (int)(_dp);
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __A(A); /* copy matrix */
  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __B(B); /* copy matrix */
  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __C(C); /* copy matrix */
  if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 8);
  if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 8);
  hwMatrix __D(D); /* copy matrix */
  double tol = (double)TOL(0);


// Input/Output variables
// Local variables
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1, n));
  int ldc = (int)(_max(1, p));
  int ldd = (int)(_max(1, p));
  int ldaf = (int)(_max(1, n + m));
  int ldbf = (int)(_max(1, n + p));
  hwMatrixI __IWORK((int)1, (int)_max(m, p), hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)_max(_max(m, p), n) + _max(3 * _max(m, p) - 1, n + _max(m, p)), hwMatrix::REAL);
  int ldwork = (int)(_max(_max(m, p), n) + _max(3 * _max(m, p) - 1, n + _max(m, p)));
// Output variables
  int nu = 0;
  int rank = 0;
  int dinfz = 0;
  int nkror = 0;
  int nkrol = 0;
  hwMatrixI __INFZ((int)1, (int)n, hwMatrixI::REAL);
  hwMatrixI __KRONR((int)1, (int)_max(n, m) + 1, hwMatrixI::REAL);
  hwMatrixI __KRONL((int)1, (int)_max(n, m) + 1, hwMatrixI::REAL);
  hwMatrix __AF((int)_max(1, n + m), (int)n + _min(p, m), hwMatrix::REAL);
  hwMatrix __BF((int)n + m, (int)_max(1, n + p), hwMatrix::REAL);
  int info = 0;

  try
  {
    ab08nd_( equil,&n,&m,&p,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__D.GetRealData(),&ldd,&nu,&rank,&dinfz,&nkror,&nkrol,__INFZ.GetRealData(),__KRONR.GetRealData(),__KRONL.GetRealData(),__AF.GetRealData(),&ldaf,__BF.GetRealData(),&ldbf,&tol,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  status = NU.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NU(0) = (double)nu;

  status = RANK.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  RANK(0) = (double)rank;

  status = DINFZ.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  DINFZ(0) = (double)dinfz;

  status = NKROR.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NKROR(0) = (double)nkror;

  status = NKROL.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NKROL(0) = (double)nkrol;

  status = INFZ.Dimension((int)(1 * n), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * n); i++)
  {
    INFZ(i) = (double)__INFZ(i);
  }
  status = KRONR.Dimension((int)(1 * _max(n, m) + 1), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * _max(n, m) + 1); i++)
  {
    KRONR(i) = (double)__KRONR(i);
  }
  status = KRONL.Dimension((int)(1 * _max(n, m) + 1), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * _max(n, m) + 1); i++)
  {
    KRONL(i) = (double)__KRONL(i);
  }
  AF = __AF;
  BF = __BF;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_ab07nd( hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &D, hwMatrix &RCOND, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out, hwMatrix &D_out )
{
  hwMathStatus status;
// Input variables


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 1);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 1);
  hwMatrix __A(A); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
  hwMatrix __B(B); /* copy matrix */

  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
  hwMatrix __C(C); /* copy matrix */

  if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __D(D); /* copy matrix */

// Local variables
  int n = (int)(A.M());
  int m = (int)(B.N());
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1,n));
  int ldc = (int)(_max(1, m));
  int ldd = (int)(_max(1, m));
  hwMatrixI __IWORK((int)1, (int)2 * m, hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)_max(1, 4 * m), hwMatrix::REAL);
  int ldwork = (int)(_max(1,4 * m));
// Output variables
  double rcond = 0.;
  int info = 0;

  try
  {
    ab07nd_( &n,&m,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__D.GetRealData(),&ldd,&rcond,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  B_out = __B;
  C_out = __C;
  D_out = __D;
  status = RCOND.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  RCOND(0) = (double)rcond;

  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_ag07bd( const hwString &JOBE, const hwMatrix &A, const hwMatrix &E, const hwMatrix &B, const hwMatrix &C, const hwMatrix &D, hwMatrix &AI, hwMatrix &EI, hwMatrix &BI, hwMatrix &CI, hwMatrix &DI, hwMatrix &INFO )
{
  hwMathStatus status;
// Input variables
  char *jobe = (char*)JOBE.c_str();
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
  hwMatrix __A(A); /* copy matrix */
  if (E.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
  if (!E.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
  hwMatrix __E(E); /* copy matrix */
  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __B(B); /* copy matrix */
  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __C(C); /* copy matrix */
  if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __D(D); /* copy matrix */


// Input/Output variables
// Local variables
  int n = (int)(B.N());
  int m = (int)(B.M());
  int lda = (int)(_max(1, n));
  int lde = (int)(_max(1, n));
  int ldb = (int)(_max(1, n));
  int ldc = (int)(_max(1, m));
  int ldd = (int)(_max(1, m));
  int ldai = (int)(_max(1, n + m));
  int ldei = (int)(_max(1, n+m));
  int ldbi = (int)(_max(1, n + m));
  int ldci = (int)(_max(1, m));
  int lddi = (int)(_max(1, m));
// Output variables
  hwMatrix __AI((int)_max(1, n + m), (int)n + m, hwMatrix::REAL);
  hwMatrix __EI((int)_max(1, n+m), (int)n + m, hwMatrix::REAL);
  hwMatrix __BI((int)m, (int)_max(1, n + m), hwMatrix::REAL);
  hwMatrix __CI((int)n+m, (int)_max(1, m), hwMatrix::REAL);
  hwMatrix __DI((int)_max(1, m), (int)m, hwMatrix::REAL);
  int info = 0;

  try
  {
    ag07bd_( jobe,&n,&m,__A.GetRealData(),&lda,__E.GetRealData(),&lde,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__D.GetRealData(),&ldd,__AI.GetRealData(),&ldai,__EI.GetRealData(),&ldei,__BI.GetRealData(),&ldbi,__CI.GetRealData(),&ldci,__DI.GetRealData(),&lddi,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  AI = __AI;
  EI = __EI;
  BI = __BI;
  CI = __CI;
  DI = __DI;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_ag08bd( const hwString &EQUIL, const hwMatrix &M, const hwMatrix &P, hwMatrix &A, hwMatrix &E, const hwMatrix &B, const hwMatrix &C, const hwMatrix &D, hwMatrix &NFZ, hwMatrix &NRANK, hwMatrix &NIZ, hwMatrix &DINFZ, hwMatrix &NKROR, hwMatrix &NINFE, hwMatrix &NKROL, hwMatrix &INFZ, hwMatrix &KRONR, hwMatrix &INFE, hwMatrix &KRONL, const hwMatrix &TOL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &E_out )
{
  hwMathStatus status;
// Input variables
  char *equil = (char*)EQUIL.c_str();
  double _dm = (double)M(0);
  int m = (int)(_dm);
  double _dp = (double)P(0);
  int p = (int)(_dp);
  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __B(B); /* copy matrix */
  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __C(C); /* copy matrix */
  if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __D(D); /* copy matrix */
  double tol = (double)TOL(0);


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 8);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 8);
  hwMatrix __A(A); /* copy matrix */

  if (E.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 9);
  if (!E.IsReal()) return status(HW_MATH_ERR_COMPLEX, 9);
  hwMatrix __E(E); /* copy matrix */

// Local variables
  int l = (int)(A.M());
  int n = (int)(A.N());
  int lda = (int)(_max(1, l));
  int lde = (int)(_max(1, l));
  int ldb = (int)(_max(1, l));
  int ldc = (int)(_max(1, p));
  int ldd = (int)(_max(1, p));
  hwMatrixI __IWORK((int)1, (int)n + _max(1, m), hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)_max( 4*(l + n), _max(p + l, m + n) * _max(p + l, m + n) + _max(1, 5 * _max(p+l,m+n))), hwMatrix::REAL);
  int ldwork = (int)(_max( 4*(l + n), _max(p + l, m + n) * _max(p + l, m + n) + _max(1, 5 * _max(p+l,m+n))));
// Output variables
  int nfz = 0;
  int nrank = 0;
  int niz = 0;
  int dinfz = 0;
  int nkror = 0;
  int ninfe = 0;
  int nkrol = 0;
  hwMatrixI __INFZ((int)1, (int)n + 1, hwMatrixI::REAL);
  hwMatrixI __KRONR((int)1, (int)n + m + 1, hwMatrixI::REAL);
  hwMatrixI __INFE((int)1, (int)1 + _min(l + p, n + m), hwMatrixI::REAL);
  hwMatrixI __KRONL((int)1, (int)l+p+1, hwMatrixI::REAL);
  int info = 0;

  try
  {
    ag08bd_( equil,&l,&n,&m,&p,__A.GetRealData(),&lda,__E.GetRealData(),&lde,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__D.GetRealData(),&ldd,&nfz,&nrank,&niz,&dinfz,&nkror,&ninfe,&nkrol,__INFZ.GetRealData(),__KRONR.GetRealData(),__INFE.GetRealData(),__KRONL.GetRealData(),&tol,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  E_out = __E;
  status = NFZ.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NFZ(0) = (double)nfz;

  status = NRANK.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NRANK(0) = (double)nrank;

  status = NIZ.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NIZ(0) = (double)niz;

  status = DINFZ.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  DINFZ(0) = (double)dinfz;

  status = NKROR.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NKROR(0) = (double)nkror;

  status = NINFE.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NINFE(0) = (double)ninfe;

  status = NKROL.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NKROL(0) = (double)nkrol;

  status = INFZ.Dimension((int)(1 * n + 1), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * n + 1); i++)
  {
    INFZ(i) = (double)__INFZ(i);
  }
  status = KRONR.Dimension((int)(1 * n + m + 1), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * n + m + 1); i++)
  {
    KRONR(i) = (double)__KRONR(i);
  }
  status = INFE.Dimension((int)(1 * 1 + _min(l + p, n + m)), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * 1 + _min(l + p, n + m)); i++)
  {
    INFE(i) = (double)__INFE(i);
  }
  status = KRONL.Dimension((int)(1 * l+p+1), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(1 * l+p+1); i++)
  {
    KRONL(i) = (double)__KRONL(i);
  }
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_sb03md( const hwString &DICO, const hwString &JOB, const hwString &FACT, const hwString &TRANA, hwMatrix &A, hwMatrix &U, hwMatrix &C, hwMatrix &SCALE, hwMatrix &SEP, hwMatrix &FERR, hwMatrix &WR, hwMatrix &WI, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &U_out, hwMatrix &C_out )
{
  hwMathStatus status;
// Input variables
  char *dico = (char*)DICO.c_str();
  char *job = (char*)JOB.c_str();
  char *fact = (char*)FACT.c_str();
  char *trana = (char*)TRANA.c_str();


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __A(A); /* copy matrix */

  if (U.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!U.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __U(U); /* copy matrix */

  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __C(C); /* copy matrix */

// Local variables
  int n = (int)(A.M());
  int lda = (int)(_max(1, n));
  int ldu = (int)(_max(1, n));
  int ldc = (int)(_max(1, n));
  hwMatrixI __IWORK((int)n, (int)n, hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)_max (2*n*n + 2*n, 3*n), hwMatrix::REAL);
  int ldwork = (int)(_max (2*n*n + 2*n, 3*n));
// Output variables
  double scale = 0.;
  double sep = 0.;
  double ferr = 0.;
  hwMatrix __WR((int)n, (int)1, hwMatrix::REAL);
  hwMatrix __WI((int)n, (int)1, hwMatrix::REAL);
  int info = 0;

  try
  {
    sb03md_( dico,job,fact,trana,&n,__A.GetRealData(),&lda,__U.GetRealData(),&ldu,__C.GetRealData(),&ldc,&scale,&sep,&ferr,__WR.GetRealData(),__WI.GetRealData(),__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  U_out = __U;
  C_out = __C;
  status = SCALE.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  SCALE(0) = (double)scale;

  status = SEP.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  SEP(0) = (double)sep;

  status = FERR.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  FERR(0) = (double)ferr;

  WR = __WR;
  WI = __WI;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_sg03ad( const hwString &DICO, const hwString &JOB, const hwString &FACT, const hwString &TRANS, const hwString &UPLO, hwMatrix &A, hwMatrix &E, hwMatrix &Q, hwMatrix &Z, hwMatrix &X, hwMatrix &SCALE, hwMatrix &SEP, hwMatrix &FERR, hwMatrix &ALPHAR, hwMatrix &ALPHAI, hwMatrix &BETA, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &E_out, hwMatrix &Q_out, hwMatrix &Z_out, hwMatrix &X_out )
{
  hwMathStatus status;
// Input variables
  char *dico = (char*)DICO.c_str();
  char *job = (char*)JOB.c_str();
  char *fact = (char*)FACT.c_str();
  char *trans = (char*)TRANS.c_str();
  char *uplo = (char*)UPLO.c_str();


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __A(A); /* copy matrix */

  if (E.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!E.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __E(E); /* copy matrix */

  if (Q.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 8);
  if (!Q.IsReal()) return status(HW_MATH_ERR_COMPLEX, 8);
  hwMatrix __Q(Q); /* copy matrix */

  if (Z.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 9);
  if (!Z.IsReal()) return status(HW_MATH_ERR_COMPLEX, 9);
  hwMatrix __Z(Z); /* copy matrix */

  if (X.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 10);
  if (!X.IsReal()) return status(HW_MATH_ERR_COMPLEX, 10);
  hwMatrix __X(X); /* copy matrix */

// Local variables
  int n = (int)(A.M());
  int lda = (int)(_max(1, n));
  int lde = (int)(_max(1,n));
  int ldq = (int)(_max(1,n));
  int ldz = (int)(_max(1,n));
  int ldx = (int)(_max(1,n));
  hwMatrixI __IWORK((int)1, (int)n * n, hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)_max(1, _max(2 * n * n, 4 * n)), hwMatrix::REAL);
  int ldwork = (int)(_max(1, _max(2 * n * n, 4 * n)));
// Output variables
  double scale = 0.;
  double sep = 0.;
  double ferr = 0.;
  hwMatrix __ALPHAR((int)n, (int)1, hwMatrix::REAL);
  hwMatrix __ALPHAI((int)n, (int)1, hwMatrix::REAL);
  hwMatrix __BETA((int)n, (int)1, hwMatrix::REAL);
  int info = 0;

  try
  {
    sg03ad_( dico,job,fact,trans,uplo,&n,__A.GetRealData(),&lda,__E.GetRealData(),&lde,__Q.GetRealData(),&ldq,__Z.GetRealData(),&ldz,__X.GetRealData(),&ldx,&scale,&sep,&ferr,__ALPHAR.GetRealData(),__ALPHAI.GetRealData(),__BETA.GetRealData(),__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  E_out = __E;
  Q_out = __Q;
  Z_out = __Z;
  X_out = __X;
  status = SCALE.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  SCALE(0) = (double)scale;

  status = SEP.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  SEP(0) = (double)sep;

  status = FERR.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  FERR(0) = (double)ferr;

  ALPHAR = __ALPHAR;
  ALPHAI = __ALPHAI;
  BETA = __BETA;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_sb04qd( hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &Z, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out )
{
  hwMathStatus status;
// Input variables


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 1);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 1);
  hwMatrix __A(A); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
  hwMatrix __B(B); /* copy matrix */

  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
  hwMatrix __C(C); /* copy matrix */

// Local variables
  int n = (int)(A.N());
  int m = (int)(B.M());
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1, m));
  int ldc = (int)(_max(1, m));
  int ldz = (int)(_max(1, m));
  hwMatrixI __IWORK((int)1, (int)4 * n, hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)_max(_max(_max(1, 2*n*n + 9*n), 5*m), n + m), hwMatrix::REAL);
  int ldwork = (int)(_max(_max(_max(1, 2*n*n + 9*n), 5*m), n + m));
// Output variables
  hwMatrix __Z((int)_max(1, m), (int)m, hwMatrix::REAL);
  int info = 0;

  try
  {
    sb04qd_( &n,&m,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__Z.GetRealData(),&ldz,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  B_out = __B;
  C_out = __C;
  Z = __Z;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_sb04md( hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &Z, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out )
{
  hwMathStatus status;
// Input variables


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 1);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 1);
  hwMatrix __A(A); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
  hwMatrix __B(B); /* copy matrix */

  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
  hwMatrix __C(C); /* copy matrix */

// Local variables
  int n = (int)(A.N());
  int m = (int)(B.M());
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1, m));
  int ldc = (int)(_max(1, n));
  int ldz = (int)(_max(1, m));
  hwMatrixI __IWORK((int)4*n, (int)1, hwMatrixI::REAL);
  hwMatrix __DWORK((int)_max(_max(_max(1, 2*n*n + 8*n), 5*m), n + m), (int)1, hwMatrix::REAL);
  int ldwork = (int)(_max(_max(_max(1, 2*n*n + 8*n), 5*m), n + m));
// Output variables
  hwMatrix __Z((int)_max(1, m), (int)m, hwMatrix::REAL);
  int info = 0;

  try
  {
    sb04md_( &n,&m,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__Z.GetRealData(),&ldz,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  B_out = __B;
  C_out = __C;
  Z = __Z;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_tb01ud( const hwString &JOBZ, hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &NCONT, hwMatrix &INDCON, hwMatrix &NBLK, hwMatrix &Z, hwMatrix &TAU, const hwMatrix &TOL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out )
{
  hwMathStatus status;
// Input variables
  char *jobz = (char*)JOBZ.c_str();
  double tol = (double)TOL(0);


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
  hwMatrix __A(A); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __B(B); /* copy matrix */

  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __C(C); /* copy matrix */

// Local variables
  int n = (int)(A.M());
  int m = (int)(B.N());
  int p = (int)(C.M());
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1, n));
  int ldc = (int)(_max(1, p));
  int ldz = (int)(_max(1, n));
  hwMatrixI __IWORK((int)m, (int)1, hwMatrixI::REAL);
  hwMatrix __DWORK((int)_max(_max(_max(1, n), 3*m), p), (int)1, hwMatrix::REAL);
  int ldwork = (int)(_max(_max(_max(1, n), 3*m), p));
// Output variables
  int ncont = 0;
  int indcon = 0;
  hwMatrixI __NBLK((int)n, (int)1, hwMatrixI::REAL);
  hwMatrix __Z((int)_max(1, n), (int)n, hwMatrix::REAL);
  hwMatrix __TAU((int)1, (int)n, hwMatrix::REAL);
  int info = 0;

  try
  {
    tb01ud_( jobz,&n,&m,&p,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,&ncont,&indcon,__NBLK.GetRealData(),__Z.GetRealData(),&ldz,__TAU.GetRealData(),&tol,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  B_out = __B;
  C_out = __C;
  status = NCONT.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NCONT(0) = (double)ncont;

  status = INDCON.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INDCON(0) = (double)indcon;

  status = NBLK.Dimension((int)(n * 1), 1, hwMatrix::REAL);
  for (int i = 0; i < (int)(n * 1); i++)
  {
    NBLK(i) = (double)__NBLK(i);
  }
  Z = __Z;
  TAU = __TAU;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_sb03od( const hwString &DICO, const hwString &FACT, const hwString &TRANS, hwMatrix &A, hwMatrix &Q, hwMatrix &B, hwMatrix &SCALE, hwMatrix &WR, hwMatrix &WI, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &Q_out, hwMatrix &B_out )
{
  hwMathStatus status;
// Input variables
  char *dico = (char*)DICO.c_str();
  char *fact = (char*)FACT.c_str();
  char *trans = (char*)TRANS.c_str();


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __A(A); /* copy matrix */

  if (Q.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!Q.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __Q(Q); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __B(B); /* copy matrix */

// Local variables
  int n = (int)(B.N());
  int m = (int)(B.M());
  int lda = (int)(_max(1, n));
  int ldq = (int)(_max(1, n));
  int ldb = (int)(_max(1, _max(n, m)));
  hwMatrix __DWORK((int)1, (int)_max(1, 4 * n + _min(m, n)), hwMatrix::REAL);
  int ldwork = (int)(_max(1, 4 * n + _min(m, n)));
// Output variables
  double scale = 0.;
  hwMatrix __WR((int)1, (int)n, hwMatrix::REAL);
  hwMatrix __WI((int)1, (int)n, hwMatrix::REAL);
  int info = 0;

  try
  {
    sb03od_( dico,fact,trans,&n,&m,__A.GetRealData(),&lda,__Q.GetRealData(),&ldq,__B.GetRealData(),&ldb,&scale,__WR.GetRealData(),__WI.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  Q_out = __Q;
  B_out = __B;
  status = SCALE.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  SCALE(0) = (double)scale;

  WR = __WR;
  WI = __WI;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_sg03bd( const hwString &DICO, const hwString &FACT, const hwString &TRANS, hwMatrix &A, hwMatrix &E, hwMatrix &Q, hwMatrix &Z, hwMatrix &B, hwMatrix &SCALE, hwMatrix &ALPHAR, hwMatrix &ALPHAI, hwMatrix &BETA, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &E_out, hwMatrix &Q_out, hwMatrix &Z_out, hwMatrix &B_out )
{
  hwMathStatus status;
// Input variables
  char *dico = (char*)DICO.c_str();
  char *fact = (char*)FACT.c_str();
  char *trans = (char*)TRANS.c_str();


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __A(A); /* copy matrix */

  if (E.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!E.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __E(E); /* copy matrix */

  if (Q.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!Q.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __Q(Q); /* copy matrix */

  if (Z.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!Z.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __Z(Z); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 8);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 8);
  hwMatrix __B(B); /* copy matrix */

// Local variables
  int n = (int)(A.M());
  int m = (int)(B.M());
  int lda = (int)(_max(1,n));
  int lde = (int)(_max(1, n));
  int ldq = (int)(_max(1, n));
  int ldz = (int)(_max(1, n));
  int ldb = (int)(_max(1, _max(m, n)));
  hwMatrix __DWORK((int)1, (int)_max(1, _max(4*n, 6*n - 6)), hwMatrix::REAL);
  int ldwork = (int)(_max(1, _max(4*n, 6*n - 6)));
// Output variables
  double scale = 0.;
  hwMatrix __ALPHAR((int)1, (int)n, hwMatrix::REAL);
  hwMatrix __ALPHAI((int)1, (int)n, hwMatrix::REAL);
  hwMatrix __BETA((int)1, (int)n, hwMatrix::REAL);
  int info = 0;

  try
  {
    sg03bd_( dico,fact,trans,&n,&m,__A.GetRealData(),&lda,__E.GetRealData(),&lde,__Q.GetRealData(),&ldq,__Z.GetRealData(),&ldz,__B.GetRealData(),&ldb,&scale,__ALPHAR.GetRealData(),__ALPHAI.GetRealData(),__BETA.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  E_out = __E;
  Q_out = __Q;
  Z_out = __Z;
  B_out = __B;
  status = SCALE.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  SCALE(0) = (double)scale;

  ALPHAR = __ALPHAR;
  ALPHAI = __ALPHAI;
  BETA = __BETA;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_mb05od( const hwString &BALANC, const hwMatrix &NDIAG, const hwMatrix &DELTA, hwMatrix &A, hwMatrix &MDIG, hwMatrix &IDIG, hwMatrix &IWARN, hwMatrix &INFO, hwMatrix &A_out )
{
  hwMathStatus status;
// Input variables
  char *balanc = (char*)BALANC.c_str();
  double _dndiag = (double)NDIAG(0);
  int ndiag = (int)(_dndiag);
  double delta = (double)DELTA(0);


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __A(A); /* copy matrix */

// Local variables
  int n = (int)(A.N());
  int lda = (int)(_max(1, n));
  hwMatrixI __IWORK((int)1, (int)n, hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)n * (2 * n + ndiag + 1) + ndiag, hwMatrix::REAL);
  int ldwork = (int)(n * (2 * n + ndiag + 1) + ndiag);
// Output variables
  int mdig = 0;
  int idig = 0;
  int iwarn = 0;
  int info = 0;

  try
  {
    mb05od_( balanc,&n,&ndiag,&delta,__A.GetRealData(),&lda,&mdig,&idig,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&iwarn,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  status = MDIG.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  MDIG(0) = (double)mdig;

  status = IDIG.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  IDIG(0) = (double)idig;

  status = IWARN.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  IWARN(0) = (double)iwarn;

  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_ab04md( const hwString &TYPE, const hwMatrix &ALPHA, const hwMatrix &BETA, hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &D, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out, hwMatrix &D_out )
{
  hwMathStatus status;
// Input variables
  char *type = (char*)TYPE.c_str();
  double alpha = (double)ALPHA(0);
  double beta = (double)BETA(0);


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __A(A); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __B(B); /* copy matrix */

  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __C(C); /* copy matrix */

  if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __D(D); /* copy matrix */

// Local variables
  int n = (int)(A.M());
  int m = (int)(B.N());
  int p = (int)(C.M());
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1, n));
  int ldc = (int)(_max(1, p));
  int ldd = (int)(_max(1, p));
  hwMatrixI __IWORK((int)1, (int)n, hwMatrixI::REAL);
  hwMatrix __DWORK((int)1, (int)_max(1, n), hwMatrix::REAL);
  int ldwork = (int)(_max(1, n));
// Output variables
  int info = 0;

  try
  {
    ab04md_( type,&n,&m,&p,&alpha,&beta,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__D.GetRealData(),&ldd,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  B_out = __B;
  C_out = __C;
  D_out = __D;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_sb10jd( hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &D, hwMatrix &E, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out, hwMatrix &D_out)
{
  hwMathStatus status;
// Input variables


// Input/Output variables
  //if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 1);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 1);
  hwMatrix __A(A); /* copy matrix */

  //if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 2);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 2);
  hwMatrix __B(B); /* copy matrix */

  //if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
  hwMatrix __C(C); /* copy matrix */

  //if (D.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!D.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __D(D); /* copy matrix */

  //if (E.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!E.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __E(E); /* copy matrix */

// Local variables
  int n = (int)(A.N());
  int m = (int)(B.N());
  int np = (int)(C.M());
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1, n));
  int ldc = (int)(_max(1, np));
  int ldd = (int)(_max(1, np));
  int lde = (int)(_max(1, n));
  hwMatrix __DWORK((int)_max( 1, 2 * n * n + 2 * n + n * _max( 5, n + m + np ) ), (int)1, hwMatrix::REAL);
  int ldwork = (int)(_max( 1, 2 * n * n + 2 * n + n * _max( 5, n + m + np ) ));
// Output variables
  int nsys = 0;
  int info = 0;

  try
  {
    sb10jd_( &n,&m,&np,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__D.GetRealData(),&ldd,__E.GetRealData(),&lde,&nsys,__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  __A.Resize(nsys,nsys);
  __B.Resize(nsys,m);
  __C.Resize(np,nsys);
  __D.Resize(np,m);
  A_out = __A;
  B_out = __B;
  C_out = __C;
  D_out = __D;

  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_sg02ad( const hwString &DICO, const hwString &JOBB, const hwString &FACT, const hwString &UPLO, const hwString &JOBL, const hwString &SCAL, const hwString &SORT, const hwString &ACC, const hwMatrix &P, const hwMatrix &A, const hwMatrix &E, const hwMatrix &B, const hwMatrix &Q, const hwMatrix &R, const hwMatrix &L, hwMatrix &RCONDU, hwMatrix &X, hwMatrix &ALFAR, hwMatrix &ALFAI, hwMatrix &BETA, hwMatrix &S, hwMatrix &T, hwMatrix &U, const hwMatrix &TOL, hwMatrix &IWARN, hwMatrix &INFO )
{
  hwMathStatus status;
// Input variables
  char *dico = (char*)DICO.c_str();
  char *jobb = (char*)JOBB.c_str();
  char *fact = (char*)FACT.c_str();
  char *uplo = (char*)UPLO.c_str();
  char *jobl = (char*)JOBL.c_str();
  char *scal = (char*)SCAL.c_str();
  char *sort = (char*)SORT.c_str();
  char *acc = (char*)ACC.c_str();
  double _dp = (double)P(0);
  int p = (int)(_dp);
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 10);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 10);
  hwMatrix __A(A); /* copy matrix */
  if (E.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 11);
  if (!E.IsReal()) return status(HW_MATH_ERR_COMPLEX, 11);
  hwMatrix __E(E); /* copy matrix */
  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 12);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 12);
  hwMatrix __B(B); /* copy matrix */
  if (Q.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 13);
  if (!Q.IsReal()) return status(HW_MATH_ERR_COMPLEX, 13);
  hwMatrix __Q(Q); /* copy matrix */
  if (R.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 14);
  if (!R.IsReal()) return status(HW_MATH_ERR_COMPLEX, 14);
  hwMatrix __R(R); /* copy matrix */
  if (L.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 15);
  if (!L.IsReal()) return status(HW_MATH_ERR_COMPLEX, 15);
  hwMatrix __L(L); /* copy matrix */
  double tol = (double)TOL(0);


// Input/Output variables
// Local variables
  int n = (int)(A.M());
  int m = (int)(B.N());
  int lda = (int)(_max(1, n));
  int lde = (int)(_max(1, n));
  int ldb = (int)(_max(1, n));
  int ldq = (int)(_max(1, _max(n, p)));
  int ldr = (int)(_max(1, _max(p, m)));
  int ldl = (int)(_max(1,n));
  int ldx = (int)(_max(1, n));
  int lds = (int)(_max(1,2 * n+m));
  int ldt = (int)(_max(1,2 * n+m));
  int ldu = (int)(_max(1, 2*n));
  hwMatrixI __IWORK((int)_max(1, _max(m, 2*n)), (int)1, hwMatrixI::REAL);
  hwMatrix __DWORK((int)_max(7*(2*n+1)+16, _max(16*n,_max(2*n+m,3*m))), (int)1, hwMatrix::REAL);
  int ldwork = (int)(_max(7*(2*n+1)+16, _max(16*n,_max(2*n+m,3*m))));
  hwMatrixI __IBWORK((int)2 * n, (int)1, hwMatrixI::REAL);
// Output variables
  double rcondu = 0.;
  hwMatrix __X((int)_max(1, n), (int)n, hwMatrix::REAL);
  hwMatrix __ALFAR((int)1, (int)2*n, hwMatrix::REAL);
  hwMatrix __ALFAI((int)1, (int)2*n, hwMatrix::REAL);
  hwMatrix __BETA((int)1, (int)2*n, hwMatrix::REAL);
  hwMatrix __S((int)_max(1,2 * n+m), (int)_max(1,2 * n+m), hwMatrix::REAL);
  hwMatrix __T((int)_max(1,2 * n+m), (int)2*n, hwMatrix::REAL);
  hwMatrix __U((int)_max(1, 2*n), (int)2*n, hwMatrix::REAL);
  int iwarn = 0;
  int info = 0;

  try
  {
    sg02ad_( dico,jobb,fact,uplo,jobl,scal,sort,acc,&n,&m,&p,__A.GetRealData(),&lda,__E.GetRealData(),&lde,__B.GetRealData(),&ldb,__Q.GetRealData(),&ldq,__R.GetRealData(),&ldr,__L.GetRealData(),&ldl,&rcondu,__X.GetRealData(),&ldx,__ALFAR.GetRealData(),__ALFAI.GetRealData(),__BETA.GetRealData(),__S.GetRealData(),&lds,__T.GetRealData(),&ldt,__U.GetRealData(),&ldu,&tol,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,__IBWORK.GetRealData(),&iwarn,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  status = RCONDU.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  RCONDU(0) = (double)rcondu;

  X = __X;
  ALFAR = __ALFAR;
  ALFAI = __ALFAI;
  BETA = __BETA;
  S = __S;
  T = __T;
  U = __U;
  status = IWARN.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  IWARN(0) = (double)iwarn;

  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_sb02od( const hwString &DICO, const hwString &JOBB, const hwString &FACT, const hwString &UPLO, const hwString &JOBL, const hwString &SORT, const hwMatrix &P, const hwMatrix &A, const hwMatrix &B, const hwMatrix &Q, const hwMatrix &R, const hwMatrix &L, hwMatrix &RCONDU, hwMatrix &X, hwMatrix &ALFAR, hwMatrix &ALFAI, hwMatrix &BETA, hwMatrix &S, hwMatrix &T, hwMatrix &U, const hwMatrix &TOL, hwMatrix &INFO )
{
  hwMathStatus status;
// Input variables
  char *dico = (char*)DICO.c_str();
  char *jobb = (char*)JOBB.c_str();
  char *fact = (char*)FACT.c_str();
  char *uplo = (char*)UPLO.c_str();
  char *jobl = (char*)JOBL.c_str();
  char *sort = (char*)SORT.c_str();
  double _dp = (double)P(0);
  int p = (int)(_dp);
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 8);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 8);
  hwMatrix __A(A); /* copy matrix */
  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 9);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 9);
  hwMatrix __B(B); /* copy matrix */
  if (Q.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 10);
  if (!Q.IsReal()) return status(HW_MATH_ERR_COMPLEX, 10);
  hwMatrix __Q(Q); /* copy matrix */
  if (R.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 11);
  if (!R.IsReal()) return status(HW_MATH_ERR_COMPLEX, 11);
  hwMatrix __R(R); /* copy matrix */
  if (L.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 12);
  if (!L.IsReal()) return status(HW_MATH_ERR_COMPLEX, 12);
  hwMatrix __L(L); /* copy matrix */
  double tol = (double)TOL(0);


// Input/Output variables
// Local variables
  int n = (int)(A.M());
  int m = (int)(B.N());
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1, n));
  int ldq = (int)(_max(1, _max(n, p)));
  int ldr = (int)(_max(1, _max(m, p)));
  int ldl = (int)(_max(1, n));
  int ldx = (int)(_max(1, n));
  int lds = (int)(_max(1,2 * n+m));
  int ldt = (int)(_max(1,2*n+m));
  int ldu = (int)(_max(1, 2*n));
  hwMatrixI __IWORK((int)_max(1, _max(1, 2*n)), (int)1, hwMatrixI::REAL);
  hwMatrix __DWORK((int)_max(7*(2*n+1)+16, _max(16*n, _max(2*n+m,3*m))), (int)1, hwMatrix::REAL);
  int ldwork = (int)(_max(7*(2*n+1)+16, _max(16*n, _max(2*n+m,3*m))));
  hwMatrixI __IBWORK((int)2*n, (int)1, hwMatrixI::REAL);
// Output variables
  double rcondu = 0.;
  hwMatrix __X((int)_max(1, n), (int)n, hwMatrix::REAL);
  hwMatrix __ALFAR((int)1, (int)2*n, hwMatrix::REAL);
  hwMatrix __ALFAI((int)1, (int)2*n, hwMatrix::REAL);
  hwMatrix __BETA((int)1, (int)2*n, hwMatrix::REAL);
  hwMatrix __S((int)_max(1,2*n+m), (int)_max(1,2*n+m), hwMatrix::REAL);
  hwMatrix __T((int)_max(1,2*n+m), (int)2*n, hwMatrix::REAL);
  hwMatrix __U((int)_max(1, 2*n), (int)2*n, hwMatrix::REAL);
  int info = 0;

  try
  {
    sb02od_( dico,jobb,fact,uplo,jobl,sort,&n,&m,&p,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__Q.GetRealData(),&ldq,__R.GetRealData(),&ldr,__L.GetRealData(),&ldl,&rcondu,__X.GetRealData(),&ldx,__ALFAR.GetRealData(),__ALFAI.GetRealData(),__BETA.GetRealData(),__S.GetRealData(),&lds,__T.GetRealData(),&ldt,__U.GetRealData(),&ldu,&tol,__IWORK.GetRealData(),__DWORK.GetRealData(),&ldwork,__IBWORK.GetRealData(),&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  status = RCONDU.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  RCONDU(0) = (double)rcondu;

  X = __X;
  ALFAR = __ALFAR;
  ALFAI = __ALFAI;
  BETA = __BETA;
  S = __S;
  T = __T;
  U = __U;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_ab13ad( const hwString &DICO, const hwString &EQUIL, const hwMatrix &ALPHA, hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &NS, hwMatrix &HSV, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out )
{
  hwMathStatus status;
// Input variables
  char *dico = (char*)DICO.c_str();
  char *equil = (char*)EQUIL.c_str();
  double alpha = (double)ALPHA(0);


// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __A(A); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __B(B); /* copy matrix */

  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __C(C); /* copy matrix */

// Local variables
  int n = (int)(A.M());
  int m = (int)(B.N());
  int p = (int)(C.M());
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1, n));
  int ldc = (int)(_max(1, p));
  hwMatrix __DWORK((int)1, (int)_max(1, n * (_max(_max(n, m), p) + 5) + n * (n + 1) / 2), hwMatrix::REAL);
  int ldwork = (int)(_max(1, n * (_max(_max(n, m), p) + 5) + n * (n + 1) / 2));
// Output variables
  int ns = 0;
  hwMatrix __HSV((int)n, (int)1, hwMatrix::REAL);
  int info = 0;

  try
  {
    ab13ad_( dico,equil,&n,&m,&p,&alpha,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,&ns,__HSV.GetRealData(),__DWORK.GetRealData(),&ldwork,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  B_out = __B;
  C_out = __C;
  status = NS.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  NS(0) = (double)ns;

  HSV = __HSV;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_mb03rd( const hwString &JOBX, const hwString &SORT, const hwMatrix &PMAX, hwMatrix &A, hwMatrix &X, hwMatrix &NBLCKS, hwMatrix &BLSIZE, hwMatrix &WR, hwMatrix &WI, const hwMatrix &TOL, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &X_out )
{
  hwMathStatus status;
// Input variables
  char *jobx = (char*)JOBX.c_str();
  char *sort = (char*)SORT.c_str();
  double pmax = (double)PMAX(0);
  double tol = (double)TOL(0);
// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __A(A); /* copy matrix */

  if (X.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!X.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __X(X); /* copy matrix */
// Local variables
  int n = (int)(A.N());
  int lda = (int)(_max(1, n));
  int ldx = (int)(_max(1, n));
  hwMatrix __DWORK((int)1, (int)n, hwMatrix::REAL);
// Output variables
  int nblcks = 0;
  hwMatrixI __BLSIZE((int)1, (int)n, hwMatrixI::REAL);
  hwMatrix __WR((int)1, (int)n, hwMatrix::REAL);
  hwMatrix __WI((int)1, (int)n, hwMatrix::REAL);
  int info = 0;

  try
  {
    mb03rd_( jobx,sort,&n,&pmax,__A.GetRealData(),&lda,__X.GetRealData(),&ldx,&nblcks,__BLSIZE.GetRealData(),__WR.GetRealData(),__WI.GetRealData(),&tol,__DWORK.GetRealData(),&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  X_out = __X;
  status = NBLCKS.Dimension(1, 1, hwMatrix::REAL);

  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }

  NBLCKS(0) = (double)nblcks;
  status = BLSIZE.Dimension(1, nblcks, hwMatrix::REAL);

  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }

  for (int i = 0; i < nblcks; ++i)
    BLSIZE(i) = (double) __BLSIZE(i);

  WR = __WR;
  WI = __WI;

  status = INFO.Dimension(1, 1, hwMatrix::REAL);

  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }

  INFO(0) = (double)info;

  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_ab05pd( const hwString &OVER, const hwMatrix &ALPHA, const hwMatrix &A1, const hwMatrix &B1, const hwMatrix &C1, const hwMatrix &D1, const hwMatrix &A2, const hwMatrix &B2, const hwMatrix &C2, const hwMatrix &D2, hwMatrix &N, hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &D, hwMatrix &INFO )
{
  hwMathStatus status;
// Input variables
  char *over = (char*)OVER.c_str();
  double alpha = (double)ALPHA(0);
  if (A1.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
  if (!A1.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
  hwMatrix __A1(A1); /* copy matrix */
  if (B1.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!B1.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __B1(B1); /* copy matrix */
  if (C1.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!C1.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __C1(C1); /* copy matrix */
  if (D1.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!D1.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __D1(D1); /* copy matrix */
  if (A2.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 7);
  if (!A2.IsReal()) return status(HW_MATH_ERR_COMPLEX, 7);
  hwMatrix __A2(A2); /* copy matrix */
  if (B2.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 8);
  if (!B2.IsReal()) return status(HW_MATH_ERR_COMPLEX, 8);
  hwMatrix __B2(B2); /* copy matrix */
  if (C2.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 9);
  if (!C2.IsReal()) return status(HW_MATH_ERR_COMPLEX, 9);
  hwMatrix __C2(C2); /* copy matrix */
  if (D2.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 10);
  if (!D2.IsReal()) return status(HW_MATH_ERR_COMPLEX, 10);
  hwMatrix __D2(D2); /* copy matrix */


// Input/Output variables
// Local variables
  int n1 = (int)(A1.N());
  int m = (int)(B1.N());
  int p = (int)(C1.M());
  int n2 = (int)(A2.N());
  int lda1 = (int)(_max(1, n1));
  int ldb1 = (int)(_max(1, n1));
  int ldc1 = (int)(_max(1, p));
  int ldd1 = (int)(_max(1, p));
  int lda2 = (int)(_max(1, n2));
  int ldb2 = (int)(_max(1, n2));
  int ldc2 = (int)(_max(1, p));
  int ldd2 = (int)(_max(1, p));
  int lda = (int)(_max(1, n1 + n2));
  int ldb = (int)(_max(1, n1 + n2));
  int ldc = (int)(_max(1, p));
  int ldd = (int)(_max(1, p));
// Output variables
  int n = 0;
  hwMatrix __A((int)_max(1, n1 + n2), (int)n1 + n2, hwMatrix::REAL);
  hwMatrix __B((int)_max(1, n1 + n2), (int)m, hwMatrix::REAL);
  hwMatrix __C((int)_max(1, p), (int)n1 + n2, hwMatrix::REAL);
  hwMatrix __D((int)_max(1, p), (int)m, hwMatrix::REAL);
  int info = 0;

  try
  {
    ab05pd_( over,&n1,&m,&p,&n2,&alpha,__A1.GetRealData(),&lda1,__B1.GetRealData(),&ldb1,__C1.GetRealData(),&ldc1,__D1.GetRealData(),&ldd1,__A2.GetRealData(),&lda2,__B2.GetRealData(),&ldb2,__C2.GetRealData(),&ldc2,__D2.GetRealData(),&ldd2,&n,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__D.GetRealData(),&ldd,&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  status = N.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  N(0) = (double)n;

  A = __A;
  B = __B;
  C = __C;
  D = __D;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_tb01id( const hwString &JOB, hwMatrix &MAXRED, hwMatrix &A, hwMatrix &B, hwMatrix &C, hwMatrix &SCALE, hwMatrix &INFO, hwMatrix &MAXRED_out, hwMatrix &A_out, hwMatrix &B_out, hwMatrix &C_out )
{
  hwMathStatus status;
// Input variables
  char *job = (char*)JOB.c_str();


// Input/Output variables
  double maxred = (double)MAXRED(0);
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
  hwMatrix __A(A); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __B(B); /* copy matrix */

  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __C(C); /* copy matrix */

// Local variables
  int n = (int)(B.M());
  int m = (int)(B.N());
  int p = (int)(C.N());
  int lda = (int)(_max(1, n));
  int ldb = (int)(_max(1, n));
  int ldc = (int)(_max(1, p));
// Output variables
  hwMatrix __SCALE((int)1, (int)n, hwMatrix::REAL);
  int info = 0;

  try
  {
    tb01id_( job,&n,&m,&p,&maxred,__A.GetRealData(),&lda,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__SCALE.GetRealData(),&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  status = MAXRED_out.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  MAXRED_out(0) = (double)maxred;
  A_out = __A;
  B_out = __B;
  C_out = __C;
  SCALE = __SCALE;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;


  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_tg01ad( const hwString &JOB, const hwMatrix &THRESH, hwMatrix &A, hwMatrix &E, hwMatrix &B, hwMatrix &C, hwMatrix &LSCALE, hwMatrix &RSCALE, hwMatrix &INFO, hwMatrix &A_out, hwMatrix &E_out, hwMatrix &B_out, hwMatrix &C_out )
{
  hwMathStatus status;
// Input variables
  char *job = (char*)JOB.c_str();
  double thresh = (double)THRESH(0);

// Input/Output variables
  if (A.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 3);
  if (!A.IsReal()) return status(HW_MATH_ERR_COMPLEX, 3);
  hwMatrix __A(A); /* copy matrix */

  if (E.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 4);
  if (!E.IsReal()) return status(HW_MATH_ERR_COMPLEX, 4);
  hwMatrix __E(E); /* copy matrix */

  if (B.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 5);
  if (!B.IsReal()) return status(HW_MATH_ERR_COMPLEX, 5);
  hwMatrix __B(B); /* copy matrix */

  if (C.IsEmpty()) return status(HW_MATH_ERR_EMPTYMATRIX, 6);
  if (!C.IsReal()) return status(HW_MATH_ERR_COMPLEX, 6);
  hwMatrix __C(C); /* copy matrix */

// Local variables
  int l = (int)(A.M());
  int n = (int)(A.N());
  int m = (int)(B.N());
  int p = (int)(C.M());
  int lda = (int)(_max(1, l));
  int lde = (int)(_max(1, l));
  int ldb = (int)(_max(1, l));
  int ldc = (int)(_max(1, p));
  hwMatrix __DWORK((int)1, (int)3 * (l + n), hwMatrix::REAL);
// Output variables
  hwMatrix __LSCALE((int)1, (int)l, hwMatrix::REAL);
  hwMatrix __RSCALE((int)1, (int)n, hwMatrix::REAL);
  int info = 0;

  try
  {
    tg01ad_( job,&l,&n,&m,&p,&thresh,__A.GetRealData(),&lda,__E.GetRealData(),&lde,__B.GetRealData(),&ldb,__C.GetRealData(),&ldc,__LSCALE.GetRealData(),__RSCALE.GetRealData(),__DWORK.GetRealData(),&info );
  }
  catch (const int)
  {
    return status(HW_MATH_ERR_NOTALLOWED);
  }

  A_out = __A;
  E_out = __E;
  B_out = __B;
  C_out = __C;
  LSCALE = __LSCALE;
  RSCALE = __RSCALE;
  status = INFO.Dimension(1, 1, hwMatrix::REAL);
  if (!status.IsOk())
  {
    status.ResetArgs();
    return status;
  }
  INFO(0) = (double)info;

  return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_tb05ad(hwMatrix& A, hwMatrix& B, hwMatrix& C, hwMatrix& EVRE, hwMatrix& EVIM, int& ldwork)
{
    if (!A.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 1);

    if (!A.IsSquare())
        return hwMathStatus(HW_MATH_ERR_MTXNOTSQUARE, 1);

    if (!B.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 2);

    if (!C.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 3);

    int n = A.M();  // number of states

    if (B.M() != n)
        return hwMathStatus(HW_MATH_ERR_ARRAYSIZE, 1, 2);

    int p = B.N();  // number of inputs

    if (C.N() != n)
        return hwMathStatus(HW_MATH_ERR_ARRAYSIZE, 1, 3);

    int q = C.M();  // number of outputs

    if (!C.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 4);

    hwMathStatus status;

    status = EVRE.Dimension(n, 1, hwMatrix::REAL);
    status = EVIM.Dimension(n, 1, hwMatrix::REAL);

    if (!n || !p || !q)
        return status;

    hwMatrix G(q * p, 1, hwMatrix::COMPLEX);
    hwMatrix HINVB(n, p, hwMatrix::COMPLEX);
    hwMatrixI IWORK(n, hwMatrixI::REAL);
    int lzwork = n * n + 2 * n;
    hwMatrix ZWORK(lzwork, hwMatrix::COMPLEX);
    hwComplex freq(0.0, 100.0);    // dummy value
    double*    a      = A.GetRealData();
    double*    b      = B.GetRealData();
    double*    c      = C.GetRealData();
    hwComplex* g      = G.GetComplexData();
    double*    evre   = EVRE.GetRealData();
    double*    evim   = EVIM.GetRealData();
    hwComplex* h      = HINVB.GetComplexData();
    int*       iwork  = IWORK.GetRealData();
    hwComplex* zwork  = ZWORK.GetComplexData();
    char*      baleig = "A";
    char*      inita  = "G";

    // first call and workspace query
    ldwork = _max(p, q) - 1;
    ldwork = n + _max(n, ldwork);

    hwMatrix DWORK(ldwork, hwMatrix::REAL);
    double* dwork = DWORK.GetRealData();
    double RCOND;
    int info = 0;

    try
    {
        tb05ad_(baleig, inita, &n, &p, &q, &freq, a, &n, b, &n, c, &q, &RCOND, g, &q, evre, evim, h, &n, iwork, dwork, &ldwork, zwork, &lzwork, &info);
    }
    catch (const int)
    {
        return status(HW_MATH_ERR_NOTALLOWED);
    }

    if (info == 2)
        return status(HW_MATH_ERR_SINGMATRIX);

    ldwork = static_cast<int> (dwork[0]);

    return status;
}
//---------------------------------------------------------------------------
hwMathStatus slicot_tb05ad(const hwMatrix& FREQ, const hwMatrix& H, const hwMatrix& B, const hwMatrix& C, double& RCOND, hwMatrix& G, int ldwork)
{
    if (!FREQ.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 1);

    int numPts = FREQ.Size();

    if (!H.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 2);

    if (!H.IsSquare())
        return hwMathStatus(HW_MATH_ERR_MTXNOTSQUARE, 2);

    if (!B.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 3);

    if (!C.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 4);

    int n = H.M();  // number of states

    if (B.M() != n)
        return hwMathStatus(HW_MATH_ERR_ARRAYSIZE, 2, 3);

    int p = B.N();  // number of inputs

    if (C.N() != n)
        return hwMathStatus(HW_MATH_ERR_ARRAYSIZE, 2, 4);

    int q = C.M();  // number of outputs

    if (!C.IsReal())
        return hwMathStatus(HW_MATH_ERR_COMPLEX, 4);

    hwMathStatus status;

    status = G.Dimension(q * p, numPts, hwMatrix::COMPLEX);

    if (!numPts || !n || !p || !q)
        return status;

    hwMatrix Bcopy(B);
    hwMatrix Ccopy(C);
    hwMatrix HINVB(n, p, hwMatrix::COMPLEX);
    hwMatrixI IWORK(n, hwMatrixI::REAL);
    int lzwork = n * n + 2 * n;
    hwMatrix ZWORK(lzwork, hwMatrix::COMPLEX);
    double*     f      = const_cast<double*> (FREQ.GetRealData());
    double*     a      = const_cast<double*> (H.GetRealData());
    double*     b      = Bcopy.GetRealData();
    double*     c      = Ccopy.GetRealData();
    hwComplex*  g      = G.GetComplexData();
    double*     evre   = nullptr;
    double*     evim   = nullptr;
    hwComplex*  h      = HINVB.GetComplexData();
    int*        iwork  = IWORK.GetRealData();
    hwComplex*  zwork  = ZWORK.GetComplexData();
    char*       baleig = "C";
    char*       inita  = "H";

    hwMatrix DWORK(ldwork, hwMatrix::REAL);
    double* dwork = DWORK.GetRealData();

    // now do the rest of the calculations
    int       info = 0;
    double    rcond;
    hwComplex freq;

    for (int i = 0; i < numPts; ++i)
    {
        freq.Imag() = *(f++);

        try
        {
            tb05ad_(baleig, inita, &n, &p, &q, &freq, a, &n, b, &n, c, &q, &rcond, g, &q, evre, evim, h, &n, iwork, dwork, &ldwork, zwork, &lzwork, &info);
        }
        catch (const int)
        {
            return status(HW_MATH_ERR_NOTALLOWED);
        }

        if (info == 2)
            return status(HW_MATH_ERR_SINGMATRIX);

        g++;

        if (i == 0 || rcond < RCOND)
        {
            RCOND = rcond;
        }
    }

    return status;
}
//---------------------------------------------------------------------------
