/**
* @file slicot.h
* @date December 2013
* Copyright (C) 2007-2018 Altair Engineering, Inc.
* This file is part of the OpenMatrix Language ("OpenMatrix") software.
* Open Source License Information:
* OpenMatrix is free software. You can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* OpenMatrix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Commercial License Information:
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/
#ifndef __SLICOT_H__
#define __SLICOT_H__

template <typename T> class hwTComplex;
typedef hwTComplex<double> hwComplex;

#ifdef __cplusplus
  extern "C"
  {
#endif

extern int sb01bd_ ( char *dico, int *n, int *m, int *np, double *alpha, double *a, int *lda, double *b, int *ldb, double *wr, double *wi, int *nfp, int *nap, int *nup, double *f, int *ldf, double *z, int *ldz, double *tol, double *dwork, int *ldwork, int *iwarn, int *info );
extern int ab01od_ ( char *stages, char *jobu, char *jobv, int *n, int *m, double *a, int *lda, double *b, int *ldb, double *u, int *ldu, double *v, int *ldv, int *ncont, int *indcon, int *kstair, double *tol, int *iwork, double *dwork, int *ldwork, int *info );
extern int sb02md_ ( char *dico, char *hinv, char *uplo, char *scal, char *sort, int *n, double *a, int *lda, double *g, int *ldg, double *q, int *ldq, double *u, int *ldu, int *info, double *rcond, double *wr, double *wi, double *s, int *lds, int *iwork, double *dwork, int *ldwork, int *bwork );
extern int mc01td_ ( char *dico, int *dp, double *p, int *stable, int *nz, double *dwork, int *iwarn, int *info );
extern int mb02md_ ( char *job, int *m, int *n, int *l, int *rank, double *c, int *ldc, double *s, double *x, int *ldx, double *tol, int *iwork, double *dwork, int *ldwork, int *iwarn, int *info );
extern int mb02nd_ ( int *m, int *n, int *l, int *rank, double *theta, double *c, int *ldc, double *x, int *ldx, double *q, int *inul, double *tol, double *reltol, int *iwork, double *dwork, int *ldwork, int *bwork, int *iwarn, int *info );
extern int mb02qd_ ( char *job, char *iniper, int *m, int *n, int *nrhs, double *rcond, double *svlmax, double *a, int *lda, double *b, int *ldb, double *y, int *jpvt, int *rank, double *sval, double *dwork, int *ldwork, int *info );
extern int mb03od_ ( char *jobqr, int *m, int *n, double *a, int *lda, int *jpvt, double *rcond, double *svlmax, double *tau, int *rank, double *sval, double *dwork, int *ldwork, int *info );
extern int mb03pd_ ( char *jobrq, int *m, int *n, double *a, int *lda, int *jpvt, double *rcond, double *svlmax, double *tau, int *rank, double *sval, double *dwork, int *info );
extern int mb04gd_ ( int *m, int *n, double *a, int *lda, int *jpvt, double *tau, double *dwork, int *info );
extern int mb04md_ ( int *n, double *maxred, double *a, int *lda, double *scale, int *info );
extern int ab08nd_ ( char *equil, int *n, int *m, int *p, double *a, int *lda, double *b, int *ldb, double *c, int *ldc, double *d, int *ldd, int *nu, int *rank, int *dinfz, int *nkror, int *nkrol, int *infz, int *kronr, int *kronl, double *af, int *ldaf, double *bf, int *ldbf, double *tol, int *iwork, double *dwork, int *ldwork, int *info );
extern int ab07nd_ ( int *n, int *m, double *a, int *lda, double *b, int *ldb, double *c, int *ldc, double *d, int *ldd, double *rcond, int *iwork, double *dwork, int *ldwork, int *info );
extern int ag07bd_ ( char *jobe, int *n, int *m, double *a, int *lda, double *e, int *lde, double *b, int *ldb, double *c, int *ldc, double *d, int *ldd, double *ai, int *ldai, double *ei, int *ldei, double *bi, int *ldbi, double *ci, int *ldci, double *di, int *lddi, int *info );
extern int ag08bd_ ( char *equil, int *l, int *n, int *m, int *p, double *a, int *lda, double *e, int *lde, double *b, int *ldb, double *c, int *ldc, double *d, int *ldd, int *nfz, int *nrank, int *niz, int *dinfz, int *nkror, int *ninfe, int *nkrol, int *infz, int *kronr, int *infe, int *kronl, double *tol, int *iwork, double *dwork, int *ldwork, int *info );
extern int sb03md_ ( char *dico, char *job, char *fact, char *trana, int *n, double *a, int *lda, double *u, int *ldu, double *c, int *ldc, double *scale, double *sep, double *ferr, double *wr, double *wi, int *iwork, double *dwork, int *ldwork, int *info );
extern int sg03ad_ ( char *dico, char *job, char *fact, char *trans, char *uplo, int *n, double *a, int *lda, double *e, int *lde, double *q, int *ldq, double *z, int *ldz, double *x, int *ldx, double *scale, double *sep, double *ferr, double *alphar, double *alphai, double *beta, int *iwork, double *dwork, int *ldwork, int *info );
extern int sb04qd_ ( int *n, int *m, double *a, int *lda, double *b, int *ldb, double *c, int *ldc, double *z, int *ldz, int *iwork, double *dwork, int *ldwork, int *info );
extern int sb04md_ ( int *n, int *m, double *a, int *lda, double *b, int *ldb, double *c, int *ldc, double *z, int *ldz, int *iwork, double *dwork, int *ldwork, int *info );
extern int tb01ud_ ( char *jobz, int *n, int *m, int *p, double *a, int *lda, double *b, int *ldb, double *c, int *ldc, int *ncont, int *indcon, int *nblk, double *z, int *ldz, double *tau, double *tol, int *iwork, double *dwork, int *ldwork, int *info );
extern int sb03od_ ( char *dico, char *fact, char *trans, int *n, int *m, double *a, int *lda, double *q, int *ldq, double *b, int *ldb, double *scale, double *wr, double *wi, double *dwork, int *ldwork, int *info );
extern int sg03bd_ ( char *dico, char *fact, char *trans, int *n, int *m, double *a, int *lda, double *e, int *lde, double *q, int *ldq, double *z, int *ldz, double *b, int *ldb, double *scale, double *alphar, double *alphai, double *beta, double *dwork, int *ldwork, int *info );
extern int mb05od_ ( char *balanc, int *n, int *ndiag, double *delta, double *a, int *lda, int *mdig, int *idig, int *iwork, double *dwork, int *ldwork, int *iwarn, int *info );
extern int ab04md_ ( char *type, int *n, int *m, int *p, double *alpha, double *beta, double *a, int *lda, double *b, int *ldb, double *c, int *ldc, double *d, int *ldd, int *iwork, double *dwork, int *ldwork, int *info );
extern int sb10jd_ ( int *n, int *m, int *np, double *a, int *lda, double *b, int *ldb, double *c, int *ldc, double *d, int *ldd, double *e, int *lde, int *nsys, double *dwork, int *ldwork, int *info );
extern int sg02ad_ ( char *dico, char *jobb, char *fact, char *uplo, char *jobl, char *scal, char *sort, char *acc, int *n, int *m, int *p, double *a, int *lda, double *e, int *lde, double *b, int *ldb, double *q, int *ldq, double *r, int *ldr, double *l, int *ldl, double *rcondu, double *x, int *ldx, double *alfar, double *alfai, double *beta, double *s, int *lds, double *t, int *ldt, double *u, int *ldu, double *tol, int *iwork, double *dwork, int *ldwork, int *bwork, int *iwarn, int *info );
extern int sb02od_ ( char *dico, char *jobb, char *fact, char *uplo, char *jobl, char *sort, int *n, int *m, int *p, double *a, int *lda, double *b, int *ldb, double *q, int *ldq, double *r, int *ldr, double *l, int *ldl, double *rcondu, double *x, int *ldx, double *alfar, double *alfai, double *beta, double *s, int *lds, double *t, int *ldt, double *u, int *ldu, double *tol, int *iwork, double *dwork, int *ldwork, int *bwork, int *info );
extern int ab13ad_ ( char *dico, char *equil, int *n, int *m, int *p, double *alpha, double *a, int *lda, double *b, int *ldb, double *c, int *ldc, int *ns, double *hsv, double *dwork, int *ldwork, int *info );
extern int mb03rd_ ( char *jobx, char *sort, int *n, double *pmax, double *a, int *lda, double *x, int *ldx, int *nblcks, int *blsize, double *wr, double *wi, double *tol, double *dwork, int *info );
extern int ab05pd_ ( char *over, int *n1, int *m, int *p, int *n2, double *alpha, double *a1, int *lda1, double *b1, int *ldb1, double *c1, int *ldc1, double *d1, int *ldd1, double *a2, int *lda2, double *b2, int *ldb2, double *c2, int *ldc2, double *d2, int *ldd2, int *n, double *a, int *lda, double *b, int *ldb, double *c, int *ldc, double *d, int *ldd, int *info );
extern int tb01id_ ( char *job, int *n, int *m, int *p, double *maxred, double *a, int *lda, double *b, int *ldb, double *c, int *ldc, double *scale, int *info );
extern int tg01ad_ ( char *job, int *l, int *n, int *m, int *p, double *thresh, double *a, int *lda, double *e, int *lde, double *b, int *ldb, double *c, int *ldc, double *lscale, double *rscale, double *dwork, int *info );
extern int tb05ad_ ( char* baleig, char* inita, int* n, int* m, int* p, hwComplex* freq, double* a, int* lda, double* b, int* ldb, double* c, int* ldc, double* rcond, hwComplex* g, int* ldg, double* evre, double* evim, hwComplex* hinvb, int* ldhinv, int* iwork, double* dwork, int* ldwork, hwComplex* zwork, int* lzwork, int* info );

#ifdef __cplusplus
   }
#endif

#endif /* __SLICOT_H__ */
