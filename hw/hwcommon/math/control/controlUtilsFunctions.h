/**
* @file ControlUtilsFunctions.h
* @date December 2013
* Copyright (C) 2007-2018 Altair Engineering, Inc.
* This file is part of the OpenMatrix Language ("OpenMatrix") software.
* Open Source License Information:
* OpenMatrix is free software. You can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* OpenMatrix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Commercial License Information:
* For a copy of the commercial license terms and conditions, contact the Altair Legal Department at Legal@altair.com and in the subject line, use the following wording: Request for Commercial License Terms for OpenMatrix.
* Altair's dual-license business model allows companies, individuals, and organizations to create proprietary derivative works of OpenMatrix and distribute them - whether embedded or bundled with other software - under a commercial license agreement.
* Use of Altair's trademarks and logos is subject to Altair's trademark licensing policies.  To request a copy, email Legal@altair.com and in the subject line, enter: Request copy of trademark and logo usage policy.
*/
#ifndef __CONTROLUTILSFUNCTIONS_H__
#define __CONTROLUTILSFUNCTIONS_H__

#include "slicotExports.h"

// forward declarations
class hwString;
class hwMathStatus;
template <typename T> class hwTComplex;
template <typename T1, typename T2> class hwTMatrix;
typedef hwTMatrix<double, hwTComplex<double> > hwMatrix;
typedef hwTMatrix<int, hwTComplex<int> > hwMatrixI;

SLICOT_DECLS hwMathStatus slicot_ab13cd(const hwMatrix& A,
                                        const hwMatrix& B,
                                        const hwMatrix& C,
                                        const hwMatrix& D, 
                                        const hwMatrix& TOL,
                                        hwMatrix&       INFO,
                                        hwMatrix&       FPEAK,
                                        hwMatrix&       RESULT);
                                         
SLICOT_DECLS hwMathStatus slicot_ab13bd(const hwString& DICO,
                                        const hwString& JOBN,
                                        const hwMatrix& A,
                                        const hwMatrix& B,
                                        const hwMatrix& C,
                                        const hwMatrix& D, 
                                        const hwMatrix& TOL,
                                        hwMatrix&       INFO,
                                        hwMatrix&       IWARN,
                                        hwMatrix&       NQ,
                                        hwMatrix&       RESULT);

SLICOT_DECLS hwMathStatus slicot_td04ad(const hwMatrix&  UCOEFF,
                                        const hwMatrix&  DCOEFF,
                                        const hwMatrixI& INDEX,
                                        double           tol,
                                        hwMatrix&        A,
                                        hwMatrix&        B,
                                        hwMatrix&        C,
                                        hwMatrix&        D,
                                        int&             NR,
                                        int&             INFO);

SLICOT_DECLS hwMathStatus slicot_tb04ad(const hwMatrix& A,
                                        const hwMatrix& B,
                                        const hwMatrix& C,
                                        const hwMatrix& D,
                                        hwMatrix&       UCOEFF,
                                        hwMatrix&       DCOEFF,
                                        hwMatrixI&      INDEX,
                                        double          tol1,
                                        double          tol2,
                                        int&            NR,
                                        int&            INFO);

SLICOT_DECLS hwMathStatus slicot_ab13dd(const hwString& DICO,
                                        const hwString& JOBE,
                                        const hwString& EQUIL,
                                        const hwString& JOBD,
                                        const hwMatrix& FPEAKIN,
                                        const hwMatrix& A,
                                        const hwMatrix& B,
                                        const hwMatrix& C,
                                        const hwMatrix& D,
                                        const hwMatrix& E,
                                        const hwMatrix& TOL,
                                        hwMatrix&       GPEAK,
                                        hwMatrix&       FPEAKOUT,
                                        hwMatrix&       INFO);

SLICOT_DECLS hwMathStatus slicot_sb10dd(const hwMatrix& A,
                                        const hwMatrix& B,
                                        const hwMatrix& C,
                                        const hwMatrix& D,
                                        int             NCON,
                                        int             NMEAS,
                                        double          GAMMA,
                                        double          TOL,
                                        hwMatrix&       AK,
                                        hwMatrix&       BK,
                                        hwMatrix&       CK,
                                        hwMatrix&       DK,
                                        hwMatrix&       X,
                                        hwMatrix&       Z,
                                        hwMatrix&       RCOND,
                                        hwMatrix&       INFO);

SLICOT_DECLS hwMathStatus slicot_sb10fd(const hwMatrix& A,
                                        const hwMatrix& B,
                                        const hwMatrix& C,
                                        const hwMatrix& D,
                                        int             NCON,
                                        int             NMEAS,
                                        double          GAMMA,
                                        double          TOL,
                                        hwMatrix&       AK,
                                        hwMatrix&       BK,
                                        hwMatrix&       CK,
                                        hwMatrix&       DK,
                                        hwMatrix&       RCOND,
                                        hwMatrix&       INFO);

SLICOT_DECLS hwMathStatus slicot_expm(const hwMatrix& A, 
                                      hwMatrix&       X,
                                      int&            warn,
                                      int&            info);

SLICOT_DECLS hwMathStatus notslicot_logm(const hwMatrix& A,
                                         hwMatrix&       X);

SLICOT_DECLS hwMathStatus notslicot_sqrtm(const hwMatrix& A,
                                          hwMatrix&       X);

SLICOT_DECLS hwMathStatus rsf2csf(const hwMatrix& UR,
                                 const hwMatrix& TR,
                                 hwMatrix&       UC,
                                 hwMatrix&       TC);

#endif /* __CONTROLUTILSFUNCTIONS_H__ */
