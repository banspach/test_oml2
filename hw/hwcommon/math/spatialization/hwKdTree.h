////////////////////////////////////////////////////////////////////
// File   : hwKdTree.h
// Date   : 16 August, 2004, jnellikk/champati -- Generated
// Copyright (c) 2003 Altair Engineering Inc.  All Rights 
// Reserved.  Contains trade secrets of Altair Engineering, Inc.  
// Copyright notice does not imply publication.  Decompilation or 
// disassembly of this software is strictly prohibited.
////////////////////////////////////////////////////////////////////

//:---------------------------------------------------------------------------
//:Description
//
//  class to represent a kd tree which provides functionality to return nearest 
//  neighbor, range search etc in k dimensions
//
//:---------------------------------------------------------------------------

#ifndef __HWKDTREE_H__
#define __HWKDTREE_H__

// HP does not recognize name spaces.
//#ifdef HP_PLATFORM
//#   define STD
//#else
#define STD std
//#endif

#include <vector>
#include <assert.h>
#include <math/spatialization/hwKdPoint.h>

// forward declarations
template <typename T> class hwTTriple;
typedef hwTTriple<double> hwTriple;

template <typename hwKdCoord, typename hwKdDist, int N> class hwiKdTree;

template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER> class hwKdTreeIterator;
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER> class hwKdTreeConstIterator;

typedef int hwKdIdx;
typedef hwKdIdx*  hwKdIdxArray;
typedef STD::vector<hwKdIdx> hwKdIdxList;

//HwConvert {{

// -------------------------------------------------------------------
//! \class hwKdTree hwKdTree.h math/spatialization/hwKdTree.h
//! \brief Class to represent a kd tree which provides functionality
//! to return nearest neighbor, range search etc in k dimensions.
//!
//! Users should implement a function (takes an object of type T and reference
//! to an hwKdPoint as arguements) which implements how to fill in the values'
//! for each dimension from object T to hwKdPoint. 
//!
//! Sample implementation that fills an hwKdPoint from hwudbPhyEnt*.
//! template arguements  for the function are:
//! \li    T = hwudbPhyEnt* - Type of object with which KD Tree is being created
//! \li real = float - type specified for values of coordinates during KD tree ]
//!            creation.
//! \li    N = 3 - number of dimensions specified during tree creation.
//!
//! \code
//! template <typename T, typename real, int N>
//! static void fill_coord(const T obj, hwKdPoint<real,N>& point)
//! {
//!     if( N != 3 ) return; // hwudbPhyEnt is 3 dimensional
//!     hwTriple pos = obj->GetPos();
//!     point[0] = (real)pos.GetX();
//!     point[1] = (real)pos.GetY();
//!     point[2] = (real)pos.GetZ();
//! }
//! \endcode
//!  
//! Now KD tree can be declared as
//! hwKdTree< hwudbPhyEnt*, float, double, 3> myTree( &fill_coord, 2 );
//! where:
//! \li   hwudbPhyEnt = Object type from which myTree is created
//! \li   float       = Type to be used for dimension value inside myTree
//! \li   double      = Type to be used for distance. (If more precision needed.)
//! \li   3           = MyTree's dimension.
//! \li   fill_coord  = Function that will fill in an hwKdPoint from
//!                     an hwudbPhyEnt
//! \li   2           = Number of objects to hold in a leaf node.
// -------------------------------------------------------------------

template <typename T, typename real, int N> class hwKdTreeFiller {};

template <typename T, typename real, int N> class hwKdTreeFiller3D
{
public :
	hwKdTreeFiller3D(T obj, hwKdPoint<real,3>& point)
	{
		point[0] = (real)obj.GetX();
		point[1] = (real)obj.GetY();
		point[2] = (real)obj.GetZ();
	}
	~hwKdTreeFiller3D() {;}
};
template <typename T, typename real, int N> class hwKdTreeFiller3DPtr
{
public :
	hwKdTreeFiller3DPtr(T obj, hwKdPoint<real,3>& point)
	{
		point[0] = (real)obj->GetX();
		point[1] = (real)obj->GetY();
		point[2] = (real)obj->GetZ();
	}
	~hwKdTreeFiller3DPtr() {;}
};

template <
			typename T,
			typename hwKdCoord = float,
			typename hwKdDist = float,
			int N = 3,
			typename CONTAINER = STD::vector<T>,
			typename FILLER = hwKdTreeFiller<T, hwKdCoord, N>
		>
class hwKdTree
{
  //! Needs access to the internal CONTAINER
	friend class hwKdTreeIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>;
	//! Needs access to the internal CONTAINER
	friend class hwKdTreeConstIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>;
private:
	//typedef hwStlVector< hwKdPoint<hwKdCoord,N> > hwKdPointList;
  typedef STD::vector< hwKdPoint<hwKdCoord,N> > hwKdPointList;
public :
    //! Constructor for 3d trees where T implements GetX(), GetY() & GetZ().
	//! A default function is used to fill the hwKdPoint using T.GetX(), T.GetY() etc.
    //! As bucket size increases, tree construction time reduces but
    //! search time increases. 1024 is simply a default value chosen as a compromise
    hwKdTree( const size_t bktSize = 1024);
    //! Destructor
    virtual ~hwKdTree() { DeleteTree(); }
    //! Reset the tree
    void Clear() { 
        DeleteTree();
        items_.clear();
    }
    //! Get the number of items in the tree
    size_t GetSize() const { return items_.size(); }
    //! Get if there are no items in the tree
    bool IsEmpty() const { return items_.empty(); }
    //! Insert an item to the tree
    void Insert(T item) { items_.push_back(item); dirty_ = true; }
    //! Insert a list of items to the tree
    void Insert(const CONTAINER &items) {for(size_t i=0; i<items.size(); ++i) items_.push_back(items[i]); dirty_ = true;}
    //! Get the nearest neighbor given an xyz coordinate.
    //! Convenient function to operate in 3 dimensions.
    T GetNn(hwKdCoord x, hwKdCoord y, hwKdCoord z, hwKdDist* dist = 0) const;
    //! Get the nearest neighbor given an triple.
    //! Convenient function to operate in 3 dimensions.
    T GetNn(const hwTriple &pos, hwKdDist* dist = 0) const;
//HwConvert }}
    //! Get the nearest neighbor given an hwKdPoint.
    T GetNn( const hwKdPoint<hwKdCoord,N>& pt, hwKdDist* dist = 0) const;
//HwConvert {{
    //! Get specified number of nearest neighbours given an hwTriple.
    //! Convenient function to operate in 3 dimensions.
    size_t GetNn(
                //! Position to search for neighbors
                const hwTriple &pos,
                //! Number of neighbors requested. If this many are not found,
                //! returns as many that are found. Check the return value or
                //! size of neigbor array for correct number of neighbors.
                size_t number_nearest_neighbors,
                //! Neighbors found
                CONTAINER &neighbors,
                //! Distances to neighbors in same order "hwStlVector<hwKdDist> *distances = 0".
                STD::vector<hwKdDist> *distances = 0
              ) const;
//HwConvert }}
    //! Get specified number of nearest neighbours given an hwKdPoint.
    size_t GetNn(
                //! Position to search for neighbors
                const hwKdPoint<hwKdCoord,N> &pos,
                //! Number of neighbors requested. If this many are not found,
                //! returns as many that are found. Check the return value or
                //! size of neigbor array for correct number of neighbors.
                size_t number_nearest_neighbors,
                //! Neighbors found
                CONTAINER &neighbors,
                //! Distances to neighbors in same order "hwStlVector<hwKdDist> *distances = 0".
                STD::vector<hwKdDist> *distances = 0
              ) const;
//HwConvert {{
    //! Perform range search given bottom left and top right positions.
    //! Convenient function to operate in 3 dimensions.
    size_t Search(
                //! Bottom left position of search cube
                const hwTriple &bottom_left,
                //! Top right position of search cube
                const hwTriple &top_right,
                //! Items found inside the cube
                CONTAINER &items
                 ) const;
    //! Perform range search given bottom left and top right positions.
    // ACCEPTS an additional argument : A temporary container used only by the internals of the code.
    // If this container can be re-used across multiple calls of this function, a lot of performance gain can 
    // be observed by avoiding reallocations due to vector push_back.
    size_t Search(
                //! Bottom left position of search cube
                const hwTriple &bottom_left,
                //! Top right position of search cube
                const hwTriple &top_right,
                //! Items found inside the cube
                CONTAINER &items,
                //! A re-used vector to hold the indices. Only for performance reasons
                hwKdIdxList &index_array
                 ) const;
//HwConvert }}
    //! Perform range search given bottom left and top right positions.
    size_t Search(
                //! Bottom left position of search cube
                const hwKdPoint<hwKdCoord,N> &bottom_left,
                //! Top right position of search cube
                const hwKdPoint<hwKdCoord,N> &top_right,
                //! Items found inside the cube
                CONTAINER &items
                 ) const;

    //! Perform range search given hwKdPoints. 
    // ACCEPTS an additional argument : A temporary container used only by the internals of the code.
    // If this container can be re-used across multiple calls of this function, a lot of performance gain can 
    // be observed by avoiding reallocations due to vector push_back.
    size_t Search(
                //! Bottom left position of search cube
                const hwKdPoint<hwKdCoord,N> &bottom_left,
                //! Top right position of search cube
                const hwKdPoint<hwKdCoord,N> &top_right,
                //! Items found inside the cube
                CONTAINER &items,
                //! A re-used vector to hold the indices. Only for performance reasons
                hwKdIdxList &index_array
                ) const;

//HwConvert {{
    //! Get coincident items given an hwTriple.
    //! Convenient function to operate in 3 dimensions.
    size_t GetCoincidentItems(
                //! Position to search for coincident items
                const hwTriple &pos,
                //! Items found
                CONTAINER &items, 
                //! Tolerance to consider an item as coincident
                hwKdCoord tolerance = 0.0
                ) const { return Search( pos - hwTriple(tolerance,tolerance,tolerance), 
                                   pos + hwTriple(tolerance,tolerance,tolerance), items ); }
//HwConvert }}
    //! Get coincident items given an hwKdPoint.
    size_t GetCoincidentItems(
                //! Position to search for coincident items
                const hwKdPoint<hwKdCoord,N> &pos,
                //! Items found
                CONTAINER &items, 
                //! Tolerance to consider an item as coincident
                hwKdCoord tolerance = 0.0
                ) const { return Search( pos - hwKdPoint<hwKdCoord,N>(tolerance), 
                                   pos + hwKdPoint<hwKdCoord,N>(tolerance), items ); }
    //! Prevent copying.  There is currently no deep copy definition implemented.  (To Do.)
    //! But hiding these causes build errors in afcmesh (copy construction of MidMeshThicknessMapperNameSpace due to RangeSearcher)
    //and hc (hc/src/replnew_info.cpp).
    hwKdTree( const hwKdTree& )  { assert(NULL); }
    hwKdTree& operator = ( const hwKdTree& )  { assert(NULL); return *this; }

//HwConvert {{
private :
    //! Build the tree
    void BuildTree() const;
    //! Delete the tree
    void DeleteTree() const { if( theTree_ ) { delete theTree_; theTree_ = 0; } dirty_ = true; }

    //! The actual tree
    mutable hwiKdTree<hwKdCoord, hwKdDist, N>* theTree_;
    //! dirty flag to see if tree need to be rebuilt
    mutable bool dirty_;
    //! Items in the tree
    CONTAINER items_;
    //! Actual coordinates extracted from items_
    mutable hwKdPointList pts_;
    //! Number of points at each leaf Node. As bucket size increase, tree construction
    //! time reduces and search time increases. Default value is 1024 points.
    size_t bktSize_;
};
//typedef hwKdTree<hwTriple, double, double,3, hwStlVector<hwTriple>, hwKdTreeFiller3D<hwTriple, double, 3> >  hwTripleTree;
typedef hwKdTree<hwTriple, double, double,3, STD::vector<hwTriple>, hwKdTreeFiller3D<hwTriple, double, 3> >  hwTripleTree;
//HwConvert }}

#include "math/spatialization/hwKdTree.cc" // include implementation for template class in header itself

#endif // __HWKDTREE_H__

