////////////////////////////////////////////////////////////////////
// File	  : hwKdTree.cc
// Date	  : 16 August, 2004, jnellikk -- generated
// Copyright (c) 2003 Altair Engineering Inc.  All Rights 
// Reserved.  Contains trade secrets of Altair Engineering, Inc.  
// Copyright notice does not imply publication.  Decompilation or 
// disassembly of this software is strictly prohibited.
////////////////////////////////////////////////////////////////////
//:---------------------------------------------------------------------------
//:Description
//
//  Implementation of a class that represents a kd tree capable of returning
//  nearest neighbor, range search, coincident point etc. in k dimensions
//
//:---------------------------------------------------------------------------

//! Constructor for 3d trees where T implements GetX(), GetY() & GetZ().
//! As bucket size increases, tree construction time reduces but
//! search time increases. 1024 is simply a default value chosen as a compromise
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::hwKdTree( const size_t bktSize )
: theTree_(0), dirty_(true), bktSize_(bktSize)
{
}

//! Get the nearest neighbor given an xyz coordinate.
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
T hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::GetNn(hwKdCoord x, hwKdCoord y, hwKdCoord z, hwKdDist *dist) const
{
	hwKdCoord pt[3];
	pt[0] = x; pt[1] = y; pt[2] = z;
	hwKdPoint<hwKdCoord, 3> search_pt(pt);

	return GetNn( search_pt, dist );
}

//! Get the nearest neighbor given an xyz coordinate.
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
T hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::GetNn(const hwTriple &pos, hwKdDist *dist) const
{
	hwKdCoord pt[3];
	pt[0] = pos.GetX(); pt[1] = pos.GetY(); pt[2] = pos.GetZ();
	hwKdPoint<hwKdCoord, 3> search_pt(pt);

	return GetNn( search_pt, dist );
}

//! Get the nearest neighbor given an hwKdPoint.
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
T hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::GetNn( const hwKdPoint<hwKdCoord,N>& pt, hwKdDist *dist) const
{
	if(items_.size() == 0) 
        return T();
	BuildTree();

	// Get one neighbor
	hwKdDist distance_array[1];
	hwKdIdx index_array[1];
	theTree_->Nn(pt, 1, index_array, distance_array);
	
	// Populate the return values
	if( index_array[0] == hwiKdTree<hwKdCoord, hwKdDist, N>::GetNullIdx() )
		return T();
	if(dist)
		*dist = sqrt(distance_array[0]);
	return items_[index_array[0]];
}

//! Get specified number of nearest neighbours given an hwTriple.
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
size_t hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::GetNn(
						const hwTriple &pos,
						size_t number_nearest_neighbors,
						CONTAINER &neighbors,
            STD::vector<hwKdDist> *distances /*hwStlVector<hwKdDist> *distances*/
						) const
{	
	hwKdCoord pt[3];
	pt[0] = (hwKdCoord)pos.GetX(); 
	pt[1] = (hwKdCoord)pos.GetY(); 
	pt[2] = (hwKdCoord)pos.GetZ();
	hwKdPoint<hwKdCoord, 3> search_pt(pt);
	
	return GetNn( search_pt, number_nearest_neighbors, neighbors, distances );
}

//! Get specified number of nearest neighbours given an hwKdPoint.
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
size_t hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::GetNn(
						const hwKdPoint<hwKdCoord,N> &pos,
						size_t number_nearest_neighbors,
						CONTAINER &neighbors,
            STD::vector<hwKdDist> *distances /*hwStlVector<hwKdDist> *distances*/
						) const
{	
	if(items_.size() == 0) return 0;
	BuildTree();

	hwKdDist* distance_array = new hwKdDist[number_nearest_neighbors];
	hwKdIdxArray index_array = new hwKdIdx[number_nearest_neighbors];
	theTree_->Nn(pos, number_nearest_neighbors, index_array, distance_array);
	
	size_t retVal = 0;
	for(size_t i=0; i<number_nearest_neighbors; ++i) {
		if( index_array[i] == hwiKdTree<hwKdCoord,hwKdDist,N>::GetNullIdx() ) break; // no more valid neighbours
		
		neighbors.push_back( items_[index_array[i]] );
		if( distances ) distances->push_back( sqrt(distance_array[i]) );
		++retVal;
	}
	if( distance_array ) delete [] distance_array;
	if( index_array ) delete [] index_array;
	return retVal;
}

//! Perform range search given hwTriples.
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
size_t hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::Search(
						const hwTriple &bottom_left,
						const hwTriple &top_right,
						CONTAINER &items
						) const
{
	hwKdCoord pt[3];
	pt[0] = (hwKdCoord)bottom_left.GetX(); 
	pt[1] = (hwKdCoord)bottom_left.GetY(); 
	pt[2] = (hwKdCoord)bottom_left.GetZ();
	hwKdPoint<hwKdCoord, 3> search_pt1(pt);

	pt[0] = (hwKdCoord)top_right.GetX(); 
	pt[1] = (hwKdCoord)top_right.GetY(); 
	pt[2] = (hwKdCoord)top_right.GetZ();
	hwKdPoint<hwKdCoord, 3> search_pt2(pt);
	
	return Search( search_pt1, search_pt2, items );
}

//! Perform range search given hwTriples.
// ACCEPTS an additional argument : A temporary container used only by the internals of the code.
// If this container can be re-used across multiple calls of this function, a lot of performance gain can 
// be observed by avoiding reallocations due to vector push_back.
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
size_t hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::Search(
						const hwTriple &bottom_left,
						const hwTriple &top_right,
						CONTAINER &items,
                        hwKdIdxList &index_array
						) const
{
	hwKdCoord pt[3];
	pt[0] = (hwKdCoord)bottom_left.GetX(); 
	pt[1] = (hwKdCoord)bottom_left.GetY(); 
	pt[2] = (hwKdCoord)bottom_left.GetZ();
	hwKdPoint<hwKdCoord, 3> search_pt1(pt);

	pt[0] = (hwKdCoord)top_right.GetX(); 
	pt[1] = (hwKdCoord)top_right.GetY(); 
	pt[2] = (hwKdCoord)top_right.GetZ();
	hwKdPoint<hwKdCoord, 3> search_pt2(pt);
	
	return Search( search_pt1, search_pt2, items, index_array );
}

//! Perform range search given hwKdPoints.
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
size_t hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::Search(
						const hwKdPoint<hwKdCoord,N> &bottom_left,
						const hwKdPoint<hwKdCoord,N> &top_right,
						CONTAINER &items
						) const
{
	if(items_.size() == 0) return 0;
	BuildTree();

	// parform range serach
	hwKdIdxList index_array;
	theTree_->Search(bottom_left, top_right, index_array);
	
	// populate the return list with actual items
	for(size_t i=0; i<index_array.size(); ++i)	
		items.push_back( items_[index_array[i]] );
	
	return index_array.size();
}

//! Perform range search given hwKdPoints. 
// ACCEPTS an additional argument : A temporary container used only by the internals of the code.
// If this container can be re-used across multiple calls of this function, a lot of performance gain can 
// be observed by avoiding reallocations due to vector push_back.
template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
size_t hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::Search(
						const hwKdPoint<hwKdCoord,N> &bottom_left,
						const hwKdPoint<hwKdCoord,N> &top_right,
						CONTAINER &items,
                        hwKdIdxList &index_array
						) const
{
	if(items_.size() == 0) return 0;
	BuildTree();

	// perform range serach
    index_array.clear();
	theTree_->Search(bottom_left, top_right, index_array);
	
	// populate the return list with actual items
	for(size_t i=0; i<index_array.size(); ++i)	
		items.push_back( items_[index_array[i]] );
	
	return index_array.size();
}

template <typename T, typename hwKdCoord, typename hwKdDist, int N, typename CONTAINER, typename FILLER>
void hwKdTree<T, hwKdCoord, hwKdDist, N,CONTAINER,FILLER>::BuildTree() const
{
	if(! dirty_) return;
	DeleteTree();
	dirty_ = false;
    if(items_.size() == 0) return;
    pts_.clear();
    pts_.reserve(items_.size());
	hwKdPoint<hwKdCoord, N> p;
        typename CONTAINER::const_iterator it = items_.begin();
        while(it != items_.end())
        {
          FILLER(*it, p);
          pts_.push_back(p);
          ++it;
        }
	theTree_ = new hwiKdTree<hwKdCoord, hwKdDist, N>(pts_, bktSize_);
}

/**
* Class representing implementation of the real KD tree.
* There are many inner classes defined in this class.
**/
template <typename hwKdCoord, typename hwKdDist, int N> class hwiKdTree
{
private:
	typedef STD::vector< hwKdPoint<hwKdCoord,N> > hwKdPointList;
protected:
	//! forward declarations
	class hwKdResult;
	class hwKdBBox;

/**
* An abstract base class for a kdTree Node
**/
	class hwKdNode
	{
	public:
		//! Default constructor
		hwKdNode() {;}
		//! Virtual Destructor
		virtual ~hwKdNode() {;}
		//! pure virtual to search all nodes of the tree for nearest neighbour
		virtual void NbrSearch(
						//! distance from point to box
						hwKdDist	boxDist, 
						//! query point
						const hwKdPoint<hwKdCoord, N>	queryPt,
						//! number of near neighbors requested
						const size_t	numNbr,	
						//! point cloud to search in
						const hwKdPointList& pts, 
						//! number of dimensions
						const size_t dim,
						//! other node is visited if boxDist*(1+eps)*(1+eps) > current Max distance
						const double	eps,
						//! neighbors found so far. sorted on distance.
						hwKdResult&		resultPts 
							) = 0;
		//! pure virtual to perform range search
		virtual void RangeSearch(
						//! Bounding Box to search for points
						const hwKdBBox&	bbox,
						//! point cloud to search in
						const hwKdPointList& pts, 
						//! number of dimensions
						const size_t dim,
						//! points found so far.
						hwKdIdxList& resultPts 						
							) = 0;
	};

/**
* A leaf Node for the kdTree
**/
	class hwKdLeafNode : public hwKdNode
	{
	public:
		//! Constructor taking number of points in the bucket and bucket of points for this node
		hwKdLeafNode( size_t numPts, hwKdIdxArray bucket) : numPts_(numPts),  bucket_(bucket) {;}
		//! Virtual Destructor. Do nothing
		virtual ~hwKdLeafNode() {;}
		//! Search the tree for nearest neighbours
		virtual void NbrSearch(hwKdDist	boxDist, const hwKdPoint<hwKdCoord, N>	queryPt,const size_t	numNbr,
							   const hwKdPointList& pts, const size_t dim,const double	eps, hwKdResult& resultPts );
		//! perform range search
		virtual void RangeSearch(const hwKdBBox&	bbox, const hwKdPointList& pts, 
								 const size_t dim, hwKdIdxList& resultPts );
	private:
		//! no. points in bucket
		size_t	numPts_;
		//! bucket of points
		hwKdIdxArray bucket_;	
	};
	
/**
* A split node for the kdTree
**/
	class hwKdSplitNode : public hwKdNode
	{
	public:
		//! Constructor
		hwKdSplitNode(
						//! cutting dimension
						size_t cutDim,
						//! cutting value along cutting dimesion
						hwKdCoord cutVal,
						//! lower bound along cutting dimension
						hwKdCoord lv,
						//! upper bound along cutting dimension
						hwKdCoord hv,
						//! left child
						hwKdNode* lc=NULL, 
						//! right child
						hwKdNode* rc=NULL	
					  ): cutDim_( cutDim ), cutVal_(cutVal)
		{
			cdBnds_[0] = lv; cdBnds_[1] = hv;
			child_[0]	= lc; child_[1]	= rc;
		}
		//! Virtual Destructor
		virtual ~hwKdSplitNode()
		{
			if (child_[0]!= NULL) delete child_[0];
			if (child_[1]!= NULL) delete child_[1];
		}
		//! Search the tree for nearest neighbours
		virtual void NbrSearch(hwKdDist	boxDist, const hwKdPoint<hwKdCoord, N>	queryPt,const size_t	numNbr,
							   const hwKdPointList& pts, const size_t dim,const double	eps, hwKdResult& resultPts );
		//! perform range search
		virtual void RangeSearch(const hwKdBBox&	bbox, const hwKdPointList& pts,
								 const size_t dim, hwKdIdxList& resultPts );
    private:
		//! dim orthogonal to cutting plane
		size_t		cutDim_;
		//! location of cutting plane
		hwKdCoord	cutVal_;
		//! lower and upper bounds along cutDim_
		hwKdCoord	cdBnds_[2];
		//! left and right children
		hwKdNode* 	child_[2];	
	};
	
/**
* A class representing the bounding Box
**/
	class hwKdBBox {
	public:
		//! Constructor taking dimension and high and low value for all dimensions
		hwKdBBox( hwKdCoord l=0, hwKdCoord h=0) { low_ = l; high_ = h; }
		//! (almost a) copy constructor
		hwKdBBox(const hwKdBBox &r) { low_ = r.low_; high_ = r.high_; }
		//! Constructor taking the bounding points
		hwKdBBox(hwKdPoint<hwKdCoord, N> low, hwKdPoint<hwKdCoord, N> high) {low_ = low; high_ = high; }
		//! Destructor
		~hwKdBBox() {;}
		//! Check if point p is inside the bounding Box. Return true if yes, false if not.
		bool Inside(const size_t dim, const hwKdPoint<hwKdCoord, N> p) const;
		//! Get the maximum length side
		hwKdCoord GetMaxLength( const size_t dim ) const;
		//! Get the lower bound
		const hwKdPoint<hwKdCoord, N>& Low() const { return low_; }
		//! Get the lower bound
		hwKdPoint<hwKdCoord, N>& Low() { return low_; }
		//! Get the upper bound
		const hwKdPoint<hwKdCoord, N>& High() const { return high_; }
		//! Get the upper bound
		hwKdPoint<hwKdCoord, N>& High() { return high_; }
	private:
		//! Lower bound of the BBox
		hwKdPoint<hwKdCoord, N>	low_;
		//! Upper bound of the BBox
		hwKdPoint<hwKdCoord, N>	high_;
	};
	

/**
* A class to store results (index and distance) during searching
* Uses insertion sort to keep the results sorted
* Ineffecient, but not many neighbours expected to serach for.
* Otherwise, can use hwValSortedVector. But adds dependency
**/
	class hwKdResult
	{
	public:
		//! constructor (given max size)
		hwKdResult(const size_t maxSize) : maxSize_(maxSize), size_(0) { results_ = new node[maxSize_+1]; }
		//! destructor
		~hwKdResult(){ if(results_) delete [] results_; size_ = 0; maxSize_ = 0; }
		//! return minimum distance
		hwKdDist MinDist() const { return (size_ > 0 ? results_[0].dist : hwiKdTree::GetNullDist()); }
		//! return maximum distance
		hwKdDist MaxDist() const { return (size_ > 0 ? results_[size_-1].dist : hwiKdTree::GetNullDist()); }
		//! Return the ith distance
		hwKdDist Dist(const size_t i) const { return (i < size_ ? results_[i].dist : hwiKdTree::GetNullDist()); }
		//! Return the ith index
		hwKdIdx Idx(const size_t i) const { return (i < size_ ? results_[i].idx : hwiKdTree::GetNullIdx()); }
		//! Insert distance and index. Uses insertion sort. But not many neighbours expected to find.
		void Insert( const hwKdDist dist, const hwKdIdx idx);
		//! Get the number of results stored so far
		size_t Size() { return size_; }
	private:
		//! A search result node to hold the distance and index of a neighbor
		struct node {hwKdDist dist; hwKdIdx idx; };
		//! Number of neighbours requested is the maximum size
		size_t		maxSize_;
		//! Number of result nodes currently stored
		size_t		size_;
		//! Array of result nodes. Sorted from minimum to maximum
		node	*results_;
	};

/**
* Actual hwiKdTree members
**/
public:
	//! Constructor. Builds the tree from a point array
    hwiKdTree(
			//! The point cloud that make up the tree
			hwKdPointList& pts,
			//! number of points in a leaf node. Default is 1
			size_t		bktSize = 1024	
			);
	//! Destructor
    virtual ~hwiKdTree();
	//! Search and return nearest neighbours.
	//! If more neighbours requested than points in the array, indices and distances at the
	//! end of the return arrays are invalid. Use GetNullIdx() and GetNullDist() to check.
    virtual void Nn(
						//! Query point
						const hwKdPoint<hwKdCoord, N>	queryPt,
						//! Number of near neighbors requested
						const size_t	numNbr,	
						//! Nearest neighbor array (returned)
						hwKdIdxArray	nbrIdxs,	
						//! Distances to nearest neighbors (returned)
						hwKdDist*		nbrDists,
						//! Value used in determining if other node of a aplitting node need to be searched.
						const double	eps=0.0
						);
	// Search for points in the tree that fall inside the bbox
	void Search(const hwKdPoint<hwKdCoord, N> low, const hwKdPoint<hwKdCoord, N> high, hwKdIdxList& resultPts);
	//! Define a null distance. If found on distances returned after Nn search, no more neighbours
	static hwKdDist GetNullDist() { return -1.0; }
	//! Define an invalid index. If found on indices returned after Nn search, no more neighbours
	static int    GetNullIdx() { return -1; }

protected:
	//  BoxDistance - utility routine which computes distance from a point to bounding box
	static hwKdDist BoxDistance(const hwKdPoint<hwKdCoord, N> query, const hwKdPoint<hwKdCoord, N>	BoxLow, const hwKdPoint<hwKdCoord, N>	BoxHigh, const size_t dim);
	//! Get the enclosed rectangle for the points in the tree
	void GetBBox( hwKdBBox& bbox );
	//! Create the actual tree recursively. Returns the root of the tree created
	//! In the recursive call, only indices, number of points and bounding box values change
	hwKdNode* CreateKdTree( hwKdIdxArray idxs, const size_t numPts, hwKdBBox& bbox );
	//! Split points using sliding mid point rule. If no points on one side of the mid point,
	//! slide the mid point to min or max along cut dimension as appropriate.
	//! The indices get rearranged in the process.
	void SlidingMidPointSplit( 
								hwKdIdxArray idxs, 
								const hwKdBBox& bbox, 
								const size_t numPts, 
								size_t& cutDim, 
								hwKdCoord& cutVal, 
								size_t& numLowPts );
	
	//! calculate spread for points with given indices in given dimension
	hwKdCoord Spread(const hwKdIdxArray idxs, const size_t numPts, const size_t dim) const; 
	//! calculate max min for points with given indices in given dimension
	void MinMax(const hwKdIdxArray idxs, const size_t numPts, const size_t dim, hwKdCoord& min, hwKdCoord& max) const;
	//!	Split the points in an array about a given plane along a given cutting dimension.
	//!	Indexing is done indirectly through the index array.
	void PlaneSplit(
					//! indices to point cloud to split
					const hwKdIdxArray idxs,
					//! number of points to split
					const int numPts,
					//! dimension along which to split
					const size_t dim,
					//! cutting value
					const hwKdCoord cutVal,	
					//! first break such that values < cv
					size_t& br1,
					//! second break such that values == cv
					size_t& br2 );		

	//! Number of points to store in a leaf node
    size_t			bktSize_;
	//! The point cloud that make up the tree
    hwKdPointList*	pts_;
	//! Indices to the points. All point rearrangements are done indirectly using these indices.
    hwKdIdxArray	pidx_;
	//! Root node of the tree
    hwKdNode* 		root_;
	//! Bounding box low point (for all points in tree)
    hwKdPoint<hwKdCoord,N>		bndBoxLo_;	
	//! Bounding box high point (for all points in tree)
    hwKdPoint<hwKdCoord,N>		bndBoxHi_;
};

#include "math/spatialization/hwiKdTree.cc" // include implementation of hwiKdTree class and subclasses
