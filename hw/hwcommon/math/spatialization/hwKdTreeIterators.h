////////////////////////////////////////////////////////////////////
// File      : hwKdTreeIterators.h
// Date      : 6/27/2007
// Copyright (c) 2007 Altair Engineering Inc.  All Rights 
// Reserved.  Contains trade secrets of Altair Engineering, Inc.  
// Copyright notice does not imply publication.  Decompilation or 
// disassembly of this software is strictly prohibited. 
////////////////////////////////////////////////////////////////////
//:---------------------------------------------------------------------------
//:Description
//
//  Iterators for hwKdTree
//
//:---------------------------------------------------------------------------

#ifndef __HWKDTREE_ITERATORS_H__
#define __HWKDTREE_ITERATORS_H__

#include <math/spatialization/hwKdTree.h>

// -------------------------------------------------------------------
//! \brief Iterator for hwKdTree
//! 
//! \code
//! hwKdTree<nodepointer, double, double, 3, NodePtrVect, NodeCordinate> tree;
//! hwKdTreeIterator<nodepointer, double, double, 3, NodePtrVect, NodeCordinate> iter(tree);
//! while( !iter.End() ) {
//!		++iter; // do other stuff
//! }
//! \endcode
// -------------------------------------------------------------------
template <
			typename T,
			typename hwKdCoord = float,
			typename hwKdDist = float,
			int N = 3,
			typename CONTAINER = STD::vector<T>,
			typename FILLER = hwKdTreeFiller<T, hwKdCoord, N>
		>
class hwKdTreeIterator
{
public:
	//! Constructor
	hwKdTreeIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>(hwKdTree<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER> &tree) : p_tree(&tree) {Begin();}
	//! Destructor
	~hwKdTreeIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>() {;}
	
	//! Rewind to the beginning
	void Begin() { p_iterator = p_tree->items_.begin(); }
	//! Advance to next element.
	void Advance() { ++p_iterator; }
	//! Advance to next element.
	void operator++() { ++p_iterator; }
	//! See if reached the end of iteration
	bool End() const { return p_iterator == p_tree->items_.end(); }
	//! Get the value
	const T& operator*() const { return *p_iterator; }
	//! Get reference to the value. Can use as lvalue
	T& operator*() { return *p_iterator; }
private:
	//! post increment is disallowed
	void operator++(int);
	//! Copy constructor is disallowed for now
	hwKdTreeIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>(const hwKdTreeIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>& rhs);
	//! assignment operator is disallowed for now
	hwKdTreeIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>& operator=(const hwKdTreeIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>& rhs);

	//! The tree iterating over
	hwKdTree<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER> *p_tree;
	//! The actual iterator
  typename CONTAINER::iterator p_iterator;
};

// -------------------------------------------------------------------
//! \brief const iterator for hwKdTree
//! 
//! \code
//! hwKdTree<nodepointer, double, double, 3, NodePtrVect, NodeCordinate> tree;
//! hwKdTreeConstIterator<nodepointer, double, double, 3, NodePtrVect, NodeCordinate> iter(tree);
//! while( !iter.End() ) {
//!		++iter; // do other stuff
//! }
//! \endcode
// -------------------------------------------------------------------
template <
			typename T,
			typename hwKdCoord = float,
			typename hwKdDist = float,
			int N = 3,
			typename CONTAINER = STD::vector<T>,
			typename FILLER = hwKdTreeFiller<T, hwKdCoord, N>
		>
class hwKdTreeConstIterator
{
public:
	//! Constructor
	hwKdTreeConstIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>(const hwKdTree<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>& tree) : p_tree(&tree) {Begin();}
	//! Destructor
	~hwKdTreeConstIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>() {;}

	//! Rewind to the beginning
	void Begin() { p_iterator = p_tree->items_.begin(); }
	//! Advance to next element.
	void Advance() { ++p_iterator; }
	//! Advance to next element.
	void operator++() { ++p_iterator; }
	//! See if reached the end of iteration
	bool End() const { return p_iterator == p_tree->items_.end(); }
	//! Get the value
	const T& operator*() const { return *p_iterator; }
	//! Get the value
private:
	//! post increment is disallowed
	void operator++(int);
	//! Copy constructor is disallowed for now
	hwKdTreeConstIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>(const hwKdTreeConstIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>& rhs);
	//! assignment operator is disallowed for now
	hwKdTreeConstIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>& operator=(const hwKdTreeConstIterator<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>& rhs);

	//! The tree iterating over
	const hwKdTree<T,hwKdCoord,hwKdDist,N,CONTAINER,FILLER>* p_tree;
	//! The actual iterator
	typename CONTAINER::const_iterator p_iterator;
};

#endif // __HWKDTREE_ITERATORS_H__
