////////////////////////////////////////////////////////////////////
// File	  : hwiKdTree.cc
// Date	  : 16 August, 2004, jnellikk -- generated
// Copyright (c) 2003 Altair Engineering Inc.  All Rights 
// Reserved.  Contains trade secrets of Altair Engineering, Inc.  
// Copyright notice does not imply publication.  Decompilation or 
// disassembly of this software is strictly prohibited.
////////////////////////////////////////////////////////////////////
//:---------------------------------------------------------------------------
//:Description
//
//  Implementation of hwiKdTree class
//
//:---------------------------------------------------------------------------

//! Constructor to build the tree from a point array
template <typename hwKdCoord, typename hwKdDist, int N>
hwiKdTree<hwKdCoord, hwKdDist, N>::hwiKdTree(
			hwKdPointList&	pts,	 // point array 
			size_t		bktSize		 // number of points in a leaf node. Default to 1
			)
:bktSize_(bktSize), pts_(&pts), pidx_(0), root_(0)
{
	// If there are no points, no more construction work needed
	if( pts_->size() == 0 ) return;
	
	// Allocate index array and assign the indices for the given set of points
	pidx_ = new hwKdIdx[pts_->size()];
	for( size_t inx = 0; inx < pts_->size(); inx++ ) pidx_[inx] = (hwKdIdx)inx; 

	// Calculate the bounding box for the given points
	hwKdBBox bbox;
	GetBBox( bbox );
	bndBoxLo_ = bbox.Low();
	bndBoxHi_ = bbox.High();

	//! Create the actual tree recursively.
	root_ = CreateKdTree( pidx_, pts_->size(), bbox );
}

//! Destructor
template <typename hwKdCoord, typename hwKdDist, int N>
hwiKdTree<hwKdCoord, hwKdDist, N>::~hwiKdTree()
{
    if (root_) delete root_;
    if (pidx_) delete [] pidx_;
}

//! Search and return nearest neighbours
template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::Nn(
					const hwKdPoint<hwKdCoord,N>	queryPt,		// query point
					const size_t	numNbr,			// number of near neighbors to return
					hwKdIdxArray	nbrIdxs,	// nearest neighbor array (returned)
					hwKdDist*		nbrDists,	// dist to near neighbors (returned)
					const double	eps
					)
{
	if( !root_ ) return;

	// Even if request is made for more neighbours than points in tree, don't return
	// so that we get all the points as neighbours with correct distances.
    //if (numNbr > pts_->size()) { return; }

    hwKdResult resultPts(numNbr);	// create storage for neighbours
	
	// search starting at the root
    root_->NbrSearch(BoxDistance(queryPt, bndBoxLo_, bndBoxHi_, N),
		             queryPt, numNbr, *pts_, N, eps, resultPts );

	// extract the results to output structures
    for (size_t inx = 0; inx < numNbr; inx++) {
		nbrDists[inx] = resultPts.Dist(inx); // could be infinite distance if not numNbr neighbours
		nbrIdxs[inx] = resultPts.Idx(inx);	 // could be -ve if not numNbr neighbours found
    }
}

// Search for points in the tree that fall inside the bbox
template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::Search(const hwKdPoint<hwKdCoord,N> low, const hwKdPoint<hwKdCoord,N> high, hwKdIdxList& resultPts)
{
		if( !root_ ) return;
		hwKdBBox bbox( low, high );
		root_->RangeSearch( bbox, *pts_, N, resultPts );
}

//! Get the enclosed rectangle for the points in the tree
template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::GetBBox( hwKdBBox& bbox )
{
	hwKdPoint<hwKdCoord,N>& bBoxLo = bbox.Low();
	hwKdPoint<hwKdCoord,N>& bBoxHi = bbox.High();

	for( size_t inx = 0; inx < N; inx++ ) {
		hwKdCoord min = (*pts_)[0][inx];
		hwKdCoord max = min;

		for( size_t jnx = 0; jnx < pts_->size(); jnx++ ) {
			if( (*pts_)[jnx][inx] < min ) min = (*pts_)[jnx][inx];
			else if( (*pts_)[jnx][inx] > max ) max = (*pts_)[jnx][inx];
		}

		bBoxLo[inx] = min; // store min in this dimension
		bBoxHi[inx] = max;	// store max in this dimension
	}
}

//! Create the actual tree recursively. Returns the root of the tree created
//! In the recursive call, only indices, number of points and bounding box values change
template <typename hwKdCoord, typename hwKdDist, int N>
typename hwiKdTree<hwKdCoord, hwKdDist, N>::hwKdNode* 
hwiKdTree<hwKdCoord, hwKdDist, N>::CreateKdTree( hwKdIdxArray idxs, const size_t numPts, hwKdBBox& bbox )
{
	if( numPts <= bktSize_ ) {	// numPts smaller than bucket size, make a leaf node
		if (numPts == 0)		// empty leaf node
			return NULL;		// return null leaf
		else	// construct the leaf node and return
			return new hwKdLeafNode(numPts, idxs); 
    }
    else {	// numPts more than bucket size, make a splitting node
		size_t cutDim;				// cutting dimension
		hwKdCoord cutVal;			// cutting value
		size_t numLowPts;			// number on low side of cut
		hwKdNode  *left, *right;// left and right children

		// invoke splitting procedure
		SlidingMidPointSplit(idxs, bbox, numPts, cutDim, cutVal, numLowPts);

		hwKdCoord lv = (bbox.Low())[cutDim];	// save bounds for cutting dimension
		hwKdCoord hv = (bbox.High())[cutDim];

		// build left subtree for points in index range 0 to numLowPts-1.
		(bbox.High())[cutDim] = cutVal;		// modify bounds for left subtree
		left = CreateKdTree( idxs, numLowPts, bbox );
		(bbox.High())[cutDim] = hv;		// restore bounds

		// build right subtree for points in index range numLowPts to numPts-1.
		(bbox.Low())[cutDim] = cutVal;		// modify bounds for right subtree
		right = CreateKdTree(idxs+numLowPts, numPts-numLowPts, bbox );
		(bbox.Low())[cutDim] = lv;		// restore bounds

		// create the splitting node
		return new hwKdSplitNode(cutDim, cutVal, lv, hv, left, right);
    }
}

//! Split points using sliding mid point rule.
//! The indices get rearranged in the process
template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::SlidingMidPointSplit(
						hwKdIdxArray idxs, 
						const hwKdBBox& bbox, 
						const size_t numPts, 
						size_t& cutDim, 
						hwKdCoord& cutVal, 
						size_t& numLowPts )
{
	hwKdCoord maxLen = bbox.GetMaxLength( N ); // get longest side of bounding box
	double ERR = 0.001;

	// find long side with most spread
	hwKdCoord maxSpread = -1;		
	for( size_t inx = 0; inx < N; inx++ ) {
		// is it among longest? If more sides close to longest side, get one with max spread
		if (double( (bbox.High())[inx] - (bbox.Low())[inx] ) >= (1-ERR)*maxLen) {
			hwKdCoord spr = Spread(idxs, numPts, inx); // calculate spread
			if (spr > maxSpread) { maxSpread = spr; cutDim = inx;}
		}
    }

	// ideal split at midpoint
    hwKdCoord idealCutVal = ((bbox.Low())[cutDim] + (bbox.High())[cutDim])/2;
	// Slide to min or max if needed
    hwKdCoord min, max; 
	MinMax(idxs, numPts, cutDim, min, max);	// find min/max coordinates in cut dimension
    if (idealCutVal < min) cutVal = min;
    else if (idealCutVal > max) cutVal = max;
	else cutVal = idealCutVal;

	// permute points accordingly
    size_t br1, br2;
    PlaneSplit(idxs, (int)numPts, cutDim, cutVal, br1, br2);
    
	//	if idealCutVal < min (implying br2 >= 1),
    //		then we select n_lo = 1 (so there is one point on left) and
    //  if idealCutVal > max (implying br1 <= n-1),
    //		then we select n_lo = n-1 (so there is one point on right).
    //	Otherwise, we select n_lo as close to n/2 as possible within
    //		[br1..br2].
    if (idealCutVal < min) numLowPts = 1;
    else if (idealCutVal > max) numLowPts = numPts-1;
    else if (br1 > numPts/2) numLowPts = br1;
    else if (br2 < numPts/2) numLowPts = br2;
    else numLowPts = numPts/2;
}

//! calculate spread
template <typename hwKdCoord, typename hwKdDist, int N>
hwKdCoord hwiKdTree<hwKdCoord, hwKdDist, N>::Spread( const hwKdIdxArray idxs, const size_t numPts, const size_t dim) const
{
	hwKdCoord min, max; 
	MinMax( idxs, numPts, dim, min, max );
    return (max - min);			// total spread is difference
}


//! calculate max min for points with given indices
template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::MinMax(const hwKdIdxArray idxs, const size_t numPts, const size_t dim, hwKdCoord& min, hwKdCoord& max) const
{
	min = (*pts_)[idxs[0]][dim];		// compute max and min coords
    max = min;
    
	for (size_t inx = 1; inx < numPts; inx++) {
		hwKdCoord c = (*pts_)[idxs[inx]][dim];
		if (c < min) min = c;
		else if (c > max) max = c;
    }
}

template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::PlaneSplit(
					const hwKdIdxArray idxs,		// point indices
					const int numPts,		// number of points
					const size_t dim,		// dimension along which to split
					const hwKdCoord cutVal,		// cutting value
					size_t& br1,		// first break (values < cv)
					size_t& br2		// second break (values == cv)
						   )
{
    int left = 0;		 // left position
    int right = numPts-1; // right position

	// partition points [0..n-1] about cutVal
	for(;;) {
		// go upto first point on the left that is >= cutVal
		while (left < numPts && (*pts_)[idxs[left]][dim] < cutVal) left++;
		// go upto the first point on the right that is < cutVal
		while (right >= 0 && (*pts_)[idxs[right]][dim] >= cutVal) right--;
		// if left is past right, we are all done.
		if (left > right) break;
		// swap left and right points and increment left and right.
        std::swap(idxs[left], idxs[right]);
		left++; right--;
    }
    br1 = left;			// now: pa[0..br1-1] < cv <= pa[br1..n-1]
	
	// partition pa[br1..n-1] about cv
	right = numPts-1;	// use same left position. But reset right to maximum right.
    for(;;) {
		// go upto the next point on left that is > cutVal
		while (left < numPts && (*pts_)[idxs[left]][dim] <= cutVal) left++;
		// go upto the next point on the right that is <= cutVal
		while (right >= (int)br1 && (*pts_)[idxs[right]][dim] > cutVal) right--;
		// if left is past right, we are all done.
		if (left > right) break;
		// swap left and right points and increment left and right.
        std::swap(idxs[left], idxs[right]);
		left++; right--;
    }
    br2 = left;			// now: pa[br1..br2-1] == cv < pa[br2..n-1]
}


//  BoxDistance - utility routine which computes distance from point to
//	box (Note: most distances to boxes are computed using incremental
//	distance updates, not this function.)
template <typename hwKdCoord, typename hwKdDist, int N>
hwKdDist hwiKdTree<hwKdCoord, hwKdDist, N>::BoxDistance(const hwKdPoint<hwKdCoord,N> query, const hwKdPoint<hwKdCoord,N> boxLow, const hwKdPoint<hwKdCoord,N> boxHigh, const size_t dim)
{
	hwKdDist dist = 0.0;	// sum of squared distances
    hwKdDist t;

    for (size_t inx = 0; inx < dim; inx++) {
		if (query[inx] < boxLow[inx]) {		// query is left of box
			t = hwKdDist(boxLow[inx]) - hwKdDist(query[inx]);
			dist += t*t;
		}
		else if (query[inx] > boxHigh[inx]) {	// query is right of box
			t = hwKdDist(query[inx]) - hwKdDist(boxHigh[inx]);
			dist += t*t;
		}
    }
    return dist;
}


/********************************************************************************************
*					hwiKdTree::hwKdLeafNode definitions
********************************************************************************************/
template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::hwKdLeafNode::NbrSearch(
						hwKdDist	boxDist, // not used for leaf node
						const hwKdPoint<hwKdCoord,N>	queryPt,		// query point
						const size_t	numNbr,	// number of near neighbors to return
						const hwKdPointList& pts, // point cloud to search in
						const size_t dim, // number of dimension
						const double	eps,
						hwKdResult&		resultPts) // output. neighbours found
{
    hwKdDist maxDist = resultPts.MaxDist();	// maximum distance so far
    for( size_t inx = 0; inx < numPts_; inx++) {	// check points in bucket
		hwKdPoint<hwKdCoord,N> p = pts[bucket_[inx]];		// next data point
		hwKdDist dist = 0;
		
		size_t jnx;
		for(jnx = 0; jnx < dim; jnx++) {
			hwKdDist t = hwKdDist(queryPt[jnx]) - hwKdDist(p[jnx]);
			dist += t*t;
			// exceeds dist to numNbr-th smallest?
			if( dist  > maxDist && resultPts.Size() == numNbr) break;
		}
		if( jnx < dim ) continue; // distance more than numNbr-th smallest
		
		// add it to the list. If we ever need to eliminate the coincident point from neighbors, 
		// make sure distance is not zero before adding to resultPts
		resultPts.Insert(dist, bucket_[inx]);
		maxDist = resultPts.MaxDist();
		
    }
}

//! perform range search
template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::hwKdLeafNode::RangeSearch(const hwKdBBox&	bbox, const hwKdPointList& pts, 
										  const size_t dim, hwKdIdxList& resultPts )
{
	// check if each point of this node is inside or outside the bbox.
    for( size_t inx = 0; inx < numPts_; inx++) {		
		// Add point to result vector if inside the bbox
		if( bbox.Inside( dim, pts[bucket_[inx]] ) ) 
			resultPts.push_back(bucket_[inx]);
    }
}


/********************************************************************************************
*					hwiKdTree::hwKdSplitNode definitions
********************************************************************************************/
template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::hwKdSplitNode::NbrSearch(
						hwKdDist	boxDist, 
						const hwKdPoint<hwKdCoord,N>	queryPt,		// query point
						const size_t	numNbr,	// number of near neighbors to return
						const hwKdPointList& pts, // point cloud to search in
						const size_t dim, // number of dimension
						const double	eps,
						hwKdResult&		resultPts) // output. neighbours found
{
	// distance to cutting plane
    hwKdCoord cutDiff = queryPt[cutDim_] - cutVal_;
	double maxErr = (1.0+eps)*(1.0+eps);

    if (cutDiff < 0) {	// left of cutting plane
		// visit closer child first
		if(child_[0]) child_[0]->NbrSearch(boxDist, queryPt, numNbr, pts, dim, eps, resultPts);

		hwKdCoord boxDiff = cdBnds_[0] - queryPt[cutDim_];
		if (boxDiff < 0) boxDiff = 0;	// within bounds - ignore
			
		// distance to further box
		boxDist += (hwKdDist)( cutDiff*cutDiff - boxDiff*boxDiff);

		// visit further child if close enough
		if (boxDist * maxErr < resultPts.MaxDist() || resultPts.Size() != numNbr)
			if(child_[1]) child_[1]->NbrSearch(boxDist, queryPt, numNbr, pts, dim, eps, resultPts);
    }
    else {			// right of cutting plane
		// visit closer child first
		if(child_[1]) child_[1]->NbrSearch(boxDist, queryPt, numNbr, pts, dim, eps, resultPts);

		hwKdCoord boxDiff = queryPt[cutDim_] - cdBnds_[1];
		if (boxDiff < 0) boxDiff = 0; // within bounds - ignore
		
		// distance to further box
		boxDist += (hwKdDist)( cutDiff*cutDiff - boxDiff*boxDiff);

		// visit further child if close enough
		if (boxDist * maxErr < resultPts.MaxDist() || resultPts.Size() != numNbr)
			if(child_[0]) child_[0]->NbrSearch(boxDist, queryPt, numNbr, pts, dim, eps, resultPts);
    }
}

//! perform range search
template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::hwKdSplitNode::RangeSearch(const hwKdBBox&	bbox, const hwKdPointList& pts, 
										   const size_t dim, hwKdIdxList& resultPts )
{
	hwKdPoint<hwKdCoord,N> low = bbox.Low();
	hwKdPoint<hwKdCoord,N> high = bbox.High();

	// if bbox is fully to the left side of cutting plane, search left tree only
	// if bbox is fully to the right side of the cutting plane, search right tree only
	// if bbox is on both sides of the cutting plane, search both left and right tree
	if( low[cutDim_] < cutVal_ && high[cutDim_] < cutVal_ ) { // these braces needed here..
		if(child_[0]) child_[0]->RangeSearch( bbox, pts, dim, resultPts );
	}
	else if( low[cutDim_] > cutVal_ && high[cutDim_] > cutVal_ ) { // these braces needed here..
		if(child_[1]) child_[1]->RangeSearch( bbox, pts, dim, resultPts );
	}
	else {
		if(child_[0]) child_[0]->RangeSearch( bbox, pts, dim, resultPts );
		if(child_[1]) child_[1]->RangeSearch( bbox, pts, dim, resultPts );
	}
}

/********************************************************************************************
*					hwiKdTree::hwKdBBox definitions
********************************************************************************************/
//! Check if point p is inside the bounding Box. Return true if yes, false if not.
template <typename hwKdCoord, typename hwKdDist, int N>
bool hwiKdTree<hwKdCoord, hwKdDist, N>::hwKdBBox::Inside(size_t dim, hwKdPoint<hwKdCoord,N> p) const
{
    for (size_t inx = 0; inx < dim; inx++)
		if (p[inx] < low_[inx] || p[inx] > high_[inx]) return false;
    return true;
}

template <typename hwKdCoord, typename hwKdDist, int N>
hwKdCoord hwiKdTree<hwKdCoord, hwKdDist, N>::hwKdBBox::GetMaxLength( const size_t dim ) const
{
	hwKdCoord maxLen = high_[0] - low_[0];
	for (size_t inx = 1; inx < dim; inx++) {
		hwKdCoord len = high_[inx] - low_[inx];
		if (len  > maxLen) maxLen = len;
	}
	return maxLen;
}


/********************************************************************************************
*					hwiKdTree::hwKdResult definitions
********************************************************************************************/
//! Insert distance and index. Uses insertion sort
template <typename hwKdCoord, typename hwKdDist, int N>
void hwiKdTree<hwKdCoord, hwKdDist, N>::hwKdResult::Insert( const hwKdDist dist, const hwKdIdx idx)
{
	size_t i;
	// slide larger values up
	for (i = size_; i > 0; i--) {
		if (results_[i-1].dist > dist)
			results_[i] = results_[i-1];
		else break;
	}
	results_[i].dist = dist;	// store element here
	results_[i].idx  = idx;
	if (size_ < maxSize_) size_++;
}


