////////////////////////////////////////////////////////////////////
// File	  : hwKdPoint.h
// Date	  : 6th Sept, 2004, champati/jnellikk -- generated
// Copyright (c) 2003 Altair Engineering Inc.  All Rights 
// Reserved.  Contains trade secrets of Altair Engineering, Inc.  
// Copyright notice does not imply publication.  Decompilation or 
// disassembly of this software is strictly prohibited.
////////////////////////////////////////////////////////////////////
//:---------------------------------------------------------------------------
//:Description
//
//  Class that defines a point for the KD Tree.
//  
//:---------------------------------------------------------------------------

#ifndef __HW_KD_POINT_H__
#define __HW_KD_POINT_H__

// -------------------------------------------------------------------
//! \class hwKdPoint hwKdPoint.h math/spatialization/hwKdPoint.h
//! \brief Class that defines a point for the KD Tree.
// -------------------------------------------------------------------

template<typename real,int N> class hwKdPoint
{
public :
	//! Default constructor
	hwKdPoint () {for(size_t i=0; i<N; ++i) point_[i] = 0;}
	//! Constructor that takes a value
	hwKdPoint (real val) {for(size_t i=0; i<N; ++i) point_[i] = val;}
	//! Constructor that takes an array of size N
	hwKdPoint (real point[N]) {for(size_t i=0; i<N; ++i) point_[i] = point[i];}
	//! Copy Constructor
	hwKdPoint (const hwKdPoint &p) {for(size_t i=0; i<N; ++i) point_[i] = p.point_[i];}
	//! Subscript operator. Can be used as lvalue. 
	real& operator[](size_t i) {return point_[i];}
	//! Subscript operator
	real operator[](size_t i) const {return point_[i];}
	//! Operator +
	hwKdPoint operator+(const hwKdPoint &p) const
	{
		hwKdPoint ret;
		for( size_t i=0; i<N; ++i ) { ret.point_[i] = point_[i] + p.point_[i]; }
		return ret;
	}
	//! Operator -
	hwKdPoint operator-(const hwKdPoint &p) const
	{
		hwKdPoint ret;
		for( size_t i=0; i<N; ++i ) { ret.point_[i] = point_[i] - p.point_[i]; }
		return ret;
	}
	//! Assignment oparetor
    hwKdPoint& operator=(real val)
	{
		for( size_t i=0; i<N; ++i ) point_[i] = val;
		return *this;
	}
	//! Assignment oparetor
	hwKdPoint& operator=(const hwKdPoint &p) 
	{
		if(this != &p) {for(size_t i=0; i<N; ++i) point_[i] = p.point_[i];}
		return *this;
	}
//	//! Oparetor <
//	bool operator<(const hwKdPoint &p) const
//	{
//		for(size_t i=0; i<N; ++i) {
//			if(point_[i] < p.point_[i]) return true;
//			else if(point_[i] > p.point_[i]) return false;
//		}
//		return false;
//	}
	//! Euality operator
	bool operator==(const hwKdPoint &p) const
	{
		for(size_t i=0; i<N; ++i) {if(point_[i] != p.point_[i]) return false;}
		return true;
	}
	//! Debug the point
	void Debug() const {
		for(size_t i=0; i<N; ++i) {printf("  pt[%d] = %g\n", i, point_[i]);}
	}
private :
	real point_[N];
};

#endif // __HW_KD_POINT_H__
