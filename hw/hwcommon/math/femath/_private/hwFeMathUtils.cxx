////////////////////////////////////////////////////////////////////
// File	  : hwFeMathUtils.cxx
// Date	  : 03 August, 2004 - Generated Bassayya Kalal
// Copyright (c) 2004 Altair Engineering Inc.  All Rights 
// Reserved.  Contains trade secrets of Altair Engineering, Inc.  
// Copyright notice does not imply publication.  Decompilation or 
// disassembly of this software is strictly prohibited.
////////////////////////////////////////////////////////////////////
//:---------------------------------------------------------------------------
//:Description
//
//  hwFeMathUtils implementation
//
//:---------------------------------------------------------------------------

#include <math/femath/hwFeMathUtils.h>
#include <math/femath/hwFacePos.h>
#include <math/basic/hwSystem.h>
#include <math/basic/hwBox.h>

//! Gets dot product's angle in degrees
double hwFeMathUtils::GetAngleBetweenVectors(const hwTriple &v1, const hwTriple &v2)
{
	double len1 = v1.Length();
	double len2 = v2.Length();
	if(!len1 || !len2) return 0.0;
	double dotProd = (v1 % v2)/(len1 * len2);
	if(dotProd >= 1.0) return 0.0;
	else if (dotProd <= -1.0) return 180.0;
	dotProd = (acos(dotProd));
	return RadianToDegree(dotProd);
}

//! Gets dot product's angle in radians
double hwFeMathUtils::GetAngleBetweenVectorsInRadians(const hwTriple &v1, const hwTriple &v2)
{
	double len1 = v1.Length();
	double len2 = v2.Length();
	if(!len1 || !len2) return 0.0;
	double dotProd = (v1 % v2)/(len1 * len2);
	if(dotProd >= 1.0) return 0.0;
	else if (dotProd <= -1.0) return hwFeMathUtils::DegreeToRadian(180.0);
	return (acos(dotProd));
}

//! Get distance between two points
double hwFeMathUtils::GetDistance(const hwTriple &v1, const hwTriple &v2)
{
	hwTriple vector(v1, v2);
	return vector.Length();
}


extern "C" {

#define FALSE 0
#define TRUE 1
#define ABS fabs

typedef struct {
   int p1,p2,p3;
} ITRIANGLE;
typedef struct {
   int p1,p2;
} IEDGE;
typedef struct {
   double x,y,z;
} XYZ;


/*
   Return TRUE if a point (xp,yp) is inside the circumcircle made up
   of the points (x1,y1), (x2,y2), (x3,y3)
   The circumcircle centre is returned in (xc,yc) and the radius r
   NOTE: A point on the edge is inside the circumcircle
*/
int _CircumCircle(double xp,double yp,
   double x1,double y1,double x2,double y2,double x3,double y3,
   double *xc,double *yc,double *r, double EPSILON)
{
   double m1,m2,mx1,mx2,my1,my2;
   double dx,dy,rsqr,drsqr;

   /* Check for coincident points */
   if (ABS(y1-y2) < EPSILON && ABS(y2-y3) < EPSILON)
       return(FALSE);

   if (ABS(y2-y1) < EPSILON) {
      m2 = - (x3-x2) / (y3-y2);
      mx2 = (x2 + x3) / 2.0;
      my2 = (y2 + y3) / 2.0;
      *xc = (x2 + x1) / 2.0;
      *yc = m2 * (*xc - mx2) + my2;
   } else if (ABS(y3-y2) < EPSILON) {
      m1 = - (x2-x1) / (y2-y1);
      mx1 = (x1 + x2) / 2.0;
      my1 = (y1 + y2) / 2.0;
      *xc = (x3 + x2) / 2.0;
      *yc = m1 * (*xc - mx1) + my1;
   } else {
      m1 = - (x2-x1) / (y2-y1);
      m2 = - (x3-x2) / (y3-y2);
      mx1 = (x1 + x2) / 2.0;
      mx2 = (x2 + x3) / 2.0;
      my1 = (y1 + y2) / 2.0;
      my2 = (y2 + y3) / 2.0;
      *xc = (m1 * mx1 - m2 * mx2 + my2 - my1) / (m1 - m2);
      *yc = m1 * (*xc - mx1) + my1;
   }

   dx = x2 - *xc;
   dy = y2 - *yc;
   rsqr = dx*dx + dy*dy;
   *r = sqrt(rsqr);

   dx = xp - *xc;
   dy = yp - *yc;
   drsqr = dx*dx + dy*dy;

	// Original
	// return((drsqr <= rsqr) ? TRUE : FALSE);
	// Suggested
	return((drsqr <= rsqr + EPSILON) ? TRUE : FALSE);

}







/*
   Triangulation subroutine
   Takes as input NV vertices in array pxyz
   Returned is a list of ntri triangular faces in the array v
   These triangles are arranged in a consistent clockwise order.
   The triangle array 'v' should be malloced to 3 * nv
   The vertex array pxyz must be big enough to hold 3 more points
   The vertex array must be sorted in increasing x values say

   qsort(p,nv,sizeof(XYZ),XYZCompare);
      :
   int XYZCompare(void *v1,void *v2)
   {
      XYZ *p1,*p2;
      p1 = v1;
      p2 = v2;
      if (p1->x < p2->x)
         return(-1);
      else if (p1->x > p2->x)
         return(1);
      else
         return(0);
   }
*/
int _Triangulate(int nv,XYZ *pxyz,ITRIANGLE *v,int *ntri, double EPSILON)
{
   int *complete = NULL;
   IEDGE *edges = NULL;
   int nedge = 0;
   int trimax,emax = 200;
   int status = 0;

   int inside;
   int i,j,k;
   double xp,yp,x1,y1,x2,y2,x3,y3,xc,yc,r;
   double xmin,xmax,ymin,ymax,xmid,ymid;
   double dx,dy,dmax;

   /* Allocate memory for the completeness list, flag for each triangle */
   trimax = 4 * nv;
   if ((complete = (int*) malloc(trimax*sizeof(int))) == NULL) {
      status = 1;
      goto skip;
   }

   /* Allocate memory for the edge list */
   if ((edges = (IEDGE *) malloc(emax*(long)sizeof(IEDGE))) == NULL) {
      status = 2;
      goto skip;
   }

   /*
      Find the maximum and minimum vertex bounds.
      This is to allow calculation of the bounding triangle
   */
   xmin = pxyz[0].x;
   ymin = pxyz[0].y;
   xmax = xmin;
   ymax = ymin;
   for (i=1;i<nv;i++) {
      if (pxyz[i].x < xmin) xmin = pxyz[i].x;
      if (pxyz[i].x > xmax) xmax = pxyz[i].x;
      if (pxyz[i].y < ymin) ymin = pxyz[i].y;
      if (pxyz[i].y > ymax) ymax = pxyz[i].y;
   }
   dx = xmax - xmin;
   dy = ymax - ymin;
   dmax = (dx > dy) ? dx : dy;
   xmid = (xmax + xmin) / 2.0;
   ymid = (ymax + ymin) / 2.0;

   /*
      Set up the supertriangle
      This is a triangle which encompasses all the sample points.
      The supertriangle coordinates are added to the end of the
      vertex list. The supertriangle is the first triangle in
      the triangle list.
   */
   pxyz[nv+0].x = xmid - 20 * dmax;
   pxyz[nv+0].y = ymid - dmax;
   pxyz[nv+0].z = 0.0;
   pxyz[nv+1].x = xmid;
   pxyz[nv+1].y = ymid + 20 * dmax;
   pxyz[nv+1].z = 0.0;
   pxyz[nv+2].x = xmid + 20 * dmax;
   pxyz[nv+2].y = ymid - dmax;
   pxyz[nv+2].z = 0.0;
   v[0].p1 = nv;
   v[0].p2 = nv+1;
   v[0].p3 = nv+2;
   complete[0] = FALSE;
   *ntri = 1;

   /*
      Include each point one at a time into the existing mesh
   */
   for (i=0;i<nv;i++) {

      xp = pxyz[i].x;
      yp = pxyz[i].y;
      nedge = 0;

      /*
         Set up the edge buffer.
         If the point (xp,yp) lies inside the circumcircle then the
         three edges of that triangle are added to the edge buffer
         and that triangle is removed.
      */
      for (j=0;j<(*ntri);j++) {
         if (complete[j])
            continue;
         x1 = pxyz[v[j].p1].x;
         y1 = pxyz[v[j].p1].y;
         x2 = pxyz[v[j].p2].x;
         y2 = pxyz[v[j].p2].y;
         x3 = pxyz[v[j].p3].x;
         y3 = pxyz[v[j].p3].y;
         inside = _CircumCircle(xp,yp,x1,y1,x2,y2,x3,y3,&xc,&yc,&r, EPSILON);
		 // Original
         //if (xc + r < xp)
		 // Suggested
		 if (xc + r + EPSILON < xp)
			complete[j] = TRUE;
         if (inside) {
            /* Check that we haven't exceeded the edge list size */
            if (nedge+3 >= emax) {
               emax += 100;
               IEDGE *local_edges = NULL;
               if ((local_edges = (IEDGE *) realloc(edges,emax*(long)sizeof(IEDGE))) == NULL) {
                  status = 3;
                  goto skip;
               }
               edges = local_edges;
            }
            edges[nedge+0].p1 = v[j].p1;
            edges[nedge+0].p2 = v[j].p2;
            edges[nedge+1].p1 = v[j].p2;
            edges[nedge+1].p2 = v[j].p3;
            edges[nedge+2].p1 = v[j].p3;
            edges[nedge+2].p2 = v[j].p1;
            nedge += 3;
            v[j] = v[(*ntri)-1];
            complete[j] = complete[(*ntri)-1];
            (*ntri)--;
            j--;
         }
      }

      /*
         Tag multiple edges
         Note: if all triangles are specified anticlockwise then all
               interior edges are opposite pointing in direction.
      */
      for (j=0;j<nedge-1;j++) {
         for (k=j+1;k<nedge;k++) {
            if ((edges[j].p1 == edges[k].p2) && (edges[j].p2 == edges[k].p1)) {
               edges[j].p1 = -1;
               edges[j].p2 = -1;
               edges[k].p1 = -1;
               edges[k].p2 = -1;
            }
            /* Shouldn't need the following, see note above */
            if ((edges[j].p1 == edges[k].p1) && (edges[j].p2 == edges[k].p2)) {
               edges[j].p1 = -1;
               edges[j].p2 = -1;
               edges[k].p1 = -1;
               edges[k].p2 = -1;
            }
         }
      }

      /*
         Form new triangles for the current point
         Skipping over any tagged edges.
         All edges are arranged in clockwise order.
      */
      for (j=0;j<nedge;j++) {
         if (edges[j].p1 < 0 || edges[j].p2 < 0)
            continue;
         if ((*ntri) >= trimax) {
            status = 4;
            goto skip;
         }
         v[*ntri].p1 = edges[j].p1;
         v[*ntri].p2 = edges[j].p2;
         v[*ntri].p3 = i;
         complete[*ntri] = FALSE;
         (*ntri)++;
      }
   }

   /*
      Remove triangles with supertriangle vertices
      These are triangles which have a vertex number greater than nv
   */
   for (i=0;i<(*ntri);i++) {
      if (v[i].p1 >= nv || v[i].p2 >= nv || v[i].p3 >= nv) {
         v[i] = v[(*ntri)-1];
         (*ntri)--;
         i--;
      }
   }

skip:
   if(edges)
       free(edges);
   if(complete)
       free(complete);
   return(status);
}

}



int XYZCompare(const void *v1, const void *v2)
{
	XYZ *p1 = (XYZ*)v1,*p2 = (XYZ*)v2;
	if (p1->x < p2->x) return(-1);
	else if (p1->x > p2->x) return(1);
	else return(0);
}


bool hwFeMathUtils::Triangulate(
								const hwTripleList &point_cloud,
								hwTripleList &vertices,
								hwIntList &tris,
								double EPSILON
							)
{
	if(point_cloud.size() < 3) {
		return false;
	}
	//! align the points on a plane
	hwBox3 box(point_cloud);
	if(box.IsEmpty()) {
		return false;
	}
//	{
//		hwTriple axis;
//		for(size_t i=0; i<3; ++i) {
//			axis = box.GetAxis(i);
//			fprintf(stderr, "axis = %g    %g    %g\n", axis[0], axis[1], axis[2]);
//		}
//	}
	int maxExtentId = 0, minExtentId = 0;
	double minExtent = box.GetExtent(maxExtentId), maxExtent = box.GetExtent(minExtentId);
	for(size_t i=1; i<3; ++i) {
		if(box.GetExtent(i) > maxExtent) {
			maxExtent = box.GetExtent(i);
			maxExtentId = (int)i;
		}
		if(box.GetExtent(i) < minExtent) {
			minExtent = box.GetExtent(i);
			minExtentId = (int)i;
		}
	}
//	fprintf(stderr, "minid = %d    maxid = %d\n", minExtentId, maxExtentId);
	hwTriple axis1, axis2;
	switch(maxExtentId) {
		case 0 :
			if(minExtentId == 1) { // plane is XZ
				axis1 = box.GetAxis(2);
				axis2 = box.GetAxis(0);
			}
			else {	// plane is XY
				axis1 = box.GetAxis(0);
				axis2 = box.GetAxis(1);
			}
		break;
		case 1 :
			if(minExtentId == 0) { // plane is YZ
				axis1 = box.GetAxis(1);
				axis2 = box.GetAxis(2);
			}
			else {	// plane is XY
				axis1 = box.GetAxis(0);
				axis2 = box.GetAxis(1);
			}
		break;
		case 2 :
			if(minExtentId == 0) { // plane is YZ
				axis1 = box.GetAxis(1);
				axis2 = box.GetAxis(2);
			}
			else {	// plane is Xz
				axis1 = box.GetAxis(2);
				axis2 = box.GetAxis(0);
			}
		break;
	}
//	fprintf(stderr, "axis1 = %g    %g    %g\n", axis1[0], axis1[1], axis1[2]);
//	fprintf(stderr, "axis2 = %g    %g    %g\n", axis2[0], axis2[1], axis2[2]);
	hwTriple center = hwTriple(0,0,0);
	hwSystem lcs(center, center+axis1, center+axis2);
	size_t nv = point_cloud.size();
	XYZ *p = (XYZ*) malloc(nv*sizeof(XYZ));
	for(size_t i=0; i<nv; ++i) {
		hwTriple local = lcs.GetLocal(point_cloud[i]);
		p[i].x = local.GetX();
		p[i].y = local.GetY();
		p[i].z = local.GetZ();
	}
	qsort(p,nv,sizeof(XYZ),XYZCompare);
    // 3 xyz fields are added for the _Triangulate to create its 'supertriangle'
    XYZ * newp = (XYZ*) realloc(p, (nv+3)*sizeof(XYZ));
    if (newp == nullptr)
    {
        // if reallocation fails, freshly allocate and copy data
        newp = (XYZ*) malloc((nv+3)*sizeof(XYZ));
        memcpy(newp, p, sizeof(XYZ) * nv);
        free (p);
    }
    p = newp;
	ITRIANGLE *v = (ITRIANGLE*) malloc(3*nv*sizeof(ITRIANGLE));
	int ntri = 0;

	bool status = true;
	if(_Triangulate((int)nv, p, v, &ntri, EPSILON) == 0) {
		vertices.reserve(vertices.size() + nv);
		for(size_t i=0; i<nv; ++i) {
			hwTriple global(lcs.GetGlobal(hwTriple(p[i].x, p[i].y, p[i].z)));
			vertices.push_back(global);
		}
		tris.reserve(tris.size() + ntri*3);
		for(size_t i=0; i<ntri; ++i) {
			tris.push_back(v[i].p1);
			tris.push_back(v[i].p2);
			tris.push_back(v[i].p3);
		}
	
		status = true;
	}
	else {
		status = false;
	}
	
	free(p);
	free(v);
	return status;
}

hwTriple hwFeMathUtils::GetNormal(
								//! the vertex list
								hwTripleList &vertices,
								//! the tria index list
								hwIntList &trias
							)
{
	if(trias.empty()) return hwTriple(0,0,1);
	hwTriple normal(0, 0, 0);
	hwFacePos face;
	for(size_t i=0; i<trias.size(); i+=3) {
		hwTriple v1 = vertices[trias[i]];
		hwTriple v2 = vertices[trias[i+1]];
		hwTriple v3 = vertices[trias[i+2]];
		face.SetTriaValues(v1, v2, v3);
		normal.Add(face.GetNormal());
	}
	normal.Divide((double) trias.size());
	return normal;
}
