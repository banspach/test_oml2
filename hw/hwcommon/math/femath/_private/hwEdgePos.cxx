////////////////////////////////////////////////////////////////////
// File	  : hwEdgePos.cxx
// Date	  : 21 April, 2004 - Generated By Bassayya Kalal
// Copyright (c) 2004 Altair Engineering Inc.  All Rights 
// Reserved.  Contains trade secrets of Altair Engineering, Inc.  
// Copyright notice does not imply publication.  Decompilation or 
// disassembly of this software is strictly prohibited.
////////////////////////////////////////////////////////////////////
//:---------------------------------------------------------------------------
//:Description
//
//  hwEdgePos implementation
//
//:---------------------------------------------------------------------------

#include <math/femath/hwEdgePos.h>
#include <math/femath/hwFeMathUtils.h>

/*hwEdgePos::hwEdgePos(hwudbNode &node1, hwudbNode &node2)
{
	pos1_ = node1.Pos();
	pos2_ = node2.Pos();
}*/

hwEdgePos::hwEdgePos(const hwTriple &v1, const hwTriple &v2)
{
	pos1_ = v1;
	pos2_ = v2;
}
hwEdgePos:: ~hwEdgePos()
{
	// nothing to delete
}

hwEdgePos::hwEdgePos (const hwEdgePos &edgePos)
{
	pos1_ = edgePos.pos1_;
	pos2_ = edgePos.pos2_;
}

hwEdgePos &hwEdgePos::operator=(const hwEdgePos& edgePos)
{
	pos1_ = edgePos.pos1_;
	pos2_ = edgePos.pos2_;
	return *this; 
}
// Creates an edge with two end nodes
/*hwEdgePos *hwEdgePos::Create(hwudbNode &node1, hwudbNode &node2)
{
	if(! IsValidData(node1, node2)) return 0;
	hwEdgePos *edge = NULL;
	edge = new hwEdgePos(node1, node2);
	return edge; 
}*/

// Creates an edge with two points
hwEdgePos *hwEdgePos::Create(const hwTriple &v1, const hwTriple &v2)
{
	if(! IsValidData(v1, v2)) return 0;
	hwEdgePos *edge = NULL;
	edge = new hwEdgePos(v1, v2);
	return edge; 
}

bool hwEdgePos::GetParametricPos(const hwTriple &pos, double &paramPos) const
{
	return(hwEdgePos::GetParametricPos(GetPoint1(), GetPoint2(), pos, paramPos));
	return(false); 
}
hwTriple hwEdgePos::GetActualPos(double paramPos) const
{
	return(hwEdgePos::GetActualPos(GetPoint1(), GetPoint2(), paramPos));
}

double hwEdgePos::DistanceFromPos(const hwTriple &pos, hwTriple *intersection) const
{
	return(hwEdgePos::DistanceFromPos(GetPoint1(), GetPoint2(), pos, intersection));
	return(0);
}

bool hwEdgePos::IntersectsPlane(const hwTriple &origin, 
									const hwTriple &normal,
									hwTriple *intersection
									) const
{
	return(hwEdgePos::IntersectsPlane(GetPoint1(), GetPoint2(), origin, normal,intersection));
	return(false); 
}

bool hwEdgePos::IntersectsTria(const hwTriple &v1,const hwTriple &v2,
								  const hwTriple &v3, hwTriple *intersection
								  )const
{
	return(hwEdgePos::IntersectsTria(GetPoint1(), GetPoint2(), v1, v2, v3, intersection));
	return(false); 
}

double hwEdgePos::DistanceFromPos(const hwTriple &v1,const hwTriple &v2, 
						const hwTriple &pos,hwTriple *intersection
						)
{
	// Below algorithm is based on dist = vect1 x vect2;
	// where vect1 = (pos2_ - pos1_); vect1 = norm(vect1);
	// vect2 = pos - pos1_;
	hwTriple dirVector(v2 - v1); 
	dirVector.Normalize();
	hwTriple edgeV1Pos(pos - v1);
	hwTriple crossProduct = dirVector * edgeV1Pos;
	double dist = crossProduct.Len();
	double distV1Pos = edgeV1Pos.Len();
	// This can be also be done by resolving the vector edgeP1Pos along edge
	double distV1Isect = sqrt(distV1Pos * distV1Pos - dist * dist);
	dirVector *= distV1Isect;
	double angle = hwFeMathUtils::GetAngleBetweenVectors(dirVector, edgeV1Pos);
	if(angle < 90.0) *intersection = v1 + dirVector;
	else *intersection = v1 - dirVector;
	return(dist);
}

bool hwEdgePos::IntersectsPlane(const hwTriple &v1, const hwTriple &v2,
					const hwTriple &origin,
					const hwTriple &normal, 
					hwTriple *intersection
					)
{
	double dinomValue = normal%(v2 - v1); 
	if(dinomValue == 0.0)
	{
		return(0); //edge is either parallel or not intersecting
	}
	double parmValue = (normal%(origin-v1))/dinomValue;
	//hwTriple parmVector;
	//parmVector*=parmValue;
	if(parmValue >= HW_ZERO && parmValue <= 1.0)
	{
		*intersection = v1 + (v2 - v1)*parmValue;
		return(true);
	}
	return(false); 
}
bool hwEdgePos::IntersectsTria(const hwTriple &pos1, const hwTriple &pos2, 
					const hwTriple &v1, const hwTriple &v2, 
					const hwTriple &v3, hwTriple *intersection
					)
{
	bool rayDirReversed = false;
	hwTriple triaVect1 = v2 - v1; //tria edge1
	hwTriple triaVect2 = v3 - v1; //tria edge2
	hwTriple triaNormVect = triaVect1 * triaVect2;
	if(triaNormVect[0] == 0.0 && triaNormVect[0] == 0.0 && triaNormVect[0] == 0.0) //triangle is degenerate
	{
		return (false); // do not deal with this case
	}
	hwTriple edgeOrigin = pos1;
	hwTriple edgeDirVector(pos2 - pos1); // pos direction vector
	hwTriple resVect = triaNormVect + edgeDirVector;
	if(resVect.Len() > triaNormVect.Len()) // Bas done
	{
		edgeOrigin = pos2;
		edgeDirVector = pos1 - pos2; //make edge vector and tria normal vector in opposite direction
		rayDirReversed = true;
	}
	hwTriple edgeDirTriaEdgeVect2 = edgeDirVector * triaVect2; //CROSS(pvect,dir,edge2)

	double det = triaVect1 % edgeDirTriaEdgeVect2;
	if(fabs(det) == HW_ZERO)
	{
		return(false); // edge lies on the triangle
	}
	double invDet = 1/det;
	hwTriple EdgeP1TriaV1(edgeOrigin - v1);

	double uParam = invDet * (EdgeP1TriaV1 % edgeDirTriaEdgeVect2); // cross and dot product
	if(uParam < 0.0 || uParam > 0.0)
	{
		return(false);
	}

	hwTriple EdgeP1TriaV1Edge1(EdgeP1TriaV1 * triaVect1);	
	double vParam = invDet * (edgeDirVector % EdgeP1TriaV1Edge1);
	if(vParam < 0.0 || vParam > 0.0)
	{
		return(false);
	}
	EdgeP1TriaV1 = edgeOrigin - v1;
	double a = -triaNormVect % EdgeP1TriaV1;
	double b = triaNormVect % edgeDirVector;
	// get intersect point of ray with triangle plane
	double r = a / b;
	if (r < 0.0) return (false); //no intersect or edge goes away from triangle
	//for a segment,also test if (r > 1.0) =>no intersect
	edgeDirVector *= r;
	*intersection = edgeOrigin + edgeDirVector; // intersect point of edge and tria
	return (true); // I is in T
}

bool hwEdgePos::GetParametricPos(const hwTriple &v1, const hwTriple &v2, const hwTriple &pos, double &param)
{
	hwTriple p(v2 - v1);
	hwTriple p1(pos - v1);
	double edgeLength = p.Length();
	if(fabs(edgeLength) <= HW_ZERO) {
		param = 0.0;
		return false;
	}
	// faster normalize
	p /= edgeLength;
	double length = p1%p;

	param = length/edgeLength;

	if(param < -HW_ZERO) {
		param=0.0;
		return false;
	}
	else if(param > (1.0+HW_ZERO)) {
		param=1.0;
		return false;
	}
	else return true;   
}

hwTriple hwEdgePos::GetActualPos(const hwTriple &v1, const hwTriple &v2, double param)
{
//	hwTriple dirP1P2(v2 - v1);
//	dirP1P2.Normalize(); // direction cosines
//	dirP1P2 *= Length(v1,v2)* paramPos;
//	return(v1 + dirP1P2); 

    double mult[2] = { 1.0-param, param };
    return (v1*mult[0] + v2*mult[1]);
}

/*bool hwEdgePos::IsValidData(hwudbNode &node1, hwudbNode &node2)  
{
	if(&node1 == &node2) { return (false); }
	return true;
}*/
bool hwEdgePos::IsValidData(const hwTriple &v1, const hwTriple &v2)  
{
	if(v1 == v2) {
		return false;
	}
	return true;
}

//! Intersection with a box. Return true if intersected, else false.
//! If intersected, the distance to intersection points are returned.
//! If intersection is on edge or intersecting conicident faces (flat box), distances are same.
bool hwEdgePos::IntersectsBoxAsRay( const hwTriple &min, const hwTriple &max, double& distNear, double& distFar ) const
{
	hwTriple dir(pos1_, pos2_); // get the ray direction
	dir.Normalize();
	distNear = HW_NEG_INF, distFar = HW_POS_INF; // set intersections distances to pos & neg infinity
	// box is intersection of 3 slabs. So check for each slab
	for( size_t inx = 0; inx < 3; inx++ ) { // for  X,Y and Z planes
		if( hwmath::IsZero(dir[inx]) ) { // parallel to current plane
			if( pos1_[inx] < min[inx] || pos1_[inx] > max[inx] ) // outside current start plane and end plane
				return false; // no intrersection possible if outside and parallel to current start and end planes
		}
		double t1 = (min[inx] - pos1_[inx]) / dir[inx]; // intersection distance with first plane
		double t2 = (max[inx] - pos1_[inx]) / dir[inx];	// intersection distance with second plane
		if(t1 > t2) hwmath::Swap(t1, t2); // swap to make sure t1 is for near plane. smallest is t1.
		if(t1 > distNear) distNear = t1; // want largest distNear
		if(t2 < distFar) distFar = t2; // want smallest distFar
		if( hwmath::IsEqual(distFar,distNear) ) break; // slabs/faces coincident or ray intersect at corner
		// smallest distFar less than largest distNear or distFar is -ve mean no intersection
		if(distNear > distFar || distFar < 0) return false;
	}
	return true;
}

