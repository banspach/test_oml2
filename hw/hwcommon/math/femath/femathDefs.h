/* $\Id$
** ------------------------------------------------------------------
** femathDefs.h - femath definitions.
** ------------------------------------------------------------------
** Copyright (c) 2003 Altair Engineering Inc. All Rights Reserved
** Contains trade secrets of Altair Engineering, Inc.  Copyright notice 
** does not imply publication.  Decompilation or disassembly of this 
** software is strictly prohibited.
** ------------------------------------------------------------------
*/
#ifndef HWX_FEMATH_DEFS_H
#define HWX_FEMATH_DEFS_H

// Windows export macro.
#ifdef OS_WIN
#   ifdef XFEMATH_EXPORT
#      undef XFEMATH_DECLS
#      define XFEMATH_DECLS __declspec(dllexport)
#   else
#      undef XFEMATH_DECLS
#      define XFEMATH_DECLS __declspec(dllimport)
#   endif  // XFEMATH_EXPORT
#else
#   undef XFEMATH_DECLS
#   define XFEMATH_DECLS
#endif // OS_WIN


#if OS_WIN
// disable warnings on 255 char debug symbols
#pragma warning(disable:4786)
#endif  // OS_WIN

#include <stdlib.h>
#include <iostream>

#include <algorithm>
#include <string>

#endif   // HWX_FEMATH_DEFS_H
