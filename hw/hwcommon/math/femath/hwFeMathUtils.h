///////////////////////////////////////////////////////////////////
// File	  : hwFeMathUtils.h
// Date	  : 02 August, 2004 - Generated Bassayya Kalal
// Copyright (c) 2004 Altair Engineering Inc.  All Rights 
// Reserved.  Contains trade secrets of Altair Engineering, Inc.  
// Copyright notice does not imply publication.  Decompilation or 
// disassembly of this software is strictly prohibited.
////////////////////////////////////////////////////////////////////
//:---------------------------------------------------------------------------
//:Description
//
//  hwFeMathUtils class definitions
//
//:---------------------------------------------------------------------------

#ifndef __HWFEMATHUTILS_H__
#define __HWFEMATHUTILS_H__

#include <math/femath/femathDefs.h>
#include <math/basic/hwTriple.h>
//HwConvert {{

// -------------------------------------------------------------------
//! \class hwFeMathUtils hwFeMathUtils.h math/femath/hwFeMathUtils.h
//! \brief A utility class that supports some math functions
// -------------------------------------------------------------------

class XFEMATH_DECLS hwFeMathUtils
{
	public :
	//! Gets dot product's angle in degrees
	static double GetAngleBetweenVectors(const hwTriple &v1, const hwTriple &v2);
	//! Gets dot product's angle in radians
	static double GetAngleBetweenVectorsInRadians(const hwTriple &v1, const hwTriple &v2);
	//! Get distance between two points
	static double GetDistance(const hwTriple &v1, const hwTriple &v2);

	//! Converts the given angle to radian
	static double DegreeToRadian(double angle) { return ((angle * HW_M_PI)/180.0); }
	//! Converts the given angle to degree
	static double RadianToDegree(double angle) { return ((angle * 180.0)/HW_M_PI); } 

	//! delaunay triangulation
	//! given a point cloud this function returns
	//! a modified vertex list (vertices)
	//! and an integer list (trias) which are the indices in the vertices list
	//! the number of actual trias created are trias.size()/3
	static bool Triangulate(
								// the point cloud to triangulate
								const hwTripleList &point_cloud,
								//! the vertex list
								hwTripleList &vertices,
								//! the tria index list
								hwIntList &trias,
								//! an epsilon for error estimation
								double epsilon = HW_ZERO
							);
							
	//! Given a vertex list (vertices)
	//! and an integer list (trias) which are the indices in the vertices list
	//! returns the average normal
	static hwTriple GetNormal(
								//! the vertex list
								hwTripleList &vertices,
								//! the tria index list
								hwIntList &trias
							);
private :
};


#endif // __HWFEMATHUTILS_H__
