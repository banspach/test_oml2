# Sample GitLab Project

This sample project shows how a project in GitLab looks for demonstration purposes. It contains issues, merge requests and Markdown files in many branches,
named and filled with lorem ipsum.

You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

This project only includes files to build Command Line Compose
	hwcommon/math,
	hwcommon/oml 
	and the make files to build the binaries.

hwcommon/math/makefile is modified to disable femath build.
hwcommon/oml/toolboxes/makefile is modified to disable libraries omlpythonbridge and omlipc.

1. Setup Enviroment

The setup.bat file can be used to set environment variables and paths to build and launch command line Compose.
You will need to make the following modifications to a Perforce workspace on your computer

@echo MODIFY HW_THIRDPARTY to a Perforce Workspace on your computer
set HW_THIRDPARTY=C:/sandboxes/comp22.3_slim_mu/hw/third_party

@echo MODIFY HW_FRAMEWORK to a Perforce Workspace on your computer
set HW_FRAMEWORK=C:/sandboxes/comp22.3_slim_mu/common/framework/win64/hwx

@echo ADD A PATH TO AN MKL DIRECTORY from a Perforce Workspace on your computer 
set PATH=%PATH%;C:\sandboxes\comp22.3_slim_mu\hw\third_party\intel\intel2019u3\win64

2. Build Compose

The following intructions are working for me.

	From the root directory
	cd hw\hwcommon\math
	make
	cd ..\oml
	make

I think I should be able to run the math and oml makefiles using a path, but that does not work for me.

The following example from the root directory does not run for me.  
Example:  make -f hw\hwcommon\math\makefile

make -f hw\hwcommon\math\makefile
make[1]: *** kernel: No such file or directory.  Stop.
make: *** [C:/gitlab/test_oml2B/hw/make-subprojects.cfg:86: kernel_target] Error 2


